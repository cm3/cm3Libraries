unit = 1.;
L = 1*unit;

lsca = L;
Point(1)= {0,0,0,lsca};
Point(2)= {L,0,0,lsca};
Point(3)= {L,L,0,lsca};
Point(4)= {0,L,0,lsca};
//+
Line(1) = {4, 3};
//+
Line(2) = {3, 2};
//+
Line(3) = {2, 1};
//+
Line(4) = {1, 4};
//+
Curve Loop(1) = {1, 2, 3, 4};
//+
Plane Surface(1) = {1};
//+
Extrude {0, 0, 1} {
  Surface{1}; Layers{1}; Recombine;
}
//+
Physical Volume(11) = {1};
//+
Physical Surface(1) = {25};
//+
Physical Surface(2) = {17};
//+
Physical Surface(3) = {21};
//+
Physical Surface(4) = {13};
//+
Physical Surface(5) = {1};
//+
Physical Surface(6) = {26};
//+
Physical Point(1) = {1};
//+
Physical Point(2) = {2};
//+
Physical Point(3) = {3};
//+
Physical Point(4) = {4};
//+
Physical Point(5) = {14};
//+
Physical Point(6) = {10};
//+
Physical Point(7) = {6};
//+
Physical Point(8) = {5};
//+
Transfinite Curve {1, 3} = 2 Using Progression 1;
Transfinite Curve {2, 4} = 3 Using Progression 1;
//+
Transfinite Surface {1};
//+
Recombine Surface {1};
