#coding-Utf-8-*-

from gmshpy import *
from dG3Dpy import*
from math import*

#script to launch PBC problem with a python script

# material law
lawnum = 11 # unique number of law

# material law
lawnum = 11 # unique number of law
law1 = IMDEACPUMATDG3DMaterialLaw(lawnum,'crystal.prop'); 
#lawPert1 = dG3DMaterialLawWithTangentByPerturbation(law1, 1e-8)


# geometry
meshfile="cube.msh" # name of mesh file

fullDg = 0 #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)
beta1  = 100
# creation of part Domain
nfield = 29 # number of the field (physical number of surface)

myfield1 = dG3DDomain(1000,nfield,space1,lawnum,fullDg,3)
#myfield1.matrixByPerturbation(1,1,1,1e-8)
#myfield1.stabilityParameters(beta1)

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 100  # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-8  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)



# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
#mysolver.addMaterialLaw(lawPert1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)

mysolver.displacementBC('Face',32,2,0.)
mysolver.displacementBC('Face',34,1,0.)
mysolver.displacementBC('Face',30,0,0.)

#mysolver.displacementBC('Face',1,0,0)
#mysolver.displacementBC('Face',1,1,0)
mysolver.displacementBC('Face',31,0,0.2)

# build view
mysolver.internalPointBuildView("Strain_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("Strain_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("Strain_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("Strain_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("Strain_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("Strain_xz",IPField.STRAIN_XZ, 1, 1);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);
mysolver.internalPointBuildView("pression",IPField.PRESSION,1,1)
mysolver.internalPointBuildView("plastic possion ratio",IPField.PLASTIC_POISSON_RATIO,1,1)

#archiving
mysolver.archivingForceOnPhysicalGroup('Face',30,0)
mysolver.archivingForceOnPhysicalGroup('Face',30,1)
mysolver.archivingForceOnPhysicalGroup('Face',30,2)
mysolver.archivingNodeDisplacement(43,0,1)
mysolver.archivingNodeDisplacement(43,1,1)
mysolver.archivingNodeDisplacement(43,2,1)
# solve
mysolver.solve()

check = TestCheck()
check.equal(-5.574939e+09,mysolver.getArchivedForceOnPhysicalGroup("Face", 30, 0),1.e-3)
