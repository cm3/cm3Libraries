import pandas as pd
import matplotlib.pyplot as plt

plt.rcParams.update({'font.size': 14})


plt.figure(1)
f7 = pd.read_csv('REF/force2comp1.csv',sep=';',names= ['Time','Val'])
u7 = pd.read_csv('REF/NodalDisplacementPhysical6Num3comp1.csv',sep=';',names= ['Time','Val'])
x = u7['Val'].values
y = f7['Val'].values
x[0] = 0.0
y[0] = 0
plt.plot(x,-y,'r-',linewidth=2,label=r'$T^{\rm{perfect}}(2^3)$ REF')

f7 = pd.read_csv('force2comp1.csv',sep=';',names= ['Time','Val'])
u7 = pd.read_csv('NodalDisplacementPhysical6Num3comp1.csv',sep=';',names= ['Time','Val'])
x = u7['Val'].values
y = f7['Val'].values
x[0] = 0.0
y[0] = 0
plt.plot(x,-y,'g--',linewidth=2,label=r'$T^{\rm{perfect}}(2^3)$ CUR')


plt.xlabel(r'Prescribed displacement $u_y$ (mm)')
plt.ylabel(r'Reaction force $\bar{F}_y$ (N)')
plt.legend(loc='upper left')

plt.savefig("FE_UD_forceDisp.png",bbox_inches='tight')
plt.savefig("FE_UD_forceDisp.eps",bbox_inches='tight')
plt.savefig("FE_UD_forceDisp.pdf",bbox_inches='tight')

plt.show()
