mm = 1.;

R = 0.5*mm;
H = 2*R;
L = 4*R;

lscaf = 0.1*mm;
lsca = 10*lscaf;

p1 = newp; Point(p1) = {0,0,0,lsca};
p2 = newp; Point(p2) = {R,0,0,lscaf};
p3 = newp; Point(p3) = {H,0,0,lscaf};
p4 = newp; Point(p4) = {H,L,0,lsca};
p5 = newp; Point(p5) = {0,L,0,lsca};
p6 = newp; Point(p6) = {0,R,0,lscaf};
p7 = newp; Point(p7) = {R*Sqrt(0.5),R*Sqrt(0.5),0,lscaf};

//+
Line(1) = {2, 3};
//+
Line(2) = {3, 4};
//+
Line(3) = {4, 5};
//+
Line(4) = {5, 6};
//+
Circle(5) = {6, 1, 7};
//+
Circle(6) = {7, 1, 2};
//+
Curve Loop(1) = {4, 5, 6, 1, 2, 3};
//+
Plane Surface(1) = {1};
//+
Physical Surface(11) = {1};
//+
Physical Curve(2) = {1};
//+
Physical Curve(3) = {2};
//+
Physical Curve(4) = {3};
//+
Physical Curve(5) = {4};
//+
Physical Point(6) = {4};
//+
Physical Point(7) = {5};
