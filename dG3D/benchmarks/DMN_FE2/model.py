#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*

#script to launch PBC problem with a python script
#DEFINE MICRO PROBLEM

linearElastic=0

if linearElastic:
  # material law
  lawnum1 = 11 # unique number of law
  E = 3E3 #MPa
  nu = 0.3
  K = E/3./(1.-2.*nu)	# Bulk mudulus
  mu =E/2./(1.+nu)	  # Shear mudulus
  rho = 7850e-9 # Bulk mass
  sy0 = 100. #MPa
  Dsy = 0.2*sy0
  h = 30.
  harden1 = ExponentialJ2IsotropicHardening(lawnum1, sy0,Dsy,h)
  law1   = J2SmallStrainDG3DMaterialLaw(lawnum1,rho,E,nu,harden1)

  lawnum2 = 12 # unique number of law
  E2 = E*10.
  nu2 = 0.2
  appOrder = 11 # approximation order for exp and log operator of a tensor

  law2 = dG3DLinearElasticMaterialLaw(lawnum2,rho,E2,nu2)

else:

  # material law
  lawnum1 = 11 # unique number of law
  E = 3E3 #MPa
  nu = 0.3
  K = E/3./(1.-2.*nu)	# Bulk mudulus
  mu =E/2./(1.+nu)	  # Shear mudulus
  rho = 7850e-9 # Bulk mass
  sy0 = 100. #MPa
  Dsy = 0.2*sy0
  h = 30.
  harden1 = ExponentialJ2IsotropicHardening(lawnum1, sy0,Dsy,h)
  law1   = J2LinearDG3DMaterialLaw(lawnum1,rho,E,nu,harden1)
  law1.setStrainOrder(11)

  lawnum2 = 12 # unique number of law
  E2 = E*10.
  nu2 = 0.2
  appOrder = 11 # approximation order for exp and log operator of a tensor
  potential2 = biLogarithmicElasticPotential(lawnum2,E2,nu2,appOrder)
  law2 = dG3DHyperelasticMaterialLaw(lawnum2,rho,potential2)


# DEFINE MACROPROBLEM
matnum1 = 1;
rho = 1e-9
macromat1 = DMNBasedDG3DMaterialLaw(matnum1, rho, "DMNInteraction.txt",False,1e-6)
macromat1.addLaw(0,law1)
macromat1.addLaw(1,law2)
macromat1.fixDofsByComp(2)
macromat1.viewInternalData(IPField.F_XX)
macromat1.viewInternalData(IPField.F_YY)
macromat1.viewIPAveragePerPhase(0,IPField.PLASTICSTRAIN)
macromat1.viewIPAverage(IPField.SVM)
macromat1.viewBulkIPs(22,0)
macromat1.viewBulkIPs(23,0)

macromeshfile="model.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 200  # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-4  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)


# creation of  macro part Domain

dim =2
beta1 = 50;
fullDG = False;

averageStrainBased = True

# non DG domain
nfield1 = 11
macrodomain1 = dG3DDomain(10,nfield1,0,matnum1,fullDG,dim)
macrodomain1.stabilityParameters(beta1)
macrodomain1.averageStrainBased(averageStrainBased)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(macromeshfile)

mysolver.addDomain(macrodomain1)
mysolver.addMaterialLaw(macromat1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)
mysolver.options("-ksp_type preonly -pc_type lu -pc_factor_mat_solver_type petsc")


# boundary condition
mysolver.displacementBC("Face",11,2,0.0)

mysolver.displacementBC("Edge",2,1,0.0)
mysolver.displacementBC("Edge",5,0,0.0)

fct = PiecewiseLinearFunction()
fct.put(0.,0.)
fct.put(0.1,0.025)
fct.put(0.2,0.02)
fct.put(0.3,0.025)
fct.put(0.4,0.035)
fct.put(0.6,0.03)
fct.put(0.7,0.035)
fct.put(0.8,0.05)
fct.put(1.,0.03)
mysolver.displacementBC("Edge",4,1,fct)

mysolver.setNumStepTimeInterval(0.1,20)
mysolver.setNumStepTimeInterval(0.2,3)
mysolver.setNumStepTimeInterval(0.3,3)
mysolver.setNumStepTimeInterval(0.4,30)
mysolver.setNumStepTimeInterval(0.6,3)
mysolver.setNumStepTimeInterval(0.7,3)
mysolver.setNumStepTimeInterval(0.8,80)
mysolver.setNumStepTimeInterval(1.,5)



# archivage
mysolver.internalPointBuildView("U_XX",IPField.U_XX);
mysolver.internalPointBuildView("U_XY",IPField.U_XY);
mysolver.internalPointBuildView("U_XZ",IPField.U_XZ);
mysolver.internalPointBuildView("U_YY",IPField.U_YY);
mysolver.internalPointBuildView("U_YZ",IPField.U_YZ);
mysolver.internalPointBuildView("U_ZZ",IPField.U_ZZ);
mysolver.internalPointBuildView("U_NORM",IPField.U_NORM);

mysolver.internalPointBuildViewIncrement("U_XX",IPField.U_XX);
mysolver.internalPointBuildViewIncrement("U_XY",IPField.U_XY);
mysolver.internalPointBuildViewIncrement("U_XZ",IPField.U_XZ);
mysolver.internalPointBuildViewIncrement("U_YY",IPField.U_YY);
mysolver.internalPointBuildViewIncrement("U_YZ",IPField.U_YZ);
mysolver.internalPointBuildViewIncrement("U_ZZ",IPField.U_ZZ);
mysolver.internalPointBuildViewIncrement("U_NORM",IPField.U_NORM);

mysolver.internalPointBuildView("Green-Lagrange_xx",IPField.GL_XX, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yy",IPField.GL_YY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_zz",IPField.GL_ZZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xy",IPField.GL_XY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yz",IPField.GL_YZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xz",IPField.GL_XZ, 1, 1);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange equivalent strain",IPField.GL_EQUIVALENT_STRAIN, 1, 1);
mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);

mysolver.internalPointBuildView("Deformation energy",IPField.DEFO_ENERGY, 1, 1);
mysolver.internalPointBuildView("Plastic energy",IPField.PLASTIC_ENERGY, 1, 1);

mysolver.archivingForceOnPhysicalGroup("Edge",2,1)
mysolver.archivingForceOnPhysicalGroup("Edge",2,0)
mysolver.archivingNodeDisplacement(6,1)

# solve
mysolver.solve()

check = TestCheck()
check.equal(-3.845082e+01,mysolver.getArchivedForceOnPhysicalGroup("Edge",2, 1),1.e-4)
