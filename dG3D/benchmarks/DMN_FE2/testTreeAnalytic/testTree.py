from gmshpy import *
from dG3Dpy import*
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import os

def treeTest(C0, C1, W0, W1, W2, W3, n10, n11, n00):
  Cleft = fullMatrixDouble()
  f0 = W0/(W0+W1)
  homogenizedTangentBimaterial(C0,C1,f0,n10[0],n10[1],n10[2],Cleft)
  
  Cright = fullMatrixDouble()
  f0 = W2/(W2+W3)
  homogenizedTangentBimaterial(C0,C1,f0,n11[0],n11[1],n11[2],Cright)
  
  Ceff = fullMatrixDouble()
  f0 = (W0+W1)/(W0+W1+W2+W3)
  homogenizedTangentBimaterial(Cleft,Cright,f0,n00[0],n00[1],n00[2],Ceff)
  return Ceff
  
  
def getHook(E, nu, planeStrain):
	A = fullMatrixDouble() 
	isotropicElasticTensor(E,nu,A)
	Are = fullMatrixDouble()
	if planeStrain:
		reducePlaneStrain(A,Are)
		return Are
	else:
	  reduceMat99ToMat66(A,Are)
	  return Are

def upLayer(Cin, Win, nin):
  N = len(Cin)
  print(f'numInput={N}')
  curSize = N//2
  Cout = []
  Wout = np.zeros(curSize)
  N = len(Cin)
  for i in range(N//2):
    Cleft = Cin[2*i]
    Cright = Cin[2*i+1]
    n = nin[i]
    Wout[i] = (Win[2*i]+Win[2*i+1])
    Ceff = fullMatrixDouble()
    f0 = Win[2*i]/(Win[2*i]+Win[2*i+1])
    homogenizedTangentBimaterial(Cleft,Cright,f0,n[0],n[1],n[2],Ceff)
    Cout.append(Ceff)
  return Cout, Wout

def randomTree(C0,C1,N, W, n):
  '''N - deep
     W - weights = 2**(N-1)
     n - normal = 2**(N-1)-1
  '''
  Wstart = W[:]
  Cstart = []
  for i in range(2**(N-1)):
    if i%2==0:
      Cstart.append(C0)
    else:
      Cstart.append(C1)
  print(f'Wstart={Wstart}')
  startPos = 0
  for i in range(N-1,0,-1):
    print(f"depth={i}")
    numEl = 2**(i-1)
    print(f'numEl = {numEl}')
    nstart = n[startPos:(startPos+numEl)]
    print(nstart)
    Cout, Wout = upLayer(Cstart, Wstart, nstart)
    print(f'CoutSize = {len(Cout)}, WoutSize = {len(Wout)}')
    Wstart = Wout[:]
    Cstart = Cout[:]
    startPos = startPos+numEl
    print(f'Wstart={Wstart}')
  return Cstart[0], Wstart[0]
  
planeStrain = False

C0 = getHook(70e3,0.3,planeStrain)
C0._print('matrix C0')

C1 = getHook(70e5,0.2,planeStrain)
C1._print('matrix C1')

Cvoid = getHook(0,0.,planeStrain)
Cvoid._print('matrix Cvoid')
  
N = 4
normal = np.random.rand(2**(N-1)-1,3)
#for i in range(2**(N-1)-1):
#  normal[i,2]=0.
W = np.random.rand(2**(N-1))
C, Wout = randomTree(C0,Cvoid,N,W,normal)
C._print()
print(Wout, np.sum(W))
print(f'detC= {C.determinant()}')

print(normal, W)
Ceff1 = treeTest(C0,Cvoid,W[0],W[1],W[2],W[3],normal[0], normal[1], normal[4])
Ceff2 = treeTest(C0,Cvoid,W[4],W[5],W[6],W[7],normal[2], normal[3], normal[5])
print(f'det Ceff1= {Ceff1.determinant()}')
print(f'det Ceff2= {Ceff2.determinant()}')
invCeff1 = fullMatrixDouble()

Ceff = fullMatrixDouble()
f0 = (W[0]+W[1]+W[2]+W[3])/(W[0]+W[1]+W[2]+W[3]+W[4]+W[5]+W[6]+W[7])
homogenizedTangentBimaterial(Ceff1,Ceff2,f0,normal[6][0],normal[6][1],normal[6][2],Ceff)
Ceff._print('Ceff')
print(f'detCeff= {Ceff.determinant()}')
