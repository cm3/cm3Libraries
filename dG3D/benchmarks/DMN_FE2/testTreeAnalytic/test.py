from gmshpy import *
from dG3Dpy import*
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import os

def getHook(E, nu, planeStrain):
	A = fullMatrixDouble() 
	isotropicElasticTensor(E,nu,A)
	Are = fullMatrixDouble()
	if planeStrain:
		reducePlaneStrain(A,Are)
		return Are
	else:
	  reduceMat99ToMat66(A,Are)
	  return Are

def treeTest(C0, C1, W0, W1, W2, W3, n10, n11, n00):
  Cleft = fullMatrixDouble()
  f0 = W0/(W0+W1)
  homogenizedTangentBimaterial(C0,C1,f0,n10[0],n10[1],n10[2],Cleft)
  
  Cright = fullMatrixDouble()
  f0 = W2/(W2+W3)
  homogenizedTangentBimaterial(C0,C1,f0,n11[0],n11[1],n11[2],Cright)
  
  Ceff = fullMatrixDouble()
  f0 = (W0+W1)/(W0+W1+W2+W3)
  homogenizedTangentBimaterial(Cleft,Cright,f0,n00[0],n00[1],n00[2],Ceff)
  
  return Ceff
  
def getPerpenVec(n):
  if np.abs(n[0]) > 0:
    return [-(n[1]+n[2]),n[0],n[0]]
  else:
    return [1.,-n[2],n[1]]
    
   
  
def treeTestComplianceBase(C0, C1, W0, W1, W2, W3, n10, n11, n00):
  Cleft = fullMatrixDouble()
  f0 = W0/(W0+W1)
  t10 = getPerpenVec(n10)
  homogenizedComplianceBimaterial(C0,C1,f0,n10[0],n10[1],n10[2],t10[0],t10[1],t10[2],Cleft)
  
  Cright = fullMatrixDouble()
  f0 = W2/(W2+W3)
  t11 = getPerpenVec(n11)
  homogenizedComplianceBimaterial(C0,C1,f0,n11[0],n11[1],n11[2],t11[0],t11[1],t11[2],Cright)
  
  Ceff = fullMatrixDouble()
  f0 = (W0+W1)/(W0+W1+W2+W3)
  t00 = getPerpenVec(n00)
  homogenizedComplianceBimaterial(Cleft,Cright,f0,n00[0],n00[1],n00[2],t00[0],t00[1],t00[2],Ceff)
  
  return Ceff



planeStrain = False

C0 = getHook(70e3,0.3,planeStrain)
C0._print('matrix C0')

C1 = getHook(70e5,0.2,planeStrain)
C1._print('matrix C1')

Cvoid = getHook(0,0.,planeStrain)
Cvoid._print('matrix Cvoid')

n0 = 1.
n1 = 1.
n2 = 1.
f0 = 0.5

homo = BimaterialHomogenization(planeStrain)

Ceff1 = fullMatrixDouble()
homo.compute(C0,C1,f0,n0,n1,n2,Ceff1)
Ceff1._print('CASE 1')

Ceff2 = fullMatrixDouble()
homogenizedTangentBimaterial(C0,C1,f0,n0,n1,n2,Ceff2)
Ceff2._print("CASE 2")

Ceff3 = fullMatrixDouble()
homogenizedComplianceBimaterial(C0,C1,f0,n0,n1,n2,0,-1.,1.,Ceff3)
Ceff3._print("CASE 3")


n00 = np.random.rand(3)
n10 = np.random.rand(3)
n11 = np.random.rand(3)
n20 = np.random.rand(3)
n21 = np.random.rand(3)
n21 = np.random.rand(3)
n22 = np.random.rand(3)
n23 = np.random.rand(3)
W = np.random.rand(8)
print(W)

Ceff = treeTest(C0,C1,W[0],W[1],W[2],W[3],n10, n11, n00)
Ceff._print('stiffness basesd')

CeffCl = treeTestComplianceBase(C0,C1,W[0],W[1],W[2],W[3],n10, n11, n00)

CeffCl._print('compliance based')

CeffVoidLeft = treeTest(C0,Cvoid,W[0],W[1],W[2],W[3],n20, n21, n10)
CeffVoidLeft._print('CeffVoidLeft')
print(f'CeffVoidLeft.determinant() = {CeffVoidLeft.determinant()}')
CeffVoidRight = treeTest(C0,Cvoid,W[4],W[5],W[6],W[7],n22, n23, n11)
CeffVoidRight._print('CeffVoidRight')
print(f'CeffVoidRight.determinant() = {CeffVoidRight.determinant()}')
f0 = (W[0]+W[1]+W[2]+W[3])/(W[0]+W[1]+W[2]+W[3]+W[4]+W[5]+W[6]+W[7])
Ceffall= fullMatrixDouble()
homogenizedTangentBimaterial(CeffVoidLeft,CeffVoidRight,f0,n00[0],n00[1],n00[2],Ceffall)
Ceffall._print('Ceffall - stiffness based')
print(f'Ceffall.determinant() = {Ceffall.determinant()}')

'''
t00 = getPerpenVec(n00)
print(t00)
homogenizedComplianceBimaterial(CeffVoidLeft,CeffVoidRight,f0,n00[0],n00[1],n00[2],t00[0],t00[1],t00[2],Ceffall)
Ceffall._print('Ceffall - compliance based')
'''
