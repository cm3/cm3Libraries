# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import matplotlib as mpl


# activate latex text rendering
plt.rc('xtick', labelsize=14)
plt.rc('ytick', labelsize=14)

plt.close('all')

plt.figure(1)

err = pd.read_csv('adaptiveSGD-laminateTraining-old/history.csv',sep=';')
plt.plot(1+err['Epoch'],err['LossTrain'],'r-',label='adaptiveSGD-laminateTraining-old')
plt.plot(1+err['Epoch'],err['LossTest'],'r--')

err = pd.read_csv('Adam-laminateTraining/history.csv',sep=';')
plt.plot(1+err['Epoch'],err['mare-Train'],'g-',label='Adam-laminateTraining')
plt.plot(1+err['Epoch'],err['mare-Test'],'g--')

err = pd.read_csv('Adam-linearElasticTraining/history.csv',sep=';')
plt.plot(1+err['Epoch'],err['mare-Train'],'b-',label='Adam-linearElasticTraining')
plt.plot(1+err['Epoch'],err['mare-Test'],'b--')

err = pd.read_csv('SGD-laminateTraining/history.csv',sep=';')
plt.plot(1+err['Epoch'],err['mare-Train'],'c-',label='SGD-laminateTraining')
plt.plot(1+err['Epoch'],err['mare-Test'],'c--')

plt.plot([1, 50],[0.01,0.01],'--')

plt.xscale('log')
plt.yscale('log')
plt.xlabel('Epochs',fontsize=14)
plt.ylabel('Error',fontsize=14)
plt.legend(loc='right',ncol=1,fontsize=14,bbox_to_anchor=(1.1,1))

plt.savefig('trainingComparison.pdf',bbox_inches = "tight")
plt.savefig('trainingComparison.eps',bbox_inches = "tight")

plt.show()
