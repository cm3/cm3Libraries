from gmshpy import *
from dG3Dpy import*
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import os

def listToMat(v):
  vec = fullVectorDouble(len(v))
  for i in range(0,len(v)):
    vec.set(i,v[i])
  mat = fullMatrixDouble()
  vecToMat(vec,9,9,mat)
  return mat


plt.close('all')
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)

tree = Tree()
retrain =0
if retrain:
  fname = 'best_tree_trained.txt'
  tree.loadDataFromFile(fname)
  tree.printTree()
else:
  tree.createPerfectTree(5,2,1,'rectifier')
  tree.initialize()
  tree.printTree()
  tree.saveDataToFile("tree_initial.txt")

# show figure of tree
tree.printTreeInteraction("treeView.txt",True)
os.system('dot  -Teps treeView.txt > treeView.eps')
os.system('epstopdf treeView.eps')

tree.printPhaseFraction()

planeStrain=True
homo= LaminateHomogenization(planeStrain)
traingT = TrainingDeepMaterialNetwork(tree,homo)

dataSource = '../../offlineData/'
C1 = pd.read_csv(dataSource+'C1.csv',sep=';')
C2 = pd.read_csv(dataSource+'C2.csv',sep=';')
Ceff = pd.read_csv(dataSource+'Chomo.csv',sep=';')
#print(C1)
#print(C2)
#print(Ceff)

if retrain:
  ptrain = pd.read_csv("indexTrain.csv",sep=';')
  ptest = pd.read_csv("indexTest.csv",sep=';')
  indexTrain = ptrain['indexTrain']
  indexTest = ptest['indexTest']
else:
  dataSize = len(Ceff)
  values = [i for i in range(dataSize)]
  np.random.shuffle(values)
  indexTrain, indexTest = values[:200], values[200:]
  ptrain = pd.DataFrame(indexTrain,columns=['indexTrain'])
  ptest = pd.DataFrame(indexTest,columns=['indexTest'])
  ptrain.to_csv("indexTrain.csv",sep=';',index = False)
  ptest.to_csv("indexTest.csv",sep=';',index = False)

ptrain = pd.DataFrame(indexTrain,columns=['indexTrain'])
ptest = pd.DataFrame(indexTest,columns=['indexTest'])
ptrain.to_csv("indexTrain.csv",sep=';',index = False)
ptest.to_csv("indexTest.csv",sep=';',index = False)

print('For train:\n', indexTrain)
print('For test:\n',indexTest)

Nsample = len(indexTrain)
print('number of samples = %d'%Nsample)
traingT.trainingDataSize(Nsample,2)
# add samples data
samp = 0
for i in indexTrain:
  C1mat = listToMat(C1.values[i][:])
  C2mat = listToMat(C2.values[i][:])
  Ceffmat = listToMat(Ceff.values[i][:])
  #C1mat._print("C1mat")
  #C2mat._print("C2mat")
  #Ceffmat._print("Ceffmat")
  traingT.setTrainingSample(samp,C1mat,C2mat,Ceffmat)
  samp += 1

Ntest = len(indexTest)
print('number of test samples = %d'%Ntest)
traingT.testDataSize(Ntest,2)
samp = 0
for i in indexTest:
  C1mat = listToMat(C1.values[i][:])
  C2mat = listToMat(C2.values[i][:])
  Ceffmat = listToMat(Ceff.values[i][:])
  #C1mat._print("C1mat")
  #C2mat._print("C2mat")
  #Ceffmat._print("Ceffmat")
  traingT.setTestSample(samp,C1mat,C2mat,Ceffmat)
  samp += 1


learningRate = 1.e-3
maxEpoch = 50
historyFile = 'history.csv'
treeFile = 'tree_trained.txt'
loss ="mare"
testMesure = "mare"
nbBatches = int(Nsample/1)

optimizer = Adam()
lrScheduler = PowerLearningRateDecay(learningRate,0.5,maxEpoch/2.)
trainPro = GradientDescentTraining(traingT,optimizer,lrScheduler)
trainPro.fit(loss,learningRate,maxEpoch,nbBatches,testMesure,treeFile,historyFile)

tree.printPhaseFraction()

hist  = pd.read_csv(historyFile,sep=';')
plt.plot(hist['Epoch'],hist[loss+'-Train'],'-',label='Train')
plt.plot(hist['Epoch'],hist[loss+'-Test'],'--',label='Test')
plt.yscale('log')
plt.legend(fontsize = 14, loc='upper right')
plt.ylabel('Error (%)',fontsize=14, )
plt.xlabel('Epoch',fontsize=14)
plt.title('f0 = %.5f f1 = %.5f'%(tree.getPhaseFraction(0),tree.getPhaseFraction(1)))
plt.savefig(treeFile[:-4]+'error_history.pdf')
#plt.show()
