from gmshpy import *
from dG3Dpy import*
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import os

def listToMat(v):
  vec = fullVectorDouble(len(v))
  for i in range(0,len(v)):
    vec.set(i,v[i])
  mat = fullMatrixDouble()
  vecToMat(vec,9,9,mat)
  return mat

plt.close('all')
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)

tree = Tree()
retrain =0
if retrain:
  fname = 'tree_trained.txt'
  tree.loadDataFromFile(fname)
  tree.printTree()
else:
  tree.createPerfectTree(5,2,1,'rectifier')
  tree.initialize()
  tree.printTree()
  tree.saveDataToFile("tree_initial.txt")
tree.printTreeInteraction("treeView.txt",True)
os.system('dot  -Teps treeView.txt > treeView.eps')
os.system('epstopdf treeView.eps')

tree.printPhaseFraction()

# get interation
ni = NetworkInteraction()
ni.getInteractionFromTree(tree)
reduction = CoefficientReductionAll()
traingT = TrainingDeepMaterialNetworkLinearElastic(ni,reduction)

dataSource = '../../offlineData/'
C1 = pd.read_csv(dataSource+'C1.csv',sep=';')
C2 = pd.read_csv(dataSource+'C2.csv',sep=';')
Ceff = pd.read_csv(dataSource+'Chomo.csv',sep=';')
#print(C1)
#print(C2)
#print(Ceff)

if retrain:
  ptrain = pd.read_csv("indexTrain.csv",sep=';')
  ptest = pd.read_csv("indexTest.csv",sep=';')
  indexTrain = ptrain['indexTrain']
  indexTest = ptest['indexTest']
else:
  dataSize = len(Ceff)
  values = [i for i in range(dataSize)]
  np.random.shuffle(values)
  indexTrain, indexTest = values[:200], values[200:]
  ptrain = pd.DataFrame(indexTrain,columns=['indexTrain'])
  ptest = pd.DataFrame(indexTest,columns=['indexTest'])
  ptrain.to_csv("indexTrain.csv",sep=';',index = False)
  ptest.to_csv("indexTest.csv",sep=';',index = False)

ptrain = pd.DataFrame(indexTrain,columns=['indexTrain'])
ptest = pd.DataFrame(indexTest,columns=['indexTest'])
ptrain.to_csv("indexTrain.csv",sep=';',index = False)
ptest.to_csv("indexTest.csv",sep=';',index = False)

print('For train:\n', indexTrain)
print('For test:\n',indexTest)

# to store during training
allLaws = list()
allModels = list()

Nsample = len(indexTrain)
print('number of samples = %d'%Nsample)
traingT.trainingDataSize(Nsample)
# add samples data
samp = 0
for i in indexTrain:
  C1mat = listToMat(C1.values[i][:])
  C2mat = listToMat(C2.values[i][:])
  Ceffmat = listToMat(Ceff.values[i][:])
  #C1mat._print("C1mat")
  #C2mat._print("C2mat")
  #Ceffmat._print("Ceffmat")
  rho = 7850e-9 # Bulk mass
  lawnum1 = 11 # unique number of law
  pot1  = smallStrainLinearElasticPotential(lawnum1,C1mat)
  law1   = dG3DHyperelasticMaterialLaw(lawnum1,rho,pot1)

  lawnum2 = 12 # unique number of law
  pot2  = smallStrainLinearElasticPotential(lawnum2,C2mat)
  law2   = dG3DHyperelasticMaterialLaw(lawnum2,rho,pot2)

  dmn = dG3DDeepMaterialNetwork()
  dmn.addLaw(0,law1)
  dmn.addLaw(1,law2)
  dmn.fixDofsByComp(2)
  allLaws.append(law1)
  allLaws.append(law2)
  allModels.append(dmn)
  traingT.setTrainingSample(samp,dmn,Ceffmat)
  samp += 1


Ntest = len(indexTest)
print('number of test samples = %d'%Ntest)
traingT.testDataSize(Ntest)
samp = 0
for i in indexTest:
  C1mat = listToMat(C1.values[i][:])
  C2mat = listToMat(C2.values[i][:])
  Ceffmat = listToMat(Ceff.values[i][:])
  #C1mat._print("C1mat")
  #C2mat._print("C2mat")
  #Ceffmat._print("Ceffmat")
  rho = 7850e-9 # Bulk mass
  lawnum1 = 11 # unique number of law
  pot1  = smallStrainLinearElasticPotential(lawnum1,C1mat)
  law1   = dG3DHyperelasticMaterialLaw(lawnum1,rho,pot1)

  lawnum2 = 12 # unique number of law
  pot2  = smallStrainLinearElasticPotential(lawnum2,C2mat)
  law2   = dG3DHyperelasticMaterialLaw(lawnum2,rho,pot2)

  dmn = dG3DDeepMaterialNetwork()
  dmn.addLaw(0,law1)
  dmn.addLaw(1,law2)
  dmn.fixDofsByComp(2)
  allLaws.append(law1)
  allLaws.append(law2)
  allModels.append(dmn)
  traingT.setTestSample(samp,dmn,Ceffmat)
  samp += 1


learningRate = 1.e-3
maxEpoch = 50
historyFile = 'history.csv'
treeFile = 'tree_trained.txt'
loss ="mare"
testMesure = "mare"
nbBatches = int(Nsample/1)

optimizer = Adam()
lrScheduler = PowerLearningRateDecay(learningRate,0.5,maxEpoch/2.)
trainPro = GradientDescentTraining(traingT,optimizer,lrScheduler)
trainPro.fit(loss,learningRate,maxEpoch,nbBatches,testMesure,treeFile,historyFile)


hist  = pd.read_csv(historyFile,sep=';')
plt.plot(hist['Epoch'],hist[loss+'-Train'],'-',label='Train')
plt.plot(hist['Epoch'],hist[loss+'-Test'],'--',label='Test')
plt.yscale('log')
plt.legend(fontsize = 14, loc='upper right')
plt.ylabel('Error (%)',fontsize=14, )
plt.xlabel('Epoch',fontsize=14)
plt.title('f0 = %.5f f1 = %.5f'%(ni.getPhaseFraction(0),ni.getPhaseFraction(1)))
plt.savefig(treeFile[:-4]+'error_history.pdf')
#plt.show()
