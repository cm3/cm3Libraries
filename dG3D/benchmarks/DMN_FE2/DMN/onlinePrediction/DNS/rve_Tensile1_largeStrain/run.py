from gmshpy import *
from dG3Dpy import*
from math import*


# material law
lawnum1 = 11
# material law
E1 = 3.2E3
nu1 = 0.3
rho1 = 2.7e-9 # Bulk mass
sy01   = 100.
h1    = E1/50.
# creation of material law
law1 = J2LinearDG3DMaterialLaw(lawnum1,rho1,E1,nu1,sy01, h1)
law1.setStrainOrder(11)

lawnum2 = 12 # unique number of law
E2 = E1*10.
nu2 = 0.2
appOrder = 11 # approximation order for exp and log operator of a tensor
potential2 = biLogarithmicElasticPotential(lawnum2,E2,nu2,appOrder)
law2 = dG3DHyperelasticMaterialLaw(lawnum2,rho1,potential2)

# geometry
meshfile="rve.msh" # name of mesh file
#
myfield1 = dG3DDomain(1000,11,0,lawnum1,0,2)
myfield2 = dG3DDomain(1000,12,0,lawnum2,0,2)


# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 150  # number of step (used only if soltype=1)
ftime =1.  # Final time (used only if soltype=1)
tol=1.e-5  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.addDomain(myfield2)
mysolver.addMaterialLaw(law2)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)

mysolver.snlManageTimeStep(10,5,2.,10)

system = 1   # Displacement elimination =0 Multiplier elimination = 1 Displacement+ multiplier = 2
control = 0     # load control = 0 arc length control euler = 1
mysolver.setSystemType(system)
mysolver.setControlType(control)

mysolver.setMessageView(True)

mysolver.displacementBC('Face',11,2,0.)
mysolver.displacementBC('Face',12,2,0.)

microBC = nonLinearPeriodicBC(10,2)
microBC.setBCPhysical(1,2,3,4)
method =5	# Periodic mesh = 0
degree = 3 # Order used for polynomial interpolation 
addvertex = 0  # Polynomial interpolation by mesh vertex = 0, Polynomial interpolation by virtual vertex 
microBC.setPeriodicBCOptions(method, degree, bool(addvertex))
microBC.setDeformationGradient(2.,1.,1.,2.)
fctxx = piecewiseScalarFunction()
fctyy = piecewiseScalarFunction()
fctxy = piecewiseScalarFunction()
fctyx = piecewiseScalarFunction()

fctxx.put(0,0)
fctxx.put(0.4,0.075)
fctxx.put(0.5,0.02)
fctxx.put(0.7,0.15)
fctxx.put(1,0.)

fctyy.put(0,0)
fctyy.put(1,0.)

fctxy.put(0,0)
fctxy.put(1,0.)
fctyx.put(0,0)
fctyx.put(1,0.)

microBC.setPathFunctionDeformationGradient(0,0,fctxx)
microBC.setPathFunctionDeformationGradient(1,1,fctyy)
microBC.setPathFunctionDeformationGradient(1,0,fctyx)
microBC.setPathFunctionDeformationGradient(0,1,fctxy)
  
#
mysolver.addMicroBC(microBC)

mysolver.stressAveragingFlag(True) # set stress averaging ON- 0 , OFF-1 
mysolver.setStressAveragingMethod(0) # 0 -volume 1- surface

#tangent averaging flag
mysolver.tangentAveragingFlag(False) # set tangent averaging ON -0, OFF -1 #** If turned off, Elast Tensor will not be computed
mysolver.setTangentAveragingMethod(2,1e-6) # 0- perturbation 1- condensation

mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)

# solve
#mysolver.initMicroSolver()
mysolver.solve()


