import numpy as np
import matplotlib.pyplot as plt
import pandas as pd


plt.close('all')
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)

plt.figure(1)

eps = pd.read_csv('E_0_GP_0_strain.csv',sep=';')
sig = pd.read_csv('E_0_GP_0_stress.csv',sep=';')
plt.plot(eps['Comp.00'],sig['Comp.01'],'r-',linewidth=2,label='DNS, sig01')
plt.plot(eps['Comp.00'],sig['Comp.00'],'g--',linewidth=2,label='DNS, sig00')
plt.plot(eps['Comp.00'],sig['Comp.11'],'b:',linewidth=2,label='DNS, sig11')


plt.legend(fontsize = 14, loc='upper left')
plt.ylabel('sig',fontsize=14, )
plt.xlabel('eps',fontsize=14)
plt.savefig('stress strain.pdf')
