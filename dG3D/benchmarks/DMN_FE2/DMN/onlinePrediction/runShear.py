from gmshpy import *
from dG3Dpy import*
import matplotlib.pyplot as plt
import pandas as pd
import os

def onlinePrediction(DMNFileName, treeFormat):
  # material law
  lawnum1 = 11
  # material law
  E1 = 3.2E3
  nu1 = 0.3
  rho1 = 2.7e-9 # Bulk mass
  sy01   = 100.
  h1    = E1/50.
  # creation of material law
  law1 = J2LinearDG3DMaterialLaw(lawnum1,rho1,E1,nu1,sy01, h1)
  law1.setStrainOrder(11)

  lawnum2 = 12 # unique number of law
  E2 = E1*10.
  nu2 = 0.2
  appOrder = 11 # approximation order for exp and log operator of a tensor
  potential2 = biLogarithmicElasticPotential(lawnum2,E2,nu2,appOrder)
  law2 = dG3DHyperelasticMaterialLaw(lawnum2,rho1,potential2)

  if treeFormat:
    tree = Tree()
    tree.loadDataFromFile(DMNFileName)
    tree.printTree()
    dmn = dG3DDeepMaterialNetwork(tree)
  else:
    dmn = dG3DDeepMaterialNetwork(DMNFileName)

  dmn.addLaw(0,law1)
  dmn.addLaw(1,law2)
  dmn.fixDofsByComp(2)
  dmn.viewIPAverage(IPField.F_XX)
  dmn.viewIPAverage(IPField.F_XY)
  dmn.viewIPAverage(IPField.F_YX)
  dmn.viewIPAverage(IPField.F_YY)
  dmn.viewIPAverage(IPField.P_XX)
  dmn.viewIPAverage(IPField.P_XY)
  dmn.viewIPAverage(IPField.P_YX)
  dmn.viewIPAverage(IPField.P_YY)
  dmn.viewIPAverage(IPField.P_ZZ)
  #init solver
  dmn.initSolve()

  eps00 = []
  sig00 = []

  eps11 = []
  sig11 = []

  eps22 = []
  sig22 = []

  eps01 = []
  sig01 = []


  epsMat = fullMatrixDouble(3,3)
  epsMat.set(0,0,1.)
  epsMat.set(1,1,1.)
  epsMat.set(2,2,1.)

  deps = 5e-4
  maxFailed = 100
  failed = 0
  unload = 0

  for i in range(0,1000):
    neweps01 = epsMat(0,1)+deps
    #neweps10 = epsMat(1,0)+deps
    #neweps00 = epsMat(0,0)+deps*0.3
    #neweps11 = epsMat(1,1)-deps*0.3
    #epsMat.set(0,0,neweps00)
    epsMat.set(0,1,neweps01)
    #epsMat.set(1,0,neweps10)
    #epsMat.set(1,1,neweps11)

    if unload==0 and neweps01 >0.05:
      deps = -deps
      unload = 1

    if unload==1 and neweps01 < 0.02:
      deps = -deps
      unload = 2

    if unload==2 and neweps01 >0.1:
      deps = -deps
      unload = 4

    if unload == 4 and neweps01 < 0.:
      break

    sigMat = fullMatrixDouble(3,3)
    ok = dmn.solve(epsMat,sigMat,10)
    dmn.saveIPDataToFile(i+1)
    if ok:
      eps00.append(epsMat(0,0))
      sig00.append(sigMat(0,0))

      eps11.append(epsMat(1,1))
      sig11.append(sigMat(1,1))

      eps22.append(epsMat(2,2))
      sig22.append(sigMat(2,2))

      eps01.append(epsMat(0,1))
      sig01.append(sigMat(0,1))

    else:
      neweps01 = epsMat(0,1)-deps
      #neweps10 = epsMat(1,0)-deps
      #neweps00 = epsMat(0,0)-deps*0.3
      #neweps11 = epsMat(1,1)+deps*0.3
      #epsMat.set(0,0,neweps00)
      epsMat.set(0,1,neweps01)
      #epsMat.set(1,1,neweps11)
      deps *= 0.5
      failed += 1
      if failed > maxFailed:
        break

  p = pd.DataFrame()
  p['eps01'] = eps01
  p['sig00'] = sig00
  p['sig01'] = sig01
  p['sig11'] = sig11
  p.to_csv("stressStrain.csv",sep=';',index = False)


plt.close('all')

plt.figure(1)
eps = pd.read_csv('DNS/rve_Shear1_largeStrain/E_0_GP_0_strain.csv',sep=';')
sig = pd.read_csv('DNS/rve_Shear1_largeStrain/E_0_GP_0_stress.csv',sep=';')
plt.plot(eps['Comp.01'],sig['Comp.01'],'ro',label='DNS sig01')
plt.plot(eps['Comp.01'],sig['Comp.00'],'ro',label='DNS sig00')
plt.plot(eps['Comp.01'],sig['Comp.11'],'ro',label='DNS sig11')

colors= ['r', 'g', 'b', 'c', 'm', 'y', 'k', 'w']
allDirs = ["adaptiveSGD-laminateTraining-old","Adam-laminateTraining","Adam-linearElasticTraining","SGD-laminateTraining"]
for fol in allDirs:
  folder = 'Shear-'+fol
  os.system('rm -rf '+folder)
  os.system('mkdir '+folder)
  os.chdir(folder)
  if fol == "Adam-linearElasticTraining":
    onlinePrediction('../../offlineTraining/'+fol+'/best_tree_trained.txt',False)
  else:
    onlinePrediction('../../offlineTraining/'+fol+'/best_tree_trained.txt',True)
  os.chdir("..")
  #plot
  da = pd.read_csv(folder+'/stressStrain.csv',sep=';')
  plt.plot(da['eps01'],da['sig01'],colors[allDirs.index(fol)]+'-',label=fol+' sig01')
  plt.plot(da['eps01'],da['sig00'],colors[allDirs.index(fol)]+'--',label=fol+' sig00')
  plt.plot(da['eps01'],da['sig11'],colors[allDirs.index(fol)]+'-.',label=fol+' sig11')


plt.legend(fontsize = 8, ncol=1, loc='upper left',bbox_to_anchor=(0.5, 0.5))
plt.ylabel('sig',fontsize=16, )
plt.xlabel('eps',fontsize=16)

plt.savefig('Shear-comparison.eps')
plt.savefig('Shear-comparison.pdf')

plt.show()
