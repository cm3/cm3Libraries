from gmshpy import *
from dG3Dpy import*
from math import*
import os
import time
import numpy as np
import pandas as pd

A = getTotalVolume()
A.loadModel('rve.msh')

vt = A.totalVolume()
v1 = A.volumePhysical(2,11)
v2 = A.volumePhysical(2,12)
print('phase I = %.5f phase II = %.5f'%(v1/vt,v2/vt))

