from gmshpy import *
from dG3Dpy import*
from math import*
import os
import numpy as np
import pandas as pd

def makeTest(C1, C2):
  # material law
  rho = 7850e-9 # Bulk mass
  lawnum1 = 11 # unique number of law
  pot1  = smallStrainLinearElasticPotential(lawnum1,C1)
  law1   = dG3DHyperelasticMaterialLaw(lawnum1,rho,pot1)

  lawnum2 = 12 # unique number of law
  pot2  = smallStrainLinearElasticPotential(lawnum2,C2)
  law2   = dG3DHyperelasticMaterialLaw(lawnum2,rho,pot2)

  # geometry
  meshfile="rve.msh" # name of mesh file
  #
  myfield1 = dG3DDomain(1000,11,0,lawnum1,0,2)
  myfield2 = dG3DDomain(1000,12,0,lawnum2,0,2)

  # solver
  sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
  soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
  nstep = 1  # number of step (used only if soltype=1)
  ftime =1.  # Final time (used only if soltype=1)
  tol=1.e-5  # relative tolerance for NR scheme (used only if soltype=1)
  nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)

  # creation of Solver
  mysolver = nonLinearMechSolver(1000)
  mysolver.loadModel(meshfile)
  mysolver.addDomain(myfield1)
  mysolver.addMaterialLaw(law1)
  mysolver.addDomain(myfield2)
  mysolver.addMaterialLaw(law2)
  mysolver.Scheme(soltype)
  mysolver.Solver(sol)
  mysolver.snlData(nstep,ftime,tol)
  mysolver.stepBetweenArchiving(nstepArch)
  
  mysolver.snlManageTimeStep(10,5,2.,10)

  system = 1   # Displacement elimination =0 Multiplier elimination = 1 Displacement+ multiplier = 2
  control = 0     # load control = 0 arc length control euler = 1
  mysolver.setSystemType(system)
  mysolver.setControlType(control)

  mysolver.setMessageView(True)

  mysolver.displacementBC('Face',11,2,0.)
  mysolver.displacementBC('Face',12,2,0.)

  microBC = nonLinearPeriodicBC(10,2)
  microBC.setBCPhysical(1,2,3,4)
  method =5	# Periodic mesh = 0
  degree = 3 # Order used for polynomial interpolation 
  addvertex = 0  # Polynomial interpolation by mesh vertex = 0, Polynomial interpolation by virtual vertex 
  microBC.setPeriodicBCOptions(method, degree, bool(addvertex))
  #microBC.setDeformationGradient(1.,0.1,0.1,1.)
  #
  mysolver.addMicroBC(microBC)
  
  mysolver.stressAveragingFlag(True) # set stress averaging ON- 0 , OFF-1 
  mysolver.setStressAveragingMethod(0) # 0 -volume 1- surface
  
  #tangent averaging flag
  mysolver.tangentAveragingFlag(True) # set tangent averaging ON -0, OFF -1 #** If turned off, Elast Tensor will not be computed
  mysolver.setTangentAveragingMethod(2,1e-6) # 0- perturbation 1- condensation
    
  # solve
  mysolver.initMicroSolver()
  #mysolver.solve()
  
  L = fullMatrixDouble()
  mysolver.getHomogenizedTangent(L)
  
  names = list()
  for row in range(L.size1()):
    for col in range(L.size2()):
      names.append(mysolver.getHomogenizedTangentCompNameInMatrixForm(row,col))
  
  os.system('rm -rf E_0_GP_0_*')
  return (L, names)
  
def lhssample(n,p):
  x = np.random.uniform(size=[n,p])
  for i in range(0,p):
      x[:,i] = (np.argsort(x[:,i])+0.5)/n
  return x

def matToList(A):
  v = fullVectorDouble()
  matToVec(A,v)
  N = v.size()
  return [v(i) for i in range(0,N)]
  
def getStr():
  
  return ['C'+str(i)+str(j)+str(k)+str(l) for i in range(0,3) for j in range(0,3) for j in range(0,6) for j in range(0,6)]

C1 = list()
C2 = list()
Chomo = list()  

maxS = 300
allPt = lhssample(maxS,7)
print(allPt)
ppp = pd.DataFrame(allPt,columns=['v'+str(i) for i in range(7)])
ppp.to_csv("allPt.csv",sep=';',index = False)

L1 = fullMatrixDouble()
L2 = fullMatrixDouble()

abc = list()
for numS in range(maxS):
  Cxx1= 1e3
  Cyy1 = (10.**(2.*(allPt[numS][0])-1.))*Cxx1
  Cxy1 = (0.9*allPt[numS][1])*((Cxx1*Cyy1)**0.5)
  Gxy1 = (10.**(2.*(allPt[numS][2])-1.))*((Cxx1*Cyy1)**0.5)
  
  Cxx2 = (10.**(2.*(allPt[numS][3])-1.))*Cxx1
  Cyy2 = (10.**(2.*(allPt[numS][4])-1.))*Cxx2
  Cxy2 = (0.9*allPt[numS][5])*((Cxx2*Cyy2)**0.5)
  Gxy2 = (10.**(2.*(allPt[numS][6])-1.))*((Cxx2*Cyy2)**0.5)
  
  print([Cxx1,Cyy1,Gxy1,Cxy1,Cxx2,Cyy2,Gxy2,Cxy2])
  abc.append([Cxx1,Cyy1,Gxy1,Cxy1,Cxx2,Cyy2,Gxy2,Cxy2])
 
  planeStrainElasticTensor(Cxx1,Cyy1,Gxy1,Cxy1,L1)
  planeStrainElasticTensor(Cxx2,Cyy2,Gxy2,Cxy2,L2)
  L, names = makeTest(L1,L2)
  C1.append(matToList(L1))
  C2.append(matToList(L2))
  Chomo.append(matToList(L))    
  if numS%30==0:
    pabc = pd.DataFrame(abc,columns=['Cxx1','Cyy1','Gxy1','Cxy1','Cxx2','Cyy2','Gxy2','Cxy2'])
    p1 = pd.DataFrame(C1,columns=names)
    p2 = pd.DataFrame(C2,columns=names)
    phomo = pd.DataFrame(Chomo,columns=names)
    #tofile
    pabc.to_csv("alphabetagamma.csv",sep=';',index = False)
    p1.to_csv("C1.csv",sep=';',index = False)
    p2.to_csv("C2.csv",sep=';',index = False)
    phomo.to_csv("Chomo.csv",sep=';',index = False)
pabc = pd.DataFrame(abc,columns=['Cxx1','Cyy1','Gxy1','Cxy1','Cxx2','Cyy2','Gxy2','Cxy2'])
p1 = pd.DataFrame(C1,columns=names)
p2 = pd.DataFrame(C2,columns=names)
phomo = pd.DataFrame(Chomo,columns=names)
#tofile
pabc.to_csv("alphabetagamma.csv",sep=';',index = False)
p1.to_csv("C1.csv",sep=';',index = False)
p2.to_csv("C2.csv",sep=';',index = False)
phomo.to_csv("Chomo.csv",sep=';',index = False)

