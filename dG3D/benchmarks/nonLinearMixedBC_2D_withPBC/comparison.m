clear all
close all
clc

figure(1);
hold on;

energ = importdata('E_0_GP_0_defoEnergy.csv');

plot(energ.data(:,1),energ.data(:,2),'*')

eps = importdata('E_0_GP_0_strain.csv');
sig = importdata('E_0_GP_0_stress.csv');

S  =size(eps.data);
N = S(1);
homoEner = zeros(N,1);
for i=1:N
   homoEner(i) = 0.;
   for j=2:10
       homoEner(i) = homoEner(i)+eps.data(i,j)*sig.data(i,j)*0.5;
   end
end

plot(energ.data(:,1),homoEner,'s')

legend('<0.5*\sigma_m:\epsilon_m>_V', '0.5* \sigma_M: \epsilon_M',2)