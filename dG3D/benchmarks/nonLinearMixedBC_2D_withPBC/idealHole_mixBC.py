#coding-Utf-8-*-

from gmshpy import *
from dG3Dpy import*

#script to launch PBC problem with a python script

# material law
lawnum = 11 # unique number of law


E = 70E3
nu = 0.3
K = E/3./(1.-2.*nu)	# Bulk mudulus
mu =E/2./(1.+nu)	  # Shear mudulus
rho = 2.7 # Bulk mass
sy0 = 507.e100
h = 200

# creation of material law

law1 = dG3DLinearElasticMaterialLaw(lawnum,rho,E,nu)

# geometry
geofile="idealHole.geo" # name of mesh file
meshfile="idealHole.msh" # name of mesh file


# creation of part Domain
nfield = 11 # number of the field (physical number of entity)
dim =2
myfield1 = dG3DDomain(10,nfield,0,lawnum,0,dim)


# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 200  # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-6  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=20 # Number of step between 2 archiving (used only if soltype=1)
system = 3 # Displacement elimination =0 Multiplier elimination = 1 Displacement+ multiplier = 2
control = 0 # load control = 0 arc length control euler = 1

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,2,2)
#mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.setSystemType(system)
mysolver.setControlType(control)
mysolver.stiffnessModification(bool(1))
mysolver.iterativeProcedure(bool(1))
mysolver.setMessageView(bool(1))


#boundary condition
microBC = nonLinearMixedBC(10,2)
microBC.setOrder(1)
microBC.setBCPhysical(1,2,3,4)

#microBC.setKinematicPhysical(1,0)
#microBC.setKinematicPhysical(1,1)
#microBC.setKinematicPhysical(3,0)
#microBC.setKinematicPhysical(3,1)
#microBC.setKinematicPhysical(2,0)
#microBC.setKinematicPhysical(2,1)
#microBC.setKinematicPhysical(4,0)
#microBC.setKinematicPhysical(4,1)


microBC.setStaticPhysical(1,0)
#microBC.setStaticPhysical(1,1)
microBC.setStaticPhysical(2,0)
microBC.setStaticPhysical(2,1)
microBC.setStaticPhysical(3,0)
microBC.setStaticPhysical(3,1)
#microBC.setStaticPhysical(4,0)
microBC.setStaticPhysical(4,1)

microBC.setPeriodicPhysicals(4,2,0,50.)
microBC.setPeriodicPhysicals(1,3,1,50.)

 # Deformation gradient
microBC.setDeformationGradient(1.01,0.0,0.001,0.999)

mysolver.addMicroBC(microBC)



#stress averaging flag and averaging method 0- VOLUME, 1- SURFACE
mysolver.stressAveragingFlag(bool(1)) # set stress averaging ON- 0 , OFF-1
mysolver.setStressAveragingMethod(0) # 0 -volume 1- surface
#tangent averaging flag
mysolver.tangentAveragingFlag(bool(1)) # set tangent averaging ON -0, OFF -1
mysolver.setTangentAveragingMethod(2,1e-6) # 0- perturbation 1- condensation

# other BC
mysolver.displacementBC("Face",11,2,0.0)


# build view
mysolver.internalPointBuildView("Green-Lagrange_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xz",IPField.STRAIN_XZ, 1, 1);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange equivalent strain",IPField.GL_EQUIVALENT_STRAIN, 1, 1);
mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);

# solve
mysolver.solve()

# test check
check = TestCheck()
check.equal(3.907745e+02,mysolver.getHomogenizedStress(0,0),1.e-5)
check.equal(4.056310e+04,mysolver.getHomogenizedTangent(0,0,0,0),1.e-5)



