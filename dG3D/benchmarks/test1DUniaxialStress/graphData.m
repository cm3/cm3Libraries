clear all;
close all
clc

coor = dlmread('coordinate_PLASTICSTRAIN_OnPhysical_11.csv');
data = dlmread('IPDdata_PLASTICSTRAIN_OnPhysical_11.csv');
[X I] = sort(coor(:,1));

N = size(data,1);

figure(1)
hold on;
for i=1:10:N
    plot(X,data(i,I),'rx')
end

coor = dlmread('coordinate_LOCAL_0_OnPhysical_11.csv');
data = dlmread('IPDdata_LOCAL_0_OnPhysical_11.csv');
[X I] = sort(coor(:,1));

N = size(data,1);
figure(2)
hold on;
for i=1:10:N
    plot(X,data(i,I),'b-')
end

coor = dlmread('coordinate_NONLOCAL_0_OnPhysical_11.csv');
data = dlmread('IPDdata_NONLOCAL_0_OnPhysical_11.csv');
[X I] = sort(coor(:,1));

N = size(data,1);
figure(2)
hold on;
for i=1:10:N
    plot(X,data(i,I),'g--')
end

coor = dlmread('coordinate_DAMAGE_OnPhysical_11.csv');
data = dlmread('IPDdata_DAMAGE_OnPhysical_11.csv');
[X I] = sort(coor(:,1));

N = size(data,1);
figure(3)
hold on;
for i=1:10:N
    plot(X,data(i,I),'g-.')
end

figure(4)
hold on;

L = 5.
u = dlmread('NodalDisplacement2comp0.csv');
f = dlmread('force1comp0.csv');
plot(u(:,2)/L,-f(:,2),'bs-')

F = dlmread('Average_F_XX.csv',';');
P = dlmread('Average_P_XX.csv',';');
plot(F(:,2)-1,P(:,2),'go-')

F = dlmread('Average_F_XX.csv',';');
P = dlmread('Average_P_YY.csv',';');
plot(F(:,2)-1,P(:,2),'rx')

