#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*

#script to launch beam problem with a python script
# material law
lawnum = 11 # unique number of law

E = 2.45E3 #MPa
nu = 0.39
K = E/3./(1.-2.*nu)	# Bulk mudulus
mu =E/2./(1.+nu)	  # Shear mudulus
rho = 7850. # Bulk mass

sy0c = 48.
#  compute solution and BC (given directly to the solver
harden = ExponentialJ2IsotropicHardening(1,sy0c,30.,50.)

cl = IsotropicCLengthLaw(1, 4.)

H = 30.
Dinf = 1
pi = 0.0
alpha = 1
damlaw = SimpleSaturateDamageLaw(1, H, Dinf,pi,alpha)
law1 =  NonLocalDamageJ2HyperDG3DMaterialLaw(lawnum,rho,E,nu,harden,cl,damlaw)
#law1 =  J2LinearDG3DMaterialLaw(lawnum,rho,E,nu,harden)
law1.setStrainOrder(7)


# geometry
meshfile = "line.msh"

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 100   # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-5  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)
fullDg = 0 #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)
beta1  = 100

  
# creation of ElasticField
#matrix
myfield1 = dG3D1DDomain(11,11,space1,lawnum,0,1)
#myfield1.matrixByPerturbation(1,1,1,1e-8)
myfield1.setState(1)
L = 5.
x0 = 0;
A0 = 0.99
xL = L
AL = 1
a = (AL - A0)/(xL-x0)

crossSection = linearScalarFunction(x0,A0,a)
myfield1.setCrossSectionDistribution(crossSection)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)

mysolver.snlManageTimeStep(15,5,2.,20) # maximal 20 times

mysolver.displacementBC("Edge",11,1,0.)
mysolver.displacementBC("Edge",11,2,0.)
mysolver.displacementBC("Node",1,0,0.)

method = 0
withPF = True
mysolver.pathFollowing(withPF,method)
if method == 0:
	mysolver.setPathFollowingIncrementAdaptation(True,4)
	mysolver.setPathFollowingControlType(0)
	mysolver.setPathFollowingCorrectionMethod(0)
	mysolver.setPathFollowingArcLengthStep(1e-1)
	mysolver.setBoundsOfPathFollowingArcLengthSteps(0,0.1);
else:
	# time-step adaptation by number of NR iterations
	mysolver.setPathFollowingIncrementAdaptation(True,4)
	mysolver.setPathFollowingLocalSteps(1e-2,1.)
	mysolver.setPathFollowingSwitchCriterion(0.)
	mysolver.setPathFollowingLocalIncrementType(1); 
	mysolver.setBoundsOfPathFollowingLocalSteps(1.,3.)

if withPF:
	mysolver.forceBC("Node",2,0,1e2)
else:
	mysolver.displacementBC("Node",2,0,7e-2*L)

mysolver.internalPointBuildView("P_xx",IPField.P_XX, 1, 1)
mysolver.internalPointBuildView("P_yy",IPField.P_YY, 1, 1)
mysolver.internalPointBuildView("P_zz",IPField.P_ZZ, 1, 1)
mysolver.internalPointBuildView("P_xy",IPField.P_XY, 1, 1)
mysolver.internalPointBuildView("P_yx",IPField.P_YX, 1, 1)
mysolver.internalPointBuildView("P_yz",IPField.P_YZ, 1, 1)
mysolver.internalPointBuildView("P_zy",IPField.P_ZY, 1, 1)
mysolver.internalPointBuildView("P_xz",IPField.P_XZ, 1, 1)
mysolver.internalPointBuildView("P_zx",IPField.P_ZX, 1, 1)

mysolver.internalPointBuildView("F_xx",IPField.F_XX, 1, 1)
mysolver.internalPointBuildView("F_yy",IPField.F_YY, 1, 1)
mysolver.internalPointBuildView("F_zz",IPField.F_ZZ, 1, 1)
mysolver.internalPointBuildView("F_xy",IPField.F_XY, 1, 1)
mysolver.internalPointBuildView("F_yx",IPField.F_YX, 1, 1)
mysolver.internalPointBuildView("F_yz",IPField.F_YZ, 1, 1)
mysolver.internalPointBuildView("F_zy",IPField.F_ZY, 1, 1)
mysolver.internalPointBuildView("F_xz",IPField.F_XZ, 1, 1)
mysolver.internalPointBuildView("F_zx",IPField.F_ZX, 1, 1)


mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.internalPointBuildView("Damage",IPField.DAMAGE, 1, 1)
mysolver.internalPointBuildView("DEFO_ENERGY",IPField.DEFO_ENERGY, 1, 1)

mysolver.internalPointBuildView("LOCAL0",IPField.LOCAL_0, 1, 1)
mysolver.internalPointBuildView("LOCAL1",IPField.LOCAL_1, 1, 1) 

mysolver.internalPointBuildView("NONLOCAL0",IPField.NONLOCAL_0, 1, 1)
mysolver.internalPointBuildView("NONLOCAL1",IPField.NONLOCAL_1, 1, 1) 

mysolver.internalPointBuildView("DAMAGE0",IPField.DAMAGE_0, 1, 1)
mysolver.internalPointBuildView("DAMAGE1",IPField.DAMAGE_1, 1, 1) 
mysolver.internalPointBuildView("FAILURE_ONSET",IPField.FAILURE_ONSET, 1, 1)
mysolver.internalPointBuildView("FAILURE_PLASTICSTRAIN",IPField.FAILURE_PLASTICSTRAIN, 1, 1)  
mysolver.internalPointBuildView("ACTIVE_DISSIPATION",IPField.ACTIVE_DISSIPATION, 1, 1)  

mysolver.archivingForceOnPhysicalGroup("Node", 1, 0)
mysolver.archivingNodeDisplacement(2,0)

mysolver.archivingAverageValue(IPField.P_YY)
mysolver.archivingAverageValue(IPField.F_YY)
mysolver.archivingAverageValue(IPField.P_XX)
mysolver.archivingAverageValue(IPField.F_XX)

mysolver.archivingIPDataOnPhysical(IPField.LOCAL_0,11,1)
mysolver.archivingIPDataOnPhysical(IPField.LOCAL_1,11,1)
mysolver.archivingIPDataOnPhysical(IPField.NONLOCAL_0,11,1)
mysolver.archivingIPDataOnPhysical(IPField.NONLOCAL_1,11,1)
mysolver.archivingIPDataOnPhysical(IPField.DAMAGE,11,1)
mysolver.archivingIPDataOnPhysical(IPField.PLASTICSTRAIN,11,1)
mysolver.archivingIPDataOnPhysical(IPField.FAILURE_ONSET,11,1)
mysolver.archivingIPDataOnPhysical(IPField.DAMAGE_0,11,1)
mysolver.archivingIPDataOnPhysical(IPField.DAMAGE_1,11,1)

mysolver.solve()

check = TestCheck()
check.equal(-2.626952,mysolver.getArchivedForceOnPhysicalGroup("Node", 1, 0),1.e-4)
