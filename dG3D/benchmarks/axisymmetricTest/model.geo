mm = 1.;

R = 5*mm;
H = 2*R;
L = 4*R;

lsca = 0.2*mm;

p1 = newp; Point(p1) = {0,0,0,lsca};
p2 = newp; Point(p2) = {R,0,0,lsca};
p3 = newp; Point(p3) = {H,0,0,lsca};
p4 = newp; Point(p4) = {H,0,L,lsca};
p5 = newp; Point(p5) = {0,0,L,lsca};
p6 = newp; Point(p6) = {0,0,R,lsca};
p7 = newp; Point(p7) = {R*Sqrt(0.5),0,R*Sqrt(0.5),lsca};

l23 = newl; Line(l23) ={p2,p3};
l34 = newl; Line(l34) ={p3,p4};
l45 = newl; Line(l45) ={p4,p5};
l56 = newl; Line(l56) ={p5,p6};
l67 = newl; Circle(l67)={p6,p1,p7};
l72 = newl; Circle(l72)={p7,p1,p2};
l74 = newl; Line(l74) = {p7,p4};

loop1 =newreg; Line Loop(loop1) = {l72,l23,l34,-l74};
surf1 = newreg; Plane Surface(surf1)={loop1};

loop2 =newreg; Line Loop(loop2) = {l74,l45,l56,l67};
surf2 = newreg; Plane Surface(surf2)={loop2};

Physical Surface(11) = {surf1,surf2};
Physical Line(1) ={l23};
Physical Line(4) ={l56};
Physical Line(3) ={l45};
Physical Point(5)={p5};

//+
Transfinite Line {-l56, l74, l23} = 6 Using Progression 1.2;
//+
Transfinite Line {l72,l34,l45,l67} = 5 Using Progression 1;
//+
Transfinite Surface {surf2} Left;
Transfinite Surface {surf1} Right;

Recombine Surface{surf1, surf2};
//+
Translate {1, 0, 0} {
  Surface{11}; Surface{9}; 
}
//+
Physical Line(2) = {2};
