unit = 1;
L = 6*unit;
H = 10*unit;
t = 5*unit;
lsca = 0.5*unit;
Point(1) = {0,0,0,lsca};
Point(2) = {L-t,0,0,lsca};
Point(3) = {L,0,0,lsca};
Point(4) = {L,t,0,lsca};
Point(5) = {L,H,0,lsca};
Point(6) = {L-t,H,0,lsca};
Point(7) = {L-t,t,0,lsca};
Point(8) = {0,t,0,lsca};
//+
Line(1) = {1, 2};
//+
Line(2) = {2, 3};
//+
Line(3) = {3, 4};
//+
Line(4) = {4, 5};
//+
Line(5) = {5, 6};
//+
Line(6) = {6, 7};
//+
Line(7) = {7, 8};
//+
Line(8) = {8, 1};
//+
Line(9) = {7, 4};
//+
Line(10) = {7, 2};
//+
Curve Loop(1) = {7, 8, 1, -10};
//+
Plane Surface(1) = {1};
//+
Curve Loop(2) = {10, 2, 3, -9};
//+
Plane Surface(2) = {2};
//+
Curve Loop(3) = {9, 4, 5, 6};
//+
Plane Surface(3) = {3};
//+
Physical Surface(11) = {1, 2, 3};
//+
Physical Curve(1) = {1, 2};
//+
Physical Curve(2) = {3, 4};
//+
Physical Curve(3) = {5};
//+
Physical Curve(4) = {6, 7};
//+
Physical Curve(5) = {8};
//+
//+
Transfinite Surface {1};
//+
Transfinite Surface {2};
//+
Transfinite Surface {3};
//+
//Recombine Surface {1, 2, 3};
//+
Physical Point(12) = {4};
//+
Physical Point(13) = {5};
