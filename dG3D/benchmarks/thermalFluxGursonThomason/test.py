#coding-Utf-8-*-
from gmshpy import*
from dG3Dpy import*
from math import*

lawnum1 = 11

rho = 7.6e-9 # ton/mm3 
young = 210.e3 # MPa
nu    = 0.3	
sy0   = 354. # MPa
H = young/100. # MPa


Cv = 450e6 # mJ/(ton*K)
Cp = rho*Cv

alphaThermal = 12e-6; #1/K
Kthermal = 50. # mW/(mm*K)
TaylorQuineyFactor = 0.9

Tinitial = 298.

# matrix hardening
harden1 = LinearExponentialJ2IsotropicHardening(lawnum1,sy0,H,0,10)

# nucleation law
# - nucleation
function1 = LinearNucleationFunctionLaw(0, 0.2)
criterion1 = PorosityBasedNucleationCriterionLaw(0, 0.005, 1.)




# Gurson
q1    = 1.5
q2    = 1.
q3    = 1.5 
fVinitial = 1.5e-3

#Thomason
lambda0 = 1. 
kappa = 1.

# nonlocal length
l = 1. # mm
cl1 = IsotropicCLengthLaw(lawnum1, l*l)

law1 = NonLocalPorousCoupledDG3DMaterialLaw(lawnum1, young, nu, rho, q1,q2,q3, fVinitial, lambda0, kappa, harden1, cl1,TaylorQuineyFactor,Kthermal,Cp,alphaThermal,1e-8,False,1e-8)

law1.setOrderForLogExp(9)
law1.setYieldSurfaceExponent(20)
law1.setNucleationLaw(function1, criterion1)
law1.setSubStepping(True,3)
method =2
law1.setNonLocalMethod(method)
if method==2:
	law1.clearCLengthLaw()
	law1.setCLengthLaw(cl1)
	law1.setCLengthLaw(cl1)
	law1.setCLengthLaw(cl1)

law1.setReferenceT(Tinitial)



# geometry
meshfile="test.msh" # name of mesh file

beta1 = 1e2
fullDG = False
dgnl = False
dgTher = False
eqRatio = 1.0e3
eqRatioTher = 1.0e3
fieldSource = True
mecaSource = True

if method==2:
	numNonlocalVar = 3;
else:
	numNonlocalVar=1;

myfield1 = dG3DDomain(11,11,0,lawnum1,fullDG,2,numNonlocalVar,1)
myfield1.stabilityParameters(beta1)
myfield1.setNonLocalStabilityParameters(beta1,dgnl) 		# Adding stability parameters (for DG)

myfield1.setNonLocalEqRatio(eqRatio)
myfield1.setConstitutiveExtraDofDiffusionStabilityParameters(beta1,dgTher)
myfield1.setConstitutiveExtraDofDiffusionEqRatio(eqRatioTher)
myfield1.setConstitutiveExtraDofDiffusionAccountSource(fieldSource, mecaSource)


# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 30   # number of step (used only if soltype=1)
ftime =100.   # Final time (used only if soltype=1)
tol=1.e-8 # relative tolerance for NR scheme (used only if soltype=1)
tolAbs = 1e-12
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)

mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)

mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol,tolAbs)
mysolver.stepBetweenArchiving(nstepArch)
mysolver.options("-ksp_type preonly -pc_type lu -pc_factor_mat_solver_type mumps")

# boundary conditions

mysolver.initialBC("Face","Position",11,3+numNonlocalVar,Tinitial) # initial temperature
if method==1:
	mysolver.initialBC("Face","Position",11,3,fVinitial) # initial for nonlocal vars
elif method==3:
	mysolver.initialBC("Face","Position",11,3,log(fVinitial))

mysolver.displacementBC("Face",11,2,0.)
mysolver.displacementBC("Edge",1,0,0.)
mysolver.displacementBC("Edge",1,1,0.)
mysolver.displacementBC("Edge",3,1,0.)
mysolver.displacementBC("Edge",5,0,0.)
mysolver.displacementBC("Edge",5,1,0.)

#pressure = 1000. # MPa
#mysolver.pressureOnPhysicalGroupBC("Edge",4, pressure/ftime, 0)

mysolver.displacementBC("Edge",4,3+numNonlocalVar,200/ftime,Tinitial)
mysolver.displacementBC("Edge",5,3+numNonlocalVar,0./ftime,Tinitial)
mysolver.displacementBC("Edge",1,3+numNonlocalVar,0./ftime,Tinitial)

# normal flux
#mysolver.scalarFluxBC("Edge",2,3+numNonlocalVar,-1e4/ftime)

# convection BC
fctTime = simpleFunctionTimeWithConstantDouble(1.,False) # function is not time-dependent
Hflux = -1.e3 # mw/(mm2*K)
fctTemp = linearScalarFunction(Tinitial,0.,Hflux) # Hflux*(T-Tinitial)
fctTime.setDependenceFunction(fctTemp)
fctTime.setDependenceComponents(3+numNonlocalVar)
mysolver.scalarFluxBC("Edge",2,3+numNonlocalVar,fctTime)

mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("damage",IPField.DAMAGE,1,1)
mysolver.internalPointBuildView("local_fV",IPField.LOCAL_POROSITY,1,1)
mysolver.internalPointBuildView("local_fV_max",IPField.LOCAL_POROSITY,1,IPField.MAX_VALUE)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.internalPointBuildView("triaxiality",IPField.STRESS_TRIAXIALITY, 1, 1)

mysolver.internalPointBuildView("COALESCENCE",IPField.COALESCENCE, 1, 1)
mysolver.internalPointBuildView("chi",IPField.LIGAMENT_RATIO, 1, 1)
mysolver.internalPointBuildView("chi_max",IPField.LIGAMENT_RATIO, 1, IPField.MAX_VALUE)

mysolver.internalPointBuildView("LOCAL_0",IPField.LOCAL_0, 1, 1)
mysolver.internalPointBuildView("NONLOCAL_0",IPField.NONLOCAL_0, 1, 1)
mysolver.internalPointBuildView("LOCAL_1",IPField.LOCAL_1, 1, 1)
mysolver.internalPointBuildView("NONLOCAL_1",IPField.NONLOCAL_1, 1, 1)
mysolver.internalPointBuildView("LOCAL_2",IPField.LOCAL_2, 1, 1)
mysolver.internalPointBuildView("NONLOCAL_2",IPField.NONLOCAL_2, 1, 1)
mysolver.internalPointBuildView("LOCAL_POROSITY",IPField.LOCAL_POROSITY,1,1)
mysolver.internalPointBuildView("CORRECTED_POROSITY",IPField.CORRECTED_POROSITY,1,1)
mysolver.internalPointBuildView("YIELD_POROSITY",IPField.YIELD_POROSITY,1,1)

mysolver.internalPointBuildView("temperature",IPField.TEMPERATURE, 1, 1)
mysolver.internalPointBuildView("qx",IPField.THERMALFLUX_X, 1, 1)
mysolver.internalPointBuildView("qy",IPField.THERMALFLUX_Y, 1, 1)
mysolver.internalPointBuildView("qz",IPField.THERMALFLUX_Z, 1, 1)

mysolver.archivingForceOnPhysicalGroup("Edge", 1, 0)
mysolver.archivingForceOnPhysicalGroup("Edge", 1, 1)

mysolver.archivingNodeDisplacement(12,3+numNonlocalVar)
mysolver.archivingNodeDisplacement(13,3+numNonlocalVar)
mysolver.archivingNodeDisplacement(13,0)

mysolver.solve()

check = TestCheck()
check.equal(-2.495474e+02,mysolver.getArchivedForceOnPhysicalGroup("Edge", 1, 0),1.e-6)
