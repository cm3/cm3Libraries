SetFactory("OpenCASCADE");
Geometry.AutoCoherence=0.0001;
//Mesh.SecondOrderLinear = 1;   // second order nodes (as well as nodes generated with subdivision algorithms) simply created by linear interpolation
l1 = 0.50;
l2 = l1*1.8;
l3= l1;
L = 9.97;
R= 8.67/2.0;
// exterior cube
Point(1) = {-L,-L,0,l1};
Point(2) = {L,-L,0,l1};
Point(3) = {L,L,0,l1};
Point(4) = {-L,L,0,l1};

Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,1};
Line Loop(5) = {1,2,3,4};

// holes
ratio=1.005;
r1 = R/ratio;
r2 = R*ratio;
// interior sphere
xo=-L/2;
yo=-L/2;
Point(101) = {xo,yo,0,l2};
Point(102) = {xo+r2,yo,0,l2};
Point(103) = {xo, yo+r1,0,l2};
Point(104) = {xo-r2,yo,0,l3};
Point(105) = {xo,yo-r1,0,l3};
Ellipse(29) = {102,101,103};
Ellipse(30) = {104,101,103};
Ellipse(31) = {104,101,105};
Ellipse(32) = {102,101,105};

// interior sphere
xo=L/2;
yo=L/2;
Point(201) = {xo,yo,0,l2};
Point(202) = {xo+r2,yo,0,l3};
Point(203) = {xo, yo+r1,0,l3};
Point(204) = {xo-r2,yo,0,l2};
Point(205) = {xo,yo-r1,0,l2};
Ellipse(33) = {202,201,203};
Ellipse(34) = {204,201,203};
Ellipse(35) = {204,201,205};
Ellipse(36) = {202,201,205};

/////////////////////
ratio=1.005;
r1 = R*ratio;
r2 = R/ratio;

xo=L/2;
yo=-L/2;
Point(301) = {xo,yo,0,l2};
Point(302) = {xo+r2,yo,0,l3};
Point(303) = {xo, yo+r1,0,l2};
Point(304) = {xo-r2,yo,0,l2};
Point(305) = {xo,yo-r1,0,l3};
Ellipse(37) = {303,301,302};
Ellipse(38) = {303,301,304};
Ellipse(39) = {305,301,304};
Ellipse(40) = {305,301,302};

// interior sphere
xo=-L/2;
yo= L/2;
Point(401) = {xo,yo,0,l2};
Point(402) = {xo+r2,yo,0,l2};
Point(403) = {xo, yo+r1,0,l3};
Point(404) = {xo-r2,yo,0,l3};
Point(405) = {xo,yo-r1,0,l2};
Ellipse(41) = {403,401,402};
Ellipse(42) = {403,401,404};
Ellipse(43) = {405,401,404};
Ellipse(44) = {405,401,402};


Line Loop(101) = {29,-30,31,-32};
Line Loop(102) = {33,-34,35,-36};
Line Loop(103) = {37,-38,39,-40};
Line Loop(104) = {41,-42,43,-44};

Plane Surface(100) ={5,101,102,103,104};

Physical Surface(11) ={100};

Physical Line(1) = {1};
Physical Line(2) = {2};
Physical Line(3) = {3};
Physical Line(4) = {4};

Physical Line(101) = {29,30,31,32};
Physical Line(102) = {33,34,35,36};
Physical Line(103) = {37,38,39,40};
Physical Line(104) = {41,42,43,44};


Physical Point(1) ={4};

//Recombine Surface {100};


