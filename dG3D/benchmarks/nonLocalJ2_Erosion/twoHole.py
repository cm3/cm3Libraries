#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*

#script to launch beam problem with a python script

# material law
lawnum = 1 # unique number of law
rho   = 7850
young = 28.9e9
nu    = 0.3 
sy0   = 150e6
h     = 73e6
hexp  = 60.


# geometry
meshfile="twoHole.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 1000   # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-4   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)
fullDg = 0 #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)
beta1  = 100.


#  compute solution and BC (given directly to the solver
harden = ExponentialJ2IsotropicHardening(1,sy0, h, hexp)
cl     = IsotropicCLengthLaw(1, 5e-6)
damlaw = SimpleSaturateDamageLaw(1, 50.,1.,0.,1.)
law1   = NonLocalDamageJ2HyperDG3DMaterialLaw(lawnum,rho,young,nu,harden,cl,damlaw)

# cohesive law


# creation of ElasticField
nfield = 11 # number of the field (physical number of surface)
myfield1 = dG3DDomain(1000,nfield,space1,lawnum,fullDg,3,1)
#myfield1.matrixByPerturbation(1,1,1,1e-8)
myfield1.stabilityParameters(beta1)
myfield1.setNonLocalStabilityParameters(beta1,True)
myfield1.setNonLocalEqRatio(1.e6)
myfield1.averageStrainBased(True)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)

#element erosion
bulkEro = True
interfaceEro = False
EroMethod = 1 # 1- erosion when fist IP reach failure Cr, 2-all IPs
mysolver.setElementErosion(bulkEro,interfaceEro,EroMethod)
#cr based on critical damage
Type = 1 # 1- maximal value, 2 -minimal value
cr = GeneralCritialCriterion(1,0.9,IPField.DAMAGE,Type)
# set global Cr
mysolver.setGlobalErosionCheck(True,cr)


# BC
mysolver.displacementBC("Volume",11,2,0.)
mysolver.displacementBC("Face",14,0,0.)
mysolver.displacementBC("Face",12,1,0.)
mysolver.displacementBC("Face",13,1,3e-3)


mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.internalPointBuildView("Damage",IPField.DAMAGE, 1, 1)
mysolver.internalPointBuildView("Deleted",IPField.DELETED, 1, 1)

mysolver.archivingForceOnPhysicalGroup("Face", 12, 1)
mysolver.archivingNodeDisplacement(19,1,1)

mysolver.solve()

check = TestCheck()
check.equal(-2.885220e+03,mysolver.getArchivedForceOnPhysicalGroup("Face", 12, 1),1.e-4)


