#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*
#from dG3DpyDebug import*

#script to launch composite problem with a python script

# material law
lawnonlocal = 1 # unique number of law
rho = 7850. # Bulk mass

# geometry
geofile="RVE.geo"
meshfile="RVE.msh" # name of mesh file
propertiesLC="propertiesIncSec2nd.i01" #properties file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 2000   # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-5   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=10 # Number of step between 2 archiving (used only if soltype=1)
fulldg = 0
eqRatio = 1.e6

#  compute solution and BC (given directly to the solver
# creation of law
law1 = NonLocalDamageDG3DMaterialLaw(lawnonlocal,rho,propertiesLC)

# creation of ElasticField
nfield = 100 # number of the field (physical number of surface)
space1 = 0 # function space (Lagrange=6
myfield1 = dG3DDomain(1000,nfield,space1,lawnonlocal,fulldg,3)
#myfield1.setNonLocalStabilityParameters(beta1,True)
myfield1.setNonLocalEqRatio(eqRatio)
#myfield1.matrixByPerturbation(1,1,1,1e-8)
# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,3,2)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
#mysolver.lineSearch(bool(1))
mysolver.snlManageTimeStep(500, 3, 2, 10)
mysolver.stepBetweenArchiving(nstepArch)
# BC
#shearing
#mysolver.displacementBC("Face",1234,0,0.)
#mysolver.displacementBC("Face",1234,1,0.)
#mysolver.displacementBC("Face",1234,2,0.)
#mysolver.displacementBC("Face",5678,1,0.00)
#mysolver.displacementBC("Face",5678,0,0.00001)
#mysolver.forceBC("Face",2376,2,100000000)
#mysolver.forceBC("Face",4158,2,-100000000)
#mysolver.forceBC("Face",5678,0,100000000)
#tension along z
#mysolver.displacementBC("Face",1234,2,0.)
#mysolver.displacementBC("Face",5678,2,0.0001)
#mysolver.displacementBC("Face",4158,0,0.)
#mysolver.displacementBC("Face",1265,1,0.)
#tension along x
L=0.001
cyclicFunctionRed11=cycleFunctionTime(0.,0.,ftime/4, 0.05*L, ftime/2, 0.1*L, 3*ftime/4,0.15*L, ftime, 0.2*L); 
cyclicFunctionRed22=cycleFunctionTime(0.,0.,ftime/4, 0.05*L, ftime/2, 0.05*L, 3*ftime/4,0.*L, ftime, 0.*L); 
cyclicFunctionGreen11=cycleFunctionTime(0.,0.,ftime/2, 0.12*L, ftime, 0.24*L); 
cyclicFunctionGreen22=cycleFunctionTime(0.,0.,ftime/2, 0.08*L, ftime, 0.*L); 
cyclicFunctionBlue11=cycleFunctionTime(0.,0.,ftime/2, 0.08*L, ftime, 0.16*L); 
cyclicFunctionBlue22=cycleFunctionTime(0.,0.,ftime/2, 0.12*L, ftime, 0.06*L); 


mysolver.displacementBC("Face",1234,2,0.)
mysolver.constraintBC("Face",5678,2)
mysolver.displacementBC("Face",1265,1,0.)
mysolver.displacementBC("Face",4158,0,0.)
mysolver.displacementBC("Face",3487,1,cyclicFunctionRed22)
mysolver.displacementBC("Face",2376,0,cyclicFunctionRed11)

mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("damage",IPField.DAMAGE, 1, 1)
mysolver.internalPointBuildView("eps_xx",IPField.STRAIN_XX, 1, 1)
mysolver.internalPointBuildView("eps_yy",IPField.STRAIN_YY, 1, 1)
mysolver.internalPointBuildView("eps_zz",IPField.STRAIN_ZZ, 1, 1)
mysolver.internalPointBuildView("eps_xy",IPField.STRAIN_XY, 1, 1)
mysolver.internalPointBuildView("eps_yz",IPField.STRAIN_YZ, 1, 1)
mysolver.internalPointBuildView("eps_xz",IPField.STRAIN_XZ, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.internalPointBuildView("mtx_svm",IPField.MTX_SVM, 1, 1)
mysolver.internalPointBuildView("mtx_sig_xx",IPField.MTX_SIG_XX, 1, 1)
mysolver.internalPointBuildView("mtx_sig_yy",IPField.MTX_SIG_YY, 1, 1)
mysolver.internalPointBuildView("mtx_sig_zz",IPField.MTX_SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("mtx_sig_xy",IPField.MTX_SIG_XY, 1, 1)
mysolver.internalPointBuildView("mtx_sig_yz",IPField.MTX_SIG_YZ, 1, 1)
mysolver.internalPointBuildView("mtx_sig_xz",IPField.MTX_SIG_XZ, 1, 1)
mysolver.internalPointBuildView("inc_svm",IPField.INC_SVM, 1, 1)
mysolver.internalPointBuildView("inc_sig_xx",IPField.INC_SIG_XX, 1, 1)
mysolver.internalPointBuildView("inc_sig_yy",IPField.INC_SIG_YY, 1, 1)
mysolver.internalPointBuildView("inc_sig_zz",IPField.INC_SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("inc_sig_xy",IPField.INC_SIG_XY, 1, 1)
mysolver.internalPointBuildView("inc_sig_yz",IPField.INC_SIG_YZ, 1, 1)
mysolver.internalPointBuildView("inc_sig_xz",IPField.INC_SIG_XZ, 1, 1)
mysolver.archivingForceOnPhysicalGroup("Face", 3487, 1, nstepArch)
mysolver.archivingForceOnPhysicalGroup("Face", 2376, 0, nstepArch)
mysolver.archivingNodeDisplacement(1,0,nstepArch)
mysolver.archivingNodeDisplacement(2,0,nstepArch)
mysolver.archivingNodeDisplacement(1,1,nstepArch)
mysolver.archivingNodeDisplacement(2,1,nstepArch)
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.SVM,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.SIG_XX,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.SIG_YY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.SIG_ZZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.SIG_XY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.SIG_XZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.SIG_YZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.STRAIN_XX,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.STRAIN_YY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.PLASTICSTRAIN,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.DAMAGE,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.MTX_SVM,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.MTX_SIG_XX,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.MTX_SIG_YY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.MTX_SIG_ZZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.MTX_SIG_XY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.MTX_SIG_XZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.MTX_SIG_YZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.INC_SVM,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.INC_SIG_XX,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.INC_SIG_YY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.INC_SIG_ZZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.INC_SIG_XY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.INC_SIG_XZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.INC_SIG_YZ,IPField.MEAN_VALUE, nstepArch);



mysolver.solve()

check = TestCheck()
check.equal(8.030582e+01,mysolver.getArchivedForceOnPhysicalGroup("Face", 2376, 0),1.e-6)
check.equal(9.582181e+00,mysolver.getArchivedForceOnPhysicalGroup("Face", 3487, 1),1.e-6)

