# test file

set(PYFILE twoHole.py)

set(FILES2DELETE 
  *.csv
  disp*
  stress*
  twoHole.msh
)

add_cm3python_test(${PYFILE} "${FILES2DELETE}")
