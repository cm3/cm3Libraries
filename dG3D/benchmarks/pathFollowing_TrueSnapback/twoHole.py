#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*

#script to launch PBC problem with a python script

# material law
lawnum = 11 # unique number of law
E = 70E3 #Young modulus
nu = 0.3 #Poisson ratio
K = E/3./(1.-2.*nu)
mu = E/2./(1+nu)
sy0 = 300. #Yield stress
h = E/10. # hardening modulus
rho = 1.  #density
# geometry

geofile="twoHole.geo"
meshfile="twoHole.msh" # name of mesh file

# creation of material law
harden = LinearExponentialJ2IsotropicHardening(1, sy0, h, 0., 10.)
cl     = IsotropicCLengthLaw(1, 1e-1)
damlaw = SimpleSaturateDamageLaw(1, 50.,1.,0.,1.)
law1   = NonLocalDamageJ2HyperDG3DMaterialLaw(lawnum,rho,E,nu,harden,cl,damlaw)
law1.setUseBarF(True)


# creation of part Domain
nfield = 11 # number of the field (physical number of entity)
dim =3
beta1 = 50;
fdg=0
myfield1 = dG3DDomain(1000,nfield,0,lawnum,fdg,dim,1)
myfield1.stabilityParameters(beta1)
#myfield1.setNonLocalStabilityParameters(beta1,True)
myfield1.setNonLocalEqRatio(1.e3)
# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 30  # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-5 # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,3,1)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)

mysolver.pathFollowing(True,1)
# time-step adaptation by number of NR iterations
mysolver.setPathFollowingIncrementAdaptation(True,4)
mysolver.setPathFollowingLocalSteps(1e-4,5)
mysolver.setPathFollowingLocalIncrementType(1); 

mysolver.displacementBC("Volume",11,2,0.)

mysolver.displacementBC("Face",12,0,0.)
mysolver.displacementBC("Face",12,1,0.)

mysolver.displacementBC("Face",13,0,0.)
mysolver.constraintBC("Face",13,1)
mysolver.forceBC("Face",13,1,2e2)

# build view
mysolver.internalPointBuildView("Green-Lagrange_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xz",IPField.STRAIN_XZ, 1, 1);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);
mysolver.internalPointBuildView("Damage",IPField.DAMAGE, 1, 1); 
mysolver.internalPointBuildView("irreversible energy",IPField.IRREVERSIBLE_ENERGY, 1, 1); 

mysolver.archivingNodeDisplacement(19,1,1)
mysolver.archivingForceOnPhysicalGroup('Face',12,1)

# solve
mysolver.solve()

check = TestCheck()
check.equal(-1.163966e+03,mysolver.getArchivedForceOnPhysicalGroup("Face", 12, 1),1.e-4)
