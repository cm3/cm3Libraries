import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

plt.figure()

u = pd.read_csv("cube-fortran/NodalDisplacementPhysical43Num2comp0.csv",sep=";",header=None)
f = pd.read_csv("cube-fortran/force30comp0.csv",sep=";",header=None)
plt.plot(u.values[:,1],f.values[:,1],"r-",label="CUR -fortran")

u = pd.read_csv("cube/NodalDisplacementPhysical43Num2comp0.csv",sep=";",header=None)
f = pd.read_csv("cube/force30comp0.csv",sep=";",header=None)
plt.plot(u.values[:,1],f.values[:,1],"g--",label="ref")


plt.xlabel("disp")
plt.ylabel("force")

plt.legend()

plt.show()

