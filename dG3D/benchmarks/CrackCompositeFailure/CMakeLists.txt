# test file

set(PYFILE composite.py)

set(FILES2DELETE 
  disp*.msh  
  energy.csv
  force*.csv
  NodalDisplacement*.csv
	stress*.msh
)

add_cm3python_test(${PYFILE} "${FILES2DELETE}")
