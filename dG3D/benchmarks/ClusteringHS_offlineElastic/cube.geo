// Test case a SCB with a vertical load at its free extremity
// Size

x=1.;
y=1.;

// definition of points
Point(1) = { 0.0 , 0.0 , 0.0 };
Point(2) = {  x  , 0.0 , 0.0 };
Point(3) = {  x  ,  y  , 0.0 };
Point(4) = { 0.0 ,  y  , 0.0 };

// Line between points
Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,1};

// Surface definition
Line Loop(1) = {1,2,3,4};

Plane Surface(1) = {1};

// Physical objects to applied BC and material
Physical Surface(1234) = {1};

Physical Line(12) = {1};
Physical Line(23) = {2};
Physical Line(34) = {3};
Physical Line(41) = {4};

Physical Point(1) ={1};
Physical Point(2) ={2};
Physical Point(3) ={3};
Physical Point(4) ={4};

Transfinite Line {1,2,3,4} = 2;
Transfinite Surface {1};
Recombine Surface {1};
