#coding-Utf-8-*-
import numpy as npy

def ElasticConstantsTriclinicFromTangentCSV():
    with open('ElasticStiffnessHomIso.csv','r') as data_tangent:
        B_string=data_tangent.read()

    B_string=B_string.split('\n')
    del B_string[0]
    del B_string[-1]

    k=0
    tmp_string = B_string[k]
    tmp_string = tmp_string.split(' ')
    B=list(map(float,tmp_string))
    print(B)
    
    C_voigt=npy.zeros((6,6));
    i=0
    for index_2 in range(0,6):
        for index_1 in range(0,6):
            C_voigt[index_1,index_2]=B[i]
            i=i+1  

    print(C_voigt);
    C00 = C_voigt[0,0]
    C11 = C_voigt[1,1]
    C22 = C_voigt[2,2]
    C33 = C_voigt[3,3]
    C44 = C_voigt[4,4]
    C55 = C_voigt[5,5]
    
    C01 = C_voigt[0,1]
    C02 = C_voigt[0,2]
    C03 = C_voigt[0,3]
    C04 = C_voigt[0,4]
    C05 = C_voigt[0,5]
       
    C12 = C_voigt[1,2]
    C13 = C_voigt[1,3]
    C14 = C_voigt[1,4]
    C15 = C_voigt[1,5]
        
    C23 = C_voigt[2,3]
    C24 = C_voigt[2,4]
    C25 = C_voigt[2,5]
    
    C34 = C_voigt[3,4]
    C35 = C_voigt[3,5]

    C45 = C_voigt[4,5]
    
    return C00, C11, C22, C33, C44, C55, C01, C02, C03, C04, C05, C12, C13, C14, C15, C23, C24, C25, C34, C35, C45

    
def GetVoigtNotation(C):

    #C is a 4th order tensor.
    #The function returns its voight notation

    C_mat =  npy.zeros((6,6))
    C_mat[0] = [C[0,0,0,0], C[0,0,1,1], C[0,0,2,2], C[0,0,1,2], C[0,0,2,0], C[0,0,0,1]]
    C_mat[1] = [C[1,1,0,0], C[1,1,1,1], C[1,1,2,2], C[1,1,1,2], C[1,1,2,0], C[1,1,0,1]]
    C_mat[2] = [C[2,2,0,0], C[2,2,1,1], C[2,2,2,2], C[2,2,1,2], C[2,2,2,0], C[2,2,0,1]]
    C_mat[3] = [C[1,2,0,0], C[1,2,1,1], C[1,2,2,2], C[1,2,1,2], C[1,2,2,0], C[1,2,0,1]]
    C_mat[4] = [C[2,0,0,0], C[2,0,1,1], C[2,0,2,2], C[2,0,1,2], C[2,0,2,0], C[2,0,0,1]]
    C_mat[5] = [C[0,1,0,0], C[0,1,1,1], C[0,1,2,2], C[0,1,1,2], C[0,1,2,0], C[0,1,0,1]]

    return C_mat

