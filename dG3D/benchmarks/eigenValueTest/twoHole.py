#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*

#script to launch beam problem with a python script


lawnum1 = 1 # unique number of law
rho   = 7850
young = 28.9e9
nu    = 0.3 
law1   = dG3DLinearElasticMaterialLaw(lawnum1,rho,young,nu)
law1.setUseBarF(True)


# geometry
geofile="twoHole.geo"
meshfile="twoHole.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 5 #eigen value


# creation of ElasticField
beta1 = 1e2
fullDG = False;
averageStrainBased = False

myfield1 = dG3DDomain(1000,83,0,lawnum1,fullDG,3)
myfield1.stabilityParameters(beta1)
myfield1.averageStrainBased(averageStrainBased)


# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)

mysolver.Scheme(soltype)
mysolver.Solver(sol)

# BC
#mysolver.displacementBC("Volume",83,2,0.)
mysolver.displacementBC("Face",84,0,0.)
mysolver.displacementBC("Face",84,1,0.)
mysolver.displacementBC("Face",84,2,0.)
mysolver.displacementBC("Face",85,0,0.)
mysolver.displacementBC("Face",85,1,5e-4)
mysolver.displacementBC("Face",85,2,0.)

mysolver.eigenValueSolver(100)
mysolver.setModeView(0)
mysolver.setModeView(1)
mysolver.setModeView(10)
mysolver.setModeView(50)

mysolver.setEigenSolverParamerters(1,200,'krylovschur',1e-3)
mysolver.setDisturbedEigenMode(5,1e-4,True)

mysolver.solve()

check = TestCheck()
import csv
data = csv.reader(open('eigenValues_step1.csv'), delimiter='\t')
variables = list(data)
check.equal(7.047718e+08,float(variables[0][1]),1e-6)
check.equal(2.109249e+09,float(variables[1][1]),1e-6)

