// Plane strain specimen tests with notch


// Geometry parameters
//====================================================
// Units
mm=1.0e-3;

L = 1.*mm;
L_notch = L/2.;
tol = 1.*mm*mm;
LoadingMode = 0;


// Mesh size
//====================================================
sl1 = L/2.;
sl2 = L/100;
sl3 = L/10;



// Points
//====================================================
Point(1)= {0.,0.,0.,sl1};
Point(3)={L,L,0.,sl3};
Point(4)={0.,L,0.,sl1};
Point(5)={0.,L/2.+tol,0.,sl1};
Point(6)={L/2.,L/2.,0.,sl2};
Point(7)={0.,L/2.-tol,0.,sl1};

If (LoadingMode == 0)
	Point(21)= {L*0.6,0.,0.,sl1};
	Point(2)={L,0.,0.,sl3};
	Point(22)= {L,L*0.43,0.,sl2};
	Point(23)= {L,L*0.57,0.,sl2};
Else
	Point(21)= {L*0.8,0.,0.,sl2};
	Point(2)={L,0.,0.,sl2};
	Point(22)= {L,L*0.6,0.,sl2*2.};
	Point(23)= {L,L*0.8,0.,sl1};
EndIf



// Lines
//====================================================
Line(1)={1,21};
Line(21) = {21,2};
Line(2)={2,22};
Line(22)={22,23};
Line(23)={23,3};
Line(3)={3,4};
Line(4)={4,5};
Line(5)={5,6};
Line(6)={6,7};
Line(7)={7,1};



// Surfaces
//====================================================

Line Loop(10) = {1,21,2,22,23,3,4,5,6,7};

Plane Surface(11) = {-10};


// Mesh size
//====================================================

If (LoadingMode == 0)
	Point(30) = {L*0.5,L*0.43,0.,sl2};
	Point(31) = {L*0.5,L*0.57,0.,sl2};
	Point(32) = {L*0.6,L*0.43,0.,sl2};
	Point(33) = {L*0.6,L*0.57,0.,sl2};
	Point(34) = {L*0.7,L*0.43,0.,sl2};
	Point(35) = {L*0.7,L*0.57,0.,sl2};
	Point(36) = {L*0.81,L*0.43,0.,sl2};
	Point(37) = {L*0.79,L*0.57,0.,sl2};
	Point(38) = {L*0.89,L*0.43,0.,sl2};
	Point(39) = {L*0.91,L*0.57,0.,sl2};
	//Point(11) = {L*0.25,L*0.75,0.,sl2};
	Point{30,31,32,33,34,35,36,37,38,39} In Surface{11};


Else
	Point(29) = {L*0.45,L*0.48,0.,sl2};
	
	Point(30) = {L*0.45,L*0.4,0.,sl2};
	Point(31) = {L*0.5,L*0.3,0.,sl2};
	Point(32) = {L*0.6,L*0.2,0.,sl2};
	Point(33) = {L*0.7,L*0.1,0.,sl2};

	Point(34) = {L*0.6,L*0.50,0.,sl2};
	Point(35) = {L*0.7,L*0.35,0.,sl2};
	Point(36) = {L*0.8,L*0.25,0.,sl2};
	Point(37) = {L*0.9,L*0.15,0.,sl2};

	Point(38) = {L*0.5,L*0.52,0.,sl2};

	Point(39) = {L*0.9,L*0.55,0.,sl2*2.};
	//Point(39) = {L*0.85,L*0.5,0.,sl2};
	Point{29,30,31,32,33,34,35,36,37,38,39} In Surface{11};
EndIf




// Physical
//====================================================


Physical Surface(100) = {11};
Physical Line(101) = {1,21};
Physical Line(102) = {2,22};
Physical Line(103) = {3};
Physical Line(104) = {4,7};

Physical Point(111) = {3};
Physical Point(112) = {2};














