#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*
#from dG3DpyDebug import*
#script to launch beam problem with a python script

# material law
lawnum   = 1 # unique number of law
rho      = 7850
properties = "property.i01"



# geometry
geofile="cube.geo" # name of mesh file
meshfile="cube.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1


#program 1
T0=100.   #1 for strain rate 1, 100 for strain rate 2
gammamax=5.e-3
ftime1 =4.*T0  # Final time (used only if soltype=1)
nstep1 = int(ftime1/1.e-3)  # number of step (used only if soltype=1)

#program 2
peak=40.
ftime2=200.
gammacreep=20.e-3
nstep2 = int(ftime2/1.e-4)  # number of step (used only if soltype=1)

#program 3
gamma=1.e-3
omega=3.14159/20.
ftime3 =4.*20.  # Final time (used only if soltype=1)
nstep3 = int(ftime3/1.e-3)  # number of step (used only if soltype=1)


program=3

nstepArch=100 # Number of step between 2 archiving (used only if soltype=1)


tol=1.e-3   # relative tolerance for NR scheme (used only if soltype=1)
fullDg = 0 #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)


#  compute solution and BC (given directly to the solver
# creation of law
#law1 = NonLocalDamageDG3DMaterialLaw(lawnum,rho,properties)
law1 = NonLocalDamageDG3DMaterialLaw(lawnum,rho,properties)
law1.setUseBarF(True)
# creation of ElasticField
nfield = 10 # number of the field (physical number of surface)
myfield1 = dG3DDomain(1000,nfield,space1,lawnum,fullDg,3)
myfield1.stabilityParameters(30.)
#myfield1.matrixByPerturbation(1,1,1,1e-8)
# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,3,1)
#mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.stepBetweenArchiving(nstepArch)
# BC
#shearing
#mysolver.displacementBC("Face",1234,0,0.)
#mysolver.displacementBC("Face",1234,1,0.)
#mysolver.displacementBC("Face",1234,2,0.)
#mysolver.displacementBC("Face",5678,1,0.00)
#mysolver.forceBC("Face",2376,2,10000)
#mysolver.forceBC("Face",4158,2,-10000)
#mysolver.forceBC("Face",5678,0,10000)
#tension along z

if program==1:
  mysolver.snlData(nstep1,ftime1,tol,1e-8)
  mysolver.displacementBC("Face",1234,2,0.)
  mysolver.displacementBC("Face",1265,1,0.)
  mysolver.displacementBC("Face",4158,0,0.)
  program1Fucntion=cycleFunctionTime(0., 0., T0, gammamax, 3.*T0, -1.*gammamax, ftime1, 0.);
  program1Fucntionover2=cycleFunctionTime(0., 0., T0, -0.5*gammamax, 3.*T0, 0.5*gammamax, ftime1, 0.);
  mysolver.displacementBC("Face",5678,2,program1Fucntionover2)
  mysolver.displacementBC("Face",2376,0,program1Fucntion)
  mysolver.displacementBC("Face",3487,1,program1Fucntionover2)

if program==2:
  mysolver.snlData(nstep2,ftime2,tol,1e-8)
  mysolver.displacementBC("Face",1234,2,0.)
  mysolver.displacementBC("Face",1265,1,0.)
  mysolver.displacementBC("Face",4158,0,0.)
  program2Fucntion=cycleFunctionTime(0., 0., peak, gammacreep, ftime2, gammacreep);
  program2Fucntionover2=cycleFunctionTime(0., 0., peak, -0.5*gammacreep, ftime2, -0.5*gammacreep);
  mysolver.displacementBC("Face",5678,2,program2Fucntionover2)
  mysolver.displacementBC("Face",2376,0,program2Fucntion)
  mysolver.displacementBC("Face",3487,1,program2Fucntionover2)

if program==3:
  mysolver.snlData(nstep3,ftime3,tol,1e-8)
  mysolver.displacementBC("Edge",41,2,0.)
  mysolver.displacementBC("Face",1265,1,0.)
  mysolver.displacementBC("Edge",41,0,0.)
  program3Fucntion1=cosinusoidalFunctionTime(gamma*3.**0.5/4.,omega); #*2 as we constrain the shear angle
  program3Fucntion2=sinusoidalFunctionTime(gamma,omega);
  program3Fucntion2over2=sinusoidalFunctionTime(-1.*gamma/2.,omega);
  program3Fucntion1Plus2=cosinusoidalPlusSinusoidalFunctionTime(gamma*3.**0.5/4.,omega,gamma,omega);#*2 as we constrain the shear angle
  program3Fucntion1MinusHalf2=cosinusoidalPlusSinusoidalFunctionTime(gamma*3.**0.5/4.,omega,-0.5*gamma,omega);#*2 as we constrain the shear angle

  mysolver.displacementBC("Edge",85,2,program3Fucntion2over2)
  mysolver.displacementBC("Edge",23,0,program3Fucntion2)
  mysolver.displacementBC("Edge",23,2,program3Fucntion1)
  mysolver.displacementBC("Edge",67,0,program3Fucntion1Plus2)
  mysolver.displacementBC("Edge",67,2,program3Fucntion1MinusHalf2)
  mysolver.displacementBC("Edge",85,0,program3Fucntion1)
  mysolver.displacementBC("Face",3487,1,program3Fucntion2over2)


mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.internalPointBuildView("eps_xx",IPField.STRAIN_XX, 1, 1)
mysolver.internalPointBuildView("eps_yy",IPField.STRAIN_YY, 1, 1)
mysolver.internalPointBuildView("eps_zz",IPField.STRAIN_ZZ, 1, 1)
mysolver.internalPointBuildView("eps_xy",IPField.STRAIN_XY, 1, 1)
mysolver.internalPointBuildView("eps_yz",IPField.STRAIN_YZ, 1, 1)
mysolver.internalPointBuildView("eps_xz",IPField.STRAIN_XZ, 1, 1)
 
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.SIG_XX,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.STRAIN_XX,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.SIG_YY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.STRAIN_YY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.SIG_ZZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.STRAIN_ZZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.SIG_XY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.STRAIN_XY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.SIG_YZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.STRAIN_YZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.SIG_XZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.STRAIN_XZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.MTX_SVM,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.MTX_SIG_XX,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.MTX_SIG_YY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.MTX_SIG_ZZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.MTX_SIG_XY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.MTX_SIG_XZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.MTX_SIG_YZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.INC_SVM,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.INC_SIG_XX,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.INC_SIG_YY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.INC_SIG_ZZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.INC_SIG_XY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.INC_SIG_XZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.INC_SIG_YZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingForceOnPhysicalGroup("Face", 1234, 2,nstepArch)
mysolver.archivingForceOnPhysicalGroup("Face", 1265, 1,nstepArch)
mysolver.archivingForceOnPhysicalGroup("Face", 5678, 0,nstepArch)
mysolver.solve()


check = TestCheck()
check.equal(2.995116e-01,mysolver.getArchivedForceOnPhysicalGroup("Face", 1234, 2),1.e-6)
check.equal(2.995116e-01,mysolver.getArchivedForceOnPhysicalGroup("Face", 1265, 1),1.e-6)
check.equal(-5.598670e-02,mysolver.getArchivedForceOnPhysicalGroup("Face", 5678, 0),1.e-6)


