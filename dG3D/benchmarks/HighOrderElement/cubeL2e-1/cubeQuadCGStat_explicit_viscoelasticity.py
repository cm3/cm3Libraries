#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*

#script to launch beam problem with a python script

# material law
lawnum = 8 # unique number of law
rho   = 1000
#young = 0.0207e6
#nu    = 0.49999862 #K=2.5e9;M=2.5000e+09;c=1.5811e+03;t=4.7434e-08;
#sy0   = 500e8
#h     = 400e8
### need to do: v = 0.3
K = 2.5e9
mu1 = "cube_Viscoelastic_mu_values.txt"
eta1 = "cube_Viscoelastic_eta_values.txt"
size_eta1 = 1

K_EOS = 2.5e9
AVflag = 1;              # 0 for non-artificial viscosity; 1 for artificial viscosity
c1 = 1.1;
cl = 0.4;

# geometry
meshfile="cubeTet_um_L2e-1_ord3.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 2 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 1   # number of step (used only if soltype=1)
ftime =5e-5   # Final time (used only if soltype=1)
tol=1.e-6   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)
fullDg = 0 #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)
beta1 = 20

#  compute solution and BC (given directly to the solver
# creation of law
#law1 = J2LinearDG3DMaterialLaw(lawnum,rho,young,nu,sy0,h)
devLaw1 = ViscoelasticDG3DMaterialLaw(1, rho, K,eta1,size_eta1,mu1)
#devLaw1 = J2LinearDG3DMaterialLaw(1,rho,young,nu,sy0,h)
law1 = EOSDG3DMaterialLaw(AVflag, lawnum, rho, K_EOS, c1, cl, devLaw1) 		# accoustic EOS + AV


# creation of ElasticField
nfield = 10 # number of the field (physical number of surface)
myfield1 = dG3DDomain(1000,nfield,space1,lawnum,fullDg)
myfield1.stabilityParameters(beta1)
#myfield1.matrixByPerturbation(1,1,1,1e-8)
# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)
mysolver.explicitSpectralRadius(ftime,0.3,1.)# 1 means no damping, 0.6 is the time scale factor dynamicRelaxation
mysolver.explicitTimeStepEvaluation(nstep)
# BC
#shearing
#mysolver.displacementBC("Face",1234,0,0.)
#mysolver.displacementBC("Face",1234,1,0.)
#mysolver.displacementBC("Face",1234,2,0.)
#mysolver.displacementBC("Face",5678,1,0.00)
#mysolver.displacementBC("Face",5678,0,0.00001)
#mysolver.forceBC("Face",2376,2,100000000)
#mysolver.forceBC("Face",4158,2,-100000000)
#mysolver.forceBC("Face",5678,0,100000000)
#tension along z
mysolver.displacementBC("Face",1234,2,0.)
#mysolver.displacementBC("Face",5678,2,0.00005)
mysolver.displacementBC("Face",2376,0,0.)
mysolver.displacementBC("Face",1265,1,0.)
mysolver.displacementBC("Face",3487,1,0.)
mysolver.displacementBC("Face",4158,0,0.)
mysolver.displacementBC("Face",5678,0,0.)
mysolver.displacementBC("Face",5678,1,0.)
#mysolver.displacementBC("Volume",10,1,0.)
#mysolver.displacementBC("Volume",10,0,0.)
F=40e6
cyclicFunctionF = cycleFunctionTime(0, 0, 1e-9, -F, 3e-8, -F,1,-F);
mysolver.forceBC("Face",5678,2,cyclicFunctionF);

#mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
#mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
#mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
#mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
#mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
#mysolver.archivingForceOnPhysicalGroup("Face", 1234, 2)
#mysolver.archivingForceOnPhysicalGroup("Face", 5678, 2)

mysolver.solve()

