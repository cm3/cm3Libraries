#coding-Utf-8-*-
from gmshpy import *
#from dG3DpyDebug import*
from dG3Dpy import*

#script to launch composite problem with a python script

# material law
lawnonlocal = 1 # unique number of law
lawlinear = 2 # unique number of law
rho = 7850. # Bulk mass


# geometry
geofile="RVE.geo"
meshfile="RVE.msh" # name of mesh file
propertiesLC="lemaitreChaboche.i01" #properties file
propertiesLin="linear.i01" #properties file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 400   # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-6   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=10 # Number of step between 2 archiving (used only if soltype=1)
fulldg = 1
beta1 = 100.
eqRatio = 1.e6
dim=3
nlVar=1

#  compute solution and BC (given directly to the solver
# creation of law
law1 = NonLocalDamageDG3DMaterialLaw(lawnonlocal,rho,propertiesLC)
#law1.setUseBarF(True)
law2 = NonLocalDamageDG3DMaterialLaw(lawlinear,rho,propertiesLin)
law2.setNumberOfNonLocalVariables(1)
#law2.setUseBarF(True)

# creation of ElasticField
Mfield = 51 # number of the field (physical number of Mtrix)
Ffield = 52 # number of the field (physical number of Fiber)

space1 = 0 # function space (Lagrange=0)

Matixfield = dG3DDomain(1000,Mfield,space1,lawnonlocal,fulldg,dim,nlVar)
Fiberfield = dG3DDomain(1000,Ffield,space1,lawlinear,fulldg,dim,nlVar)
Matixfield.setNonLocalEqRatio(eqRatio)
Matixfield.stabilityParameters(beta1)
Matixfield.setNonLocalStabilityParameters(100.,bool(1))
Fiberfield.stabilityParameters(beta1)
Fiberfield.setNonLocalEqRatio(eqRatio)
Fiberfield.setNonLocalStabilityParameters(100.,bool(1))

#in // we need to create the ininterdomain after adding the domains
myinterfield = interDomainBetween3D(1000,Matixfield,Fiberfield,0) #law = 0
myinterfield.stabilityParameters(beta1)
myinterfield.setNonLocalStabilityParameters(30,bool(1))  #interface is continous
myinterfield.setNonLocalEqRatio(eqRatio)

#myfield1.matrixByPerturbation(1,1e-8)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
#mysolver.createModel(geofile,meshfile,3,1)
mysolver.loadModel(meshfile)
mysolver.addDomain(Matixfield)
mysolver.addDomain(Fiberfield)
mysolver.addDomain(myinterfield)
mysolver.addMaterialLaw(law1)
mysolver.addMaterialLaw(law2)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol,1.e-5)
#mysolver.lineSearch(bool(1))
#mysolver.snlManageTimeStep(16, 3, 2, 10)
mysolver.stepBetweenArchiving(nstepArch)
mysolver.options("petsc_mumps")



# BC
# fixed face
mysolver.displacementBC("Face",101,0,0.)

mysolver.displacementBC("Face",111,1,0.)
#mysolver.constraintBC("Face",112,1)

mysolver.displacementBC("Face",121,2,0.)
mysolver.displacementBC("Face",122,2,0.)


# displacement
d2=0.000011
d3=0.
cyclicFunction1=cycleFunctionTime(0.,0.,ftime/2., d2, ftime, d3); 

#mysolver.displacementBC("Face",6,0,cyclicFunction1)
#mysolver.displacementBC("Face",7,0,cyclicFunction1)
#mysolver.displacementBC("Face",8,0,cyclicFunction1)
#mysolver.displacementBC("Face",9,0,cyclicFunction1)
mysolver.displacementBC("Face",102,0,cyclicFunction1)

#mysolver.displacementBC("Face",102,0,d1)





# needed resultes
mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("damage",IPField.DAMAGE, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.archivingForceOnPhysicalGroup("Face", 101, 0,nstepArch)
mysolver.archivingForceOnPhysicalGroup("Face", 102, 0,nstepArch)
mysolver.archivingForceOnPhysicalGroup("Face", 122, 2,nstepArch)
mysolver.archivingNodeDisplacement(1,0,nstepArch)
mysolver.archivingNodeDisplacement(2,0,nstepArch)
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SVM,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SVM,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SVM,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_XX,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_XX,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_XX,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_YY,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_YY,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_YY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_ZZ,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_ZZ,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_ZZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_XY,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_XY,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_XY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_XZ,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_XZ,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_XZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_YZ,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_YZ,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_YZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.PLASTICSTRAIN,IPField.MIN_VALUE,nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.PLASTICSTRAIN,IPField.MAX_VALUE,nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.PLASTICSTRAIN,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.DAMAGE,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.DAMAGE,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.DAMAGE,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SVM,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SVM,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SVM,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_XX,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_XX,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_XX,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_YY,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_YY,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_YY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_ZZ,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_ZZ,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_ZZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_XY,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_XY,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_XY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_XZ,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_XZ,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_XZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_YZ,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_YZ,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_YZ,IPField.MEAN_VALUE, nstepArch);




mysolver.solve()

try:
  import linecache
  linedisp = linecache.getline('force102comp0_part0.csv',40)
except:
  print('Cannot read the results in the files')
  import os
  os._exit(1)
else:
  check = TestCheck()
  check.equal(-2.108133e-02,float(linedisp.split(';')[1]))



