
// Notched bar plate : geometry file

// ----------- Input data ----------- 
mm = 1.0e-3;		// Units
L = 10.*mm;		// Width
sl1 = 8.0*mm;		// Mesh size (coarsest zone)
sl2 = 3.0*mm;		// (finer zone, near the hole along the perimeter)
sl3 = 0.7*mm;		// (finest zone, along the crack)


// Plate dimensions
Lx = 1.*L;		// Width
Ly = 2.*L; 		// Lenght
r = 0.25*L; 		// Notched distance

// Mode symetry (=0 : unsymmetric ; =1 : symmetric)
dim = 2;



// ----------- Geometry building ----------- 
// Perimeter (points and line created in anti-clockwise order)
Point(1)={0.,	0.,	0.,sl1};
Point(2)={Lx,	0.,	0.,sl1};
Point(3)={Lx,	0.42*Ly,0.,sl2};
Point(4)={Lx,	0.5*Ly,	0.,sl3};
Point(5)={Lx,	0.58*Ly,0.,sl2};
Point(6)={Lx,	Ly,	0.,sl1};
Point(7)={0.,	Ly,	0.,sl1};

Line(1)={1,2};	// Bottom face
Line(2)={2,3};	// Right face
Line(3)={3,4};
Line(4)={4,5};
Line(5)={5,6};
Line(6)={6,7};	// Top face

// Line for left face and notches (points and line created in anti-clockwise order)
x0 = 0.*Lx;
y0 = 0.5*Ly;
Point(8) =  {x0,	y0+r,	0.,sl2};
Point(9) =  {x0+r,	y0,		0.,sl3};
Point(10) = {x0,	y0-r,	0.,sl2};
Point(11) = {x0,	y0,		0.,sl3};
Line(11) ={8,9};
Line(12) ={9,10};


// Closing perimeter
Line(7) = {7,8};
Line(8) = {10,1};

Line Loop(1) = {1,2,3,4,5,6,7,11,12,8};	// Perimeter
Plane Surface(1) = {1};

Physical Surface(51) = {1};
Physical Line(61) = {1};		// Face bottom (y = 0)
Physical Line(62) = {6};		// Face top (y = Ly)
Physical Point(71) = {1};		// Point (x=0,y=0,z=0) to block rigid mode
Physical Point(72) = {2};		// Point (x=Lx,y=0,z=0) to block rigid mode
Physical Point(73) = {7};		// Point (x=0,y=Ly,z=0)
Physical Point(74) = {6};		// Point (x=Lx,y=Ly,z=0) to block rigid mode







