// C++ Interface: partDomain
//
// Description: Interface class for dg 3D
//
// Author:  <L. Noels>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "nonLinearMechSolver.h"
#include "dG3DTerms.h"
#include "dG3DDomain.h"
#include "dG3DFilterDofComponent.h"
#include "IFace.h"
#include "GaussLobatto2DQuadratureRules.h"
#include "GmshMessage.h"
#include "numericalMaterial.h"
#include "IEdge.h"
#include "MInterfaceLine.h"
#include "MInterfacePoint.h"
#include "dG3DMultiscaleMaterialLaw.h"
#include "terms.h"
#include "pathFollowingTerms.h"
#include "dG3DHomogenizedTangentTerms.h"
#include "FractureCohesiveDG3DMaterialLaw.h"
#include "nonLocalDamageDG3DIPVariable.h"
#include "mixedFunctionSpace.h"
#include "dG3DCurlFunctionSpace.h"
#include "dG3DHierarchicalFunctionSpace.h"


dG3DDomain::dG3DDomain (const int tag,const int phys, const int ws, const int lnum, const int fdg, const int dim, int nonLocalVar, int nonConstitutiveExtraDOFVar, int nonCurlData, bool hasBodyForceForHO, int BFModuliType):
  dgPartDomain(tag,phys,ws,fdg,nonLocalVar, nonConstitutiveExtraDOFVar, nonCurlData, hasBodyForceForHO, BFModuliType),
  _mlaw(NULL),
  _lnum(lnum),
  _dim(dim),
  _beta1(10.),
  _sts(1.),
  _evalStiff(false),
  _isHighOrder(false),
  _nonLocalBeta(100.),
  _nonLocalContinuity(true),
  _nonLocalEqRatio(1.),
  _constitutiveExtraDofDiffusionBeta(100.),
  _constitutiveExtraDofDiffusionContinuity(true),
  _constitutiveExtraDofDiffusionEqRatio(100.),
  _constitutiveExtraDofDiffusionUseEnergyConjugatedField(false),
  _constitutiveExtraDofDiffusionAccountFieldSource(true),
  _constitutiveExtraDofDiffusionAccountMecaSource(false),
  _constitutiveCurlEqRatio(100.), // Verify later this number
  _constitutiveCurlAccountFieldSource(true),
  _constitutiveCurlAccountMecaSource(false),
  _incrementNonlocalBased(false),
  _planeStressState(false),
  _space(NULL),
  _gaussorderbulk(-1),
  _gaussorderbound(-1),
  _interQuad(NULL),
  _gqt(Gauss),
  _subSteppingMethod(0),
  _subStepNumber(1),
  _bulkDamageBlockMethod(-1),
  _forceInterfaceElementBroken(false),
  _failureIPRatio(1.),
  _EqMultiFieldFactor(1.),
  _imposeCrackFlag(false),
  _accountPathFollowing(true)
{
  switch(_wsp)
  {
    case functionSpaceType::Lagrange:
      if(_fullDg)
      {
        _space = new dG3DLagrangeFunctionSpace(getTag(),3+getNumNonLocalVariable()+getNumConstitutiveExtraDofDiffusionVariable());
      }
      else
      {
        _space = new g3DLagrangeFunctionSpace(getTag(),3+getNumNonLocalVariable()+getNumConstitutiveExtraDofDiffusionVariable());
      }
      if (getNumConstitutiveCurlVariable()>0)
      {
        nlsFunctionSpace<double>*  sp1 = dynamic_cast<nlsFunctionSpace<double>*>(_space);
        // curl space
        g3DHierarchicalCurlFunctionSpace* sp2 = new g3DHierarchicalCurlFunctionSpace(getTag(),getNumConstitutiveCurlVariable(),1); // order = 1 by default
        sp2->updateCompIndex(sp1->getNumComp());
        _space = new CurlMixedFunctionSpace<double>(sp1,sp2);
      };
      break;
    case functionSpaceType::Hierarchical:
      if(_fullDg)
      {
        _space = new dG3DHierarchicalFunctionSpace(getTag(),1,3+getNumNonLocalVariable()+getNumConstitutiveExtraDofDiffusionVariable());
      }
      else
      {
        _space = new g3DHierarchicalFunctionSpace(getTag(),1,3+getNumNonLocalVariable()+getNumConstitutiveExtraDofDiffusionVariable());
      }
      
      if (getNumConstitutiveCurlVariable()>0)
      {
        nlsFunctionSpace<double>*  sp1 = dynamic_cast<nlsFunctionSpace<double>*>(_space);
        // curl space
        g3DHierarchicalCurlFunctionSpace* sp2 = new g3DHierarchicalCurlFunctionSpace(getTag(),getNumConstitutiveCurlVariable(),1); // order = 1 by default
        sp2->updateCompIndex(sp1->getNumComp());
        _space = new CurlMixedFunctionSpace<double>(sp1,sp2);
      };
      break;
    case functionSpaceType::Inter :
      // do nothing
      break; // InterfaceDomain set function space later
    default:
      Msg::Error("Function space type is unknown on domain %d",getTag());
  }
  evaluateMecaField=true;
  for(unsigned int i = 0; i < nonConstitutiveExtraDOFVar; i++)
    evaluateExtraDofDiffusionField.push_back(true);

  for(unsigned int i = 0; i < nonLocalVar; i++)
    evaluateNonLocalField.push_back(true);

  for(unsigned int i = 0; i < nonCurlData; i++)
    evaluateCurlField.push_back(true);

}

dG3DDomain::dG3DDomain(const dG3DDomain &source) : dgPartDomain(source),
  _mlaw(NULL),
  _lnum(source._lnum),
  _dim(source._dim),
  _beta1(source._beta1),
  _sts(source._sts),
  _evalStiff(false),
  _isHighOrder(source._isHighOrder),
  _nonLocalBeta(source._nonLocalBeta),
  _nonLocalContinuity(source._nonLocalContinuity),
  _nonLocalEqRatio(source._nonLocalEqRatio),
  _constitutiveExtraDofDiffusionBeta(source._constitutiveExtraDofDiffusionBeta),
  _constitutiveExtraDofDiffusionContinuity(source._constitutiveExtraDofDiffusionContinuity),
  _constitutiveExtraDofDiffusionEqRatio(source._constitutiveExtraDofDiffusionEqRatio),
  _constitutiveExtraDofDiffusionUseEnergyConjugatedField(source._constitutiveExtraDofDiffusionUseEnergyConjugatedField),
  _constitutiveExtraDofDiffusionAccountFieldSource(source._constitutiveExtraDofDiffusionAccountFieldSource),
  _constitutiveExtraDofDiffusionAccountMecaSource(source._constitutiveExtraDofDiffusionAccountMecaSource),
  _constitutiveCurlEqRatio(source._constitutiveCurlEqRatio),
  _constitutiveCurlAccountFieldSource(source._constitutiveCurlAccountFieldSource),
  _constitutiveCurlAccountMecaSource(source._constitutiveCurlAccountMecaSource),
  _incrementNonlocalBased(source._incrementNonlocalBased),
  _planeStressState(source._planeStressState),
  _space(NULL),
  _gaussorderbulk(source._gaussorderbulk),
  _gaussorderbound(source._gaussorderbound),
  _interQuad(NULL),
  _gqt(source._gqt),
  _subSteppingMethod(source._subSteppingMethod),
  _subStepNumber(source._subStepNumber),
  _bulkDamageBlockMethod(source._bulkDamageBlockMethod),
  _forceInterfaceElementBroken(source._forceInterfaceElementBroken),
  _failureIPRatio(source._failureIPRatio),
  _EqMultiFieldFactor(source._EqMultiFieldFactor),
  _imposeCrackFlag(source._imposeCrackFlag),
  _accountPathFollowing(source._accountPathFollowing)
{
	switch(_wsp)
  {
    case functionSpaceType::Lagrange:
      if(_fullDg)
      {
        _space = new dG3DLagrangeFunctionSpace(getTag(),3+getNumNonLocalVariable()+getNumConstitutiveExtraDofDiffusionVariable());
      }
      else
      {
        _space = new g3DLagrangeFunctionSpace(getTag(),3+getNumNonLocalVariable()+getNumConstitutiveExtraDofDiffusionVariable());
      }
    
      if (getNumConstitutiveCurlVariable()>0)
      {
        nlsFunctionSpace<double>*  sp1 = dynamic_cast<nlsFunctionSpace<double>*>(_space);
        // curl space
        g3DHierarchicalCurlFunctionSpace* sp2 = new g3DHierarchicalCurlFunctionSpace(getTag(),getNumConstitutiveCurlVariable(),1); // order = 1 by default
        sp2->updateCompIndex(sp1->getNumComp());
        _space = new CurlMixedFunctionSpace<double>(sp1,sp2);
      };
      break;
    case functionSpaceType::Hierarchical:
      if(_fullDg)
      {
        _space = new dG3DHierarchicalFunctionSpace(getTag(),1,3+getNumNonLocalVariable()+getNumConstitutiveExtraDofDiffusionVariable());
      }
      else
      {
        _space = new g3DHierarchicalFunctionSpace(getTag(),1,3+getNumNonLocalVariable()+getNumConstitutiveExtraDofDiffusionVariable());
      }
      
      if (getNumConstitutiveCurlVariable()>0)
      {
        nlsFunctionSpace<double>*  sp1 = dynamic_cast<nlsFunctionSpace<double>*>(_space);
        // curl space
        g3DHierarchicalCurlFunctionSpace* sp2 = new g3DHierarchicalCurlFunctionSpace(getTag(),getNumConstitutiveCurlVariable(),1); // order = 1 by default
        sp2->updateCompIndex(sp1->getNumComp());
        _space = new CurlMixedFunctionSpace<double>(sp1,sp2);
      };
      break;
    case functionSpaceType::Inter :
      // do nothing
      break; // InterfaceDomain set function space later
    default:
      Msg::Error("Function space type is unknown on domain %d",getTag());
  }
  evaluateMecaField=source.evaluateMecaField;
  evaluateExtraDofDiffusionField=source.evaluateExtraDofDiffusionField;
  evaluateNonLocalField=source.evaluateNonLocalField;
  evaluateCurlField=source.evaluateCurlField;
}

dG3DDomain::~dG3DDomain(){
  if (_interQuad) {delete _interQuad; _interQuad = NULL;};
  if (_space) {delete _space; _space = NULL;};
};

void dG3DDomain::setMaterialLaw(const std::map<int,materialLaw*> &maplaw)
{
  if(!setmaterial){
    for(std::map<int,materialLaw*>::const_iterator it=maplaw.begin(); it!=maplaw.end(); ++it){
      if(it->first == _lnum){
        _mlaw = it->second;
        _mlaw->initLaws(maplaw);
        _isHighOrder = _mlaw->isHighOrder();
      }
    }
    setmaterial = true;
  }
}
void dG3DDomain::accountPathFollowing(const bool fl){
  _accountPathFollowing = fl;
  if (fl){
    Msg::Info("Domain %d is considered in path following",getPhysical());
  }
  else{
    Msg::Info("Domain %d is not considered in path following",getPhysical());
  }
};

void dG3DDomain::setIncrementNonlocalBased(const bool fl){
  _incrementNonlocalBased = fl;
  if (_incrementNonlocalBased){
    Msg::Info("nonlocal based on increment is considered in dom %d",getPhysical());
  }
}

void dG3DDomain::setMaterialLawNumber(const int num)
{
  if (num != _lnum)
  {
    Msg::Info("new law %d replaces old law %d",num,_lnum);
    _lnum =num;
    this->setmaterial= false;
  }
};

void dG3DDomain::setHierarchicalOrder(const int comp, const int order)
{
  HierarchicalFunctionSpace* hspace = dynamic_cast<HierarchicalFunctionSpace*>(this->getFunctionSpace());
  if (hspace != NULL)
  {
    if (comp == 0 || comp == 1 || comp==2)
    {
      hspace->setOrder(0,order);
      hspace->setOrder(1,order);
      hspace->setOrder(2,order);
    }
    else
    {
      hspace->setOrder(comp,order);
    }
  }
  else
  {
    Msg::Error("HierarchicalFunctionSpace must be used to apply dG3DDomain::setHierarchicalOrder !!!");
  }
}

void dG3DDomain::usePrimaryShapeFunction(const int comp)
{
  VertexBasedLagrangeFunctionSpace* sp = dynamic_cast<VertexBasedLagrangeFunctionSpace*>(this->getFunctionSpace());
  if (sp != NULL)
  {
    if (comp == 0 || comp==1 || comp ==2)
    {
      Msg::Info("Primary FE approximation is used for displacement field");
      sp->usePrimaryShapeFunction(0);
      sp->usePrimaryShapeFunction(1);
      sp->usePrimaryShapeFunction(2);
    }
    else
    {
      sp->usePrimaryShapeFunction(comp);
      Msg::Info("Primary FE approximation is used for comp %d",comp);      
    }
  }
  else
  {
    Msg::Error("VertexBasedLagrangeFunctionSpace must be used in dG3DDomain::usePrimaryShapeFunction !!!");
  }
}

void dG3DDomain::setPlaneStressState(const bool fl){
	_planeStressState = fl;
	if (_planeStressState){
		Msg::Info("Plane stress problem is considered");
	}
};

void dG3DDomain::matrixByPerturbation(const int iinter, const double eps){
	_interByPert = iinter;
	_eps = eps;
}

void dG3DDomain::stabilityParameters(const double b1)
{
  _beta1 = b1;
  _sts = 1./sqrt(_beta1);
}

void dG3DDomain::setBulkDamageBlockedMethod(const int method){_bulkDamageBlockMethod = method;};
void dG3DDomain::forceCohesiveInsertionAllIPs(const bool flg, const double fratio){_forceInterfaceElementBroken = flg; _failureIPRatio = fratio;};
void dG3DDomain::strainSubstep(const int method, const int numstep){ _subSteppingMethod = method; _subStepNumber = numstep;};

void dG3DDomain::setMultipleFieldEqRatio(const double val){
 _EqMultiFieldFactor = val;
};

void dG3DDomain::setNewIdForComp(const int comp, const int type){
  ThreeDLagrangeFunctionSpace* gspace = dynamic_cast<ThreeDLagrangeFunctionSpace*>(this->getFunctionSpace());
  if (gspace != NULL){
    gspace->setNewIdForComp(comp,type);
  }
};

void dG3DDomain::setImposedCrack(const bool flag){
	_imposeCrackFlag = flag;
};

void dG3DDomain::imposeCrack(const int em, const int ep){
	printf("add crack between %d and %d \n",em,ep);
	_imposedInterfaceCrack.insert(TwoNum(em,ep));
};


const double dG3DDomain::stabilityParameter(const int i) const
{
  switch(i)
  {
   case 1:
    return _beta1;
   default:
    Msg::Error("This domain has only 1 stability parameters !");
    return 0.;
  }
}

void dG3DDomain::matrixByPerturbation(const int ibulk, const int iinter, const int ivirt, const double eps)
{
  this->setBulkMatrixByPerturbation(ibulk,eps);
  iinter == 0 ? _interByPert = false : _interByPert = true;
  ivirt == 0 ? _virtByPert = false : _virtByPert = true;
}

FunctionSpaceBase* dG3DDomain::getSpaceForBC(const nonLinearBoundaryCondition::type bc_type,
                                             const nonLinearBoundaryCondition::location bc_location,
                                             const nonLinearNeumannBC::NeumannBCType neumann_type,
                                             const int dof_comp,
                                             const mixedFunctionSpaceBase::DofType dofType,
                                             const elementGroup *groupBC,const int domcomp) const
{
  if (dofType == mixedFunctionSpaceBase::DOF_STANDARD)
  {
    // when DG is used
    // if BC is applied on a dimension smaller than one of current domain dG3DBoundaryConditionLagrangeFunctionSpace must be use
    // if BC is applied on the same dimension as one of the current domain dG3DLagrangeFunctionSpace must be used
    //
    // when CG is used
    // g3DDirichletBoundaryConditionLagrangeFunctionSpace, g3DNeumannBoundaryConditionLagrangeFunctionSpace should be used
    // as keys are generated based on vertex
    //

    // with PRESSURE, SCALAR_FLUX boundary conditions
    // dof_comp is not used as all Dof must be present, only domcomp is used
    // with others,
    // dof_comp is used, domcomp is not used

    FunctionSpaceBase* spacebc = NULL;
    if(bc_type == nonLinearBoundaryCondition::DIRICHLET)
    {
      switch(_wsp){
       case functionSpaceType::Lagrange:
        if(_fullDg){
          if (getDim() == 3){
            if(bc_location !=nonLinearBoundaryCondition::ON_VOLUME)
              spacebc= new dG3DBoundaryConditionLagrangeFunctionSpace(getTag(),1,dof_comp,groupBC,g,_groupGhostMPI);
            else
              spacebc = new dG3DLagrangeFunctionSpace(getTag(),1,dof_comp);
          }
          else if (getDim() == 2){
            if(bc_location !=nonLinearBoundaryCondition::ON_FACE)
              spacebc= new dG3DBoundaryConditionLagrangeFunctionSpace(getTag(),1,dof_comp,groupBC,g,_groupGhostMPI);
            else
              spacebc = new dG3DLagrangeFunctionSpace(getTag(),1,dof_comp);
          }
        }
        else{
          spacebc = new g3DDirichletBoundaryConditionLagrangeFunctionSpace(getTag(),1,dof_comp);

        }
      break;
      case functionSpaceType::Hierarchical:
        if(_fullDg)
        {
          if (getDim() == 3)
          {
            if(bc_location !=nonLinearBoundaryCondition::ON_VOLUME)
              spacebc= new dG3DBoundaryConditionHierarchicalFunctionSpace(getTag(),1,1,dof_comp,groupBC,g,_groupGhostMPI);
            else
              spacebc = new dG3DHierarchicalFunctionSpace(getTag(),1,1,dof_comp);
          }
          else if (getDim() == 2)
          {
            if(bc_location !=nonLinearBoundaryCondition::ON_FACE)
              spacebc= new dG3DBoundaryConditionHierarchicalFunctionSpace(getTag(),1,1,dof_comp,groupBC,g,_groupGhostMPI);
            else
              spacebc = new dG3DHierarchicalFunctionSpace(getTag(),1,1,dof_comp);
          }
        }
        else
        {
          spacebc = new g3DDirichletBoundaryConditionHierarchicalFunctionSpace(getTag(),1,1,dof_comp);
        }
        break;
      default :
         Msg::Error("Unknown function space type for a dirichlet BC on domain %d",_phys);
      }
    }
    else if(bc_type == nonLinearBoundaryCondition::NEUMANN)
    {
      switch(_wsp)
      {
        case functionSpaceType::Lagrange :
          if(_fullDg){
            if (neumann_type == nonLinearNeumannBC::FORCE){
              // force in reference configuration following dof_comp is applied
              if ((this->getDim() == 3 and bc_location == nonLinearBoundaryCondition::ON_VOLUME) or
                  (this->getDim() == 2 and bc_location == nonLinearBoundaryCondition::ON_FACE) or
                  (this->getDim() == 1 and bc_location == nonLinearBoundaryCondition::ON_EDGE)){
                // applying BC in an entity having same dimension as current domain
                spacebc = new dG3DLagrangeFunctionSpace(getTag(),1,dof_comp);
              }
              else{
                // applying BC in an entity having a smaller dimension as current domain
                spacebc = new dG3DBoundaryConditionLagrangeFunctionSpace(getTag(),1,dof_comp,groupBC,gi,giv,NULL); // no ghostMPI for Neumann BC
              }
            }
            else if (neumann_type == nonLinearNeumannBC::PRESSURE or neumann_type == nonLinearNeumannBC::SCALAR_FLUX){
              // allways applying BC in an entity having a smaller dimension as current domain
              spacebc = new dG3DBoundaryConditionLagrangeFunctionSpace(getTag(),domcomp,groupBC,gi,giv,NULL); // no ghostMPI for Neumann BC
            }
            else{
              Msg::Error("nonLinearNeumannBC::NeumannBCType %s is not defined",neumann_type);
            }
          }
          else
            if (neumann_type == nonLinearNeumannBC::FORCE){
              spacebc = new g3DNeumannBoundaryConditionLagrangeFunctionSpace(getTag(),1,dof_comp);
            }
            else if (neumann_type == nonLinearNeumannBC::PRESSURE or neumann_type == nonLinearNeumannBC::SCALAR_FLUX){
              spacebc = new g3DNeumannBoundaryConditionLagrangeFunctionSpace(getTag(),domcomp);
            }
            else{
              Msg::Error("nonLinearNeumannBC::NeumannBCType %s is not defined",neumann_type);
            }

          break;
        //
        case functionSpaceType::Hierarchical :
          if(_fullDg)
          {
            if (neumann_type == nonLinearNeumannBC::FORCE){
              // force in reference configuration following dof_comp is applied
              if ((this->getDim() == 3 and bc_location == nonLinearBoundaryCondition::ON_VOLUME) or
                  (this->getDim() == 2 and bc_location == nonLinearBoundaryCondition::ON_FACE) or
                  (this->getDim() == 1 and bc_location == nonLinearBoundaryCondition::ON_EDGE)){
                // applying BC in an entity having same dimension as current domain
                spacebc = new dG3DHierarchicalFunctionSpace(getTag(),1,1,dof_comp);
              }
              else{
                // applying BC in an entity having a smaller dimension as current domain
                spacebc = new dG3DBoundaryConditionHierarchicalFunctionSpace(getTag(),1,1,dof_comp,groupBC,gi,giv,NULL); // no ghostMPI for Neumann BC
              }
            }
            else if (neumann_type == nonLinearNeumannBC::PRESSURE or neumann_type == nonLinearNeumannBC::SCALAR_FLUX){
              // allways applying BC in an entity having a smaller dimension as current domain
              spacebc = new dG3DBoundaryConditionHierarchicalFunctionSpace(getTag(),1,1,domcomp,groupBC,gi,giv,NULL); // no ghostMPI for Neumann BC
            }
            else{
              Msg::Error("nonLinearNeumannBC::NeumannBCType %s is not defined",neumann_type);
            }
          }
          else
            if (neumann_type == nonLinearNeumannBC::FORCE){
              spacebc = new g3DNeumannBoundaryConditionHierarchicalFunctionSpace(getTag(),1,1,dof_comp);
            }
            else if (neumann_type == nonLinearNeumannBC::PRESSURE or neumann_type == nonLinearNeumannBC::SCALAR_FLUX){
              spacebc = new g3DNeumannBoundaryConditionHierarchicalFunctionSpace(getTag(),1,domcomp);
            }
            else{
              Msg::Error("nonLinearNeumannBC::NeumannBCType %s is not defined",neumann_type);
            }
          break;
        //
        default :
        Msg::Error("Unknown function space type on a neumman BC on domain %d",_phys);
      }
    }
    else if(bc_type == nonLinearBoundaryCondition::INITIAL)
    {
      switch(_wsp)
      {
        case functionSpaceType::Lagrange:
          if(_fullDg)
          {
            if (getDim()==3){
              if(bc_location !=nonLinearBoundaryCondition::ON_VOLUME)
                spacebc = new dG3DBoundaryConditionLagrangeFunctionSpace(getTag(),1,dof_comp,groupBC,g,_groupGhostMPI);
              else
                spacebc = new dG3DLagrangeFunctionSpace(getTag(),1,dof_comp);
            }
            else if (getDim()==2){
              if(bc_location !=nonLinearBoundaryCondition::ON_FACE)
                spacebc = new dG3DBoundaryConditionLagrangeFunctionSpace(getTag(),1,dof_comp,groupBC,g,_groupGhostMPI);
              else
                spacebc = new dG3DLagrangeFunctionSpace(getTag(),1,dof_comp);
            }
          }
          else{
            spacebc = new g3DDirichletBoundaryConditionLagrangeFunctionSpace(getTag(),1,dof_comp);
          }
          break;
        //
        case functionSpaceType::Hierarchical:
          if(_fullDg)
          {
            if (getDim()==3){
              if(bc_location !=nonLinearBoundaryCondition::ON_VOLUME)
                spacebc = new dG3DBoundaryConditionHierarchicalFunctionSpace(getTag(),1,1,dof_comp,groupBC,g,_groupGhostMPI);
              else
                spacebc = new dG3DHierarchicalFunctionSpace(getTag(),1,1,dof_comp);
            }
            else if (getDim()==2){
              if(bc_location !=nonLinearBoundaryCondition::ON_FACE)
                spacebc = new dG3DBoundaryConditionHierarchicalFunctionSpace(getTag(),1,1,dof_comp,groupBC,g,_groupGhostMPI);
              else
                spacebc = new dG3DHierarchicalFunctionSpace(getTag(),1,1,dof_comp);
            }
          }
          else{
            spacebc = new g3DDirichletBoundaryConditionHierarchicalFunctionSpace(getTag(),1,1,dof_comp);
          }
          break;
        //
        default :
          Msg::Error("Unknown function space type for an initial BC on domain %d",_phys);
      }
    }
    // sync with space
    if (spacebc != NULL)
    {
      VertexBasedLagrangeFunctionSpace* spacebcLag = dynamic_cast<VertexBasedLagrangeFunctionSpace*>(spacebc);
      const VertexBasedLagrangeFunctionSpace* sp = dynamic_cast<const VertexBasedLagrangeFunctionSpace*>(this->getFunctionSpace());
      if (sp != NULL && spacebcLag != NULL)
      {
        const std::set<int>& primaryComp = sp->getCompsWithPrimaryShapeFunction();
        for (std::set<int>::const_iterator itt = primaryComp.begin(); itt != primaryComp.end(); itt++)
        {
          spacebcLag->usePrimaryShapeFunction(*itt);
        }
      };
      
      HierarchicalFunctionSpace* spacebcHier = dynamic_cast<HierarchicalFunctionSpace*>(spacebc);
      const HierarchicalFunctionSpace* spHier = dynamic_cast<const HierarchicalFunctionSpace*>(this->getFunctionSpace());
      if (spacebcHier != NULL &&  spHier!=NULL)
      {
        std::vector<int> comp;
        spacebcHier->getComp(comp);
        for (int i=0; i< comp.size(); i++)
        {
          int orderComp = spHier->getOrderComp(comp[i]);
          spacebcHier->setOrder(comp[i],orderComp);
        }
        
      }
    };
    return spacebc;
  }
  else if (dofType == mixedFunctionSpaceBase::DOF_CURL)
  {
    FunctionSpaceBase* spacebc = NULL;
    if(bc_type == nonLinearBoundaryCondition::DIRICHLET)
    {
      spacebc = new g3DDirichletBoundaryConditionHierarchicalCurlFunctionSpace(getTag(),1,dof_comp,1);
    }
    else if(bc_type == nonLinearBoundaryCondition::NEUMANN)
    {
      if (neumann_type == nonLinearNeumannBC::FORCE){
        spacebc = new g3DNeumannBoundaryConditionHierarchicalCurlFunctionSpace(getTag(),1,dof_comp,1);
      }
      else
      {
        Msg::Error("nonLinearNeumannBC::NeumannBCType %s is not defined",neumann_type);
      }
    }
    else if(bc_type == nonLinearBoundaryCondition::INITIAL)
    {
      spacebc = new g3DDirichletBoundaryConditionHierarchicalCurlFunctionSpace(getTag(),1,dof_comp,1);
    }
    return spacebc;
  }
  else
  {
    Msg::Error("Dof type %d has not been defined",dofType);
    return NULL;
  }
}

QuadratureBase* dG3DDomain::getQuadratureRulesForNeumannBC(nonLinearNeumannBC &neu) const
{
  QuadratureBase *integ = NULL;
  if((neu.onWhat == nonLinearBoundaryCondition::ON_VOLUME) or
     (neu.onWhat == nonLinearBoundaryCondition::ON_FACE and this->getDim() == 2) or
     (neu.onWhat == nonLinearBoundaryCondition::ON_EDGE and this->getDim() == 1) or
     (neu.onWhat == nonLinearBoundaryCondition::ON_VERTEX and this->getDim() == 0)){
    // when applying BC on domain
    GaussQuadrature *integgauss = static_cast<GaussQuadrature*>(integBulk);
    integ = new GaussQuadrature(*integgauss);
  }
  else
  {
    if (neu.g->size() > 0)
    {
      elementGroup::elementContainer::const_iterator it = neu.g->begin();
      MElement *ie = it->second;
      
      if (getFunctionSpaceType() == functionSpaceType::Hierarchical)
      {
        const HierarchicalFunctionSpace* hspace = dynamic_cast<const HierarchicalFunctionSpace*>(getFunctionSpace());
        int maxOrder = hspace->getMaxOrderBasis();
        QuadratureFactory::createGaussQuadratureForNeumannBCHierarchicalFE(maxOrder,ie,integ);
      }
      else
      {
        if( _gqt == Gauss)
        {
          QuadratureFactory::createGaussQuadratureForNeumannBC(ie,integ);
        }
        else if (_gqt == Lobatto)
        {
          QuadratureFactory::createLobattoQuadratureForNeumannBC(ie,integ);
        }
        else
        {
          Msg::Error("quadrature type %d is not considered",_gqt);
        }
      }
      IntPt* GP;
      int npts = integ->getIntPoints(ie,&GP);
      Msg::Info("NeumannBC: use %d integration points for element type %d",npts,ie->getTypeForMSH());

    }
    else{
      integ = new QuadratureVoid();
      Msg::Error("Neumann BC has no element");
    }
  }
  if (integ == NULL){
    Msg::Error("missing case in dG3DDomain::getQuadratureRulesForNeumannBC");
  }
  return integ;
}



void dG3DDomain::createTerms(unknownField *uf,IPField*ip)
{
  FunctionSpace<double>* dgspace = static_cast<FunctionSpace<double>*>(getFunctionSpace());
  if (getFunctionSpaceType() == functionSpaceType::Lagrange)
  {
    massterm = new mass3D(*dgspace,_mlaw, getNumNonLocalVariable());
  }
  else if (getFunctionSpaceType() == functionSpaceType::Hierarchical)
  {
    massterm = new mass3DHierarchicalFE(*dgspace,_mlaw, getNumNonLocalVariable());
  }
  else
  {
    Msg::Error("mass matrix has not implemented for this kind of functions space");
    massterm = new BiNonLinearTermVoid();
  }

  ltermBulk = new dG3DForceBulk(*dgspace,_mlaw,_fullDg,ip,_incrementNonlocalBased);
  if(_bmbp)
    btermBulk = new BilinearTermPerturbation<double>(ltermBulk,*dgspace,*dgspace,uf,ip,this,_eps);
  else
    btermBulk = new dG3DStiffnessBulk(*dgspace,_mlaw,_fullDg,ip);

  btermElasticBulk = new dG3DElasticStiffnessBulk(*dgspace,_mlaw,_fullDg,ip);
  btermUndamagedElasticBulk = new dG3DUndamagedElasticStiffnessBulk(*dgspace,_mlaw,_fullDg,ip);

  if(_fullDg)
  {
    ltermBound = new dG3DForceInter(*dgspace,dgspace,_mlaw,_mlaw,_interQuad,_beta1,_fullDg,ip,_incrementNonlocalBased);
    if(_interByPert)
      btermBound = new BilinearTermPerturbation<double>(ltermBound,*dgspace,*dgspace,uf,ip,this,_eps);
    else
    {
      btermBound = new dG3DStiffnessInter(*dgspace,*dgspace,_mlaw,_mlaw,_interQuad,_beta1,ip,uf,_fullDg,_incrementNonlocalBased);
    }
  }
  else
  {
     ltermBound = new nonLinearTermVoid();
     btermBound = new BiNonLinearTermVoid();
  }
  (static_cast<dG3DForceBulk*>(ltermBulk))->setDim(_dim);
  if(!_bmbp)
  {
    (static_cast<dG3DStiffnessBulk*>(btermBulk))->setDim(_dim);
  }
  (static_cast<dG3DElasticStiffnessBulk*>(btermElasticBulk))->setDim(_dim);
  (static_cast<dG3DUndamagedElasticStiffnessBulk*>(btermUndamagedElasticBulk))->setDim(_dim);

  if( getNumNonLocalVariable() >0)
  {
    (static_cast<dG3DForceBulk*>(ltermBulk))->setNonLocalEqRatio(getNonLocalEqRatio());
    (static_cast<dG3DForceBulk*>(ltermBulk))->setNumNonLocalVariable(getNumNonLocalVariable());
    if(!_bmbp)
    {
      (static_cast<dG3DStiffnessBulk*>(btermBulk))->setNonLocalEqRatio(getNonLocalEqRatio());
      (static_cast<dG3DStiffnessBulk*>(btermBulk))->setNumNonLocalVariable(getNumNonLocalVariable());
    }

    (static_cast<dG3DElasticStiffnessBulk*>(btermElasticBulk))->setNonLocalEqRatio(getNonLocalEqRatio());
    (static_cast<dG3DElasticStiffnessBulk*>(btermElasticBulk))->setNumNonLocalVariable(getNumNonLocalVariable());
    
    (static_cast<dG3DUndamagedElasticStiffnessBulk*>(btermUndamagedElasticBulk))->setNonLocalEqRatio(getNonLocalEqRatio());
    (static_cast<dG3DUndamagedElasticStiffnessBulk*>(btermUndamagedElasticBulk))->setNumNonLocalVariable(getNumNonLocalVariable());

    if(_fullDg)
    {
      (static_cast<dG3DForceInter*>(ltermBound))->setDim(_dim);
      (static_cast<dG3DForceInter*>(ltermBound))->setNonLocalEqRatio(getNonLocalEqRatio());
      (static_cast<dG3DForceInter*>(ltermBound))->setNumNonLocalVariable(getNumNonLocalVariable());
      (static_cast<dG3DForceInter*>(ltermBound))->setNonLocalStabilityParameter(getNonLocalStabilityParameter());
      (static_cast<dG3DForceInter*>(ltermBound))->setNonLocalContinuity(getNonLocalContinuity());
      if (!_interByPert){
        (static_cast<dG3DStiffnessInter*>(btermBound))->setDim(_dim);
        (static_cast<dG3DStiffnessInter*>(btermBound))->setNonLocalEqRatio(getNonLocalEqRatio());
        (static_cast<dG3DStiffnessInter*>(btermBound))->setNumNonLocalVariable(getNumNonLocalVariable());
        (static_cast<dG3DStiffnessInter*>(btermBound))->setNonLocalStabilityParameter(getNonLocalStabilityParameter());
        (static_cast<dG3DStiffnessInter*>(btermBound))->setNonLocalContinuity(getNonLocalContinuity());
      }
    }
  }
  if( getNumConstitutiveExtraDofDiffusionVariable() >0)
  {
    (static_cast<dG3DForceBulk*>(ltermBulk))->setNumConstitutiveExtraDofDiffusionVariable(getNumConstitutiveExtraDofDiffusionVariable());
    (static_cast<dG3DForceBulk*>(ltermBulk))->setConstitutiveExtraDofDiffusionEqRatio(getConstitutiveExtraDofDiffusionEqRatio());
    (static_cast<dG3DForceBulk*>(ltermBulk))->setConstitutiveExtraDofDiffusionUseEnergyConjugatedField(getConstitutiveExtraDofDiffusionUseEnergyConjugatedField());
    (static_cast<dG3DForceBulk*>(ltermBulk))->setConstitutiveExtraDofDiffusionAccountFieldSource(getConstitutiveExtraDofDiffusionAccountFieldSource());
    (static_cast<dG3DForceBulk*>(ltermBulk))->setConstitutiveExtraDofDiffusionAccountMecaSource(getConstitutiveExtraDofDiffusionAccountMecaSource());
    if(!_bmbp)
    {
      (static_cast<dG3DStiffnessBulk*>(btermBulk))->setNumConstitutiveExtraDofDiffusionVariable(getNumConstitutiveExtraDofDiffusionVariable());
      (static_cast<dG3DStiffnessBulk*>(btermBulk))->setConstitutiveExtraDofDiffusionEqRatio(getConstitutiveExtraDofDiffusionEqRatio());
      (static_cast<dG3DStiffnessBulk*>(btermBulk))->setConstitutiveExtraDofDiffusionUseEnergyConjugatedField(getConstitutiveExtraDofDiffusionUseEnergyConjugatedField());
      (static_cast<dG3DStiffnessBulk*>(btermBulk))->setConstitutiveExtraDofDiffusionAccountFieldSource(getConstitutiveExtraDofDiffusionAccountFieldSource());
      (static_cast<dG3DStiffnessBulk*>(btermBulk))->setConstitutiveExtraDofDiffusionAccountMecaSource(getConstitutiveExtraDofDiffusionAccountMecaSource());
    }
    if(_fullDg)
    {
      (static_cast<dG3DForceInter*>(ltermBound))->setNumConstitutiveExtraDofDiffusionVariable(getNumConstitutiveExtraDofDiffusionVariable());
      (static_cast<dG3DForceInter*>(ltermBound))->setConstitutiveExtraDofDiffusionEqRatio(getConstitutiveExtraDofDiffusionEqRatio());
      (static_cast<dG3DForceInter*>(ltermBound))->setConstitutiveExtraDofDiffusionUseEnergyConjugatedField(getConstitutiveExtraDofDiffusionUseEnergyConjugatedField());
      (static_cast<dG3DForceInter*>(ltermBound))->setConstitutiveExtraDofDiffusionAccountFieldSource(getConstitutiveExtraDofDiffusionAccountFieldSource());
      (static_cast<dG3DForceInter*>(ltermBound))->setConstitutiveExtraDofDiffusionAccountMecaSource(getConstitutiveExtraDofDiffusionAccountMecaSource());
      (static_cast<dG3DForceInter*>(ltermBound))->setConstitutiveExtraDofDiffusionStabilityParameter(getConstitutiveExtraDofDiffusionStabilityParameter());
      (static_cast<dG3DForceInter*>(ltermBound))->setConstitutiveExtraDofDiffusionContinuity(getConstitutiveExtraDofDiffusionContinuity());
      if(!_interByPert)
      {
        (static_cast<dG3DStiffnessInter*>(btermBound))->setNumConstitutiveExtraDofDiffusionVariable(getNumConstitutiveExtraDofDiffusionVariable());
        (static_cast<dG3DStiffnessInter*>(btermBound))->setConstitutiveExtraDofDiffusionEqRatio(getConstitutiveExtraDofDiffusionEqRatio());
        (static_cast<dG3DStiffnessInter*>(btermBound))->setConstitutiveExtraDofDiffusionUseEnergyConjugatedField(getConstitutiveExtraDofDiffusionUseEnergyConjugatedField());
        (static_cast<dG3DStiffnessInter*>(btermBound))->setConstitutiveExtraDofDiffusionAccountFieldSource(getConstitutiveExtraDofDiffusionAccountFieldSource());
        (static_cast<dG3DStiffnessInter*>(btermBound))->setConstitutiveExtraDofDiffusionAccountMecaSource(getConstitutiveExtraDofDiffusionAccountMecaSource());
        (static_cast<dG3DStiffnessInter*>(btermBound))->setConstitutiveExtraDofDiffusionStabilityParameter(getConstitutiveExtraDofDiffusionStabilityParameter());
        (static_cast<dG3DStiffnessInter*>(btermBound))->setConstitutiveExtraDofDiffusionContinuity(getConstitutiveExtraDofDiffusionContinuity());
      }
    }
  }
  if(getNumConstitutiveCurlVariable() > 0)
  {
    (static_cast<dG3DForceBulk*>(ltermBulk))->setNumConstitutiveCurlVariable(getNumConstitutiveCurlVariable());
    (static_cast<dG3DForceBulk*>(ltermBulk))->setConstitutiveCurlEqRatio(getConstitutiveCurlEqRatio());
    (static_cast<dG3DForceBulk*>(ltermBulk))->setConstitutiveCurlAccountFieldSource(getConstitutiveCurlAccountFieldSource());
    (static_cast<dG3DForceBulk*>(ltermBulk))->setConstitutiveCurlAccountMecaSource(getConstitutiveCurlAccountMecaSource());
    if(!_bmbp)
    {
      (static_cast<dG3DStiffnessBulk*>(btermBulk))->setNumConstitutiveCurlVariable(getNumConstitutiveCurlVariable());
      (static_cast<dG3DStiffnessBulk*>(btermBulk))->setConstitutiveCurlEqRatio(getConstitutiveCurlEqRatio());
      (static_cast<dG3DStiffnessBulk*>(btermBulk))->setConstitutiveCurlAccountFieldSource(getConstitutiveCurlAccountFieldSource());
      (static_cast<dG3DStiffnessBulk*>(btermBulk))->setConstitutiveCurlAccountMecaSource(getConstitutiveCurlAccountMecaSource());
    }
    if(_fullDg)
    {
      (static_cast<dG3DForceInter*>(ltermBound))->setNumConstitutiveCurlVariable(getNumConstitutiveCurlVariable());
      (static_cast<dG3DForceInter*>(ltermBound))->setConstitutiveCurlEqRatio(getConstitutiveCurlEqRatio());
      (static_cast<dG3DForceInter*>(ltermBound))->setConstitutiveCurlAccountFieldSource(getConstitutiveCurlAccountFieldSource());
      (static_cast<dG3DForceInter*>(ltermBound))->setConstitutiveCurlAccountMecaSource(getConstitutiveCurlAccountMecaSource());
      if(!_interByPert)
      {
        (static_cast<dG3DStiffnessInter*>(btermBound))->setNumConstitutiveCurlVariable(getNumConstitutiveCurlVariable());
        (static_cast<dG3DStiffnessInter*>(btermBound))->setConstitutiveCurlEqRatio(getConstitutiveCurlEqRatio());
        (static_cast<dG3DStiffnessInter*>(btermBound))->setConstitutiveCurlAccountFieldSource(getConstitutiveCurlAccountFieldSource());
        (static_cast<dG3DStiffnessInter*>(btermBound))->setConstitutiveCurlAccountMecaSource(getConstitutiveCurlAccountMecaSource());
      }
    }
  }

  this->btermVirtBound = new BiNonLinearTermVoid();
  this->ltermVirtBound = new nonLinearTermVoid();

  //path following
  bool withPF = false;
  if (getMacroSolver()!=NULL and _accountPathFollowing)
  {
    withPF = this->getMacroSolver()->withPathFollowing();
  }
  
  if (withPF){
    if (getMacroSolver()->getPathFollowingLocation() == pathFollowingManager::INTERFACE){
      scalarTermPF = new nonLinearScalarTermVoid();
      linearTermPF = new nonLinearTermVoid();
    }
    else{
      scalarTermPF = new dG3DDissipationPathFollowingBulkScalarTerm(this,ip);
      linearTermPF = new dG3DDissipationPathFollowingBulkLinearTerm(this,ip);
    }

    if (getMacroSolver()->getPathFollowingLocation() == pathFollowingManager::BULK)
    {
      scalarTermPFBound = new nonLinearScalarTermVoid();
      linearTermPFBound = new nonLinearTermVoid();
    }
    else
    {
      scalarTermPFBound = new dG3DDissipationPathFollowingBoundScalarTerm(this,ip);
      linearTermPFBound = new dG3DDissipationPathFollowingBoundLinearTerm(this,ip);
    }
    if(hasBodyForceForHO())
      linearTermBodyForcePF = new dG3DBodyForceTermPathFollowing(this,ip);
  }
  else{
    scalarTermPF = new nonLinearScalarTermVoid();;
    linearTermPF = new nonLinearTermVoid();;
    scalarTermPFBound = new nonLinearScalarTermVoid();;
    linearTermPFBound = new nonLinearTermVoid();
  }
  btermTangent = new dG3DHomogenizedTangent(this,ip);
  if(hasBodyForceForHO())
    btermBodyForceTangent = new dG3DBodyForceTangent(this, ip);
  btermElasticTangent = new dG3DHomogenizedElasticTangent(this,ip);
  btermUndamagedElasticTangent = new dG3DHomogenizedUndamagedElasticTangent(this,ip);
}

void dG3DDomain::initializeTerms(unknownField *uf,IPField*ip)
{
  createTerms(uf,ip);

  /* same value for fracture strength on both side */
  // not very elegant put very efficient to put here
  if(getMaterialLawMinus()->getType() == materialLaw::fracture and getMaterialLawPlus()->getType() == materialLaw::fracture)
  {
    // loop on ipvariable
    IntPt *GP;
    for(elementGroup::elementContainer::const_iterator it=gi->begin(); it!=gi->end(); ++it)
    {
      MElement *ele = it->second;
      int npts_inter=integBound->getIntPoints(ele,&GP);
      AllIPState::ipstateElementContainer *vips = ip->getAips()->getIPstate(ele->getNum());
      for(int j=0;j<npts_inter;j++){ // npts_inter points on both sides
        IPStateBase* ipsm = (*vips)[j];
        IPStateBase* ipsp = (*vips)[j+npts_inter];
        FractureCohesive3DIPVariable *ipvm = static_cast<FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::initial));
        FractureCohesive3DIPVariable *ipvp = static_cast<FractureCohesive3DIPVariable*>(ipsp->getState(IPStateBase::initial));
        ipvp->setFractureStrengthFactor(ipvm->fractureStrengthFactor());
        ipvm = static_cast<FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::previous));
        ipvp = static_cast<FractureCohesive3DIPVariable*>(ipsp->getState(IPStateBase::previous));
        ipvp->setFractureStrengthFactor(ipvm->fractureStrengthFactor());
        ipvm = static_cast<FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::current));
        ipvp = static_cast<FractureCohesive3DIPVariable*>(ipsp->getState(IPStateBase::current));
        ipvp->setFractureStrengthFactor(ipvm->fractureStrengthFactor());
      }
    }
  }
}

LinearTermBase<double>*
dG3DDomain::createNeumannTerm(FunctionSpace<double> *spneu,  const elementGroup* g,const simpleFunctionTime<double>* f,
                                   const unknownField *uf,const IPField * ip, const nonLinearBoundaryCondition::location onWhat,
                                   const nonLinearNeumannBC::NeumannBCType neumann_type, const int comp) const
{
  LinearTermBase<double>*term = NULL;
  if(neumann_type == nonLinearNeumannBC::PRESSURE)
  {
    term = new g3DFiniteStrainsPressureTerm(this,*spneu,uf,f);
  }
  else if(neumann_type == nonLinearNeumannBC::SCALAR_FLUX)
  {
    term = new g3DFiniteStrainsScalarFluxLinearTerm(this,*spneu,uf,comp,f);
  }
  else if (neumann_type == nonLinearNeumannBC::FORCE)
  {
    term= new g3DLoadTerm(*spneu,f,comp);
  }
  else{
    Msg::Error("nonLinearNeumannBC::NeumannBCType %s is not defined dG3DDomain::createNeumannTerm",neumann_type);
  }

  g3DLinearTerm<double>* gterm = static_cast<g3DLinearTerm<double>*>(term);
  gterm->setDim(_dim);
  if(getNumNonLocalVariable()>0)
  {
    gterm->setNonLocalEqRatio(getNonLocalEqRatio());
    gterm->setNumNonLocalVariable(getNumNonLocalVariable());
  }
  if( getNumConstitutiveExtraDofDiffusionVariable() >0)
  {
    gterm->setNumConstitutiveExtraDofDiffusionVariable(getNumConstitutiveExtraDofDiffusionVariable());
    gterm->setConstitutiveExtraDofDiffusionEqRatio(getConstitutiveExtraDofDiffusionEqRatio());
    gterm->setConstitutiveExtraDofDiffusionUseEnergyConjugatedField(getConstitutiveExtraDofDiffusionUseEnergyConjugatedField());
    gterm->setConstitutiveExtraDofDiffusionAccountFieldSource(getConstitutiveExtraDofDiffusionAccountFieldSource());
    gterm->setConstitutiveExtraDofDiffusionAccountMecaSource(getConstitutiveExtraDofDiffusionAccountMecaSource());
  }
  if(getNumConstitutiveCurlVariable() > 0)
  {
    gterm->setNumConstitutiveCurlVariable(getNumConstitutiveCurlVariable());
    gterm->setConstitutiveCurlEqRatio(getConstitutiveCurlEqRatio());
    gterm->setConstitutiveCurlAccountFieldSource(getConstitutiveCurlAccountFieldSource());
    gterm->setConstitutiveCurlAccountMecaSource(getConstitutiveCurlAccountMecaSource());
  }

  return term;
}
BilinearTermBase* dG3DDomain::createNeumannMatrixTerm(FunctionSpace<double> *spneu, const elementGroup* g,
                                                                     const simpleFunctionTime<double>* f, const unknownField *uf,
                                                                     const IPField * ip, const nonLinearBoundaryCondition::location onWhat,
                                                                     const nonLinearNeumannBC::NeumannBCType neumann_type,
                                                                     const int comp) const
{
  BilinearTermBase* bneuterm = NULL;
  if (neumann_type == nonLinearNeumannBC::PRESSURE){
    bneuterm = new g3DFiniteStrainsPressureBilinearTerm(this,*spneu,uf,f);
  }
  else if (neumann_type == nonLinearNeumannBC::SCALAR_FLUX){
    bneuterm =  new g3DFiniteStrainsScalarFluxBiLinearTerm(this,*spneu,*spneu,uf,comp,f);
  }
  else if (neumann_type == nonLinearNeumannBC::FORCE){
    bneuterm = new BiNonLinearTermVoid();
  }
  else{
    Msg::Error("nonLinearNeumannBC::NeumannBCType %s is not defined dG3DDomain::createNeumannMatrixTerm",neumann_type);
  }

  g3DBilinearTerm<double,double>* gterm = dynamic_cast<g3DBilinearTerm<double,double>*>(bneuterm);
//  gterm->setDim(_dim);
  if (gterm != NULL){
    gterm->setDim(_dim);
    if(getNumNonLocalVariable()>0)
    {
      gterm->setNonLocalEqRatio(getNonLocalEqRatio());
      gterm->setNumNonLocalVariable(getNumNonLocalVariable());
    }
    if( getNumConstitutiveExtraDofDiffusionVariable() >0)
    {
      gterm->setNumConstitutiveExtraDofDiffusionVariable(getNumConstitutiveExtraDofDiffusionVariable());
      gterm->setConstitutiveExtraDofDiffusionEqRatio(getConstitutiveExtraDofDiffusionEqRatio());
      gterm->setConstitutiveExtraDofDiffusionUseEnergyConjugatedField(getConstitutiveExtraDofDiffusionUseEnergyConjugatedField());
      gterm->setConstitutiveExtraDofDiffusionAccountFieldSource(getConstitutiveExtraDofDiffusionAccountFieldSource());
      gterm->setConstitutiveExtraDofDiffusionAccountMecaSource(getConstitutiveExtraDofDiffusionAccountMecaSource());
    }
    if (getNumConstitutiveCurlVariable() > 0)
    {
      gterm->setNumConstitutiveCurlVariable(getNumConstitutiveCurlVariable());
      gterm->setConstitutiveCurlEqRatio(getConstitutiveCurlEqRatio());
      gterm->setConstitutiveCurlAccountFieldSource(getConstitutiveCurlAccountFieldSource());
      gterm->setConstitutiveCurlAccountMecaSource(getConstitutiveCurlAccountMecaSource());
    }
  }
  return bneuterm;
}



void dG3DDomain::getNoMassDofs(std::set<Dof>& DofVector) const
{
  // Loop on all elements
  int numExtraDof = getNumNonLocalVariable()+getNumConstitutiveExtraDofDiffusionVariable();
  std::vector<Dof> R;
  const partDomain *dom = static_cast<const partDomain*>(this);
  for(elementGroup::elementContainer::const_iterator it=g->begin(); it!=g->end();++it){
    // get dofs of an element
    MElement *ele = it->second;
    R.clear();
    getFunctionSpace()->getKeys(ele,R);

    int nbFF = ele->getNumShapeFunctions();
    // Loop on all non-local var and shape functions
    for(int nl = 0; nl < numExtraDof; ++nl){
      for(int i = 0; i < nbFF; ++i){
        DofVector.insert(R[i+(3+nl)*nbFF]);
      }
    }
  }
}


void dG3DDomain::setDeformationGradientGradient(AllIPState *aips, const STensor33 &GM, const IPStateBase::whichState ws)
{
  IntPt *GP;
  if (gi->size() >0)
  {
    //
    for(elementGroup::elementContainer::const_iterator it=gi->begin(); it!=gi->end();++it)
    {
      int npts = integBound->getIntPoints(it->second,&GP);
      AllIPState::ipstateElementContainer *vips = aips->getIPstate(it->second->getNum());
   
      for(int j=0;j<npts;j++)
      {
        IPStateBase* ipsm = (*vips)[j];
        IPStateBase* ipsp = (*vips)[j+npts];

        dG3DIPVariableBase* ipvm = static_cast<dG3DIPVariableBase*>(ipsm->getState(ws));
        dG3DIPVariableBase* ipvp = static_cast<dG3DIPVariableBase*>(ipsp->getState(ws));
        if( ipvm->hasBodyForceForHO())
        {
          ipvm->getRefToGM()=GM;
        }
        if( ipvp->hasBodyForceForHO())
        {
          ipvp->getRefToGM()=GM;
        }
     }
    }
  }
  // bulk
  if (g->size() > 0)
  {
    unsigned int counter = 0;
    for (elementGroup::elementContainer::const_iterator it = g->begin(); it != g->end(); ++it){
      IntPt *GP;
      int npts_bulk=integBulk->getIntPoints(it->second,&GP);

      AllIPState::ipstateElementContainer *vips = aips->getIPstate(it->second->getNum());
   
      for(int j=0;j<npts_bulk;j++)
      {
        IPStateBase* ips = (*vips)[j];

        dG3DIPVariableBase* ipv = static_cast<dG3DIPVariableBase*>(ips->getState(ws));
        if( ipv->hasBodyForceForHO())
        {
          ipv->getRefToGM()=GM;
        }
      }
    }
  }

}

void dG3DDomain::computedFmdFM(AllIPState *aips, MInterfaceElement *ie, IntPt *GP, partDomain* efMinus, partDomain *efPlus,
                               std::map<Dof, STensor3> &dUdF, std::vector<Dof> &Rm, std::vector<Dof> &Rp,const bool useBarF, const IPStateBase::whichState ws) const   
{
  MElement *ele = dynamic_cast<MElement*>(ie);
  
  // do nothing if element is already eliminated
  if (!getElementErosionFilter()(ele)) return;
  nlsFunctionSpace<double>* _spaceminus = dynamic_cast<nlsFunctionSpace<double>*>(efMinus->getFunctionSpace());
  nlsFunctionSpace<double>* _spaceplus = dynamic_cast<nlsFunctionSpace<double>*>(efPlus->getFunctionSpace());
  
  const dG3DMaterialLaw *mlawMinus = static_cast<const dG3DMaterialLaw*>(this->getMaterialLawMinus());
  int npts=integBound->getIntPoints(ele,&GP);
  MElement *em = ie->getElem(0);
  MElement *ep = ie->getElem(1);
  AllIPState::ipstateElementContainer *vips = aips->getIPstate(ele->getNum());
  
  // Get Gauss points values on minus and plus elements
  IntPt *GPm; IntPt *GPp;
  _interQuad->getIntPoints(ie,GP,&GPm,&GPp);
  static std::vector<SVector3> dBarJdum,dBarJdup;
  static std::vector< std::vector< STensor33 > > dBarFdum,dBarFdup; //for each GP and for each node
  if(useBarF){
      int nbFFm = _spaceminus->getNumShapeFunctions(em,0); // same for component 0, 1, 2
      int nbFFp = _spaceplus->getNumShapeFunctions(ep,0);
      dBarJdum.resize(nbFFm);
      dBarFdum.resize(npts);
      dBarJdup.resize(nbFFp);
      dBarFdup.resize(npts);
      for (int k=0;k<nbFFm;k++){
        STensorOperation::zero(dBarJdum[k]); //check here if ok
      }
      for (int k=0;k<nbFFp;k++){
        STensorOperation::zero(dBarJdup[k]); //check here if ok
      }
      for (int ipg = 0; ipg < npts; ipg++){
        dBarFdum[ipg].resize(nbFFm);
        dBarFdup[ipg].resize(nbFFp);
        for (int k=0;k<nbFFm;k++){
          STensorOperation::zero(dBarFdum[ipg][k]); //check here if ok
        }
        for (int k=0;k<nbFFp;k++){
          STensorOperation::zero(dBarFdup[ipg][k]); //check here if ok
        }
      }
      
      double totalWeight=0.;
      for(int ipg = 0; ipg < npts; ipg++)
        totalWeight+=GP[ipg].weight;
      for(int ipg = 0; ipg < npts; ipg++)
      {
        const IPStateBase *ipsm = (*vips)[ipg];
        const dG3DIPVariableBase *ipvm  = static_cast<const dG3DIPVariableBase*>(ipsm->getState(IPStateBase::current));
        const IPStateBase *ipsp  = (*vips)[ipg+npts];
        const dG3DIPVariableBase *ipvp     = static_cast<const dG3DIPVariableBase*>(ipsp->getState(IPStateBase::current));
        const FractureCohesive3DIPVariable *ipvwfm;
        const FractureCohesive3DIPVariable *ipvwfp;
        const STensor3 *barFm;
        const STensor3 *barFp;
        if(mlawMinus->getType() == materialLaw::fracture )
        {
          ipvwfm = static_cast<const FractureCohesive3DIPVariable*>(ipvm);
          barFm=&ipvwfm->getBulkDeformationGradient(); //this is actually barF
          ipvwfp = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
          barFp=&ipvwfp->getBulkDeformationGradient(); //this is actually barF
        }
        else
        {
          barFm=&ipvm->getConstRefToDeformationGradient(); //this is actually barF
          barFp=&ipvp->getConstRefToDeformationGradient(); //this is actually barF
        }
        const std::vector<TensorialTraits<double>::GradType> &Gradsm = ipvm->gradf(_spaceminus,ie->getElem(0),GPm[ipg]);
        const std::vector<TensorialTraits<double>::GradType> &Gradsp = ipvp->gradf(_spaceplus,ie->getElem(1),GPp[ipg]);

        STensor3 invBarFm(barFm->invert());
        double localJm=ipvm->getLocalJ();
        double barJm=ipvm->getBarJ();
        double weightm=GP[ipg].weight*pow(localJm,1.-1./((double)(_dim)))*pow(barJm,1./((double)(_dim)))/totalWeight;

        STensor3 invBarFp(barFp->invert());
        double localJp=ipvp->getLocalJ();
        double barJp=ipvp->getBarJ();
        double weightp=GP[ipg].weight*pow(localJp,1.-1./((double)(_dim)))*pow(barJp,1./((double)(_dim)))/totalWeight;

        for(int k=0;k<nbFFm;k++)
        {
          SVector3 dBarJduTmp;
          STensorOperation::zero(dBarJduTmp);
          for(int ll=0; ll<_dim; ll++)
          {
            for(int n=0; n<_dim; n++)
            {
              dBarJduTmp(ll) += invBarFm(n,ll)*Gradsm[k][n]*weightm;
            }
          }
          dBarJdum[k]+=dBarJduTmp; 
        }
        for(int k=0;k<nbFFp;k++)
        {
          SVector3 dBarJduTmp;
          STensorOperation::zero(dBarJduTmp);
          for(int ll=0; ll<_dim; ll++)
          {
            for(int n=0; n<_dim; n++)
            {
              dBarJduTmp(ll) += invBarFp(n,ll)*Gradsp[k][n]*weightp;
            }
          }
          dBarJdup[k]+=dBarJduTmp; 
        }
      }
      
      static STensor3 I;
      STensorOperation::unity(I);
      for (int ipg = 0; ipg < npts; ipg++)
      {
        const IPStateBase *ipsm        = (*vips)[ipg];
        const dG3DIPVariableBase *ipvm     = static_cast<const dG3DIPVariableBase*>(ipsm->getState(IPStateBase::current));
        const IPStateBase *ipsp        = (*vips)[ipg+npts];
        const dG3DIPVariableBase *ipvp     = static_cast<const dG3DIPVariableBase*>(ipsp->getState(IPStateBase::current));
        const FractureCohesive3DIPVariable *ipvwfm;
        const FractureCohesive3DIPVariable *ipvwfp;
        const STensor3 *barFm;
        const STensor3 *barFp;
        
        if(mlawMinus->getType() == materialLaw::fracture )
        {
          ipvwfm = static_cast<const FractureCohesive3DIPVariable*>(ipvm);
          barFm=&ipvwfm->getBulkDeformationGradient(); //this is actually barF
          ipvwfp = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
          barFp=&ipvwfp->getBulkDeformationGradient(); //this is actually barF
        }
        else
        {
          barFm=&ipvm->getConstRefToDeformationGradient(); //this is actually barF
          barFp=&ipvp->getConstRefToDeformationGradient(); //this is actually barF
        }

        const std::vector<TensorialTraits<double>::GradType> &Gradsm = ipvm->gradf(_spaceminus,ie->getElem(0),GPm[ipg]);
        const std::vector<TensorialTraits<double>::GradType> &Gradsp = ipvp->gradf(_spaceplus,ie->getElem(1),GPp[ipg]);

        STensor3 invBarFm(barFm->invert());
        double localJm=ipvm->getLocalJ();
        double barJm=ipvm->getBarJ();
        double weightm=GP[ipg].weight*pow(localJm,1.-1./((double)(_dim)))*pow(barJm,1./((double)(_dim)))/totalWeight;

        STensor3 invBarFp(barFp->invert());
        double localJp=ipvp->getLocalJ();
        double barJp=ipvp->getBarJ();
        double weightp=GP[ipg].weight*pow(localJp,1.-1./((double)(_dim)))*pow(barJp,1./((double)(_dim)))/totalWeight;

        for(int k=0;k<nbFFm;k++)
        {
          static STensor33 dBarFduTmp;
          STensorOperation::zero(dBarFduTmp);
          for(int kk=0; kk<_dim; kk++)
          {
            for(int m=0; m<_dim; m++)
            {
              for(int jj=0; jj<_dim; jj++)
              {
                dBarFduTmp(kk,m,jj) += pow(barJm,1./((double)(_dim)))*pow(localJm,-1./((double)(_dim)))*I(kk,jj)*Gradsm[k](m);
                for(int p=0; p<_dim; p++)
                {
                  dBarFduTmp(kk,m,jj) -= 1./((double)(_dim))*pow(barJm,1./((double)(_dim)))*pow(localJm,-1./((double)(_dim)))*
                                                        barFm->operator()(kk,m)*invBarFm(p,jj)*Gradsm[k](p);             
                }
                dBarFduTmp(kk,m,jj) += 1./((double)(_dim))/barJm*barFm->operator()(kk,m)*dBarJdum[k](jj);
              }                                
            }
          }
          if(_dim==2 or _dim==1)
            dBarFduTmp(2,2,2)=Gradsm[k](3);
          if(_dim==1)
            dBarFduTmp(1,1,1)=Gradsm[k](2);
          dBarFdum[ipg][k]=dBarFduTmp;
        }
        for(int k=0;k<nbFFp;k++)
        {
          static STensor33 dBarFduTmp;
          STensorOperation::zero(dBarFduTmp);
          for(int kk=0; kk<_dim; kk++)
          {
            for(int m=0; m<_dim; m++)
            {
              for(int jj=0; jj<_dim; jj++)
              {
                dBarFduTmp(kk,m,jj) += pow(barJp,1./((double)(_dim)))*pow(localJp,-1./((double)(_dim)))*I(kk,jj)*Gradsp[k](m);
                for(int p=0; p<_dim; p++)
                {
                  dBarFduTmp(kk,m,jj) -= 1./((double)(_dim))*pow(barJp,1./((double)(_dim)))*pow(localJp,-1./((double)(_dim)))*
                                                        barFp->operator()(kk,m)*invBarFp(p,jj)*Gradsp[k](p);             
                }
                dBarFduTmp(kk,m,jj) += 1./((double)(_dim))/barJp*barFp->operator()(kk,m)*dBarJdup[k](jj);
              }                                
            }
          }
          if(_dim==2 or _dim==1)
            dBarFduTmp(2,2,2)=Gradsp[k](3);
          if(_dim==1)
            dBarFduTmp(1,1,1)=Gradsp[k](2);
          dBarFdup[ipg][k]=dBarFduTmp;
        }
      }
    }
    

  for(int j=0;j<npts;j++) {    
     IPStateBase *ipsm = (*vips)[j];
     IPStateBase *ipsp = (*vips)[j + npts];

     dG3DIPVariableBase *ipvm = static_cast<dG3DIPVariableBase *>(ipsm->getState(ws));
     dG3DIPVariableBase *ipvp = static_cast<dG3DIPVariableBase *>(ipsp->getState(ws));

     // get shape function gradients
     const std::vector<TensorialTraits<double>::GradType> &Gradm = ipvm->gradf(_spaceminus, em, GPm[j]);
     const std::vector<TensorialTraits<double>::GradType> &Gradp = ipvp->gradf(_spaceplus, ep, GPp[j]);
       
     STensor43 &dFm = ipvm->getRefToDFmdFM();
     STensor43 &dFp = ipvp->getRefToDFmdFM();
     if(dUdF.find(Rm[_spaceminus->getShapeFunctionsIndex(em, 0)]) != dUdF.end()){
        STensorOperation::zero(dFm);
        STensorOperation::zero(dFp);
        for (int cc = 0; cc < 3; cc++) {
           int nbFFm = _spaceminus->getNumShapeFunctions(em, cc);
           int nbFFmTotalLast = _spaceminus->getShapeFunctionsIndex(em, cc);
           int nbFFp = _spaceplus->getNumShapeFunctions(ep, cc);
           int nbFFpTotalLast = _spaceplus->getShapeFunctionsIndex(ep, cc); 
        if(useBarF){
          for (int i = 0; i < nbFFm; i++) {
             for (int ll = 0; ll < 3; ll++){
                for (int m = 0; m < 3; m++) {
                   for (int n = 0; n < 3; n++) {
                      dFm(ll,0,m,n) += dBarFdum[j][i](ll,0,cc) * dUdF[Rm[i + nbFFmTotalLast]](m,n);  
                      dFm(ll,1,m,n) += dBarFdum[j][i](ll,1,cc) * dUdF[Rm[i + nbFFmTotalLast]](m,n);
                      dFm(ll,2,m,n) += dBarFdum[j][i](ll,2,cc) * dUdF[Rm[i + nbFFmTotalLast]](m,n);
                   }   
                }
             }
          }             
          for (int i = 0; i < nbFFp; i++) {
             for (int ll = 0; ll < 3; ll++){
                for (int m = 0; m < 3; m++) {
                   for (int n = 0; n < 3; n++) {
                      dFp(ll,0,m,n) += dBarFdup[j][i](ll,0,cc) * dUdF[Rp[i + nbFFpTotalLast]](m,n);
                      dFp(ll,1,m,n) += dBarFdup[j][i](ll,1,cc) * dUdF[Rp[i + nbFFpTotalLast]](m,n);
                      dFp(ll,2,m,n) += dBarFdup[j][i](ll,2,cc) * dUdF[Rp[i + nbFFpTotalLast]](m,n);
                   }
                }
            }   
         }        
        }
        else{                  
           for (int i = 0; i < nbFFm; i++) {
             for (int m = 0; m < 3; m++) {
                for (int n = 0; n < 3; n++) {
                   dFm(cc,0,m,n) += Gradm[i + nbFFmTotalLast][0] * dUdF[Rm[i + nbFFmTotalLast]](m,n);  
                   dFm(cc,1,m,n) += Gradm[i + nbFFmTotalLast][1] * dUdF[Rm[i + nbFFmTotalLast]](m,n);
                   dFm(cc,2,m,n) += Gradm[i + nbFFmTotalLast][2] * dUdF[Rm[i + nbFFmTotalLast]](m,n);
                }
             }
          }
            
          for (int i = 0; i < nbFFp; i++) {
             for (int m = 0; m < 3; m++) {
                for (int n = 0; n < 3; n++) {
                   dFp(cc,0,m,n) += Gradp[i + nbFFpTotalLast][0] * dUdF[Rp[i + nbFFpTotalLast]](m,n);
                   dFp(cc,1,m,n) += Gradp[i + nbFFpTotalLast][1] * dUdF[Rp[i + nbFFpTotalLast]](m,n);
                   dFp(cc,2,m,n) += Gradp[i + nbFFpTotalLast][2] * dUdF[Rp[i + nbFFpTotalLast]](m,n);
                }
             }   
         }
       }  
     }  
   } 
  }   
}        
           
                
void dG3DDomain::computedFmdFM(MElement *e, IntPt *GP, AllIPState::ipstateElementContainer *vips, std::map<Dof, STensor3> &dUdF, std::vector<Dof> &R, const bool useBarF, IPStateBase::whichState ws) const
{
  if (!getElementErosionFilter()(e)) return;
  const nlsFunctionSpace<double>* nlspace  = dynamic_cast<const nlsFunctionSpace<double>*>(getFunctionSpace());
  int npts_bulk=integBulk->getIntPoints(e,&GP);
  static std::vector<SVector3> dBarJdu;
  static std::vector< std::vector< STensor33 > > dBarFdu; //for each GP and for each node
  if(useBarF){
     int nbFF = nlspace->getNumShapeFunctions(e,0);
     dBarJdu.resize(nbFF);
     dBarFdu.resize(npts_bulk);
     for (int k=0;k<nbFF;k++){
        STensorOperation::zero(dBarJdu[k]); //check here if ok
     }
     for (int ipg = 0; ipg < npts_bulk; ipg++){
        dBarFdu[ipg].resize(nbFF);
        for (int k=0;k<nbFF;k++){
          STensorOperation::zero(dBarFdu[ipg][k]); //check here if ok
        }
     }
     double totalWeight=0.;
     for(int ipg = 0; ipg < npts_bulk; ipg++)
        totalWeight+=GP[ipg].weight;
     for(int ipg = 0; ipg < npts_bulk; ipg++){
        const IPStateBase *ips = (*vips)[ipg];
        const dG3DIPVariableBase *ipv  = static_cast<const dG3DIPVariableBase*>(ips->getState(IPStateBase::current));
        const std::vector<TensorialTraits<double>::GradType>& Gradsipg = ipv->gradf(getFunctionSpace(),e,GP[ipg]);
        const STensor3 &barF = ipv->getConstRefToDeformationGradient(); //this is actually barF
        STensor3 invBarF(barF.invert());
        double localJ=ipv->getLocalJ();
        double barJ=ipv->getBarJ();
        double weight=GP[ipg].weight*pow(localJ,1.-(1./((double)(_dim))))*pow(barJ,1./((double)(_dim)))/totalWeight;
        for(int k=0;k<nbFF;k++)
        {
          SVector3 dBarJduTmp;
          STensorOperation::zero(dBarJduTmp);
          SVector3 Bipg;
          Bipg(0) = Gradsipg[k][0];
          Bipg(1) = Gradsipg[k][1];
          Bipg(2) = Gradsipg[k][2];
          for(int ll=0; ll<_dim; ll++)
          {
            for(int n=0; n<_dim; n++)
            {
              dBarJduTmp(ll) += invBarF(n,ll)*Bipg(n)*weight;
            }
          }
          dBarJdu[k]+=dBarJduTmp; 
        }
      }
      
      static STensor3 I;
      STensorOperation::unity(I);
      for (int ipg = 0; ipg < npts_bulk; ipg++)
      {
        const IPStateBase *ips = (*vips)[ipg];
        const dG3DIPVariableBase *ipv = static_cast<const dG3DIPVariableBase*>(ips->getState(IPStateBase::current));
        const std::vector<TensorialTraits<double>::GradType>& Gradsipg = ipv->gradf(getFunctionSpace(),e,GP[ipg]);
        const STensor3 &barF = ipv->getConstRefToDeformationGradient(); //this is actually barF
        STensor3 invBarF(barF.invert());
        double localJ=ipv->getLocalJ();
        double barJ=ipv->getBarJ();
        for(int k=0;k<nbFF;k++)
        {
          static STensor33 dBarFduTmp;
          STensorOperation::zero(dBarFduTmp);
          for(int kk=0; kk<_dim; kk++){
            for(int m=0; m<_dim; m++){
              for(int jj=0; jj<_dim; jj++){
                dBarFduTmp(kk,m,jj) += pow(barJ,1./((double)(_dim)))*pow(localJ,-1./((double)(_dim)))*I(kk,jj)*Gradsipg[k](m);
                for(int p=0; p<_dim; p++){
                  dBarFduTmp(kk,m,jj) -= 1./((double)(_dim))*pow(barJ,1./((double)(_dim)))*pow(localJ,-1./((double)(_dim)))*barF(kk,m)*invBarF(p,jj)*Gradsipg[k](p);             
                }
                dBarFduTmp(kk,m,jj) += 1./((double)(_dim))/barJ*barF(kk,m)*dBarJdu[k](jj);
              }                                
            }
          }
          if(_dim==2 or _dim==1)
            dBarFduTmp(2,2,2)=Gradsipg[k](3);
          if(_dim==1)
            dBarFduTmp(1,1,1)=Gradsipg[k](2);
          dBarFdu[ipg][k]=dBarFduTmp;
        }
      }
    }
  
  for (int j = 0; j < npts_bulk; j++) {
     IPStateBase *ips = (*vips)[j];
     dG3DIPVariableBase *ipv = static_cast<dG3DIPVariableBase *>(ips->getState(ws));
     // get grad of shape function from Gauss point
     const std::vector<TensorialTraits<double>::GradType> &Grads = ipv->gradf(getFunctionSpace(), e, GP[j]);
     STensor43 &dF = ipv->getRefToDFmdFM();
     if(dUdF.find(R[nlspace->getShapeFunctionsIndex(e, 0)]) != dUdF.end()){
        STensorOperation::zero(dF);
        for (int k = 0; k < 3; k++) {
          int nbFF = nlspace->getNumShapeFunctions(e, k);
          int nbFFTotalLast = nlspace->getShapeFunctionsIndex(e, k);
          if(useBarF){
            for (int i = 0; i < nbFF; i++){
             for (int ll = 0; ll < 3; ll++){
               for (int m = 0; m < 3; m++) {
                 for (int n = 0; n < 3; n++) {
                   dF(ll,0,m,n) += dBarFdu[j][i](ll,0,k) * dUdF[R[i + nbFFTotalLast]](m,n);  
                   dF(ll,1,m,n) += dBarFdu[j][i](ll,1,k) * dUdF[R[i + nbFFTotalLast]](m,n);  
                   dF(ll,2,m,n) += dBarFdu[j][i](ll,2,k) * dUdF[R[i + nbFFTotalLast]](m,n);       
                 }
               }
             }  
            }     
         } 
         else{
           for (int i = 0; i < nbFF; i++) {
             for (int m = 0; m < 3; m++) {
               for (int n = 0; n < 3; n++) {
                 dF(k,0,m,n) += Grads[i + nbFFTotalLast][0] * dUdF[R[i + nbFFTotalLast]](m,n);  
                 dF(k,1,m,n) += Grads[i + nbFFTotalLast][1] * dUdF[R[i + nbFFTotalLast]](m,n);  
                 dF(k,2,m,n) += Grads[i + nbFFTotalLast][2] * dUdF[R[i + nbFFTotalLast]](m,n);         
               }
             }
          } 
        }  
      }   
    } 
  }
}



void dG3DDomain::computedFmdGM(AllIPState *aips, MInterfaceElement *ie, IntPt *GP, partDomain* efMinus, partDomain *efPlus,
                               std::map<Dof, STensor33> &dUdG, std::vector<Dof> &Rm, std::vector<Dof> &Rp,const bool useBarF, const IPStateBase::whichState ws) const   
{
  MElement *ele = dynamic_cast<MElement*>(ie);
  
  // do nothing if element is already eliminated
  if (!getElementErosionFilter()(ele)) return;
  nlsFunctionSpace<double>* _spaceminus = dynamic_cast<nlsFunctionSpace<double>*>(efMinus->getFunctionSpace());
  nlsFunctionSpace<double>* _spaceplus = dynamic_cast<nlsFunctionSpace<double>*>(efPlus->getFunctionSpace());
  
  const dG3DMaterialLaw *mlawMinus = static_cast<const dG3DMaterialLaw*>(this->getMaterialLawMinus());
  int npts=integBound->getIntPoints(ele,&GP);
  MElement *em = ie->getElem(0);
  MElement *ep = ie->getElem(1);
  AllIPState::ipstateElementContainer *vips = aips->getIPstate(ele->getNum());
  
  // Get Gauss points values on minus and plus elements
  IntPt *GPm; IntPt *GPp;
  _interQuad->getIntPoints(ie,GP,&GPm,&GPp);
  static std::vector<SVector3> dBarJdum,dBarJdup;
  static std::vector< std::vector< STensor33 > > dBarFdum,dBarFdup; //for each GP and for each node
  if(useBarF){
      int nbFFm = _spaceminus->getNumShapeFunctions(em,0); // same for component 0, 1, 2
      int nbFFp = _spaceplus->getNumShapeFunctions(ep,0);
      dBarJdum.resize(nbFFm);
      dBarFdum.resize(npts);
      dBarJdup.resize(nbFFp);
      dBarFdup.resize(npts);
      for (int k=0;k<nbFFm;k++){
        STensorOperation::zero(dBarJdum[k]); //check here if ok
      }
      for (int k=0;k<nbFFp;k++){
        STensorOperation::zero(dBarJdup[k]); //check here if ok
      }
      for (int ipg = 0; ipg < npts; ipg++){
        dBarFdum[ipg].resize(nbFFm);
        dBarFdup[ipg].resize(nbFFp);
        for (int k=0;k<nbFFm;k++){
          STensorOperation::zero(dBarFdum[ipg][k]); //check here if ok
        }
        for (int k=0;k<nbFFp;k++){
          STensorOperation::zero(dBarFdup[ipg][k]); //check here if ok
        }
      }
      
      double totalWeight=0.;
      for(int ipg = 0; ipg < npts; ipg++)
        totalWeight+=GP[ipg].weight;
      for(int ipg = 0; ipg < npts; ipg++)
      {
        const IPStateBase *ipsm = (*vips)[ipg];
        const dG3DIPVariableBase *ipvm  = static_cast<const dG3DIPVariableBase*>(ipsm->getState(IPStateBase::current));
        const IPStateBase *ipsp  = (*vips)[ipg+npts];
        const dG3DIPVariableBase *ipvp     = static_cast<const dG3DIPVariableBase*>(ipsp->getState(IPStateBase::current));
        const FractureCohesive3DIPVariable *ipvwfm;
        const FractureCohesive3DIPVariable *ipvwfp;
        const STensor3 *barFm;
        const STensor3 *barFp;
        if(mlawMinus->getType() == materialLaw::fracture )
        {
          ipvwfm = static_cast<const FractureCohesive3DIPVariable*>(ipvm);
          barFm=&ipvwfm->getBulkDeformationGradient(); //this is actually barF
          ipvwfp = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
          barFp=&ipvwfp->getBulkDeformationGradient(); //this is actually barF
        }
        else
        {
          barFm=&ipvm->getConstRefToDeformationGradient(); //this is actually barF
          barFp=&ipvp->getConstRefToDeformationGradient(); //this is actually barF
        }
        const std::vector<TensorialTraits<double>::GradType> &Gradsm = ipvm->gradf(_spaceminus,ie->getElem(0),GPm[ipg]);
        const std::vector<TensorialTraits<double>::GradType> &Gradsp = ipvp->gradf(_spaceplus,ie->getElem(1),GPp[ipg]);

        STensor3 invBarFm(barFm->invert());
        double localJm=ipvm->getLocalJ();
        double barJm=ipvm->getBarJ();
        double weightm=GP[ipg].weight*pow(localJm,1.-1./((double)(_dim)))*pow(barJm,1./((double)(_dim)))/totalWeight;

        STensor3 invBarFp(barFp->invert());
        double localJp=ipvp->getLocalJ();
        double barJp=ipvp->getBarJ();
        double weightp=GP[ipg].weight*pow(localJp,1.-1./((double)(_dim)))*pow(barJp,1./((double)(_dim)))/totalWeight;

        for(int k=0;k<nbFFm;k++)
        {
          SVector3 dBarJduTmp;
          STensorOperation::zero(dBarJduTmp);
          for(int ll=0; ll<_dim; ll++)
          {
            for(int n=0; n<_dim; n++)
            {
              dBarJduTmp(ll) += invBarFm(n,ll)*Gradsm[k][n]*weightm;
            }
          }
          dBarJdum[k]+=dBarJduTmp; 
        }
        for(int k=0;k<nbFFp;k++)
        {
          SVector3 dBarJduTmp;
          STensorOperation::zero(dBarJduTmp);
          for(int ll=0; ll<_dim; ll++)
          {
            for(int n=0; n<_dim; n++)
            {
              dBarJduTmp(ll) += invBarFp(n,ll)*Gradsp[k][n]*weightp;
            }
          }
          dBarJdup[k]+=dBarJduTmp; 
        }
      }
      
      static STensor3 I;
      STensorOperation::unity(I);
      for (int ipg = 0; ipg < npts; ipg++)
      {
        const IPStateBase *ipsm        = (*vips)[ipg];
        const dG3DIPVariableBase *ipvm     = static_cast<const dG3DIPVariableBase*>(ipsm->getState(IPStateBase::current));
        const IPStateBase *ipsp        = (*vips)[ipg+npts];
        const dG3DIPVariableBase *ipvp     = static_cast<const dG3DIPVariableBase*>(ipsp->getState(IPStateBase::current));
        const FractureCohesive3DIPVariable *ipvwfm;
        const FractureCohesive3DIPVariable *ipvwfp;
        const STensor3 *barFm;
        const STensor3 *barFp;
        
        if(mlawMinus->getType() == materialLaw::fracture )
        {
          ipvwfm = static_cast<const FractureCohesive3DIPVariable*>(ipvm);
          barFm=&ipvwfm->getBulkDeformationGradient(); //this is actually barF
          ipvwfp = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
          barFp=&ipvwfp->getBulkDeformationGradient(); //this is actually barF
        }
        else
        {
          barFm=&ipvm->getConstRefToDeformationGradient(); //this is actually barF
          barFp=&ipvp->getConstRefToDeformationGradient(); //this is actually barF
        }

        const std::vector<TensorialTraits<double>::GradType> &Gradsm = ipvm->gradf(_spaceminus,ie->getElem(0),GPm[ipg]);
        const std::vector<TensorialTraits<double>::GradType> &Gradsp = ipvp->gradf(_spaceplus,ie->getElem(1),GPp[ipg]);

        STensor3 invBarFm(barFm->invert());
        double localJm=ipvm->getLocalJ();
        double barJm=ipvm->getBarJ();
        double weightm=GP[ipg].weight*pow(localJm,1.-1./((double)(_dim)))*pow(barJm,1./((double)(_dim)))/totalWeight;

        STensor3 invBarFp(barFp->invert());
        double localJp=ipvp->getLocalJ();
        double barJp=ipvp->getBarJ();
        double weightp=GP[ipg].weight*pow(localJp,1.-1./((double)(_dim)))*pow(barJp,1./((double)(_dim)))/totalWeight;

        for(int k=0;k<nbFFm;k++)
        {
          static STensor33 dBarFduTmp;
          STensorOperation::zero(dBarFduTmp);
          for(int kk=0; kk<_dim; kk++)
          {
            for(int m=0; m<_dim; m++)
            {
              for(int jj=0; jj<_dim; jj++)
              {
                dBarFduTmp(kk,m,jj) += pow(barJm,1./((double)(_dim)))*pow(localJm,-1./((double)(_dim)))*I(kk,jj)*Gradsm[k](m);
                for(int p=0; p<_dim; p++)
                {
                  dBarFduTmp(kk,m,jj) -= 1./((double)(_dim))*pow(barJm,1./((double)(_dim)))*pow(localJm,-1./((double)(_dim)))*
                                                        barFm->operator()(kk,m)*invBarFm(p,jj)*Gradsm[k](p);             
                }
                dBarFduTmp(kk,m,jj) += 1./((double)(_dim))/barJm*barFm->operator()(kk,m)*dBarJdum[k](jj);
              }                                
            }
          }
          if(_dim==2 or _dim==1)
            dBarFduTmp(2,2,2)=Gradsm[k](3);
          if(_dim==1)
            dBarFduTmp(1,1,1)=Gradsm[k](2);
          dBarFdum[ipg][k]=dBarFduTmp;
        }
        for(int k=0;k<nbFFp;k++)
        {
          static STensor33 dBarFduTmp;
          STensorOperation::zero(dBarFduTmp);
          for(int kk=0; kk<_dim; kk++)
          {
            for(int m=0; m<_dim; m++)
            {
              for(int jj=0; jj<_dim; jj++)
              {
                dBarFduTmp(kk,m,jj) += pow(barJp,1./((double)(_dim)))*pow(localJp,-1./((double)(_dim)))*I(kk,jj)*Gradsp[k](m);
                for(int p=0; p<_dim; p++)
                {
                  dBarFduTmp(kk,m,jj) -= 1./((double)(_dim))*pow(barJp,1./((double)(_dim)))*pow(localJp,-1./((double)(_dim)))*
                                                        barFp->operator()(kk,m)*invBarFp(p,jj)*Gradsp[k](p);             
                }
                dBarFduTmp(kk,m,jj) += 1./((double)(_dim))/barJp*barFp->operator()(kk,m)*dBarJdup[k](jj);
              }                                
            }
          }
          if(_dim==2 or _dim==1)
            dBarFduTmp(2,2,2)=Gradsp[k](3);
          if(_dim==1)
            dBarFduTmp(1,1,1)=Gradsp[k](2);
          dBarFdup[ipg][k]=dBarFduTmp;
        }
      }
    }
    

  for(int j=0;j<npts;j++) {    
     IPStateBase *ipsm = (*vips)[j];
     IPStateBase *ipsp = (*vips)[j + npts];

     dG3DIPVariableBase *ipvm = static_cast<dG3DIPVariableBase *>(ipsm->getState(ws));
     dG3DIPVariableBase *ipvp = static_cast<dG3DIPVariableBase *>(ipsp->getState(ws));

     // get shape function gradients
     const std::vector<TensorialTraits<double>::GradType> &Gradm = ipvm->gradf(_spaceminus, em, GPm[j]);
     const std::vector<TensorialTraits<double>::GradType> &Gradp = ipvp->gradf(_spaceplus, ep, GPp[j]);
       
     STensor53 &dFm = ipvm->getRefToDFmdGM();
     STensor53 &dFp = ipvp->getRefToDFmdGM();
     if(dUdG.find(Rm[_spaceminus->getShapeFunctionsIndex(em, 0)]) != dUdG.end()){
        STensorOperation::zero(dFm);
        STensorOperation::zero(dFp);
        for (int cc = 0; cc < 3; cc++) {
           int nbFFm = _spaceminus->getNumShapeFunctions(em, cc);
           int nbFFmTotalLast = _spaceminus->getShapeFunctionsIndex(em, cc);
           int nbFFp = _spaceplus->getNumShapeFunctions(ep, cc);
           int nbFFpTotalLast = _spaceplus->getShapeFunctionsIndex(ep, cc); 
        if(useBarF){
          for (int i = 0; i < nbFFm; i++) {
             for (int ll = 0; ll < 3; ll++){
                for (int m = 0; m < 3; m++) {
                   for (int n = 0; n < 3; n++) {
                      for (int s = 0; s < 3; s++) {
                        dFm(ll,0,m,n,s) += dBarFdum[j][i](ll,0,cc) * dUdG[Rm[i + nbFFmTotalLast]](m,n,s);  
                        dFm(ll,1,m,n,s) += dBarFdum[j][i](ll,1,cc) * dUdG[Rm[i + nbFFmTotalLast]](m,n,s);
                        dFm(ll,2,m,n,s) += dBarFdum[j][i](ll,2,cc) * dUdG[Rm[i + nbFFmTotalLast]](m,n,s);
                      }  
                   }   
                }
             }
          }             
          for (int i = 0; i < nbFFp; i++) {
             for (int ll = 0; ll < 3; ll++){
                for (int m = 0; m < 3; m++) {
                   for (int n = 0; n < 3; n++) {
                     for (int s = 0; s < 3; s++) {
                       dFp(ll,0,m,n,s) += dBarFdup[j][i](ll,0,cc) * dUdG[Rp[i + nbFFpTotalLast]](m,n,s);
                       dFp(ll,1,m,n,s) += dBarFdup[j][i](ll,1,cc) * dUdG[Rp[i + nbFFpTotalLast]](m,n,s);
                       dFp(ll,2,m,n,s) += dBarFdup[j][i](ll,2,cc) * dUdG[Rp[i + nbFFpTotalLast]](m,n,s);
                     }  
                   }
                }
            }   
         }        
        }
        else{                  
           for (int i = 0; i < nbFFm; i++) {
             for (int m = 0; m < 3; m++) {
                for (int n = 0; n < 3; n++) {
                  for (int s = 0; s < 3; s++) {
                   dFm(cc,0,m,n,s) += Gradm[i + nbFFmTotalLast][0] * dUdG[Rm[i + nbFFmTotalLast]](m,n,s);  
                   dFm(cc,1,m,n,s) += Gradm[i + nbFFmTotalLast][1] * dUdG[Rm[i + nbFFmTotalLast]](m,n,s);
                   dFm(cc,2,m,n,s) += Gradm[i + nbFFmTotalLast][2] * dUdG[Rm[i + nbFFmTotalLast]](m,n,s);
                  }
                }
             }
          }
            
          for (int i = 0; i < nbFFp; i++) {
             for (int m = 0; m < 3; m++) {
                for (int n = 0; n < 3; n++) {
                  for (int s = 0; s < 3; s++) {
                   dFp(cc,0,m,n,s) += Gradp[i + nbFFpTotalLast][0] * dUdG[Rp[i + nbFFpTotalLast]](m,n,s);
                   dFp(cc,1,m,n,s) += Gradp[i + nbFFpTotalLast][1] * dUdG[Rp[i + nbFFpTotalLast]](m,n,s);
                   dFp(cc,2,m,n,s) += Gradp[i + nbFFpTotalLast][2] * dUdG[Rp[i + nbFFpTotalLast]](m,n,s);
                  }  
                }
             }   
         }
       }  
     }  
   } 
  }   
}            


void dG3DDomain::computedFmdGM(MElement *e, IntPt *GP, AllIPState::ipstateElementContainer *vips, std::map<Dof, STensor33> &dUdG, std::vector<Dof> &R, const bool useBarF, IPStateBase::whichState ws) const
{
  if (!getElementErosionFilter()(e)) return;
  const nlsFunctionSpace<double>* nlspace  = dynamic_cast<const nlsFunctionSpace<double>*>(getFunctionSpace());
  int npts_bulk=integBulk->getIntPoints(e,&GP);
  static std::vector<SVector3> dBarJdu;
  static std::vector< std::vector< STensor33 > > dBarFdu; //for each GP and for each node
  if(useBarF){
     int nbFF = nlspace->getNumShapeFunctions(e,0);
     dBarJdu.resize(nbFF);
     dBarFdu.resize(npts_bulk);
     for (int k=0;k<nbFF;k++){
        STensorOperation::zero(dBarJdu[k]); //check here if ok
     }
     for (int ipg = 0; ipg < npts_bulk; ipg++){
        dBarFdu[ipg].resize(nbFF);
        for (int k=0;k<nbFF;k++){
          STensorOperation::zero(dBarFdu[ipg][k]); //check here if ok
        }
     }
     double totalWeight=0.;
     for(int ipg = 0; ipg < npts_bulk; ipg++)
        totalWeight+=GP[ipg].weight;
     for(int ipg = 0; ipg < npts_bulk; ipg++){
        const IPStateBase *ips = (*vips)[ipg];
        const dG3DIPVariableBase *ipv  = static_cast<const dG3DIPVariableBase*>(ips->getState(IPStateBase::current));
        const std::vector<TensorialTraits<double>::GradType>& Gradsipg = ipv->gradf(getFunctionSpace(),e,GP[ipg]);
        const STensor3 &barF = ipv->getConstRefToDeformationGradient(); //this is actually barF
        STensor3 invBarF(barF.invert());
        double localJ=ipv->getLocalJ();
        double barJ=ipv->getBarJ();
        double weight=GP[ipg].weight*pow(localJ,1.-(1./((double)(_dim))))*pow(barJ,1./((double)(_dim)))/totalWeight;
        for(int k=0;k<nbFF;k++)
        {
          SVector3 dBarJduTmp;
          STensorOperation::zero(dBarJduTmp);
          SVector3 Bipg;
          Bipg(0) = Gradsipg[k][0];
          Bipg(1) = Gradsipg[k][1];
          Bipg(2) = Gradsipg[k][2];
          for(int ll=0; ll<_dim; ll++)
          {
            for(int n=0; n<_dim; n++)
            {
              dBarJduTmp(ll) += invBarF(n,ll)*Bipg(n)*weight;
            }
          }
          dBarJdu[k]+=dBarJduTmp; 
        }
      }
      
      static STensor3 I;
      STensorOperation::unity(I);
      for (int ipg = 0; ipg < npts_bulk; ipg++)
      {
        const IPStateBase *ips = (*vips)[ipg];
        const dG3DIPVariableBase *ipv = static_cast<const dG3DIPVariableBase*>(ips->getState(IPStateBase::current));
        const std::vector<TensorialTraits<double>::GradType>& Gradsipg = ipv->gradf(getFunctionSpace(),e,GP[ipg]);
        const STensor3 &barF = ipv->getConstRefToDeformationGradient(); //this is actually barF
        STensor3 invBarF(barF.invert());
        double localJ=ipv->getLocalJ();
        double barJ=ipv->getBarJ();
        for(int k=0;k<nbFF;k++)
        {
          static STensor33 dBarFduTmp;
          STensorOperation::zero(dBarFduTmp);
          for(int kk=0; kk<_dim; kk++){
            for(int m=0; m<_dim; m++){
              for(int jj=0; jj<_dim; jj++){
                dBarFduTmp(kk,m,jj) += pow(barJ,1./((double)(_dim)))*pow(localJ,-1./((double)(_dim)))*I(kk,jj)*Gradsipg[k](m);
                for(int p=0; p<_dim; p++){
                  dBarFduTmp(kk,m,jj) -= 1./((double)(_dim))*pow(barJ,1./((double)(_dim)))*pow(localJ,-1./((double)(_dim)))*barF(kk,m)*invBarF(p,jj)*Gradsipg[k](p);             
                }
                dBarFduTmp(kk,m,jj) += 1./((double)(_dim))/barJ*barF(kk,m)*dBarJdu[k](jj);
              }                                
            }
          }
          if(_dim==2 or _dim==1)
            dBarFduTmp(2,2,2)=Gradsipg[k](3);
          if(_dim==1)
            dBarFduTmp(1,1,1)=Gradsipg[k](2);
          dBarFdu[ipg][k]=dBarFduTmp;
        }
      }
    }
  
  for (int j = 0; j < npts_bulk; j++) {
     IPStateBase *ips = (*vips)[j];
     dG3DIPVariableBase *ipv = static_cast<dG3DIPVariableBase *>(ips->getState(ws));
     // get grad of shape function from Gauss point
     const std::vector<TensorialTraits<double>::GradType> &Grads = ipv->gradf(getFunctionSpace(), e, GP[j]);
     STensor53 &dF = ipv->getRefToDFmdGM();
     if(dUdG.find(R[nlspace->getShapeFunctionsIndex(e, 0)]) != dUdG.end()){
        STensorOperation::zero(dF);
        for (int k = 0; k < 3; k++) {
          int nbFF = nlspace->getNumShapeFunctions(e, k);
          int nbFFTotalLast = nlspace->getShapeFunctionsIndex(e, k);
          if(useBarF){
            for (int i = 0; i < nbFF; i++){
             for (int ll = 0; ll < 3; ll++){
               for (int m = 0; m < 3; m++) {
                 for (int n = 0; n < 3; n++) {
                   for (int s = 0; s < 3; s++) {
                     dF(ll,0,m,n,s) += dBarFdu[j][i](ll,0,k) * dUdG[R[i + nbFFTotalLast]](m,n,s);  
                     dF(ll,1,m,n,s) += dBarFdu[j][i](ll,1,k) * dUdG[R[i + nbFFTotalLast]](m,n,s);  
                     dF(ll,2,m,n,s) += dBarFdu[j][i](ll,2,k) * dUdG[R[i + nbFFTotalLast]](m,n,s); 
                   }        
                 }
               }
             }  
            }     
         } 
         else{
           for (int i = 0; i < nbFF; i++) {
             for (int m = 0; m < 3; m++) {
               for (int n = 0; n < 3; n++) {
                 for (int s = 0; s < 3; s++) {
                   dF(k,0,m,n,s) += Grads[i + nbFFTotalLast][0] * dUdG[R[i + nbFFTotalLast]](m,n,s);  
                   dF(k,1,m,n,s) += Grads[i + nbFFTotalLast][1] * dUdG[R[i + nbFFTotalLast]](m,n,s);  
                   dF(k,2,m,n,s) += Grads[i + nbFFTotalLast][2] * dUdG[R[i + nbFFTotalLast]](m,n,s);     
                 }      
               }
             }
          } 
        }  
      }   
    } 
  }
}


////ling Wu // begin 

void dG3DDomain::setdFmdFM(AllIPState *aips, std::map<Dof, STensor3> &dUdF,const IPStateBase::whichState ws){
  IntPt *GP;
  std::vector<Dof> Rm;
  std::vector<Dof> Rp;
  // interface elements
  if (gi->size() >0)
  {
    dG3DMaterialLaw *mlawMinus = static_cast<dG3DMaterialLaw*>(this->getMaterialLawMinus());
    dG3DMaterialLaw *mlawPlus = static_cast<dG3DMaterialLaw*>(this->getMaterialLawPlus());
    dG3DMaterialLaw *mlawbulkm;
    dG3DMaterialLaw *mlawbulkp;
    if(mlawMinus->getType() == materialLaw::fracture)
      mlawbulkm = static_cast< dG3DMaterialLaw* >((static_cast< FractureByCohesive3DLaw* > (mlawMinus) )->getBulkLaw());
    else
      mlawbulkm= static_cast<dG3DMaterialLaw*>(mlawMinus);
    if(mlawPlus->getType() == materialLaw::fracture)
      mlawbulkp = static_cast< dG3DMaterialLaw* >((static_cast< FractureByCohesive3DLaw* > (mlawPlus) )->getBulkLaw());
    else
      mlawbulkp= static_cast<dG3DMaterialLaw*>(mlawPlus);
    if (mlawbulkm->getUseBarF()!=mlawbulkp->getUseBarF()) //check with the bulk law
      Msg::Error("dG3DDomain::setdFmdFM All the constitutive laws need to use Fbar or not");
    bool useBarF=mlawbulkm->getUseBarF();
    
    for(elementGroup::elementContainer::const_iterator it=gi->begin(); it!=gi->end();++it)
    {
      MInterfaceElement *ie = dynamic_cast<MInterfaceElement*>(it->second);
      Rm.clear();
      this->getMinusDomain()->getFunctionSpace()->getKeys(ie->getElem(0),Rm);
      Rp.clear();
      this->getPlusDomain()->getFunctionSpace()->getKeys(ie->getElem(1),Rp);
      
      this->computedFmdFM(aips, ie, GP, getMinusDomain(), getPlusDomain(), dUdF, Rm, Rp,useBarF, ws);
    }
  }
  // bulk
  if (g->size() > 0)
  {
    dG3DMaterialLaw *mlaw;
    if(_mlaw->getType() == materialLaw::fracture)
      mlaw = static_cast< dG3DMaterialLaw* >((static_cast< FractureByCohesive3DLaw* > (getMaterialLaw()) )->getBulkLaw());
    else{
      mlaw= static_cast<dG3DMaterialLaw*>(getMaterialLaw());
    }
    bool useBarF=mlaw->getUseBarF();
    for (elementGroup::elementContainer::const_iterator it = g->begin(); it != g->end(); ++it){
      MElement *e = it->second;
      //std::cout << "Elem: " << e->getNum() << std::endl;
      Rm.clear();
      getFunctionSpace()->getKeys(e,Rm);

      this->computedFmdFM(e, GP, aips->getIPstate(e->getNum()), dUdF, Rm, useBarF, ws); 
    }
  }
};




void dG3DDomain::setdFmdGM(AllIPState *aips, std::map<Dof, STensor33> &dUdG,const IPStateBase::whichState ws){
  IntPt *GP;
  std::vector<Dof> Rm;
  std::vector<Dof> Rp;
  // interface elements
  if (gi->size() >0)
  {
    dG3DMaterialLaw *mlawMinus = static_cast<dG3DMaterialLaw*>(this->getMaterialLawMinus());
    dG3DMaterialLaw *mlawPlus = static_cast<dG3DMaterialLaw*>(this->getMaterialLawPlus());
    dG3DMaterialLaw *mlawbulkm;
    dG3DMaterialLaw *mlawbulkp;
    if(mlawMinus->getType() == materialLaw::fracture)
      mlawbulkm = static_cast< dG3DMaterialLaw* >((static_cast< FractureByCohesive3DLaw* > (mlawMinus) )->getBulkLaw());
    else
      mlawbulkm= static_cast<dG3DMaterialLaw*>(mlawMinus);
    if(mlawPlus->getType() == materialLaw::fracture)
      mlawbulkp = static_cast< dG3DMaterialLaw* >((static_cast< FractureByCohesive3DLaw* > (mlawPlus) )->getBulkLaw());
    else
      mlawbulkp= static_cast<dG3DMaterialLaw*>(mlawPlus);
    if (mlawbulkm->getUseBarF()!=mlawbulkp->getUseBarF()) //check with the bulk law
      Msg::Error("dG3DDomain::setdFmdGM All the constitutive laws need to use Fbar or not");
    bool useBarF=mlawbulkm->getUseBarF();
    
    for(elementGroup::elementContainer::const_iterator it=gi->begin(); it!=gi->end();++it)
    {
      MInterfaceElement *ie = dynamic_cast<MInterfaceElement*>(it->second);
      Rm.clear();
      this->getMinusDomain()->getFunctionSpace()->getKeys(ie->getElem(0),Rm);
      Rp.clear();
      this->getPlusDomain()->getFunctionSpace()->getKeys(ie->getElem(1),Rp);
      
      this->computedFmdGM(aips, ie, GP, getMinusDomain(), getPlusDomain(), dUdG, Rm, Rp,useBarF, ws);
    }
  }
  // bulk
  if (g->size() > 0)
  {
    dG3DMaterialLaw *mlaw;
    if(_mlaw->getType() == materialLaw::fracture)
      mlaw = static_cast< dG3DMaterialLaw* >((static_cast< FractureByCohesive3DLaw* > (getMaterialLaw()) )->getBulkLaw());
    else{
      mlaw= static_cast<dG3DMaterialLaw*>(getMaterialLaw());
    }
    bool useBarF=mlaw->getUseBarF();
    for (elementGroup::elementContainer::const_iterator it = g->begin(); it != g->end(); ++it){
      MElement *e = it->second;
      //std::cout << "Elem: " << e->getNum() << std::endl;
      Rm.clear();
      getFunctionSpace()->getKeys(e,Rm);

      this->computedFmdGM(e, GP, aips->getIPstate(e->getNum()), dUdG, Rm, useBarF, ws); 
    }
  }
};


//ling wu // end


void dG3DDomain::computeAllIPStrain(AllIPState *aips,const unknownField *ufield,const IPStateBase::whichState ws, const bool virt){
  IntPt *GP;
  fullVector<double> dispm;
  fullVector<double> dispp;
  std::vector<Dof> R;
  // interface elements
  if (gi->size() >0)
  {
    dG3DMaterialLaw *mlawMinus = static_cast<dG3DMaterialLaw*>(this->getMaterialLawMinus());
    dG3DMaterialLaw *mlawPlus = static_cast<dG3DMaterialLaw*>(this->getMaterialLawPlus());
    dG3DMaterialLaw *mlawbulkm;
    dG3DMaterialLaw *mlawbulkp;
    if(mlawMinus->getType() == materialLaw::fracture)
      mlawbulkm = static_cast< dG3DMaterialLaw* >((static_cast< FractureByCohesive3DLaw* > (mlawMinus) )->getBulkLaw());
    else
      mlawbulkm= static_cast<dG3DMaterialLaw*>(mlawMinus);
    if(mlawPlus->getType() == materialLaw::fracture)
      mlawbulkp = static_cast< dG3DMaterialLaw* >((static_cast< FractureByCohesive3DLaw* > (mlawPlus) )->getBulkLaw());
    else
      mlawbulkp= static_cast<dG3DMaterialLaw*>(mlawPlus);
    if (mlawbulkm->getUseBarF()!=mlawbulkp->getUseBarF()) //check with the bulk law
      Msg::Error("dG3DDomain::computeAllIPStrain All the constitutive laws need to use Fbar or not");
    bool useBarF=mlawbulkm->getUseBarF();
    //
    for(elementGroup::elementContainer::const_iterator it=gi->begin(); it!=gi->end();++it)
    {
      MInterfaceElement *ie = dynamic_cast<MInterfaceElement*>(it->second);
      R.clear();
      this->getMinusDomain()->getFunctionSpace()->getKeys(ie->getElem(0),R);
      dispm.resize(R.size());
      ufield->get(R,dispm);
      R.clear();
      this->getPlusDomain()->getFunctionSpace()->getKeys(ie->getElem(1),R);
      dispp.resize(R.size());
      ufield->get(R,dispp);
      int npts = integBound->getIntPoints(it->second,&GP);
      this->computeStrain(aips,ie, GP,ws,getMinusDomain(),getPlusDomain(),dispm,dispp,virt, useBarF);
    }
  }
  // bulk
  if (g->size() > 0)
  {
    dG3DMaterialLaw *mlaw;
    if(_mlaw->getType() == materialLaw::fracture)
      mlaw = static_cast< dG3DMaterialLaw* >((static_cast< FractureByCohesive3DLaw* > (getMaterialLaw()) )->getBulkLaw());
    else{
      mlaw= static_cast<dG3DMaterialLaw*>(getMaterialLaw());
    }
    bool useBarF=mlaw->getUseBarF();

    //ofstream  outfile;
    //outfile.open("gmsh_sf.txt");
    unsigned int counter = 0;
    for (elementGroup::elementContainer::const_iterator it = g->begin(); it != g->end(); ++it){
      MElement *e = it->second;
      //std::cout << "Elem: " << e->getNum() << std::endl;
      R.clear();
      getFunctionSpace()->getKeys(e,R);
      dispm.resize(R.size());
      ufield->get(R,dispm);
      IntPt *GP;
      int npts_bulk=integBulk->getIntPoints(e,&GP);

      // Test output to debug curlVals and CurlcurlVals
      // sf components of cm3 against gmsh
      // Perform out of sf just once since done for all elem together
      /*while (counter == 0)
      {
          counter++;
          // For all GPs together of single element
          int numedges = e->getNumEdges();
          const int elemType = e->getTypeForMSH();
          const size_t globalNumEdges = GModel::current()->getNumEdges();
          const std::string functionSpaceType1 = "HcurlLegendre";
          //const std::string functionSpaceType2 = "CurlHcurlLegendre";
          int numComponents = 0;
          int numOrientations = 0;

          std::vector<double> localCoord;
          std::vector<double> gmsh_curlVals;
          std::vector<double> transformed_gmshcurlVals;
          //std::vector<double> transformed_gmshCurlcurlVals;
          //std::vector<double> gmsh_CurlcurlVals;
          std::vector<int> basisFunctionsOrientation1, basisFunctionsOrientation2;
          for (unsigned int j = 0; j < npts_bulk; ++j)
          {
              for (unsigned int m = 0; m < 3; ++m)
              {
                  localCoord.push_back(GP[j].pt[m]);
              }
          }
          static int _initialized = 0;
          if (!_initialized)
          {
              gmsh::initialize();
              _initialized = 1;
          }
          std::vector<std::pair<int, int> > entities;
          const int dim = 3;
          gmsh::model::getEntities(entities, dim);
          //std::cout << "Entities size: " << entities.size() << std::endl;
          const int tag = entities[0].second;
          //std::cout << "Tag: " << tag << std::endl;

          // performed for all elements
          gmsh::model::mesh::getBasisFunctionsOrientationForElements(elemType, functionSpaceType1,
                                                                     basisFunctionsOrientation1, tag);

          std::vector<int> uniqueOrientationsNumbering;
          const int numberOfOrientations = gmsh::model::mesh::getNumberOfOrientations(elemType, functionSpaceType1);
          //uniqueOrientationsNumbering.reserve(numberOfOrientations / 20);
          std::pair<int, int> previousPair(-1, -1);
          for (unsigned int i = 0; i < basisFunctionsOrientation1.size(); ++i)
          {
              if (basisFunctionsOrientation1[i] == previousPair.first)
              {
                  basisFunctionsOrientation1[i] = previousPair.second;
              }
              else
              {
                  auto itFind = std::find(uniqueOrientationsNumbering.begin(), uniqueOrientationsNumbering.end(),
                                          basisFunctionsOrientation1[i]);
                  if (itFind == uniqueOrientationsNumbering.end())
                  {
                      uniqueOrientationsNumbering.push_back(basisFunctionsOrientation1[i]);
                      previousPair = std::make_pair(basisFunctionsOrientation1[i],
                                                    uniqueOrientationsNumbering.size() - 1);
                      basisFunctionsOrientation1[i] = previousPair.second;
                  }
                  else
                  {
                      previousPair = std::make_pair(basisFunctionsOrientation1[i],
                                                    itFind - uniqueOrientationsNumbering.begin());
                      basisFunctionsOrientation1[i] = previousPair.second;
                  }
              }
          }

          gmsh::model::mesh::getBasisFunctions(elemType, localCoord, functionSpaceType1, numComponents, gmsh_curlVals,
                                               numOrientations, uniqueOrientationsNumbering);
          //gmsh::model::mesh::getBasisFunctionsOrientationForElements(elemType, functionSpaceType2, basisFunctionsOrientation2);
          //gmsh::model::mesh::getBasisFunctions(elemType,localCoord,functionSpaceType2,numComponents,gmsh_CurlcurlVals,numOrientations,basisFunctionsOrientation2);

          std::vector<double> jacs, dets, coords;
          // performed for all elements
          gmsh::model::mesh::getJacobians(elemType, localCoord, jacs, dets, coords);


          const unsigned  int numElem = g->size();
          double jacMatrix[numElem][npts_bulk][3][3];
          unsigned int count = 0;
          for (unsigned int ele = 0; ele < numElem; ++ele)
          {
              unsigned int offset = 0;
              for (unsigned int gp = 0; gp < npts_bulk; ++gp)
              {
                  jacMatrix[ele][gp][0][0] = jacs[0 + offset+count];
                  jacMatrix[ele][gp][0][1] = jacs[1 + offset+count];
                  jacMatrix[ele][gp][0][2] = jacs[2 + offset+count];
                  jacMatrix[ele][gp][1][0] = jacs[3 + offset+count];
                  jacMatrix[ele][gp][1][1] = jacs[4 + offset+count];
                  jacMatrix[ele][gp][1][2] = jacs[5 + offset+count];
                  jacMatrix[ele][gp][2][0] = jacs[6 + offset+count];
                  jacMatrix[ele][gp][2][1] = jacs[7 + offset+count];
                  jacMatrix[ele][gp][2][2] = jacs[8 + offset+count];

                  offset+=9;
              }
              count = count + (npts_bulk * 9);
          }

          // Jac Transformations

          transformed_gmshcurlVals.resize(numElem * npts_bulk * numedges * 3, 0.0);
          //transformed_gmshCurlcurlVals.resize(gmsh_CurlcurlVals.size(), 0.0);
          unsigned int count2 = 0;
          double invjac[numElem][npts_bulk][3][3];
          double invjacT[numElem][npts_bulk][3][3];
          for (unsigned int ele = 0; ele < numElem; ++ele)
          {
              for (unsigned int gp = 0; gp < npts_bulk; ++gp)
              {
                  //const double invdetJ = 1.0 / dets[gp + count2];
                  inv3x3(jacMatrix[ele][gp], invjac[ele][gp]);
                  for (unsigned int i = 0; i < 3; ++i)
                  {
                      for (unsigned int j = 0; j < 3; ++j)
                      {
                          invjacT[ele][gp][i][j] = invjac[ele][gp][j][i];
                      }
                  }
              }
              count2+=npts_bulk;
          }


          for (unsigned int ele = 0; ele < numElem; ++ele)
          {
              for (unsigned int gp = 0; gp < npts_bulk; ++gp)
              {
                  for (unsigned int edge = 0; edge < numedges; ++edge)
                  {
                      for (unsigned int k = 0; k < 3; ++k)
                      {
                          for (unsigned int l = 0; l < 3; ++l)
                          {
                              transformed_gmshcurlVals[ele * numedges * npts_bulk * 3 + gp * numedges * 3 + edge * 3 + k] += invjacT[ele][gp][k][l] *
                                      gmsh_curlVals[basisFunctionsOrientation1[ele] * numedges * npts_bulk * 3
                                                    + edge * 3 + l];
                          }
                      }
                  }
              }
          }

          // Output to file

          unsigned int counter2 = 0;
          for (elementGroup::elementContainer::const_iterator it2 = g->begin(); it2 != g->end(); ++it2)
          {
              MElement *E = it2->second;
              unsigned int count3 = 0;
              unsigned int count4 = 0;
              for (unsigned int gp = 0; gp < npts_bulk; ++gp)
              {
                  outfile << "Elem:" << E->getNum() << std::endl;
                  outfile << "GP: ";
                  //std::cout << "gmsh GP size: " << localCoord.size()/3 << std::endl;
                  outfile << localCoord[gp + count3] << ", " << localCoord[gp + count3 + 1] << ", " << localCoord[gp + count3 + 2] << std::endl;

                  outfile << "GMSH_curlVals" << "\t \t \t" << "GMSH_CurlcurlVals" << std::endl;
                  //std::cout << "gmsh curlVals size: " << gmsh_curlVals.size()/3 << std::endl;
                  //std::cout << "gmsh CurlcurlVals size: " << gmsh_CurlcurlVals.size()/3 << std::endl;
                  for (unsigned int i = 0; i < (numedges * 3); i = i + 3)
                  {
                      outfile << transformed_gmshcurlVals[i + count4 + counter2] << ", "
                              << transformed_gmshcurlVals[i + count4 + counter2 + 1] << ", "
                              << transformed_gmshcurlVals[i + count4 + counter2 + 2] << std::endl;
                  }
                  outfile << std::endl;

                  count3 += 2;
                  count4 += 18;
              }
              counter2 += 72;
          }
      }*/
      this->computeStrain(e,npts_bulk,GP,aips->getIPstate(e->getNum()),ws,dispm, useBarF);
    }
    //outfile.close();
  }
};


bool dG3DDomain::computeIPVariable(AllIPState *aips,const unknownField *ufield, const IPStateBase::whichState ws, bool stiff)
{
  _evalStiff = false; // directly access to computeIPv is for matrix by perturbation otherwise compute ipvarible thanks to this function
  if (ws == IPStateBase::initial)
  {
    this->initialIPVariable(aips,ufield,ws,stiff);
  }
  else
  {
    #if defined(HAVE_MPI)
    if ((this->getMaterialLawMinus()->isNumeric() or this->getMaterialLawPlus()->isNumeric()) and this->_otherRanks.size()>0)
    {
      bool ok = this->computeIPVariableMPI(aips,ufield,ws,stiff);
      return ok;
    }
    else
    #endif //HAVE_MPI
    {
      this->computeAllIPStrain(aips,ufield,ws,false);
      IntPt *GP;
      double uem,vem,uep,vep;
      fullVector<double> dispm;
      fullVector<double> dispp;
      std::vector<Dof> R;
      // interface elements
      
      for(elementGroup::elementContainer::const_iterator it=gi->begin(); it!=gi->end();++it)
      {
        MInterfaceElement *ie = dynamic_cast<MInterfaceElement*>(it->second);
        R.clear();
        this->getMinusDomain()->getFunctionSpace()->getKeys(ie->getElem(0),R);
        dispm.resize(R.size());
        ufield->get(R,dispm);
        R.clear();
        this->getPlusDomain()->getFunctionSpace()->getKeys(ie->getElem(1),R);
        dispp.resize(R.size());
        ufield->get(R,dispp);
        int npts = integBound->getIntPoints(it->second,&GP);
        bool success = this->computeIpv(aips,ie, GP,ws,getMinusDomain(),getPlusDomain(),getMaterialLawMinus(),getMaterialLawPlus(),dispm,dispp,false,stiff);
        if (!success)
        {
          return false;
        }
      }
      // bulk
      for (elementGroup::elementContainer::const_iterator it = g->begin(); it != g->end(); ++it)
      {
        MElement *e = it->second;
        R.clear();
        getFunctionSpace()->getKeys(e,R);
        dispm.resize(R.size());
        ufield->get(R,dispm);
        bool success = this->computeIpv(aips,e,ws,getMaterialLaw(),dispm,stiff);
        if (!success)
        {
          return false;
        }
      }

    }
  }
  _evalStiff = true;
  return true;
};

void dG3DDomain::computeStrain(MElement *e, const int npts_bulk, IntPt *GP,
                               AllIPState::ipstateElementContainer *vips,
                               IPStateBase::whichState ws,
                               fullVector<double> &disp, bool useBarF) const
{
  if (!getElementErosionFilter()(e)) return;
  const nlsFunctionSpace<double>* nlspace  = dynamic_cast<const nlsFunctionSpace<double>*>(getFunctionSpace());
  //Msg::Info("nb bulk gp %d",npts_bulk);
    if(evaluateMecaField) {
        for (int j = 0; j < npts_bulk; j++) {
            IPStateBase *ips = (*vips)[j];
            dG3DIPVariableBase *ipv = static_cast<dG3DIPVariableBase *>(ips->getState(ws));
            // get grad of shape function from Gauss point
            const std::vector<TensorialTraits<double>::ValType> &Vals = ipv->f(getFunctionSpace(), e, GP[j]);
            const std::vector<TensorialTraits<double>::GradType> &Grads = ipv->gradf(getFunctionSpace(), e, GP[j]);
            //x y z
            STensor3 &F = ipv->getRefToDeformationGradient();
            STensorOperation::unity(F);

            for (int k = 0; k < 3; k++) {
                int nbFF = nlspace->getNumShapeFunctions(e, k);
                int nbFFTotalLast = nlspace->getShapeFunctionsIndex(e, k);
                for (int i = 0; i < nbFF; i++) {
                    //x y z: take compoenent x arbitrarily
                    // xx yy zz xy xz yz
                    F(k, 0) += Grads[i + nbFFTotalLast][0] * disp(i + nbFFTotalLast);
                    F(k, 1) += Grads[i + nbFFTotalLast][1] * disp(i + nbFFTotalLast);
                    F(k, 2) += Grads[i + nbFFTotalLast][2] * disp(i + nbFFTotalLast);
                }
            }
            #ifdef _DEBUG
            if (STensorOperation::determinantSTensor3(F) < 1.e-15)
                Msg::Error("Negative Jacobian in ele %d", e->getNum());
            #endif //_DEBUG
        }

        if (useBarF) {
            double barJ = 0.;
            double totalWeight = 0.;
            for (int j = 0; j < npts_bulk; j++) {
                IPStateBase *ips = (*vips)[j];
                dG3DIPVariableBase *ipv = static_cast<dG3DIPVariableBase *>(ips->getState(ws));
                double J = ipv->getJ(_dim); // we assume the domain is in the Oxy plane;
                ipv->getRefToLocalJ() = J;
                double weight = GP[j].weight;
                barJ += J * weight;
                totalWeight += weight;
            }
            barJ /= totalWeight;
            for (int j = 0; j < npts_bulk; j++) {
                IPStateBase *ips = (*vips)[j];
                dG3DIPVariableBase *ipv = static_cast<dG3DIPVariableBase *>(ips->getState(ws));
                double localJ = ipv->getLocalJ();
                ipv->getRefToBarJ() = barJ;
                STensor3 &barF = ipv->getRefToDeformationGradient();
                for(int k=0; k<_dim; k++)
                  for(int l=0; l<_dim; l++)
                    barF(k,l) *= pow(barJ / localJ, 1. / ((double)(_dim)));
		if (STensorOperation::isnan(barF) && _dim ==2)
			Msg::Error("The domain should be in the Oxy plane");
	    }
        }
    }
  /**
   * NONLOCAL FIELD
   * 
   * */
  if (getNumNonLocalVariable() > 0)
  {
    for(int j=0;j<npts_bulk;j++)
    {
      IPStateBase* ips = (*vips)[j];

      dG3DIPVariableBase *ipv = static_cast<dG3DIPVariableBase*>(ips->getState(ws));
      // get grad of shape function from Gauss point
      const std::vector<TensorialTraits<double>::ValType> &Vals = ipv->f(getFunctionSpace(),e,GP[j]);
      const std::vector<TensorialTraits<double>::GradType> &Grads = ipv->gradf(getFunctionSpace(),e,GP[j]);

      if( ipv->getNumberNonLocalVariable()>getNumNonLocalVariable() )
        Msg::Error("Your material law uses more non local variables than your domain dG3DDomain::computeStrain bulk");
      
      for (int nl = 0; nl < ipv->getNumberNonLocalVariable(); nl++) {
          if (evaluateNonLocalField[nl]) {
              int nbFF = nlspace->getNumShapeFunctions(e, 3 + nl);
              int nbFFTotalLast = nlspace->getShapeFunctionsIndex(e, 3 + nl);

              double &nonlocalVar = ipv->getRefToNonLocalVariable(nl);
              SVector3 &gradNonlocalVar = ipv->getRefToGradNonLocalVariable()[nl];

              nonlocalVar = 0.;
              STensorOperation::zero(gradNonlocalVar);
              for (int i = 0; i < nbFF; i++) {
                  nonlocalVar += Vals[i + nbFFTotalLast] * disp(i + nbFFTotalLast);
                  gradNonlocalVar(0) += Grads[i + nbFFTotalLast][0] * disp(i + nbFFTotalLast);
                  gradNonlocalVar(1) += Grads[i + nbFFTotalLast][1] * disp(i + nbFFTotalLast);
                  gradNonlocalVar(2) += Grads[i + nbFFTotalLast][2] * disp(i + nbFFTotalLast);
              }
          }
      }
    }
    
    // compute nonlocal var for law
    for(int j=0;j<npts_bulk;j++){
      IPStateBase* ips = (*vips)[j];
      dG3DIPVariableBase *ipv = static_cast<dG3DIPVariableBase*>(ips->getState(ws));
      for (int nl = 0; nl < ipv->getNumberNonLocalVariable(); nl++)
      {
          if (evaluateNonLocalField[nl])
              ipv->getRefToNonLocalVariableInMaterialLaw(nl) = ipv->getConstRefToNonLocalVariable(nl);
      }
    }
  }
  
  /*
   * 
   * EXTRA FIELD
   * 
   * */
  if (getNumConstitutiveExtraDofDiffusionVariable() > 0)
  {
    // porition from last
    for(int j=0;j<npts_bulk;j++)
    {
      IPStateBase* ips = (*vips)[j];
      dG3DIPVariableBase *ipv = static_cast<dG3DIPVariableBase*>(ips->getState(ws));
      // get grad of shape function from Gauss point
      const std::vector<TensorialTraits<double>::ValType> &Vals = ipv->f(getFunctionSpace(),e,GP[j]);
      const std::vector<TensorialTraits<double>::GradType> &Grads = ipv->gradf(getFunctionSpace(),e,GP[j]);
      
      if( ipv->getNumConstitutiveExtraDofDiffusionVariable()>getNumConstitutiveExtraDofDiffusionVariable() )
        Msg::Error("Your material law uses more constitutive extra dof diffusion variables than your domain dG3DDomain::computeStrain bulk");
        
      if((getNumConstitutiveExtraDofDiffusionVariable() == 2)&&(getConstitutiveExtraDofDiffusionUseEnergyConjugatedField()))
      {
        // eletro-thermomec, if fV, fT are used
        // if two fields (T, V) are considered using fT= 1/T, fV = -V/T as unknowns
        for (int extraDOFField = 0; extraDOFField < ipv->getNumConstitutiveExtraDofDiffusionVariable(); extraDOFField++)
        {
            if(evaluateExtraDofDiffusionField[extraDOFField]) {
                // unknown
                double &energyConjugatedField = ipv->getRefToEnergyConjugatedField(extraDOFField);
                SVector3 &gradEnergyConjugatedField = ipv->getRefToGradEnergyConjugatedField()[extraDOFField];
                energyConjugatedField = 0.;
                STensorOperation::zero(gradEnergyConjugatedField);

                int indexFieldfT = 3 + getNumNonLocalVariable() + extraDOFField;
                int nbFF = nlspace->getNumShapeFunctions(e, indexFieldfT);
                int nbFFTotalLast = nlspace->getShapeFunctionsIndex(e, indexFieldfT);
                for (int i = 0; i < nbFF; i++) {
                    const double &ftemp = disp(i + nbFFTotalLast);
                    energyConjugatedField += Vals[i + nbFFTotalLast] * ftemp;
                    gradEnergyConjugatedField(0) += Grads[i + nbFFTotalLast][0] * ftemp;
                    gradEnergyConjugatedField(1) += Grads[i + nbFFTotalLast][1] * ftemp;
                    gradEnergyConjugatedField(2) += Grads[i + nbFFTotalLast][2] * ftemp;
                }

                // field and grad field must be estimated from unknowns
                double &T = ipv->getRefToField(extraDOFField);
                SVector3 &gradT = ipv->getRefToGradField()[extraDOFField];

                int extraDOFField_aux = (extraDOFField == 0) ? 1 : 0;
                // derivatives of field and grad with with respect to conjugate fields and their gradients
                double &dFielddEnergyConjugatedField = ipv->getRefTodFielddEnergyConjugatedField()[extraDOFField][extraDOFField];
                double &dFielddEnergyConjugatedField2 = ipv->getRefTodFielddEnergyConjugatedField()[extraDOFField][extraDOFField_aux];
                SVector3 &dGradFielddEnergyConjugatedField = ipv->getRefTodGradFielddEnergyConjugatedField()[extraDOFField][extraDOFField];
                SVector3 &dGradFielddEnergyConjugatedField2 = ipv->getRefTodGradFielddEnergyConjugatedField()[extraDOFField][extraDOFField_aux];
                STensor3 &dGradFielddGradEnergyConjugatedField = ipv->getRefTodGradFielddGradEnergyConjugatedField()[extraDOFField][extraDOFField];
                STensor3 &dGradFielddGradEnergyConjugatedField2 = ipv->getRefTodGradFielddGradEnergyConjugatedField()[extraDOFField][extraDOFField_aux];

                if (extraDOFField == 0) {
                    // temperature field
                    T = 1 / (energyConjugatedField);
                    gradT(0) = -1 / (pow(energyConjugatedField, 2)) * (gradEnergyConjugatedField(0));
                    gradT(1) = -1 / (pow(energyConjugatedField, 2)) * (gradEnergyConjugatedField(1));
                    gradT(2) = -1 / (pow(energyConjugatedField, 2)) * (gradEnergyConjugatedField(2));

                    dFielddEnergyConjugatedField = -1. / (energyConjugatedField) / (energyConjugatedField);
                    dFielddEnergyConjugatedField2 = 0.;

                    dGradFielddEnergyConjugatedField(0) =
                            2 / (energyConjugatedField) / (energyConjugatedField) / (energyConjugatedField) *
                            (gradEnergyConjugatedField(0));
                    dGradFielddEnergyConjugatedField(1) =
                            2 / (energyConjugatedField) / (energyConjugatedField) / (energyConjugatedField) *
                            (gradEnergyConjugatedField(1));
                    dGradFielddEnergyConjugatedField(2) =
                            2 / (energyConjugatedField) / (energyConjugatedField) / (energyConjugatedField) *
                            (gradEnergyConjugatedField(2));

                    dGradFielddEnergyConjugatedField2(0) = 0.;
                    dGradFielddEnergyConjugatedField2(1) = 0.;
                    dGradFielddEnergyConjugatedField2(2) = 0.;

                    STensorOperation::diag(dGradFielddGradEnergyConjugatedField,
                                           -1. / (energyConjugatedField) / (energyConjugatedField));
                    STensorOperation::zero(dGradFielddGradEnergyConjugatedField2);
                } else {
                    // volteage field
                    double fT = ipv->getConstRefToEnergyConjugatedField(0);
                    SVector3 gradfT = ipv->getConstRefToGradEnergyConjugatedField()[0];

                    T = -1 * (energyConjugatedField) / (fT);
                    gradT(0) = -1 / (fT) * (gradEnergyConjugatedField(0)) +
                               (energyConjugatedField) / (pow(fT, 2)) * (gradfT(0));
                    gradT(1) = -1 / (fT) * (gradEnergyConjugatedField(1)) +
                               (energyConjugatedField) / (pow(fT, 2)) * (gradfT(1));
                    gradT(2) = -1 / (fT) * (gradEnergyConjugatedField(2)) +
                               (energyConjugatedField) / (pow(fT, 2)) * (gradfT(2));

                    dFielddEnergyConjugatedField = -1. / (fT);
                    dFielddEnergyConjugatedField2 = (energyConjugatedField) / (fT) / (fT);

                    dGradFielddEnergyConjugatedField(0) = 1. / (fT) / (fT) * (gradfT(0));
                    dGradFielddEnergyConjugatedField(1) = 1. / (fT) / (fT) * (gradfT(1));
                    dGradFielddEnergyConjugatedField(2) = 1. / (fT) / (fT) * (gradfT(2));

                    dGradFielddEnergyConjugatedField2(0) = 1. / (fT) / (fT) * (gradEnergyConjugatedField(0)) -
                                                           2 * (energyConjugatedField) / (fT) / (fT) / (fT) *
                                                           (gradfT(0));
                    dGradFielddEnergyConjugatedField2(1) = 1. / (fT) / (fT) * (gradEnergyConjugatedField(1)) -
                                                           2 * (energyConjugatedField) / (fT) / (fT) / (fT) *
                                                           (gradfT(1));
                    dGradFielddEnergyConjugatedField2(2) = 1. / (fT) / (fT) * (gradEnergyConjugatedField(2)) -
                                                           2 * (energyConjugatedField) / (fT) / (fT) / (fT) *
                                                           (gradfT(2));

                    STensorOperation::diag(dGradFielddGradEnergyConjugatedField, -1. / (fT));
                    STensorOperation::diag(dGradFielddGradEnergyConjugatedField2,
                                           (energyConjugatedField) / (fT) / (fT));
                }
            }
          
        }
      }
      else
      {
        // field are unknowns
        for (int extraDOFField = 0; extraDOFField < ipv->getNumConstitutiveExtraDofDiffusionVariable(); extraDOFField++) {
            if (evaluateExtraDofDiffusionField[extraDOFField]) {
                double &T = ipv->getRefToField(extraDOFField);
                SVector3 &gradT = ipv->getRefToGradField()[extraDOFField];
                T = 0.;
                STensorOperation::zero(gradT);

                int indexFieldfT = 3 + getNumNonLocalVariable() + extraDOFField;
                int nbFF = nlspace->getNumShapeFunctions(e, indexFieldfT);
                int nbFFTotalLast = nlspace->getShapeFunctionsIndex(e, indexFieldfT);
                for (int i = 0; i < nbFF; i++) {
                    T += Vals[i + nbFFTotalLast] * disp(i + nbFFTotalLast);
                    gradT(0) += Grads[i + nbFFTotalLast][0] * disp(i + nbFFTotalLast);
                    gradT(1) += Grads[i + nbFFTotalLast][1] * disp(i + nbFFTotalLast);
                    gradT(2) += Grads[i + nbFFTotalLast][2] * disp(i + nbFFTotalLast);
                }
            }
        }
      }
    }
  }
}
bool dG3DDomain::computeIpv(AllIPState *aips,MElement *e, IPStateBase::whichState ws,
                                        materialLaw *mlaw__,fullVector<double> &disp, bool stiff)
{
  if (!getElementErosionFilter()(e)) return true;

  dG3DMaterialLaw *mlaw;
  AllIPState::ipstateElementContainer *vips = aips->getIPstate(e->getNum());
  IntPt *GP;
  int npts_bulk=integBulk->getIntPoints(e,&GP);

  if(mlaw__->getType() == materialLaw::fracture)
    mlaw = static_cast< dG3DMaterialLaw* >((static_cast< FractureByCohesive3DLaw* > (mlaw__) )->getBulkLaw());
  else{
    mlaw= static_cast<dG3DMaterialLaw*>(mlaw__);
  }
  bool useBarF=mlaw->getUseBarF();
  bool needDTangent=false;
  BodyForceModuliType  ComputeBodyForceModuli = useWhichModuliForBF();
  
  if (hasBodyForceForHO() and ComputeBodyForceModuli == Present){
      needDTangent= true;
  } 
         
  if(_evalStiff) { this->computeStrain(e,npts_bulk,GP,vips,ws,disp,useBarF);};
  for(int j=0;j<npts_bulk;j++)
  {
    // get dgipv from ipstate
    IPStateBase* ips = (*vips)[j];
    dG3DIPVariableBase *ipv = static_cast<dG3DIPVariableBase*>(ips->getState(ws));
    const dG3DIPVariableBase *ipvprev = static_cast<const dG3DIPVariableBase*>(ips->getState(IPStateBase::previous));

    if ((_subSteppingMethod == 0) or (_subSteppingMethod == 1))
    {
      if (_planeStressState)
          mlaw->stress3DTo2D(ipv,ipvprev,stiff,false,needDTangent);
      else
      	mlaw->stress(ipv,ipvprev, stiff,true,needDTangent);
      	
      if(hasBodyForceForHO()){
        if(ComputeBodyForceModuli == Present){  
          ipv->setValueForBodyForce(ipv->getConstRefToTangentModuli(), ComputeBodyForceModuli); 
        }
        else if(ComputeBodyForceModuli == Previous){
          ipv->setValueForBodyForce(ipvprev->getConstRefToTangentModuli(),ComputeBodyForceModuli);  
        }
        else if(ComputeBodyForceModuli == Homogeneous){
          ipv->setValueForBodyForce(getHomogeneousTangent(),ComputeBodyForceModuli); 
        }
        else{ Msg::Error("Moduli used to compute body force is not defined");}        
      }   	
    }
    else if (_subSteppingMethod == 2)
    {
      // get
      dG3DIPVariableBase* dgipv = static_cast<dG3DIPVariableBase*>(ipv);
      const dG3DIPVariableBase* dgipvprev = static_cast<const dG3DIPVariableBase*>(ipvprev);

      // create a temporary ipv
      IPVariable* ipvTemp =  dgipvprev->clone();
      dG3DIPVariableBase* dgipvTemp = dynamic_cast<dG3DIPVariableBase*>(ipvTemp);
      if (dgipvTemp == NULL) Msg::Error("dG3DIPVariableBase::clone is not correctly defined");

      // compute dF
      static STensor3 dF;
      dF  = dgipv->getConstRefToDeformationGradient();
      dF -= dgipvprev->getConstRefToDeformationGradient();
      // compute dNLV if nonlocal damage is used
      fullVector<double> dnonlocalVar;
      int numNL = dgipv->getNumberNonLocalVariable();
      if (numNL > 0){
        dnonlocalVar.resize(numNL);
        for (int iv = 0; iv < numNL; iv++){
          dnonlocalVar(iv) = (dgipv->getConstRefToNonLocalVariableInMaterialLaw(iv) - dgipvprev->getConstRefToNonLocalVariableInMaterialLaw(iv));
        }
      }

      int iter = 1;
      while (1){
        // update kinematics
        double time = double(iter)/(double(_subStepNumber));
        //Msg::Info("step %d, time = %f",iter,time);
        STensor3& F = dgipv->getRefToDeformationGradient();
        F = dgipvprev->getConstRefToDeformationGradient();
        F.daxpy(dF,time);

        for (int iv=0; iv < numNL; iv++){
          dgipv->getRefToNonLocalVariableInMaterialLaw(iv) = dgipvprev->getConstRefToNonLocalVariableInMaterialLaw(iv);
          dgipv->getRefToNonLocalVariableInMaterialLaw(iv) += (time*dnonlocalVar(iv));
        }

        if (iter == _subStepNumber){
          // in last substep
          if (_planeStressState)
            mlaw->stress3DTo2D(ipv,ipvTemp,stiff,false,needDTangent);
          else
            mlaw->stress(ipv,ipvTemp,stiff,true,needDTangent);
          break;
        }
        else
        {
          if (_planeStressState)
            mlaw->stress3DTo2D(ipv,ipvTemp,false,false,needDTangent);
          else
            mlaw->stress(ipv,ipvTemp,false,needDTangent);
          dgipvTemp->operator=(*(dynamic_cast<const IPVariable*>(ipv)));
        }
        // next step
        iter++;
      }
      if(hasBodyForceForHO()){
        if(ComputeBodyForceModuli == Present){  
          ipv->setValueForBodyForce(ipv->getConstRefToTangentModuli(), ComputeBodyForceModuli); 
        }
        else if(ComputeBodyForceModuli == Previous){
          ipv->setValueForBodyForce(ipvTemp->getConstRefToTangentModuli(),ComputeBodyForceModuli);  
        }
        else if(ComputeBodyForceModuli == Homogeneous){
          ipv->setValueForBodyForce(getHomogeneousTangent(),ComputeBodyForceModuli); 
        }
        else{ Msg::Error("Moduli used to compute body force is not defined");}        
      } 
                
      if (ipvTemp!=NULL) delete ipvTemp;
    }
    else
    {
      Msg::Error("This substepping method %d has not been implemented",_subSteppingMethod);
    }
    
    if (!ipv->getConstitutiveSuccessFlag())
    {
      Msg::Error("constitive estimation is failed at ele %d GP %d",e->getNum(), j);
      return false;
    }
  }
  return true;
}

void dG3DDomain::setValuesForBodyForce(AllIPState *aips, const IPStateBase::whichState ws){
  IntPt *GP;
  BodyForceModuliType  ComputeBodyForceModuli = useWhichModuliForBF();
  // interface elements
  if (gi->size() >0)
  {
    for(elementGroup::elementContainer::const_iterator it=gi->begin(); it!=gi->end();++it)
    {
      MInterfaceElement *ie = dynamic_cast<MInterfaceElement*>(it->second);
      MElement *ele = dynamic_cast<MElement*>(ie);
  
      // do nothing if element is already eliminated
      if (getElementErosionFilter()(ele)){
         int npts=integBound->getIntPoints(ele,&GP);   
         AllIPState::ipstateElementContainer *vips = aips->getIPstate(ele->getNum());
         for(int j=0;j<npts;j++) {    
            IPStateBase *ipsm = (*vips)[j];
            IPStateBase *ipsp = (*vips)[j + npts];

            dG3DIPVariableBase *ipvm = static_cast<dG3DIPVariableBase *>(ipsm->getState(ws));
            dG3DIPVariableBase *ipvp = static_cast<dG3DIPVariableBase *>(ipsp->getState(ws));
            
            if(ComputeBodyForceModuli == Present){  
              ipvp->setValueForBodyForce(ipvp->getConstRefToTangentModuli(),ComputeBodyForceModuli);
              ipvm->setValueForBodyForce(ipvm->getConstRefToTangentModuli(),ComputeBodyForceModuli); 
            }
            else if(ComputeBodyForceModuli == Previous){
              ipvp->setValueForBodyForce(ipsp->getState(IPStateBase::previous)->getConstRefToTangentModuli(),ComputeBodyForceModuli);
              ipvm->setValueForBodyForce(ipsm->getState(IPStateBase::previous)->getConstRefToTangentModuli(),ComputeBodyForceModuli);  
            }
            else if(ComputeBodyForceModuli == Homogeneous){
              ipvp->setValueForBodyForce(getHomogeneousTangent(),ComputeBodyForceModuli); 
              ipvm->setValueForBodyForce(getHomogeneousTangent(),ComputeBodyForceModuli); 
            }
            else{ Msg::Error("Moduli used to compute body force is not defined");} 
         }
       }
     }
   }  
  // bulk
  if (g->size() > 0)
  {
    for (elementGroup::elementContainer::const_iterator it = g->begin(); it != g->end(); ++it){
      MElement *e = it->second;
      if (getElementErosionFilter()(e)){
         AllIPState::ipstateElementContainer *vips = aips->getIPstate(e->getNum());
         int npts_bulk=integBulk->getIntPoints(e,&GP);
         for(int j=0;j<npts_bulk;j++) {
            // get dgipv from ipstate
            IPStateBase* ips = (*vips)[j];
            dG3DIPVariableBase *ipv = static_cast<dG3DIPVariableBase*>(ips->getState(ws));
            if(ComputeBodyForceModuli == Present){  
              ipv->setValueForBodyForce(ipv->getConstRefToTangentModuli(),ComputeBodyForceModuli);
            }
            else if(ComputeBodyForceModuli == Previous){
              ipv->setValueForBodyForce(ips->getState(IPStateBase::previous)->getConstRefToTangentModuli(),ComputeBodyForceModuli);
            }
            else if(ComputeBodyForceModuli == Homogeneous){
              ipv->setValueForBodyForce(getHomogeneousTangent(),ComputeBodyForceModuli); 
            }
            else{ Msg::Error("Moduli used to compute body force is not defined");} 
         } 
       }
     }
  }
};


void dG3DDomain::computeStrain(AllIPState *aips,MInterfaceElement *ie, IntPt *GP,const IPStateBase::whichState ws,
                                        partDomain* efMinus, partDomain *efPlus, fullVector<double> &dispm,
                                        fullVector<double> &dispp, const bool virt,
                                        bool useBarF)
{
  MElement *ele = dynamic_cast<MElement*>(ie);
  
  // do nothing if element is already eliminated
  if (!getElementErosionFilter()(ele)) return;

  if(!virt)
  {
    nlsFunctionSpace<double>* _spaceminus = dynamic_cast<nlsFunctionSpace<double>*>(efMinus->getFunctionSpace());
    nlsFunctionSpace<double>* _spaceplus = dynamic_cast<nlsFunctionSpace<double>*>(efPlus->getFunctionSpace());
    
    int npts=integBound->getIntPoints(ele,&GP);
    MElement *em = ie->getElem(0);
    MElement *ep = ie->getElem(1);

    // Get Gauss points values on minus and plus elements
    IntPt *GPm; IntPt *GPp;
    _interQuad->getIntPoints(ie,GP,&GPm,&GPp);
    
    // based on elements,
    std::vector<GaussPointSpaceValues<double>*> vgps;
    _spaceminus->get(ele,npts,GP,vgps);

    AllIPState::ipstateElementContainer *vips = aips->getIPstate(ele->getNum());
    /**
     * 
     * CURRENT AND REFERENCE NORMAL
     * 
     * **/
     
    // compute current interface based on the average displaceemnt of the node at interface    
    int nbvertexInter = ele->getNumVertices();
    std::vector<int> vnlist, vplist;
    vnlist.resize(nbvertexInter);
    ie->getLocalVertexNum(0,vnlist);
    vplist.resize(nbvertexInter);
    ie->getLocalVertexNum(1,vplist);
     
    fullVector<double> dispinter(3*nbvertexInter);
    dispinter.setAll(0.);
    for (int iv=0; iv < nbvertexInter; iv++)
    {
      double um, vm, wm;
      double up, vp, wp;
      em->getNode(vnlist[iv],um,vm,wm);
      ep->getNode(vplist[iv],up,vp,wp);
      
      std::vector<TensorialTraits<double>::ValType> Valm, Valp;
      _spaceminus->f(em,um,vm,wm,Valm);
      _spaceplus->f(ep,up,vp,wp,Valp);
      
      for (int i=0; i<3; i++)
      {
        int nbFFm = _spaceminus->getNumShapeFunctions(em,i);
        int nbFFmLastTotal = _spaceminus->getShapeFunctionsIndex(em,i);
        for (int k=0; k< nbFFm; k++)
        {
          dispinter(iv+i*nbvertexInter) += 0.5*Valm[k+nbFFmLastTotal]*dispm(k+nbFFmLastTotal);
        }
        
        int nbFFp = _spaceplus->getNumShapeFunctions(ep,i);
        int nbFFpLastTotal = _spaceplus->getShapeFunctionsIndex(ep,i);
        for (int k=0; k< nbFFp; k++)
        {
          dispinter(iv+i*nbvertexInter) += 0.5*Valp[k+nbFFpLastTotal]*dispp(k+nbFFpLastTotal);
        }
      }
    }
   
    for(int j=0;j<npts;j++)
    {
      IPStateBase* ipsm = (*vips)[j];
      IPStateBase* ipsp = (*vips)[j+npts];

      dG3DIPVariableBase* ipvm = static_cast<dG3DIPVariableBase*>(ipsm->getState(ws));
      dG3DIPVariableBase* ipvp = static_cast<dG3DIPVariableBase*>(ipsp->getState(ws));

      const std::vector<TensorialTraits<double>::GradType> &Grads = vgps[j]->_vgrads;
      // minus outward normal
      static SVector3 referencePhi0[2];
      static SVector3 currentPhi0[2];

      // zero vector first
      STensorOperation::zero(referencePhi0[0]);
      STensorOperation::zero(referencePhi0[1]);
      STensorOperation::zero(currentPhi0[0]);
      STensorOperation::zero(currentPhi0[1]);

      if (efMinus->getDim() == 3){
        for(int k=0;k<nbvertexInter;k++)
        {
          double x = ele->getVertex(k)->x();
          double y = ele->getVertex(k)->y();
          double z = ele->getVertex(k)->z();
          //evaulate displacements at interface
          for(int i=0;i<2;i++)
          {
           referencePhi0[i](0) += Grads[k](i)*x;
           referencePhi0[i](1) += Grads[k+nbvertexInter](i)*y;
           referencePhi0[i](2) += Grads[k+nbvertexInter+nbvertexInter](i)*z;
           currentPhi0[i](0) += Grads[k](i)*(x+dispinter(k));
           currentPhi0[i](1) += Grads[k+nbvertexInter](i)*(y+dispinter(k+nbvertexInter));
           currentPhi0[i](2) += Grads[k+nbvertexInter+nbvertexInter](i)*(z+dispinter(k+2*nbvertexInter));
          }
        }
      }
      else if (efMinus->getDim() == 2){
        for(int k=0;k<nbvertexInter;k++)
        {
          double x = ele->getVertex(k)->x();
          double y = ele->getVertex(k)->y();
          double z = ele->getVertex(k)->z();
           referencePhi0[0](0) += Grads[k](0)*x;
           referencePhi0[0](1) += Grads[k+nbvertexInter](0)*y;
           referencePhi0[0](2) += Grads[k+nbvertexInter+nbvertexInter](0)*z;
           currentPhi0[0](0) += Grads[k](0)*(x+dispinter(k));
           currentPhi0[0](1) += Grads[k+nbvertexInter](0)*(y+dispinter(k+nbvertexInter));
           currentPhi0[0](2) += Grads[k+nbvertexInter+nbvertexInter](0)*(z+dispinter(k+2*nbvertexInter));
        }

        static SVector3 phi0[2];
        static SVector3 phi0Cur[2];

        STensorOperation::zero(phi0[0]);
        STensorOperation::zero(phi0[1]);

        STensorOperation::zero(phi0Cur[0]);
        STensorOperation::zero(phi0Cur[1]);


        std::vector<TensorialTraits<double>::GradType> gradmUVW;
        _spaceminus->gradfuvw(em,GPm[j].pt[0],GPm[j].pt[1],GPm[j].pt[2],gradmUVW);
        int nbFFm = em->getNumVertices();
        for(int k=0;k<nbFFm;k++)
        {
          double x = em->getVertex(k)->x();
          double y = em->getVertex(k)->y();
          double z = em->getVertex(k)->z();
          //evaulate displacements at interface
          for(int i=0;i<2;i++)
          {
           phi0[i](0) += gradmUVW[k](i)*x;
           phi0[i](1) += gradmUVW[k+nbFFm](i)*y;
           phi0[i](2) += gradmUVW[k+nbFFm+nbFFm](i)*z;
           phi0Cur[i](0) += gradmUVW[k](i)*x;
           phi0Cur[i](1) += gradmUVW[k+nbFFm](i)*y;
           phi0Cur[i](2) += gradmUVW[k+2*nbFFm](i)*z;
          }
        }

        currentPhi0[1] = crossprod(phi0Cur[0],phi0Cur[1]);
        currentPhi0[1].normalize();
        referencePhi0[1] = crossprod(phi0[0],phi0[1]);
        referencePhi0[1].normalize();
      }


      (ipvm->getRefToCurrentOutwardNormal())   = crossprod(currentPhi0[0],currentPhi0[1]);
      (ipvm->getRefToReferenceOutwardNormal()) = crossprod(referencePhi0[0],referencePhi0[1]);

      (ipvp->getRefToCurrentOutwardNormal())   = crossprod(currentPhi0[0],currentPhi0[1]);
      (ipvp->getRefToReferenceOutwardNormal()) = crossprod(referencePhi0[0],referencePhi0[1]);
      
      
      // basis for cohesive
      FractureCohesive3DIPVariable *ipvfm = dynamic_cast<FractureCohesive3DIPVariable*>(ipvm);
      FractureCohesive3DIPVariable *ipvfp = dynamic_cast<FractureCohesive3DIPVariable*>(ipvp);

      if((ipvfm!=NULL) and (ipvfp!= NULL) )
      {

        Cohesive3DIPVariableBase* cohipvfm = ipvfm->getIPvFrac();
        Cohesive3DIPVariableBase* cohipvfp = ipvfp->getIPvFrac();
        
        if (cohipvfm->useLocalBasis()){
          // for cohesiev band model
          cohipvfm->getRefToCohesiveNormal() =ipvm->getConstRefToCurrentOutwardNormal();
          cohipvfm->getRefToCohesiveTangent() = currentPhi0[0];
          cohipvfm->getRefToCohesiveBiTangent() = crossprod(cohipvfm->getConstRefToCohesiveNormal(),currentPhi0[0]);

          cohipvfm->getRefToCohesiveNormal().normalize();
          cohipvfm->getRefToCohesiveTangent().normalize();
          cohipvfm->getRefToCohesiveBiTangent().normalize();

          cohipvfm->getRefToCohesiveReferenceNormal() =ipvm->getConstRefToReferenceOutwardNormal();
          cohipvfm->getRefToCohesiveReferenceTangent() = referencePhi0[0];
          cohipvfm->getRefToCohesiveReferenceBiTangent() = crossprod(cohipvfm->getConstRefToCohesiveReferenceNormal(),referencePhi0[0]);

          cohipvfm->getRefToCohesiveReferenceNormal().normalize();
          cohipvfm->getRefToCohesiveReferenceTangent().normalize();
          cohipvfm->getRefToCohesiveReferenceBiTangent().normalize();
        }

        if (cohipvfp->useLocalBasis()){
          cohipvfp->getRefToCohesiveNormal() =ipvp->getConstRefToCurrentOutwardNormal();
          cohipvfp->getRefToCohesiveTangent() = currentPhi0[0];
          cohipvfp->getRefToCohesiveBiTangent() = crossprod(cohipvfp->getConstRefToCohesiveNormal(),currentPhi0[0]);

          cohipvfp->getRefToCohesiveNormal().normalize();
          cohipvfp->getRefToCohesiveTangent().normalize();
          cohipvfp->getRefToCohesiveBiTangent().normalize();

          cohipvfp->getRefToCohesiveReferenceNormal() =ipvp->getConstRefToReferenceOutwardNormal();
          cohipvfp->getRefToCohesiveReferenceTangent() = referencePhi0[0];
          cohipvfp->getRefToCohesiveReferenceBiTangent() = crossprod(cohipvfp->getConstRefToCohesiveReferenceNormal(),referencePhi0[0]);

          cohipvfp->getRefToCohesiveReferenceNormal().normalize();
          cohipvfp->getRefToCohesiveReferenceTangent().normalize();
          cohipvfp->getRefToCohesiveReferenceBiTangent().normalize();
        }
      }
    }
     
    /*
     * DIPLACEMENT FIELD
     * */
    for(int j=0;j<npts;j++) {
        if (evaluateMecaField) {
            IPStateBase *ipsm = (*vips)[j];
            IPStateBase *ipsp = (*vips)[j + npts];

            dG3DIPVariableBase *ipvm = static_cast<dG3DIPVariableBase *>(ipsm->getState(ws));
            dG3DIPVariableBase *ipvp = static_cast<dG3DIPVariableBase *>(ipsp->getState(ws));

            // get shape function gradients
            const std::vector<TensorialTraits<double>::GradType> &Gradm = ipvm->gradf(_spaceminus, em, GPm[j]);
            const std::vector<TensorialTraits<double>::GradType> &Gradp = ipvp->gradf(_spaceplus, ep, GPp[j]);
            const std::vector<TensorialTraits<double>::ValType> &Valm = ipvm->f(_spaceminus, em, GPm[j]);
            const std::vector<TensorialTraits<double>::ValType> &Valp = ipvp->f(_spaceplus, ep, GPp[j]);

            STensor3 &Fm = ipvm->getRefToDeformationGradient();
            STensorOperation::unity(Fm);

            for (int cc = 0; cc < 3; cc++) {
                int nbFFm = _spaceminus->getNumShapeFunctions(em, cc);
                int nbFFmTotalLast = _spaceminus->getShapeFunctionsIndex(em, cc);
                for (int i = 0; i < nbFFm; i++) {
                    Fm(cc, 0) += Gradm[i + nbFFmTotalLast][0] * dispm(i + nbFFmTotalLast);
                    Fm(cc, 1) += Gradm[i + nbFFmTotalLast][1] * dispm(i + nbFFmTotalLast);
                    Fm(cc, 2) += Gradm[i + nbFFmTotalLast][2] * dispm(i + nbFFmTotalLast);
                }
            }
            
            #ifdef _DEBUG
            if (STensorOperation::determinantSTensor3(Fm) < 1.e-15)
                Msg::Error("Negative Jacobian Fm, em = %d, ele=%d", em->getNum(), ele->getNum());
            #endif //_DEBUG

            STensor3 &Fp = ipvp->getRefToDeformationGradient();
            STensorOperation::unity(Fp);

            for (int cc = 0; cc < 3; cc++) {
                int nbFFp = _spaceplus->getNumShapeFunctions(ep, cc);
                int nbFFpTotalLast = _spaceplus->getShapeFunctionsIndex(ep, cc);
                for (int i = 0; i < nbFFp; i++) {
                    Fp(cc, 0) += Gradp[i + nbFFpTotalLast][0] * dispp(i + nbFFpTotalLast);
                    Fp(cc, 1) += Gradp[i + nbFFpTotalLast][1] * dispp(i + nbFFpTotalLast);
                    Fp(cc, 2) += Gradp[i + nbFFpTotalLast][2] * dispp(i + nbFFpTotalLast);
                }
            }
            #ifdef _DEBUG
            if (STensorOperation::determinantSTensor3(Fp) < 1.e-15)
                Msg::Error("Negative Jacobian Fp, ep =%d ele = %d", ep->getNum(), ele->getNum());
            #endif //_DEBUG

            // displacement jump
            SVector3 &ujump = ipvm->getRefToJump();
            int nbFFmx = _spaceminus->getNumShapeFunctions(em, 0);
            int nbFFmy = _spaceminus->getNumShapeFunctions(em, 1);
            int nbFFmz = _spaceminus->getNumShapeFunctions(em, 2);
            int nbFFpx = _spaceplus->getNumShapeFunctions(ep, 0);
            int nbFFpy = _spaceplus->getNumShapeFunctions(ep, 1);
            int nbFFpz = _spaceplus->getNumShapeFunctions(ep, 2);

            ujump[0] = computeJumpField(Valm, 0, nbFFmx, Valp, 0, nbFFpx, dispm, dispp);
            ujump[1] = computeJumpField(Valm, nbFFmx, nbFFmy, Valp, nbFFpx, nbFFpy, dispm, dispp);
            ujump[2] = computeJumpField(Valm, nbFFmx + nbFFmy, nbFFmz, Valp, nbFFpx + nbFFpy, nbFFpz, dispm, dispp);
            ipvp->getRefToJump() = ujump;
        };
    }

        if (useBarF) {
            double barJm = 0.;
            double barJp = 0.;
            double totalWeight = 0.;
            for (int j = 0; j < npts; j++) {
                IPStateBase *ipsm = (*vips)[j];
                IPStateBase *ipsp = (*vips)[j + npts];
                double weight = GP[j].weight;
                totalWeight += weight;
                dG3DIPVariableBase *ipvm = static_cast<dG3DIPVariableBase *>(ipsm->getState(ws));
                dG3DIPVariableBase *ipvp = static_cast<dG3DIPVariableBase *>(ipsp->getState(ws));

                double Jm = ipvm->getJ(_dim); // we assume the domain is in the Oxy plane
                ipvm->getRefToLocalJ() = Jm;
                barJm += Jm * weight;
                double Jp = ipvp->getJ(_dim); // we assume the domain is in the Oxy plane
                ipvp->getRefToLocalJ() = Jp;
                barJp += Jp * weight;
            }
            barJm /= totalWeight;
            barJp /= totalWeight;
            for (int j = 0; j < npts; j++) {
                IPStateBase *ipsm = (*vips)[j];
                IPStateBase *ipsp = (*vips)[j + npts];

                dG3DIPVariableBase *ipvm = static_cast<dG3DIPVariableBase *>(ipsm->getState(ws));
                dG3DIPVariableBase *ipvp = static_cast<dG3DIPVariableBase *>(ipsp->getState(ws));

                double localJm = ipvm->getLocalJ();
                ipvm->getRefToBarJ() = barJm;
                STensor3 &barFm = ipvm->getRefToDeformationGradient();
                for(int k=0; k<_dim; k++)
                  for(int l=0; l<_dim; l++)
                    barFm(k,l) *= pow(barJm / localJm, 1. /((double)(_dim)) );

                double localJp = ipvp->getLocalJ();
                ipvp->getRefToBarJ() = barJp;
                STensor3 &barFp = ipvp->getRefToDeformationGradient();
                for(int k=0; k<_dim; k++)
                  for(int l=0; l<_dim; l++)
                    barFp(k,l) *= pow(barJp / localJp, 1. / ((double)(_dim)));
            }
        }

        if (_averageStrainBased and (efMinus->getMaterialLaw()->getNum() == efPlus->getMaterialLaw()->getNum())) {
            for (int j = 0; j < npts; j++) {
                IPStateBase *ipsm = (*vips)[j];
                IPStateBase *ipsp = (*vips)[j + npts];
                dG3DIPVariableBase *ipvm = static_cast<dG3DIPVariableBase *>(ipsm->getState(ws));
                dG3DIPVariableBase *ipvp = static_cast<dG3DIPVariableBase *>(ipsp->getState(ws));
                STensor3 &Fm = ipvm->getRefToDeformationGradient();
                STensor3 &Fp = ipvp->getRefToDeformationGradient();
                // compute Fm as average 0.5*(Fm+Fp)
                // assign Fp = Fm
                Fm += Fp;
                Fm *= (0.5);
                Fp = Fm;
                if (useBarF)
                    Msg::Error(
                            "The setBulkDeformationGradient does not allow to recover the local one for the derivatives");
            }
        }
    /*
     * NONLOCAL FIELD
     * 
     */
    if (getNumNonLocalVariable() > 0)
    {
      for(int j=0;j<npts;j++) {
          IPStateBase *ipsm = (*vips)[j];
          IPStateBase *ipsp = (*vips)[j + npts];

          dG3DIPVariableBase *ipvm = static_cast<dG3DIPVariableBase *>(ipsm->getState(ws));
          dG3DIPVariableBase *ipvp = static_cast<dG3DIPVariableBase *>(ipsp->getState(ws));

          // get shape function gradients
          const std::vector<TensorialTraits<double>::GradType> &Gradm = ipvm->gradf(_spaceminus, em, GPm[j]);
          const std::vector<TensorialTraits<double>::GradType> &Gradp = ipvp->gradf(_spaceplus, ep, GPp[j]);
          const std::vector<TensorialTraits<double>::ValType> &Valm = ipvm->f(_spaceminus, em, GPm[j]);
          const std::vector<TensorialTraits<double>::ValType> &Valp = ipvp->f(_spaceplus, ep, GPp[j]);

          // non-local for minus part
          if (ipvm->getNumberNonLocalVariable() > getNumNonLocalVariable())
              Msg::Error(
                      "Your material law uses more non local variables than your domain dG3DDomain::computeStrain negative interface");

          for (int nlm = 0; nlm < ipvm->getNumberNonLocalVariable(); nlm++) {
              if (evaluateNonLocalField[nlm]) {
                  double &nonlocalVarm = ipvm->getRefToNonLocalVariable(nlm);
                  nonlocalVarm = 0.;

                  SVector3 &gradNonlocalVarm = ipvm->getRefToGradNonLocalVariable()[nlm];
                  STensorOperation::zero(gradNonlocalVarm);
                  int nbFFm = _spaceminus->getNumShapeFunctions(em, 3 + nlm);
                  int nbFFmTotalLast = _spaceminus->getShapeFunctionsIndex(em, 3 + nlm);
                  for (int i = 0; i < nbFFm; i++) {
                      //epsbar: take compoenent epsbar
                      nonlocalVarm += Valm[i + nbFFmTotalLast] * dispm(i + nbFFmTotalLast);
                      gradNonlocalVarm(0) += Gradm[i + nbFFmTotalLast][0] * dispm(i + nbFFmTotalLast);
                      gradNonlocalVarm(1) += Gradm[i + nbFFmTotalLast][1] * dispm(i + nbFFmTotalLast);
                      gradNonlocalVarm(2) += Gradm[i + nbFFmTotalLast][2] * dispm(i + nbFFmTotalLast);
                  }
              }
          }
          // for plus part
          if (ipvp->getNumberNonLocalVariable() > getNumNonLocalVariable())
              Msg::Error(
                      "Your material law uses more non local variables than your domain dG3DDomain::computeStrain positive interface");
          for (int nlp = 0; nlp < ipvp->getNumberNonLocalVariable(); nlp++) {
              if (evaluateNonLocalField[nlp]) {
                  double &nonlocalVarp = ipvp->getRefToNonLocalVariable(nlp);
                  nonlocalVarp = 0.;
                  SVector3 &gradNonlocalVarp = ipvp->getRefToGradNonLocalVariable()[nlp];
                  STensorOperation::zero(gradNonlocalVarp);
                  int nbFFp = _spaceplus->getNumShapeFunctions(ep, 3 + nlp);
                  int nbFFpTotalLast = _spaceplus->getShapeFunctionsIndex(ep, 3 + nlp);
                  for (int i = 0; i < nbFFp; i++) {
                      //epsbar: take compoenent epsbar
                      nonlocalVarp += Valp[i + nbFFpTotalLast] * dispp(i + nbFFpTotalLast);
                      gradNonlocalVarp(0) += Gradp[i + nbFFpTotalLast][0] * dispp(i + nbFFpTotalLast);
                      gradNonlocalVarp(1) += Gradp[i + nbFFpTotalLast][1] * dispp(i + nbFFpTotalLast);
                      gradNonlocalVarp(2) += Gradp[i + nbFFpTotalLast][2] * dispp(i + nbFFpTotalLast);
                  }
              }
          }
          // compute the jump
          // reset the number
          for (int nl = 0; nl < ipvp->getNumberNonLocalVariable(); nl++) {
              if (evaluateNonLocalField[nl]) {
                  int nbFFm = _spaceminus->getNumShapeFunctions(em, 3 + nl);
                  int nbFFmTotalLast = _spaceminus->getShapeFunctionsIndex(em, 3 + nl);
                  int nbFFp = _spaceminus->getNumShapeFunctions(ep, 3 + nl);
                  int nbFFpTotalLast = _spaceplus->getShapeFunctionsIndex(ep, 3 + nl);
                  double &epsjump = ipvm->getRefToNonLocalJump()(nl);
                  epsjump = computeJumpField(Valm, nbFFmTotalLast, nbFFm, Valp, nbFFmTotalLast, nbFFp, dispm, dispp);
                  ipvp->getRefToNonLocalJump()(nl) = epsjump;

              }
          }
      }
      
      
      for(int j=0;j<npts;j++)
      {
        IPStateBase* ipsm = (*vips)[j];
        IPStateBase* ipsp = (*vips)[j+npts];

        dG3DIPVariableBase* ipvm = static_cast<dG3DIPVariableBase*>(ipsm->getState(ws));
        dG3DIPVariableBase* ipvp = static_cast<dG3DIPVariableBase*>(ipsp->getState(ws));
        for (int nlm = 0; nlm < ipvm->getNumberNonLocalVariable(); nlm++)
        {
            if (evaluateNonLocalField[nlm]) ipvm->getRefToNonLocalVariableInMaterialLaw(nlm) = ipvm->getConstRefToNonLocalVariable(nlm);
        }
        for (int nlp = 0; nlp < ipvp->getNumberNonLocalVariable(); nlp++)
        {
            if (evaluateNonLocalField[nlp]) ipvp->getRefToNonLocalVariableInMaterialLaw(nlp) = ipvp->getConstRefToNonLocalVariable(nlp);
        }
      
      }
      
    
      // mean part
      if (_averageStrainBased and (efMinus->getMaterialLaw()->getNum() == efPlus->getMaterialLaw()->getNum()))
      {
        for(int j=0;j<npts;j++)
        {
          IPStateBase* ipsm = (*vips)[j];
          IPStateBase* ipsp = (*vips)[j+npts];
          dG3DIPVariableBase* ipvm = static_cast<dG3DIPVariableBase*>(ipsm->getState(ws));
          dG3DIPVariableBase* ipvp = static_cast<dG3DIPVariableBase*>(ipsp->getState(ws));
          
          // average nonlocal values
          for (int nlv = 0; nlv < ipvm->getNumberNonLocalVariable(); nlv++){
              if (evaluateNonLocalField[nlv]) {
                  // value averge
                  double &valminus = ipvm->getRefToNonLocalVariableInMaterialLaw(nlv);
                  valminus += ipvp->getConstRefToNonLocalVariableInMaterialLaw(nlv);
                  valminus *= 0.5;
                  ipvp->getRefToNonLocalVariableInMaterialLaw(nlv) = valminus;
              }
          }
        }
      }
    }
    
    /**
     * COHESIVE AND FRACTURE 
     * 
     * **/
    if(evaluateMecaField) {
        // comupute jump at each interfce nodes
        fullVector<double> jumpinter;
        bool willComputeJumpinter = false;
        for (int j = 0; j < npts; j++) {
            IPStateBase *ipsm = (*vips)[j];
            IPStateBase *ipsp = (*vips)[j + npts];
            FractureCohesive3DIPVariable *ipvfm = dynamic_cast<FractureCohesive3DIPVariable *>(ipsm->getState(ws));
            FractureCohesive3DIPVariable *ipvfp = dynamic_cast<FractureCohesive3DIPVariable *>(ipsp->getState(ws));
            if ((ipvfm != NULL) and (ipvfp != NULL)) {
                Cohesive3DIPVariableBase *cohipvfm = ipvfm->getIPvFrac();
                Cohesive3DIPVariableBase *cohipvfp = ipvfp->getIPvFrac();
                // Compute spatial gradient of jump on interface at interface IP
                if (cohipvfm->withJumpGradient() and cohipvfp->withJumpGradient()) {
                    willComputeJumpinter = true;
                    break;
                }
            } else {
                // all ip in one state has same type
                break;
            }
        }

        if (willComputeJumpinter) {
            jumpinter.resize(3 * nbvertexInter);
            jumpinter.setAll(0.);
            for (int iv = 0; iv < nbvertexInter; iv++) {
                double um, vm, wm;
                double up, vp, wp;
                em->getNode(vnlist[iv], um, vm, wm);
                ep->getNode(vplist[iv], up, vp, wp);

                std::vector<TensorialTraits<double>::ValType> Valm, Valp;
                _spaceminus->f(em, um, vm, wm, Valm);
                _spaceplus->f(ep, up, vp, wp, Valp);


                for (int i = 0; i < 3; i++) {
                    int nbFFm = _spaceminus->getNumShapeFunctions(em, i);
                    int nbFFmLastTotal = _spaceminus->getShapeFunctionsIndex(em, i);
                    for (int k = 0; k < nbFFm; k++) {
                        jumpinter(iv + i * nbvertexInter) -= Valm[k + nbFFmLastTotal] * dispm(k + nbFFmLastTotal);
                    }

                    int nbFFp = _spaceplus->getNumShapeFunctions(ep, i);
                    int nbFFpLastTotal = _spaceplus->getShapeFunctionsIndex(ep, i);
                    for (int k = 0; k < nbFFp; k++) {
                        jumpinter(iv + i * nbvertexInter) += Valp[k + nbFFpLastTotal] * dispp(k + nbFFpLastTotal);
                    }
                }
            }
        }

        for (int j = 0; j < npts; j++) {

            IPStateBase *ipsm = (*vips)[j];
            IPStateBase *ipsp = (*vips)[j + npts];

            dG3DIPVariableBase *ipvm = static_cast<dG3DIPVariableBase *>(ipsm->getState(ws));
            dG3DIPVariableBase *ipvp = static_cast<dG3DIPVariableBase *>(ipsp->getState(ws));

            STensor3 &Fm = ipvm->getRefToDeformationGradient();
            STensor3 &Fp = ipvp->getRefToDeformationGradient();


            FractureCohesive3DIPVariable *ipvfm = dynamic_cast<FractureCohesive3DIPVariable *>(ipvm);
            FractureCohesive3DIPVariable *ipvfp = dynamic_cast<FractureCohesive3DIPVariable *>(ipvp);

            if ((ipvfm != NULL) and (ipvfp != NULL)) {

                Cohesive3DIPVariableBase *cohipvfm = ipvfm->getIPvFrac();
                Cohesive3DIPVariableBase *cohipvfp = ipvfp->getIPvFrac();

                // store bulk deformation gradient at interface IP
                if (cohipvfm->withDeformationGradient() and cohipvfp->withDeformationGradient()) {
                    cohipvfm->setBulkDeformationGradient(Fm);
                    cohipvfp->setBulkDeformationGradient(Fp);
                }

                // Compute spatial gradient of jump on interface at interface IP
                if (cohipvfm->withJumpGradient() and cohipvfp->withJumpGradient()) {
                    const std::vector<TensorialTraits<double>::GradType> &gradVal = ipvm->gradf(_spaceminus, ie, GP[j]);
                    STensor3 &Gradjumpm = cohipvfm->getRefToJumpGradient();
                    STensorOperation::zero(Gradjumpm);
                    for (int k = 0; k < 3; k++) {
                        for (int l = 0; l < nbvertexInter; l++) {
                            Gradjumpm(0, k) += jumpinter(l) * gradVal[l](k);
                            Gradjumpm(1, k) += jumpinter(l + nbvertexInter) * gradVal[l + nbvertexInter](k);
                            Gradjumpm(2, k) += jumpinter(l + 2 * nbvertexInter) * gradVal[l + 2 * nbvertexInter](k);
                        }
                    }
                    cohipvfp->getRefToJumpGradient() = Gradjumpm;
                };
            }
        }
    }
     /***
     * EXTERA FIELD
     * 
     * **/
    if (getNumConstitutiveExtraDofDiffusionVariable() > 0)
    {
      for(int j=0;j<npts;j++)
      {
        
        IPStateBase* ipsm = (*vips)[j];
        IPStateBase* ipsp = (*vips)[j+npts];

        dG3DIPVariableBase* ipvm = static_cast<dG3DIPVariableBase*>(ipsm->getState(ws));
        dG3DIPVariableBase* ipvp = static_cast<dG3DIPVariableBase*>(ipsp->getState(ws));

        // get shape function gradients
        const std::vector<TensorialTraits<double>::GradType> &Gradm = ipvm->gradf(_spaceminus,em,GPm[j]);
        const std::vector<TensorialTraits<double>::GradType> &Gradp = ipvp->gradf(_spaceplus,ep,GPp[j]);
        const std::vector<TensorialTraits<double>::ValType> &Valm = ipvm->f(_spaceminus,em,GPm[j]);
        const std::vector<TensorialTraits<double>::ValType> &Valp = ipvp->f(_spaceplus,ep,GPp[j]);
        
        // constitutive extra dofs
        if( ipvm->getNumConstitutiveExtraDofDiffusionVariable()>getNumConstitutiveExtraDofDiffusionVariable() )
          Msg::Error("Your material law uses more constitutive extra dof diffusion variables than your domain dG3DDomain::computeStrain bulk");
          
        
        for (int extraDOFFieldm = 0; extraDOFFieldm < ipvm->getNumConstitutiveExtraDofDiffusionVariable(); extraDOFFieldm++) {
            if (evaluateExtraDofDiffusionField[extraDOFFieldm]) {
                if ((ipvm->getNumConstitutiveExtraDofDiffusionVariable() == 2) &&
                    (getConstitutiveExtraDofDiffusionUseEnergyConjugatedField())) {
                    double &energyConjugatedFieldm = ipvm->getRefToEnergyConjugatedField(extraDOFFieldm);
                    SVector3 &gradEnergyConjugatedFieldm = ipvm->getRefToGradEnergyConjugatedField()[extraDOFFieldm];
                    energyConjugatedFieldm = 0.;
                    STensorOperation::zero(gradEnergyConjugatedFieldm);

                    int nbFFm = _spaceminus->getNumShapeFunctions(em, 3 + getNumNonLocalVariable() + extraDOFFieldm);
                    int nbFFmTotalLast = _spaceminus->getShapeFunctionsIndex(em, 3 + getNumNonLocalVariable() +
                                                                                 extraDOFFieldm);
                    for (int i = 0; i < nbFFm; i++) {
                        const double &ftemp = dispm(i + nbFFmTotalLast);
                        energyConjugatedFieldm += Valm[i + nbFFmTotalLast] * ftemp;
                        gradEnergyConjugatedFieldm(0) += Gradm[i + nbFFmTotalLast][0] * ftemp;
                        gradEnergyConjugatedFieldm(1) += Gradm[i + nbFFmTotalLast][1] * ftemp;
                        gradEnergyConjugatedFieldm(2) += Gradm[i + nbFFmTotalLast][2] * ftemp;
                    }

                    double &Tm = ipvm->getRefToField(extraDOFFieldm);
                    SVector3 &gradTm = ipvm->getRefToGradField()[extraDOFFieldm];

                    int extraDOFField_aux = (extraDOFFieldm == 0) ? 1 : 0;

                    double &dFielddEnergyConjugatedFieldm = ipvm->getRefTodFielddEnergyConjugatedField()[extraDOFFieldm][extraDOFFieldm];
                    double &dFielddEnergyConjugatedField2m = ipvm->getRefTodFielddEnergyConjugatedField()[extraDOFFieldm][extraDOFField_aux];
                    SVector3 &dGradFielddEnergyConjugatedFieldm = ipvm->getRefTodGradFielddEnergyConjugatedField()[extraDOFFieldm][extraDOFFieldm];
                    SVector3 &dGradFielddEnergyConjugatedField2m = ipvm->getRefTodGradFielddEnergyConjugatedField()[extraDOFFieldm][extraDOFField_aux];
                    STensor3 &dGradFielddGradEnergyConjugatedFieldm = ipvm->getRefTodGradFielddGradEnergyConjugatedField()[extraDOFFieldm][extraDOFFieldm];
                    STensor3 &dGradFielddGradEnergyConjugatedField2m = ipvm->getRefTodGradFielddGradEnergyConjugatedField()[extraDOFFieldm][extraDOFField_aux];

                    if (extraDOFFieldm == 0) {
                        Tm = 1 / (energyConjugatedFieldm);

                        gradTm(0) = -1 / (pow(energyConjugatedFieldm, 2)) * (gradEnergyConjugatedFieldm(0));
                        gradTm(1) = -1 / (pow(energyConjugatedFieldm, 2)) * (gradEnergyConjugatedFieldm(1));
                        gradTm(2) = -1 / (pow(energyConjugatedFieldm, 2)) * (gradEnergyConjugatedFieldm(2));

                        dFielddEnergyConjugatedFieldm = -1. / (energyConjugatedFieldm) / (energyConjugatedFieldm);
                        dFielddEnergyConjugatedField2m = 0.;

                        dGradFielddEnergyConjugatedFieldm(0) =
                                2 / (energyConjugatedFieldm) / (energyConjugatedFieldm) / (energyConjugatedFieldm) *
                                (gradEnergyConjugatedFieldm(0));
                        dGradFielddEnergyConjugatedFieldm(1) =
                                2 / (energyConjugatedFieldm) / (energyConjugatedFieldm) / (energyConjugatedFieldm) *
                                (gradEnergyConjugatedFieldm(1));
                        dGradFielddEnergyConjugatedFieldm(2) =
                                2 / (energyConjugatedFieldm) / (energyConjugatedFieldm) / (energyConjugatedFieldm) *
                                (gradEnergyConjugatedFieldm(2));

                        dGradFielddEnergyConjugatedField2m(0) = 0.;
                        dGradFielddEnergyConjugatedField2m(1) = 0.;
                        dGradFielddEnergyConjugatedField2m(2) = 0.;

                        STensorOperation::diag(dGradFielddGradEnergyConjugatedFieldm,
                                               -1. / (energyConjugatedFieldm) / (energyConjugatedFieldm));
                        STensorOperation::zero(dGradFielddGradEnergyConjugatedField2m);
                    }
                    else {
                        double fTm = ipvm->getConstRefToEnergyConjugatedField(0);
                        SVector3 gradfTm = ipvm->getConstRefToGradEnergyConjugatedField()[0];

                        Tm = -1 * (energyConjugatedFieldm) / (fTm);

                        gradTm(0) = -1 / (fTm) * (gradEnergyConjugatedFieldm(0)) +
                                    (energyConjugatedFieldm) / (pow(fTm, 2)) * (gradfTm(0));
                        gradTm(1) = -1 / (fTm) * (gradEnergyConjugatedFieldm(1)) +
                                    (energyConjugatedFieldm) / (pow(fTm, 2)) * (gradfTm(1));
                        gradTm(2) = -1 / (fTm) * (gradEnergyConjugatedFieldm(2)) +
                                    (energyConjugatedFieldm) / (pow(fTm, 2)) * (gradfTm(2));

                        dFielddEnergyConjugatedFieldm = -1. / (fTm);
                        dFielddEnergyConjugatedField2m = (energyConjugatedFieldm) / (fTm) / (fTm);

                        dGradFielddEnergyConjugatedFieldm(0) = 1. / (fTm) / (fTm) * (gradfTm(0));
                        dGradFielddEnergyConjugatedFieldm(1) = 1. / (fTm) / (fTm) * (gradfTm(1));
                        dGradFielddEnergyConjugatedFieldm(2) = 1. / (fTm) / (fTm) * (gradfTm(2));

                        dGradFielddEnergyConjugatedField2m(0) = 1. / (fTm) / (fTm) * (gradEnergyConjugatedFieldm(0)) -
                                                                2 * (energyConjugatedFieldm) / (fTm) / (fTm) / (fTm) *
                                                                (gradfTm(0));
                        dGradFielddEnergyConjugatedField2m(1) = 1. / (fTm) / (fTm) * (gradEnergyConjugatedFieldm(1)) -
                                                                2 * (energyConjugatedFieldm) / (fTm) / (fTm) / (fTm) *
                                                                (gradfTm(1));
                        dGradFielddEnergyConjugatedField2m(2) = 1. / (fTm) / (fTm) * (gradEnergyConjugatedFieldm(2)) -
                                                                2 * (energyConjugatedFieldm) / (fTm) / (fTm) / (fTm) *
                                                                (gradfTm(2));

                        STensorOperation::diag(dGradFielddGradEnergyConjugatedFieldm, -1. / (fTm));
                        STensorOperation::diag(dGradFielddGradEnergyConjugatedField2m,
                                               (energyConjugatedFieldm) / (fTm) / (fTm));
                    }
                }
                else {
                    double &Tm = ipvm->getRefToField(extraDOFFieldm);
                    Tm = 0.;
                    SVector3 &gradTm = ipvm->getRefToGradField()[extraDOFFieldm];
                    STensorOperation::zero(gradTm);

                    int nbFFm = _spaceminus->getNumShapeFunctions(em, 3 + getNumNonLocalVariable() + extraDOFFieldm);
                    int nbFFmTotalLast = _spaceminus->getShapeFunctionsIndex(em, 3 + getNumNonLocalVariable() +
                                                                                 extraDOFFieldm);
                    for (int i = 0; i < nbFFm; i++) {
                        Tm += Valm[i + nbFFmTotalLast] * dispm(i + nbFFmTotalLast);
                        gradTm(0) += Gradm[i + nbFFmTotalLast][0] * dispm(i + nbFFmTotalLast);
                        gradTm(1) += Gradm[i + nbFFmTotalLast][1] * dispm(i + nbFFmTotalLast);
                        gradTm(2) += Gradm[i + nbFFmTotalLast][2] * dispm(i + nbFFmTotalLast);
                    }
                }
            }
            if (ipvp->getNumConstitutiveExtraDofDiffusionVariable() > getNumConstitutiveExtraDofDiffusionVariable())
                Msg::Error(
                        "Your material law uses more constitutive extra dof diffusion variables than your domain dG3DDomain::computeStrain bulk");
        }
        for (int extraDOFFieldp = 0; extraDOFFieldp < ipvp->getNumConstitutiveExtraDofDiffusionVariable(); extraDOFFieldp++) {
            if (evaluateExtraDofDiffusionField[extraDOFFieldp]) {
                if ((ipvp->getNumConstitutiveExtraDofDiffusionVariable() == 2) &&
                    (getConstitutiveExtraDofDiffusionUseEnergyConjugatedField())) {
                    double &energyConjugatedFieldp = ipvp->getRefToEnergyConjugatedField(extraDOFFieldp);
                    SVector3 &gradEnergyConjugatedFieldp = ipvp->getRefToGradEnergyConjugatedField()[extraDOFFieldp];
                    energyConjugatedFieldp = 0.;
                    STensorOperation::zero(gradEnergyConjugatedFieldp);

                    int nbFFp = _spaceplus->getNumShapeFunctions(ep, 3 + getNumNonLocalVariable() + extraDOFFieldp);
                    int nbFFpTotalLast = _spaceplus->getShapeFunctionsIndex(ep, 3 + getNumNonLocalVariable() +
                                                                                extraDOFFieldp);
                    for (int i = 0; i < nbFFp; i++) {
                        const double &ftemp = dispp(i + nbFFpTotalLast);
                        energyConjugatedFieldp += Valp[i + nbFFpTotalLast] * ftemp;
                        gradEnergyConjugatedFieldp(0) += Gradp[i + nbFFpTotalLast][0] * ftemp;
                        gradEnergyConjugatedFieldp(1) += Gradp[i + nbFFpTotalLast][1] * ftemp;
                        gradEnergyConjugatedFieldp(2) += Gradp[i + nbFFpTotalLast][2] * ftemp;
                    }

                    double &Tp = ipvp->getRefToField(extraDOFFieldp);
                    SVector3 &gradTp = ipvp->getRefToGradField()[extraDOFFieldp];

                    int extraDOFField_aux = (extraDOFFieldp == 0) ? 1 : 0;

                    double &dFielddEnergyConjugatedFieldp = ipvp->getRefTodFielddEnergyConjugatedField()[extraDOFFieldp][extraDOFFieldp];
                    double &dFielddEnergyConjugatedField2p = ipvp->getRefTodFielddEnergyConjugatedField()[extraDOFFieldp][extraDOFField_aux];
                    SVector3 &dGradFielddEnergyConjugatedFieldp = ipvp->getRefTodGradFielddEnergyConjugatedField()[extraDOFFieldp][extraDOFFieldp];
                    SVector3 &dGradFielddEnergyConjugatedField2p = ipvp->getRefTodGradFielddEnergyConjugatedField()[extraDOFFieldp][extraDOFField_aux];
                    STensor3 &dGradFielddGradEnergyConjugatedFieldp = ipvp->getRefTodGradFielddGradEnergyConjugatedField()[extraDOFFieldp][extraDOFFieldp];
                    STensor3 &dGradFielddGradEnergyConjugatedField2p = ipvp->getRefTodGradFielddGradEnergyConjugatedField()[extraDOFFieldp][extraDOFField_aux];

                    if (extraDOFFieldp == 0) {
                        Tp = 1 / (energyConjugatedFieldp);

                        gradTp(0) = -1 / (pow(energyConjugatedFieldp, 2)) * (gradEnergyConjugatedFieldp(0));
                        gradTp(1) = -1 / (pow(energyConjugatedFieldp, 2)) * (gradEnergyConjugatedFieldp(1));
                        gradTp(2) = -1 / (pow(energyConjugatedFieldp, 2)) * (gradEnergyConjugatedFieldp(2));

                        dFielddEnergyConjugatedFieldp = -1. / (energyConjugatedFieldp) / (energyConjugatedFieldp);
                        dFielddEnergyConjugatedField2p = 0.;

                        dGradFielddEnergyConjugatedFieldp(0) =
                                2 / (energyConjugatedFieldp) / (energyConjugatedFieldp) / (energyConjugatedFieldp) *
                                (gradEnergyConjugatedFieldp(0));
                        dGradFielddEnergyConjugatedFieldp(1) =
                                2 / (energyConjugatedFieldp) / (energyConjugatedFieldp) / (energyConjugatedFieldp) *
                                (gradEnergyConjugatedFieldp(1));
                        dGradFielddEnergyConjugatedFieldp(2) =
                                2 / (energyConjugatedFieldp) / (energyConjugatedFieldp) / (energyConjugatedFieldp) *
                                (gradEnergyConjugatedFieldp(2));

                        dGradFielddEnergyConjugatedField2p(0) = 0.;
                        dGradFielddEnergyConjugatedField2p(1) = 0.;
                        dGradFielddEnergyConjugatedField2p(2) = 0.;

                        STensorOperation::diag(dGradFielddGradEnergyConjugatedFieldp,
                                               -1. / (energyConjugatedFieldp) / (energyConjugatedFieldp));
                        STensorOperation::zero(dGradFielddGradEnergyConjugatedField2p);
                    }
                    else {
                        double fTp = ipvp->getConstRefToEnergyConjugatedField(0);
                        SVector3 gradfTp = ipvp->getConstRefToGradEnergyConjugatedField()[0];

                        Tp = -1 * (energyConjugatedFieldp) / (fTp);

                        gradTp(0) = -1 / (fTp) * (gradEnergyConjugatedFieldp(0)) +
                                    (energyConjugatedFieldp) / (pow(fTp, 2)) * (gradfTp(0));
                        gradTp(1) = -1 / (fTp) * (gradEnergyConjugatedFieldp(1)) +
                                    (energyConjugatedFieldp) / (pow(fTp, 2)) * (gradfTp(1));
                        gradTp(2) = -1 / (fTp) * (gradEnergyConjugatedFieldp(2)) +
                                    (energyConjugatedFieldp) / (pow(fTp, 2)) * (gradfTp(2));

                        dFielddEnergyConjugatedFieldp = -1. / (fTp);
                        dFielddEnergyConjugatedField2p = (energyConjugatedFieldp) / (fTp) / (fTp);

                        dGradFielddEnergyConjugatedFieldp(0) = 1. / (fTp) / (fTp) * (gradfTp(0));
                        dGradFielddEnergyConjugatedFieldp(1) = 1. / (fTp) / (fTp) * (gradfTp(1));
                        dGradFielddEnergyConjugatedFieldp(2) = 1. / (fTp) / (fTp) * (gradfTp(2));

                        dGradFielddEnergyConjugatedField2p(0) = 1. / (fTp) / (fTp) * (gradEnergyConjugatedFieldp(0)) -
                                                                2 * (energyConjugatedFieldp) / (fTp) / (fTp) / (fTp) *
                                                                (gradfTp(0));
                        dGradFielddEnergyConjugatedField2p(1) = 1. / (fTp) / (fTp) * (gradEnergyConjugatedFieldp(1)) -
                                                                2 * (energyConjugatedFieldp) / (fTp) / (fTp) / (fTp) *
                                                                (gradfTp(1));
                        dGradFielddEnergyConjugatedField2p(2) = 1. / (fTp) / (fTp) * (gradEnergyConjugatedFieldp(2)) -
                                                                2 * (energyConjugatedFieldp) / (fTp) / (fTp) / (fTp) *
                                                                (gradfTp(2));

                        STensorOperation::diag(dGradFielddGradEnergyConjugatedFieldp, -1. / (fTp));
                        STensorOperation::diag(dGradFielddGradEnergyConjugatedField2p,
                                               (energyConjugatedFieldp) / (fTp) / (fTp));
                    }
                }
                else {
                    double &Tp = ipvp->getRefToField(extraDOFFieldp);
                    Tp = 0.;
                    SVector3 &gradTp = ipvp->getRefToGradField()[extraDOFFieldp];
                    STensorOperation::zero(gradTp);

                    int nbFFp = _spaceplus->getNumShapeFunctions(ep, 3 + getNumNonLocalVariable() + extraDOFFieldp);
                    int nbFFpTotalLast = _spaceplus->getShapeFunctionsIndex(ep, 3 + getNumNonLocalVariable() +
                                                                                extraDOFFieldp);
                    for (int i = 0; i < nbFFp; i++) {
                        Tp += Valp[i + nbFFpTotalLast] * dispp(i + nbFFpTotalLast);
                        gradTp(0) += Gradp[i + nbFFpTotalLast][0] * dispp(i + nbFFpTotalLast);
                        gradTp(1) += Gradp[i + nbFFpTotalLast][1] * dispp(i + nbFFpTotalLast);
                        gradTp(2) += Gradp[i + nbFFpTotalLast][2] * dispp(i + nbFFpTotalLast);
                    }
                }
            }
        }
        // compute the jump
        std::vector<double> Tminus, Tplus;
        Tminus.resize(getNumConstitutiveExtraDofDiffusionVariable());
        Tplus.resize(getNumConstitutiveExtraDofDiffusionVariable());
        
        for (int extraDOFField = 0; extraDOFField < ipvm->getNumConstitutiveExtraDofDiffusionVariable(); extraDOFField++) {
            if (evaluateExtraDofDiffusionField[extraDOFField]) {
                int nbFFm = _spaceminus->getNumShapeFunctions(em, 3 + getNumNonLocalVariable() + extraDOFField);
                int nbFFmTotalLast = _spaceminus->getShapeFunctionsIndex(em,
                                                                         3 + getNumNonLocalVariable() + extraDOFField);

                int nbFFp = _spaceplus->getNumShapeFunctions(ep, 3 + getNumNonLocalVariable() + extraDOFField);
                int nbFFpTotalLast = _spaceplus->getShapeFunctionsIndex(ep,
                                                                        3 + getNumNonLocalVariable() + extraDOFField);
                // reset counter

                if (ipvm->getNumConstitutiveExtraDofDiffusionVariable() == 1) //thermomec
                {
                    Tminus[extraDOFField] = computFieldValue(Valm, nbFFmTotalLast, nbFFm, dispm);
                    Tplus[extraDOFField] = computFieldValue(Valp, nbFFpTotalLast, nbFFp, dispp);

                    ipvm->getRefToFieldJump()(extraDOFField) = Tplus[extraDOFField] - Tminus[extraDOFField];
                    ipvp->getRefToFieldJump()(extraDOFField) = Tplus[extraDOFField] - Tminus[extraDOFField];

                    // compute 1/T jump to be energetically consistent
                    if (getConstitutiveExtraDofDiffusionUseEnergyConjugatedField()) {
                        double &ftjump = ipvm->getRefToEnergyConjugatedFieldJump()(extraDOFField);
                        double &dftjumpdTp = ipvp->getRefTodEnergyConjugatedFieldJumpdFieldp()[extraDOFField][extraDOFField];
                        double &dftjumpdTm = ipvm->getRefTodEnergyConjugatedFieldJumpdFieldm()[extraDOFField][extraDOFField];

                        ftjump = 1. / Tplus[extraDOFField] - 1. / Tminus[extraDOFField];
                        dftjumpdTp = -1. / (Tplus[extraDOFField] * Tplus[extraDOFField]);
                        dftjumpdTm = 1. / (Tminus[extraDOFField] * Tminus[extraDOFField]);

                        ipvp->getRefToEnergyConjugatedFieldJump()(extraDOFField) = ftjump;
                        ipvm->getRefTodEnergyConjugatedFieldJumpdFieldm()[extraDOFField][extraDOFField] = dftjumpdTm;
                        ipvp->getRefTodEnergyConjugatedFieldJumpdFieldp()[extraDOFField][extraDOFField] = dftjumpdTp;
                    }
                }
                else if (ipvm->getNumConstitutiveExtraDofDiffusionVariable() == 2) //elecThermech
                {
                    if (getConstitutiveExtraDofDiffusionUseEnergyConjugatedField()) {
                        double &ftjump = ipvm->getRefToEnergyConjugatedFieldJump()(extraDOFField);
                        ftjump = computeJumpField(Valm, nbFFmTotalLast, nbFFm, Valp, nbFFpTotalLast, nbFFp, dispm,
                                                  dispp);
                        ipvp->getRefToEnergyConjugatedFieldJump()(extraDOFField) = ftjump;
                    }
                    else {
                        Tminus[extraDOFField] = computFieldValue(Valm, nbFFmTotalLast, nbFFm, dispm);
                        Tplus[extraDOFField] = computFieldValue(Valp, nbFFpTotalLast, nbFFp, dispp);
                        ipvm->getRefToFieldJump()(extraDOFField) = Tplus[extraDOFField] - Tminus[extraDOFField];
                        ipvp->getRefToFieldJump()(extraDOFField) = Tplus[extraDOFField] - Tminus[extraDOFField];

                        if (extraDOFField == 0) {
                            double &ftjump = ipvm->getRefToEnergyConjugatedFieldJump()(extraDOFField);
                            double &dftjumpdTp = ipvp->getRefTodEnergyConjugatedFieldJumpdFieldp()[extraDOFField][extraDOFField];
                            double &dftjumpdTm = ipvm->getRefTodEnergyConjugatedFieldJumpdFieldm()[extraDOFField][extraDOFField];

                            ftjump = 1. / Tplus[extraDOFField] - 1. / Tminus[extraDOFField];
                            dftjumpdTp = -1. / (Tplus[extraDOFField] * Tplus[extraDOFField]);
                            dftjumpdTm = 1. / (Tminus[extraDOFField] * Tminus[extraDOFField]);

                            ipvp->getRefToEnergyConjugatedFieldJump()(extraDOFField) = ftjump;
                        }
                        else {
                            double &fvjump = ipvm->getRefToEnergyConjugatedFieldJump()(extraDOFField);
                            double &dfvjumpdvp = ipvp->getRefTodEnergyConjugatedFieldJumpdFieldp()[extraDOFField][extraDOFField];
                            double &dfvjumpdvm = ipvm->getRefTodEnergyConjugatedFieldJumpdFieldm()[extraDOFField][extraDOFField];
                            double &dfvjumpdTp = ipvp->getRefTodEnergyConjugatedFieldJumpdFieldp()[extraDOFField][0];
                            double &dfvjumpdTm = ipvm->getRefTodEnergyConjugatedFieldJumpdFieldm()[extraDOFField][0];

                            double vm = Tminus[1];
                            double vp = Tplus[1];
                            double tm = Tminus[0];
                            double tp = Tplus[0];
                            fvjump = vm / tm - vp / tp;

                            dfvjumpdvp = -1. / tp;
                            dfvjumpdvm = 1. / tm;
                            dfvjumpdTp = vp / (tp * tp);
                            dfvjumpdTm = -vm / (tm * tm);

                            ipvp->getRefToEnergyConjugatedFieldJump()(extraDOFField) = fvjump;
                        }
                    }
                }
                else Msg::Error("UseEnergyConjugatedField with more than 2 extra dofs not implemented");
            }
            //
        }
      }
    }
  }
  else
  {  // Virtual interface element
     Msg::Error("No virtual interface for 3D");
  }
}
bool dG3DDomain::computeIpv(AllIPState *aips,MInterfaceElement *ie, IntPt *GP,const IPStateBase::whichState ws,
                                        partDomain* efMinus, partDomain *efPlus,materialLaw *mlawmi,
                                        materialLaw *mlawp,fullVector<double> &dispm,
                                        fullVector<double> &dispp,
                                        const bool virt, bool stiff,const bool checkfrac)
{
	MElement *ele = dynamic_cast<MElement*>(ie);
	if (!getElementErosionFilter()(ele)) return true;

  if(!virt)
  {

    dG3DMaterialLaw *mlawminus = static_cast<dG3DMaterialLaw*>(mlawmi);
    dG3DMaterialLaw *mlawplus = static_cast<dG3DMaterialLaw*>(mlawp);

    dG3DMaterialLaw *mlawbulkm;
    dG3DMaterialLaw *mlawbulkp;
    if(mlawminus->getType() == materialLaw::fracture)
      mlawbulkm = static_cast< dG3DMaterialLaw* >((static_cast< FractureByCohesive3DLaw* > (mlawminus) )->getBulkLaw());
    else
      mlawbulkm= static_cast<dG3DMaterialLaw*>(mlawminus);
    if(mlawplus->getType() == materialLaw::fracture)
      mlawbulkp = static_cast< dG3DMaterialLaw* >((static_cast< FractureByCohesive3DLaw* > (mlawplus) )->getBulkLaw());
    else
      mlawbulkp= static_cast<dG3DMaterialLaw*>(mlawplus);
    if (mlawbulkm->getUseBarF()!=mlawbulkp->getUseBarF()) //check with the bulk law
    {
      Msg::Error("dG3DDomain::computeIpv All the constitutive laws need to use Fbar or not");
      return false;
    }
    bool useBarF=mlawbulkm->getUseBarF();

    if(_evalStiff){ this->computeStrain(aips,ie, GP,ws,efMinus,efPlus,dispm,dispp,virt, useBarF);}

    int npts=integBound->getIntPoints(ele,&GP);
    bool needDTangent=false;
    BodyForceModuliType  ComputeBodyForceModuli = useWhichModuliForBF();
  
    if (hasBodyForceForHO() and ComputeBodyForceModuli == Present){
      needDTangent= true;
    } 
    AllIPState::ipstateElementContainer *vips = aips->getIPstate(ele->getNum());
    //Msg::Info("nb interf gp %d",npts);
    for(int j=0;j<npts;j++){

      IPStateBase* ipsm = (*vips)[j];
      IPStateBase* ipsp = (*vips)[j+npts];

      IPVariable* ipvm = ipsm->getState(ws);
      IPVariable* ipvp = ipsp->getState(ws);
      const IPVariable* ipvmprev = ipsm->getState(IPStateBase::previous);
      const IPVariable* ipvpprev = ipsp->getState(IPStateBase::previous);

      if (_subSteppingMethod == 0){
        // no substepting
        if (mlawminus->getNum() != mlawplus->getNum())
        {
          // if minus and plus material are different
          if (_planeStressState){
              mlawminus->stress3DTo2D(ipvm,ipvmprev, stiff, checkfrac,needDTangent);
              mlawplus->stress3DTo2D(ipvp,ipvpprev, stiff, checkfrac,needDTangent);
          }
          else{
              mlawminus->stress(ipvm,ipvmprev, stiff, checkfrac,needDTangent);
              mlawplus->stress(ipvp,ipvpprev, stiff, checkfrac,needDTangent);
          }
        }
        else{
          // if both part are the same
          if (_planeStressState)
            mlawminus->stress3DTo2D(ipvm,ipvmprev,stiff,checkfrac,needDTangent);
          else
          	mlawminus->stress(ipvm,ipvmprev,stiff,checkfrac,needDTangent);
          if (_averageStrainBased){
            // if the computation is performed based on the average strain
            // positive IP is equal to minus IP
            ipvp->operator=(*(dynamic_cast<const IPVariable*>(ipvm)));

          }
          else{
            if (_planeStressState)
              mlawplus->stress3DTo2D(ipvp,ipvpprev,stiff,checkfrac,needDTangent);
            else
              mlawplus->stress(ipvp,ipvpprev,stiff,checkfrac,needDTangent);
          }
        }
      }
      else if ((_subSteppingMethod == 1) or (_subSteppingMethod == 2))  {
        // compute negative part based on the substepting
        dG3DIPVariableBase* dgipvm = static_cast<dG3DIPVariableBase*>(ipvm);
        const dG3DIPVariableBase* dgipvmprev = static_cast<const dG3DIPVariableBase*>(ipvmprev);

        // create a temporary ipv
        // it cannot be static because different Ipv can be use with different domains
        IPVariable* ipvTemp =  dgipvmprev->clone();
        dG3DIPVariableBase* dgipvTemp = dynamic_cast<dG3DIPVariableBase*>(ipvTemp);
        if (dgipvTemp == NULL) Msg::Error("dG3DIPVariableBase::clone is not correctly defined");

        static STensor3 dF;
        dF = dgipvm->getConstRefToDeformationGradient();
        dF -= dgipvmprev->getConstRefToDeformationGradient();

        // if nonlocal damage is used
        fullVector<double> dnonlocalVarm;
        int numNL = dgipvm->getNumberNonLocalVariable();
        if (numNL > 0){
          dnonlocalVarm.resize(numNL);
          for (int iv = 0; iv < numNL; iv++){
            dnonlocalVarm(iv) = (dgipvm->getConstRefToNonLocalVariableInMaterialLaw(iv) - dgipvmprev->getConstRefToNonLocalVariableInMaterialLaw(iv));
          }
        }

        int iter = 1;
        while (1){
          // update kinematics
          double time = double(iter)/(double(_subStepNumber));
          //Msg::Info("step %d, time = %f",iter,time);
          STensor3& F = dgipvm->getRefToDeformationGradient();
          F = dgipvmprev->getConstRefToDeformationGradient();
          F.daxpy(dF,time);

          for (int iv=0; iv < numNL; iv++){
            dgipvm->getRefToNonLocalVariableInMaterialLaw(iv) = dgipvmprev->getConstRefToNonLocalVariableInMaterialLaw(iv);
            dgipvm->getRefToNonLocalVariableInMaterialLaw(iv) += (time*dnonlocalVarm(iv));
          }

          if (iter == _subStepNumber){
            // in last substep
            if (_planeStressState)
              mlawminus->stress3DTo2D(ipvm,ipvTemp,stiff,checkfrac,needDTangent);
            else
              mlawminus->stress(ipvm,ipvTemp,stiff,checkfrac,needDTangent);
            break;
          }
          else{
            if (_planeStressState)
            	mlawminus->stress3DTo2D(ipvm,ipvTemp,false,checkfrac,needDTangent);
            else
              mlawminus->stress(ipvm,ipvTemp,false,checkfrac,needDTangent);
            dgipvTemp->operator=(*(dynamic_cast<const IPVariable*>(ipvm)));
          }
          // next step
          iter++;
        }
        if (ipvTemp!=NULL) delete ipvTemp;

        if (_averageStrainBased and (mlawminus->getNum() == mlawplus->getNum())){
           // if the computation is performed based on the average strain
          // positive IP is equal to minus IP
          ipvp->operator=(*(dynamic_cast<const IPVariable*>(ipvm)));

        }
        else{
          dG3DIPVariableBase* dgipvp = static_cast<dG3DIPVariableBase*>(ipvp);
          const dG3DIPVariableBase* dgipvpprev = static_cast<const dG3DIPVariableBase*>(ipvpprev);

          // create a temporary ipv
          ipvTemp =  dgipvpprev->clone();
          dgipvTemp = dynamic_cast<dG3DIPVariableBase*>(ipvTemp);
          if (dgipvTemp == NULL) Msg::Error("dG3DIPVariableBase::clone is not correctly defined");

          dF = dgipvp->getConstRefToDeformationGradient();
          dF -= dgipvpprev->getConstRefToDeformationGradient();

          fullVector<double> dnonlocalVarp;
          numNL = dgipvp->getNumberNonLocalVariable();
          if (numNL > 0){
            dnonlocalVarp.resize(numNL);
            for (int iv = 0; iv < numNL; iv++){
              dnonlocalVarp(iv) = (dgipvp->getConstRefToNonLocalVariableInMaterialLaw(iv) - dgipvpprev->getConstRefToNonLocalVariableInMaterialLaw(iv));
            }
          }

          int iter = 1;
          while (1){
            // update kinematics
            double time = double(iter)/(double(_subStepNumber));
            //Msg::Info("step %d, time = %f",iter,time);
            STensor3& F = dgipvp->getRefToDeformationGradient();
            F = dgipvpprev->getConstRefToDeformationGradient();
            F.daxpy(dF,time);

            for (int iv=0; iv < numNL; iv++){
              dgipvp->getRefToNonLocalVariableInMaterialLaw(iv) = dgipvpprev->getConstRefToNonLocalVariableInMaterialLaw(iv);
              dgipvp->getRefToNonLocalVariableInMaterialLaw(iv) += (time*dnonlocalVarp(iv));
            }

            // stress  computation at substep

            if (iter == _subStepNumber){
            // in last substep
              if (_planeStressState)
                mlawplus->stress3DTo2D(ipvp,ipvTemp,stiff,checkfrac,needDTangent);
              else
                mlawplus->stress(ipvp,ipvTemp,stiff,checkfrac,needDTangent);
              break;
            }
            else{
              if (_planeStressState)
                 mlawplus->stress3DTo2D(ipvp,ipvTemp,false,checkfrac,needDTangent);
              else
                mlawplus->stress(ipvp,ipvTemp,false,checkfrac,needDTangent);
              dgipvTemp->operator=(*(dynamic_cast<const IPVariable*>(ipvp)));
            }
            // next step
            iter++;
          }
          if (ipvTemp!=NULL) delete ipvTemp;

        }
      }
      else{
        Msg::Error("This substepping method %d has not been implemented",_subSteppingMethod);
      }
      if(hasBodyForceForHO()){
        if(ComputeBodyForceModuli == Present){  
          ipvp->setValueForBodyForce(ipvp->getConstRefToTangentModuli(),ComputeBodyForceModuli);
          ipvm->setValueForBodyForce(ipvm->getConstRefToTangentModuli(),ComputeBodyForceModuli); 
        }
        else if(ComputeBodyForceModuli == Previous){
          ipvp->setValueForBodyForce(ipvpprev->getConstRefToTangentModuli(),ComputeBodyForceModuli);
          ipvm->setValueForBodyForce(ipvmprev->getConstRefToTangentModuli(),ComputeBodyForceModuli);  
        }
        else if(ComputeBodyForceModuli == Homogeneous){
          ipvp->setValueForBodyForce(getHomogeneousTangent(),ComputeBodyForceModuli); 
          ipvm->setValueForBodyForce(getHomogeneousTangent(),ComputeBodyForceModuli); 
        }
        else{ Msg::Error("Moduli used to compute body force is not defined");}        
      }
      
      if (!(ipvm->getConstitutiveSuccessFlag()) or !(ipvp->getConstitutiveSuccessFlag()))
      {
        Msg::Error("Constitutive evaluation is failed at interface ele %d, GP %d %d",ele->getNum(), j, j+npts);
        return false;
      }
    }  
  }
  else
  {  // Virtual interface element
     Msg::Error("No virtual interface for 3D");

  }
  return true;
}

int dG3DDomain::getStressDimension() const{
	int stresDim = 0;
  if (getHomogenizationOrder() == 1){
    stresDim += 9; // stress
  }
  else if (getHomogenizationOrder() == 2){
    stresDim +=  (9+27); // stress and higher order stress
  }
  else{
    Msg::Error("Homogenized order is not well defined %d",getHomogenizationOrder());
  }

  if (getExtractIrreversibleEnergyFlag()){
   stresDim += 1;
  }

  if (this->isExtractCohesiveLawFromMicroDamage()){
  // all stress
  // and FD
    stresDim+=9;
  }

  if(getNumConstitutiveExtraDofDiffusionVariable()>0)
  {
    // all mechanical stress + extra flux + source
    stresDim+=4*getNumConstitutiveExtraDofDiffusionVariable();
  }
  return stresDim;
}

bool dG3DDomain::getAccountPathFollowingFlag() const{
  return _accountPathFollowing;
};

void dG3DDomain::checkInternalState(IPField* ipf) const{
  #if defined(HAVE_MPI)
  if (_otherRanks.size()>0){
    // accumulation data from other ranks and send to rootRanks
    for (std::set<int>::iterator it = _domainIPBulk.begin(); it != _domainIPBulk.end(); it++){
      int num = *it;
      IPStateBase* ips = NULL;
      if (Msg::GetCommRank() == _rootRank){
        int elnum, gnum;
        numericalMaterialBase::getTwoIntsFromType(num,elnum,gnum);
        AllIPState::ipstateElementContainer *vips = ipf->getAips()->getIPstate(elnum);
        ips = (*vips)[gnum];
      }
      else if (_otherRanks.find(Msg::GetCommRank()) != _otherRanks.end()){
        AllIPState::ipstateElementContainer *vips = ipf->getAips()->getIPstate(num);
        ips = (*vips)[0];
      }
      IPVariable *ipv = ips->getState(IPStateBase::current);
      const IPVariable *ipvprev = ips->getState(IPStateBase::previous);
      this->getMaterialLaw()->checkInternalState(ipv,ipvprev);
      // check to send
      if (_otherRanks.find(Msg::GetCommRank()) != _otherRanks.end()){
        const dG3DMultiscaleIPVariable* ipvMult = static_cast<const dG3DMultiscaleIPVariable*>(ipv);
        const dG3DMultiscaleIPVariable* ipvMultPrev = static_cast<const dG3DMultiscaleIPVariable*>(ipvprev);
        if (ipvMult->solverIsBroken() and !ipvMultPrev->solverIsBroken()){
          ipf->solverBroken(num);
        }
      }
    };

    if (_fullDg){
      for (std::set<int>::iterator it = _domainIPInterfaceMinus.begin(); it != _domainIPInterfaceMinus.end(); it++){
        int num = *it;
        IPStateBase* ips = NULL;
        if (Msg::GetCommRank() == _rootRank){
          int elnum, gnum;
          numericalMaterialBase::getTwoIntsFromType(num,elnum,gnum);
          AllIPState::ipstateElementContainer *vips = ipf->getAips()->getIPstate(elnum);
          ips = (*vips)[gnum];
        }
        else if (_otherRanks.find(Msg::GetCommRank()) != _otherRanks.end()){
          AllIPState::ipstateElementContainer *vips = ipf->getAips()->getIPstate(num);
          ips = (*vips)[0];
        }
        IPVariable *ipv = ips->getState(IPStateBase::current);
        const IPVariable *ipvprev = ips->getState(IPStateBase::previous);
        this->getMaterialLawMinus()->checkInternalState(ipv,ipvprev);

        // check to send
        if (_otherRanks.find(Msg::GetCommRank()) != _otherRanks.end()){
          const dG3DMultiscaleIPVariable* ipvMult = NULL;
          const dG3DMultiscaleIPVariable* ipvMultPrev = NULL;
          const FractureCohesive3DIPVariable *fipv = dynamic_cast<const FractureCohesive3DIPVariable*>(ipv);
          if (fipv != NULL){
            const FractureCohesive3DIPVariable *fipvprev = static_cast<const FractureCohesive3DIPVariable*>(ipvprev);
            ipvMult = static_cast<const dG3DMultiscaleIPVariable*>(fipv->getIPvBulk());
            ipvMultPrev = static_cast<const dG3DMultiscaleIPVariable*>(fipvprev->getIPvBulk());
          }
          else{
            ipvMult = static_cast<const dG3DMultiscaleIPVariable*>(ipv);
            ipvMultPrev = static_cast<const dG3DMultiscaleIPVariable*>(ipvprev);
          }

          if (ipvMult->solverIsBroken() and !ipvMultPrev->solverIsBroken()){
            ipf->solverBroken(num);
          }
        }
      };


      for (std::set<int>::iterator it = _domainIPInterfacePlus.begin(); it != _domainIPInterfacePlus.end(); it++){
        int num = *it;
        IPStateBase* ips = NULL;
        if (Msg::GetCommRank() == _rootRank){
          int elnum,gnum;
          numericalMaterialBase::getTwoIntsFromType(num,elnum,gnum);
          AllIPState::ipstateElementContainer *vips = ipf->getAips()->getIPstate(elnum);
          ips = (*vips)[gnum];
        }
        else if (_otherRanks.find(Msg::GetCommRank()) != _otherRanks.end()){
          AllIPState::ipstateElementContainer *vips = ipf->getAips()->getIPstate(num);
          ips = (*vips)[0];
        }
        IPVariable *ipv = ips->getState(IPStateBase::current);
        const IPVariable *ipvprev = ips->getState(IPStateBase::previous);
        this->getMaterialLawPlus()->checkInternalState(ipv,ipvprev);

        // check to send
        if (_otherRanks.find(Msg::GetCommRank()) != _otherRanks.end()){
          const dG3DMultiscaleIPVariable* ipvMult = NULL;
          const dG3DMultiscaleIPVariable* ipvMultPrev = NULL;
          const FractureCohesive3DIPVariable *fipv = dynamic_cast<const FractureCohesive3DIPVariable*>(ipv);
          if (fipv != NULL){
            const FractureCohesive3DIPVariable *fipvprev = static_cast<const FractureCohesive3DIPVariable*>(ipvprev);
            ipvMult = static_cast<const dG3DMultiscaleIPVariable*>(fipv->getIPvBulk());
            ipvMultPrev = static_cast<const dG3DMultiscaleIPVariable*>(fipvprev->getIPvBulk());
          }
          else{
            ipvMult = static_cast<const dG3DMultiscaleIPVariable*>(ipv);
            ipvMultPrev = static_cast<const dG3DMultiscaleIPVariable*>(ipvprev);
          }

          if (ipvMult->solverIsBroken() and !ipvMultPrev->solverIsBroken()){
            ipf->solverBroken(num);
          }
        }
      }
    }
  }
  else
  #endif // HAVE_MPI
  {
    for (elementGroup::elementContainer::const_iterator it = this->element_begin(); it!= this->element_end(); it++){
      MElement* ele = it->second;
      IntPt* GP;
      int npts= integBulk->getIntPoints(ele,&GP);
      AllIPState::ipstateElementContainer *vips = ipf->getAips()->getIPstate(ele->getNum());
      for (int i=0; i< npts; i++){
        IPStateBase* ips = (*vips)[i];
        IPVariable *ipv = ips->getState(IPStateBase::current);
        const IPVariable *ipvprev = ips->getState(IPStateBase::previous);
        this->getMaterialLaw()->checkInternalState(ipv,ipvprev);
      }
    }
    if (_fullDg){
      for (elementGroup::elementContainer::const_iterator it = this->gi->begin(); it != this->gi->end(); it++){
        MElement* ele = it->second;
        IntPt* GP;
        int npts= integBound->getIntPoints(ele,&GP);
        AllIPState::ipstateElementContainer *vips = ipf->getAips()->getIPstate(ele->getNum());

        for (int i=0; i< npts; i++){
          IPStateBase* ipsm = (*vips)[i];
          // minus
          IPVariable* ipvm = ipsm->getState(IPStateBase::current);
          const IPVariable* ipvprevm =ipsm->getState(IPStateBase::previous);
          this->getMaterialLawMinus()->checkInternalState(ipvm,ipvprevm);

          IPStateBase* ipsp = (*vips)[i+npts];
          // plus
          IPVariable* ipvp = ipsp->getState(IPStateBase::current);
          const IPVariable* ipvprevp = ipsp->getState(IPStateBase::previous);
          this->getMaterialLawPlus()->checkInternalState(ipvp,ipvprevp);
        }
      }
    }
  }
};

void dG3DDomain::checkFailure(IPField* ipf) const{
  // Check and manage insertion of cohesive element

  const dG3DMaterialLaw *mlawminus = static_cast<const dG3DMaterialLaw*>(this->getMaterialLawMinus());
  const dG3DMaterialLaw *mlawplus = static_cast<const dG3DMaterialLaw*>(this->getMaterialLawPlus());

  // Check failure only if needed (if DG and fracture law are used)
  if (_fullDg and (mlawminus->getType() == materialLaw::fracture and mlawplus->getType() == materialLaw::fracture))
  {
    // Get mlaw
      // in minus part
    const FractureByCohesive3DLaw * flawMinus = static_cast<const FractureByCohesive3DLaw*>(mlawminus);
    const Cohesive3DLaw *mclaw = static_cast<const Cohesive3DLaw*>(flawMinus->getFractureLaw());
    const dG3DMaterialLaw* bulkLawMinus =static_cast<const dG3DMaterialLaw*>(flawMinus->getBulkLaw());
      // in plus part
    const FractureByCohesive3DLaw* flawPlus = static_cast<const FractureByCohesive3DLaw*>(mlawplus);
    const dG3DMaterialLaw* bulkLawPlus = static_cast<const dG3DMaterialLaw*>(flawPlus->getBulkLaw());

    // if only domain work only on rootranks
    // Loop on interface elements to check failure
    for(elementGroup::elementContainer::const_iterator it=gi->begin(); it!=gi->end(); ++it){
      MElement *ele = it->second;
      MInterfaceElement *ie = dynamic_cast<MInterfaceElement*>(ele);

      int numElMinus = ie->getElem(0)->getNum();
      int numElPlus = ie->getElem(1)->getNum();


      // Get ipstate container of the given interface element
      IntPt* GP;
      int npts=integBound->getIntPoints(ele,&GP);
      AllIPState::ipstateElementContainer *vips = ipf->getAips()->getIPstate(ele->getNum());


      // Synchronise ipv deletion on both sides of an interface element
        // loop on ips
      for(int j=0;j<npts;j++){
        // get ipv_frac
        IPStateBase* ipsm = (*vips)[j];
        IPStateBase* ipsp = (*vips)[j+npts];
        FractureCohesive3DIPVariable *ipvfm = static_cast<FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::current));
        FractureCohesive3DIPVariable *ipvfp = static_cast<FractureCohesive3DIPVariable*>(ipsp->getState(IPStateBase::current));
        const FractureCohesive3DIPVariable *ipvfprevm = static_cast<const FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::previous));
        const FractureCohesive3DIPVariable *ipvfprevp = static_cast<const FractureCohesive3DIPVariable*>(ipsp->getState(IPStateBase::previous));

        // force ipv of each side to be deleted if one of them is already deleted
        if (!ipvfprevm->isDeleted())
        {
          if (ipvfm->isDeleted()){
            Msg::Info("dG3DDomain::checkFailure: a couple of IP was deleted at one interface element");
            ipvfp->Delete(true);

          }
          else if (ipvfp->isDeleted()){
            ipvfm->Delete(true);
            Msg::Info("dG3DDomain::checkFailure: a couple of IP was deleted at one interface element");
          }
        }
      }




      // check broken state when element is not broken yet from previous state
      if (!(ipf->elementIsBroken(ele))){
        // if element is not yet broken
        bool willCheckFailure = true;
        if (_imposeCrackFlag){
          // if crack path is forced following some interfaces
          willCheckFailure =false;
          if (_imposedInterfaceCrack.size() > 0){
            TwoNum key(numElMinus, numElPlus);
            if (_imposedInterfaceCrack.find(key) != _imposedInterfaceCrack.end()){
              willCheckFailure = true;
              printf("checking failure at interface between ele %d  and ele %d \n",numElMinus, numElPlus);
            }
          }
        }

        if (willCheckFailure){
          bool interfaceElementBroken = false; // element broken flag (said element is broken if all Ip are already broken)

          std::set<int> brokenIPs; // all broken IPs

          /* Check for cohesive insertion on ip's of the interface element */
          for(int j=0;j<npts;j++){
            IPStateBase* ipsm = (*vips)[j];
            IPStateBase* ipsp = (*vips)[j+npts];

            const FractureCohesive3DIPVariable *ipvfprevm = static_cast<const FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::previous));
            const FractureCohesive3DIPVariable *ipvfprevp = static_cast<const FractureCohesive3DIPVariable*>(ipsp->getState(IPStateBase::previous));
            FractureCohesive3DIPVariable *ipvfm = static_cast<FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::current));
            FractureCohesive3DIPVariable *ipvfp = static_cast<FractureCohesive3DIPVariable*>(ipsp->getState(IPStateBase::current));

            // if previously broken
            if (ipvfprevm->isbroken()){
              ipvfm->broken();
              ipvfp->broken();
            }
            else{
              // check for current state
              if (!ipvfm->isbroken()){
                mclaw->checkCohesiveInsertion(ipsm,ipsp);
                if(ipvfm->isbroken()){
                  // synchonize data when one ipv is broken
                  ipf->brokenIP(numericalMaterialBase::createTypeWithTwoInts(ele->getNum(),j));
                  ipf->brokenIP(numericalMaterialBase::createTypeWithTwoInts(ele->getNum(),j+npts));
                  //printf("Checked: failed IPs, ipminus %d ip plus %d of interface %ld between elements %d %d \n",j,j+npts,ele->getNum(),numElMinus,numElPlus);
                };
              }
            };

            // Count the number of currently broken ipv
            if(ipvfm->isbroken()){
              brokenIPs.insert(j);
            };
          }
          int nbIpVBroken = brokenIPs.size();       // Number of IPv broken at the interface element


          if (nbIpVBroken ==npts) // all GP are broken
          {
            interfaceElementBroken = true; // interfaceElement is naturally broken
          }

          // Check if forcing is alowed and not already done
          if (_forceInterfaceElementBroken and !interfaceElementBroken)
          {
            // Determine the needed number of broken ipv before forcing
            int nptsToBreakInterfaceElement(0);
            if (_failureIPRatio <= 0.)
            {
              // break interface element when cohesive IP is inserted in first IP
              nptsToBreakInterfaceElement = 1;
            }
            else if (_failureIPRatio >= 1.)
            {
              // break interface element when cohesive IPs are inserted in all IP
              nptsToBreakInterfaceElement = npts;
            }
            else
            {
              // break interface element when cohesive IPs are inserted in agiven number of IPs
              nptsToBreakInterfaceElement = npts*_failureIPRatio;
              if (nptsToBreakInterfaceElement < 1) nptsToBreakInterfaceElement = 1;
            }

            // Check if a sufficient number of IP's are broken
            if(nbIpVBroken >= nptsToBreakInterfaceElement)
            {
              interfaceElementBroken = true; // element is force to break
              // Force cohesive insertion at all cohesive ipv if not already broken
              for(int j=0;j<npts;j++)
              {
                if (brokenIPs.find(j) == brokenIPs.end()){
                  // Get Ipstate
                  IPStateBase* ipsm = (*vips)[j];
                  IPStateBase* ipsp = (*vips)[j+npts];
                  // Check if failure already detected and force insertion if needed
                  FractureCohesive3DIPVariable *ipvfm = static_cast<FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::current));
                    // force broken all remaining IPVs
                  mclaw->checkCohesiveInsertion(ipsm,ipsp,true);
                  ipf->brokenIP(numericalMaterialBase::createTypeWithTwoInts(ele->getNum(),j));
                  ipf->brokenIP(numericalMaterialBase::createTypeWithTwoInts(ele->getNum(),j+npts));

                  printf("Forced: failed IPs, ipminus %d ip plus %d of interface %ld between elements %d %d \n",j,j+npts,ele->getNum(),numElMinus,numElPlus);
                }
              }
            }
          }

          // Control of crack insertion
            // If an interface element is totally broken at this step
          if (interfaceElementBroken){
            // add to the list of newly cracked elements
            ipf->addOneBrokenElement(ele);
          }

          // Control damage blocking inside bulk elements in terms of chosen method
          bool blockBulkDamage = false; // true if damage is blocked in bulk elements
          if (_bulkDamageBlockMethod == 0)
          {
           // block damage when cohesive crack is inserted in first IP
           if (nbIpVBroken > 0) blockBulkDamage = true;
          }
          else if (_bulkDamageBlockMethod == 1)
          {
            // block damage when cohesive crack is interface element is broken
            if (interfaceElementBroken) blockBulkDamage = true;
          }
          else
          {
            // no block damage in bulk ips
            blockBulkDamage = false;
          }


          // Block damage in bulk in Plus and Minus bulk elements when using
          // bulk material with damage if needed
          if (blockBulkDamage){
            if (bulkLawMinus->withEnergyDissipation()){
              // Get bulk element and check if it exists
              const partDomain* dom = this->getMinusDomain();
              MElement* em = ie->getElem(0);
              if(dom->g_find(em) and (!ipf->dissipationIsBlockedOnElement(em))){
                ipf->addOneBlockedDissipationElement(em);
                // Loop on bulk ipv of the element
                int npts_bulk = dom->getBulkGaussIntegrationRule()->getIntPoints(em,&GP);
                AllIPState::ipstateElementContainer *vips_bulk = ipf->getAips()->getIPstate(em->getNum());
                for(int k=0;k<npts_bulk;k++)
                {
                  // Update critical damage and flag in bulk ipv's
                  IPStateBase* ips_bulk = (*vips_bulk)[k];
                  dG3DIPVariableBase *ipv_bulk = static_cast<dG3DIPVariableBase*>(ips_bulk->getState(IPStateBase::current));
                  const dG3DIPVariableBase* ipv_bulkPrev = static_cast<const dG3DIPVariableBase*>(ips_bulk->getState(IPStateBase::previous));

                  if (ipv_bulkPrev->dissipationIsBlocked()){
                    ipv_bulk->blockDissipation(true);
                  }
                  else{
                    ipv_bulk->blockDissipation(true);
                    ipf->blockDissipationIP(numericalMaterialBase::createTypeWithTwoInts(em->getNum(),k));
                  }
                }
              }
            }

            if (bulkLawPlus->withEnergyDissipation())
            {
              const partDomain* dom = this->getPlusDomain();
              MElement* ep = ie->getElem(1);
              if(dom->g_find(ep) and (!ipf->dissipationIsBlockedOnElement(ep)))
              {
                ipf->addOneBlockedDissipationElement(ep);
                // Loop on bulk ipv of the element
                int npts_bulk = dom->getBulkGaussIntegrationRule()->getIntPoints(ep,&GP);
                AllIPState::ipstateElementContainer *vips_bulk = ipf->getAips()->getIPstate(ep->getNum());
                for(int k=0;k<npts_bulk;k++)
                {
                  // Update critical damage and flag in bulk ipv's
                  IPStateBase* ips_bulk = (*vips_bulk)[k];
                  dG3DIPVariableBase *ipv_bulk = static_cast<dG3DIPVariableBase*>(ips_bulk->getState(IPStateBase::current));
                  const dG3DIPVariableBase* ipv_bulkPrev = static_cast<const dG3DIPVariableBase*>(ips_bulk->getState(IPStateBase::previous));

                  if (ipv_bulkPrev->dissipationIsBlocked()){
                    ipv_bulk->blockDissipation(true);
                  }
                  else{
                    ipv_bulk->blockDissipation(true);
                    ipf->blockDissipationIP(numericalMaterialBase::createTypeWithTwoInts(ep->getNum(),k));
                  }
                }
              }
            }
          }
        }
      }
      #ifdef _DEBUG
      else{
        printf("element %ld with negative %d, positive %d is fully broken\n", ele->getNum(),numElMinus,numElPlus);
      }
      #endif //_DEBUG
    }
  }


  // block damage in elements which are not related to crack mouth
  if (this->elementGroupSize() > 0 and ((_bulkDamageBlockMethod ==0) or (_bulkDamageBlockMethod ==1))){
    const materialLaw* mlaw = this->getMaterialLaw();
    if (mlaw->withEnergyDissipation()){
      IntPt* GP;
      for (elementGroup::elementContainer::const_iterator ite = this->element_begin(); ite!= this->element_end(); ite++){
        MElement* ele = ite->second;

        if (!ipf->dissipationIsBlockedOnElement(ele)){
          int npts_bulk = this->getBulkGaussIntegrationRule()->getIntPoints(ele,&GP);
          AllIPState::ipstateElementContainer *vips_bulk = ipf->getAips()->getIPstate(ele->getNum());

          // if one IP is broken, block all element
          bool willBlock = false;
          for(int k=0;k<npts_bulk;k++){
            IPStateBase* ips_bulk = (*vips_bulk)[k];
            dG3DIPVariableBase *ipv_bulk = static_cast<dG3DIPVariableBase*>(ips_bulk->getState(IPStateBase::current));
            const dG3DIPVariableBase *ipvprev_bulk = static_cast<const dG3DIPVariableBase*>(ips_bulk->getState(IPStateBase::previous));

            if (ipvprev_bulk->dissipationIsBlocked() or mlaw->brokenCheck(ipv_bulk)){
              willBlock = true;
              break;
            }
          }

          if (willBlock){
            ipf->addOneBlockedDissipationElement(ele);
            // block all IP
            for(int k=0;k<npts_bulk;k++){
              IPStateBase* ips_bulk = (*vips_bulk)[k];
              dG3DIPVariableBase *ipv_bulk = static_cast<dG3DIPVariableBase*>(ips_bulk->getState(IPStateBase::current));
              const dG3DIPVariableBase *ipvprev_bulk = static_cast<const dG3DIPVariableBase*>(ips_bulk->getState(IPStateBase::previous));
              if (ipvprev_bulk->dissipationIsBlocked()){
                ipv_bulk->blockDissipation(true);
              }
              else{
                ipv_bulk->blockDissipation(true);
                ipf->blockDissipationIP(numericalMaterialBase::createTypeWithTwoInts(ele->getNum(),k));
              }
            }
          }
        }
      }
    }
  }

};



void dG3DDomain::initialIPVariable(AllIPState *aips,const unknownField *ufield, const IPStateBase::whichState ws, bool stiff)
{
  dG3DMaterialLaw* mlawMinus = dynamic_cast<dG3DMaterialLaw*>(this->getMaterialLawMinus());
  dG3DMaterialLaw* mlawPlus = dynamic_cast<dG3DMaterialLaw*>(this->getMaterialLawPlus());

  // compute strain
  this->computeAllIPStrain(aips,ufield,ws,false);
  //
  IntPt *GP;  
  for(elementGroup::elementContainer::const_iterator it=gi->begin(); it!=gi->end(); ++it)
  {
    MElement* ele = it->second;
    AllIPState::ipstateElementContainer *vips = aips->getIPstate(ele->getNum());
    int npts=integBound->getIntPoints(ele,&GP);
    // Grads values on all Gauss points;
        
    for(int j=0;j<npts;j++)
    {
      IPStateBase* ipsm = (*vips)[j];
      IPStateBase* ipsp = (*vips)[j+npts];

      dG3DIPVariableBase* ipvm = static_cast<dG3DIPVariableBase*>(ipsm->getState(ws));
      dG3DIPVariableBase* ipvp = static_cast<dG3DIPVariableBase*>(ipsp->getState(ws));

			if (mlawMinus!=NULL){
				mlawMinus->initialIPVariable(ipvm,stiff);
			}
			else{
				Msg::Error("Minus material is NULL dG3DDomain::initialIPVariable");
			}

			if (mlawPlus!=NULL){
				mlawPlus->initialIPVariable(ipvp,stiff);
			}
			else{
				Msg::Error("Plus material is NULL dG3DDomain::initialIPVariable");
			}
    }
  }
  // loop on element
  fullVector<double> disp;
  dG3DMaterialLaw* mlaw = dynamic_cast<dG3DMaterialLaw*>(this->getMaterialLaw());
  for(elementGroup::elementContainer::const_iterator it=g->begin(); it!=g->end(); ++it)
  {
    MElement *ele = it->second;
    int npts = integBulk->getIntPoints(ele,&GP);
    AllIPState::ipstateElementContainer *vips = aips->getIPstate(ele->getNum());

    for(int j=0;j<npts;j++){
      dG3DIPVariableBase *ipv = static_cast<dG3DIPVariableBase*>((*vips)[j]->getState(ws));
      if (mlaw!=NULL){
        mlaw->initialIPVariable(ipv,stiff);
      };
    }
  }
}
void dG3DDomain::setGaussIntegrationRule(){
  if (this->g->size()>0)
  {
    elementGroup::elementContainer::const_iterator it = g->begin();
    MElement *e = it->second;
    // default case bulk
    if(_gaussorderbulk == -1)
    {
      if (getFunctionSpaceType() == functionSpaceType::Hierarchical)
      {
        HierarchicalFunctionSpace* hspace = dynamic_cast<HierarchicalFunctionSpace*>(getFunctionSpace());
        int maxOrder = hspace->getMaxOrderBasis();
        QuadratureFactory::createBulkGaussQuadratureHierarchicalFE(maxOrder,e,integBulk);
      }
      else
      {
        QuadratureFactory::createBulkGaussQuadrature(e,integBulk);
      }
    }
    else
    {
      integBulk = new GaussQuadrature(_gaussorderbulk);
    }

    IntPt* GP;
    int npts = integBulk->getIntPoints(e,&GP);
    Msg::Info("bulk: use %d integration points for element type %d",npts,e->getType());
  }
  else
  {
    integBulk = new QuadratureVoid();
  }
    // default case bound

  if (gi->size() > 0)
  {
    elementGroup::elementContainer::const_iterator it = gi->begin();
    MElement *ie = it->second;
    if(_gaussorderbound == -1)
    {
      if ((getMinusDomain()->getFunctionSpaceType() == functionSpaceType::Hierarchical) and 
          (getPlusDomain()->getFunctionSpaceType() == functionSpaceType::Hierarchical))
      {
        HierarchicalFunctionSpace* hspaceMinus = dynamic_cast<HierarchicalFunctionSpace*>(getMinusDomain()->getFunctionSpace());
        HierarchicalFunctionSpace* hspacePlus = dynamic_cast<HierarchicalFunctionSpace*>(getPlusDomain()->getFunctionSpace());
        int maxOrder = std::max(hspaceMinus->getMaxOrderBasis(),hspacePlus->getMaxOrderBasis());
        if( _gqt == Gauss)
        {
          QuadratureFactory::createInterfaceGaussQuadratureHierarchicalFE(maxOrder,ie,integBound,_interQuad);          
        }
        else if (_gqt == Lobatto)
        {
          QuadratureFactory::createInterfaceLobattoQuadratureHierarchicalFE(maxOrder,ie,integBound,_interQuad);
        }
        else
        {
          Msg::Error("interface quadarature method %d has not been implemented",_gqt);
        }
      }
      else
      {
        if( _gqt == Gauss)
        {
          QuadratureFactory::createInterfaceGaussQuadrature(ie,integBound,_interQuad);          
        }
        else if (_gqt == Lobatto)
        {
          QuadratureFactory::createInterfaceLobattoQuadrature(ie,integBound,_interQuad);
        }
        else
        {
          Msg::Error("interface quadarature method %d has not been implemented",_gqt);
        }
      }
    }
    else{
      if(getDim()==3){
        _gqt == Gauss ? integBound = new GaussQuadrature(_gaussorderbound) : integBound = new GaussLobatto2DQuadrature(_gaussorderbound);
        _gqt == Gauss ? _interQuad = new interface2DQuadrature(_gaussorderbound,false) : _interQuad = new interface2DQuadrature(_gaussorderbound,true);
      }
      else{
        integBound = new GaussQuadrature(_gaussorderbound);
        _interQuad = new interface1DQuadrature(_gaussorderbound);
      }
    }

    IntPt* GP;
    int npts = integBound->getIntPoints(ie,&GP);
    Msg::Info("interface: use %d integration points for element type %d",npts,ie->getType());

  }
  else{

    integBound = new QuadratureVoid();
    _interQuad = new interfaceQuadratureVoid();
  }
}

void dG3DDomain::allocateInterfaceMPI(const int rankOtherPart, const elementGroup &groupOtherPart,dgPartDomain** dgdom)
{
  // domain creation has it function create a domain with a part of the same domain contained on an other part,
  // the type of domain is known
  if(*dgdom==NULL){ // domain doesn't exist --> creation
    (*dgdom) = new interDomainBetween3D(this->getTag(),this,this,0);
    // set the stability parameters
    interDomainBetween3D* newInterDom3D = static_cast< interDomainBetween3D* >(*dgdom);
    newInterDom3D->stabilityParameters(this->stabilityParameter(1));
    // set other options from domain
    newInterDom3D->setBulkDamageBlockedMethod(_bulkDamageBlockMethod);
    newInterDom3D->strainSubstep(_subSteppingMethod,_subStepNumber);
    newInterDom3D->forceCohesiveInsertionAllIPs(_forceInterfaceElementBroken,_failureIPRatio);
    newInterDom3D->setNonLocalStabilityParameters(this->getNonLocalStabilityParameter(),this->getNonLocalContinuity());
    newInterDom3D->setNonLocalEqRatio(this->getNonLocalEqRatio());

    newInterDom3D->setConstitutiveExtraDofDiffusionStabilityParameters(this->getConstitutiveExtraDofDiffusionStabilityParameter(), this->getConstitutiveExtraDofDiffusionContinuity());
    newInterDom3D->setConstitutiveExtraDofDiffusionEqRatio(this->getConstitutiveExtraDofDiffusionEqRatio());
    newInterDom3D->setConstitutiveExtraDofDiffusionUseEnergyConjugatedField(this->getConstitutiveExtraDofDiffusionUseEnergyConjugatedField());
    newInterDom3D->setConstitutiveExtraDofDiffusionAccountSource(this->getConstitutiveExtraDofDiffusionAccountFieldSource(),this->getConstitutiveExtraDofDiffusionAccountMecaSource());

    newInterDom3D->setConstitutiveCurlEqRatio(this->getConstitutiveCurlEqRatio());
    newInterDom3D->setConstitutiveCurlAccountSource(this->getConstitutiveCurlAccountFieldSource(),this->getConstitutiveCurlAccountMecaSource());

    newInterDom3D->setIncrementNonlocalBased(_incrementNonlocalBased);
		newInterDom3D->setPlaneStressState(_planeStressState);
  }
}


// copy paste of linear change this !!
void dG3DDomain::createInterfaceMPI(const int rankOtherPart, const elementGroup &groupOtherPart,dgPartDomain* &dgdom)
{
  // domain creation has it function create a domain with a part of the same domain contained on an other part,
  // the type of domain is known
  allocateInterfaceMPI(rankOtherPart, groupOtherPart,&dgdom);
  // create interface (no need to use an interface builder ??)

  if (getDim() ==2){
    std::set<IEdge> map_edge;// the type Iedge is known
    // create interface with groupOtherPart (no match possible match has to be done with dom->gib)
    for(elementGroup::elementContainer::const_iterator it = groupOtherPart.begin(); it != groupOtherPart.end(); ++it)
    {
      MElement *e=it->second;
      int nedge = e->getNumEdges();
      for(int j=0;j<nedge;j++){
        std::vector<MVertex*> vv;
        e->getEdgeVertices(j,vv);
        //map_edge.emplace(vv,e,this);
        IEdge *ie = new IEdge(vv,e,this);
        std::set<IEdge>::const_iterator itie=map_edge.find(*ie);
        if(itie==map_edge.end())
          map_edge.insert(*ie);
        delete ie;
      }
    }
    // now use interface created on boundary to identify the interface element
    for(elementGroup::elementContainer::const_iterator it=giv->begin(); it!=giv->end(); ++it) // loop on gib too (normally no need)
    {
      MInterfaceElement *interele = dynamic_cast<MInterfaceElement*>(it->second);
      MElement *e = interele->getElem(0); // on boundary interface geetElem(0) == getElem(1)
      int nedge = e->getNumEdges();
      for(int j=0;j<nedge;j++){
        std::vector<MVertex*> vv;
        e->getEdgeVertices(j,vv);
        IEdge ie(vv,e,this);
        std::set<IEdge>::iterator it_edge = map_edge.find(ie);
        if(it_edge != map_edge.end()){
          MElement *interel;
          // the partition with the lowest number is choosen by convention like the one corresponding to minus element
          if(dgdom->getPhysical() == _phys){
            if(Msg::GetCommRank() < rankOtherPart){
              interel =  new MInterfaceLine(vv, 0, Msg::GetCommRank()+1, e, it_edge->getElement());
            }
            else{
              // vv has to be oriented has the minus element --> get the
              std::vector<MVertex*> vvother  = it_edge->getVertices();
              interel = new MInterfaceLine(vvother,0,Msg::GetCommRank()+1,it_edge->getElement(),e);
            }
            _groupGhostMPI->insert(it_edge->getElement());
          }
          else{ // interDomain defined between 2 partitions. The minus element has to be included in the minus domain
            elementGroup::elementContainer::const_iterator itfound = dgdom->getMinusDomain()->element_end();
            for(elementGroup::elementContainer::const_iterator ite=dgdom->getMinusDomain()->element_begin(); ite!=dgdom->getMinusDomain()->element_end();++ite)
            {
              if(e->getNum()==(ite->second)->getNum())
              {
                itfound = ite;
                break;
              }
            }
            if(itfound!=dgdom->getMinusDomain()->element_end())
          {
              interel =  new MInterfaceLine(vv, 0, Msg::GetCommRank()+1, e, it_edge->getElement());
              // add element of other partition to prescribed Dirichlet BC later
              if(e->getPartition() != Msg::GetCommRank()+1)
                 dgdom->getMinusDomain()->_groupGhostMPI->insert(e);
              else
                 dgdom->getPlusDomain()->_groupGhostMPI->insert(it_edge->getElement());

            }
            else{ // e is included in the plus Domain
              // vv has to be oriented has the minus element --> get the
              std::vector<MVertex*> vvother  = it_edge->getVertices();
              interel = new MInterfaceLine(vvother,0,Msg::GetCommRank()+1,it_edge->getElement(),e);
              // add element of other partition to prescribed Dirichlet BC later
              if(e->getPartition() != Msg::GetCommRank()+1)
                 dgdom->getPlusDomain()->_groupGhostMPI->insert(e);
              else
                 dgdom->getMinusDomain()->_groupGhostMPI->insert(it_edge->getElement());
            }
          }
          dgdom->gi->insert(interel);
          // add element of other partition to prescribed Dirichlet BC later
          map_edge.erase(it_edge);
        }
      }
    }
  }
  else if (getDim() == 3) {
    std::set<IFace> map_face;// the type Iedge is known
    // create interface with groupOtherPart (no match possible match has to be done with dom->gib)
    for(elementGroup::elementContainer::const_iterator it = groupOtherPart.begin(); it != groupOtherPart.end(); ++it)
    {
      MElement *e=it->second;
      int nface = e->getNumFaces();
      for(int j=0;j<nface;j++){
        std::vector<MVertex*> vv;
        e->getFaceVertices(j,vv);
        //map_face.emplace(vv,e,this);
        IFace *ie = new IFace(vv,e,this);
        std::set<IFace>::const_iterator itie=map_face.find(*ie);
        if(itie==map_face.end())
          map_face.insert(*ie);
        delete ie;

      }
    }
    // now use interface created on boundary to identify the interface element
    for(elementGroup::elementContainer::const_iterator it=giv->begin(); it!=giv->end(); ++it) // loop on gib too (normally no need)
    {
      MInterfaceElement *interele = dynamic_cast<MInterfaceElement*>(it->second);
      MElement *e = interele->getElem(0); // on boundary interface geetElem(0) == getElem(1)
      int nface = e->getNumFaces();
      for(int j=0;j<nface;j++)
      {
        std::vector<MVertex*> vv;
        e->getFaceVertices(j,vv);
        IFace ie(vv,e,this);
        std::set<IFace>::iterator it_face = map_face.find(ie);
        if(it_face != map_face.end()){
          MElement *interel;
          // the partition with the lowest number is choosen by convention like the one corresponding to minus element
          if(_phys == dgdom->getPhysical())
          {
            if(Msg::GetCommRank() < rankOtherPart)
            {
              if(e->getTypeForMSH() == MSH_TET_4)
              {
                interel =  new MInterfaceTriangleN(vv, 1, 0, Msg::GetCommRank()+1, e, it_face->getElement());
              }
              else if(e->getTypeForMSH() == MSH_TET_10)
              {
                interel =  new MInterfaceTriangleN(vv, 2, 0, Msg::GetCommRank()+1, e, it_face->getElement());
              }
              else if(e->getTypeForMSH() == MSH_HEX_8)
              {
                interel =  new MInterfaceQuadrangleN(vv, 1, 0, Msg::GetCommRank()+1, e, it_face->getElement());
              }
              else if(e->getTypeForMSH() == MSH_HEX_27)
              {
                interel =  new MInterfaceQuadrangleN(vv, 2, 0, Msg::GetCommRank()+1, e, it_face->getElement());
              }
              else if(e->getTypeForMSH() == MSH_TET_20)
              {
                interel =  new MInterfaceTriangleN(vv, 3, 0, Msg::GetCommRank()+1, e, it_face->getElement());
              }
              else if (e->getTypeForMSH() == MSH_PRI_6)
              {
                if (vv.size() == 3){
                  interel =  new MInterfaceTriangleN(vv, 1, 0, Msg::GetCommRank()+1, e, it_face->getElement());
                }
                else if (vv.size() == 4){
                  interel =  new MInterfaceQuadrangleN(vv, 1, 0, Msg::GetCommRank()+1, e, it_face->getElement());
                }
              }
              else if (e->getTypeForMSH() == MSH_PRI_18){
                if (vv.size() == 6){
                  interel =  new MInterfaceTriangleN(vv, 2, 0, Msg::GetCommRank()+1, e, it_face->getElement());
                }
                else if (vv.size() == 9){
                  interel =  new MInterfaceQuadrangleN(vv, 2, 0, Msg::GetCommRank()+1, e, it_face->getElement());
                }
              }
              else
              {
                Msg::Error("dG3DDomain::createInterfaceMPI");
              }
            }
            else
            {
              // vv has to be oriented has the minus element --> get the
              std::vector<MVertex*> vvother  = it_face->getVertices();
              if(it_face->getElement()->getTypeForMSH() == MSH_TET_4)
              {
                interel =  new MInterfaceTriangleN(vvother,1, 0,Msg::GetCommRank()+1,it_face->getElement(),e);
              }
              else if(it_face->getElement()->getTypeForMSH() == MSH_TET_10)
              {
                interel =  new MInterfaceTriangleN(vvother,2, 0,Msg::GetCommRank()+1,it_face->getElement(),e);
              }
              else if(it_face->getElement()->getTypeForMSH() == MSH_HEX_8)
              {
                interel =  new MInterfaceQuadrangleN(vvother,1,0,Msg::GetCommRank()+1,it_face->getElement(),e);
              }
              else if(it_face->getElement()->getTypeForMSH() == MSH_HEX_27)
              {
                interel =  new MInterfaceQuadrangleN(vvother,2,0,Msg::GetCommRank()+1,it_face->getElement(),e);
              }
              else if(it_face->getElement()->getTypeForMSH() == MSH_TET_20)
              {
                interel =  new MInterfaceTriangleN(vvother,3, 0,Msg::GetCommRank()+1,it_face->getElement(),e);
              }
              else if (it_face->getElement()->getTypeForMSH() == MSH_PRI_6){
                if (vvother.size() == 3)
                  interel =  new MInterfaceTriangleN(vvother,1, 0,Msg::GetCommRank()+1,it_face->getElement(),e);
                else if (vvother.size() == 4)
                  interel =  new MInterfaceQuadrangleN(vvother,1,0,Msg::GetCommRank()+1,it_face->getElement(),e);
              }
              else if (it_face->getElement()->getTypeForMSH() == MSH_PRI_18){
                if (vvother.size() == 6)
                  interel =  new MInterfaceTriangleN(vvother,2, 0,Msg::GetCommRank()+1,it_face->getElement(),e);
                else if (vvother.size() == 9)
                  interel =  new MInterfaceQuadrangleN(vvother,2,0,Msg::GetCommRank()+1,it_face->getElement(),e);
              }

              else
              {
                Msg::Error("dG3DDomain::createInterfaceMPI");
              }
            }
            // add element of other partition to prescribed Dirichlet BC later
            _groupGhostMPI->insert(it_face->getElement());
          }
          else{ // interDomain defined between 2 partitions the minus element has to be included in the minus domain
                 // idem when filling _groupGhsotMPI
            elementGroup::elementContainer::const_iterator itfound = dgdom->getMinusDomain()->element_end();
            for(elementGroup::elementContainer::const_iterator ite=dgdom->getMinusDomain()->element_begin(); ite!=dgdom->getMinusDomain()->element_end();++ite)
            {
              if(e->getNum()==(ite->second)->getNum())
              {
                itfound = ite;
                break;
              }
            }
            if(itfound!=dgdom->getMinusDomain()->element_end())
            {
              if(e->getTypeForMSH() == MSH_TET_4)
              {
                interel =  new MInterfaceTriangleN(vv, 1, 0, Msg::GetCommRank()+1, e, it_face->getElement());
              }
              else if(e->getTypeForMSH() == MSH_TET_10)
              {
                interel =  new MInterfaceTriangleN(vv, 2, 0, Msg::GetCommRank()+1, e, it_face->getElement());
              }
              else if(e->getTypeForMSH() == MSH_HEX_8)
              {
                interel =  new MInterfaceQuadrangleN(vv, 1, 0, Msg::GetCommRank()+1, e, it_face->getElement());
              }
              else if(e->getTypeForMSH() == MSH_HEX_27)
              {
                interel =  new MInterfaceQuadrangleN(vv, 2, 0, Msg::GetCommRank()+1, e, it_face->getElement());
              }
              else if(e->getTypeForMSH() == MSH_TET_20)
              {
                interel =  new MInterfaceTriangleN(vv, 3, 0, Msg::GetCommRank()+1, e, it_face->getElement());
              }
              else if(e->getTypeForMSH() == MSH_PRI_6)
              {
                if (vv.size() == 3)
                  interel =  new MInterfaceTriangleN(vv, 1, 0, Msg::GetCommRank()+1, e, it_face->getElement());
                else if (vv.size()== 4)
                  interel =  new MInterfaceQuadrangleN(vv, 1, 0, Msg::GetCommRank()+1, e, it_face->getElement());
              }
              else if(e->getTypeForMSH() == MSH_PRI_18)
              {
                if (vv.size() == 6)
                  interel =  new MInterfaceTriangleN(vv, 2, 0, Msg::GetCommRank()+1, e, it_face->getElement());
                else if (vv.size()== 9)
                  interel =  new MInterfaceQuadrangleN(vv, 2, 0, Msg::GetCommRank()+1, e, it_face->getElement());
              }
              else
              {
                Msg::Error("dG3DDomain::createInterfaceMPI");
              }
              // add element of other partition to prescribed Dirichlet BC later
              if(e->getPartition() != Msg::GetCommRank()+1)
                 dgdom->getMinusDomain()->_groupGhostMPI->insert(e);
              else
                 dgdom->getPlusDomain()->_groupGhostMPI->insert(it_face->getElement());

            }
            else
            {
               // vv has to be oriented has the minus element --> get the
              std::vector<MVertex*> vvother  = it_face->getVertices();
              if(it_face->getElement()->getTypeForMSH() == MSH_TET_4)
              {
                interel =  new MInterfaceTriangleN(vvother,1, 0,Msg::GetCommRank()+1,it_face->getElement(),e);
              }
              else if(it_face->getElement()->getTypeForMSH() == MSH_TET_10)
              {
                interel =  new MInterfaceTriangleN(vvother,2, 0,Msg::GetCommRank()+1,it_face->getElement(),e);
              }
              else if(it_face->getElement()->getTypeForMSH() == MSH_HEX_8)
              {
                interel =  new MInterfaceQuadrangleN(vvother,1,0,Msg::GetCommRank()+1,it_face->getElement(),e);
              }
              else if(it_face->getElement()->getTypeForMSH() == MSH_HEX_27)
              {
                interel =  new MInterfaceQuadrangleN(vvother,2,0,Msg::GetCommRank()+1,it_face->getElement(),e);
              }
              else if(it_face->getElement()->getTypeForMSH() == MSH_TET_20)
              {
                interel =  new MInterfaceTriangleN(vvother,3, 0,Msg::GetCommRank()+1,it_face->getElement(),e);
              }
              else if(it_face->getElement()->getTypeForMSH() == MSH_PRI_6)
              {
                if (vvother.size() == 3)
                  interel =  new MInterfaceTriangleN(vvother,1, 0,Msg::GetCommRank()+1,it_face->getElement(),e);
                else if (vv.size() == 4)
                  interel =  new MInterfaceQuadrangleN(vvother,1,0,Msg::GetCommRank()+1,it_face->getElement(),e);
              }
              else if(it_face->getElement()->getTypeForMSH() == MSH_PRI_18)
              {
                if (vvother.size() == 6)
                  interel =  new MInterfaceTriangleN(vvother,2, 0,Msg::GetCommRank()+1,it_face->getElement(),e);
                else if (vv.size() == 9)
                  interel =  new MInterfaceQuadrangleN(vvother,2,0,Msg::GetCommRank()+1,it_face->getElement(),e);
              }
              else
              {
                Msg::Error("dG3DDomain::createInterfaceMPI");
              }
              // add element of other partition to prescribed Dirichlet BC later
              if(e->getPartition() != Msg::GetCommRank()+1)
                 dgdom->getPlusDomain()->_groupGhostMPI->insert(e);
              else
                 dgdom->getMinusDomain()->_groupGhostMPI->insert(it_face->getElement());
            }
          }
          dgdom->gi->insert(interel);
          map_face.erase(it_face);
        }
      }
    }
  }
}



FilterDof* dG3DDomain::createFilterDof(const int comp) const
{
  FilterDof* fdof = new DG3DFilterDofComponent(comp);
  return fdof;

}

void dG3DDomain::gaussIntegration(const GaussType gqt,const int orderbulk_, const int orderbound_){
  _gqt = gqt;
  _gaussorderbulk = orderbulk_;
  _gaussorderbound= orderbound_;
}


void dG3DDomain::createInterface(manageInterface &maninter)
{
// create all interface even if CG to allow CG/DG mixed formulation
  for (elementGroup::elementContainer::const_iterator it = g->begin(); it != g->end(); ++it)
  {
    MElement *e=it->second;
    if (e->getDim() == 3){
      int nface = e->getNumFaces();
      for(int j=0;j<nface;j++){
          std::vector<MVertex*> vv;
          e->getFaceVertices(j,vv);
          IElement *ie = new IFace(vv,e,this);
          maninter.insert(ie,this);
        }
      }
      else if (e->getDim() == 2){
        int nedge = e->getNumEdges();
        for(int j=0;j<nedge;j++){
          std::vector<MVertex*> vv;
          e->getEdgeVertices(j,vv);
          IElement *ie = new IEdge(vv,e,this);
          maninter.insert(ie,this);
        }
      }
    }
//  }
}

MElement* dG3DDomain::createVirtualInterface(IElement *ie) const
{
  std::vector<MVertex*> Mv  = ie->getVertices();
  MElement *ele;
  if(ie->getElement()->getTypeForMSH() == MSH_TET_4)
  {
    ele = new MInterfaceTriangleN(Mv,1,0,0,ie->getElement(),ie->getElement());
  }
  else if(ie->getElement()->getTypeForMSH() == MSH_TET_10)
  {
    ele = new MInterfaceTriangleN(Mv,2,0,0,ie->getElement(),ie->getElement());
  }
  else if(ie->getElement()->getTypeForMSH() == MSH_HEX_8)
  {
    ele = new MInterfaceQuadrangleN(Mv,1,0,0,ie->getElement(),ie->getElement());
  }
  else if(ie->getElement()->getTypeForMSH() == MSH_HEX_27)
  {
    ele = new MInterfaceQuadrangleN(Mv,2,0,0,ie->getElement(),ie->getElement());
  }
  else if (ie->getElement()->getDim() == 2){
    ele = new MInterfaceLine(Mv,0,0,ie->getElement(),ie->getElement());
  }
  else if(ie->getElement()->getTypeForMSH() == MSH_TET_20)
  {
    ele = new MInterfaceTriangleN(Mv,3,0,0,ie->getElement(),ie->getElement());
  }
  else if (ie->getElement()->getTypeForMSH() == MSH_PRI_6)
  {
    if (Mv.size() == 3){
      ele = new MInterfaceTriangleN(Mv,1,0,0,ie->getElement(),ie->getElement());
    }
    else if (Mv.size() == 4){
      ele = new MInterfaceQuadrangleN(Mv,1,0,0,ie->getElement(),ie->getElement());
    }
  }
  else if (ie->getElement()->getTypeForMSH() == MSH_PRI_18)
  {
    if (Mv.size() == 6){
      ele = new MInterfaceTriangleN(Mv,2,0,0,ie->getElement(),ie->getElement());
    }
    else if (Mv.size() == 9){
      ele = new MInterfaceQuadrangleN(Mv,2,0,0,ie->getElement(),ie->getElement());
    }
  }

  else
  {
    Msg::Error("missing case in dG3DDomain::createVirtualInterface");
  }
  return ele;
}
MElement* dG3DDomain::createInterface(IElement *ie1, IElement *ie2) const
{
  #if defined(HAVE_MPI)
  if(Msg::GetCommSize() != 1){ // with mpi special attention is needed for creation of
                               // interface on interface domain
                               // minus element == element with the lowest part number (if they are different)
    int part1 = ie1->getElement()->getPartition();
    int part2 = ie2->getElement()->getPartition();
    MElement *nelem;
    // HERE WE USE DG FOR
    //         1° 2 domains -> if phys1 != phys2
    //         2° Parallel -> if part1 != part2
    //         3° if choice -> _full dg

    if(ie2->getPhys() > ie1->getPhys() )
    {
      std::vector<MVertex*> vv  = ie1->getVertices();
      if(ie1->getElement()->getTypeForMSH() == MSH_TET_4)
      {
        nelem = new MInterfaceTriangleN(vv, 1, 0, Msg::GetCommRank()+1, ie1->getElement(), ie2->getElement());
      }
      else if(ie1->getElement()->getTypeForMSH() == MSH_TET_10)
      {
        nelem = new MInterfaceTriangleN(vv, 2, 0, Msg::GetCommRank()+1, ie1->getElement(), ie2->getElement());
      }
      else if(ie1->getElement()->getTypeForMSH() == MSH_HEX_8)
      {
        nelem = new MInterfaceQuadrangleN(vv, 1, 0, Msg::GetCommRank()+1, ie1->getElement(), ie2->getElement());
      }
      else if(ie1->getElement()->getTypeForMSH() == MSH_HEX_27)
      {
        nelem = new MInterfaceQuadrangleN(vv, 2, 0, Msg::GetCommRank()+1, ie1->getElement(), ie2->getElement());
      }
      else if (ie1->getElement()->getDim()==2){
        nelem = new MInterfaceLine(vv,0,Msg::GetCommRank()+1,ie1->getElement(),ie2->getElement());
      }
      else if(ie1->getElement()->getTypeForMSH() == MSH_TET_20)
      {
        nelem = new MInterfaceTriangleN(vv, 3, 0, Msg::GetCommRank()+1, ie1->getElement(), ie2->getElement());
      }
      else if (ie1->getElement()->getTypeForMSH() == MSH_PRI_6)
      {
        if (vv.size() == 3){
          nelem = new MInterfaceTriangleN(vv, 1, 0, Msg::GetCommRank()+1, ie1->getElement(), ie2->getElement());
        }
        else if (vv.size() == 4){
          nelem = new MInterfaceQuadrangleN(vv, 1, 0, Msg::GetCommRank()+1, ie1->getElement(), ie2->getElement());
        }
      }
      else if (ie1->getElement()->getTypeForMSH() == MSH_PRI_18)
      {
        if (vv.size() == 6){
          nelem = new MInterfaceTriangleN(vv,2, 0, Msg::GetCommRank()+1, ie1->getElement(), ie2->getElement());
        }
        else if (vv.size() == 9){
          nelem = new MInterfaceQuadrangleN(vv, 2, 0, Msg::GetCommRank()+1, ie1->getElement(), ie2->getElement());
        }
      }
      else
      {
        Msg::Error("missing case in dG3DDomain::createInterface");
        nelem = NULL;
      }
    }
    else if(ie2->getPhys() < ie1->getPhys() )
    {
      std::vector<MVertex*> vv  = ie2->getVertices();
      if(ie2->getElement()->getTypeForMSH() == MSH_TET_4)
      {
        nelem = new MInterfaceTriangleN(vv, 1, 0,Msg::GetCommRank()+1,ie2->getElement(),ie1->getElement());
      }
      else if(ie2->getElement()->getTypeForMSH() == MSH_TET_10)
      {
        nelem = new MInterfaceTriangleN(vv, 2, 0,Msg::GetCommRank()+1,ie2->getElement(),ie1->getElement());
      }
      else if(ie2->getElement()->getTypeForMSH() == MSH_HEX_8)
      {
        nelem = new MInterfaceQuadrangleN(vv, 1, 0,Msg::GetCommRank()+1,ie2->getElement(),ie1->getElement());
      }
      else if(ie2->getElement()->getTypeForMSH() == MSH_HEX_27)
      {
        nelem = new MInterfaceQuadrangleN(vv, 2, 0,Msg::GetCommRank()+1,ie2->getElement(),ie1->getElement());
      }
      else if (ie2->getElement()->getDim()==2){
        nelem  = new MInterfaceLine(vv,0,Msg::GetCommRank()+1,ie2->getElement(),ie1->getElement());
      }
      else if(ie2->getElement()->getTypeForMSH() == MSH_TET_20)
      {
        nelem = new MInterfaceTriangleN(vv, 3, 0,Msg::GetCommRank()+1,ie2->getElement(),ie1->getElement());
      }
      else if (ie2->getElement()->getTypeForMSH() == MSH_PRI_6)
      {
        if (vv.size() == 3){
          nelem = new MInterfaceTriangleN(vv, 1, 0,Msg::GetCommRank()+1,ie2->getElement(),ie1->getElement());
        }
        else if (vv.size() == 4){
          nelem = new MInterfaceQuadrangleN(vv, 1, 0,Msg::GetCommRank()+1,ie2->getElement(),ie1->getElement());
        }
      }
      else if (ie2->getElement()->getTypeForMSH() == MSH_PRI_18)
      {
        if (vv.size() == 6){
          nelem = new MInterfaceTriangleN(vv, 2, 0,Msg::GetCommRank()+1,ie2->getElement(),ie1->getElement());
        }
        else if (vv.size() == 9){
          nelem = new MInterfaceQuadrangleN(vv, 2, 0,Msg::GetCommRank()+1,ie2->getElement(),ie1->getElement());
        }
      }
      else
      {
        Msg::Error("missing case in dG3DDomain::createInterface");
        nelem=NULL;
      }
    }
    else if(part2 > part1){
      std::vector<MVertex*> vv = ie1->getVertices();
      if(ie1->getElement()->getTypeForMSH() == MSH_TET_4)
      {
        nelem = new MInterfaceTriangleN(vv, 1, 0, Msg::GetCommRank()+1, ie1->getElement(), ie2->getElement());
      }
      else if(ie1->getElement()->getTypeForMSH() == MSH_TET_10)
      {
        nelem = new MInterfaceTriangleN(vv, 2, 0, Msg::GetCommRank()+1, ie1->getElement(), ie2->getElement());
      }
      else if(ie1->getElement()->getTypeForMSH() == MSH_HEX_8)
      {
        nelem = new MInterfaceQuadrangleN(vv, 1, 0, Msg::GetCommRank()+1, ie1->getElement(), ie2->getElement());
      }
      else if(ie1->getElement()->getTypeForMSH() == MSH_HEX_27)
      {
        nelem = new MInterfaceQuadrangleN(vv, 2, 0, Msg::GetCommRank()+1, ie1->getElement(), ie2->getElement());
      }
      else if (ie1->getElement()->getDim()==2){
        nelem = new MInterfaceLine(vv,0,Msg::GetCommRank()+1,ie1->getElement(),ie2->getElement());
      }
      else if(ie1->getElement()->getTypeForMSH() == MSH_TET_20)
      {
        nelem = new MInterfaceTriangleN(vv, 3, 0, Msg::GetCommRank()+1, ie1->getElement(), ie2->getElement());
      }
      else if(ie1->getElement()->getTypeForMSH() == MSH_PRI_6)
      {
        if (vv.size() == 3){
          nelem = new MInterfaceTriangleN(vv, 1, 0, Msg::GetCommRank()+1, ie1->getElement(), ie2->getElement());
        }
        else if (vv.size() == 4){
          nelem = new MInterfaceQuadrangleN(vv, 1, 0, Msg::GetCommRank()+1, ie1->getElement(), ie2->getElement());
        }
      }
      else if(ie1->getElement()->getTypeForMSH() == MSH_PRI_18)
      {
        if (vv.size() == 6){
          nelem = new MInterfaceTriangleN(vv, 2, 0, Msg::GetCommRank()+1, ie1->getElement(), ie2->getElement());
        }
        else if (vv.size() == 9){
          nelem = new MInterfaceQuadrangleN(vv, 2, 0, Msg::GetCommRank()+1, ie1->getElement(), ie2->getElement());
        }
      }

      else
      {
        Msg::Error("missing case in dG3DDomain::createInterface");
      }
    }
    else if(part1 > part2){
      std::vector<MVertex*> vv  = ie2->getVertices();
      if(ie2->getElement()->getTypeForMSH() == MSH_TET_4)
      {
        nelem = new MInterfaceTriangleN(vv, 1, 0,Msg::GetCommRank()+1,ie2->getElement(),ie1->getElement());
      }
      else if(ie2->getElement()->getTypeForMSH() == MSH_TET_10)
      {
        nelem = new MInterfaceTriangleN(vv, 2, 0,Msg::GetCommRank()+1,ie2->getElement(),ie1->getElement());
      }
      else if(ie2->getElement()->getTypeForMSH() == MSH_HEX_8)
      {
        nelem = new MInterfaceQuadrangleN(vv, 1, 0,Msg::GetCommRank()+1,ie2->getElement(),ie1->getElement());
      }
      else if(ie2->getElement()->getTypeForMSH() == MSH_HEX_27)
      {
        nelem = new MInterfaceQuadrangleN(vv, 2, 0,Msg::GetCommRank()+1,ie2->getElement(),ie1->getElement());
      }
      else if (ie2->getElement()->getDim()==2){
        nelem = new MInterfaceLine(vv,0,Msg::GetCommRank()+1,ie2->getElement(),ie1->getElement());
      }
      else if(ie2->getElement()->getTypeForMSH() == MSH_TET_20)
      {
        nelem = new MInterfaceTriangleN(vv, 3, 0,Msg::GetCommRank()+1,ie2->getElement(),ie1->getElement());
      }
      else if(ie2->getElement()->getTypeForMSH() == MSH_PRI_6)
      {
        if (vv.size() == 3){
          nelem = new MInterfaceTriangleN(vv, 1, 0,Msg::GetCommRank()+1,ie2->getElement(),ie1->getElement());
        }
        else if (vv.size() == 4){
          nelem = new MInterfaceQuadrangleN(vv, 1, 0,Msg::GetCommRank()+1,ie2->getElement(),ie1->getElement());
        }
      }
      else if(ie2->getElement()->getTypeForMSH() == MSH_PRI_18)
      {
        if (vv.size() == 6){
          nelem = new MInterfaceTriangleN(vv, 2, 0,Msg::GetCommRank()+1,ie2->getElement(),ie1->getElement());
        }
        else if (vv.size() == 9){
          nelem = new MInterfaceQuadrangleN(vv, 2, 0,Msg::GetCommRank()+1,ie2->getElement(),ie1->getElement());
        }
      }
      else
      {
        Msg::Error("missing case in dG3DDomain::createInterface");
        nelem = NULL;
      }
    }
    else if(_fullDg or _isHighOrder) //arbitrary choice
    {
      std::vector<MVertex*> vv  = ie1->getVertices();
      if(ie1->getElement()->getTypeForMSH() == MSH_TET_4)
      {
        nelem = new MInterfaceTriangleN(vv, 1, 0, Msg::GetCommRank()+1, ie1->getElement(), ie2->getElement());
      }
      else if(ie1->getElement()->getTypeForMSH() == MSH_TET_10)
      {
        nelem = new MInterfaceTriangleN(vv, 2, 0, Msg::GetCommRank()+1, ie1->getElement(), ie2->getElement());
      }
      else if(ie1->getElement()->getTypeForMSH() == MSH_HEX_8)
      {
        nelem = new MInterfaceQuadrangleN(vv, 1, 0, Msg::GetCommRank()+1, ie1->getElement(), ie2->getElement());
      }
      else if(ie1->getElement()->getTypeForMSH() == MSH_HEX_27)
      {
        nelem = new MInterfaceQuadrangleN(vv, 2, 0, Msg::GetCommRank()+1, ie1->getElement(), ie2->getElement());
      }
      else if (ie1->getElement()->getDim()==2){
        nelem = new MInterfaceLine(vv,0,Msg::GetCommRank()+1,ie1->getElement(),ie2->getElement());
      }
      else if(ie1->getElement()->getTypeForMSH() == MSH_TET_20)
      {
        nelem = new MInterfaceTriangleN(vv, 3, 0, Msg::GetCommRank()+1, ie1->getElement(), ie2->getElement());
      }
      else if(ie1->getElement()->getTypeForMSH() == MSH_PRI_6)
      {
        if (vv.size() == 3) {
          nelem = new MInterfaceTriangleN(vv, 1, 0, Msg::GetCommRank()+1, ie1->getElement(), ie2->getElement());
        }
        else if (vv.size() == 4){
          nelem = new MInterfaceQuadrangleN(vv, 1, 0, Msg::GetCommRank()+1, ie1->getElement(), ie2->getElement());
        }
      }
      else if(ie1->getElement()->getTypeForMSH() == MSH_PRI_18)
      {
        if (vv.size() == 6) {
          nelem = new MInterfaceTriangleN(vv, 2, 0, Msg::GetCommRank()+1, ie1->getElement(), ie2->getElement());
        }
        else if (vv.size() == 9){
          nelem = new MInterfaceQuadrangleN(vv, 2, 0, Msg::GetCommRank()+1, ie1->getElement(), ie2->getElement());
        }
      }

      else
      {
        Msg::Error("missing case in dG3DDomain::createInterface");
      }
    }
    else
    {
      nelem = NULL;
    }
    if((part1 != Msg::GetCommRank() + 1) and (part1 != 0)) // add ghost interface for Dirichlet BC
    {
      ie1->getDomain()->_groupGhostMPI->insert(ie1->getElement());
    }
    if((part2 != Msg::GetCommRank() + 1) and (part2 != 0)) // add ghost interface for Dirichlet BC
    {
      ie2->getDomain()->_groupGhostMPI->insert(ie2->getElement());
    }
    return nelem;
  }
  else
 #endif // HAVE_MPI
  {
    if(_isHighOrder or _fullDg or (ie1->getPhys() != ie2->getPhys())){
      if(ie1->getPhys() <= ie2->getPhys())
      {
        std::vector<MVertex*> vv = ie1->getVertices();
        MElement *nelem;
        if(ie1->getElement()->getTypeForMSH() == MSH_TET_4)
        {
          nelem = new MInterfaceTriangleN(vv, 1, 0, 0, ie1->getElement(), ie2->getElement());
        }
        else if(ie1->getElement()->getTypeForMSH() == MSH_TET_10)
        {
          nelem = new MInterfaceTriangleN(vv, 2, 0, 0, ie1->getElement(), ie2->getElement());
        }
        else if(ie1->getElement()->getTypeForMSH() == MSH_HEX_8)
        {
          nelem = new MInterfaceQuadrangleN(vv, 1, 0, 0, ie1->getElement(), ie2->getElement());
        }
        else if(ie1->getElement()->getTypeForMSH() == MSH_HEX_27)
        {
          nelem = new MInterfaceQuadrangleN(vv, 2, 0, 0, ie1->getElement(), ie2->getElement());
        }
        else if (ie1->getElement()->getDim()==2){
          nelem = new MInterfaceLine(vv,0,0,ie1->getElement(),ie2->getElement());
        }
        else if(ie1->getElement()->getTypeForMSH() == MSH_TET_20)
        {
          nelem = new MInterfaceTriangleN(vv, 3, 0, 0, ie1->getElement(), ie2->getElement());
        }
        else if (ie1->getElement()->getTypeForMSH() == MSH_PRI_6)
        {
          if (vv.size() == 3){
            nelem = new MInterfaceTriangleN(vv, 1, 0, 0, ie1->getElement(), ie2->getElement());
          }
          else if (vv.size() == 4){
            nelem = new MInterfaceQuadrangleN(vv, 1, 0, 0, ie1->getElement(), ie2->getElement());
          }
        }
        else if (ie1->getElement()->getTypeForMSH() == MSH_PRI_18)
        {
          if (vv.size() == 6){
            nelem = new MInterfaceTriangleN(vv, 2, 0, 0, ie1->getElement(), ie2->getElement());
          }
          else if (vv.size() == 9){
            nelem = new MInterfaceQuadrangleN(vv, 2, 0, 0, ie1->getElement(), ie2->getElement());
          }
        }
        else
        {
          Msg::Error("missing case in dG3DDomain::createInterface");
        }
        return nelem;
      }
      else
      {
        std::vector<MVertex*> vv  = ie2->getVertices();
        MElement *nelem;
        if(ie2->getElement()->getTypeForMSH() == MSH_TET_4)
        {
          nelem = new MInterfaceTriangleN(vv, 1, 0, 0, ie2->getElement(), ie1->getElement());
        }
        else if(ie2->getElement()->getTypeForMSH() == MSH_TET_10)
        {
          nelem = new MInterfaceTriangleN(vv, 2, 0, 0, ie2->getElement(), ie1->getElement());
        }
        else if(ie2->getElement()->getTypeForMSH() == MSH_HEX_8)
        {
          nelem = new MInterfaceQuadrangleN(vv, 1, 0, 0, ie2->getElement(), ie1->getElement());
        }
        else if(ie2->getElement()->getTypeForMSH() == MSH_HEX_27)
        {
          nelem = new MInterfaceQuadrangleN(vv, 2, 0, 0, ie2->getElement(), ie1->getElement());
        }
        else if (ie2->getElement()->getDim()==2){
          nelem = new MInterfaceLine(vv,0,0,ie2->getElement(),ie1->getElement());
        }
        else if(ie2->getElement()->getTypeForMSH() == MSH_TET_20)
        {
          nelem = new MInterfaceTriangleN(vv, 3, 0, 0, ie2->getElement(), ie1->getElement());
        }
        else if(ie2->getElement()->getTypeForMSH() == MSH_PRI_6)
        {
          if (vv.size() == 3){
            nelem = new MInterfaceTriangleN(vv, 1, 0, 0, ie2->getElement(), ie1->getElement());
          }
          else if (vv.size() == 4){
            nelem = new MInterfaceQuadrangleN(vv, 1, 0, 0, ie2->getElement(), ie1->getElement());
          }
        }
        else if(ie2->getElement()->getTypeForMSH() == MSH_PRI_18)
        {
          if (vv.size() == 6){
            nelem = new MInterfaceTriangleN(vv, 2, 0, 0, ie2->getElement(), ie1->getElement());
          }
          else if (vv.size() == 9){
            nelem = new MInterfaceQuadrangleN(vv, 2, 0, 0, ie2->getElement(), ie1->getElement());
          }
        }
        else
        {
          Msg::Error("missing case in dG3DDomain::createInterface");
        }
        return nelem;
      }
    }
    else{
      return NULL;
    }
  }
}


#if defined(HAVE_MPI)
bool dG3DDomain::computeIPVariableMPI(AllIPState *aips,const unknownField *ufield,const IPStateBase::whichState ws, bool stiff){
  IntPt *GP;
  MPI_Status status;
  if (Msg::GetCommRank() == _rootRank){
    // compute all IP strain on root rank
    this->computeAllIPStrain(aips,ufield,ws,false);
    // send strain to other procs
    // for bulk elements
    for (std::set<int>::iterator it = _otherRanks.begin(); it!= _otherRanks.end(); it++){
      int otherRank = *it;
      std::set<int>& IPBulkonRank = _mapIPBulk[otherRank];
      for (std::set<int>::iterator its = IPBulkonRank.begin(); its!= IPBulkonRank.end(); its++){
        int num = *its;
        int elnum,gnum;
        numericalMaterialBase::getTwoIntsFromType(num,elnum,gnum);
        AllIPState::ipstateElementContainer *vips = aips->getIPstate(elnum);
        IPStateBase* ips = (*vips)[gnum];
        IPVariable* ipv = ips->getState(ws);
        int bufferSize = ipv->getMacroNumberElementDataSendToMicroProblem(); // already known
        double *buffer = new double[bufferSize];
        ipv->getMacroDataSendToMicroProblem(buffer);
        MPI_Send(buffer,bufferSize,MPI_DOUBLE,otherRank,num,MPI_COMM_WORLD);
        delete[] buffer;
      };

      std::set<int>& IPInterfaceMinusonRank = _mapIPInterfaceMinus[otherRank];
      for (std::set<int>::iterator its = IPInterfaceMinusonRank.begin(); its!= IPInterfaceMinusonRank.end(); its++){
        int num = *its;
        int elnum,gnum;
        numericalMaterialBase::getTwoIntsFromType(num,elnum,gnum);
        AllIPState::ipstateElementContainer *vips = aips->getIPstate(elnum);
        IPStateBase* ips = (*vips)[gnum];
        IPVariable* ipv = ips->getState(ws);
        int bufferSize = ipv->getMacroNumberElementDataSendToMicroProblem(); // already known
        double *buffer = new double[bufferSize];
        ipv->getMacroDataSendToMicroProblem(buffer);
        MPI_Send(buffer,bufferSize,MPI_DOUBLE,otherRank,num,MPI_COMM_WORLD);
        delete[] buffer;
      };

      std::set<int>& IPInterfacePlusonRank = _mapIPInterfacePlus[otherRank];
      for (std::set<int>::iterator its = IPInterfacePlusonRank.begin(); its!= IPInterfacePlusonRank.end(); its++){
        int num = *its;
        int elnum,gnum;
        numericalMaterialBase::getTwoIntsFromType(num,elnum,gnum);
        AllIPState::ipstateElementContainer *vips = aips->getIPstate(elnum);
        IPStateBase* ips = (*vips)[gnum];
        IPVariable* ipv = ips->getState(ws);
        int bufferSize = ipv->getMacroNumberElementDataSendToMicroProblem(); // already known
        double *buffer = new double[bufferSize];
        ipv->getMacroDataSendToMicroProblem(buffer);
        MPI_Send(buffer,bufferSize,MPI_DOUBLE,otherRank,num,MPI_COMM_WORLD);
        delete[] buffer;
      };
    }
  }
  else if (_otherRanks.find(Msg::GetCommRank()) != _otherRanks.end()){
    for (std::set<int>::iterator it = _domainIPBulk.begin(); it != _domainIPBulk.end(); it++){
      int num = *it;
      int elnum,gnum;
      numericalMaterialBase::getTwoIntsFromType(num,elnum,gnum);
      AllIPState::ipstateElementContainer *vips = aips->getIPstate(num);
      IPStateBase* ips = (*vips)[0];
      IPVariable* ipv = ips->getState(ws);
      int bufferSize = ipv->getMacroNumberElementDataSendToMicroProblem();
      double *buffer = new double[bufferSize];
      MPI_Recv(buffer,bufferSize,MPI_DOUBLE,_rootRank,num,MPI_COMM_WORLD,&status);
      ipv->setReceivedMacroDataToMicroProblem(buffer);
      delete [] buffer;
    };

    for (std::set<int>::iterator it = _domainIPInterfaceMinus.begin(); it != _domainIPInterfaceMinus.end(); it++){
      int num = *it;
      int elnum, phys, gnum;
      numericalMaterialBase::getTwoIntsFromType(num,elnum,gnum);
      AllIPState::ipstateElementContainer *vips = aips->getIPstate(num);
      IPStateBase* ips = (*vips)[0];
      IPVariable* ipv = ips->getState(ws);
      int bufferSize = ipv->getMacroNumberElementDataSendToMicroProblem();
      double *buffer = new double[bufferSize];
      MPI_Recv(buffer,bufferSize,MPI_DOUBLE,_rootRank,num,MPI_COMM_WORLD,&status);
      ipv->setReceivedMacroDataToMicroProblem(buffer);
      delete [] buffer;
    };

    for (std::set<int>::iterator it = _domainIPInterfacePlus.begin(); it != _domainIPInterfacePlus.end(); it++){
      int num = *it;
      int elnum,gnum;
      numericalMaterialBase::getTwoIntsFromType(num,elnum,gnum);
      AllIPState::ipstateElementContainer *vips = aips->getIPstate(num);
      IPStateBase* ips = (*vips)[0];
      IPVariable* ipv =  ips->getState(ws);
      int bufferSize = ipv->getMacroNumberElementDataSendToMicroProblem();
      double *buffer = new double[bufferSize];
      MPI_Recv(buffer,bufferSize,MPI_DOUBLE,_rootRank,num,MPI_COMM_WORLD,&status);
      ipv->setReceivedMacroDataToMicroProblem(buffer);
      delete [] buffer;
    };
  };
  bool needDTangent=false;
  BodyForceModuliType  ComputeBodyForceModuli = useWhichModuliForBF();
  
  if (hasBodyForceForHO() and ComputeBodyForceModuli == Present){
      needDTangent= true;
  }
  for (std::set<int>::iterator it = _domainIPBulk.begin(); it != _domainIPBulk.end(); it++){
    int num = *it;
    int elnum, gnum;
    numericalMaterialBase::getTwoIntsFromType(num,elnum,gnum);
    IPStateBase* ips = NULL;
    if (Msg::GetCommRank() == _rootRank){
      AllIPState::ipstateElementContainer *vips = aips->getIPstate(elnum);
      ips = (*vips)[gnum];
    }
    else if (_otherRanks.find(Msg::GetCommRank()) != _otherRanks.end()){
      AllIPState::ipstateElementContainer *vips = aips->getIPstate(num);
      ips = (*vips)[0];
    }
    IPVariable *ipv = ips->getState(ws);
    const IPVariable *ipvprev = ips->getState(IPStateBase::previous);
    dG3DMaterialLaw *mlaw = dynamic_cast<dG3DMaterialLaw*>(this->getMaterialLaw());
    if (_planeStressState)
      mlaw->stress3DTo2D(ipv,ipvprev,stiff,false,needDTangent);
    else
      mlaw->stress(ipv,ipvprev,stiff,false,needDTangent);

    if(hasBodyForceForHO()){
      if(ComputeBodyForceModuli == Present){  
          ipv->setValueForBodyForce(ipv->getConstRefToTangentModuli(),ComputeBodyForceModuli);
      }
      else if(ComputeBodyForceModuli == Previous){
        ipv->setValueForBodyForce(ipvprev->getConstRefToTangentModuli(),ComputeBodyForceModuli);  
      }
      else if(ComputeBodyForceModuli == Homogeneous){
          ipv->setValueForBodyForce(getHomogeneousTangent(),ComputeBodyForceModuli); 
      }
      else{ Msg::Error("Moduli used to compute body force is not defined");}    
    }  
  };
  
  for (std::set<int>::iterator it = _domainIPInterfaceMinus.begin(); it != _domainIPInterfaceMinus.end(); it++){
    int num = *it;
    int elnum, gnum;
    numericalMaterialBase::getTwoIntsFromType(num,elnum,gnum);
    IPStateBase* ips = NULL;
    if (Msg::GetCommRank() == _rootRank){
      AllIPState::ipstateElementContainer *vips = aips->getIPstate(elnum);
      ips = (*vips)[gnum];
    }
    else if (_otherRanks.find(Msg::GetCommRank()) != _otherRanks.end()){
      AllIPState::ipstateElementContainer *vips = aips->getIPstate(num);
      ips = (*vips)[0];
    }
    IPVariable *ipv = ips->getState(ws);
    const IPVariable *ipvprev = ips->getState(IPStateBase::previous);
    dG3DMaterialLaw *mlawMinus = dynamic_cast<dG3DMaterialLaw*>(this->getMaterialLawMinus());
    if (_planeStressState)
	mlawMinus->stress3DTo2D(ipv,ipvprev,stiff,true,needDTangent);
    else
    	mlawMinus->stress(ipv,ipvprev,stiff,true,needDTangent);
    if(hasBodyForceForHO()){
      if(ComputeBodyForceModuli == Present){  
        ipv->setValueForBodyForce(ipv->getConstRefToTangentModuli(),ComputeBodyForceModuli);
      }
      else if(ComputeBodyForceModuli == Previous){
        ipv->setValueForBodyForce(ipvprev->getConstRefToTangentModuli(),ComputeBodyForceModuli);  
      }
      else if(ComputeBodyForceModuli == Homogeneous){
        ipv->setValueForBodyForce(getHomogeneousTangent(), ComputeBodyForceModuli); 
      }
      else{ Msg::Error("Moduli used to compute body force is not defined");}    
    }  
  };

  for (std::set<int>::iterator it = _domainIPInterfacePlus.begin(); it != _domainIPInterfacePlus.end(); it++){
    int num = *it;
    int elnum,gnum;
    numericalMaterialBase::getTwoIntsFromType(num,elnum,gnum);
    IPStateBase* ips = NULL;
    if (Msg::GetCommRank() == _rootRank){
      AllIPState::ipstateElementContainer *vips = aips->getIPstate(elnum);
      ips = (*vips)[gnum];
    }
    else if (_otherRanks.find(Msg::GetCommRank()) != _otherRanks.end()){
      AllIPState::ipstateElementContainer *vips = aips->getIPstate(num);
      ips = (*vips)[0];
    }
    IPVariable *ipv = ips->getState(ws);
    const IPVariable *ipvprev = ips->getState(IPStateBase::previous);
    dG3DMaterialLaw *mlawPlus= dynamic_cast<dG3DMaterialLaw*>(this->getMaterialLawPlus());
    if (_planeStressState)
	mlawPlus->stress3DTo2D(ipv,ipvprev,stiff,true,needDTangent);
    else
       mlawPlus->stress(ipv,ipvprev,stiff,true,needDTangent);
    if(hasBodyForceForHO()){
      if(ComputeBodyForceModuli == Present){  
          ipv->setValueForBodyForce(ipv->getConstRefToTangentModuli(),ComputeBodyForceModuli);
      }
      else if(ComputeBodyForceModuli == Previous){
        ipv->setValueForBodyForce(ipvprev->getConstRefToTangentModuli(),ComputeBodyForceModuli);  
      }
      else if(ComputeBodyForceModuli == Homogeneous){
          ipv->setValueForBodyForce(getHomogeneousTangent(),ComputeBodyForceModuli); 
      }
      else{ Msg::Error("Moduli used to compute body force is not defined");}    
    }  
  }

  if (_otherRanks.find(Msg::GetCommRank()) != _otherRanks.end()){
    for (std::set<int>::iterator it = _domainIPBulk.begin(); it != _domainIPBulk.end(); it++){
      int num = *it;
      int elnum,gnum;
      numericalMaterialBase::getTwoIntsFromType(num,elnum,gnum);
      AllIPState::ipstateElementContainer *vips = aips->getIPstate(num);
      IPStateBase* ips = (*vips)[0];
      IPVariable* ipv = ips->getState(ws);
      int bufferSize = ipv->getMicroNumberElementDataSendToMacroProblem();
      double *buffer = new double[bufferSize];
      ipv->getMicroDataToMacroProblem(buffer);
      MPI_Send(buffer,bufferSize,MPI_DOUBLE,_rootRank,num,MPI_COMM_WORLD);
      delete [] buffer;
    };
    for (std::set<int>::iterator it = _domainIPInterfaceMinus.begin(); it != _domainIPInterfaceMinus.end(); it++){
      int num = *it;
      int elnum,gnum;
      numericalMaterialBase::getTwoIntsFromType(num,elnum,gnum);
      AllIPState::ipstateElementContainer *vips = aips->getIPstate(num);
      IPStateBase* ips = (*vips)[0];
      IPVariable* ipv = ips->getState(ws);
      int bufferSize = ipv->getMicroNumberElementDataSendToMacroProblem();
      double *buffer = new double[bufferSize];
      ipv->getMicroDataToMacroProblem(buffer);
      MPI_Send(buffer,bufferSize,MPI_DOUBLE,_rootRank,num,MPI_COMM_WORLD);
      delete [] buffer;
    };
    for (std::set<int>::iterator it = _domainIPInterfacePlus.begin(); it != _domainIPInterfacePlus.end(); it++){
      int num = *it;
      int elnum,gnum;
      numericalMaterialBase::getTwoIntsFromType(num,elnum,gnum);
      AllIPState::ipstateElementContainer *vips = aips->getIPstate(num);
      IPStateBase* ips = (*vips)[0];
      IPVariable* ipv = ips->getState(ws);
      int bufferSize = ipv->getMicroNumberElementDataSendToMacroProblem();
      double *buffer = new double[bufferSize];
      ipv->getMicroDataToMacroProblem(buffer);
      MPI_Send(buffer,bufferSize,MPI_DOUBLE,_rootRank,num,MPI_COMM_WORLD);
      delete [] buffer;
    };
  }
  else if (Msg::GetCommRank() == _rootRank){

    for (std::set<int>::iterator it = _otherRanks.begin(); it!= _otherRanks.end(); it++){
      int otherRank = *it;
      std::set<int>& IPBulkonRank = _mapIPBulk[otherRank];
      for (std::set<int>::iterator its = IPBulkonRank.begin(); its!= IPBulkonRank.end(); its++){
        int num = *its;
        int elnum,gnum;
        numericalMaterialBase::getTwoIntsFromType(num,elnum,gnum);
        AllIPState::ipstateElementContainer *vips = aips->getIPstate(elnum);
        IPStateBase* ips = (*vips)[gnum];
        IPVariable* ipv = ips->getState(ws);
        int bufferSize = ipv->getMicroNumberElementDataSendToMacroProblem();
        double *buffer = new double[bufferSize];
        MPI_Recv(buffer,bufferSize,MPI_DOUBLE,otherRank,num,MPI_COMM_WORLD,&status);
        ipv->setReceivedMicroDataToMacroProblem(buffer);
        dG3DMaterialLaw* mlaw = dynamic_cast<dG3DMaterialLaw*>(this->getMaterialLaw());
        mlaw->setElasticStiffness(ipv);
        delete[] buffer;
      };
      std::set<int>& IPInterfaceMinusonRank = _mapIPInterfaceMinus[otherRank];
      for (std::set<int>::iterator its = IPInterfaceMinusonRank.begin(); its!= IPInterfaceMinusonRank.end(); its++){
        int num = *its;
        int elnum,gnum;
        numericalMaterialBase::getTwoIntsFromType(num,elnum,gnum);
        AllIPState::ipstateElementContainer *vips = aips->getIPstate(elnum);
        IPStateBase* ips = (*vips)[gnum];
        IPVariable* ipv = ips->getState(ws);
        int bufferSize = ipv->getMicroNumberElementDataSendToMacroProblem();
        double *buffer = new double[bufferSize];
        MPI_Recv(buffer,bufferSize,MPI_DOUBLE,otherRank,num,MPI_COMM_WORLD,&status);
        ipv->setReceivedMicroDataToMacroProblem(buffer);
        dG3DMaterialLaw* mlawMinus = dynamic_cast<dG3DMaterialLaw*>(this->getMaterialLawMinus());
        mlawMinus->setElasticStiffness(ipv);
        delete[] buffer;
      };

      std::set<int>& IPInterfacePlusonRank = _mapIPInterfacePlus[otherRank];
      for (std::set<int>::iterator its = IPInterfacePlusonRank.begin(); its!= IPInterfacePlusonRank.end(); its++){
        int num = *its;
        int elnum,gnum;
        numericalMaterialBase::getTwoIntsFromType(num,elnum,gnum);
        AllIPState::ipstateElementContainer *vips = aips->getIPstate(elnum);
        IPStateBase* ips = (*vips)[gnum];
        IPVariable* ipv = ips->getState(ws);
        int bufferSize = ipv->getMicroNumberElementDataSendToMacroProblem();
        double *buffer = new double[bufferSize];
        MPI_Recv(buffer,bufferSize,MPI_DOUBLE,otherRank,num,MPI_COMM_WORLD,&status);
        ipv->setReceivedMicroDataToMacroProblem(buffer);
        dG3DMaterialLaw* mlawPlus = dynamic_cast<dG3DMaterialLaw*>(this->getMaterialLawPlus());
        mlawPlus->setElasticStiffness(ipv);
        delete[] buffer;
      };
    }

    if (_averageStrainBased and (this->getMaterialLawMinus()->getNum() == this->getMaterialLawPlus()->getNum())){
      for (elementGroup::elementContainer::const_iterator ite = gi->begin(); ite!= gi->end(); ite++){
        MElement* ele = ite->second;
        int npts = this->getInterfaceGaussIntegrationRule()->getIntPoints(ele,&GP);
        AllIPState::ipstateElementContainer *vips = aips->getIPstate(ele->getNum());
        for(int j=0;j<npts;j++){
          IPStateBase* ipsm = (*vips)[j];
          IPStateBase* ipsp = (*vips)[j+npts];

          IPVariable* ipvm = ipsm->getState(IPStateBase::current);
          IPVariable* ipvp = ipsp->getState(IPStateBase::current);

          ipvp->operator=(*ipvm);
        }

      }
    }
  };
  return true;
};
#endif

interDomainBase::interDomainBase(partDomain *dom1,partDomain *dom2, const int bnum) :  _mlawMinus(NULL), _mlawPlus(NULL), _bulkNum(bnum),
    _lawFromBulkPart(false)
{
  // By convention the domain with the small number is the domMinus
  // For MPI in domMinus->getPhysical() = domPlus->getPhysical() --> domMinus = domain on the partion with the lowest rank

  // look for element (INTERDOMAIN HAVE TO BE CREATED AFTER addDomain to solver)
  if(dom2->getPhysical() > dom1->getPhysical())
  {
    _domMinus = dom1;
    _domPlus = dom2;
  }
  else if(dom1->getPhysical() > dom2->getPhysical())
  {
    _domMinus = dom2;
    _domPlus = dom1;
  }
#if defined(HAVE_MPI)
  else if((Msg::GetCommSize() > 1) and (dom1->getPhysical() == dom2->getPhysical()))
  {
    _domMinus = dom1; // same --> no matter
    _domPlus = dom2;
  }
  else if ( (Msg::GetCommSize() > 1) and ((dom1->elementGroupSize() !=0) and (dom2->elementGroupSize() !=0))){
    // Check if the two domains are added to solver
    elementGroup::elementContainer::const_iterator it1 = dom1->element_begin();
    const MElement *ele1 = it1->second;
    elementGroup::elementContainer::const_iterator it2 = dom2->element_begin();
    const MElement *ele2 = it2->second;
    if(ele1->getPartition() < ele2->getPartition())
    {
      _domMinus = dom1;
      _domPlus = dom2;
    }
    else if(ele2->getPartition() < ele1->getPartition())
    {
      _domMinus = dom2;
      _domPlus = dom1;
    }
  }
 #endif // HAVE_MPI
  else  // same partition choose in function of physical number
  {
    Msg::Error("Impossible to known the partion of domain --> no inter domain created between %d and %d",dom1->getPhysical(),dom2->getPhysical());
  }
};

interDomainBase::interDomainBase(const interDomainBase &source) : _domMinus(source._domMinus),
                                                                  _domPlus(source._domPlus),
                                                                  _mlawMinus(source._mlawMinus),
                                                                  _mlawPlus(source._mlawPlus),
                                                                  _bulkNum(source._bulkNum),
                                                                  _lawFromBulkPart(source._lawFromBulkPart)
                                                                  
{

}

void interDomainBase::setLawFromBulkPartFlag(const bool fl){_lawFromBulkPart = fl;};


interDomainBetween3D::interDomainBetween3D(const int tag, partDomain *dom1,partDomain *dom2,const int lnum, const int bnum) :
                                  interDomainBase(dom1,dom2,bnum),
                                  dG3DDomain(tag,manageInterface::getKey(dom1->getPhysical(),
                                                                    dom2->getPhysical()),10000,lnum,true,dom1->getDim(),
                                                                    dom1->getNumNonLocalVariable(),
                                                                    dom1->getNumConstitutiveExtraDofDiffusionVariable(),
                                                                    dom1->getNumConstitutiveCurlVariable())
{
  if(dom1->getNumNonLocalVariable()<dom2->getNumNonLocalVariable() or dom1->getNumConstitutiveExtraDofDiffusionVariable()<dom2->getNumConstitutiveExtraDofDiffusionVariable()
     or dom1->getNumConstitutiveCurlVariable() < dom2->getNumConstitutiveCurlVariable())
    Msg::Error("interDomainBetween3D::interDomainBetween3D: domain 2 has less variable than domain 1");
  // create empty elementGroup
  g = new elementGroup();
  // set functionalSpace
  if (_space) delete _space;
  _space = new dG3DFunctionSpaceBetween2Domains(_domMinus->getFunctionSpace(),_domPlus->getFunctionSpace());

  _nonLocalBeta = (static_cast<dG3DDomain *> (dom1))-> getNonLocalStabilityParameter();
  _nonLocalContinuity = (static_cast<dG3DDomain *> (dom1))->getNonLocalContinuity();
  _nonLocalEqRatio =  (static_cast<dG3DDomain *> (dom1))->getNonLocalEqRatio();

  _constitutiveExtraDofDiffusionBeta = (static_cast<dG3DDomain *> (dom1))->getConstitutiveExtraDofDiffusionStabilityParameter();
  _constitutiveExtraDofDiffusionContinuity = (static_cast<dG3DDomain *> (dom1))->getConstitutiveExtraDofDiffusionContinuity();
  _constitutiveExtraDofDiffusionEqRatio = (static_cast<dG3DDomain *> (dom1))->getConstitutiveExtraDofDiffusionEqRatio();
  _constitutiveExtraDofDiffusionUseEnergyConjugatedField = (static_cast<dG3DDomain *> (dom1))->getConstitutiveExtraDofDiffusionUseEnergyConjugatedField();
  _constitutiveExtraDofDiffusionAccountFieldSource = (static_cast<dG3DDomain *> (dom1))->getConstitutiveExtraDofDiffusionAccountFieldSource();
  _constitutiveExtraDofDiffusionAccountMecaSource = (static_cast<dG3DDomain *> (dom1))->getConstitutiveExtraDofDiffusionAccountMecaSource();

  _constitutiveCurlEqRatio = (static_cast<dG3DDomain *> (dom1))->getConstitutiveCurlEqRatio();
  _constitutiveCurlAccountFieldSource = (static_cast<dG3DDomain *>(dom1))->getConstitutiveCurlAccountFieldSource();
  _constitutiveCurlAccountMecaSource = (static_cast<dG3DDomain *>(dom1))->getConstitutiveCurlAccountMecaSource();
}


interDomainBetween3D::interDomainBetween3D(const interDomainBetween3D &source) : interDomainBase(source), dG3DDomain(source)
{}

void interDomainBetween3D::createTerms(unknownField *uf,IPField*ip)
{
  // void term (as they have to exist)
  this->ltermBulk = new nonLinearTermVoid();
  this->btermBulk = new BiNonLinearTermVoid();

  this->btermVirtBound = new BiNonLinearTermVoid();
  this->ltermVirtBound = new nonLinearTermVoid();
  this->massterm = new BiNonLinearTermVoid();

  FunctionSpace<double>* dgspace1 = static_cast<FunctionSpace<double>*>(getMinusDomain()->getFunctionSpace());
  FunctionSpace<double>* dgspace2 = static_cast<FunctionSpace<double>*>(getPlusDomain()->getFunctionSpace());
  dG3DDomain* dgldomMinus = static_cast<dG3DDomain*>(_domMinus);
  dG3DDomain* dgldomPlus = static_cast<dG3DDomain*>(_domPlus);
  
  ltermBound = new dG3DForceInter(*dgspace1,dgspace2,_mlawMinus,_mlawPlus,_interQuad,_beta1,true,ip,_incrementNonlocalBased);
  if(_interByPert)
    btermBound = new BilinearTermPerturbation<double>(ltermBound,*dgspace1,*dgspace2,uf,ip,this,_eps);
  else{
    btermBound = new dG3DStiffnessInter(*dgspace1,*dgspace2,_mlawMinus,_mlawPlus,_interQuad,_beta1,ip,uf,true,_incrementNonlocalBased);
  }

  scalarTermPF = new nonLinearScalarTermVoid();
  linearTermPF = new nonLinearTermVoid();

  (static_cast<dG3DForceInter*>(ltermBound))->setDim(_dim);
  if(!_interByPert)
  {
    (static_cast<dG3DStiffnessInter*>(btermBound))->setDim(_dim);
  }

  if( getNumNonLocalVariable() >0)
  {
    (static_cast<dG3DForceInter*>(ltermBound))->setNonLocalEqRatio(getNonLocalEqRatio());
    (static_cast<dG3DForceInter*>(ltermBound))->setNumNonLocalVariable(getNumNonLocalVariable());
    (static_cast<dG3DForceInter*>(ltermBound))->setNonLocalStabilityParameter(getNonLocalStabilityParameter());
    (static_cast<dG3DForceInter*>(ltermBound))->setNonLocalContinuity(getNonLocalContinuity());
    if(!_interByPert)
    {
      (static_cast<dG3DStiffnessInter*>(btermBound))->setNonLocalEqRatio(getNonLocalEqRatio());
      (static_cast<dG3DStiffnessInter*>(btermBound))->setNumNonLocalVariable(getNumNonLocalVariable());
      (static_cast<dG3DStiffnessInter*>(btermBound))->setNonLocalStabilityParameter(getNonLocalStabilityParameter());
      (static_cast<dG3DStiffnessInter*>(btermBound))->setNonLocalContinuity(getNonLocalContinuity());
    }
  }
  if (getNumConstitutiveExtraDofDiffusionVariable()>0)
  {

    (static_cast<dG3DForceInter*>(ltermBound))->setNumConstitutiveExtraDofDiffusionVariable(getNumConstitutiveExtraDofDiffusionVariable());
    (static_cast<dG3DForceInter*>(ltermBound))->setConstitutiveExtraDofDiffusionEqRatio(getConstitutiveExtraDofDiffusionEqRatio());
    (static_cast<dG3DForceInter*>(ltermBound))->setConstitutiveExtraDofDiffusionUseEnergyConjugatedField(getConstitutiveExtraDofDiffusionUseEnergyConjugatedField());
    (static_cast<dG3DForceInter*>(ltermBound))->setConstitutiveExtraDofDiffusionAccountFieldSource(getConstitutiveExtraDofDiffusionAccountFieldSource());
    (static_cast<dG3DForceInter*>(ltermBound))->setConstitutiveExtraDofDiffusionAccountMecaSource(getConstitutiveExtraDofDiffusionAccountMecaSource());
    (static_cast<dG3DForceInter*>(ltermBound))->setConstitutiveExtraDofDiffusionStabilityParameter(getConstitutiveExtraDofDiffusionStabilityParameter());
    (static_cast<dG3DForceInter*>(ltermBound))->setConstitutiveExtraDofDiffusionContinuity(getConstitutiveExtraDofDiffusionContinuity());
    if(!_interByPert)
    {
      (static_cast<dG3DStiffnessInter*>(btermBound))->setNumConstitutiveExtraDofDiffusionVariable(getNumConstitutiveExtraDofDiffusionVariable());
      (static_cast<dG3DStiffnessInter*>(btermBound))->setConstitutiveExtraDofDiffusionEqRatio(getConstitutiveExtraDofDiffusionEqRatio());
      (static_cast<dG3DStiffnessInter*>(btermBound))->setConstitutiveExtraDofDiffusionUseEnergyConjugatedField(getConstitutiveExtraDofDiffusionUseEnergyConjugatedField());
      (static_cast<dG3DStiffnessInter*>(btermBound))->setConstitutiveExtraDofDiffusionAccountFieldSource(getConstitutiveExtraDofDiffusionAccountFieldSource());
      (static_cast<dG3DStiffnessInter*>(btermBound))->setConstitutiveExtraDofDiffusionAccountMecaSource(getConstitutiveExtraDofDiffusionAccountMecaSource());
      (static_cast<dG3DStiffnessInter*>(btermBound))->setConstitutiveExtraDofDiffusionStabilityParameter(getConstitutiveExtraDofDiffusionStabilityParameter());
      (static_cast<dG3DStiffnessInter*>(btermBound))->setConstitutiveExtraDofDiffusionContinuity(getConstitutiveExtraDofDiffusionContinuity());
    }
  }
  if(getNumConstitutiveCurlVariable() > 0)
  {
    (static_cast<dG3DForceInter*>(ltermBound))->setNumConstitutiveCurlVariable(getNumConstitutiveCurlVariable());
    (static_cast<dG3DForceInter*>(ltermBound))->setConstitutiveCurlEqRatio(getConstitutiveCurlEqRatio());
    (static_cast<dG3DForceInter*>(ltermBound))->setConstitutiveCurlAccountFieldSource(getConstitutiveCurlAccountFieldSource());
    (static_cast<dG3DForceInter*>(ltermBound))->setConstitutiveCurlAccountMecaSource(getConstitutiveCurlAccountMecaSource());
    if(!_interByPert)
    {
      (static_cast<dG3DStiffnessInter*>(btermBound))->setNumConstitutiveCurlVariable(getNumConstitutiveCurlVariable());
      (static_cast<dG3DStiffnessInter*>(btermBound))->setConstitutiveCurlEqRatio(getConstitutiveCurlEqRatio());
      (static_cast<dG3DStiffnessInter*>(btermBound))->setConstitutiveCurlAccountFieldSource(getConstitutiveCurlAccountFieldSource());
      (static_cast<dG3DStiffnessInter*>(btermBound))->setConstitutiveCurlAccountMecaSource(getConstitutiveCurlAccountMecaSource());
    }
  }
  bool withPF = false;
  if (getMacroSolver()!=NULL and _accountPathFollowing)
  {
    withPF = this->getMacroSolver()->withPathFollowing();
  }
  
  if (withPF){
    if (this->getMacroSolver()->getPathFollowingLocation() == pathFollowingManager::BULK){
      scalarTermPFBound = new nonLinearScalarTermVoid();
      linearTermPFBound = new nonLinearTermVoid();
    }
    else{
      scalarTermPFBound = new dG3DDissipationPathFollowingBoundScalarTerm(this,ip);
      linearTermPFBound = new dG3DDissipationPathFollowingBoundLinearTerm(this,ip);
    }
  }
  else{
    scalarTermPFBound = new nonLinearScalarTermVoid();
    linearTermPFBound = new nonLinearTermVoid();
  }
}

void interDomainBetween3D::setMaterialLaw(const std::map<int,materialLaw*> &maplaw)
{
  // oterwise the law is already set
  if(!setmaterial){
    _domMinus->setMaterialLaw(maplaw);
    _domPlus->setMaterialLaw(maplaw);
    _mlaw = NULL;
    if(_lnum == 0){
      // no fracture law prescribed --> materialLawMinus and Plus are given via domain
      if (_bulkNum != 0){
        for(std::map<int,materialLaw*>::const_iterator it=maplaw.begin(); it!=maplaw.end(); ++it){
          if(it->first == _bulkNum){
            _mlawMinus = it->second;
            _mlawPlus = it->second;
            break;
          }
        }
        if (_mlawMinus == NULL){
          Msg::Error("The material law %d is not found",_bulkNum);
        }
      }
      else{
        if ((_domMinus->getMaterialLaw()->getType() == materialLaw::fracture) and _lawFromBulkPart){
          materialLaw2LawsInitializer *m2law = dynamic_cast<materialLaw2LawsInitializer*>(_domMinus->getMaterialLaw());
          if (m2law){
            int lawMinusNum = m2law->bulkLawNumber();
            for(std::map<int,materialLaw*>::const_iterator it=maplaw.begin(); it!=maplaw.end(); ++it){
              if (it->first == lawMinusNum){
                _mlawMinus = it->second;
                break;
              }
            }
            if (_mlawMinus == NULL){
              Msg::Error("The material law %d is not found",lawMinusNum);
            }
          }
          else{
            Msg::Error("fractureBy2Laws must be used interDomainBetween3D::setMaterialLaw");
          }
        }
        else{
          _mlawMinus = _domMinus->getMaterialLaw();
        }

        if ((_domPlus->getMaterialLaw()->getType() == materialLaw::fracture) and _lawFromBulkPart){
          materialLaw2LawsInitializer *m2law = dynamic_cast<materialLaw2LawsInitializer*>(_domPlus->getMaterialLaw());
          if (m2law){
            int lawPlusNum = m2law->bulkLawNumber();
            for(std::map<int,materialLaw*>::const_iterator it=maplaw.begin(); it!=maplaw.end(); ++it){
              if (it->first == lawPlusNum){
                _mlawPlus = it->second;
                break;
              }
            }
            if (_mlawPlus == NULL){
              Msg::Error("The material law %d is not found",lawPlusNum);
            }

          }
          else{
            Msg::Error("fractureBy2Laws must be used interDomainBetween3D::setMaterialLaw");
          }
        }
        else{
          _mlawPlus = _domPlus->getMaterialLaw();
        }
      }
    }
    else{
      // create law (use lnum with negative number same number for the two but it doesn't matter)
      // these law don't appear in map with all laws
      // check if minus and plus laws are already fracture by 2 laws;
      if (_bulkNum != 0){
        // if _bulkNum is set, this used for bulk law
        materialLaw* mlawInt = NULL;
        for(std::map<int,materialLaw*>::const_iterator it=maplaw.begin(); it!=maplaw.end(); ++it){
          if(it->first == _bulkNum){
            mlawInt = it->second;
            break;
          }
        }
        if (mlawInt == NULL){
          Msg::Error("The material law %d is not found",_bulkNum);
        }

        if (mlawInt->isNumeric()){
          _mlawMinus = new MultiscaleFractureByCohesive3DLaw(-_bulkNum,_bulkNum,_lnum);
        }
        else{
          _mlawMinus = new FractureByCohesive3DLaw(-_bulkNum,_bulkNum,_lnum);
        }

        _mlawPlus = _mlawMinus;
      }
      else{
        // if _bulkNum is not set, the material laws are constructed from positive and negative parts
        int numlawBulkMinus(0), numlawBulkPlus(0);
        if(_domMinus->getMaterialLaw()->getType() != materialLaw::fracture){
          numlawBulkMinus = _domMinus->getLawNum();
        }
        else{
          materialLaw2LawsInitializer *m2law = dynamic_cast<materialLaw2LawsInitializer*>(_domMinus->getMaterialLaw());
          numlawBulkMinus = m2law->bulkLawNumber();
        }
        if(_domPlus->getMaterialLaw()->getType() != materialLaw::fracture){
          numlawBulkPlus = _domPlus->getLawNum();
        }
        else{
          materialLaw2LawsInitializer *m2law = dynamic_cast<materialLaw2LawsInitializer*>(_domPlus->getMaterialLaw());
          numlawBulkPlus = m2law->bulkLawNumber();
        }

        if (_domMinus->getMaterialLaw()->isNumeric()){
          _mlawMinus = new MultiscaleFractureByCohesive3DLaw(-numlawBulkMinus,numlawBulkMinus,_lnum);
        }
        else
          _mlawMinus = new FractureByCohesive3DLaw(-numlawBulkMinus,numlawBulkMinus,_lnum);

        if (_domPlus->getMaterialLaw()->isNumeric()){
          _mlawPlus = new MultiscaleFractureByCohesive3DLaw(-numlawBulkPlus,numlawBulkPlus,_lnum);
        }
        else
          _mlawPlus = new FractureByCohesive3DLaw(-numlawBulkPlus,numlawBulkPlus,_lnum);
      }
    }
    if (_mlawMinus == NULL || _mlawPlus == NULL)
    {
      Msg::Error("material at interface between two domains %d %d cannot be created !!!",_domMinus->getPhysical(),_domPlus->getPhysical());
      Msg::Exit(0);
    }
    _mlawMinus->initLaws(maplaw);
    _mlawPlus->initLaws(maplaw);
    setmaterial=true;
  }
}

double computFieldValue(const std::vector<TensorialTraits<double>::ValType> &Val, int pos, int n,
                      const fullVector<double> & disp)
{
  double val = 0;
  for (int j=0; j< n; j++)
  {
    val += Val[pos+j]*disp(pos+j);
  }
  return val;
}

double computeJumpField(const std::vector<TensorialTraits<double>::ValType> &Val_m, int pos_m, int n_m,
                      const std::vector<TensorialTraits<double>::ValType> &Val_p, int pos_p, const int n_p,
                      const fullVector<double> & dispm,const fullVector<double> & dispp)
{
  double jump = 0.;
  for (int j=0; j< n_m; j++)
  {
    jump -= Val_m[pos_m+j]*dispm(pos_m+j);
  }
  for (int j=0; j< n_p; j++)
  {
    jump += Val_p[pos_p+j]*dispp(pos_p+j);
  }
  return jump;
}

void computeJump(const std::vector<TensorialTraits<double>::ValType> &Val_m,const int n_m,
                      const std::vector<TensorialTraits<double>::ValType> &Val_p,
                      const int n_p,const fullVector<double> & dispm,const fullVector<double> & dispp,SVector3 &ujump)
{
  for(int i=0;i<3;i++){
    ujump(i) =0.;
    for(int j=0;j<n_m;j++)
      ujump(i)-=Val_m[j]*dispm(j+i*n_m);
    for(int j=0;j<n_p;j++)
      ujump(i)+=Val_p[j]*dispp(j+i*n_p);
  }
}

void computeNonLocalJump(const std::vector<TensorialTraits<double>::ValType> &Val_m,const int n_m,
                      const std::vector<TensorialTraits<double>::ValType> &Val_p,
                      const int n_p,const fullVector<double> & dispm,const fullVector<double> & dispp, double &epsjump,
                      const int idex)
{
  epsjump =0.;
  for(int j=0;j<n_m;j++)
    epsjump-=Val_m[j]*dispm(j+3*n_m+idex*n_m);
  for(int j=0;j<n_p;j++)
    epsjump+=Val_p[j]*dispp(j+3*n_p+idex*n_p);
}

void computeExtraFieldJump(const int index_field,const std::vector<TensorialTraits<double>::ValType> &Val_m,const int n_m,
                      const std::vector<TensorialTraits<double>::ValType> &Val_p,
                      const int n_p,const fullVector<double> & dispm,const fullVector<double> & dispp, double &temperaturejump)
{
  temperaturejump =0.;
  for(int j=0;j<n_m;j++)
    temperaturejump-=Val_m[j]*dispm(j+index_field*n_m); //depending on index_field could be temperature jump or voltage jump
  for(int j=0;j<n_p;j++)
    temperaturejump+=Val_p[j]*dispp(j+index_field*n_p);
}


void computeftjump(const int index_field,const std::vector<TensorialTraits<double>::ValType> &Val_m,const int n_m,
                      const std::vector<TensorialTraits<double>::ValType> &Val_p,
                      const int n_p,const fullVector<double> & dispm,const fullVector<double> & dispp, double &ftjump, double &dftjumpdTp, double &dftjumpdTm)
{
  ftjump =0.;dftjumpdTp=0.;dftjumpdTm=0.;
  double tp =0.,tm =0.;
  for(int j=0;j<n_m;j++)
    tm+=Val_m[j]*dispm(j+index_field*n_m); //depending on index_field could be temperature jump or voltage jump
  for(int j=0;j<n_p;j++)
    tp+=Val_p[j]*dispp(j+index_field*n_p);
  if (tp>0. &&tm>0.)
  {
    ftjump=1./tp -1/tm;

     dftjumpdTp=-1./pow(tp,2);
     dftjumpdTm= 1./pow(tm,2);
  }
 else
  {  // Virtual interface element
     Msg::Error("No in computeftjump in Domain for 3D");

  }
}

void computefvjump(const int index_fieldT,const int index_fieldv,const std::vector<TensorialTraits<double>::ValType> &Val_m,const int n_m,
                  const std::vector<TensorialTraits<double>::ValType> &Val_p,const int n_p,const fullVector<double> & dispm,const fullVector<double> & dispp,
		  double &fvjump, double &dfvjumpdTp, double &dfvjumpdTm, double &dfvjumpdvp, double &dfvjumpdvm)
{
  double ftjump=0.;
  fvjump =0.;dfvjumpdTp=0.;dfvjumpdTm=0.;dfvjumpdvp=0.;dfvjumpdvm=0.;
  double tp =0.,tm =0.;
  for(int j=0;j<n_m;j++)
    tm+=Val_m[j]*dispm(j+index_fieldT*n_m); //depending on index_field could be temperature jump
  for(int j=0;j<n_p;j++)
    tp+=Val_p[j]*dispp(j+index_fieldT*n_p);



  double vp =0.,vm =0.;
  for(int j=0;j<n_m;j++)
    vm+=Val_m[j]*dispm(j+index_fieldv*n_m); //
  for(int j=0;j<n_p;j++)
    vp+=Val_p[j]*dispp(j+index_fieldv*n_p);


  if (tp>0.&&tm>0.)
  {
   ftjump=1./tp -1./tm;

   fvjump=vm/tm -vp/tp;

   dfvjumpdvp=-1./tp;
   dfvjumpdvm= 1./tm;

   dfvjumpdTp= vp/pow(tp,2);
   dfvjumpdTm=-vm/pow(tm,2);
  }
   else
  {  // Virtual interface element
     Msg::Error("No in computefvjump in Domain for 3D");

  }

}



// New thermomechanical

ThermoMechanicsDG3DDomain::ThermoMechanicsDG3DDomain (const int tag,const int phys, const int ws, const int lnum,
                                          const int fdg, const double ThermoMechanicsEqRatio, const int dim) :
                                          dG3DDomain(tag,phys,ws,lnum,fdg, dim,0,1)
{
  setConstitutiveExtraDofDiffusionEqRatio(ThermoMechanicsEqRatio);

}

ThermoMechanicsDG3DDomain::ThermoMechanicsDG3DDomain(const ThermoMechanicsDG3DDomain &source) : dG3DDomain(source)
{

}

ThermoMechanicsInterDomainBetween3D::ThermoMechanicsInterDomainBetween3D(const int tag, partDomain *dom1,partDomain *dom2,const double ThermoMechanicsEqRatio, const int lnum, const int bnum) :
                                 interDomainBetween3D(tag, dom1, dom2, lnum, bnum)
{
  setConstitutiveExtraDofDiffusionEqRatio(ThermoMechanicsEqRatio);
}


ThermoMechanicsInterDomainBetween3D::ThermoMechanicsInterDomainBetween3D(const ThermoMechanicsInterDomainBetween3D &source) :
				interDomainBetween3D(source)
{

}


ElecTherMechDG3DDomain::ElecTherMechDG3DDomain (const int tag,const int phys, const int ws, const int lnum,
                                          const int fdg, const double ElecTherMechEqRatio, const int dim) : dG3DDomain(tag,phys,ws,lnum,fdg,dim,0,2)
{
/*  if(_space!=NULL) delete _space;   // A space is created in dG3DDomain remove it!
  switch(_wsp)
  {
   case functionSpaceType::Lagrange:
    if(_fullDg)
      _space = new dG3DLagrangeFunctionSpace(getTag(),3+getNumNonLocalVariable()+2);
    else
      _space = new g3DLagrangeFunctionSpace(getTag(),3+getNumNonLocalVariable()+2);
    break;
   case functionSpaceType::Inter :
    break; // InterfaceDomain set function space later
   default:
    Msg::Error("Function space type is unknown on domain %d",_phys);
  }*/
  setConstitutiveExtraDofDiffusionEqRatio(ElecTherMechEqRatio);
/*  _continuity=true;
  _ElecTherMechBeta=100.;*/
}

ElecTherMechDG3DDomain::ElecTherMechDG3DDomain(const ElecTherMechDG3DDomain &source) : dG3DDomain(source)
{
  /*   _ElecTherMechBeta = source._ElecTherMechBeta;
     _continuity = source._continuity;
     _ElecTherMechEqRatio = source._ElecTherMechEqRatio;*/
}


/* void ElecTherMechDG3DDomain::computeStrain(MElement *e, const int npts_bulk, IntPt* GP,
                                           AllIPState::ipstateElementContainer *vips,
                                           IPStateBase::whichState ws, fullVector<double> &disp) const
{
  if (!getElementErosionFilter()(e)) return;

  dG3DDomain::computeStrain(e, npts_bulk,GP,vips, ws,disp);
  int nbFF = e->getNumShapeFunctions();
  for(int j=0;j<npts_bulk;j++){

    IPStateBase* ips = (*vips)[j];
    ElecTherMechDG3DIPVariableBase *ipv = static_cast<ElecTherMechDG3DIPVariableBase*>(ips->getState(ws));

    const std::vector<TensorialTraits<double>::ValType> &Vals = ipv->f(_space,e,GP[j]);
    const std::vector<TensorialTraits<double>::GradType> &Grads = ipv->gradf(_space,e,GP[j]);

    if(getUseFtFv())
    {
      double *fT = &(ipv->getRefTofT());
      SVector3 *gradfT = &(ipv->getRefToGradfT());
      *fT= 0.;
      STensorOperation::zero(*gradfT);

      for (int i = 0; i < nbFF; i++)
      {
        int indexFieldfT=3+getNumNonLocalVariable();
        const double& ftemp = disp(i+indexFieldfT*nbFF);
        *fT += Vals[i+indexFieldfT*nbFF]*ftemp;
        gradfT->operator()(0) += Grads[i+indexFieldfT*nbFF][0]*ftemp;
        gradfT->operator()(1) += Grads[i+indexFieldfT*nbFF][1]*ftemp;
        gradfT->operator()(2) += Grads[i+indexFieldfT*nbFF][2]*ftemp;
      }


      double *fV = &(ipv->getRefTofV());
      SVector3 *gradfV = &(ipv->getRefToGradfV());
      *fV= 0.;
      STensorOperation::zero(*gradfV);
      for (int i = 0; i < nbFF; i++)
      {
        int indexFieldfV=3+getNumNonLocalVariable()+1;
        const double& fvolt = disp(i+indexFieldfV*nbFF);
        *fV += Vals[i+indexFieldfV*nbFF]*fvolt;
        gradfV->operator()(0) += Grads[i+indexFieldfV*nbFF][0]*fvolt;
        gradfV->operator()(1) += Grads[i+indexFieldfV*nbFF][1]*fvolt;
        gradfV->operator()(2) += Grads[i+indexFieldfV*nbFF][2]*fvolt;
      }
      SVector3 *gradT = &(ipv->getRefToGradT());
      SVector3 *gradV = &(ipv->getRefToGradV());

      double *T = &(ipv->getRefToTemperature());
      double *V = &(ipv->getRefToVoltage());
      *V = -1*(*fV)/(*fT);
      *T = 1/(*fT);


      gradV->operator()(0)= -1/(*fT)*(gradfV->operator()(0))+(*fV)/(pow(*fT,2))*(gradfT->operator()(0));
      gradV->operator()(1)= -1/(*fT)*(gradfV->operator()(1))+(*fV)/(pow(*fT,2))*(gradfT->operator()(1));
      gradV->operator()(2)= -1/(*fT)*(gradfV->operator()(2))+(*fV)/(pow(*fT,2))*(gradfT->operator()(2));


      gradT->operator()(0)=-1/(pow(*fT,2))*(gradfT->operator()(0));
      gradT->operator()(1)=-1/(pow(*fT,2))*(gradfT->operator()(1));
      gradT->operator()(2)=-1/(pow(*fT,2))*(gradfT->operator()(2));

      STensor3 *dgardVdgradfV=&(ipv->getRefTodgradVdgradfV());
      STensor3 *dgardVdgradfT=&(ipv->getRefTodgradVdgradfT());
      STensor3 *dgardTdgradfT=&(ipv->getRefTodgradTdgradfT());
      STensor3 *dgardTdgradfV=&(ipv->getRefTodgradTdgradfV());


      SVector3 *dgradVdfV=&(ipv->getRefTodgradVdfV());
      SVector3 *dgradVdfT=&(ipv->getRefTodgradVdfT());
      SVector3 *dgradTdfT=&(ipv->getRefTodgradTdfT());
      SVector3 *dgradTdfV=&(ipv->getRefTodgradTdfV());

      double *dVdfV=&(ipv->getRefTodVdfV());
      double *dVdfT=&(ipv->getRefTodVdfT());
      double *dTdfT=&(ipv->getRefTodTdfT());
      double *dTdfV=&(ipv->getRefTodTdfV());


      STensorOperation::zero(*dgardVdgradfV);
      dgardVdgradfV->operator()(0,0) =-1./(*fT);
      dgardVdgradfV->operator()(1,1) =-1./(*fT);
      dgardVdgradfV->operator()(2,2) =-1./(*fT);

      STensorOperation::zero(*dgardVdgradfT);
      dgardVdgradfT->operator()(0,0) =(*fV)/(*fT)/(*fT);
      dgardVdgradfT->operator()(1,1) =(*fV)/(*fT)/(*fT);
      dgardVdgradfT->operator()(2,2) =(*fV)/(*fT)/(*fT);

      STensorOperation::zero(*dgardTdgradfT);
      dgardTdgradfT->operator()(0,0) =-1./(*fT)/(*fT);
      dgardTdgradfT->operator()(1,1) =-1./(*fT)/(*fT);
      dgardTdgradfT->operator()(2,2) =-1./(*fT)/(*fT);

      STensorOperation::zero(*dgardTdgradfV);
      *dVdfV=-1./(*fT);
      *dVdfT=(*fV)/(*fT)/(*fT);
      *dTdfT=-1./(*fT)/(*fT);
      *dTdfV=0.;

      dgradVdfV->operator()(0)=1./(*fT)/(*fT)*(gradfT->operator()(0));
      dgradVdfV->operator()(1)=1./(*fT)/(*fT)*(gradfT->operator()(1));
      dgradVdfV->operator()(2)=1./(*fT)/(*fT)*(gradfT->operator()(2));

      dgradVdfT->operator()(0)=1./(*fT)/(*fT)*(gradfV->operator()(0))-2*(*fV)/(*fT)/(*fT)/(*fT)*(gradfT->operator()(0));
      dgradVdfT->operator()(1)=1./(*fT)/(*fT)*(gradfV->operator()(1))-2*(*fV)/(*fT)/(*fT)/(*fT)*(gradfT->operator()(1));
      dgradVdfT->operator()(2)=1./(*fT)/(*fT)*(gradfV->operator()(2))-2*(*fV)/(*fT)/(*fT)/(*fT)*(gradfT->operator()(2));

      dgradTdfT->operator()(0)=2/(*fT)/(*fT)/(*fT)*(gradfT->operator()(0));
      dgradTdfT->operator()(1)=2/(*fT)/(*fT)/(*fT)*(gradfT->operator()(1));
      dgradTdfT->operator()(2)=2/(*fT)/(*fT)/(*fT)*(gradfT->operator()(2));

      dgradTdfV->operator()(0)=0.;
      dgradTdfV->operator()(1)=0.;
      dgradTdfV->operator()(2)=0.;
    }
    else
    {
      double *temperature = &(ipv->getRefToTemperature());
      SVector3 *gradT = &(ipv->getRefToGradT());
      *temperature= 0.;
      STensorOperation::zero(*gradT);

      for (int i = 0; i < nbFF; i++)
      {
        int indexFieldT=3+getNumNonLocalVariable();
        const double& temp = disp(i+indexFieldT*nbFF);
        *temperature += Vals[i+indexFieldT*nbFF]*temp;
        gradT->operator()(0) += Grads[i+indexFieldT*nbFF][0]*temp;
        gradT->operator()(1) += Grads[i+indexFieldT*nbFF][1]*temp;
        gradT->operator()(2) += Grads[i+indexFieldT*nbFF][2]*temp;
      }


      double *Voltage = &(ipv->getRefToVoltage());
      SVector3 *gradV = &(ipv->getRefToGradV());
      *Voltage= 0.;
      STensorOperation::zero(*gradV);

      for (int i = 0; i < nbFF; i++)
      {
        int indexFieldV=3+getNumNonLocalVariable()+1;
        const double& volt = disp(i+indexFieldV*nbFF);
        *Voltage += Vals[i+indexFieldV*nbFF]*volt;
        gradV->operator()(0) += Grads[i+indexFieldV*nbFF][0]*volt;
        gradV->operator()(1) += Grads[i+indexFieldV*nbFF][1]*volt;
        gradV->operator()(2) += Grads[i+indexFieldV*nbFF][2]*volt;
      }
    }
  }
};*/


/*void ElecTherMechDG3DDomain::computeStrain(AllIPState *aips,MInterfaceElement *ie, IntPt *GP,const IPStateBase::whichState ws,
                                        partDomain* efMinus, partDomain *efPlus, fullVector<double> &dispm,
                                        fullVector<double> &dispp, const bool virt)
{
  MElement *ele = dynamic_cast<MElement*>(ie);
  if (!getElementErosionFilter()(ele)) return;

  if(!virt){
    dG3DDomain::computeStrain(aips,ie, GP,ws,efMinus,efPlus, dispm,dispp,virt);
    int npts=integBound->getIntPoints(ele,&GP);
    // Get Gauss points values on minus and plus elements
    IntPt *GPm; IntPt *GPp;
    _interQuad->getIntPoints(ie,GP,&GPm,&GPp);

    FunctionSpaceBase* _spaceminus = this->getMinusDomain()->getFunctionSpace();
    FunctionSpaceBase* _spaceplus = this->getPlusDomain()->getFunctionSpace();


    MElement *em = ie->getElem(0);
    MElement *ep = ie->getElem(1);

    int nbFFm = em->getNumVertices();
    int nbFFp = ep->getNumVertices();

    AllIPState::ipstateElementContainer *vips = aips->getIPstate(ie->getNum());
    for(int j=0;j<npts;j++){
      IPStateBase* ipsm = (*vips)[j];
      IPStateBase* ipsp = (*vips)[j+npts];

      ElecTherMechDG3DIPVariableBase* ipvm = NULL;
      ElecTherMechDG3DIPVariableBase* ipvp = NULL;
      // check if fracture or not via minus if true for minus --> true for plus too
      FractureCohesive3DIPVariable *ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipsm->getState(ws));
      if(ipvtmp!=NULL)
      {
        ipvm = static_cast<ElecTherMechDG3DIPVariableBase*>(ipvtmp->getIPvBulk());
        ipvtmp = static_cast<FractureCohesive3DIPVariable*>(ipsp->getState(ws)); // here sure cast OK
        ipvp = static_cast<ElecTherMechDG3DIPVariableBase*>(ipvtmp->getIPvBulk());
      }
      else
      {
        ipvm = static_cast<ElecTherMechDG3DIPVariableBase*>(ipsm->getState(ws));
        ipvp = static_cast<ElecTherMechDG3DIPVariableBase*>(ipsp->getState(ws));
      }

      const std::vector<TensorialTraits<double>::ValType> &Valsm = ipvm->f(_spaceminus,em,GPm[j]);
      const std::vector<TensorialTraits<double>::ValType> &Valsp = ipvp->f(_spaceplus,ep,GPp[j]);

      const std::vector<TensorialTraits<double>::GradType> &Gradsm = ipvm->gradf(_spaceminus,em,GPm[j]);
      const std::vector<TensorialTraits<double>::GradType> &Gradsp = ipvp->gradf(_spaceplus,ep,GPp[j]);

      if(getUseFtFv())
      {
        double *fTm = &(ipvm->getRefTofT());
        *fTm = 0.;

        SVector3 *gradfTm = &(ipvm->getRefToGradfT());
        STensorOperation::zero(*gradfTm);

         int indexFieldfT=3+getNumNonLocalVariable();
         for (int i = 0; i < nbFFm; i++)
         {
            const double& ftemp = dispm(i+indexFieldfT*nbFFm);
            *fTm += Valsm[i+indexFieldfT*nbFFm]*ftemp;
            gradfTm->operator()(0) += Gradsm[i+indexFieldfT*nbFFm][0]*ftemp;
            gradfTm->operator()(1) += Gradsm[i+indexFieldfT*nbFFm][1]*ftemp;
            gradfTm->operator()(2) += Gradsm[i+indexFieldfT*nbFFm][2]*ftemp;
          }

        double *fTp = &(ipvp->getRefTofT());
        *fTp = 0.;
        SVector3 *gradfTp = &(ipvp->getRefToGradfT());
        STensorOperation::zero(*gradfTp);

        for (int i = 0; i < nbFFp; i++)
        {
          const double& ftemp = dispp(i+indexFieldfT*nbFFp);
          *fTp += Valsp[i+indexFieldfT*nbFFp]*ftemp;
          gradfTp->operator()(0) += Gradsp[i+indexFieldfT*nbFFp][0]*ftemp;
          gradfTp->operator()(1) += Gradsp[i+indexFieldfT*nbFFp][1]*ftemp;
          gradfTp->operator()(2) += Gradsp[i+indexFieldfT*nbFFp][2]*ftemp;
        }

        double& fTjump=ipvm->getRefTofTJump();

        // compute the jump
        computeExtraFieldJump(indexFieldfT,Valsm,nbFFm,Valsp,nbFFp,dispm,dispp,fTjump);
        //Msg::Error("Temperature jump: %e",tempjump);
        ipvp->getRefToOneOverTemperatureJump() = fTjump;


        double *fVm = &(ipvm->getRefTofV());
        *fVm = 0.;

        SVector3 *gradfVm = &(ipvm->getRefToGradfV());
        STensorOperation::zero(*gradfVm);
        int indexFieldfV=3+getNumNonLocalVariable()+1;

        for (int i = 0; i < nbFFm; i++)
        {
          const double& fvolt = dispm(i+indexFieldfV*nbFFm);
          *fVm += Valsm[i+indexFieldfV*nbFFm]*fvolt;
          gradfVm->operator()(0) += Gradsm[i+indexFieldfV*nbFFm][0]*fvolt;
          gradfVm->operator()(1) += Gradsm[i+indexFieldfV*nbFFm][1]*fvolt;
          gradfVm->operator()(2) += Gradsm[i+indexFieldfV*nbFFm][2]*fvolt;
        }

        double *fVp = &(ipvp->getRefTofV());
        *fVp = 0.;
        SVector3 *gradfVp = &(ipvp->getRefToGradfV());
        STensorOperation::zero(*gradfVp);

        for (int i = 0; i < nbFFp; i++)
        {
          const double& fvolt = dispp(i+indexFieldfV*nbFFp);
          *fVp += Valsp[i+indexFieldfV*nbFFp]*fvolt;
          gradfVp->operator()(0) += Gradsp[i+indexFieldfV*nbFFp][0]*fvolt;
          gradfVp->operator()(1) += Gradsp[i+indexFieldfV*nbFFp][1]*fvolt;
          gradfVp->operator()(2) += Gradsp[i+indexFieldfV*nbFFp][2]*fvolt;
        }

        double *Tm = &(ipvm->getRefToTemperature());
        double *Vm = &(ipvm->getRefToVoltage());
        *Vm = -1*(*fVm)/(*fTm);
        *Tm = 1/(*fTm);

        double *Tp = &(ipvp->getRefToTemperature());
        double *Vp = &(ipvp->getRefToVoltage());
        *Vp = -1*(*fVp)/(*fTp);
        *Tp = 1/(*fTp);

        double& fVjump= ipvm->getRefTofvJump();
        // compute the jump
        computeExtraFieldJump(indexFieldfV,Valsm,nbFFm,Valsp,nbFFp,dispm,dispp,fVjump);
        ipvp->getRefTofvJump() = fVjump;

        SVector3 &gradTm = ipvm->getRefToGradT();
        SVector3 &gradVm = ipvm->getRefToGradV();
        SVector3 &gradTp = ipvp->getRefToGradT();
        SVector3 &gradVp = ipvp->getRefToGradV();

        gradVp(0)= -1/(*fTp)*(gradfVp->operator()(0))+(*fVp)/(pow(*fTp,2))*gradfTp->operator()(0);
        gradVp(1)= -1/(*fTp)*(gradfVp->operator()(1))+(*fVp)/(pow(*fTp,2))*gradfTp->operator()(1);
        gradVp(2)= -1/(*fTp)*(gradfVp->operator()(2))+(*fVp)/(pow(*fTp,2))*gradfTp->operator()(2);

        gradVm(0)= -1/(*fTm)*(gradfVm->operator()(0))+(*fVm)/(pow(*fTm,2))*gradfTm->operator()(0);
        gradVm(1)= -1/(*fTm)*(gradfVm->operator()(1))+(*fVm)/(pow(*fTm,2))*gradfTm->operator()(1);
        gradVm(2)= -1/(*fTm)*(gradfVm->operator()(2))+(*fVm)/(pow(*fTm,2))*gradfTm->operator()(2);

        gradTp(0)=-1/(pow(*fTp,2))*gradfTp->operator()(0);
        gradTp(1)=-1/(pow(*fTp,2))*gradfTp->operator()(1);
        gradTp(2)=-1/(pow(*fTp,2))*gradfTp->operator()(2);

        gradTm(0)=-1/(pow(*fTm,2))*gradfTm->operator()(0);
        gradTm(1)=-1/(pow(*fTm,2))*gradfTm->operator()(1);
        gradTm(2)=-1/(pow(*fTm,2))*gradfTm->operator()(2);

        STensor3 &dgardVdgradfVm=ipvm->getRefTodgradVdgradfV();
        STensor3 &dgardVdgradfTm=ipvm->getRefTodgradVdgradfT();
        STensor3 &dgardTdgradfTm=ipvm->getRefTodgradTdgradfT();
        STensor3 &dgardTdgradfVm=ipvm->getRefTodgradTdgradfV();

        SVector3 &dgradVdfVm=ipvm->getRefTodgradVdfV();
        SVector3 &dgradVdfTm=ipvm->getRefTodgradVdfT();
        SVector3 &dgradTdfTm=ipvm->getRefTodgradTdfT();
        SVector3 &dgradTdfVm=ipvm->getRefTodgradTdfV();

        double &dVdfVm=ipvm->getRefTodVdfV();
        double &dVdfTm=ipvm->getRefTodVdfT();
        double &dTdfTm=ipvm->getRefTodTdfT();
        double &dTdfVm=ipvm->getRefTodTdfV();

        STensor3 &dgardVdgradfVp=ipvp->getRefTodgradVdgradfV();
        STensor3 &dgardVdgradfTp=ipvp->getRefTodgradVdgradfT();
        STensor3 &dgardTdgradfTp=ipvp->getRefTodgradTdgradfT();
        STensor3 &dgardTdgradfVp=ipvp->getRefTodgradTdgradfV();

        SVector3 &dgradVdfVp=ipvp->getRefTodgradVdfV();
        SVector3 &dgradVdfTp=ipvp->getRefTodgradVdfT();
        SVector3 &dgradTdfTp=ipvp->getRefTodgradTdfT();
        SVector3 &dgradTdfVp=ipvp->getRefTodgradTdfV();

        double &dVdfVp=ipvp->getRefTodVdfV();
        double &dVdfTp=ipvp->getRefTodVdfT();
        double &dTdfTp=ipvp->getRefTodTdfT();
        double &dTdfVp=ipvp->getRefTodTdfV();


        STensorOperation::diag(dgardVdgradfVm,-1./(*fTm));
        STensorOperation::diag(dgardVdgradfTm,*fVm/(*fTm)/(*fTm));
        STensorOperation::diag(dgardTdgradfTm,-1./(*fTm)/(*fTm));
        STensorOperation::zero(dgardTdgradfVm);

        STensorOperation::diag(dgardVdgradfVp,-1./(*fTp));
        STensorOperation::diag(dgardVdgradfTp,*fVp/(*fTp)/(*fTp));
        STensorOperation::diag(dgardTdgradfTp,-1./(*fTp)/(*fTp));
        STensorOperation::zero(dgardTdgradfVp);

        dVdfVm=-1./(*fTm);
        dVdfTm=(*fVm)/(*fTm)/(*fTm);
        dTdfTm=-1./(*fTm)/(*fTm);
        dTdfVm=0.;

        dVdfVp=-1./(*fTp);
        dVdfTp=(*fVp)/(*fTp)/(*fTp);
        dTdfTp=-1./(*fTp)/(*fTp);
        dTdfVp=0.;

        dgradVdfVm(0)=1./(*fTm)/(*fTm)*(gradfTm->operator()(0));
        dgradVdfVm(1)=1./(*fTm)/(*fTm)*(gradfTm->operator()(1));
        dgradVdfVm(2)=1./(*fTm)/(*fTm)*(gradfTm->operator()(2));

        dgradVdfVp(0)=1./(*fTp)/(*fTp)*(gradfTp->operator()(0));
        dgradVdfVp(1)=1./(*fTp)/(*fTp)*(gradfTp->operator()(1));
        dgradVdfVp(2)=1./(*fTp)/(*fTp)*(gradfTp->operator()(2));

        dgradVdfTm(0)=1./(*fTm)/(*fTm)*(gradfVm->operator()(0))-2*(*fVm)/(*fTm)/(*fTm)/(*fTm)*(gradfTm->operator()(0));
        dgradVdfTm(1)=1./(*fTm)/(*fTm)*(gradfVm->operator()(1))-2*(*fVm)/(*fTm)/(*fTm)/(*fTm)*(gradfTm->operator()(1));
        dgradVdfTm(2)=1./(*fTm)/(*fTm)*(gradfVm->operator()(2))-2*(*fVm)/(*fTm)/(*fTm)/(*fTm)*(gradfTm->operator()(2));

        dgradVdfTp(0)=1./(*fTp)/(*fTp)*(gradfVp->operator()(0))-2*(*fVp)/(*fTp)/(*fTp)/(*fTp)*(gradfTp->operator()(0));
        dgradVdfTp(1)=1./(*fTp)/(*fTp)*(gradfVp->operator()(1))-2*(*fVp)/(*fTp)/(*fTp)/(*fTp)*(gradfTp->operator()(1));
        dgradVdfTp(2)=1./(*fTp)/(*fTp)*(gradfVp->operator()(2))-2*(*fVp)/(*fTp)/(*fTp)/(*fTp)*(gradfTp->operator()(2));

        dgradTdfTm(0)=2/(*fTm)/(*fTm)/(*fTm)*(gradfTm->operator()(0));
        dgradTdfTm(1)=2/(*fTm)/(*fTm)/(*fTm)*(gradfTm->operator()(1));
        dgradTdfTm(2)=2/(*fTm)/(*fTm)/(*fTm)*(gradfTm->operator()(2));

        dgradTdfTp(0)=2/(*fTp)/(*fTp)/(*fTp)*(gradfTp->operator()(0));
        dgradTdfTp(1)=2/(*fTp)/(*fTp)/(*fTp)*(gradfTp->operator()(1));
        dgradTdfTp(2)=2/(*fTp)/(*fTp)/(*fTp)*(gradfTp->operator()(2));

        dgradTdfVm(0)=0.;
        dgradTdfVm(1)=0.;
        dgradTdfVm(2)=0.;

        dgradTdfVp(0)=0.;
        dgradTdfVp(1)=0.;
        dgradTdfVp(2)=0.;
      }
      else
      {
        int indexFieldT=3+getNumNonLocalVariable();
        double *temperaturem = &(ipvm->getRefToTemperature());
        *temperaturem = 0.;

        SVector3 *gradTm = &(ipvm->getRefToGradT());
        STensorOperation::zero(*gradTm);

        for (int i = 0; i < nbFFm; i++)
        {
          const double& temp = dispm(i+indexFieldT*nbFFm);
          *temperaturem += Valsm[i+indexFieldT*nbFFm]*temp;
          gradTm->operator()(0) += Gradsm[i+indexFieldT*nbFFm][0]*temp;
          gradTm->operator()(1) += Gradsm[i+indexFieldT*nbFFm][1]*temp;
          gradTm->operator()(2) += Gradsm[i+indexFieldT*nbFFm][2]*temp;
        }

        double *temperaturep = &(ipvp->getRefToTemperature());
        *temperaturep = 0.;
        SVector3 *gradTp = &(ipvp->getRefToGradT());
        STensorOperation::zero(*gradTp);

        for (int i = 0; i < nbFFp; i++)
        {
          const double& temp = dispp(i+indexFieldT*nbFFp);
          *temperaturep += Valsp[i+indexFieldT*nbFFp]*temp;
          gradTp->operator()(0) += Gradsp[i+indexFieldT*nbFFp][0]*temp;
          gradTp->operator()(1) += Gradsp[i+indexFieldT*nbFFp][1]*temp;
          gradTp->operator()(2) += Gradsp[i+indexFieldT*nbFFp][2]*temp;
        }

        double& tempjump= ipvm->getRefToTemperatureJump();
        // compute the jump
        computeExtraFieldJump(indexFieldT,Valsm,nbFFm,Valsp,nbFFp,dispm,dispp,tempjump);
        //Msg::Error("Temperature jump: %e",tempjump);
        ipvp->getRefToTemperatureJump() = tempjump;


        double *voltagem = &(ipvm->getRefToVoltage());
        *voltagem = 0.;
        SVector3 *gradVm = &(ipvm->getRefToGradV());
        STensorOperation::zero(*gradVm);
        int indexFieldV=3+getNumNonLocalVariable()+1;

        for (int i = 0; i < nbFFm; i++)
        {
          const double& volt = dispm(i+indexFieldV*nbFFm);
          *voltagem += Valsm[i+indexFieldV*nbFFm]*volt;
          gradVm->operator()(0) += Gradsm[i+indexFieldV*nbFFm][0]*volt;
          gradVm->operator()(1) += Gradsm[i+indexFieldV*nbFFm][1]*volt;
          gradVm->operator()(2) += Gradsm[i+indexFieldV*nbFFm][2]*volt;
        }
        double *voltagep = &(ipvp->getRefToVoltage());
        *voltagep = 0.;
        SVector3 *gradVp = &(ipvp->getRefToGradV());
        STensorOperation::zero(*gradVp);

        for (int i = 0; i < nbFFp; i++)
        {
          const double& volt = dispp(i+indexFieldV*nbFFp);
          *voltagep += Valsp[i+indexFieldV*nbFFp]*volt;
          gradVp->operator()(0) += Gradsp[i+indexFieldV*nbFFp][0]*volt;
          gradVp->operator()(1) += Gradsp[i+indexFieldV*nbFFp][1]*volt;
          gradVp->operator()(2) += Gradsp[i+indexFieldV*nbFFp][2]*volt;
        }

        double& voltjump= ipvm->getRefToVoltageJump();
        // compute the jump
        computeExtraFieldJump(indexFieldV,Valsm,nbFFm,Valsp,nbFFp,dispm,dispp,voltjump);
        ipvp->getRefToVoltageJump() = voltjump;


        double fvjump=0.;double ftjump=0.;
        double dftjumpdTp=0.;double dftjumpdTm=0.;
        double dfvjumpdvp, dfvjumpdvm,dfvjumpdTm,dfvjumpdTp;
        dfvjumpdvp=0.; dfvjumpdvm=0.;dfvjumpdTm=0.;dfvjumpdTp=0.;

        computeftjump(indexFieldT,Valsm,nbFFm,Valsp,nbFFp,dispm,dispp,ftjump,dftjumpdTp,dftjumpdTm);
        computefvjump(indexFieldT,indexFieldV,Valsm,nbFFm,Valsp,nbFFp,dispm,dispp,fvjump,dfvjumpdTp,dfvjumpdTm,dfvjumpdvp,dfvjumpdvm);

        ipvm->getRefTofvJump() = fvjump;
        ipvp->getRefTofvJump() = fvjump;

        ipvm->getRefToOneOverTemperatureJump() = ftjump;
        ipvp->getRefToOneOverTemperatureJump() = ftjump;

        ipvm->getRefTodfvjumpdvm() = dfvjumpdvm;
        ipvp->getRefTodfvjumpdvp() = dfvjumpdvp;

        ipvm->getRefTodfvjumpdTm() = dfvjumpdTm;
        ipvp->getRefTodfvjumpdTp() = dfvjumpdTp;

        ipvm->getRefTodOneOverTemperatureJumpdTm() = dftjumpdTm;
        ipvp->getRefTodOneOverTemperatureJumpdTp() = dftjumpdTp;
      }
    }
  }
  else
  {  // Virtual interface element
     Msg::Error("No virtual interface for 3D");
  }
}*/

/*void ElecTherMechDG3DDomain::createTerms(unknownField *uf,IPField*ip)
{
  FunctionSpace<double>* dgspace = static_cast<FunctionSpace<double>*>(_space);
  ltermBulk = new ElecTherMechDG3DForceBulk(*dgspace,_mlaw,_fullDg,ip,_incrementNonlocalBased,getElecTherMechEqRatio(), getUseFtFv());
  if(_fullDg)
    ltermBound = new ElecTherMechDG3DForceInter(*dgspace,dgspace,_mlaw,_mlaw,_interQuad,_beta1,_fullDg,ip,_incrementNonlocalBased,getElecTherMechStabilityParameter(),
                  getElecTherMechContinuity(), getElecTherMechEqRatio(), getUseFtFv());
  else
     ltermBound = new nonLinearTermVoid();
  if(_bmbp)
    btermBulk = new BilinearTermPerturbation<double>(ltermBulk,*dgspace,*dgspace,uf,ip,this,_eps);
  else
   btermBulk = new ElecTherMechDG3DStiffnessBulk(*dgspace,_mlaw,_fullDg,ip,getElecTherMechEqRatio(), getUseFtFv());
  if(_fullDg)
  {
    if(_interByPert)
      btermBound = new BilinearTermPerturbation<double>(ltermBound,*dgspace,*dgspace,uf,ip,this,_eps);
    else
      btermBound = new ElecTherMechDG3DStiffnessInter(*dgspace,*dgspace,_mlaw,_mlaw,_interQuad,_beta1,ip,uf,_fullDg, _incrementNonlocalBased,
                        getElecTherMechStabilityParameter() ,getElecTherMechContinuity(),getElecTherMechEqRatio(), getUseFtFv());
  }
  else
    btermBound = new BiNonLinearTermVoid();

  this->btermVirtBound = new BiNonLinearTermVoid();
  this->ltermVirtBound = new nonLinearTermVoid();


  massterm = new ElecTherMechMassG3D(*dgspace,_mlaw);

}*/

/*LinearTermBase<double>* ElecTherMechDG3DDomain::createNeumannTerm(FunctionSpace<double> *spneu,  const elementGroup* g,
                                                                  const simpleFunctionTime<double>* f,const unknownField *uf,
                                                                  const IPField * ip,nonLinearBoundaryCondition::location onWhat) const
{
  //  return  new ElecTherMechG3DLoadTerm<double>(*spneu,f);

}*/

/* void ElecTherMechDG3DDomain::allocateInterfaceMPI(const int rankOtherPart, const elementGroup &groupOtherPart,dgPartDomain**  dgdom)
{
 // domain creation has it function create a domain with a part of the same domain contained on an other part,
  // the type of domain is known
  if(*dgdom==NULL){ // domain doesn't exist --> creation
    *dgdom = new ElecTherMechInterDomainBetween3D(this->getTag(),this,this,this->getElecTherMechEqRatio(),0); // TO CHANGE HERE
    // set the stability parameters
    ElecTherMechInterDomainBetween3D* newInterDom3D = static_cast< ElecTherMechInterDomainBetween3D* >(*dgdom);
    newInterDom3D->stabilityParameters(this->stabilityParameter(1));
    newInterDom3D->ElecTherMechStabilityParameters(this->getElecTherMechStabilityParameter(),this->getElecTherMechContinuity());
    newInterDom3D->useFtFv(this->getUseFtFv());
  }
}*/


ElecTherMechInterDomainBetween3D::ElecTherMechInterDomainBetween3D(const int tag, partDomain *dom1,partDomain *dom2,const double ElecTherMechEqRatio, const int lnum, const int bnum) :
                                  interDomainBase(dom1,dom2,bnum),
                                  ElecTherMechDG3DDomain(tag,manageInterface::getKey(dom1->getPhysical(),
                                                                    dom2->getPhysical()),10000,lnum,true,ElecTherMechEqRatio,dom1->getDim())
{
/*
  // create empty elementGroup
  g = new elementGroup();
  // set functionalSpace
  if((_domMinus->getFunctionSpaceType() == functionSpaceType::Lagrange) and
     (_domPlus->getFunctionSpaceType() == functionSpaceType::Lagrange)){
    if(_space != NULL) delete _space;
    _space = new dG3DLagrangeBetween2DomainsFunctionSpace(5,_spaceMinus,_spacePlus);
    _wsp = functionSpaceType::Lagrange;
  }
  else{
    Msg::Error("Impossible to create functional space on ElecTherMechInterDomain3D %d",_phys);
  }
  _continuity=(static_cast<ElecTherMechDG3DDomain *> (dom1))->getElecTherMechContinuity();
  _ElecTherMechBeta=(static_cast<ElecTherMechDG3DDomain *> (dom1))->getElecTherMechStabilityParameter();*/
  setConstitutiveExtraDofDiffusionEqRatio(ElecTherMechEqRatio);

}


ElecTherMechInterDomainBetween3D::ElecTherMechInterDomainBetween3D(const ElecTherMechInterDomainBetween3D &source) :
interDomainBase(source), ElecTherMechDG3DDomain(source)
{

}

/*void ElecTherMechInterDomainBetween3D::initializeTerms(unknownField *uf,IPField*ip)
{

  createTerms(uf,ip);
  initializeFractureBase(uf,ip, gi, integBound);
}*/

/*void ElecTherMechInterDomainBetween3D::setGaussIntegrationRule()
{
  setGaussIntegrationRuleBase(gi, &integBound, &integBulk,&_interQuad, _gaussorderbound, _gqt);
}*/

/*void ElecTherMechInterDomainBetween3D::computeAllIPStrain(AllIPState *aips,const unknownField *ufield,const IPStateBase::whichState ws, const bool virt){
  IntPt *GP;
  fullVector<double> dispm;
  fullVector<double> dispp;
  std::vector<Dof> R;
  for(elementGroup::elementContainer::iterator it=gi->begin(); it!=gi->end(); ++it){
    MInterfaceElement *iele = dynamic_cast<MInterfaceElement*>(*it);
    R.clear();
    thi->getMinusDomain()->getFunctionSpace)->getKeys(iele->getElem(0),R);
    dispm.resize(R.size());
    ufield->get(R,dispm);
    R.clear();
    this->getPlusDomain()->getFunctionSpace()->getKeys(iele->getElem(1),R);
    dispp.resize(R.size());
    ufield->get(R,dispp);

    this->computeStrain(aips,iele,GP,ws,_domMinus,_domPlus,dispm,dispp,virt);
  }
};*/


/*void ElecTherMechInterDomainBetween3D::computeIPVariable(AllIPState *aips,const unknownField *ufield,
                                                const IPStateBase::whichState ws, bool stiff)
{

 if (ws == IPStateBase::initial){
    this->initialIPVariable(aips,ufield,ws,stiff);
    return;
  }


  _evalStiff = false;
  #if defined(HAVE_MPI)
  if ((_mlawMinus->isNumeric() or _mlawPlus->isNumeric()) and this->_otherRanks.size()>0){
    this->computeIPVariableMPI(aips,ufield,ws,stiff);
  }
  else
  #endif //HAVE_MPI
  {
    this->computeAllIPStrain(aips,ufield,ws,false);
    IntPt *GP;
    fullVector<double> dispm;
    fullVector<double> dispp;
    std::vector<Dof> R;
    dG3DMaterialLaw *mlawMinus = static_cast<dG3DMaterialLaw*>(_mlawMinus);
    dG3DMaterialLaw *mlawPlus = static_cast<dG3DMaterialLaw*>(_mlawPlus);
    for(elementGroup::elementContainer::iterator it=gi->begin(); it!=gi->end(); ++it){
      MInterfaceElement *iele = dynamic_cast<MInterfaceElement*>(*it);
      int npts_inter= integBound->getIntPoints(*it,&GP);
      R.clear();
      this->getMinusDomain()->getFunctionSpace()->getKeys(iele->getElem(0),R);
      dispm.resize(R.size());
      ufield->get(R,dispm);
      R.clear();
      this->getPlusDomain()->getFunctionSpace()->getKeys(iele->getElem(1),R);
      dispp.resize(R.size());
      ufield->get(R,dispp);
      this->computeIpv(aips,iele,GP,ws,_domMinus,_domPlus,mlawMinus,mlawPlus,dispm,dispp,false,stiff);
    }
  }
  _evalStiff = true;
}*/

/*void ElecTherMechInterDomainBetween3D::setMaterialLaw(const std::map<int,materialLaw*> &maplaw)
{
  // oterwise the law is already set
  if(!setmaterial){
    _domMinus->setMaterialLaw(maplaw);
    _domPlus->setMaterialLaw(maplaw);
    _mlaw = NULL;
    if(_lnum == 0){ // no fracture law prescribed --> materialLawMinus and Plus are given via domain
      _mlawMinus = _domMinus->getMaterialLaw();
      _mlawPlus = _domPlus->getMaterialLaw();
      if(_domMinus->getPhysical() == _domPlus->getPhysical())
      {
         _mlaw = _mlawMinus; // same law on both
      }
      else if(_mlawMinus->getNum() == _mlawPlus->getNum())
      {
        _mlaw = _mlawMinus; // same law on both
      }
      else
      {
       //
	Msg::Warning("Cannot set the interface law on interDomain3D");
      }
    }
    else{
      // create law (use lnum with negative number same number for the two but it doesn't matter)
      // these law don't appear in map with all laws
      // check if minus and plus laws are already fracture by 2 laws;
      int numlawBulkMinus, numlawBulkPlus;
      if(_domMinus->getMaterialLaw()->getType() != materialLaw::fracture){
        numlawBulkMinus = _domMinus->getLawNum();
      }
      else{
        materialLaw2LawsInitializer *m2law = dynamic_cast<materialLaw2LawsInitializer*>(_domMinus->getMaterialLaw());
        numlawBulkMinus = m2law->bulkLawNumber();
      }
      if(_domPlus->getMaterialLaw()->getType() != materialLaw::fracture){
        numlawBulkPlus = _domPlus->getLawNum();
      }
      else{
        materialLaw2LawsInitializer *m2law = dynamic_cast<materialLaw2LawsInitializer*>(_domPlus->getMaterialLaw());
        numlawBulkPlus = m2law->bulkLawNumber();
      }
      _mlawMinus = new FractureByCohesive3DLaw(-numlawBulkMinus,numlawBulkMinus,_lnum);
      _mlawPlus = new FractureByCohesive3DLaw(-numlawBulkPlus,numlawBulkPlus,_lnum);
    }
    _mlawMinus->initLaws(maplaw);
    _mlawPlus->initLaws(maplaw);
    setmaterial=true;
  }
}*/
/*
void ElecTherMechInterDomainBetween3D::createTerms(unknownField *uf,IPField*ip)
{
  // void term (as they have to exist)
  this->ltermBulk = new nonLinearTermVoid();
  this->btermBulk = new BiNonLinearTermVoid();

  this->btermVirtBound = new BiNonLinearTermVoid();
  this->ltermVirtBound = new nonLinearTermVoid();
  this->massterm = new BiNonLinearTermVoid();

  FunctionSpace<double>* dgspace = static_cast<FunctionSpace<double>*>(_space);
  FunctionSpace<double>* dgspace1 = static_cast<FunctionSpace<double>*>(_spaceMinus);
  FunctionSpace<double>* dgspace2 = static_cast<FunctionSpace<double>*>(_spacePlus);
  dG3DDomain* dgldomMinus = static_cast<dG3DDomain*>(_domMinus);
  dG3DDomain* dgldomPlus = static_cast<dG3DDomain*>(_domPlus);
  ltermBound = new ElecTherMechDG3DForceInter(*dgspace1,dgspace2,_mlawMinus,_mlawPlus,_interQuad,_beta1,true,ip, _incrementNonlocalBased,
                    getElecTherMechStabilityParameter() ,getElecTherMechContinuity(),getElecTherMechEqRatio(), getUseFtFv());
  if(_interByPert)
    btermBound = new BilinearTermPerturbation<double>(ltermBound,*dgspace1,*dgspace2,uf,ip,this,_eps);
  else
    btermBound = new ElecTherMechDG3DStiffnessInter(*dgspace1,*dgspace2,_mlawMinus,_mlawPlus,_interQuad,_beta1,ip,uf,true, _incrementNonlocalBased,
                           getElecTherMechStabilityParameter() ,getElecTherMechContinuity(),getElecTherMechEqRatio(), getUseFtFv());
}*/

ElecMagTherMechDG3DDomain::ElecMagTherMechDG3DDomain(const int tag, const int phys, const int sp_, const int lnum, const int fdg,
const double ElecMagTherMechEqRatio, const int dim)
: dG3DDomain(tag,phys,sp_,lnum,fdg,dim,0,2,1)
{
    setConstitutiveExtraDofDiffusionEqRatio(ElecMagTherMechEqRatio);
    setConstitutiveCurlEqRatio(ElecMagTherMechEqRatio);
}

ElecMagTherMechDG3DDomain::ElecMagTherMechDG3DDomain(const ElecMagTherMechDG3DDomain &source)
: dG3DDomain(source)
{}


void ElecMagTherMechDG3DDomain::computeStrain(MElement *e,  const int npts, IntPt *GP,
                             AllIPState::ipstateElementContainer *vips,
                             IPStateBase::whichState ws, fullVector<double> &disp, bool useBarF) const
{
  dG3DDomain::computeStrain(e, npts, GP, vips, ws, disp, useBarF);
  //
  if(evaluateCurlField[0]) {
      const mixedFunctionSpaceBase *mixedSpace = dynamic_cast<const mixedFunctionSpaceBase *>(this->getFunctionSpace());

      if (mixedSpace == NULL) {
          Msg::Error("mixedFunctionSpaceBase must be used in ElecMagTherMechDG3DDomain::computeStrain!!!");
      }

      int numberStandardKeys = mixedSpace->getNumKeysByType(mixedFunctionSpaceBase::DOF_STANDARD, e);
      int numberCurlKeys = mixedSpace->getNumKeysByType(mixedFunctionSpaceBase::DOF_CURL, e);
      //
      const CurlFunctionSpaceBase<double> *curlSpace = dynamic_cast<const CurlFunctionSpaceBase<double> *>(this->getFunctionSpace());

      //ofstream  outfile;
      //outfile.open("CM3_sf.txt" /*+std::to_string(e->getNum())+*/);
      for (int j = 0; j < npts; j++) {
          IPStateBase *ips = (*vips)[j];

          double u = GP[j].pt[0];
          double v = GP[j].pt[1];
          double w = GP[j].pt[2];

          std::vector<SVector3> curlVals;
          // get values at GP
          curlSpace->fcurl(e, u, v, w, "HcurlLegendre", curlVals); // basis for unknown vector field at edges

          std::vector<SVector3> CurlcurlVals;
          curlSpace->Curlfcurl(e, u, v, w, "CurlHcurlLegendre",
                               CurlcurlVals); // basis for Curl of unknown vector field at edges

          // Test to output cm3 computed sf vals
          // against gmsh sf vals
          // Output to file
          /*outfile << "Elem: " << e->getNum() << std::endl;
          outfile << "GP: " << u  << ", " << v  << ", " << w << std::endl;
          outfile << "curlVals: " << "\t \t " << "CurlcurlVals: " << std::endl;
          for (int i = 0; i < numberCurlKeys; i++)
          {
              outfile << curlVals[i][0] << ", " << curlVals[i][1] << ", " << curlVals[i][2] << "\t \t" <<
              CurlcurlVals[i][0] << ", " << CurlcurlVals[i][1] << ", " << CurlcurlVals[i][2] << std::endl;
          }
          outfile << std::endl;*/

          //x y z
          dG3DIPVariableBase *ipv = static_cast<dG3DIPVariableBase *>(ips->getState(ws));
          // in case of number of curl fields > 1
          for (int curlField = 0; curlField < ipv->getNumConstitutiveCurlVariable(); ++curlField) {
              SVector3 &vectorPotA = ipv->getRefToVectorPotential(curlField);
              SVector3 &vectorCurlB = ipv->getRefToVectorCurl(curlField);
              STensorOperation::zero(vectorPotA);
              STensorOperation::zero(vectorCurlB);
              // for each edge of element e
              for (int i = 0; i < numberCurlKeys; i++) {
                  // comptue edge field at each GPs
                  // Vector Potential A at GP from A at edge dofs
                  vectorPotA(0) += curlVals[i][0] * disp(i + numberStandardKeys);
                  vectorPotA(1) += curlVals[i][1] * disp(i + numberStandardKeys);
                  vectorPotA(2) += curlVals[i][2] * disp(i + numberStandardKeys);

                  // Vector Curl Field B at GP from A at edge dofs
                  vectorCurlB(0) += CurlcurlVals[i][0] * disp(i + numberStandardKeys);
                  vectorCurlB(1) += CurlcurlVals[i][1] * disp(i + numberStandardKeys);
                  vectorCurlB(2) += CurlcurlVals[i][2] * disp(i + numberStandardKeys);
              }
          }
      }
      //outfile.close();
  }
}

ElecMagTherMechInterDomainBetween3D::ElecMagTherMechInterDomainBetween3D(const int tag, partDomain *dom1, partDomain *dom2,
const double ElecMagTherMechEqRatio, const int lnum, const int bnum)
: interDomainBase(dom1,dom2,bnum), ElecMagTherMechDG3DDomain(tag,manageInterface::getKey(dom1->getPhysical(),
                                                             dom2->getPhysical()),10000,lnum,true,ElecMagTherMechEqRatio,
                                                             dom1->getDim())
{
    setConstitutiveExtraDofDiffusionEqRatio(ElecMagTherMechEqRatio);
    setConstitutiveCurlEqRatio(ElecMagTherMechEqRatio);
}

ElecMagTherMechInterDomainBetween3D::ElecMagTherMechInterDomainBetween3D(const ElecMagTherMechInterDomainBetween3D &source)
: interDomainBase(source), ElecMagTherMechDG3DDomain(source)
{}
