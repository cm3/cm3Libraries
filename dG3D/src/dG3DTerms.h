//
// C++ Interface: terms
//
// Description: Class of terms for dg
//
//
// Author:  <L. Noels>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef DG3DTERMS_H
#define DG3DTERMS_H
#include "SVector3.h"
#include <string>
#include "dG3DDomain.h"
#include "dG3DFunctionSpace.h"
#include "dG3DMaterialLaw.h"
#include "simpleFunction.h"
#include "unknownField.h"
#include "ipField.h"
#include "terms.h"
#include "interfaceQuadrature.h"
#include <sstream>


template<class T1,class T2> class g3DBilinearTerm : public BiNonLinearTermBase
{
 protected :
  FunctionSpace<T1>& space1;
  FunctionSpace<T2>& space2;
  int _dim;
  double _nonLocalEqRatio;
  int _numNonLocalVariable;
  int _numConstitutiveExtraDofDiffusionVariable;
  int _numConstitutiveCurlVariable;
  double _constitutiveExtraDofDiffusionEqRatio;
  double _constitutiveCurlEqRatio;
  bool _constitutiveExtraDofDiffusionUseEnergyConjugatedField; //we can use it when considering 1/fT
  bool _constitutiveExtraDofDiffusionAccountFieldSource;
  bool _constitutiveExtraDofDiffusionAccountMecaSource;
  bool _constitutiveCurlAccountFieldSource;
  bool _constitutiveCurlAccountMecaSource;
 public :
  g3DBilinearTerm(FunctionSpace<T1>& space1_,FunctionSpace<T2>& space2_) : space1(space1_),space2(space2_), 
          _nonLocalEqRatio(1.), _numNonLocalVariable(0), _numConstitutiveExtraDofDiffusionVariable(0), _numConstitutiveCurlVariable(0),
          _constitutiveExtraDofDiffusionEqRatio(1.), _constitutiveCurlEqRatio(1.),
          _constitutiveExtraDofDiffusionUseEnergyConjugatedField(false), _constitutiveExtraDofDiffusionAccountFieldSource(true),
          _constitutiveExtraDofDiffusionAccountMecaSource(false),
          _constitutiveCurlAccountFieldSource(true), _constitutiveCurlAccountMecaSource(false),_dim(3)
  {}
  virtual ~g3DBilinearTerm() {}
  virtual void setDim(int num){_dim=num;}
  virtual int getDim() const {return _dim;}
  virtual void setNonLocalEqRatio(double eq) {_nonLocalEqRatio=eq;}
  virtual double getNonLocalEqRatio() const {return _nonLocalEqRatio;}
  virtual void setNumNonLocalVariable(int num) {_numNonLocalVariable=num;}
  virtual int getNumNonLocalVariable() const {return _numNonLocalVariable;}
  virtual void setNumConstitutiveExtraDofDiffusionVariable(int num) {_numConstitutiveExtraDofDiffusionVariable=num;}
  virtual int getNumConstitutiveExtraDofDiffusionVariable() const {return _numConstitutiveExtraDofDiffusionVariable;}
  virtual void setNumConstitutiveCurlVariable(int num) {_numConstitutiveCurlVariable=num;}
  virtual int getNumConstitutiveCurlVariable() const {return _numConstitutiveCurlVariable;}
  virtual void setConstitutiveExtraDofDiffusionEqRatio(double eq) {_constitutiveExtraDofDiffusionEqRatio = eq;}
  virtual double getConstitutiveExtraDofDiffusionEqRatio() const {return _constitutiveExtraDofDiffusionEqRatio;}
  virtual void setConstitutiveCurlEqRatio(double eq) {_constitutiveCurlEqRatio = eq;}
  virtual double getConstitutiveCurlEqRatio() const {return _constitutiveCurlEqRatio;}
  virtual void setConstitutiveExtraDofDiffusionUseEnergyConjugatedField( bool oft){_constitutiveExtraDofDiffusionUseEnergyConjugatedField=oft;} //we can use it when considering 1/fT
  virtual bool getConstitutiveExtraDofDiffusionUseEnergyConjugatedField()const { return _constitutiveExtraDofDiffusionUseEnergyConjugatedField;} //we can use it when considering 1/fT
  virtual void setConstitutiveExtraDofDiffusionAccountFieldSource(bool so){_constitutiveExtraDofDiffusionAccountFieldSource=so;}
  virtual bool getConstitutiveExtraDofDiffusionAccountFieldSource() const {return _constitutiveExtraDofDiffusionAccountFieldSource;}
  virtual void setConstitutiveExtraDofDiffusionAccountMecaSource(bool so) {_constitutiveExtraDofDiffusionAccountMecaSource=so;}
  virtual bool getConstitutiveExtraDofDiffusionAccountMecaSource() const { return _constitutiveExtraDofDiffusionAccountMecaSource;}
  virtual void setConstitutiveCurlAccountFieldSource(bool so) {_constitutiveCurlAccountFieldSource = so;} 
  virtual bool getConstitutiveCurlAccountFieldSource() const {return _constitutiveCurlAccountFieldSource;}
  virtual void setConstitutiveCurlAccountMecaSource(bool so) {_constitutiveCurlAccountMecaSource = so;} 
  virtual bool getConstitutiveCurlAccountMecaSource() const {return _constitutiveCurlAccountMecaSource;}

};

template<class T1,class T2> class dG3DBilinearTerm : public g3DBilinearTerm<T1,T2>
{
  protected:
    double _stabilityParameter;
    //non local
    bool _nonLocalContinuity;
    double _nonLocalStabilityParameter;
    //constitutive extra dof
    double _constitutiveExtraDofDiffusionStabilityParameter;
    bool _constitutiveExtraDofDiffusionContinuity;
    
  public:
    virtual void setNonLocalContinuity(bool nlCont) {_nonLocalContinuity=nlCont;}
    virtual bool getNonLocalContinuity() const {return _nonLocalContinuity;}
    virtual void setNonLocalStabilityParameter(double nlBeta) {_nonLocalStabilityParameter=nlBeta;}
    virtual double getNonLocalStabilityParameter() const {return _nonLocalStabilityParameter;}
    virtual void setStabilityParameter(double Beta) {_stabilityParameter=Beta;}
    virtual double getStabilityParameter() const {return _stabilityParameter;}
    virtual void setConstitutiveExtraDofDiffusionStabilityParameter(double Beta) {_constitutiveExtraDofDiffusionStabilityParameter=Beta;}
    virtual double getConstitutiveExtraDofDiffusionStabilityParameter() const {return _constitutiveExtraDofDiffusionStabilityParameter;}
    virtual void setConstitutiveExtraDofDiffusionContinuity(bool cont){ _constitutiveExtraDofDiffusionContinuity=cont;}
    virtual bool getConstitutiveExtraDofDiffusionContinuity() const {return _constitutiveExtraDofDiffusionContinuity;}
    dG3DBilinearTerm(FunctionSpace<T1>& space1_,FunctionSpace<T2>& space2_, const double beta1_): g3DBilinearTerm<T1,T2>(space1_,space2_), _stabilityParameter(beta1_),
                                                                _nonLocalContinuity(true), _nonLocalStabilityParameter(beta1_),
                                                                _constitutiveExtraDofDiffusionStabilityParameter(beta1_), _constitutiveExtraDofDiffusionContinuity(true){};
    virtual ~dG3DBilinearTerm(){};
};

template<class T1> class g3DLinearTerm : public nonLinearTermBase<double>
{
 protected :
  FunctionSpace<T1>& space1;
  int _dim;
  double _nonLocalEqRatio;
  int _numNonLocalVariable; 
  int _numConstitutiveExtraDofDiffusionVariable;
  int _numConstitutiveCurlVariable;
  double _constitutiveExtraDofDiffusionEqRatio;
  double _constitutiveCurlEqRatio;
  bool _constitutiveExtraDofDiffusionUseEnergyConjugatedField; //we can use it when considering 1/fT
  bool _constitutiveExtraDofDiffusionAccountFieldSource;
  bool _constitutiveExtraDofDiffusionAccountMecaSource;
  bool _constitutiveCurlAccountFieldSource;
  bool _constitutiveCurlAccountMecaSource;

 public :
  virtual void setDim(int num){_dim=num;}
  virtual int getDim() const {return _dim;}
  virtual void setNonLocalEqRatio(double eq) {_nonLocalEqRatio=eq;}
  virtual double getNonLocalEqRatio() const {return _nonLocalEqRatio;}
  virtual void setNumNonLocalVariable(int num) {_numNonLocalVariable=num;}
  virtual int getNumNonLocalVariable() const {return _numNonLocalVariable;}
  virtual void setNumConstitutiveExtraDofDiffusionVariable(int num) {_numConstitutiveExtraDofDiffusionVariable=num;}
  virtual int getNumConstitutiveExtraDofDiffusionVariable() const {return _numConstitutiveExtraDofDiffusionVariable;}
  virtual void setNumConstitutiveCurlVariable(int num) {_numConstitutiveCurlVariable=num;}
  virtual int getNumConstitutiveCurlVariable() const {return _numConstitutiveCurlVariable;}
  virtual void setConstitutiveExtraDofDiffusionEqRatio(double eq) {_constitutiveExtraDofDiffusionEqRatio = eq;}
  virtual double getConstitutiveExtraDofDiffusionEqRatio() const {return _constitutiveExtraDofDiffusionEqRatio;}
  virtual void setConstitutiveCurlEqRatio(double eq) {_constitutiveCurlEqRatio = eq;}
  virtual double getConstitutiveCurlEqRatio() const {return _constitutiveCurlEqRatio;}
  virtual void setConstitutiveExtraDofDiffusionUseEnergyConjugatedField( bool oft){_constitutiveExtraDofDiffusionUseEnergyConjugatedField=oft;} //we can use it when considering 1/fT
  virtual bool getConstitutiveExtraDofDiffusionUseEnergyConjugatedField()const { return _constitutiveExtraDofDiffusionUseEnergyConjugatedField;} //we can use it when considering 1/fT
  virtual void setConstitutiveExtraDofDiffusionAccountFieldSource(bool so){_constitutiveExtraDofDiffusionAccountFieldSource=so;}
  virtual bool getConstitutiveExtraDofDiffusionAccountFieldSource() const {return _constitutiveExtraDofDiffusionAccountFieldSource;}
  virtual void setConstitutiveExtraDofDiffusionAccountMecaSource(bool so) {_constitutiveExtraDofDiffusionAccountMecaSource=so;}
  virtual bool getConstitutiveExtraDofDiffusionAccountMecaSource() const { return _constitutiveExtraDofDiffusionAccountMecaSource;}
  virtual void setConstitutiveCurlAccountFieldSource(bool so) {_constitutiveCurlAccountFieldSource = so;} 
  virtual bool getConstitutiveCurlAccountFieldSource() const {return _constitutiveCurlAccountFieldSource;}
  virtual void setConstitutiveCurlAccountMecaSource(bool so) {_constitutiveCurlAccountMecaSource = so;} 
  virtual bool getConstitutiveCurlAccountMecaSource() const {return _constitutiveCurlAccountMecaSource;}

  g3DLinearTerm(FunctionSpace<T1>& space1_) : space1(space1_), 
                                     _nonLocalEqRatio(1.), _numNonLocalVariable(0), 
                                     _numConstitutiveExtraDofDiffusionVariable(0), _numConstitutiveCurlVariable(0),
                                     _constitutiveExtraDofDiffusionEqRatio(1.), _constitutiveCurlEqRatio(1.),
                                     _constitutiveExtraDofDiffusionUseEnergyConjugatedField(false), _constitutiveExtraDofDiffusionAccountFieldSource(true),
                                     _constitutiveExtraDofDiffusionAccountMecaSource(false),
                                     _constitutiveCurlAccountFieldSource(true), _constitutiveCurlAccountMecaSource(false),_dim(3)
  {}
  virtual ~g3DLinearTerm() {}
};




template<class T1>
class dG3DLinearTerm: public g3DLinearTerm<T1>{
  protected:
    double _stabilityParameter;
    //non local
    bool _nonLocalContinuity;
    double _nonLocalStabilityParameter;
    //constitutive extra dof
    double _constitutiveExtraDofDiffusionStabilityParameter;
    bool _constitutiveExtraDofDiffusionContinuity;
 
  public:
    virtual void setNonLocalContinuity(bool nlCont) {_nonLocalContinuity=nlCont;}
    virtual bool getNonLocalContinuity() const {return _nonLocalContinuity;}
    virtual void setNonLocalStabilityParameter(double nlBeta) {_nonLocalStabilityParameter=nlBeta;}
    virtual double getNonLocalStabilityParameter() const {return _nonLocalStabilityParameter;}
    virtual void setStabilityParameter(double Beta) {_stabilityParameter=Beta;}
    virtual double getStabilityParameter() const {return _stabilityParameter;}
    virtual void setConstitutiveExtraDofDiffusionStabilityParameter(double Beta) {_constitutiveExtraDofDiffusionStabilityParameter=Beta;}
    virtual double getConstitutiveExtraDofDiffusionStabilityParameter() const {return _constitutiveExtraDofDiffusionStabilityParameter;}
    virtual void setConstitutiveExtraDofDiffusionContinuity(bool cont){ _constitutiveExtraDofDiffusionContinuity=cont;}
    virtual bool getConstitutiveExtraDofDiffusionContinuity() const {return _constitutiveExtraDofDiffusionContinuity;}
    dG3DLinearTerm(FunctionSpace<T1>& space1_, const double beta1_): g3DLinearTerm<T1>(space1_), _stabilityParameter(beta1_),
                                                                _nonLocalContinuity(true), _nonLocalStabilityParameter(beta1_),
                                                                _constitutiveExtraDofDiffusionStabilityParameter(beta1_), _constitutiveExtraDofDiffusionContinuity(true){};
    virtual ~dG3DLinearTerm(){};
};



class g3DLoadTerm : public g3DLinearTerm<double>
{
  protected:
    const simpleFunctionTime<double> *Load;
    int comp;
    
  public :
    g3DLoadTerm(FunctionSpace<double>& space1_, const simpleFunctionTime<double> *Load_, const int c) : g3DLinearTerm<double>(space1_),Load(Load_),comp(c)
    {
    }
    virtual ~g3DLoadTerm() {}
    virtual void set(const fullVector<double> *datafield){}
    virtual const bool isData() const{return false;}
    virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullVector<double> > &mv) const
    {
      Msg::Error("Define me get by integration point g3DLoadTerm");
    }
    virtual void get(MElement *ele,int npts,IntPt *GP,fullVector<double> &m) const;
    virtual LinearTermBase<double>* clone () const
    {
      g3DLoadTerm* loadterm= new g3DLoadTerm(this->space1,Load,comp);
      loadterm->setDim(this->getDim());
      loadterm->setNonLocalEqRatio(this->getNonLocalEqRatio());
      loadterm->setNumNonLocalVariable(this->getNumNonLocalVariable());
      loadterm->setNumConstitutiveExtraDofDiffusionVariable(this->getNumConstitutiveExtraDofDiffusionVariable());
      loadterm->setNumConstitutiveCurlVariable(this->getNumConstitutiveCurlVariable());
      loadterm->setConstitutiveExtraDofDiffusionEqRatio(this->getConstitutiveExtraDofDiffusionEqRatio());
      loadterm->setConstitutiveCurlEqRatio(this->getConstitutiveCurlEqRatio());
      loadterm->setConstitutiveExtraDofDiffusionUseEnergyConjugatedField(this->getConstitutiveExtraDofDiffusionUseEnergyConjugatedField());
      loadterm->setConstitutiveExtraDofDiffusionAccountFieldSource(this->getConstitutiveExtraDofDiffusionAccountFieldSource());
      loadterm->setConstitutiveExtraDofDiffusionAccountMecaSource(this->getConstitutiveExtraDofDiffusionAccountMecaSource());
      loadterm->setConstitutiveCurlAccountFieldSource(this->getConstitutiveCurlAccountFieldSource());
      loadterm->setConstitutiveCurlAccountMecaSource(this->getConstitutiveCurlAccountMecaSource());
      return loadterm;
    }
};

//
inline void computeNormalOfElement(const partDomain* d, MElement* ele, SVector3& normal){
  if (ele->getDim() == 2){
    SPoint3 center= ele->barycenter();
    double xyz[3] = {center[0],center[1],center[2]};
    double uvw[3];
    ele->xyz2uvw(xyz,uvw);
    SVector3 phi0[2];
    STensorOperation::zero(phi0[0]);
    STensorOperation::zero(phi0[1]);

    const FunctionSpace<double>* space = dynamic_cast<const FunctionSpace<double>*> (d->getFunctionSpace());
    std::vector<TensorialTraits<double>::GradType> gradUVW;
    space->gradfuvw(ele,uvw[0],uvw[1],uvw[2],gradUVW);
    int nbFF = ele->getNumShapeFunctions();
    for(int k=0;k<nbFF;k++)
    {
      double x = ele->getVertex(k)->x();
      double y = ele->getVertex(k)->y();
      double z = ele->getVertex(k)->z();
      //evaulate displacements at interface
      for(int i=0;i<2;i++)
      {
       phi0[i](0) += gradUVW[k](i)*x;
       phi0[i](1) += gradUVW[k+nbFF](i)*y;
       phi0[i](2) += gradUVW[k+nbFF+nbFF](i)*z;
      }
    }
    normal = crossprod(phi0[0],phi0[1]);
    normal.normalize();
  }
}

//
// the pressure term int_A (p*ncur*dA)
// using Nanson formula ncur*dA = J*Nref*invF*dA0, with J=detF
// leading to the term int_A (p*ncur*dA) =  int_A0 p J*Nref*invF*dA0
//  nonlinearity du to J*invF 
class g3DFiniteStrainsPressureTerm : public g3DLinearTerm<double>
{
  protected:
    const partDomain* dom; // the domain this term belongs to
    const simpleFunctionTime<double> *Load;
    const unknownField* _ufield;
    SVector3 normalToDomain; // in case of 2D simulations, normal of the domain (normalToDomain) must be determined 

  public :
    g3DFiniteStrainsPressureTerm(const partDomain* d, FunctionSpace<double>& space1_,  const unknownField* ufield,
                          const simpleFunctionTime<double> *Load_) : dom(d), 
                          g3DLinearTerm<double>(space1_),Load(Load_), _ufield(ufield)
    {
      if (dom->getDim() == 2){
        computeNormalOfElement(dom,dom->element_begin()->second,normalToDomain);
      }
    }
    virtual ~g3DFiniteStrainsPressureTerm()
    {
    }
    virtual void set(const fullVector<double> *datafield){}
    virtual const bool isData() const{return false;}
    virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullVector<double> > &mv) const
    {
      Msg::Error("Define me get by integration point g3DFiniteStrainsPressureTerm");
    }
    virtual void get(MElement *ele,int npts,IntPt *GP,fullVector<double> &m) const;
    virtual LinearTermBase<double>* clone () const
    {
      g3DFiniteStrainsPressureTerm* pressureterm = new g3DFiniteStrainsPressureTerm(dom,this->space1,_ufield,Load);
      pressureterm->setDim(this->getDim());
      pressureterm->setNonLocalEqRatio(this->getNonLocalEqRatio());
      pressureterm->setNumNonLocalVariable(this->getNumNonLocalVariable());
      pressureterm->setNumConstitutiveExtraDofDiffusionVariable(this->getNumConstitutiveExtraDofDiffusionVariable());
      pressureterm->setNumConstitutiveCurlVariable(this->getNumConstitutiveCurlVariable());
      pressureterm->setConstitutiveExtraDofDiffusionEqRatio(this->getConstitutiveExtraDofDiffusionEqRatio());
      pressureterm->setConstitutiveCurlEqRatio(this->getConstitutiveCurlEqRatio());
      pressureterm->setConstitutiveExtraDofDiffusionUseEnergyConjugatedField(this->getConstitutiveExtraDofDiffusionUseEnergyConjugatedField());
      pressureterm->setConstitutiveExtraDofDiffusionAccountFieldSource(this->getConstitutiveExtraDofDiffusionAccountFieldSource());
      pressureterm->setConstitutiveExtraDofDiffusionAccountMecaSource(this->getConstitutiveExtraDofDiffusionAccountMecaSource());
      pressureterm->setConstitutiveCurlAccountFieldSource(this->getConstitutiveCurlAccountFieldSource());
      pressureterm->setConstitutiveCurlAccountMecaSource(this->getConstitutiveCurlAccountMecaSource());
      return pressureterm;
    }
};

class g3DFiniteStrainsPressureBilinearTerm : public g3DBilinearTerm<double,double>
{
 protected:
  const partDomain* dom; // the domain this term belongs to
  const simpleFunctionTime<double> *Load;
  const unknownField* _ufield;
  SVector3 normalToDomain;
  
 public :
  g3DFiniteStrainsPressureBilinearTerm(const partDomain* d, FunctionSpace<double>& space1_,  const unknownField* ufield,
                          const simpleFunctionTime<double> *Load_) : g3DBilinearTerm<double,double>(space1_,space1_),
                          dom(d),Load(Load_), _ufield(ufield)
  {
    if (dom->getDim() == 2){
      computeNormalOfElement(dom,dom->element_begin()->second,normalToDomain);
    }
  }
  virtual ~g3DFiniteStrainsPressureBilinearTerm()
  {
  }
  virtual void set(const fullVector<double> *datafield){}
  virtual const bool isData() const{return false;}

  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const
  {
    Msg::Error("Define me get by integration point g3DFiniteStrainsPressureBilinearTerm");
  }
  virtual void get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const;
  
  
  virtual BilinearTermBase* clone () const
  {
    g3DFiniteStrainsPressureBilinearTerm* pressurebterm = new g3DFiniteStrainsPressureBilinearTerm(dom,this->space1,_ufield,Load);
    pressurebterm->setDim(this->getDim());
    pressurebterm->setNonLocalEqRatio(this->getNonLocalEqRatio());
    pressurebterm->setNumNonLocalVariable(this->getNumNonLocalVariable());
    pressurebterm->setNumConstitutiveExtraDofDiffusionVariable(this->getNumConstitutiveExtraDofDiffusionVariable());
    pressurebterm->setNumConstitutiveCurlVariable(this->getNumConstitutiveCurlVariable());
    pressurebterm->setConstitutiveExtraDofDiffusionEqRatio(this->getConstitutiveExtraDofDiffusionEqRatio());
    pressurebterm->setConstitutiveCurlEqRatio(this->getConstitutiveCurlEqRatio());
    pressurebterm->setConstitutiveExtraDofDiffusionUseEnergyConjugatedField(this->getConstitutiveExtraDofDiffusionUseEnergyConjugatedField());
    pressurebterm->setConstitutiveExtraDofDiffusionAccountFieldSource(this->getConstitutiveExtraDofDiffusionAccountFieldSource());
    pressurebterm->setConstitutiveExtraDofDiffusionAccountMecaSource(this->getConstitutiveExtraDofDiffusionAccountMecaSource());
    pressurebterm->setConstitutiveCurlAccountFieldSource(this->getConstitutiveCurlAccountFieldSource());
    pressurebterm->setConstitutiveCurlAccountMecaSource(this->getConstitutiveCurlAccountMecaSource());
    return pressurebterm;
  }
};

// This class allows to put a different scalar flux on each dof components
// Example alows puting a thermal and a electrical flux at the same time to avoid
// extra computation
// the scalar flux term int_A f*dA, f is the scalar flux value
// using Nanson formula ncur*dA = J*Nref*invF*dA0, with J=detF
// leading to dA = ||J*Nref*invF||*dA0
// leading to the term int_A f dA =  int_A0 f*||J*Nref*invF||*dA0
//  nonlinearity du to J*Nref*invF*ncur
// 
class g3DFiniteStrainsScalarFluxLinearTerm : public g3DLinearTerm<double>
{
  protected:
    const partDomain* _dom;
    // needed to compute the deformation gradient
    const unknownField* _ufield;
    // component on which the scalar flux is applied
    int _comp;
    // load function for each scalar flux
    const simpleFunctionTime<double>* _load; // one load function per component
    SVector3 normalToDomain;
    
  public:
    // one function has to be given per components
    g3DFiniteStrainsScalarFluxLinearTerm(const partDomain* d, FunctionSpace<double>& space1_,
                                    const unknownField* ufield,
                                    int comp, const simpleFunctionTime<double>* f) : g3DLinearTerm<double>(space1_), _dom(d), _ufield(ufield),
                                                                      _comp(comp), _load(f)
    {      
      if (_dom->getDim() == 2){
        computeNormalOfElement(_dom,_dom->element_begin()->second,normalToDomain);
      }
    }
  // internal copy constructor for the clone method
  protected:
    g3DFiniteStrainsScalarFluxLinearTerm(const partDomain* d, FunctionSpace<double>& space1_, const unknownField* ufield, int comp_,
                                     const simpleFunctionTime<double>* load_, const SVector3& normal) : g3DLinearTerm<double>(space1_), _dom(d),
                                                                                    _ufield(ufield),
                                                                                     _comp(comp_),_load(load_), 
                                                                                     normalToDomain(normal){}
  public:
    virtual ~g3DFiniteStrainsScalarFluxLinearTerm(){}
    virtual void set(const fullVector<double> *datafield){}
    virtual const bool isData() const{return false;}

    virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullVector<double> > &mv) const
    {
      Msg::Error("Define me get by integration point g3DFiniteStrainsScalarFluxLinearTerm");
    }
    virtual void get(MElement *ele,int npts,IntPt *GP,fullVector<double> &m) const;
    virtual LinearTermBase<double>* clone () const
    {
      g3DFiniteStrainsScalarFluxLinearTerm* flux =  new g3DFiniteStrainsScalarFluxLinearTerm(_dom,this->space1,_ufield,_comp,_load,normalToDomain);
      flux->setDim(this->getDim());
      flux->setNonLocalEqRatio(this->getNonLocalEqRatio());
      flux->setNumNonLocalVariable(this->getNumNonLocalVariable());
      flux->setNumConstitutiveExtraDofDiffusionVariable(this->getNumConstitutiveExtraDofDiffusionVariable());
      flux->setNumConstitutiveCurlVariable(this->getNumConstitutiveCurlVariable());
      flux->setConstitutiveExtraDofDiffusionEqRatio(this->getConstitutiveExtraDofDiffusionEqRatio());
      flux->setConstitutiveCurlEqRatio(this->getConstitutiveCurlEqRatio());
      flux->setConstitutiveExtraDofDiffusionUseEnergyConjugatedField(this->getConstitutiveExtraDofDiffusionUseEnergyConjugatedField());
      flux->setConstitutiveExtraDofDiffusionAccountFieldSource(this->getConstitutiveExtraDofDiffusionAccountFieldSource());
      flux->setConstitutiveExtraDofDiffusionAccountMecaSource(this->getConstitutiveExtraDofDiffusionAccountMecaSource());
      flux->setConstitutiveCurlAccountFieldSource(this->getConstitutiveCurlAccountFieldSource());
      flux->setConstitutiveCurlAccountMecaSource(this->getConstitutiveCurlAccountMecaSource());
      return flux;
    }
};

// Correspondin matrix
class g3DFiniteStrainsScalarFluxBiLinearTerm : public g3DBilinearTerm<double,double>
{
  protected:
    // needed to compute the deformation gradient
    const partDomain* _dom;
    const unknownField* _ufield;
    int _comp;
    const simpleFunctionTime<double>* _load; // one load function per component
    SVector3 normalToDomain;

  public:
    // space1 only flux dofs, space2 all dof i.e. with displacement too
    g3DFiniteStrainsScalarFluxBiLinearTerm(const partDomain* d, FunctionSpace<double> &space1_, FunctionSpace<double> & space2_,
                                            const unknownField* ufield,
                                            int comp, const simpleFunctionTime<double>* f) : g3DBilinearTerm<double,double>(space1_,space2_),  _dom(d),
                                                                              _ufield(ufield), _comp(comp), _load(f)
    {
      if (_dom->getDim() == 2){
        computeNormalOfElement(_dom,_dom->element_begin()->second,normalToDomain);
      }
    }


  protected:
   // protected constructor for the copy
    g3DFiniteStrainsScalarFluxBiLinearTerm(const partDomain* d, FunctionSpace<double> &space1_, FunctionSpace<double> & space2_,
                                          const unknownField* ufield,
                                          int comp,
                                          const simpleFunctionTime<double>* load_,
                                          const SVector3& normal): g3DBilinearTerm<double,double>(space1_,space2_),  _ufield(ufield), _comp(comp), _load(load_),
                                                    _dom(d),normalToDomain(normal){}
    virtual ~g3DFiniteStrainsScalarFluxBiLinearTerm()
    {
    }
    virtual void set(const fullVector<double> *datafield){}
    virtual const bool isData() const{return false;}

    virtual void get(MElement *ele, int npts, IntPt *GP, fullMatrix<double> &m) const;
    virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const
    {
      Msg::Error("Define me get by gauss point g3DFiniteStrainsScalarFluxBiLinearTerm");
    }

    virtual BilinearTermBase* clone () const
    {
      g3DFiniteStrainsScalarFluxBiLinearTerm* fluxbiterm =  new g3DFiniteStrainsScalarFluxBiLinearTerm(_dom,this->space1,this->space2,_ufield,_comp,_load,normalToDomain);
      fluxbiterm->setDim(this->getDim());
      fluxbiterm->setNonLocalEqRatio(this->getNonLocalEqRatio());
      fluxbiterm->setNumNonLocalVariable(this->getNumNonLocalVariable());
      fluxbiterm->setNumConstitutiveExtraDofDiffusionVariable(this->getNumConstitutiveExtraDofDiffusionVariable());
      fluxbiterm->setNumConstitutiveCurlVariable(this->getNumConstitutiveCurlVariable());
      fluxbiterm->setConstitutiveExtraDofDiffusionEqRatio(this->getConstitutiveExtraDofDiffusionEqRatio());
      fluxbiterm->setConstitutiveCurlEqRatio(this->getConstitutiveCurlEqRatio());
      fluxbiterm->setConstitutiveExtraDofDiffusionUseEnergyConjugatedField(this->getConstitutiveExtraDofDiffusionUseEnergyConjugatedField());
      fluxbiterm->setConstitutiveExtraDofDiffusionAccountFieldSource(this->getConstitutiveExtraDofDiffusionAccountFieldSource());
      fluxbiterm->setConstitutiveExtraDofDiffusionAccountMecaSource(this->getConstitutiveExtraDofDiffusionAccountMecaSource());
      fluxbiterm->setConstitutiveCurlAccountFieldSource(this->getConstitutiveCurlAccountFieldSource());
      fluxbiterm->setConstitutiveCurlAccountMecaSource(this->getConstitutiveCurlAccountMecaSource());
      return fluxbiterm;
    }
};


class dG3DForceBulk : public g3DLinearTerm<double>
{
 protected:
  const dG3DMaterialLaw* _mlaw;
  const IPField *_ipf;
  const bool _fullDg;
  const bool _incrementNonlocalBased;

 public:
  dG3DForceBulk(FunctionSpace<double>& space1_,const materialLaw* mlaw,bool FullDG,
                                       const IPField *ip, const bool increBased) : g3DLinearTerm<double>(space1_),
                                                                                 _fullDg(FullDG),
                                                                                 _ipf(ip),
                                                                                 _mlaw(static_cast<const dG3DMaterialLaw*>(mlaw)),
                                                                                 _incrementNonlocalBased(increBased)
  {

  }
  virtual ~dG3DForceBulk(){}
  virtual void get(MElement *ele,int npts,IntPt *GP,fullVector<double> &m) const;
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullVector<double> > &mv) const
  {
    Msg::Error("Define me get by integration point dG3DForceBulk");
  }

  virtual const bool isData() const {return false;}
  virtual void set(const fullVector<double> *datafield){}
  virtual LinearTermBase<double>* clone () const
  {
    dG3DForceBulk* DG= new dG3DForceBulk(space1,_mlaw,_fullDg,_ipf,_incrementNonlocalBased); // we need a copy constructor or to pass all the parameters in the constructor
    DG->setDim(this->getDim());
    DG->setNonLocalEqRatio(this->getNonLocalEqRatio());
    DG->setNumNonLocalVariable(this->getNumNonLocalVariable());
    DG->setNumConstitutiveExtraDofDiffusionVariable(this->getNumConstitutiveExtraDofDiffusionVariable());
    DG->setNumConstitutiveCurlVariable(this->getNumConstitutiveCurlVariable());
    DG->setConstitutiveExtraDofDiffusionEqRatio(this->getConstitutiveExtraDofDiffusionEqRatio());
    DG->setConstitutiveCurlEqRatio(this->getConstitutiveCurlEqRatio());
    DG->setConstitutiveExtraDofDiffusionUseEnergyConjugatedField(this->getConstitutiveExtraDofDiffusionUseEnergyConjugatedField());
    DG->setConstitutiveExtraDofDiffusionAccountFieldSource(this->getConstitutiveExtraDofDiffusionAccountFieldSource());
    DG->setConstitutiveExtraDofDiffusionAccountMecaSource(this->getConstitutiveExtraDofDiffusionAccountMecaSource());
    DG->setConstitutiveCurlAccountFieldSource(this->getConstitutiveCurlAccountFieldSource());
    DG->setConstitutiveCurlAccountMecaSource(this->getConstitutiveCurlAccountMecaSource());
    return DG;
  }
};



class dG3DStiffnessBulk : public g3DBilinearTerm<double,double>
{
 protected:
  const dG3DMaterialLaw* _mlaw;
  const IPField *_ipf;
  const bool _fullDg;
  bool sym;

 public:
  dG3DStiffnessBulk(FunctionSpace<double>& space1_,FunctionSpace<double>& space2_, const materialLaw* mlaw,
                                  bool FullDG, const IPField *ip) : g3DBilinearTerm<double,double>(space1_,space2_),
                                                            _fullDg(FullDG), _ipf(ip), _mlaw(static_cast< const dG3DMaterialLaw*>(mlaw))
  {
    sym=(&space1_==&space2_);
  }

  dG3DStiffnessBulk(FunctionSpace<double>& space1_, const materialLaw* mlaw,
                                  bool FullDG,const IPField *ip) : g3DBilinearTerm<double,double>(space1_,space1_),
                                                             _fullDg(FullDG), _ipf(ip),
                                                             _mlaw(static_cast<const dG3DMaterialLaw*>(mlaw))
  {
    sym=true;
  }
  virtual ~dG3DStiffnessBulk(){}
  virtual const bool isData() const {return false;}
  virtual void set(const fullVector<double> *datafield){}
  
  virtual void get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const;
  virtual void get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m, bool considerNonElasticOnes, bool removeDamage) const;

  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const
  {
    Msg::Error("Define me get by gauss point dG3DStiffnessBulk");
  }
  virtual BilinearTermBase* clone () const
  {
    dG3DStiffnessBulk *DG= new dG3DStiffnessBulk(space1,_mlaw,_fullDg,_ipf);
    DG->setDim(this->getDim());
    DG->setNonLocalEqRatio(this->getNonLocalEqRatio());
    DG->setNumNonLocalVariable(this->getNumNonLocalVariable());
    DG->setNumConstitutiveExtraDofDiffusionVariable(this->getNumConstitutiveExtraDofDiffusionVariable());
    DG->setNumConstitutiveCurlVariable(this->getNumConstitutiveCurlVariable());
    DG->setConstitutiveExtraDofDiffusionEqRatio(this->getConstitutiveExtraDofDiffusionEqRatio());
    DG->setConstitutiveCurlEqRatio(this->getConstitutiveCurlEqRatio());
    DG->setConstitutiveExtraDofDiffusionUseEnergyConjugatedField(this->getConstitutiveExtraDofDiffusionUseEnergyConjugatedField());
    DG->setConstitutiveExtraDofDiffusionAccountFieldSource(this->getConstitutiveExtraDofDiffusionAccountFieldSource());
    DG->setConstitutiveExtraDofDiffusionAccountMecaSource(this->getConstitutiveExtraDofDiffusionAccountMecaSource());
    DG->setConstitutiveCurlAccountFieldSource(this->getConstitutiveCurlAccountFieldSource());
    DG->setConstitutiveCurlAccountMecaSource(this->getConstitutiveCurlAccountMecaSource());
    return DG;
  }
};

class dG3DElasticStiffnessBulk : public dG3DStiffnessBulk{
  public:
    dG3DElasticStiffnessBulk(FunctionSpace<double>& space1_,FunctionSpace<double>& space2_, const materialLaw* mlaw,
                                  bool FullDG, const IPField *ip)  :dG3DStiffnessBulk(space1_,space2_,mlaw,FullDG,ip){};
    dG3DElasticStiffnessBulk(FunctionSpace<double>& space1_, const materialLaw* mlaw,
                                  bool FullDG,const IPField *ip): dG3DStiffnessBulk(space1_,mlaw,FullDG,ip){}
    virtual ~dG3DElasticStiffnessBulk(){}
    virtual void get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const;
    virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const
    {
      Msg::Error("Define me get by gauss point dG3DElasticStiffnessBulk");
    }
    virtual BilinearTermBase* clone () const
    {
      dG3DElasticStiffnessBulk *DG= new dG3DElasticStiffnessBulk(space1,_mlaw,_fullDg,_ipf);
      DG->setDim(this->getDim());
      DG->setNonLocalEqRatio(this->getNonLocalEqRatio());
      DG->setNumNonLocalVariable(this->getNumNonLocalVariable());
      DG->setNumConstitutiveExtraDofDiffusionVariable(this->getNumConstitutiveExtraDofDiffusionVariable());
      DG->setNumConstitutiveCurlVariable(this->getNumConstitutiveCurlVariable());
      DG->setConstitutiveExtraDofDiffusionEqRatio(this->getConstitutiveExtraDofDiffusionEqRatio());
      DG->setConstitutiveCurlEqRatio(this->getConstitutiveCurlEqRatio());
      DG->setConstitutiveExtraDofDiffusionUseEnergyConjugatedField(this->getConstitutiveExtraDofDiffusionUseEnergyConjugatedField());
      DG->setConstitutiveExtraDofDiffusionAccountFieldSource(this->getConstitutiveExtraDofDiffusionAccountFieldSource());
      DG->setConstitutiveExtraDofDiffusionAccountMecaSource(this->getConstitutiveExtraDofDiffusionAccountMecaSource());
      DG->setConstitutiveCurlAccountFieldSource(this->getConstitutiveCurlAccountFieldSource());
      DG->setConstitutiveCurlAccountMecaSource(this->getConstitutiveCurlAccountMecaSource());
      return DG;
    }
};

class dG3DUndamagedElasticStiffnessBulk : public dG3DStiffnessBulk{
  public:
    dG3DUndamagedElasticStiffnessBulk(FunctionSpace<double>& space1_,FunctionSpace<double>& space2_, const materialLaw* mlaw,
                                  bool FullDG, const IPField *ip)  :dG3DStiffnessBulk(space1_,space2_,mlaw,FullDG,ip){};
    dG3DUndamagedElasticStiffnessBulk(FunctionSpace<double>& space1_, const materialLaw* mlaw,
                                  bool FullDG,const IPField *ip): dG3DStiffnessBulk(space1_,mlaw,FullDG,ip){}
    virtual ~dG3DUndamagedElasticStiffnessBulk(){}
    virtual void get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const;
    virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const
    {
      Msg::Error("Define me get by gauss point dG3DUndamagedElasticStiffnessBulk");
    }
    virtual BilinearTermBase* clone () const
    {
      dG3DUndamagedElasticStiffnessBulk *DG= new dG3DUndamagedElasticStiffnessBulk(space1,_mlaw,_fullDg,_ipf);
      DG->setDim(this->getDim());
      DG->setNonLocalEqRatio(this->getNonLocalEqRatio());
      DG->setNumNonLocalVariable(this->getNumNonLocalVariable());
      DG->setNumConstitutiveExtraDofDiffusionVariable(this->getNumConstitutiveExtraDofDiffusionVariable());
      DG->setNumConstitutiveCurlVariable(this->getNumConstitutiveCurlVariable());
      DG->setConstitutiveExtraDofDiffusionEqRatio(this->getConstitutiveExtraDofDiffusionEqRatio());
      DG->setConstitutiveCurlEqRatio(this->getConstitutiveCurlEqRatio());
      DG->setConstitutiveExtraDofDiffusionUseEnergyConjugatedField(this->getConstitutiveExtraDofDiffusionUseEnergyConjugatedField());
      DG->setConstitutiveExtraDofDiffusionAccountFieldSource(this->getConstitutiveExtraDofDiffusionAccountFieldSource());
      DG->setConstitutiveExtraDofDiffusionAccountMecaSource(this->getConstitutiveExtraDofDiffusionAccountMecaSource());
      DG->setConstitutiveCurlAccountFieldSource(this->getConstitutiveCurlAccountFieldSource());
      DG->setConstitutiveCurlAccountMecaSource(this->getConstitutiveCurlAccountMecaSource());
      return DG;
    }
};

class dG3DForceInter : public dG3DLinearTerm<double>
{
 protected:
  nlsFunctionSpace<double> *_minusSpace;
  nlsFunctionSpace<double> *_plusSpace;
  const dG3DMaterialLaw *_mlawMinus;
  const dG3DMaterialLaw *_mlawPlus;
  interfaceQuadratureBase *_interQuad;
  const IPField *_ipf;
  bool _fullDg;
  bool _incrementNonlocalBased;

 public:
  dG3DForceInter(FunctionSpace<double>& space1_, FunctionSpace<double> *space2_,
                                       const materialLaw *mlawMinus, const materialLaw *mlawPlus,interfaceQuadratureBase *iquad,
                                       double beta1_,
                                       bool fdg, const IPField *ip, const bool increBased) : dG3DLinearTerm<double>(space1_,beta1_),
                                                                _minusSpace(static_cast<nlsFunctionSpace<double>*>(&space1_)),
                                                                _plusSpace(static_cast<nlsFunctionSpace<double>*>(space2_)),
                                                                _ipf(ip),
                                                                _mlawMinus(static_cast<const dG3DMaterialLaw*>(mlawMinus)),
                                                                _mlawPlus(static_cast<const dG3DMaterialLaw*>(mlawPlus)),
                                                                _interQuad(iquad),_fullDg(fdg),_incrementNonlocalBased(increBased)

  {
  }
  virtual ~dG3DForceInter() {}
  

  virtual const bool isData() const{return false;}
  virtual void set(const fullVector<double> *datafield){}
  virtual void get(MElement *ele, int npts, IntPt *GP, fullVector<double> &v)const;
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullVector<double> > &mv) const
  {
    Msg::Error("Define me get by integration point dG3DForceInter");
  }
  virtual LinearTermBase<double>* clone () const
  {
    dG3DForceInter* DG= new dG3DForceInter(*_minusSpace,_plusSpace,_mlawMinus,_mlawPlus,_interQuad,_stabilityParameter,_fullDg,_ipf, _incrementNonlocalBased); // we need a copy constructor or to pass all the parameters in the constructor
    DG->setDim(this->getDim());
    DG->setNonLocalEqRatio(this->getNonLocalEqRatio());
    DG->setNumNonLocalVariable(this->getNumNonLocalVariable());
    DG->setNonLocalContinuity(this->getNonLocalContinuity());
    DG->setNonLocalStabilityParameter(this->getNonLocalStabilityParameter());
    DG->setNumConstitutiveExtraDofDiffusionVariable(this->getNumConstitutiveExtraDofDiffusionVariable());
    DG->setConstitutiveExtraDofDiffusionEqRatio(this->getConstitutiveExtraDofDiffusionEqRatio());
    DG->setNumConstitutiveCurlVariable(this->getNumConstitutiveCurlVariable());
    DG->setConstitutiveCurlEqRatio(this->getConstitutiveCurlEqRatio());
    DG->setConstitutiveExtraDofDiffusionUseEnergyConjugatedField(this->getConstitutiveExtraDofDiffusionUseEnergyConjugatedField());
    DG->setConstitutiveExtraDofDiffusionAccountFieldSource(this->getConstitutiveExtraDofDiffusionAccountFieldSource());
    DG->setConstitutiveExtraDofDiffusionAccountMecaSource(this->getConstitutiveExtraDofDiffusionAccountMecaSource());
    DG->setConstitutiveExtraDofDiffusionStabilityParameter(this->getConstitutiveExtraDofDiffusionStabilityParameter());
    DG->setConstitutiveExtraDofDiffusionContinuity(this->getConstitutiveExtraDofDiffusionContinuity());
    DG->setConstitutiveCurlAccountFieldSource(this->getConstitutiveCurlAccountFieldSource());
    DG->setConstitutiveCurlAccountMecaSource(this->getConstitutiveCurlAccountMecaSource());
    return DG;
  }
};

class dG3DStiffnessInter : public dG3DBilinearTerm<double,double>
{
 protected:
  nlsFunctionSpace<double> *_minusSpace;
  nlsFunctionSpace<double> *_plusSpace;
  const dG3DMaterialLaw *_mlawMinus;
  const dG3DMaterialLaw *_mlawPlus;
  interfaceQuadratureBase *_interQuad;
  unknownField *_ufield; // used for fracture. In that case the matrix is computed by perturbation
  const IPField *_ipf;
  const double _perturbation;
  bool _fullDg;
  bool _incrementNonlocalBased;

 public:
  dG3DStiffnessInter(FunctionSpace<double>& space1_,FunctionSpace<double>& space2_,
                                    const materialLaw* mlawMinus, const materialLaw* mlawPlus,interfaceQuadratureBase *iquad,
                                    double beta1_, const IPField *ip, unknownField *uf,
                                    bool fulldg, const bool increBased, double eps=1.e-6) : dG3DBilinearTerm<double,double>(space1_,space2_,beta1_),
                                                                     _minusSpace(static_cast<nlsFunctionSpace<double>*>(&space1_)),
                                                                     _plusSpace(static_cast<nlsFunctionSpace<double>*>(&space2_)),
                                                                     _ipf(ip), _ufield(uf),
                                                                     _mlawMinus(static_cast<const dG3DMaterialLaw*>(mlawMinus)),
                                                                     _mlawPlus(static_cast<const dG3DMaterialLaw*>(mlawPlus)),
                                                                     _interQuad(iquad),_fullDg(fulldg), _perturbation(eps), 
                                                                     _incrementNonlocalBased(increBased)
  {}
  virtual ~dG3DStiffnessInter() {}
  virtual const bool isData() const{return false;}
  virtual void set(const fullVector<double> *datafield){}
  
  virtual void get(MElement *ele,int npts,IntPt *GP, fullMatrix<double> &m)const;
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const
  {
    Msg::Error("Define me ?? get by integration point dG3DStiffnessInter");
  }
  virtual BilinearTermBase* clone () const
  {
    dG3DStiffnessInter* DG= new dG3DStiffnessInter(space1,space2,_mlawMinus,_mlawPlus,_interQuad,_stabilityParameter,
                                              _ipf,_ufield,_fullDg, _incrementNonlocalBased,_perturbation); // we need a copy constructor or to pass all the parameters in the constructor
    DG->setDim(this->getDim());
    DG->setNonLocalEqRatio(this->getNonLocalEqRatio());
    DG->setNumNonLocalVariable(this->getNumNonLocalVariable());
    DG->setNonLocalContinuity(this->getNonLocalContinuity());
    DG->setNonLocalStabilityParameter(this->getNonLocalStabilityParameter());
    DG->setNumConstitutiveExtraDofDiffusionVariable(this->getNumConstitutiveExtraDofDiffusionVariable());
    DG->setConstitutiveExtraDofDiffusionEqRatio(this->getConstitutiveExtraDofDiffusionEqRatio());
    DG->setNumConstitutiveCurlVariable(this->getNumConstitutiveCurlVariable());
    DG->setConstitutiveCurlEqRatio(this->getConstitutiveCurlEqRatio());
    DG->setConstitutiveExtraDofDiffusionUseEnergyConjugatedField(this->getConstitutiveExtraDofDiffusionUseEnergyConjugatedField());
    DG->setConstitutiveExtraDofDiffusionAccountFieldSource(this->getConstitutiveExtraDofDiffusionAccountFieldSource());
    DG->setConstitutiveExtraDofDiffusionAccountMecaSource(this->getConstitutiveExtraDofDiffusionAccountMecaSource());
    DG->setConstitutiveExtraDofDiffusionStabilityParameter(this->getConstitutiveExtraDofDiffusionStabilityParameter());
    DG->setConstitutiveExtraDofDiffusionContinuity(this->getConstitutiveExtraDofDiffusionContinuity());
    DG->setConstitutiveCurlAccountFieldSource(this->getConstitutiveCurlAccountFieldSource());
    DG->setConstitutiveCurlAccountMecaSource(this->getConstitutiveCurlAccountMecaSource());

    return DG;
  }
};

#endif // DG3DTERMS_H_
