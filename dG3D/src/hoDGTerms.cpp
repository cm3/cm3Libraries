//
// C++ Interface: terms
//
// Description: Class with definition of terms for finite strain problem
//
//
// Author: V.D. NGUYEN 2011
//
// Copyright: See COPYING file that comes with this distribution
//
#include "hoDGTerms.h"
#include "STensor33.h"
#include "STensor53.h"
#include "STensor63.h"

void hoDGStiffnessBulk::get(MElement *ele, int npts, IntPt *GP, fullMatrix<double> &mStiff) const{
  dG3DStiffnessBulk::get(ele,npts,GP,mStiff);
  if (this->sym){
    if (_mlaw->isHighOrder()){
      int nbFF = ele->getNumShapeFunctions();
      const AllIPState::ipstateElementContainer *vips = _ipf->getAips()->getIPstate(ele->getNum());

      for (int i = 0; i < npts; i++){
        const IPStateBase *ips        = (*vips)[i];
        const dG3DIPVariableBase *ipv     = static_cast<const dG3DIPVariableBase*>(ips->getState(IPStateBase::current));
        
        const hoDGIPVariable* hoipv = static_cast<const hoDGIPVariable*>(ipv);
        std::vector<TensorialTraits<double>::GradType>& Grads = hoipv->gradf(&space1,ele,GP[i]);
        std::vector<TensorialTraits<double>::HessType>& Hesses =hoipv->hessf(&space1,ele,GP[i]);
        double& detJ = hoipv->getJacobianDeterminant(ele,GP[i]);
        const double weight = GP[i].weight; 

        const STensor63& secondtangent = hoipv->getConstRefToSecondOrderTangent();
        const STensor53& firstsecond = hoipv->getConstRefToCrossTangentFirstSecond();
        const STensor53& secondfirst = hoipv->getConstRefToCrossTangentSecondFirst();

        const double ratio = detJ*weight;

        for(int j=0;j<nbFF; j++){
          for (int p=0; p<nbFF; p++){
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++)
                for (int m=0; m<3; m++)
                  for (int q=0; q<3; q++)
                    for (int r=0; r<3; r++){
                      mStiff(j+k*nbFF,p+l*nbFF) += firstsecond(k,m,l,q,r)*Grads[j+0*nbFF][m]*Hesses[p+0*nbFF](q,r)*ratio+
                                                   secondfirst(k,m,q,l,r)*Hesses[j+0*nbFF](m,q)*Grads[p+0*nbFF](r)*ratio;
                      for (int s=0; s<3; s++){
                        mStiff(j+k*nbFF,p+l*nbFF) += secondtangent(k,m,q,l,r,s)*Hesses[j+0*nbFF](m,q)*Hesses[p+0*nbFF](r,s)*ratio;
                      }
                    }
          };
        };
      };
    }
  }
  else {
    Msg::Info("Non-symetrical stiffness term is not implemented yet");
  };
};

void hoDGForceBulk::get(MElement *ele,int npts,IntPt *GP,fullVector<double> &vFor) const{
  dG3DForceBulk::get(ele,npts,GP,vFor);
  if (_mlaw->isHighOrder()){
    int nbDof = space1.getNumKeys(ele);
    int nbFF = ele->getNumShapeFunctions();

    const AllIPState::ipstateElementContainer *vips = _ipf->getAips()->getIPstate(ele->getNum());


    for (int i = 0; i < npts; i++){
      const IPStateBase *ips        = (*vips)[i];
      const dG3DIPVariableBase *ipv     = static_cast<const dG3DIPVariableBase*>(ips->getState(IPStateBase::current));
    
      const double weight = GP[i].weight; //const double detJ = ele->getJacobian(u, v, w, jac);

      const hoDGIPVariable* hoipv = static_cast<const hoDGIPVariable*>(ipv);
      std::vector<TensorialTraits<double>::HessType>& Hesses = hoipv->hessf(&space1,ele,GP[i]);
      double detJ = hoipv->getJacobianDeterminant(ele,GP[i]);

      const STensor33& QK = hoipv->getConstRefToSecondOrderStress();
      const double ratio = detJ*weight;

      for(int j=0;j<nbFF; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            for (int m=0; m<3; m++){
              vFor(j+ m*nbFF)+=QK(m,k,l)*Hesses[j+0*nbFF](k,l)*ratio;
            }
          };
        };
      };
    };
  };
};

void hoDGStiffnessInter::get(MElement *ele,int npts,IntPt *GP, fullMatrix<double> &stiff)const{
  dG3DStiffnessInter::get(ele,npts,GP,stiff);
  if (_mlawMinus->isHighOrder() or _mlawPlus->isHighOrder()){

    //printf("assebling the interface stiffness term \n");
    MInterfaceElement *ie = dynamic_cast<MInterfaceElement*>(ele);

    double hs = ie->getCharacteristicSize();

    MElement* eleminus = ie->getElem(0);
    MElement* eleplus = ie->getElem(1);

    int mDofSize = space1.getNumKeys(eleminus);
    int pDofSize = space2.getNumKeys(eleplus);

    int nbFFm = eleminus->getNumShapeFunctions();
    int nbFFp = eleplus->getNumShapeFunctions();


    _ipf->getIPv(ele,vipv,vipvprev);
    // Get values of Gauss points on minus and plus elements
    IntPt *GPm; IntPt *GPp;
    _interQuad->getIntPoints(ie,GP,&GPm,&GPp);


    for (int i = 0; i < npts; i++){
      const double u = GP[i].pt[0]; const double v = GP[i].pt[1]; const double w = GP[i].pt[2];
      const double weight = GP[i].weight;
      const double& detJ = vipv[i]->getJacobianDeterminant(ie,GP[i]);
    
      static SVector3 normal;
      normal = vipv[i]->getConstRefToReferenceOutwardNormal();
      normal*= (1./normal.norm());

      const double ratio = detJ*weight;

      std::vector<TensorialTraits<double>::GradType>& GradMinus = vipv[i]->gradf(&space1,eleminus,GPm[i]);
      std::vector<TensorialTraits<double>::GradType>&  GradPlus = vipv[i+npts]->gradf(&space2,eleplus,GPp[i]);

      std::vector<TensorialTraits<double>::HessType>& HessMinus = vipv[i]->hessf(&space1,eleminus,GPm[i]);
      std::vector<TensorialTraits<double>::HessType>& HessPlus = vipv[i+npts]->hessf(&space2,eleplus,GPp[i]);


      std::vector<TensorialTraits<double>::ValType>& ValMinus = vipv[i]->f(&space1,eleminus,GPm[i]);
      std::vector<TensorialTraits<double>::ValType>& ValPlus = vipv[i+npts]->f(&space2,eleplus,GPp[i]);

      std::vector<TensorialTraits<double>::ThirdDevType>& ThirdMius = vipv[i]->thirdDevf(&space1,eleminus,GPm[i]);
      std::vector<TensorialTraits<double>::ThirdDevType>& ThirdPlus = vipv[i+npts]->thirdDevf(&space2,eleplus,GPp[i]);


      // get all elastic tangent
      const STensor63& elJminus = vipv[i]->getConstRefToSecondOrderDGElasticTangentModuli();
      const STensor63& elJplus = vipv[i+npts]->getConstRefToSecondOrderDGElasticTangentModuli();
      const STensor53& elJFminus = vipv[i]->getConstRefToCrossSecondFirstDGElasticTangentModuli();
      const STensor53& elJFplus = vipv[i+npts]->getConstRefToCrossSecondFirstDGElasticTangentModuli();
      const STensor53& elLGminus = vipv[i]->getConstRefToCrossFirstSecondDGElasticTangentModuli();
      const STensor53& elLGplus = vipv[i+npts]->getConstRefToCrossFirstSecondDGElasticTangentModuli();

      // get all current tangent
      const STensor63& Jminus = vipv[i]->getConstRefToSecondOrderTangent();
      const STensor63& Jplus = vipv[i+npts]->getConstRefToSecondOrderTangent();
      const STensor53& LGminus = vipv[i]->getConstRefToCrossTangentFirstSecond();
      const STensor53& JFminus = vipv[i]->getConstRefToCrossTangentSecondFirst();
      const STensor53& LGplus = vipv[i+npts]->getConstRefToCrossTangentFirstSecond();
      const STensor53& JFplus = vipv[i+npts]->getConstRefToCrossTangentSecondFirst();

      static STensor63 elJmeanbetahs; 
      elJmeanbetahs = (elJminus);
      elJmeanbetahs += elJplus;
      elJmeanbetahs *= (0.5*getStabilityParameter()/hs);

      for (int k=0; k<3; k++){
        for (int q=0; q<3; q++){
          for (int l=0; l<3; l++){
            for (int m=0; m<3; m++){
              for (int r=0; r<3; r++){
                for (int j=0; j<nbFFm; j++){
                  for (int p=0; p<nbFFm; p++){
                    if (_fullDg){
                      stiff(j+k*nbFFm, p+q*nbFFm) -= 0.5*ValMinus[j+0*nbFFm]*normal[l]*(LGminus(k,l,q,m,r) - JFminus(k,l,r,q,m))*HessMinus[p+0*nbFFm](m,r)*ratio;
                      stiff(j+k*nbFFm, p+q*nbFFm) -= 0.5*ValMinus[p+0*nbFFm]*normal[l]*(elLGminus(q,l,k,m,r) - elJFminus(q,l,r,k,m))*HessMinus[j+0*nbFFm](m,r)*ratio;
                    }
                    stiff(j+k*nbFFm, p+q*nbFFm) -= 0.5*GradMinus[j+0*nbFFm](m)*normal[l]*JFminus(k,m,l,q,r)*GradMinus[p+0*nbFFm](r)*ratio;
                    stiff(j+k*nbFFm, p+q*nbFFm) -= 0.5*GradMinus[p+0*nbFFm](m)*normal[l]*elJFminus(q,m,l,k,r)*GradMinus[j+0*nbFFm](r)*ratio;
                    for (int s=0; s<3; s++){
                      if (_fullDg){
                        stiff(j+k*nbFFm, p+q*nbFFm) += 0.5*ValMinus[j+0*nbFFm]*normal[l]*Jminus(k,l,r,q,m,s)*ThirdMius[p+0*nbFFm](m,s,r)*ratio;
                        stiff(j+k*nbFFm, p+q*nbFFm) += 0.5*ValMinus[p+0*nbFFm]*normal[l]*elJminus(q,l,r,k,m,s)*ThirdMius[j+0*nbFFm](m,s,r)*ratio;
                      }
                      stiff(j+k*nbFFm, p+q*nbFFm) -= 0.5*GradMinus[j+0*nbFFm](m)*normal[l]*Jminus(k,m,l,q,r,s)*HessMinus[p+0*nbFFm](r,s)*ratio;
                      stiff(j+k*nbFFm, p+q*nbFFm) -= 0.5*GradMinus[p+0*nbFFm](m)*normal[l]*elJminus(q,m,l,k,r,s)*HessMinus[j+0*nbFFm](r,s)*ratio;
                      stiff(j+k*nbFFm, p+q*nbFFm) += GradMinus[j+0*nbFFm](m)*normal[l]*elJmeanbetahs(k,m,l,q,r,s)*GradMinus[p+0*nbFFm](r)*normal[s]*ratio;
                    }
                  }
                  for (int p=0; p<nbFFp; p++){
                    if (_fullDg){
                      stiff(j+k*nbFFm, p+q*nbFFp+mDofSize) -= 0.5*ValMinus[j+0*nbFFm]*normal[l]*(LGplus(k,l,q,m,r) - JFplus(k,l,r,q,m))*HessPlus[p+0*nbFFp](m,r)*ratio;
                      stiff(j+k*nbFFm, p+q*nbFFp+mDofSize) += 0.5*ValPlus[p+0*nbFFp]*normal[l]*(elLGminus(q,l,k,m,r) - elJFminus(q,l,r,k,m))*HessMinus[j+0*nbFFm](m,r)*ratio;
                    }
                    stiff(j+k*nbFFm, p+q*nbFFp+mDofSize) -= 0.5*GradMinus[j+0*nbFFm](m)*normal[l]*JFplus(k,m,l,q,r)*GradPlus[p+0*nbFFp](r)*ratio;
                    stiff(j+k*nbFFm, p+q*nbFFp+mDofSize) += 0.5*GradPlus[p+0*nbFFp](m)*normal[l]*elJFminus(q,m,l,k,r)*GradMinus[j+0*nbFFm](r)*ratio;
                    for (int s=0; s<3; s++){
                      if (_fullDg){
                        stiff(j+k*nbFFm, p+q*nbFFp+mDofSize) += 0.5*ValMinus[j+0*nbFFm]*normal[l]*Jplus(k,l,r,q,m,s)*ThirdPlus[p+0*nbFFp](m,s,r)*ratio;
                        stiff(j+k*nbFFm, p+q*nbFFp+mDofSize) -= 0.5*ValPlus[p+0*nbFFp]*normal[l]*elJminus(q,l,r,k,m,s)*ThirdMius[j+0*nbFFm](m,s,r)*ratio;
                      }
                      stiff(j+k*nbFFm, p+q*nbFFp+mDofSize) -= 0.5*GradMinus[j+0*nbFFm](m)*normal[l]*Jplus(k,m,l,q,r,s)*HessPlus[p+0*nbFFp](r,s)*ratio;
                      stiff(j+k*nbFFm, p+q*nbFFp+mDofSize) += 0.5*GradPlus[p+0*nbFFp](m)*normal[l]*elJminus(q,m,l,k,r,s)*HessMinus[j+0*nbFFm](r,s)*ratio;
                      stiff(j+k*nbFFm, p+q*nbFFp+mDofSize) -= GradMinus[j+0*nbFFm](m)*normal[l]*elJmeanbetahs(k,m,l,q,r,s)*GradPlus[p+0*nbFFp](r)*normal[s]*ratio;
                    }
                  }
                }
                for (int j=0; j<nbFFp; j++){
                  for (int p=0; p<nbFFm; p++){
                    if (_fullDg){
                      stiff(j+k*nbFFp+mDofSize, p+q*nbFFm) += 0.5*ValPlus[j+0*nbFFp]*normal[l]*(LGminus(k,l,q,m,r) - JFminus(k,l,r,q,m))*HessMinus[p+0*nbFFm](m,r)*ratio;
                      stiff(j+k*nbFFp+mDofSize, p+q*nbFFm) -= 0.5*ValMinus[p+0*nbFFm]*normal[l]*(elLGplus(q,l,k,m,r) - elJFplus(q,l,r,k,m))*HessPlus[j+0*nbFFp](m,r)*ratio;
                    }
                    stiff(j+k*nbFFp+mDofSize, p+q*nbFFm) += 0.5*GradPlus[j+0*nbFFp](m)*normal[l]*JFminus(k,m,l,q,r)*GradMinus[p+0*nbFFm](r)*ratio;
                    stiff(j+k*nbFFp+mDofSize, p+q*nbFFm) -= 0.5*GradMinus[p+0*nbFFm](m)*normal[l]*elJFplus(q,m,l,k,r)*GradPlus[j+0*nbFFp](r)*ratio;
                    for (int s=0; s<3; s++){
                      if (_fullDg){
                        stiff(j+k*nbFFp+mDofSize, p+q*nbFFm) -= 0.5*ValPlus[j+0*nbFFp]*normal[l]*Jminus(k,l,r,q,m,s)*ThirdMius[p+0*nbFFm](m,s,r)*ratio;
                        stiff(j+k*nbFFp+mDofSize, p+q*nbFFm) += 0.5*ValMinus[p+0*nbFFm]*normal[l]*elJplus(q,l,r,k,m,s)*ThirdPlus[j+0*nbFFp](m,s,r)*ratio;
                      }
                      stiff(j+k*nbFFp+mDofSize, p+q*nbFFm) += 0.5*GradPlus[j+0*nbFFp](m)*normal[l]*Jminus(k,m,l,q,r,s)*HessMinus[p+0*nbFFm](r,s)*ratio;
                      stiff(j+k*nbFFp+mDofSize, p+q*nbFFm) -= 0.5*GradMinus[p+0*nbFFm](m)*normal[l]*elJplus(q,m,l,k,r,s)*HessPlus[j+0*nbFFp](r,s)*ratio;
                      stiff(j+k*nbFFp+mDofSize, p+q*nbFFm) -= GradPlus[j+0*nbFFp](m)*normal[l]*elJmeanbetahs(k,m,l,q,r,s)*GradMinus[p+0*nbFFm](r)*normal[s]*ratio;
                    }
                  }
                  for (int p=0; p<nbFFp; p++){
                    if (_fullDg){
                      stiff(j+k*nbFFp+mDofSize, p+q*nbFFp+mDofSize) += 0.5*ValPlus[j+0*nbFFp]*normal[l]*(LGplus(k,l,q,m,r) - JFplus(k,l,r,q,m))*HessPlus[p+0*nbFFp](m,r)*ratio;
                      stiff(j+k*nbFFp+mDofSize, p+q*nbFFp+mDofSize) += 0.5*ValPlus[p+0*nbFFp]*normal[l]*(elLGplus(q,l,k,m,r) - elJFplus(q,l,r,k,m))*HessPlus[j+0*nbFFp](m,r)*ratio;
                    }
                    stiff(j+k*nbFFp+mDofSize, p+q*nbFFp+mDofSize) += 0.5*GradPlus[j+0*nbFFp](m)*normal[l]*JFplus(k,m,l,q,r)*GradPlus[p+0*nbFFp](r)*ratio;
                    stiff(j+k*nbFFp+mDofSize, p+q*nbFFp+mDofSize) += 0.5*GradPlus[p+0*nbFFp](m)*normal[l]*elJFplus(q,m,l,k,r)*GradPlus[j+0*nbFFp](r)*ratio;
                    for (int s=0; s<3; s++){
                      if (_fullDg){
                        stiff(j+k*nbFFp+mDofSize, p+q*nbFFp+mDofSize) -= 0.5*ValPlus[j+0*nbFFp]*normal[l]*Jplus(k,l,r,q,m,s)*ThirdPlus[p+0*nbFFp](m,s,r)*ratio;
                        stiff(j+k*nbFFp+mDofSize, p+q*nbFFp+mDofSize) -= 0.5*ValPlus[p+0*nbFFp]*normal[l]*elJplus(q,l,r,k,m,s)*ThirdPlus[j+0*nbFFp](m,s,r)*ratio;
                      }
                      stiff(j+k*nbFFp+mDofSize, p+q*nbFFp+mDofSize) += 0.5*GradPlus[j+0*nbFFp](m)*normal[l]*Jplus(k,m,l,q,r,s)*HessPlus[p+0*nbFFp](r,s)*ratio;
                      stiff(j+k*nbFFp+mDofSize, p+q*nbFFp+mDofSize) += 0.5*GradPlus[p+0*nbFFp](m)*normal[l]*elJplus(q,m,l,k,r,s)*HessPlus[j+0*nbFFp](r,s)*ratio;
                      stiff(j+k*nbFFp+mDofSize, p+q*nbFFp+mDofSize) += GradPlus[j+0*nbFFp](m)*normal[l]*elJmeanbetahs(k,m,l,q,r,s)*GradPlus[p+0*nbFFp](r)*normal[s]*ratio;
                    }
                  }
                }
              }
            }
          }
        }
      }
    };
  }
};

void hoDGForceInter::get(MElement *ele,int npts,IntPt *GP,fullVector<double> &vFor) const{
  dG3DForceInter::get(ele,npts,GP,vFor);
  if (_mlawMinus->isHighOrder() and _mlawPlus->isHighOrder()){
    MInterfaceElement *ie = dynamic_cast<MInterfaceElement*>(ele);
    double hs = ie->getCharacteristicSize();
    double betahs = getStabilityParameter()/hs;

    MElement* eleminus = ie->getElem(0);
    MElement* eleplus = ie->getElem(1);
    // number keys for each element
    int mDofSize = _minusSpace->getNumKeys(eleminus);
    int pDofSize = _plusSpace->getNumKeys(eleplus);
    // number of shape functions
    int nbFFm = eleminus->getNumShapeFunctions();
    int nbFFp = eleplus->getNumShapeFunctions();
    //

    _ipf->getIPv(ele,vipv,vipvprev);
    // Get values of Gauss points on minus and plus elements
    IntPt *GPm; IntPt *GPp;
    _interQuad->getIntPoints(ie,GP,&GPm,&GPp);


    for (int i = 0; i < npts; i++){
      const double u = GP[i].pt[0]; const double v = GP[i].pt[1]; const double w = GP[i].pt[2];
      const double weight = GP[i].weight;
      const double& detJ = vipv[i]->getJacobianDeterminant(ie,GP[i]);
      
      SVector3 normal = (vipv[i]->getConstRefToReferenceOutwardNormal());
      normal*= (1./normal.norm()); // normalize


      std::vector<TensorialTraits<double>::GradType>& GradMinus =vipv[i]->gradf(_minusSpace,eleminus,GPm[i]);
      std::vector<TensorialTraits<double>::GradType>& GradPlus = vipv[i+npts]->gradf(_plusSpace,eleplus,GPp[i]);

      std::vector<TensorialTraits<double>::HessType> &HessMinus = vipv[i]->hessf(_minusSpace,eleminus,GPm[i]);
      std::vector<TensorialTraits<double>::HessType> &HessPlus = vipv[i+npts]->hessf(_plusSpace,eleplus,GPp[i]);


      const double ratio = detJ*weight;

      // Elastic properties
      const STensor63& Jelminus = vipv[i]->getConstRefToSecondOrderDGElasticTangentModuli();
      const STensor63& Jelplus= vipv[i+npts]->getConstRefToSecondOrderDGElasticTangentModuli();
      const STensor53& JFelminus = vipv[i]->getConstRefToCrossSecondFirstDGElasticTangentModuli();
      const STensor53& JFelplus = vipv[i+npts]->getConstRefToCrossSecondFirstDGElasticTangentModuli();
      const STensor53& LGelminus = vipv[i]->getConstRefToCrossFirstSecondDGElasticTangentModuli();
      const STensor53& LGelplus = vipv[i+npts]->getConstRefToCrossFirstSecondDGElasticTangentModuli();

      if (_fullDg){
 
        std::vector<TensorialTraits<double>::ValType> &ValMinus =vipv[i]->f(_minusSpace,eleminus,GPm[i]);
        std::vector<TensorialTraits<double>::ValType> &ValPlus = vipv[i+npts]->f(_plusSpace,eleplus,GPp[i]);

        std::vector<TensorialTraits<double>::ThirdDevType> &ThirdMius = vipv[i]->thirdDevf(_minusSpace,eleminus,GPm[i]);
        std::vector<TensorialTraits<double>::ThirdDevType> &ThirdPlus = vipv[i+npts]->thirdDevf(_plusSpace,eleplus,GPp[i]);
        // tangent
        const STensor63& Jminus = vipv[i]->getConstRefToSecondOrderTangent();
        const STensor63& Jplus = vipv[i+npts]->getConstRefToSecondOrderTangent();

        const STensor53& JFminus = vipv[i]->getConstRefToCrossTangentSecondFirst();
        const STensor53& JFplus = vipv[i+npts]->getConstRefToCrossTangentSecondFirst();

        const STensor33& Gminus = vipv[i]->getConstRefToGradientOfDeformationGradient();
        const STensor33& Gplus = vipv[i+npts]->getConstRefToGradientOfDeformationGradient();

        const STensor43& Kminus = vipv[i]->getConstRefToThirdOrderDeformationGradient();
        const STensor43& Kplus = vipv[i+npts]->getConstRefToThirdOrderDeformationGradient();

        // compute the mean value of divergence of the second-stress
        static STensor3 meanDivSecondStress;
        for (int j=0; j<3; j++)
          for (int k=0; k<3; k++){
            meanDivSecondStress(j,k) = 0.;
            for (int l=0; l<3; l++)
              for (int p=0; p<3; p++)
                for (int q=0; q<3; q++){
                  meanDivSecondStress(j,k) += 0.5*(JFminus(j,k,l,p,q)*Gminus(p,q,l)+
                                                   JFplus(j,k,l,p,q)*Gplus(p,q,l));
                  for (int r=0; r<3; r++){
                    meanDivSecondStress(j,k) += 0.5*(Jminus(j,k,l,p,q,r)*Kminus(p,q,r,l)+
                                                     Jplus(j,k,l,p,q,r)*Kplus(p,q,r,l));
                  }
                }
                
          }

        const SVector3& jump = vipv[i]->getConstRefToJump();
        for (int j=0; j<nbFFm; j++){
          for (int k=0; k<3; k++){
            vFor(j+0*nbFFm) += meanDivSecondStress(0,k)*normal[k]*ValMinus[j+0*nbFFm]*ratio;
            vFor(j+1*nbFFm) += meanDivSecondStress(1,k)*normal[k]*ValMinus[j+0*nbFFm]*ratio;
            vFor(j+2*nbFFm) += meanDivSecondStress(2,k)*normal[k]*ValMinus[j+0*nbFFm]*ratio;
          }


          for (int m=0; m<3; m++){
            for (int k=0; k<3; k++){
              for (int l=0; l<3; l++){
                for (int q=0; q<3; q++){
                  vFor(j+0*nbFFm) += 0.5*jump[m]*normal[k]*(LGelminus(m,k,0,l,q)- JFelminus(m,k,q,0,l))*HessMinus[j+0*nbFFm](l,q)*ratio;
                  vFor(j+1*nbFFm) += 0.5*jump[m]*normal[k]*(LGelminus(m,k,1,l,q)- JFelminus(m,k,q,1,l))*HessMinus[j+0*nbFFm](l,q)*ratio;
                  vFor(j+2*nbFFm) += 0.5*jump[m]*normal[k]*(LGelminus(m,k,2,l,q)- JFelminus(m,k,q,2,l))*HessMinus[j+0*nbFFm](l,q)*ratio;

                  for (int r=0; r<3; r++){
                    vFor(j+0*nbFFm) += -0.5* jump[m]*normal[k]*Jelminus(m,k,l,0,q,r)*ThirdMius[j+0*nbFFm](q,r,l)*ratio;
                    vFor(j+1*nbFFm) += -0.5* jump[m]*normal[k]*Jelminus(m,k,l,1,q,r)*ThirdMius[j+0*nbFFm](q,r,l)*ratio;
                    vFor(j+2*nbFFm) += -0.5* jump[m]*normal[k]*Jelminus(m,k,l,2,q,r)*ThirdMius[j+0*nbFFm](q,r,l)*ratio;
                  }
                }
              }
            }
          }
        }

        for (int j=0; j<nbFFp; j++){
          for (int k=0; k<3; k++){
            vFor(j+0*nbFFp+mDofSize) += -1.*meanDivSecondStress(0,k)*normal[k]*ValPlus[j+0*nbFFp]*ratio;
            vFor(j+1*nbFFp+mDofSize) += -1.*meanDivSecondStress(1,k)*normal[k]*ValPlus[j+0*nbFFp]*ratio;
            vFor(j+2*nbFFp+mDofSize) += -1.*meanDivSecondStress(2,k)*normal[k]*ValPlus[j+0*nbFFp]*ratio;
          }


          for (int m=0; m<3; m++){
            for (int k=0; k<3; k++){
              for (int l=0; l<3; l++){
                for (int q=0; q<3; q++){
                  vFor(j+0*nbFFp+mDofSize) += 0.5*jump[m]*normal[k]*(LGelplus(m,k,0,l,q)- JFelplus(m,k,q,0,l))*HessPlus[j+0*nbFFp](l,q)*ratio;
                  vFor(j+1*nbFFp+mDofSize) += 0.5*jump[m]*normal[k]*(LGelplus(m,k,1,l,q)- JFelplus(m,k,q,1,l))*HessPlus[j+0*nbFFp](l,q)*ratio;
                  vFor(j+2*nbFFp+mDofSize) += 0.5*jump[m]*normal[k]*(LGelplus(m,k,2,l,q)- JFelplus(m,k,q,2,l))*HessPlus[j+0*nbFFp](l,q)*ratio;

                  for (int r=0; r<3; r++){
                    vFor(j+0*nbFFp+mDofSize) += -0.5* jump[m]*normal[k]*Jelplus(m,k,l,0,q,r)*ThirdPlus[j+0*nbFFp](q,r,l)*ratio;
                    vFor(j+1*nbFFp+mDofSize) += -0.5* jump[m]*normal[k]*Jelplus(m,k,l,1,q,r)*ThirdPlus[j+0*nbFFp](q,r,l)*ratio;
                    vFor(j+2*nbFFp+mDofSize) += -0.5* jump[m]*normal[k]*Jelplus(m,k,l,2,q,r)*ThirdPlus[j+0*nbFFp](q,r,l)*ratio;
                  }
                }
              }
            }
          }
        }
        
      }

      //  deformation gradient
      const STensor3& Fminus = vipv[i]->getConstRefToDeformationGradient();
      const STensor3& Fplus =  vipv[i+npts]->getConstRefToDeformationGradient();
      // high stress
      const STensor33& Qminus = vipv[i]->getConstRefToSecondOrderStress();
      const STensor33& Qplus = vipv[i+npts]->getConstRefToSecondOrderStress();

      static STensor33 Qmean; // mean of Q
      static STensor33 FjumpN; // jump of F

      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            Qmean(j,k,l) = 0.5*(Qminus(j,k,l)+Qplus(j,k,l));
            FjumpN(j,k,l) = (Fplus(j,k) - Fminus(j,k))*normal[l];
          }
        }
      };

      // for vForce

      // consistency term [d_F_ij]*N_k *<Q_ijk>
      for (int j=0; j<nbFFm; j++){
        for (int k=0; k<3; k++){
          for (int m=0; m<3; m++){
            vFor(j+0*nbFFm) += -1.*Qmean(0,k,m)*normal[m]*GradMinus[j+0*nbFFm](k)*ratio;
            vFor(j+1*nbFFm) += -1.*Qmean(1,k,m)*normal[m]*GradMinus[j+0*nbFFm](k)*ratio;
            vFor(j+2*nbFFm) += -1.*Qmean(2,k,m)*normal[m]*GradMinus[j+0*nbFFm](k)*ratio;
          }
        }
      }

      for (int j=0; j<nbFFp; j++){
        for (int k=0; k<3; k++){
          for (int m=0; m<3; m++){
            vFor(j+0*nbFFp+mDofSize) += Qmean(0,k,m)*normal[m]*GradPlus[j+0*nbFFp](k)*ratio;
            vFor(j+1*nbFFp+mDofSize) += Qmean(1,k,m)*normal[m]*GradPlus[j+0*nbFFp](k)*ratio;
            vFor(j+2*nbFFp+mDofSize) += Qmean(2,k,m)*normal[m]*GradPlus[j+0*nbFFp](k)*ratio;
          }
        }
      }
      // compatibility term [F_ij]*N_k *<d_Q_ijk>

      static STensor33 FjumNJelminus;
      static STensor33 FjumNJelplus;
      static STensor33 FjumNMeanJFel;
      static STensor3 FjumNJFelminus;
      static STensor3 FjumNJFelplus;
      
      STensorOperation::zero(FjumNJelminus);
      STensorOperation::zero(FjumNJelplus);
      
      STensorOperation::zero(FjumNMeanJFel);
      STensorOperation::zero(FjumNJFelminus);
      STensorOperation::zero(FjumNJFelplus);

      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            for (int p=0; p<3; p++){
              for (int q=0; q<3; q++){
                FjumNJFelminus(p,q) += FjumpN(j,k,l)*JFelminus(j,k,l,p,q);
                FjumNJFelplus(p,q) += FjumpN(j,k,l)*JFelplus(j,k,l,p,q);
                for (int r=0; r<3; r++){
                  double valm = FjumpN(j,k,l)*Jelminus(j,k,l,p,q,r);
                  double valp = FjumpN(j,k,l)*Jelplus(j,k,l,p,q,r);
                  FjumNJelminus(p,q,r) += valm;
                  FjumNJelplus(p,q,r) += valp;
                  FjumNMeanJFel(p,q,r) += betahs*0.5*(valm+valp);
                }
              }
            }
          }
        }
      }


      for (int j=0; j<nbFFm; j++){
        for (int m=0; m<3; m++){
          vFor(j+0*nbFFm) += 0.5*(FjumNJFelminus(0,m)*GradMinus[j+0*nbFFm](m))*ratio;
          vFor(j+1*nbFFm) += 0.5*(FjumNJFelminus(1,m)*GradMinus[j+0*nbFFm](m))*ratio;
          vFor(j+2*nbFFm) += 0.5*(FjumNJFelminus(2,m)*GradMinus[j+0*nbFFm](m))*ratio;
        }

        for (int m=0; m<3; m++){
          for (int k=0; k<3; k++){
            vFor(j+0*nbFFm) += 0.5*FjumNJelminus(0,m,k)*HessMinus[j+0*nbFFm](m,k)*ratio;
            vFor(j+1*nbFFm) += 0.5*FjumNJelminus(1,m,k)*HessMinus[j+0*nbFFm](m,k)*ratio;
            vFor(j+2*nbFFm) += 0.5*FjumNJelminus(2,m,k)*HessMinus[j+0*nbFFm](m,k)*ratio;
          }
        }
      };


      for (int j=0; j<nbFFp; j++){
        for (int m=0; m<3; m++){
          vFor(j+0*nbFFp+mDofSize) += 0.5*(FjumNJFelplus(0,m)*GradPlus[j+0*nbFFp](m))*ratio;
          vFor(j+1*nbFFp+mDofSize) += 0.5*(FjumNJFelplus(1,m)*GradPlus[j+0*nbFFp](m))*ratio;
          vFor(j+2*nbFFp+mDofSize) += 0.5*(FjumNJFelplus(2,m)*GradPlus[j+0*nbFFp](m))*ratio;
        }

        for (int m=0; m<3; m++){
          for (int k=0; k<3; k++){
            vFor(j+0*nbFFp+mDofSize) += 0.5*FjumNJelplus(0,m,k)*HessPlus[j+0*nbFFp](m,k)*ratio;
            vFor(j+1*nbFFp+mDofSize) += 0.5*FjumNJelplus(1,m,k)*HessPlus[j+0*nbFFp](m,k)*ratio;
            vFor(j+2*nbFFp+mDofSize) += 0.5*FjumNJelplus(2,m,k)*HessPlus[j+0*nbFFp](m,k)*ratio;
          }
        }
      };
      // stability term [F_ij]*N_k <beta/hs*J_ijkpqr>[d_F_pq]*N_r
      static STensor3 FjumNMeanJFelN;
      STensorOperation::zero(FjumNMeanJFelN);
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            for (int p=0; p<3; p++){
              for (int q=0; q<3; q++){
                for (int r=0; r<3; r++){
                  FjumNMeanJFelN(p,q)+= FjumpN(j,k,l)*betahs*0.5*(Jelminus(j,k,l,p,q,r)+Jelplus(j,k,l,p,q,r))*normal[r];
                }
              }
            }
          }
        }
      }
      for (int j=0;j<nbFFm; j++){
        for (int m=0; m<3; m++){
          vFor(j+ 0*nbFFm) += -1.*FjumNMeanJFelN(0,m)*GradMinus[j+0*nbFFm](m)*ratio;
          vFor(j+ 1*nbFFm) += -1.*FjumNMeanJFelN(1,m)*GradMinus[j+0*nbFFm](m)*ratio;
          vFor(j+ 2*nbFFm) += -1.*FjumNMeanJFelN(2,m)*GradMinus[j+0*nbFFm](m)*ratio;
        }
      }


      for (int j=0;j<nbFFp; j++){
        for (int m=0; m<3; m++){
          vFor(j+ 0*nbFFp+mDofSize) += FjumNMeanJFelN(0,m)*GradPlus[j+0*nbFFp](m)*ratio;
          vFor(j+ 1*nbFFp+mDofSize) += FjumNMeanJFelN(1,m)*GradPlus[j+0*nbFFp](m)*ratio;
          vFor(j+ 2*nbFFp+mDofSize) += FjumNMeanJFelN(2,m)*GradPlus[j+0*nbFFp](m)*ratio;
        }
      }
    };
  }
};
