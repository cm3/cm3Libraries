//
// C++ Interface: homogenized tangent terms
//
//
//
// Author:  <V.-D. NGUYEN>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "dG3DHomogenizedTangentTerms.h"
#include "highOrderTensor.h"
#include "nonLinearMechSolver.h"
#include "nonLocalDamageDG3DIPVariable.h"

void dG3DHomogenizedTangent::get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const{
  const FunctionSpaceBase* sp = _dom->getFunctionSpace();
  const dG3DMaterialLaw *mlaw = static_cast<const dG3DMaterialLaw*>(_dom->getMaterialLaw());
  bool useBarF=mlaw->getUseBarF();
	
  int nbDof = sp->getNumKeys(ele);
  int stressDim = _dom->getStressDimension();
  int _dim=_dom->getDim();
  m.resize(stressDim,nbDof);
  m.setAll(0.);
  const STensor3& _rveGeoInertia=_dom->getRVEGeometricalInertia(); 
  const double _rveVolume = _dom->getRVEVolume();
	
  const AllIPState::ipstateElementContainer *vips = _ipf->getAips()->getIPstate(ele->getNum());
  
  ////////////  use BarF
  static std::vector<SVector3> dBarJdu;
  static std::vector< std::vector< STensor33 > > dBarFdu; //for each GP and for each node
  if(useBarF){
     int nbFF = (dynamic_cast< const nlsFunctionSpace<double>*>(sp))->getNumShapeFunctions(ele,0);
     dBarJdu.resize(nbFF);
     dBarFdu.resize(npts);
     for (int k=0;k<nbFF;k++){
        STensorOperation::zero(dBarJdu[k]); //check here if ok
     }
     for (int ipg = 0; ipg < npts; ipg++){
        dBarFdu[ipg].resize(nbFF);
        for (int k=0;k<nbFF;k++){
          STensorOperation::zero(dBarFdu[ipg][k]); //check here if ok
        }
     }
     double totalWeight=0.;
     for(int ipg = 0; ipg < npts; ipg++)
        totalWeight+=GP[ipg].weight;
     for(int ipg = 0; ipg < npts; ipg++){
        const IPStateBase *ips = (*vips)[ipg];
        const dG3DIPVariableBase *ipv  = static_cast<const dG3DIPVariableBase*>(ips->getState(IPStateBase::current));
        const std::vector<TensorialTraits<double>::GradType>& Gradsipg = ipv->gradf(sp,ele,GP[ipg]);
        const STensor3 &barF = ipv->getConstRefToDeformationGradient(); //this is actually barF
        STensor3 invBarF(barF.invert());
        double localJ=ipv->getLocalJ();
        double barJ=ipv->getBarJ();
        double weight=GP[ipg].weight*pow(localJ,1.-1./((double)(_dim)))*pow(barJ,1./((double)(_dim)))/totalWeight;
        for(int k=0;k<nbFF;k++)
        {
          SVector3 dBarJduTmp;
          STensorOperation::zero(dBarJduTmp);
          SVector3 Bipg;
          Bipg(0) = Gradsipg[k][0];
          Bipg(1) = Gradsipg[k][1];
          Bipg(2) = Gradsipg[k][2];
          for(int ll=0; ll<_dim; ll++)
          {
            for(int n=0; n<_dim; n++)
            {
              dBarJduTmp(ll) += invBarF(n,ll)*Bipg(n)*weight;
            }
          }
          dBarJdu[k]+=dBarJduTmp; 
        }
      }
      
      static STensor3 I;
      STensorOperation::unity(I);
      for (int ipg = 0; ipg < npts; ipg++)
      {
        const IPStateBase *ips = (*vips)[ipg];
        const dG3DIPVariableBase *ipv = static_cast<const dG3DIPVariableBase*>(ips->getState(IPStateBase::current));
        const std::vector<TensorialTraits<double>::GradType>& Gradsipg = ipv->gradf(sp,ele,GP[ipg]);
        const STensor3 &barF = ipv->getConstRefToDeformationGradient(); //this is actually barF
        STensor3 invBarF(barF.invert());
        double localJ=ipv->getLocalJ();
        double barJ=ipv->getBarJ();
        for(int k=0;k<nbFF;k++)
        {
          static STensor33 dBarFduTmp;
          STensorOperation::zero(dBarFduTmp);
          for(int kk=0; kk<_dim; kk++){
            for(int m=0; m<_dim; m++){
              for(int jj=0; jj<_dim; jj++){
                dBarFduTmp(kk,m,jj) += pow(barJ,1./((double)(_dim)))*pow(localJ,-1./((double)(_dim)))*I(kk,jj)*Gradsipg[k](m);
                for(int p=0; p<_dim; p++){
                  dBarFduTmp(kk,m,jj) -= 1./((double)(_dim))*pow(barJ,1./((double)(_dim)))*pow(localJ,-1./((double)(_dim)))*barF(kk,m)*invBarF(p,jj)*Gradsipg[k](p);             
                }
                dBarFduTmp(kk,m,jj) += 1./((double)(_dim))/barJ*barF(kk,m)*dBarJdu[k](jj);
              }                                
            }
          }
          if(_dim==2 or _dim==1)
            dBarFduTmp(2,2,2)=Gradsipg[k](3);
          if(_dim==1)
            dBarFduTmp(1,1,1)=Gradsipg[k](2);
          dBarFdu[ipg][k]=dBarFduTmp;
        }
      }
    } 
    /////
  
  
  for (int i = 0; i < npts; i++)
  {
    const IPStateBase *ips = (*vips)[i];
    const dG3DIPVariableBase *ipv = static_cast<const dG3DIPVariableBase*>(ips->getState(IPStateBase::current));
		
    const std::vector<TensorialTraits<double>::GradType>&  Grads  = ipv->gradf(sp,ele,GP[i]);
    const std::vector<TensorialTraits<double>::ValType>&  Vals  = ipv->f(sp,ele,GP[i]);
		
    const double weight = GP[i].weight;
    double &detJ = ipv->getJacobianDeterminant(ele,GP[i]);
		
    const double ratio = detJ*weight;
    const STensor43& L = ipv->getConstRefToTangentModuli();
		
		
    for (int row=firstRowP; row< lastRowP; row++){
      int p,q;
      Tensor23::getIntsFromIndex(row-firstRowP,p,q);
      for (int k=0; k<3; k++)
      {
        int nbFF = (dynamic_cast<const nlsFunctionSpace<double>*>(sp))->getNumShapeFunctions(ele,k);
        int nbFFTotalLast = (dynamic_cast<const nlsFunctionSpace<double>*>(sp))->getShapeFunctionsIndex(ele,k);
        for (int j=0; j<nbFF; j++)
        {  
          for (int r= 0; r<3; r++)
          {
            if(useBarF){
              for (int ll= 0; ll<3; ll++){
                 m(row,j+nbFFTotalLast)+= L(p,q,ll,r)*dBarFdu[i][j](ll,r,k)*ratio;
              }   
            }
            else{
              m(row,j+nbFFTotalLast)+= L(p,q,k,r)*Grads[j+nbFFTotalLast](r)*ratio; 
            }  
          }
        }
      }
    }
    if(_dom->getHomogenizationOrder() == 2 and ipv->hasBodyForceForHO()){
       SPoint3 pt;
       ele->pnt(Vals,pt);
       const STensor33& dBdF = ipv->getConstRefTodBmdFm();  
       for (int row=firstRowP; row< lastRowP; row++){
         int p,q;
         Tensor23::getIntsFromIndex(row-firstRowP,p,q);
         for (int k=0; k<3; k++)
         {
           int nbFF = (dynamic_cast<const nlsFunctionSpace<double>*>(sp))->getNumShapeFunctions(ele,k);
           int nbFFTotalLast = (dynamic_cast<const nlsFunctionSpace<double>*>(sp))->getShapeFunctionsIndex(ele,k);
           for (int j=0; j<nbFF; j++)
           {  
            for (int r= 0; r<3; r++){
              if(useBarF){
                for (int ll= 0; ll<3; ll++){
                   m(row,j+nbFFTotalLast)-= dBdF(p,ll,r)*pt[q]*dBarFdu[i][j](ll,r,k)*ratio;
                }   
              }
              else{
                m(row,j+nbFFTotalLast)-= dBdF(p,k,r)*pt[q]*Grads[j+nbFFTotalLast](r)*ratio;
              }
            }
          }
        }
      }    
    }
    //nonlocal
    if(ipv->getNumberNonLocalVariable()>0)
    {
      const std::vector<STensor3>& dPdphiVec = ipv->getConstRefToDStressDNonLocalVariable();
      for (int row=firstRowP; row< lastRowP; row++){
        int p,q;
        Tensor23::getIntsFromIndex(row-firstRowP,p,q);
        for (int k=0; k<ipv->getNumberNonLocalVariable(); k++)
        {
          int nbFFCol = (dynamic_cast<const nlsFunctionSpace<double>*>(sp))->getNumShapeFunctions(ele,3+k);
          int nbFFTotalLastCol = (dynamic_cast<const nlsFunctionSpace<double>*>(sp))->getShapeFunctionsIndex(ele,3+k);
          for (int j=0; j<nbFFCol; j++)
          {
            const STensor3& dPdphi = dPdphiVec[k];
            m(row,j+nbFFTotalLastCol)+= dPdphi(p,q)*Vals[j+nbFFTotalLastCol]*ratio;
          }
        }
      }
    } 
  
    if (_dom->getHomogenizationOrder() == 2)
    {
      SPoint3 pt;
      ele->pnt(Vals,pt);
      for (int row=firstRowQ; row< lastRowQ; row++){
        int ii,jj,kk;
        Tensor33::getIntsFromIndex(row-firstRowQ,ii,jj,kk);
        for (int k=0; k<3; k++)
        {
           int nbFF = (dynamic_cast<const nlsFunctionSpace<double>*>(sp))->getNumShapeFunctions(ele,k);
           int nbFFTotalLast = (dynamic_cast<const nlsFunctionSpace<double>*>(sp))->getShapeFunctionsIndex(ele,k);
           for (int j=0; j<nbFF; j++)
           {  
             for (int r= 0; r<3; r++){
               if(useBarF){
                 for (int ll= 0; ll<3; ll++){
                    m(row,j+nbFFTotalLast)+= 0.5*(L(ii,jj,ll,r)*pt[kk]+L(ii,kk,ll,r)*pt[jj])*dBarFdu[i][j](ll,r,k)*ratio;
                 }   
               }
               else{
                   m(row,j+nbFFTotalLast)+= 0.5*(L(ii,jj,k,r)*pt[kk]+L(ii,kk,k,r)*pt[jj])*Grads[j+nbFFTotalLast](r)*ratio;
               }
             }
           }
         }
      }
      // for high order with body force
     
      if(ipv->hasBodyForceForHO())
      {
         const STensor33& dBdF = ipv->getConstRefTodBmdFm();    
         for (int row=firstRowQ; row< lastRowQ; row++)
         {
           int ii,jj,kk;
           Tensor33::getIntsFromIndex(row-firstRowQ,ii,jj,kk);
           for (int k=0; k<3; k++)
           {
             int nbFF = (dynamic_cast<const nlsFunctionSpace<double>*>(sp))->getNumShapeFunctions(ele,k);
             int nbFFTotalLast = (dynamic_cast<const nlsFunctionSpace<double>*>(sp))->getShapeFunctionsIndex(ele,k);
             for (int j=0; j<nbFF; j++)
             {  
                for (int r= 0; r<3; r++){
                 if(useBarF){
                   for (int ll= 0; ll<3; ll++){
                    m(row,j+nbFFTotalLast)-= 0.5*dBdF(ii,ll,r)*(pt[jj]*pt[kk]-_rveGeoInertia(jj,kk)/_rveVolume)*dBarFdu[i][j](ll,r,k)*ratio;                
                   }   
                 }
                else{
                  m(row,j+nbFFTotalLast)-= 0.5*dBdF(ii,k,r)*(pt[jj]*pt[kk]-_rveGeoInertia(jj,kk)/_rveVolume) *Grads[j+nbFFTotalLast](r)*ratio;
                 }
               }    
             }                        
           }       
         } 
        if(ipv->getNumberNonLocalVariable()>0)
        {
          Msg::Error("dG3DNonLocalHomogenizedTangent::get for second order has not been implemented");
        } 
      }
    }
		
    if (_dom->getExtractIrreversibleEnergyFlag())
    {
      const STensor3& dIrrEnergdF = ipv->getConstRefToDIrreversibleEnergyDDeformationGradient();
      for (int row=firstRowIrEnergy; row < lastRowIrEnergy; row++){
         for (int k=0; k<3; k++)
         {
           int nbFF = (dynamic_cast<const nlsFunctionSpace<double>*>(sp))->getNumShapeFunctions(ele,k);
           int nbFFTotalLast = (dynamic_cast<const nlsFunctionSpace<double>*>(sp))->getShapeFunctionsIndex(ele,k);
           for (int j=0; j<nbFF; j++)
           {  
             for (int r= 0; r<3; r++)
               if(useBarF){
                 for (int ll= 0; ll<3; ll++){
                    m(row,j+nbFFTotalLast)+= dIrrEnergdF(ll,r)*dBarFdu[i][j](ll,r,k)*ratio;
                 }   
               }
               else{
                  m(row,j+nbFFTotalLast)+= dIrrEnergdF(k,r)*Grads[j+nbFFTotalLast](r)*ratio;
               }
            }
         }
      }
      //nonlocal
      if(ipv->getNumberNonLocalVariable()>0)
      {
       if( ipv->getNumberNonLocalVariable()>_dom->getNumNonLocalVariable() ) Msg::Error("Your material law use more non local variable than your domain dG3DHomogenizedTangent::get");
       for (int row=firstRowIrEnergy; row < lastRowIrEnergy; row++){
          for (int k=0; k<ipv->getNumberNonLocalVariable(); k++){
            int nbFFCol = (dynamic_cast<const nlsFunctionSpace<double>*>(sp))->getNumShapeFunctions(ele,3+k);
            int nbFFTotalLastCol = (dynamic_cast<const nlsFunctionSpace<double>*>(sp))->getShapeFunctionsIndex(ele,3+k);
            for (int j=0; j<nbFFCol; j++){
              const double& dIrrEnergDphi = ipv->getConstRefToDIrreversibleEnergyDNonLocalVariable(k);
              m(row-firstRowIrEnergy,j+nbFFTotalLastCol)+= dIrrEnergDphi*Vals[j+nbFFTotalLastCol]*ratio;
            }
          }
        }
      }
    }
    if (_dom->isExtractCohesiveLawFromMicroDamage() and (_ipf->getNumOfActiveDissipationIPs() > 0)) 
    {
      const dG3DIPVariableBase *ipvprev = static_cast<const dG3DIPVariableBase*>(ips->getState(IPStateBase::activeDissipation));
      static STensor43 I4(1.);
      if (ipvprev->dissipationIsActive()){
        for (int row=firstRowFD; row< lastRowFD; row++){
          int p,q;
          Tensor23::getIntsFromIndex(row-firstRowFD,p,q);
          for (int k=0; k<3; k++)
          {
            int nbFF = (dynamic_cast<const nlsFunctionSpace<double>*>(sp))->getNumShapeFunctions(ele,k);
            int nbFFTotalLast = (dynamic_cast<const nlsFunctionSpace<double>*>(sp))->getShapeFunctionsIndex(ele,k);
            for (int j=0; j<nbFF; j++)
            {  
              for (int r= 0; r<3; r++){
                if(useBarF){
                  for (int ll= 0; ll<3; ll++){
                    m(row,j+nbFFTotalLast)+= I4(p,q,ll,r)*dBarFdu[i][j](ll,r,k)*ratio;
                  }   
                }
                else
                {
                   m(row,j+nbFFTotalLast)+= I4(p,q,k,r)*Grads[j+nbFFTotalLast](r)*ratio;
                } 
              }
            }
          }
        }
      }
    }
    if (_dom->getNumConstitutiveExtraDofDiffusionVariable()>0  )
    {
      if( ipv->getNumConstitutiveExtraDofDiffusionVariable()>_dom->getNumConstitutiveExtraDofDiffusionVariable()) 
             Msg::Error("Your material law use more constitutive extra dof variable than your domain dG3DHomogenizedTangent::get");
      for (int extraDOFField = 0; extraDOFField< ipv->getNumConstitutiveExtraDofDiffusionVariable(); extraDOFField++){
        static bool msg=false;
        if(_dom->getNumConstitutiveExtraDofDiffusionVariable()>1)
        {
          if(!msg)
          {
            Msg::Info("we assume no cross dependency between the extra dof sources domain dG3DHomogenizedTangent::get");
            msg=true;
          }
        }
        int indexField=3+_dom->getNumNonLocalVariable()+extraDOFField;
        int nbFFCol = (dynamic_cast<const nlsFunctionSpace<double>*>(sp))->getNumShapeFunctions(ele,indexField);
        int nbFFTotalLastCol = (dynamic_cast<const nlsFunctionSpace<double>*>(sp))->getShapeFunctionsIndex(ele,indexField);
        int extraDOFField2=extraDOFField;
        const STensor3  &dPdField        = ipv->getConstRefTodPdField()[extraDOFField];
        const STensor3  &dFluxdGradField = ipv->getConstRefTodFluxdGradField()[extraDOFField][extraDOFField2]; //in reference conf
        const SVector3  &dFluxdField     = ipv->getConstRefTodFluxdField()[extraDOFField][extraDOFField2]; //in reference conf
        const STensor33 &dFluxdF         = ipv->getConstRefTodFluxdF()[extraDOFField]; //in reference conf
        // stress- extra dof
        for (int row=firstRowP; row< lastRowP; row++){
          int p,q;
          Tensor23::getIntsFromIndex(row-firstRowP,p,q);
          for (int j=0; j<nbFFCol; j++){
            m(row,nbFFTotalLastCol+j)+= dPdField(p,q)*Vals[j+nbFFTotalLastCol]*ratio;
          }
        }

        // flux
        for (int row=firstRowFlux; row< lastRowFlux; row++){
          // term dFluxdF
          int p;
          Tensor13::getIntsFromIndex(row-firstRowFlux,p);
          for (int k=0; k<3; k++)
          {
            int nbFF = (dynamic_cast<const nlsFunctionSpace<double>*>(sp))->getNumShapeFunctions(ele,k);
            int nbFFTotalLast = (dynamic_cast<const nlsFunctionSpace<double>*>(sp))->getShapeFunctionsIndex(ele,k);
            for (int j=0; j<nbFF; j++)
            {  
              for (int r= 0; r<3; r++){
                if(useBarF){
                  for (int ll= 0; ll<3; ll++){
                    m(row,j+k*nbFF)+= dFluxdF(p,ll,r)*dBarFdu[i][j](ll,r,k)*ratio;
                  }   
                }
                else{
                  m(row,j+k*nbFF)+= dFluxdF(p,k,r)*Grads[j+k*nbFF](r)*ratio;
                }
              }
            }
          }
          // term dFluxdGradField
          for (int j=0; j<nbFFCol; j++){
            for (int r= 0; r<3; r++){
              m(row,nbFFTotalLastCol+j)+= dFluxdGradField(p,r)*Grads[nbFFTotalLastCol+j](r)*ratio;
            }
          }
          // term dFluxdField
          for (int j=0; j<nbFFCol; j++){
            m(row,nbFFTotalLastCol+j)+= dFluxdField(p)*Vals[nbFFTotalLastCol+j]*ratio;
          }
        }

        // mechanical source converts to heat
        const STensor3& DmecaSourceDF = ipv->getConstRefTodMechanicalSourcedF()[extraDOFField];
        const SVector3& DmecaSourceDGradField = ipv->getConstRefTodMechanicalSourcedGradField()[extraDOFField][extraDOFField2];
        const double& DmecaSourceDField = ipv->getConstRefTodMechanicalSourcedField()(extraDOFField,extraDOFField2);

        // term DmecaSourceDF
        for (int row = firstRowMecaSource; row < lastRowMecaSource; row++){
          for (int k=0; k<3; k++)
          {
            int nbFF = (dynamic_cast<const nlsFunctionSpace<double>*>(sp))->getNumShapeFunctions(ele,k);
            int nbFFTotalLast = (dynamic_cast<const nlsFunctionSpace<double>*>(sp))->getShapeFunctionsIndex(ele,k);
            for (int j=0; j<nbFF; j++)
            {  
              for (int r= 0; r<3; r++){
                if(useBarF){
                  for (int ll= 0; ll<3; ll++){
                     m(row,j+k*nbFF)+=  DmecaSourceDF(ll,r)*dBarFdu[i][j](ll,r,k)*ratio;
                  }   
                }
                else{
                  m(row,j+k*nbFF)+= DmecaSourceDF(k,r)*Grads[j+k*nbFF](r)*ratio;
                }
              }
            }
          }
          // term DmecaSourceDGradField
          for (int j=0; j<nbFFCol; j++){
            for (int r= 0; r<3; r++){
              m(row,nbFFTotalLastCol+j)+= DmecaSourceDGradField(r)*Grads[nbFFTotalLastCol+j](r)*ratio;
            }
          }
          // term DmecaSourceDField
	  for (int j=0; j<nbFFCol; j++){
            m(row,nbFFTotalLastCol+j)+= DmecaSourceDField*Vals[nbFFTotalLastCol+j]*ratio;
          }
        }
      }
    }	
  }
};



void dG3DBodyForceTangent::get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const{
  const FunctionSpaceBase* sp = _dom->getFunctionSpace();
	
  int nbDof = sp->getNumKeys(ele);
  m.resize(nbDof,nCols);
  m.setAll(0.);
	
  const AllIPState::ipstateElementContainer *vips = _ipf->getAips()->getIPstate(ele->getNum());

  for (int i = 0; i < npts; i++)
  {
    const IPStateBase *ips  = (*vips)[i];
    const dG3DIPVariableBase *ipv     = static_cast<const dG3DIPVariableBase*>(ips->getState(IPStateBase::current));
		
    const std::vector<TensorialTraits<double>::GradType>&  Grads  = ipv->gradf(sp,ele,GP[i]);
    const std::vector<TensorialTraits<double>::ValType>&  Vals  = ipv->f(sp,ele,GP[i]);
		
    const double weight = GP[i].weight;
    double &detJ = ipv->getJacobianDeterminant(ele,GP[i]);
		
    const double ratio = detJ*weight;
    const STensor43& dBmdGM = ipv->getConstRefTodBmdGM();
		
		
    for (int col=firstColF; col< lastColF; col++)
    {
       for (int k=0; k<3; k++)
       {
         int nbFF = (dynamic_cast<const nlsFunctionSpace<double>*>(sp))->getNumShapeFunctions(ele,k);
         int nbFFTotalLast = (dynamic_cast<const nlsFunctionSpace<double>*>(sp))->getShapeFunctionsIndex(ele,k);
         for (int j=0; j<nbFF; j++)
         {  
             m(j+k*nbFF,col)+= 0.0;
         }
       }
    }
    
    if (_dom->getHomogenizationOrder() == 2 and ipv->hasBodyForceForHO())
    {
      for (int col=firstColG; col< lastColG; col++){
        int ii,jj,kk;
        Tensor33::getIntsFromIndex(col-firstColG,ii,jj,kk);
        for (int k=0; k<3; k++)
        {
          int nbFF = (dynamic_cast<const nlsFunctionSpace<double>*>(sp))->getNumShapeFunctions(ele,k);
          int nbFFTotalLast = (dynamic_cast<const nlsFunctionSpace<double>*>(sp))->getShapeFunctionsIndex(ele,k);
          for (int j=0; j<nbFF; j++)
          {  
              m(j+k*nbFF,col)+= dBmdGM(k,ii,jj,kk)*Vals[j+nbFFTotalLast]*ratio;
          }
        }
      }           
    }	
  }
};


void dG3DBodyForceTermPathFollowing::get(MElement *ele,int npts,IntPt *GP,fullVector<double> &bm) const{
  const FunctionSpaceBase* sp = _dom->getFunctionSpace();
  const STensor33& G= _dom->getSecondOrderKinematic();
  	
  int nbDof = sp->getNumKeys(ele);
  bm.resize(nbDof);
  bm.setAll(0.);
	
  const AllIPState::ipstateElementContainer *vips = _ipf->getAips()->getIPstate(ele->getNum());

  for (int i = 0; i < npts; i++)
  {
    const IPStateBase *ips  = (*vips)[i];
    const dG3DIPVariableBase *ipv  = static_cast<const dG3DIPVariableBase*>(ips->getState(IPStateBase::current));
    const dG3DIPVariableBase *ipvprev  = static_cast<const dG3DIPVariableBase*>(ips->getState(IPStateBase::previous));
		
    const std::vector<TensorialTraits<double>::ValType>&  Vals = ipv->f(sp,ele,GP[i]);		
    const double weight = GP[i].weight;
    double &detJ = ipv->getJacobianDeterminant(ele,GP[i]);
		
    const double ratio = detJ*weight;
    SVector3 Bm;
    ipv->computeBodyForce(G, Bm);
      
    if (_dom->getHomogenizationOrder() == 2 and ipv->hasBodyForceForHO())
    {
       for (int k=0; k<3; k++)
       {
         int nbFF = (dynamic_cast<const nlsFunctionSpace<double>*>(sp))->getNumShapeFunctions(ele,k);
         int nbFFTotalLast = (dynamic_cast<const nlsFunctionSpace<double>*>(sp))->getShapeFunctionsIndex(ele,k);
         for (int j=0; j<nbFF; j++)
         {  
              bm(j+k*nbFF)+= Bm(k)*Vals[j+k*nbFF]*ratio;
         }
       }           
    }	
  }
}



void dG3DHomogenizedElasticTangent::get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const{
  const FunctionSpaceBase* sp = _dom->getFunctionSpace();
	
  int nbDof = sp->getNumKeys(ele);
  int stressDim = _dom->getStressDimension();
  m.resize(stressDim,nbDof);
  m.setAll(0.);
	
  const AllIPState::ipstateElementContainer *vips = _ipf->getAips()->getIPstate(ele->getNum());

  for (int i = 0; i < npts; i++)
  {
    const IPStateBase *ips        = (*vips)[i];
    const dG3DIPVariableBase *ipv     = static_cast<const dG3DIPVariableBase*>(ips->getState(IPStateBase::current));
		
    const std::vector<TensorialTraits<double>::GradType>&  Grads  = ipv->gradf(sp,ele,GP[i]);
    const std::vector<TensorialTraits<double>::ValType>&  Vals  = ipv->f(sp,ele,GP[i]);
		
    const double weight = GP[i].weight;
    double &detJ = ipv->getJacobianDeterminant(ele,GP[i]);
		
    const double ratio = detJ*weight;
    const STensor43& L = ipv->getConstRefToElasticTangentModuli();
    double damageCorrection=1.;
    if(removeDamage)
    {
      double damageFromInitialState=ipv->get(IPField::DAMAGE);
      if(damageFromInitialState>0.999)
        damageFromInitialState=0.999;
      damageCorrection=1./(1.-damageFromInitialState);
    }
		
  
    for (int row=firstRowP; row< lastRowP; row++){
      int p,q;
      Tensor23::getIntsFromIndex(row-firstRowP,p,q);
      for (int k=0; k<3; k++)
      {
        int nbFF = (dynamic_cast<const nlsFunctionSpace<double>*>(sp))->getNumShapeFunctions(ele,k);
        int nbFFTotalLast = (dynamic_cast<const nlsFunctionSpace<double>*>(sp))->getShapeFunctionsIndex(ele,k);
        for (int j=0; j<nbFF; j++)
        {  
          for (int r= 0; r<3; r++)
            m(row,j+k*nbFF)+= L(p,q,k,r)*Grads[j+k*nbFF](r)*ratio*damageCorrection;
        }
      }
    }
    //nonlocal
    /*if(ipv->getNumberNonLocalVariable()>0)
    {
      const std::vector<STensor3>& dPdphiVec = ipv->getConstRefToDStressDNonLocalVariable();
      for (int row=firstRowP; row< lastRowP; row++){
        int p,q;
        Tensor23::getIntsFromIndex(row-firstRowP,p,q);
        for (int j=0; j<nbFF; j++)
        {
          for (int k=0; k<ipv->getNumberNonLocalVariable(); k++)
          {
            const STensor3& dPdphi = dPdphiVec[k];
            m(row,j+k*nbFF+3*nbFF)+= dPdphi(p,q)*Vals[j+k*nbFF+3*nbFF]*ratio;
          }
        }
      }
    } 
    */
    
    if (_dom->getHomogenizationOrder() == 2)
    {
      SPoint3 pt;
      ele->pnt(Vals,pt);
      for (int row=firstRowQ; row< lastRowQ; row++){
        int ii,jj,kk;
        Tensor33::getIntsFromIndex(row-firstRowQ,ii,jj,kk);
        for (int k=0; k<3; k++)
        {
          int nbFF = (dynamic_cast<const nlsFunctionSpace<double>*>(sp))->getNumShapeFunctions(ele,k);
          int nbFFTotalLast = (dynamic_cast<const nlsFunctionSpace<double>*>(sp))->getShapeFunctionsIndex(ele,k);
          for (int j=0; j<nbFF; j++)
          {  
            for (int r= 0; r<3; r++)
              m(row,j+nbFFTotalLast)+= 0.5*(L(ii,jj,k,r)*pt[kk]+L(ii,kk,k,r)*pt[jj])*Grads[j+nbFFTotalLast](r)*ratio*damageCorrection;
          }
        }
      }
      if(ipv->getNumberNonLocalVariable()>0)
      {
        Msg::Error("dG3DNonLocalHomogenizedTangent::get for second order has not been implemented");
      } 
    }
		
    if (_dom->getExtractIrreversibleEnergyFlag()){
      const STensor3& dIrrEnergdF = ipv->getConstRefToDIrreversibleEnergyDDeformationGradient();
      for (int row=firstRowIrEnergy; row < lastRowIrEnergy; row++){
        for (int k=0; k<3; k++)
        {
          int nbFF = (dynamic_cast<const nlsFunctionSpace<double>*>(sp))->getNumShapeFunctions(ele,k);
          int nbFFTotalLast = (dynamic_cast<const nlsFunctionSpace<double>*>(sp))->getShapeFunctionsIndex(ele,k);
          for (int j=0; j<nbFF; j++)
          {  
            for (int r= 0; r<3; r++)
              m(row,j+nbFFTotalLast)+= dIrrEnergdF(k,r)*Grads[j+nbFFTotalLast](r)*ratio;
          }
        }
      }
      //nonlocal
      if(ipv->getNumberNonLocalVariable()>0)
      {
       if( ipv->getNumberNonLocalVariable()>_dom->getNumNonLocalVariable() ) Msg::Error("Your material law use more non local variable than your domain dG3DHomogenizedTangent::get");
       for (int row=firstRowIrEnergy; row < lastRowIrEnergy; row++){
         for (int k=0; k<ipv->getNumberNonLocalVariable(); k++){
           int nbFFCol = (dynamic_cast<const nlsFunctionSpace<double>*>(sp))->getNumShapeFunctions(ele,3+k);
           int nbFFTotalLastCol = (dynamic_cast<const nlsFunctionSpace<double>*>(sp))->getShapeFunctionsIndex(ele,3+k);
           for (int j=0; j<nbFFCol; j++){
             const double& dIrrEnergDphi = ipv->getConstRefToDIrreversibleEnergyDNonLocalVariable(k);
             m(row-firstRowIrEnergy,j+nbFFTotalLastCol)+= dIrrEnergDphi*Vals[j+nbFFTotalLastCol]*ratio;
            }
          }
        }
      }
    }
    if (_dom->isExtractCohesiveLawFromMicroDamage() and (_ipf->getNumOfActiveDissipationIPs() > 0)) {
      const dG3DIPVariableBase *ipvprev = static_cast<const dG3DIPVariableBase*>(ips->getState(IPStateBase::activeDissipation));
      static STensor43 I4(1.);
      if (ipvprev->dissipationIsActive()){
        for (int row=firstRowFD; row< lastRowFD; row++){
          int p,q;
          Tensor23::getIntsFromIndex(row-firstRowFD,p,q);
          for (int k=0; k<3; k++)
          {
            int nbFF = (dynamic_cast<const nlsFunctionSpace<double>*>(sp))->getNumShapeFunctions(ele,k);
            int nbFFTotalLast = (dynamic_cast<const nlsFunctionSpace<double>*>(sp))->getShapeFunctionsIndex(ele,k);
            for (int j=0; j<nbFF; j++)
            {  
              for (int r= 0; r<3; r++){
                m(row,j+nbFFTotalLast)+= I4(p,q,k,r)*Grads[j+nbFFTotalLast](r)*ratio;
              }
            }
          }
        }
      }
    }
    if (_dom->getNumConstitutiveExtraDofDiffusionVariable()>0  ){
      if( ipv->getNumConstitutiveExtraDofDiffusionVariable()>_dom->getNumConstitutiveExtraDofDiffusionVariable()) 
             Msg::Error("Your material law use more constitutive extra dof variable than your domain dG3DHomogenizedTangent::get");
      for (int extraDOFField = 0; extraDOFField< ipv->getNumConstitutiveExtraDofDiffusionVariable(); extraDOFField++){
        static bool msg=false;
        if(_dom->getNumConstitutiveExtraDofDiffusionVariable()>1)
        {
          if(!msg)
          {
            Msg::Info("we assume no cross dependency between the extra dof sources domain dG3DHomogenizedTangent::get");
            msg=true;
          }
        }
        int indexField=3+_dom->getNumNonLocalVariable()+extraDOFField;
        int nbFFCol = (dynamic_cast<const nlsFunctionSpace<double>*>(sp))->getNumShapeFunctions(ele,indexField);
        int nbFFTotalLastCol = (dynamic_cast<const nlsFunctionSpace<double>*>(sp))->getShapeFunctionsIndex(ele,indexField);
        int extraDOFField2=extraDOFField;
        const STensor3  &dPdField        = ipv->getConstRefTodPdField()[extraDOFField];
        const STensor3  &dFluxdGradField = ipv->getConstRefTodFluxdGradField()[extraDOFField][extraDOFField2]; //in reference conf
        const SVector3  &dFluxdField     = ipv->getConstRefTodFluxdField()[extraDOFField][extraDOFField2]; //in reference conf
        const STensor33 &dFluxdF         = ipv->getConstRefTodFluxdF()[extraDOFField]; //in reference conf
        // stress- extra dof
        for (int row=firstRowP; row< lastRowP; row++){
          int p,q;
          Tensor23::getIntsFromIndex(row-firstRowP,p,q);
          for (int j=0; j<nbFFCol; j++){
            m(row,nbFFTotalLastCol+j)+= dPdField(p,q)*Vals[j+nbFFTotalLastCol]*ratio;
          }
        }

        // flux
        for (int row=firstRowFlux; row< lastRowFlux; row++){
          // term dFluxdF
          int p;
          Tensor13::getIntsFromIndex(row-firstRowFlux,p);
          for (int k=0; k<3; k++)
          {
            int nbFF = (dynamic_cast<const nlsFunctionSpace<double>*>(sp))->getNumShapeFunctions(ele,k);
            int nbFFTotalLast = (dynamic_cast<const nlsFunctionSpace<double>*>(sp))->getShapeFunctionsIndex(ele,k);
            for (int j=0; j<nbFF; j++)
            {  
              for (int r= 0; r<3; r++){
                m(row,j+nbFFTotalLast)+= dFluxdF(p,k,r)*Grads[j+nbFFTotalLast](r)*ratio;
              }
            }
          }
          // term dFluxdGradField
          for (int j=0; j<nbFFCol; j++){
            for (int r= 0; r<3; r++){
              m(row,nbFFTotalLastCol+j)+= dFluxdGradField(p,r)*Grads[nbFFTotalLastCol+j](r)*ratio;
            }
          }
          // term dFluxdField
          for (int j=0; j<nbFFCol; j++){
            m(row,nbFFTotalLastCol+j)+= dFluxdField(p)*Vals[nbFFTotalLastCol+j]*ratio;
          }
        }

        // mechanical source converts to heat
        const STensor3& DmecaSourceDF = ipv->getConstRefTodMechanicalSourcedF()[extraDOFField];
        const SVector3& DmecaSourceDGradField = ipv->getConstRefTodMechanicalSourcedGradField()[extraDOFField][extraDOFField2];
        const double& DmecaSourceDField = ipv->getConstRefTodMechanicalSourcedField()(extraDOFField,extraDOFField2);

        // term DmecaSourceDF
        for (int row = firstRowMecaSource; row < lastRowMecaSource; row++){
          for (int k=0; k<3; k++)
          {
            int nbFF = (dynamic_cast<const nlsFunctionSpace<double>*>(sp))->getNumShapeFunctions(ele,k);
            int nbFFTotalLast = (dynamic_cast<const nlsFunctionSpace<double>*>(sp))->getShapeFunctionsIndex(ele,k);
            for (int j=0; j<nbFF; j++)
            {  
              for (int r= 0; r<3; r++){
                m(row,j+nbFFTotalLast)+= DmecaSourceDF(k,r)*Grads[j+nbFFTotalLast](r)*ratio;
              }
            }
          }
          // term DmecaSourceDGradField
          for (int j=0; j<nbFFCol; j++){
            for (int r= 0; r<3; r++){
              m(row,nbFFTotalLastCol+j)+= DmecaSourceDGradField(r)*Grads[nbFFTotalLastCol+j](r)*ratio;
            }
          }
          // term DmecaSourceDField
	  for (int j=0; j<nbFFCol; j++){
            m(row,nbFFTotalLastCol+j)+= DmecaSourceDField*Vals[nbFFTotalLastCol+j]*ratio;
          }
        }
      }
    }	
  }
};
 
 
/*
void dG3DExtraDofHomogenizedTangent::get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const{
	dG3DHomogenizedTangent::get(ele,npts,GP,m);
	
	const FunctionSpaceBase* space = _dom->getFunctionSpace();
	
  int nbDof = space->getNumKeys(ele);
  int nbFF = ele->getNumShapeFunctions();

  int numMDof =nbFF*3;
	
	const AllIPState::ipstateElementContainer *vips = _ipf->getAips()->getIPstate(ele->getNum());
	
  for (int i = 0; i < npts; i++){
		const IPStateBase *ips        = (*vips)[i];
		const dG3DIPVariable *ipv     = static_cast<const dG3DIPVariable*>(ips->getState(IPStateBase::current));
		const dG3DIPVariable *ipvprev = static_cast<const dG3DIPVariable*>(ips->getState(IPStateBase::activeDissipation));		
		
    const double weight = GP[i].weight;
    const std::vector<TensorialTraits<double>::ValType> & Vals = ipv->f(space,ele,GP[i]);
    const std::vector<TensorialTraits<double>::GradType>&  Grads  = ipv->gradf(space,ele,GP[i]);
    double &detJ = ipv->getJacobianDeterminant(ele,GP[i]);
    const double ratio = detJ*weight;

    int extraDOFField=0;
    int extraDOFField2=0;
    const STensor3  &dPdField        = ipv->getConstRefTodPdField()[extraDOFField];
    const STensor3  &dFluxdGradField = ipv->getConstRefTodFluxdGradField()[extraDOFField][extraDOFField2]; //in reference conf
    const SVector3  &dFluxdField     = ipv->getConstRefTodFluxdField()[extraDOFField][extraDOFField2]; //in reference conf
    const STensor33 &dFluxdF         = ipv->getConstRefTodFluxdF()[extraDOFField]; //in reference conf


    // stress- extra dof
    for (int row=firstRowP; row< lastRowP; row++){
      int p,q;
      Tensor23::getIntsFromIndex(row-firstRowP,p,q);
      for (int j=0; j<nbFF; j++){
        m(row,numMDof+j)+= dPdField(p,q)*Vals[j+numMDof]*ratio;
      }
    }

    // flux
    for (int row=firstRowFlux; row< lastRowFlux; row++){
      // term dFluxdF
			int p;
			Tensor13::getIntsFromIndex(row-firstRowFlux,p);
      for (int j=0; j<nbFF; j++){
        for (int k=0; k<3; k++){
          for (int r= 0; r<3; r++){
            m(row,j+k*nbFF)+= dFluxdF(p,k,r)*Grads[j+k*nbFF](r)*ratio;
          }
        }
      }
      // term dFluxdGradField
      for (int j=0; j<nbFF; j++){
        for (int r= 0; r<3; r++){
          m(row,numMDof+j)+= dFluxdGradField(p,r)*Grads[numMDof+j](r)*ratio;
        }
      }
      // term dFluxdField
      for (int j=0; j<nbFF; j++){
          m(row,numMDof+j)+= dFluxdField(p)*Vals[numMDof+j]*ratio;
      }
    }

    // mechanical source converts to heat
    const STensor3& DmecaSourceDF = ipv->getConstRefTodMechanicalSourcedF()[extraDOFField];
    const SVector3& DmecaSourceDGradField = ipv->getConstRefTodMechanicalSourcedGradField()[extraDOFField][extraDOFField2];
    const double& DmecaSourceDField = ipv->getConstRefTodMechanicalSourcedField()(extraDOFField,extraDOFField2);

    // term DmecaSourceDF
		for (int row = firstRowMecaSource; row < lastRowMecaSource; row++){
			for (int j=0; j<nbFF; j++){
				for (int k=0; k<3; k++){
					for (int r= 0; r<3; r++){
						m(row,j+k*nbFF)+= DmecaSourceDF(k,r)*Grads[j+k*nbFF](r)*ratio;
					}
				}
			}
			// term DmecaSourceDGradField
			for (int j=0; j<nbFF; j++){
				for (int r= 0; r<3; r++){
					m(row,numMDof+j)+= DmecaSourceDGradField(r)*Grads[numMDof+j](r)*ratio;
				}
			}
			// term DmecaSourceDField
			for (int j=0; j<nbFF; j++){
					m(row,numMDof+j)+= DmecaSourceDField*Vals[numMDof+j]*ratio;
			}
		
		}

  };
};*/

