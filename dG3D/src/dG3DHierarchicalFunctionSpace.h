//
// C++ Interface: Function Space
//
// Description: Hierarchical function space
//
//
// Author:  <V-D Nguyen>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef DG3DHIERARCHICALFUNCTIONSPACE_H_
#define DG3DHIERARCHICALFUNCTIONSPACE_H_

#include "ThreeDHierarchicalFunctionSpace.h"
#include "InterfaceKeys.h"
#include "elementGroup.h"
#include "GModel.h"
#include "MEdge.h"
#include "MFace.h"
#include "dG3DDof3IntType.h"

class dG3DDof4IntType : public dG3DDof3IntType{
  public :
    dG3DDof4IntType(long int ent, int type) : dG3DDof3IntType(ent,type){}
    virtual ~dG3DDof4IntType(){}
    inline static int createTypeWithFourInts(int comp,int field, int num=0, int dim = 0){return 1000000*field+ 10000*dim +100*num+comp;}
};

class g3DHierarchicalFunctionSpace : public HierarchicalFunctionSpace
{
  public:
    static bool initMap;
    static std::map<MEdge, int, MEdgeLessThan> allEdges;
    static std::map<MFace, int, MFaceLessThan> allFaces;
    static void numerateEdgeAndFace();
    
  protected:
    
    virtual void getKeysOnElement_PerProc(MElement *ele, std::vector<Dof> &keys, int p, int ghost) const;
    virtual void getKeysOnElement(MElement *ele, std::vector<Dof> &keys) const;
    virtual void getShapeFunctions(int c, MElement *ele, double u, double v, double w,  std::vector<double>& vals) const;
    virtual void getGradShapeFunctions(int c, MElement *ele, double u, double v, double w,  std::vector<std::vector<double> >& grads) const;

  public :
    g3DHierarchicalFunctionSpace(int id, int order, int ncomp);
    g3DHierarchicalFunctionSpace(int id, int order, int ncomp, int comp1);
    virtual ~g3DHierarchicalFunctionSpace(){};
    
    virtual std::string getFunctionSpaceName() const 
    {
      return "g3DHierarchicalFunctionSpace";
    }
    
    virtual FunctionSpaceBase* clone(const int id) const
    {
      Msg::Error("g3DHierarchicalFunctionSpace::clone has not been defined !!!");
      return NULL;
    };
    
    virtual void getKeysOnLowerDimensionPart(MElement *ele, MElement* lowEle, const std::vector<int> &vercomp, std::vector<Dof> &keys) const
    {
      Msg::Error("g3DHierarchicalFunctionSpace::getKeysOnLowerDimensionPart has not been defined !!!");
    }
};

class g3DDirichletBoundaryConditionHierarchicalFunctionSpace : public g3DHierarchicalFunctionSpace
{
  protected:
    virtual void getKeysOnElement(MElement *ele, std::vector<Dof> &keys) const;
    
    
  public:
    g3DDirichletBoundaryConditionHierarchicalFunctionSpace(int id, int order, int ncomp) : g3DHierarchicalFunctionSpace(id, order, ncomp){}
    g3DDirichletBoundaryConditionHierarchicalFunctionSpace(int id, int order, int ncomp, int comp1) : g3DHierarchicalFunctionSpace(id,order,ncomp,comp1){}

    virtual ~g3DDirichletBoundaryConditionHierarchicalFunctionSpace(){};
    
    virtual std::string getFunctionSpaceName() const 
    {
      return "g3DDirichletBoundaryConditionHierarchicalFunctionSpace";
    }
};

class g3DNeumannBoundaryConditionHierarchicalFunctionSpace : public g3DHierarchicalFunctionSpace{
  protected:
    virtual void getKeysOnElement(MElement *ele, std::vector<Dof> &keys) const;
    
    bool _parallelMesh;
    void setParallelMesh()
    {
      #if defined(HAVE_MPI)
      if(GModel::current()->getNumPartitions()>0)
      {  
        _parallelMesh = true;
      }
      else
      #endif // HAVE_MPI
     {
       _parallelMesh = false;
     }
   }
  public:
    g3DNeumannBoundaryConditionHierarchicalFunctionSpace(int id, int order, int ncomp) : g3DHierarchicalFunctionSpace(id, order, ncomp){this->setParallelMesh();}
    g3DNeumannBoundaryConditionHierarchicalFunctionSpace(int id, int order, int ncomp, int comp1) : g3DHierarchicalFunctionSpace(id,order,ncomp,comp1){this->setParallelMesh();}
    virtual ~g3DNeumannBoundaryConditionHierarchicalFunctionSpace(){};
    virtual std::string getFunctionSpaceName() const 
    {
      return "g3DNeumannBoundaryConditionHierarchicalFunctionSpace";
    }
};

class dG3DHierarchicalFunctionSpace : public HierarchicalFunctionSpace
{
  protected:
    virtual void getKeysOnElement(MElement *ele, std::vector<Dof> &keys) const;
    virtual void getShapeFunctions(int c, MElement *ele, double u, double v, double w,  std::vector<double>& vals) const;
    virtual void getGradShapeFunctions(int c, MElement *ele, double u, double v, double w,  std::vector<std::vector<double> >& grads) const;
    
  public :
    dG3DHierarchicalFunctionSpace(int id, int order, int ncomp);
    dG3DHierarchicalFunctionSpace(int id, int order, int ncomp, int comp1);
    virtual ~dG3DHierarchicalFunctionSpace(){};
    
    virtual std::string getFunctionSpaceName() const 
    {
      return "dG3DHierarchicalFunctionSpace";
    }
    
    virtual FunctionSpaceBase* clone(const int id) const
    {
      Msg::Error("dG3DHierarchicalFunctionSpace::clone has not been defined !!!");
			return NULL;
    };
    
    virtual void getKeysOnLowerDimensionPart(MElement *ele, MElement* lowEle, const std::vector<int> &vercomp, std::vector<Dof> &keys) const;
};

class dG3DBoundaryConditionHierarchicalFunctionSpace : public dG3DHierarchicalFunctionSpace
{
  protected:
    virtual void getKeysOnElement(MElement *ele, std::vector<Dof> &keys) const
    {
      Msg::Error("Function dG3DBoundaryConditionHierarchicalFunctionSpace::getKeysOnElement cannot be called");
    };
  
  public:
    struct mapInfos
    {
      std::vector<int> vertices;
      std::vector<int> edges;
      std::vector<int> faces;
      mapInfos(){}
      mapInfos(const mapInfos& src): vertices(src.vertices),edges(src.edges),faces(src.faces){};
      mapInfos& operator=(const mapInfos& src)
      {
        vertices = src.vertices;
        edges = src.edges;
        faces = src.faces;
        return *this;
      }
      ~mapInfos(){}
      void print() const
      {
        Msg::Info("vertices size = %d",vertices.size());
        Msg::Info("edges size = %d",edges.size());
        Msg::Info("faces size = %d",faces.size());
      }
    };
    
    static bool checkElement(MElement* e, MElement* eleBulk)
    {
      int nv=e->getNumVertices();
      std::vector<MVertex*> tabV;
      e->getVertices(tabV);
      int nn = eleBulk->getNumVertices();
      
      if (e->getNum() == eleBulk->getNum()){
        return true;
      }
      else if (e->getDim() == 0){
        for (int i=0; i< nn; i++){
          if (tabV[0]->getNum() == eleBulk->getVertex(i)->getNum()){
            return true;
          }
        }
      }
      else if (e->getDim() == 1){
        int nedge = eleBulk->getNumEdges();
        for(int kk=0;kk<nedge;kk++){
          std::vector<MVertex*> verEdge;
          eleBulk->getEdgeVertices(kk,verEdge);
          if((tabV[0]->getNum() == verEdge[0]->getNum() and tabV[1]->getNum() == verEdge[1]->getNum()) or 
             (tabV[0]->getNum() == verEdge[1]->getNum() and tabV[1]->getNum() == verEdge[0]->getNum())){
            return true;
          }
        }
      }
      else if (e->getDim() == 2){
        int nface = eleBulk->getNumFaces();
        for(int kk=0;kk<nface;kk++){
          std::vector<MVertex*> verFace;
          eleBulk->getFaceVertices(kk,verFace);
          int nFaceVer = verFace.size();
          if (nv == nFaceVer){
            interfaceKeys keyBound, keyVolume;
            if (e->getType() == TYPE_TRI){
              keyBound.setValues(e->getVertex(0)->getNum(),e->getVertex(1)->getNum(),e->getVertex(2)->getNum());
              keyVolume.setValues(verFace[0]->getNum(),verFace[1]->getNum(),verFace[2]->getNum());
            }
            else if (e->getType() == TYPE_QUA){
              keyBound.setValues(e->getVertex(0)->getNum(),e->getVertex(1)->getNum(),e->getVertex(2)->getNum(),e->getVertex(3)->getNum());
              keyVolume.setValues(verFace[0]->getNum(),verFace[1]->getNum(),verFace[2]->getNum(),verFace[3]->getNum());
            }
            else{
              Msg::Error("dG3DBoundaryConditionLagrangeFunctionSpace has been defined for tri and quad elements only, complete list for other element");
            }
            
            if (keyBound == keyVolume){
              return true;
            }
          }
        }
      }
      
      return false;
    }
    
    static void getLocalLocation(MElement* e, MElement* eleBulk, mapInfos& info)
    {
      if (e->getDim() < eleBulk->getDim())
      {
        int nv=e->getNumVertices();
        info.vertices.resize(nv);
        for(int j=0;j<nv;j++){
          for (int i=0;i<eleBulk->getNumVertices();++i){
            if(e->getVertex(j)->getNum() == eleBulk->getVertex(i)->getNum()){
              info.vertices[j] = i;
              break;
            }
          }
        }
        
        if (e->getDim() >=1)
        {
          // fine in edges
          int ne = e->getNumEdges();
          info.edges.resize(ne);
          for (int idedge =0; idedge< ne; idedge++)
          {
            MEdge ed = e->getEdge(idedge);
            for (int idB = 0; idB< eleBulk->getNumEdges(); idB++)
            {
              if (ed == eleBulk->getEdge(idB))
              {
                info.edges[idedge] = idB;
                break;
              }
            }
          }
        }
        
        if (e->getDim() >=2)
        {
          // find in face
          int nf = e->getNumFaces();
          info.faces.resize(nf);
          for (int iff =0; iff< nf; iff++)
          {
            MFace ff = e->getFace(iff);
            for (int jf = 0; jf< eleBulk->getNumFaces(); jf++)
            {
              if (ff == eleBulk->getFace(jf))
              {
                info.faces[iff] = jf;
                break;
              }
            }
          }
        }
      }
    };
    
  protected:  
    std::map<MElement*,std::map<MElement*,mapInfos > > mapBoundaryInterfaces;
    
    // constructor function
    void __init__(const elementGroup *g, elementGroup *gi, elementGroup *vinter,elementGroup *ghostMPI)
    {
      for(elementGroup::elementContainer::const_iterator it=g->begin(); it!=g->end(); ++it)
      {
        MElement *e = it->second;
        bool found=false;
        
        // Find the interface element linked with e
        std::map<MElement*,mapInfos > supportEle;
        mapInfos infos; // position of vertices of e in interface element
        for(elementGroup::elementContainer::const_iterator it2=vinter->begin();it2!=vinter->end();++it2)
        {
          MElement* vie = (it2->second);
          found = checkElement(e,vie);
          if (found){
            MInterfaceElement* ielem=dynamic_cast<MInterfaceElement*>(vie);
            // find bulk local vertex numbers
            MElement* eleBulk = ielem->getElem(0);
            getLocalLocation(e,eleBulk,infos);
            supportEle[eleBulk] = infos;
            break;
          }
        }
        
        // If not find on external edge. Look in internal edge
        if(!found)
        {
          for(elementGroup::elementContainer::const_iterator it2=gi->begin();it2!=gi->end();++it2)
          {
            MElement* interE = it2->second;
            found = checkElement(e,interE);
            if (found){
              MInterfaceElement* ielem = dynamic_cast<MInterfaceElement*>(interE);
              MElement* eleBulkMinus = ielem->getElem(0);
              getLocalLocation(e,eleBulkMinus,infos);
              supportEle[eleBulkMinus] = infos;
              
              MElement* eleBulkPlus = ielem->getElem(1);
              getLocalLocation(e,eleBulkPlus,infos);
              supportEle[eleBulkPlus] = infos;
              
              break;
            }
          }
        }
          
        // not found yet look on ghost element
        if(!found)
        {
          if(ghostMPI != NULL) // NULL for Neumman BC
          {
            for(elementGroup::elementContainer::const_iterator itE=ghostMPI->begin(); itE!=ghostMPI->end();++itE)
            {
              MElement *gele = itE->second;
              found = checkElement(e,gele);
              if (found) {
                getLocalLocation(e,gele,infos);
                supportEle[gele] = infos;
                break;
              }
            }
          
            if(!found)
            {
              Msg::Error("Impossible to fix dof for element %d on rank %d",e->getNum(),Msg::GetCommRank());
              Msg::Error("I am on Proc  %d", Msg::GetCommRank());
              Msg::Error("Vertex %d", e->getVertex(0)->getNum());
              Msg::Error("Vertex %d", e->getVertex(1)->getNum());
              Msg::Error("Vertex %d", e->getVertex(2)->getNum());
            }
          }
        }
        
        if (supportEle.size() == 0){
          Msg::Error("volumic element is not found for boundary element %d",e->getNum());
        }
        else{
          if (mapBoundaryInterfaces.find(e) == mapBoundaryInterfaces.end()){
            mapBoundaryInterfaces[e] = supportEle;
          }
          else{
            std::map<MElement*,mapInfos >& allocatedPart = mapBoundaryInterfaces[e];
            allocatedPart.insert(supportEle.begin(),supportEle.end());
          }
        }
      }
    }
    
    void __init__(const elementGroup *g, elementGroup *gBulk, elementGroup *ghostMPI)
    {
      for(elementGroup::elementContainer::const_iterator it=g->begin(); it!=g->end(); ++it)
      {
        MElement *e = it->second;
        std::map<MElement*,mapInfos > supportEle;
        mapInfos infos;
        for (elementGroup::elementContainer::const_iterator itBulk = gBulk->begin(); itBulk != gBulk->end(); itBulk++){
          MElement* eleBulk = itBulk->second;
          bool found = checkElement(e,eleBulk);
          if (found)
          {
            getLocalLocation(e,eleBulk,infos);
            supportEle[eleBulk] = infos;
          }
        }
        
        for (elementGroup::elementContainer::const_iterator itBulk = ghostMPI->begin(); itBulk != ghostMPI->end(); itBulk++){
          MElement* eleGhostMPI = itBulk->second;
          bool found = checkElement(e,eleGhostMPI);
          if (found)
          {
            getLocalLocation(e,eleGhostMPI,infos);
            supportEle[eleGhostMPI] = infos;
          }
        }
        
        if (supportEle.size() == 0){
          Msg::Error("volumic element is not found for boundary element %d",e->getNum());
        }
        else
        {
          if (mapBoundaryInterfaces.find(e) == mapBoundaryInterfaces.end())
          {
            mapBoundaryInterfaces[e] = supportEle;
          }
          else
          {
            std::map<MElement*,mapInfos >& allocatedPart = mapBoundaryInterfaces[e];
            allocatedPart.insert(supportEle.begin(),supportEle.end());
          }
        }
      }  
    };
    
  public:
    dG3DBoundaryConditionHierarchicalFunctionSpace(int id, int order, int ncomp, const elementGroup *g, elementGroup *gi,
                                          elementGroup *vinter, elementGroup *ghostMPI) : dG3DHierarchicalFunctionSpace(id, order, ncomp)
    {
      this->__init__(g,gi,vinter,ghostMPI);
    }
    dG3DBoundaryConditionHierarchicalFunctionSpace(int id, int order, int ncomp, int comp1, const elementGroup *g, elementGroup *gi,
                                          elementGroup *vinter, elementGroup *ghostMPI) : dG3DHierarchicalFunctionSpace(id, order, 1,comp1)
    {
      this->__init__(g,gi,vinter,ghostMPI);
    }
     dG3DBoundaryConditionHierarchicalFunctionSpace(int id, int order, int ncomp, const elementGroup *g, elementGroup *gBulk, elementGroup *ghostMPI) : 
      dG3DHierarchicalFunctionSpace(id, order, ncomp)
    {
      this->__init__(g,gBulk,ghostMPI);
    }
    dG3DBoundaryConditionHierarchicalFunctionSpace(int id, int order, int ncomp, int comp1, const elementGroup *g, elementGroup *gBulk, elementGroup *ghostMPI) : 
      dG3DHierarchicalFunctionSpace(id, order, 1,comp1)
    {
      this->__init__(g,gBulk,ghostMPI);
    }
    virtual ~dG3DBoundaryConditionHierarchicalFunctionSpace(){}
    
    virtual std::string getFunctionSpaceName() const 
    {
      return "dG3DBoundaryConditionHierarchicalFunctionSpace";
    }
    virtual void getKeys(MElement *e, std::vector<Dof> &keys) const;
    virtual void getKeysOnLowerDimensionPart(MElement *ele, MElement* lowEle, const std::vector<int> &vercomp, std::vector<Dof> &keys) const
    {
      Msg::Error("dG3DBoundaryConditionHierarchicalFunctionSpace::getKeysOnLowerDimensionPart cannot be used");
    };
    virtual void getKeysOnVertex(MElement *ele, MVertex *v, const std::vector<int> &vercomp, std::vector<Dof> &keys) const;
    
};

#endif //DG3DHIERARCHICALFUNCTIONSPACE_H_
