//
// C++ Interface: loading with material law
//
//
//
// Author:  <Van Dung NGUYEN>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "computeWithMaterialLaw.h"
#include "nonLinearMechSolver.h"
#include "STensorOperations.h"

computeMaterialLaw::computeMaterialLaw(dG3DMaterialLaw& mlaw): _materialLaw(&mlaw), _ips(NULL), _step(0)
{
  static nonLinearMechSolver solver(1000);
  _materialLaw->setMacroSolver(&solver);
  static bool state = true;
  static bool hasBodyForce=false; 
  static MVertex v(0.,0.,0.);
  static MPoint pt(&v);
  _materialLaw->createIPState(_ips,hasBodyForce,&state,&pt,1,NULL,0);
};

computeMaterialLaw::~computeMaterialLaw()
{
  delete _ips;
  _ips = NULL;
};

void computeMaterialLaw::resetState(dG3DMaterialLaw& mlaw)
{
  delete _ips; _ips = NULL;
  _materialLaw = &mlaw;
  static nonLinearMechSolver solver(1000);
  _materialLaw->setMacroSolver(&solver);
  static bool state = true;
  static bool hasBodyForce=false; 
  static MVertex v(0.,0.,0.);
  static MPoint pt(&v);
  _materialLaw->createIPState(_ips,hasBodyForce,&state,&pt,1,NULL,0);
  _step = 0;
}

void computeMaterialLaw::nextStep()
{
  IPVariable* ipvcur = _ips->getState(IPStateBase::current);
  IPVariable* ipvprev = _ips->getState(IPStateBase::previous);
  ipvprev->operator=(*ipvcur);
  if (ipvcur->withDeepMN())
  {
    std::vector<DeepMaterialNetwork*> allMNs;
    ipvcur->getAllDeepMNs(allMNs);
    for (int i=0; i< allMNs.size(); i++)
    {
      allMNs[i]->copy(IPStateBase::current,IPStateBase::previous);
    }
  };
  
  if (_ips->getMicroSolver() != NULL)
  {
    _ips->getMicroSolver()->prepareForNextMicroSolve();
	}
};

void computeMaterialLaw::setDeformationGradient(int i, int j, double val)
{
    // store current for next step
  dG3DIPVariable* dgip = dynamic_cast<dG3DIPVariable*>(_ips->getState(IPStateBase::current));
  if (!dgip)
  {
    Msg::Error("dG3DIPVariable must be created from material law computeMaterialLaw::setDeformationGradient");
    Msg::Exit(0);
  }
  STensor3& F = dgip->getRefToDeformationGradient();
  F(i,j) = val;
}

void computeMaterialLaw::setTime(double t, double dt)
{
  _materialLaw->setTime(t,dt);
}
void computeMaterialLaw::computeStressState(bool stiff)
{
  IPVariable* ipvcur = _ips->getState(IPStateBase::current);
  const IPVariable* ipvprev = _ips->getState(IPStateBase::previous);
  _materialLaw->stress(ipvcur,ipvprev,stiff,false);
  if (_ips->getMicroSolver() != NULL)
  {
    _step ++;
    _ips->getMicroSolver()->getUnknownField()->archive(_step,_step,true);
    _ips->getMicroSolver()->getIPField()->archive(_step,_step,true);
  };
};

void computeMaterialLaw::computeStressState_uniaxialStress(bool stiff)
{
  IPVariable* ipvcur = _ips->getState(IPStateBase::current);
  const IPVariable* ipvprev = _ips->getState(IPStateBase::previous);
  _materialLaw->stress3DTo1D(ipvcur,ipvprev,stiff,false);
  if (_ips->getMicroSolver() != NULL)
  {
    _step ++;
    _ips->getMicroSolver()->getUnknownField()->archive(_step,_step,true);
    _ips->getMicroSolver()->getIPField()->archive(_step,_step,true);
  }
};


void computeMaterialLaw::computeStressState_PlaneStrainUniaxialStress( char* NoZeroPlane, bool stiff )
{
  IPVariable* ipvcur = _ips->getState(IPStateBase::current);
  const IPVariable* ipvprev = _ips->getState(IPStateBase::previous);
 
  if(NoZeroPlane == NULL){
    Msg::Error("No zero stress plane is not defined");
    Msg::Exit(0);  
  }
  else if(strcmp(NoZeroPlane, "XY") == 0){
    _materialLaw->stress3DTo2D(ipvcur,ipvprev,stiff,false, false);
  }
  else if(strcmp(NoZeroPlane, "XZ") == 0){
    _materialLaw->stress3DTo2D_XZ(ipvcur,ipvprev,stiff,false,false);
  }
  else{
    Msg::Error("Unknow zero stress plane");
    Msg::Exit(0);
  }
  
  
  if (_ips->getMicroSolver() != NULL)
  {
    _step ++;
    _ips->getMicroSolver()->getUnknownField()->archive(_step,_step,true);
    _ips->getMicroSolver()->getIPField()->archive(_step,_step,true);
  }
};






double computeMaterialLaw::getTime() const
{
  return _materialLaw->getTime();
}
double computeMaterialLaw::getTimeStep() const
{
  return _materialLaw->getTimeStep();
}

std::string computeMaterialLaw::getString(int comp, StateType state, ValType vtype) const
{
  std::string str = "";
  if (vtype == Val)
  {
    str = IPField::ToString(comp);
    if (state == Previous)
    {
      str += "_prev";
    }
  } 
  else if (vtype == Increment)
  {
    str = "Increment_"+IPField::ToString(comp);
  }
  else if (vtype == Rate)
  {
    str = "Rate_"+IPField::ToString(comp);
  }
  else
  {
    Msg::Error("val type %d is not defined",vtype);
    Msg::Exit(0);
  }
  return str;
}

double computeMaterialLaw::getValue(int comp, StateType state, ValType vtype) const
{
  IPVariable* ipvcur = _ips->getState(IPStateBase::current);
  const IPVariable* ipvprev = _ips->getState(IPStateBase::previous);
  if (vtype == Val)
  {
    if (state == Previous)
    {
      return ipvprev->get(comp);
    }
    else if (state == Current)
    {
      return ipvcur->get(comp);
    }
    else
    {
      Msg::Error("state %d is not defined",state);
      Msg::Exit(0);
    }
  }
  else if (vtype == Increment)
  {
    if (comp == IPField::F_XX or comp == IPField::F_XY or comp == IPField::F_XZ or
        comp == IPField::F_YX or comp == IPField::F_YY or comp == IPField::F_YZ or
        comp == IPField::F_ZX or comp == IPField::F_ZY or comp == IPField::F_ZZ)
    {
      
      static STensor3 F, Fprev, invFprev, DF, logDF;
      F(0,0) = ipvcur->get(IPField::F_XX);
      F(0,1) = ipvcur->get(IPField::F_XY);
      F(0,2) = ipvcur->get(IPField::F_XZ);
      F(1,0) = ipvcur->get(IPField::F_YX);
      F(1,1) = ipvcur->get(IPField::F_YY);
      F(1,2) = ipvcur->get(IPField::F_YZ);
      F(2,0) = ipvcur->get(IPField::F_ZX);
      F(2,1) = ipvcur->get(IPField::F_ZY);
      F(2,2) = ipvcur->get(IPField::F_ZZ);
      
      Fprev(0,0) = ipvprev->get(IPField::F_XX);
      Fprev(0,1) = ipvprev->get(IPField::F_XY);
      Fprev(0,2) = ipvprev->get(IPField::F_XZ);
      Fprev(1,0) = ipvprev->get(IPField::F_YX);
      Fprev(1,1) = ipvprev->get(IPField::F_YY);
      Fprev(1,2) = ipvprev->get(IPField::F_YZ);
      Fprev(2,0) = ipvprev->get(IPField::F_ZX);
      Fprev(2,1) = ipvprev->get(IPField::F_ZY);
      Fprev(2,2) = ipvprev->get(IPField::F_ZZ);
      
      STensorOperation::inverseSTensor3(Fprev,invFprev);
      STensorOperation::multSTensor3(F,invFprev,DF);
      STensorOperation::logSTensor3(DF,-1,logDF);
      if (comp == IPField::F_XX) return logDF(0,0);
      else if (comp == IPField::F_XY) return logDF(0,1);
      else if (comp == IPField::F_XZ) return logDF(0,2);
      else if (comp == IPField::F_YX) return logDF(1,0);
      else if (comp == IPField::F_YY) return logDF(1,1);
      else if (comp == IPField::F_YZ) return logDF(1,2);
      else if (comp == IPField::F_ZX) return logDF(2,0);
      else if (comp == IPField::F_ZY) return logDF(2,1);
      else if (comp == IPField::F_ZZ) return logDF(2,2);
    }
    else if (comp == IPField::FP_XX or comp == IPField::FP_XY or comp == IPField::FP_XZ or
        comp == IPField::FP_YX or comp == IPField::FP_YY or comp == IPField::FP_YZ or
        comp == IPField::FP_ZX or comp == IPField::FP_ZY or comp == IPField::FP_ZZ)
    {
      static STensor3 Fp, FPprev, invFPprev, DF, logDF;
      Fp(0,0) = ipvcur->get(IPField::FP_XX);
      Fp(0,1) = ipvcur->get(IPField::FP_XY);
      Fp(0,2) = ipvcur->get(IPField::FP_XZ);
      Fp(1,0) = ipvcur->get(IPField::FP_YX);
      Fp(1,1) = ipvcur->get(IPField::FP_YY);
      Fp(1,2) = ipvcur->get(IPField::FP_YZ);
      Fp(2,0) = ipvcur->get(IPField::FP_ZX);
      Fp(2,1) = ipvcur->get(IPField::FP_ZY);
      Fp(2,2) = ipvcur->get(IPField::FP_ZZ);
      
      FPprev(0,0) = ipvprev->get(IPField::FP_XX);
      FPprev(0,1) = ipvprev->get(IPField::FP_XY);
      FPprev(0,2) = ipvprev->get(IPField::FP_XZ);
      FPprev(1,0) = ipvprev->get(IPField::FP_YX);
      FPprev(1,1) = ipvprev->get(IPField::FP_YY);
      FPprev(1,2) = ipvprev->get(IPField::FP_YZ);
      FPprev(2,0) = ipvprev->get(IPField::FP_ZX);
      FPprev(2,1) = ipvprev->get(IPField::FP_ZY);
      FPprev(2,2) = ipvprev->get(IPField::FP_ZZ);
      
      STensorOperation::inverseSTensor3(FPprev,invFPprev);
      STensorOperation::multSTensor3(Fp,invFPprev,DF);
      STensorOperation::logSTensor3(DF,-1,logDF);
      if (comp == IPField::FP_XX) return logDF(0,0);
      else if (comp == IPField::FP_XY) return logDF(0,1);
      else if (comp == IPField::FP_XZ) return logDF(0,2);
      else if (comp == IPField::FP_YX) return logDF(1,0);
      else if (comp == IPField::FP_YY) return logDF(1,1);
      else if (comp == IPField::FP_YZ) return logDF(1,2);
      else if (comp == IPField::FP_ZX) return logDF(2,0);
      else if (comp == IPField::FP_ZY) return logDF(2,1);
      else if (comp == IPField::FP_ZZ) return logDF(2,2);
    }
    else
    {
      return ipvcur->get(comp) - ipvprev->get(comp);
    }
    
  } 
  else if (vtype == Rate)
  {
    double timeStep = _materialLaw->getTimeStep();
    if (comp == IPField::F_XX or comp == IPField::F_XY or comp == IPField::F_XZ or
        comp == IPField::F_YX or comp == IPField::F_YY or comp == IPField::F_YZ or
        comp == IPField::F_ZX or comp == IPField::F_ZY or comp == IPField::F_ZZ)
    {
      
      static STensor3 F, Fprev, invFprev, DF, logDF;
      F(0,0) = ipvcur->get(IPField::F_XX);
      F(0,1) = ipvcur->get(IPField::F_XY);
      F(0,2) = ipvcur->get(IPField::F_XZ);
      F(1,0) = ipvcur->get(IPField::F_YX);
      F(1,1) = ipvcur->get(IPField::F_YY);
      F(1,2) = ipvcur->get(IPField::F_YZ);
      F(2,0) = ipvcur->get(IPField::F_ZX);
      F(2,1) = ipvcur->get(IPField::F_ZY);
      F(2,2) = ipvcur->get(IPField::F_ZZ);
      
      Fprev(0,0) = ipvprev->get(IPField::F_XX);
      Fprev(0,1) = ipvprev->get(IPField::F_XY);
      Fprev(0,2) = ipvprev->get(IPField::F_XZ);
      Fprev(1,0) = ipvprev->get(IPField::F_YX);
      Fprev(1,1) = ipvprev->get(IPField::F_YY);
      Fprev(1,2) = ipvprev->get(IPField::F_YZ);
      Fprev(2,0) = ipvprev->get(IPField::F_ZX);
      Fprev(2,1) = ipvprev->get(IPField::F_ZY);
      Fprev(2,2) = ipvprev->get(IPField::F_ZZ);
      
      STensorOperation::inverseSTensor3(Fprev,invFprev);
      STensorOperation::multSTensor3(F,invFprev,DF);
      STensorOperation::logSTensor3(DF,-1,logDF);
      if (comp == IPField::F_XX) return logDF(0,0)/timeStep;
      else if (comp == IPField::F_XY) return logDF(0,1)/timeStep;
      else if (comp == IPField::F_XZ) return logDF(0,2)/timeStep;
      else if (comp == IPField::F_YX) return logDF(1,0)/timeStep;
      else if (comp == IPField::F_YY) return logDF(1,1)/timeStep;
      else if (comp == IPField::F_YZ) return logDF(1,2)/timeStep;
      else if (comp == IPField::F_ZX) return logDF(2,0)/timeStep;
      else if (comp == IPField::F_ZY) return logDF(2,1)/timeStep;
      else if (comp == IPField::F_ZZ) return logDF(2,2)/timeStep;
    }
    else if (comp == IPField::FP_XX or comp == IPField::FP_XY or comp == IPField::FP_XZ or
        comp == IPField::FP_YX or comp == IPField::FP_YY or comp == IPField::FP_YZ or
        comp == IPField::FP_ZX or comp == IPField::FP_ZY or comp == IPField::FP_ZZ)
    {
      static STensor3 Fp, FPprev, invFPprev, DF, logDF;
      Fp(0,0) = ipvcur->get(IPField::FP_XX);
      Fp(0,1) = ipvcur->get(IPField::FP_XY);
      Fp(0,2) = ipvcur->get(IPField::FP_XZ);
      Fp(1,0) = ipvcur->get(IPField::FP_YX);
      Fp(1,1) = ipvcur->get(IPField::FP_YY);
      Fp(1,2) = ipvcur->get(IPField::FP_YZ);
      Fp(2,0) = ipvcur->get(IPField::FP_ZX);
      Fp(2,1) = ipvcur->get(IPField::FP_ZY);
      Fp(2,2) = ipvcur->get(IPField::FP_ZZ);
      
      FPprev(0,0) = ipvprev->get(IPField::FP_XX);
      FPprev(0,1) = ipvprev->get(IPField::FP_XY);
      FPprev(0,2) = ipvprev->get(IPField::FP_XZ);
      FPprev(1,0) = ipvprev->get(IPField::FP_YX);
      FPprev(1,1) = ipvprev->get(IPField::FP_YY);
      FPprev(1,2) = ipvprev->get(IPField::FP_YZ);
      FPprev(2,0) = ipvprev->get(IPField::FP_ZX);
      FPprev(2,1) = ipvprev->get(IPField::FP_ZY);
      FPprev(2,2) = ipvprev->get(IPField::FP_ZZ);
      
      STensorOperation::inverseSTensor3(FPprev,invFPprev);
      STensorOperation::multSTensor3(Fp,invFPprev,DF);
      STensorOperation::logSTensor3(DF,-1,logDF);
      if (comp == IPField::FP_XX) return logDF(0,0)/timeStep;
      else if (comp == IPField::FP_XY) return logDF(0,1)/timeStep;
      else if (comp == IPField::FP_XZ) return logDF(0,2)/timeStep;
      else if (comp == IPField::FP_YX) return logDF(1,0)/timeStep;
      else if (comp == IPField::FP_YY) return logDF(1,1)/timeStep;
      else if (comp == IPField::FP_YZ) return logDF(1,2)/timeStep;
      else if (comp == IPField::FP_ZX) return logDF(2,0)/timeStep;
      else if (comp == IPField::FP_ZY) return logDF(2,1)/timeStep;
      else if (comp == IPField::FP_ZZ) return logDF(2,2)/timeStep;
    }
    else
    {
      return (ipvcur->get(comp) - ipvprev->get(comp))/timeStep;
    }
  }
  else
  {
    Msg::Error("val type %d is not defined",vtype);
    Msg::Exit(0);
  }
  return 0.;
}


void computeMaterialLaw::getStress(std::vector<double>& Pstress){
    IPVariable* ipvcur = _ips->getState(IPStateBase::current);
    static STensor3 P;
    P(0,0) = ipvcur->get(IPField::P_XX);
    P(0,1) = ipvcur->get(IPField::P_XY);
    P(0,2) = ipvcur->get(IPField::P_XZ);
    P(1,0) = ipvcur->get(IPField::P_YX);
    P(1,1) = ipvcur->get(IPField::P_YY);
    P(1,2) = ipvcur->get(IPField::P_YZ);
    P(2,0) = ipvcur->get(IPField::P_ZX);
    P(2,1) = ipvcur->get(IPField::P_ZY);
    P(2,2) = ipvcur->get(IPField::P_ZZ);
    for(int i=0; i<3; i++){
      for(int j=0; j<3; j++){
        Pstress.push_back(P(i,j));
      }
    }
} 

void computeMaterialLaw::getDeformationGradient(std::vector<double>& Fstrain){
    IPVariable* ipvcur = _ips->getState(IPStateBase::current);
    static STensor3 F;
    F(0,0) = ipvcur->get(IPField::F_XX);
    F(0,1) = ipvcur->get(IPField::F_XY);
    F(0,2) = ipvcur->get(IPField::F_XZ);
    F(1,0) = ipvcur->get(IPField::F_YX);
    F(1,1) = ipvcur->get(IPField::F_YY);
    F(1,2) = ipvcur->get(IPField::F_YZ);
    F(2,0) = ipvcur->get(IPField::F_ZX);
    F(2,1) = ipvcur->get(IPField::F_ZY);
    F(2,2) = ipvcur->get(IPField::F_ZZ);
    for(int i=0; i<3; i++){
      for(int j=0; j<3; j++){
        Fstrain.push_back(F(i,j));
      }
    }
}   

void computeMaterialLaw::getParameterDerivative(std::vector< fullMatrix<double> >& dPdPn, std::vector< fullMatrix<double> >&dPdPv) const{
  const IPVariable* ipvcur = _ips->getState(IPStateBase::current);
  _materialLaw->getParameterDerivative(ipvcur, dPdPn, dPdPv);  
}

void computeMaterialLaw::reset_Parameter(dG3DMaterialLaw& mlaw, std::vector<std::vector<double>>& Para_Norm, std::vector<std::vector<double>>& Para_Wt, const double Vf)
{
  _materialLaw = &mlaw;
  if(Vf != 0.0){
    _materialLaw->reset_Parameter(Para_Norm, Para_Wt, Vf);
  }
  else{  
   _materialLaw->reset_Parameter(Para_Norm, Para_Wt);
  }
}

void computeMaterialLaw::reset_Parameter(dG3DMaterialLaw& mlaw, const char* Para){
   _materialLaw = &mlaw;
   _materialLaw->reset_Parameter(Para);
}   
   
   
   
