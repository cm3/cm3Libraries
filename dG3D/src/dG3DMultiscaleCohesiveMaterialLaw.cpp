//
// Author: Van Dung NGUYEN, (C) 2016
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "dG3DMultiscaleCohesiveMaterialLaw.h"
#include "math.h"
#include "nonLinearMechSolver.h"
#include "FractureCohesiveDG3DIPVariable.h"
#include "dG3DMultiscaleIPVariable.h"

GeneralMultiscaleBulkFollwedCohesive3DLaw::GeneralMultiscaleBulkFollwedCohesive3DLaw(const int num, const bool init):
	GeneralBulkFollwedCohesive3DLaw(num,init),_rotateRVEFollowingInterfaceNormal(false), _checkFailureOnsetWithInterfaceNormal(true),
_microBCFailure(NULL){

  };
GeneralMultiscaleBulkFollwedCohesive3DLaw::GeneralMultiscaleBulkFollwedCohesive3DLaw(const GeneralMultiscaleBulkFollwedCohesive3DLaw& src):
  GeneralBulkFollwedCohesive3DLaw(src),
	_rotateRVEFollowingInterfaceNormal(src._rotateRVEFollowingInterfaceNormal),
	_checkFailureOnsetWithInterfaceNormal(src._checkFailureOnsetWithInterfaceNormal){
	_microBCFailure = NULL;
	if (src._microBCFailure != NULL){
		_microBCFailure = src._microBCFailure->clone();
	}
}

void GeneralMultiscaleBulkFollwedCohesive3DLaw::addMicroBCFailure(const nonLinearMicroBC* mbc){
	if (_microBCFailure) delete _microBCFailure;
	_microBCFailure = mbc->clone();
};

bool GeneralMultiscaleBulkFollwedCohesive3DLaw::isSwitchedToFailureBC() const{
	if (_microBCFailure == NULL) return false;
	else return true;
};

GeneralMultiscaleBulkFollwedCohesive3DLaw::~GeneralMultiscaleBulkFollwedCohesive3DLaw(){
	if (_microBCFailure != NULL) delete _microBCFailure;
};

void dG3DMultiscaleCohesiveLaw::createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_,
                                          const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  IPVariable *ipvi = new BulkFollowedCohesive3DIPVariable();
  IPVariable *ipv1 = new BulkFollowedCohesive3DIPVariable();
  IPVariable *ipv2 = new BulkFollowedCohesive3DIPVariable();
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void dG3DMultiscaleCohesiveLaw::createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  ipv = new BulkFollowedCohesive3DIPVariable();
}

dG3DMultiscaleCohesiveLaw::dG3DMultiscaleCohesiveLaw(const int num):
			GeneralMultiscaleBulkFollwedCohesive3DLaw(num,true),
      _fixedCrackFace(0.),_fixedCrackFaceFlag(false)
{
};
dG3DMultiscaleCohesiveLaw::dG3DMultiscaleCohesiveLaw(const dG3DMultiscaleCohesiveLaw& src):
  GeneralMultiscaleBulkFollwedCohesive3DLaw(src),_fixedCrackFace(src._fixedCrackFace),_fixedCrackFaceFlag(src._fixedCrackFaceFlag){}


void dG3DMultiscaleCohesiveLaw::checkCohesiveInsertion(IPStateBase* ipsm, IPStateBase* ipsp, const bool forcedInsert) const{
// check failure onset from both negative and postive IPStates
	MultiscaleFractureCohesive3DIPVariable* fMinusCur = dynamic_cast<MultiscaleFractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::current));
	MultiscaleFractureCohesive3DIPVariable* fMinusPrev = dynamic_cast<MultiscaleFractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::previous));
	MultiscaleFractureCohesive3DIPVariable* fPlusCur = dynamic_cast<MultiscaleFractureCohesive3DIPVariable*>(ipsp->getState(IPStateBase::current));
	MultiscaleFractureCohesive3DIPVariable* fPlusPrev = dynamic_cast<MultiscaleFractureCohesive3DIPVariable*>(ipsp->getState(IPStateBase::previous));

	if (fMinusCur == NULL or fPlusCur == NULL or fMinusPrev==NULL or fPlusPrev==NULL){
		Msg::Error("In MultiscaleCohesive3DLaw::initCohesiveLaw, MultiscaleFractureCohesive3DIPVariable is not used ");
	}

	if (fMinusPrev->isbroken() or fPlusPrev->isbroken()){
		Msg::Warning("Previous IPs are broken");
		fMinusCur->broken();
		fPlusCur->broken();
		return;
	}

	const dG3DMultiscaleIPVariable* ipvBulkMinus = static_cast<const dG3DMultiscaleIPVariable*>(fMinusCur->getIPvBulk());
	const dG3DMultiscaleIPVariable* ipvBulkPlus = static_cast<const dG3DMultiscaleIPVariable*>(fPlusCur->getIPvBulk());

	fMinusCur->getOnsetCriterion() = ipvBulkMinus->getConstRefToLostEllipticityCriterion();
  fPlusCur->getOnsetCriterion() = ipvBulkPlus->getConstRefToLostEllipticityCriterion();

  if ((fMinusCur->getOnsetCriterion() >= 0.) or (fPlusCur->getOnsetCriterion() >=0.) or forcedInsert){
		printf("rank %d: cohesive element is inserted \n",Msg::GetCommRank());
    fMinusCur->broken();
    fPlusCur->broken();
    // broken is checked via bulk terial law, see function stress
    // initialize cohesive law in negative part
	  static SVector3 zeroVec;
	  static STensor3 zeroTen;

    static SVector3 normDir;
    normDir = fPlusCur->getConstRefToCurrentOutwardNormal();
    normDir.normalize();


    Cohesive3DIPVariableBase* coMinus = fMinusCur->getIPvFrac();
    coMinus->initializeFracture(fMinusCur->getConstRefToJump(),getKp(),0.,0.,0.,0.,0.,true,zeroTen,getKp(),normDir,zeroVec,
                              fMinusCur->getIPvBulk());
      // positive part take a same value as negative cohesive part
    Cohesive3DIPVariableBase* coPlus = fPlusCur->getIPvFrac();
    coPlus->initializeFracture(fPlusCur->getConstRefToJump(),getKp(),0.,0.,0.,0.,0.,true,zeroTen,getKp(),normDir,zeroVec,
                              fPlusCur->getIPvBulk());
  }
  else{
    fMinusCur->nobroken();
    fPlusCur->nobroken();
  }
};

void dG3DMultiscaleCohesiveLaw::transferInterfaceDataToBulk(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff) const{
	FractureCohesive3DIPVariable *fipv = static_cast < FractureCohesive3DIPVariable *> (ipv);
  const FractureCohesive3DIPVariable *fipvprev = static_cast < const FractureCohesive3DIPVariable *> (ipvprev);

  if (fipv->isbroken()){
    dG3DMultiscaleIPVariable* ipvMultiscale = dynamic_cast<dG3DMultiscaleIPVariable*>(fipv->getIPvBulk());
    const dG3DMultiscaleIPVariable* ipvMultiscaleprev = dynamic_cast<const dG3DMultiscaleIPVariable*>(fipvprev->getIPvBulk());

    BulkFollowedCohesive3DIPVariable* cohIpv = dynamic_cast<BulkFollowedCohesive3DIPVariable*>(fipv->getIPvFrac());
    const BulkFollowedCohesive3DIPVariable* cohIpvprev = dynamic_cast<const BulkFollowedCohesive3DIPVariable*>(fipvprev->getIPvFrac());

    nonLinearMechSolver* solver = NULL;
    if (ipvMultiscale!= NULL){
      solver = ipvMultiscale->getMicroSolver();
    }
 		else {
      Msg::Error("solver is not available MultiscaleDGCohesive3DLaw::transferInterfaceDataToBulk");
    }

    SVector3& ujumpDG = cohIpv->getRefToDGJump();
    ujumpDG = cohIpvprev->getConstRefToDGJump();

    // update bulk part
    const STensor3& Fb0 = fipvprev->getBulkDeformationGradient();
		const STensor3& Fb = fipv->getBulkDeformationGradient();

    STensor3& F = fipv->getRefToDeformationGradient();
		F = fipvprev->getConstRefToDeformationGradient();
    for (int i=0; i<3; i++){
			for (int j=0; j<3; j++){
        F(i,j) += Fb(i,j) - Fb0(i,j);
			}
		}

    static SVector3 refN;
    refN = fipv->getConstRefToReferenceOutwardNormal();
    refN.normalize();

		const SVector3& ujump = fipv->getConstRefToJump();
		const SVector3& ujump0 = fipvprev->getConstRefToJump();

		double GammaM = 0.;
    if (_fixedCrackFaceFlag){
      GammaM = _fixedCrackFace;
    }
    else{
      GammaM = solver->getHomogenizedCrackFace();
    }

		double V0 = solver->getRVEVolume();
		double fact = GammaM/V0;

		for (int i=0; i<3; i++){
			for (int j=0; j<3; j++){
        F(i,j) +=  fact*(ujump(i) - ujump0(i))*refN(j);
			}
		}

		if (stiff){
			STensor43& dFinterfacedF = fipv->getRefToDInterfaceDeformationGradientDDeformationGradient();
      STensor33& dFinterfacedJump = fipv->getRefToDInterfaceDeformationGradientDJump();

      STensorOperation::zero(dFinterfacedF);
      STensorOperation::zero(dFinterfacedJump);

      static STensor3 I(1.);
			for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){
            dFinterfacedJump(i,j,k) += I(i,k)*refN(j)*fact;
            for (int l=0; l<3; l++){
              dFinterfacedF(i,j,k,l) += I(i,k)*I(j,l);
            }
          }

        }
      }
		}
  }
};

void dG3DMultiscaleCohesiveLaw::stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff, const bool checkfrac, const bool dTangent){
  FractureCohesive3DIPVariable *fipv = static_cast < FractureCohesive3DIPVariable *> (ipv);
  const FractureCohesive3DIPVariable *fipvprev = static_cast < const FractureCohesive3DIPVariable *> (ipvprev);

  if (fipv->isbroken()){

    dG3DMultiscaleIPVariable* mipv = static_cast<dG3DMultiscaleIPVariable*>(fipv->getIPvBulk());
    const dG3DMultiscaleIPVariable* mipvprev = static_cast<const dG3DMultiscaleIPVariable*>(fipvprev->getIPvBulk());


    BulkFollowedCohesive3DIPVariable* cohmipv = dynamic_cast<BulkFollowedCohesive3DIPVariable*>(fipv->getIPvFrac());
    const BulkFollowedCohesive3DIPVariable* cohmipvprev = dynamic_cast<const BulkFollowedCohesive3DIPVariable*>(fipvprev->getIPvFrac());

    nonLinearMechSolver* solver = mipv->getMicroSolver();
    const homogenizedData* homoData = solver->getHomogenizationState(IPStateBase::current);
    const homogenizedData* homoDataPrev = solver->getHomogenizationState(IPStateBase::previous);


    static SVector3 refN;
    refN = fipv->getConstRefToReferenceOutwardNormal();
    refN.normalize();

    static SVector3 curN;
    curN = fipvprev->getConstRefToCurrentOutwardNormal();
    curN.normalize();

    const STensor3& P = fipv->getConstRefToFirstPiolaKirchhoffStress();
    SVector3& T = fipv->getRefToInterfaceForce();
    for (int i=0; i<3; i++) {
      T(i) = 0.;
      for (int j=0; j<3; j++){
        T(i) += P(i,j)*refN(j);
      }
    }

    const SVector3& ujump = fipv->getConstRefToJump();
    // add penalty to avoid penetration
    double realUJumpNormal = dot(ujump,curN);
    if (realUJumpNormal < 0){
      for (int i=0; i<3; i++){
				T(i) += _Kp*realUJumpNormal*curN(i);
			}
    }

    const double& lprev = homoDataPrev->getAverageLocalizationBandWidth();
		double& irr = fipv->getRefToIrreversibleEnergy();
		irr = homoData->getIrreversibleEnergy()*lprev;

    if (stiff){

			const STensor43& dFinterfacedF = fipv->getConstRefToDInterfaceDeformationGradientDDeformationGradient();
			const STensor33& dFinterfacedJump = fipv->getConstRefToDInterfaceDeformationGradientDJump();

      const STensor43& dPdFinf = homoData->getHomogenizedTangentOperator_F_F();
      STensor33& dTdF = fipv->getRefToDInterfaceForceDDeformationGradient();
      STensor3& dTdjump = fipv->getRefToDInterfaceForceDjump();

      STensorOperation::zero(dTdF);
      STensorOperation::zero(dTdjump);

			for (int i=0; i<3; i++){
				for (int j=0; j<3; j++){
					for (int p=0; p<3; p++){
						for (int q=0; q<3; q++){
							for (int k=0; k<3; k++){
								dTdjump(i,k) += dPdFinf(i,j,p,q)*refN(j)*dFinterfacedJump(p,q,k);
								for (int l=0; l<3; l++){
									dTdF(i,k,l) += dPdFinf(i,j,p,q)*refN(j)*dFinterfacedF(p,q,k,l);
								}
							}
						}
					}
				}
			}

			if (realUJumpNormal < 0){
				for (int i=0; i<3; i++){
					for (int j=0; j<3; j++){
						dTdjump(i,j) += _Kp*curN(i)*curN(j);
					};
				}
			};

			if (homoData->getIrreversibleEnergyExtractionFlag()){

				STensor3& dirrEnergDF = fipv->getRefToDIrreversibleEnergyDDeformationGradient();
				SVector3& dirrEnergDJump = fipv->getRefToDIrreversibleEnergyDJump();

        STensorOperation::zero(dirrEnergDF);
        STensorOperation::zero(dirrEnergDJump);

				const STensor3& dirrEnergdFinf = homoData->getHomogenizedTangentOperator_IrreversibleEnergy_F();
				for (int i=0; i<3; i++){
					for (int j=0; j<3; j++){
						for (int k=0; k<3; k++){
							dirrEnergDJump(k) += lprev*dirrEnergdFinf(i,j)*dFinterfacedJump(i,j,k);
							for (int l=0; l<3; l++){
								dirrEnergDF(k,l) += lprev*dirrEnergdFinf(i,j)*dFinterfacedF(i,j,k,l);
							}
						}

					}
				}
			}

    }
  }
};


TwoFieldMultiscaleCohesive3DLaw::TwoFieldMultiscaleCohesive3DLaw(const int num):
			GeneralMultiscaleBulkFollwedCohesive3DLaw(num,true), _fixedCrackFace(0.),_fixedCrackFaceFlag(false){};
TwoFieldMultiscaleCohesive3DLaw::TwoFieldMultiscaleCohesive3DLaw(const TwoFieldMultiscaleCohesive3DLaw& src):
  GeneralMultiscaleBulkFollwedCohesive3DLaw(src) ,_fixedCrackFace(src._fixedCrackFace),_fixedCrackFaceFlag(src._fixedCrackFaceFlag){}

void TwoFieldMultiscaleCohesive3DLaw::createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_,
                                          const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  IPVariable *ipvi = new TwoFieldBulkFollowedCohesive3DIPVariable();
  IPVariable *ipv1 = new TwoFieldBulkFollowedCohesive3DIPVariable();
  IPVariable *ipv2 = new TwoFieldBulkFollowedCohesive3DIPVariable();
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void TwoFieldMultiscaleCohesive3DLaw::createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  ipv = new TwoFieldBulkFollowedCohesive3DIPVariable();
}

void TwoFieldMultiscaleCohesive3DLaw::transferInterfaceDataToBulk(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff) const{
  // do nothing
};


void TwoFieldMultiscaleCohesive3DLaw::stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff, const bool checkfrac, const bool dTangent){
  FractureCohesive3DIPVariable *fipv = static_cast < FractureCohesive3DIPVariable *> (ipv);
  const FractureCohesive3DIPVariable *fipvprev = static_cast < const FractureCohesive3DIPVariable *> (ipvprev);

  if (fipv->isbroken()){
    dG3DMultiscaleIPVariable* ipvMultiscale = static_cast<dG3DMultiscaleIPVariable*>(fipv->getIPvBulk());
    const dG3DMultiscaleIPVariable* ipvMultiscalePrev = static_cast<const dG3DMultiscaleIPVariable*>(fipvprev->getIPvBulk());


    TwoFieldBulkFollowedCohesive3DIPVariable* cohmipv = dynamic_cast<TwoFieldBulkFollowedCohesive3DIPVariable*>(fipv->getIPvFrac());
    const TwoFieldBulkFollowedCohesive3DIPVariable* cohmipvprev = dynamic_cast<const TwoFieldBulkFollowedCohesive3DIPVariable*>(fipvprev->getIPvFrac());

    nonLinearMechSolver* solver = NULL;
    if (ipvMultiscale!= NULL){
      solver = ipvMultiscale->getMicroSolver();
    }
 		else {
      Msg::Error("solver is not available MultiscaleDGCohesive3DLaw::transferInterfaceDataToBulk");
    }

    static SVector3 refN;
    refN = fipv->getConstRefToReferenceOutwardNormal();
    refN.normalize();

    static SVector3 curN;
    curN = fipvprev->getConstRefToCurrentOutwardNormal();
    curN.normalize();

    // cohesive traction
    SVector3& T = fipv->getRefToInterfaceForce();
    const STensor3& P = fipv->getConstRefToFirstPiolaKirchhoffStress();
    for (int i=0; i<3; i++) {
      T(i) = 0.;
      for (int j=0; j<3; j++){
        T(i) += P(i,j)*refN(j);
      }
    }

    // cohesive jump

    double GammaM = 0.;
    if (_fixedCrackFaceFlag){
      GammaM = _fixedCrackFace;
    }
    else{
      GammaM = solver->getHomogenizedCrackFace();
    }

		double V0 = solver->getRVEVolume();
		double fact = V0/GammaM;

    SVector3& cjump = fipv->getRefToCohesiveJump();
    cjump = fipvprev->getConstRefToCohesiveJump();

    const SVector3& iJumpCur = fipv->getConstRefToIncompatibleJump();
    const SVector3& iJumpPrev = fipvprev->getConstRefToIncompatibleJump();

    for (int i=0; i<3; i++){
      cjump(i) += (iJumpCur(i) - iJumpPrev(i))*fact;
    }

    double normalCjump = dot(cjump,curN);
    if (normalCjump < 0){
      for (int i=0; i<3; i++){
        cjump(i) -= (normalCjump*curN(i));
				T(i) += this->getKp()*normalCjump*curN(i);
			}
    }

    if (stiff){
      // Finterface = F+ inS*N
      const STensor43& H = fipv->getConstRefToTangentModuli();

      STensor33& dTdF = fipv->getRefToDInterfaceForceDDeformationGradient();
      STensor3& dTdInjump = fipv->getRefToDInterfaceForceDIncompatibleJump();

      STensorOperation::zero(dTdF);
      STensorOperation::zero(dTdInjump);

			for (int i=0; i<3; i++){
				for (int j=0; j<3; j++){
					for (int p=0; p<3; p++){
						for (int q=0; q<3; q++){
              dTdF(i,p,q) += H(i,j,p,q)*refN(j);
              dTdInjump(i,p) += H(i,j,p,q)*refN(j)*refN(q);
						}
					}
				}
			}

			if (normalCjump < 0){
				for (int i=0; i<3; i++){
					for (int j=0; j<3; j++){
						dTdInjump(i,j) += this->getKp()*curN(i)*curN(j);
					};
				}
			};

      STensor3& dcjumpDinJump = fipv->getRefToDCohesiveJumpDIncompatibleJump();
      STensorOperation::unity(dcjumpDinJump);
      if (normalCjump < 0.){
        for (int i=0; i<3; i++){
					for (int j=0; j<3; j++){
						dcjumpDinJump(i,j) -= curN(i)*curN(j);
					};
				}
      }
      dcjumpDinJump *= fact;
    }
  }
};
void TwoFieldMultiscaleCohesive3DLaw::checkCohesiveInsertion(IPStateBase* ipsm, IPStateBase* ipsp, const bool forcedInsert) const{
// check failure onset from both negative and postive IPStates
	MultiscaleFractureCohesive3DIPVariable* fMinusCur = dynamic_cast<MultiscaleFractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::current));
	MultiscaleFractureCohesive3DIPVariable* fMinusPrev = dynamic_cast<MultiscaleFractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::previous));
	MultiscaleFractureCohesive3DIPVariable* fPlusCur = dynamic_cast<MultiscaleFractureCohesive3DIPVariable*>(ipsp->getState(IPStateBase::current));
	MultiscaleFractureCohesive3DIPVariable* fPlusPrev = dynamic_cast<MultiscaleFractureCohesive3DIPVariable*>(ipsp->getState(IPStateBase::previous));

	if (fMinusCur == NULL or fPlusCur == NULL or fMinusPrev==NULL or fPlusPrev==NULL){
		Msg::Error("In MultiscaleCohesive3DLaw::initCohesiveLaw, MultiscaleFractureCohesive3DIPVariable is not used ");
	}

	if (fMinusPrev->isbroken() or fPlusPrev->isbroken()){
		Msg::Warning("Previous IPs are broken");
		fMinusCur->broken();
		fPlusCur->broken();
		return;
	}

	const dG3DMultiscaleIPVariable* ipvBulkMinus = static_cast<const dG3DMultiscaleIPVariable*>(fMinusCur->getIPvBulk());
	const dG3DMultiscaleIPVariable* ipvBulkPlus = static_cast<const dG3DMultiscaleIPVariable*>(fPlusCur->getIPvBulk());


	fMinusCur->getOnsetCriterion() = ipvBulkMinus->getConstRefToLostEllipticityCriterion();
  fPlusCur->getOnsetCriterion() = ipvBulkPlus->getConstRefToLostEllipticityCriterion();

  if ((fMinusCur->getOnsetCriterion() >=0.) or (fPlusCur->getOnsetCriterion()>=0.) or forcedInsert){
		printf("rank %d: cohesive element is inserted \n",Msg::GetCommRank());
    fMinusCur->broken();
    fPlusCur->broken();
    // broken is checked via bulk terial law, see function stress
    // initialize cohesive law in negative part

    static SVector3 zeroVec(0.);
    static STensor3 zeroTen(0.);

    static SVector3 normDir;
    normDir = fPlusCur->getConstRefToCurrentOutwardNormal();
    normDir.normalize();

    Cohesive3DIPVariableBase* coMinus = fMinusCur->getIPvFrac();
    coMinus->initializeFracture(fMinusCur->getConstRefToJump(),getKp(),0.,0.,0.,0.,0.,true,zeroTen,getKp(),normDir,zeroVec,
                              fMinusCur->getIPvBulk());
      // positive part take a same value as negative cohesive part
    Cohesive3DIPVariableBase* coPlus = fPlusCur->getIPvFrac();
    coPlus->initializeFracture(fPlusCur->getConstRefToJump(),getKp(),0.,0.,0.,0.,0.,true,zeroTen,getKp(),normDir,zeroVec,
                              fPlusCur->getIPvBulk());

  }
  else{
    fMinusCur->nobroken();
    fPlusCur->nobroken();
  }
};

MultiscaleAdhesive3DLaw::MultiscaleAdhesive3DLaw(const int num):GeneralMultiscaleBulkFollwedCohesive3DLaw(num,true){};
MultiscaleAdhesive3DLaw::MultiscaleAdhesive3DLaw(const MultiscaleAdhesive3DLaw& src):
  GeneralMultiscaleBulkFollwedCohesive3DLaw(src){}

void MultiscaleAdhesive3DLaw::createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_,
                                          const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  IPVariable *ipvi = new BulkFollowedCohesive3DIPVariable();
  IPVariable *ipv1 = new BulkFollowedCohesive3DIPVariable();
  IPVariable *ipv2 = new BulkFollowedCohesive3DIPVariable();
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void MultiscaleAdhesive3DLaw::createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  ipv = new BulkFollowedCohesive3DIPVariable();
}

void MultiscaleAdhesive3DLaw::transferInterfaceDataToBulk(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff) const{
	// fracture ipv
  FractureCohesive3DIPVariable *fipv = static_cast < FractureCohesive3DIPVariable *> (ipv);
  const FractureCohesive3DIPVariable *fipvprev = static_cast < const FractureCohesive3DIPVariable *> (ipvprev);

  if (fipv->isbroken()){
     //
    dG3DMultiscaleIPVariable* ipvMultiscale = static_cast<dG3DMultiscaleIPVariable*>(fipv->getIPvBulk());
    const dG3DMultiscaleIPVariable* ipvMultiscaleprev = static_cast<const dG3DMultiscaleIPVariable*>(fipvprev->getIPvBulk());
    // cohesive IP
    BulkFollowedCohesive3DIPVariable* cohIpv = static_cast<BulkFollowedCohesive3DIPVariable*>(fipv->getIPvFrac());
    const BulkFollowedCohesive3DIPVariable* cohIpvprev = static_cast<const BulkFollowedCohesive3DIPVariable*>(fipvprev->getIPvFrac());

    nonLinearMechSolver* solver = NULL;
    if (ipvMultiscale!= NULL){
      solver = ipvMultiscale->getMicroSolver();
    }

    if (solver == NULL){
      Msg::Error("solver is not available MultiscaleDGCohesive3DLaw::transferInterfaceDataToBulk");
    }

    // update
    cohIpv->getRefToBulkDeformationGradientAtFailureOnset() = cohIpvprev->getConstRefToBulkDeformationGradientAtFailureOnset();
    cohIpv->getRefToDGJump() = cohIpvprev->getConstRefToDGJump();

    // update deformation with displacement jump
    const STensor3& Fb0 = fipv->getConstRefToBulkDeformationGradientAtFailureOnset();
    // get bulk deformation from total deformation gradient
    const STensor3& Fb = fipv->getBulkDeformationGradient();
    //
    static SVector3 refN;
    refN = fipv->getConstRefToReferenceOutwardNormal();
    refN.normalize();
    //
    const SVector3& ujump = fipv->getConstRefToJump();
    const SVector3& ujump0 = fipv->getConstRefToDGJump();

    double L = this->getCharacteristicLength();

    // update interface strain by eliminating normal part
    STensor3& Finterface = fipv->getRefToDeformationGradient();
    Finterface = Fb;
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        Finterface(i,j) += (ujump(i) - ujump0(i))*refN(j)/L; // update with disp jump
        for (int k=0; k<3; k++){
          Finterface(i,j) += (Fb0(i,k)-Fb(i,k))*refN(k)*refN(j); // eliminate normal part
        }
      }
    }


    if (stiff){
      STensor43& dFinterfacedF = fipv->getRefToDInterfaceDeformationGradientDDeformationGradient();
      STensor33& dFinterfacedJump = fipv->getRefToDInterfaceDeformationGradientDJump();

      STensorOperation::zero(dFinterfacedF);
      STensorOperation::zero(dFinterfacedJump);

      static STensor3 I(1.);

      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){
            dFinterfacedJump(i,j,k) += I(i,k)*refN(j)/_L;
            for (int l=0; l<3; l++){
              dFinterfacedF(i,j,k,l) += I(i,k)*I(j,l) - I(i,k)*refN(j)*refN(l);
            }
          }
        }
      }
    }
  }
  else{
    Msg::Error("interface IP is broken at the beginning of the simulation by dgPartDomain::breakAllInterfaceIPsFromBeginning(True)");
  }


};


void MultiscaleAdhesive3DLaw::stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff, const bool checkfrac, const bool dTangent){
  FractureCohesive3DIPVariable *fipv = static_cast < FractureCohesive3DIPVariable *> (ipv);
  const FractureCohesive3DIPVariable *fipvprev = static_cast < const FractureCohesive3DIPVariable *> (ipvprev);

  if (fipv->isbroken()){
    dG3DMultiscaleIPVariable* mipv = static_cast<dG3DMultiscaleIPVariable*>(fipv->getIPvBulk());
    const dG3DMultiscaleIPVariable* mipvprev = static_cast<const dG3DMultiscaleIPVariable*>(fipvprev->getIPvBulk());
    BulkFollowedCohesive3DIPVariable* cohmipv = dynamic_cast<BulkFollowedCohesive3DIPVariable*>(fipv->getIPvFrac());
    const BulkFollowedCohesive3DIPVariable* cohmipvprev = dynamic_cast<const BulkFollowedCohesive3DIPVariable*>(fipvprev->getIPvFrac());

    nonLinearMechSolver* solver = mipv->getMicroSolver();
    homogenizedData* homoData = solver->getHomogenizationState(IPStateBase::current);

    const STensor3& P = fipv->getConstRefToFirstPiolaKirchhoffStress();
    // cohesive traction
    SVector3& T = fipv->getRefToInterfaceForce();

    static SVector3 refNorm;
    refNorm = fipv->getConstRefToReferenceOutwardNormal();
    refNorm.normalize();

    for (int i=0; i<3; i++){
      T(i) = 0.;
      for (int j=0; j<3; j++){
        T(i) += P(i,j)*refNorm(j);
      }
    }

    static SVector3 curN;
    curN = fipvprev->getConstRefToCurrentOutwardNormal();
    curN.normalize();
    const SVector3& ujump = fipv->getConstRefToJump();
    // add penalty to avoid penetration
    double realUJumpNormal = dot(ujump,curN);
    if (realUJumpNormal < 0){
      for (int i=0; i<3; i++){
				T(i) += this->getKp()*realUJumpNormal*curN(i);
			}
    }

    if (stiff){
      const STensor43& dFinterfacedF = fipv->getConstRefToDInterfaceDeformationGradientDDeformationGradient();
      const STensor33& dFinterfacedJump = fipv->getConstRefToDInterfaceDeformationGradientDJump();

      const STensor43& dPdFinterface = homoData->getHomogenizedTangentOperator_F_F();

      STensor33& dTdF = fipv->getRefToDInterfaceForceDDeformationGradient();
      STensor3& dTdJump = fipv->getRefToDInterfaceForceDjump();

      STensorOperation::zero(dTdF);
      STensorOperation::zero(dTdJump);

      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int p=0; p<3; p++){
            for (int q=0; q<3; q++){
              for (int k=0; k<3; k++){
                dTdJump(i,k) += dPdFinterface(i,j,p,q)*refNorm(j)*dFinterfacedJump(p,q,k);
                for (int l=0; l<3; l++){
                  dTdF(i,k,l) += dPdFinterface(i,j,p,q)*refNorm(j)*dFinterfacedF(p,q,k,l);
                }
              }
            }
          }
        }
      }

      if (realUJumpNormal < 0){
				for (int i=0; i<3; i++){
					for (int j=0; j<3; j++){
						dTdJump(i,j) += this->getKp()*curN(i)*curN(j);
					};
				}
			}
    }
  }

};

void MultiscaleAdhesive3DLaw::checkCohesiveInsertion(IPStateBase* ipsm, IPStateBase* ipsp, const bool forcedInsert) const{
	MultiscaleFractureCohesive3DIPVariable* fMinusCur = dynamic_cast<MultiscaleFractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::current));
	const MultiscaleFractureCohesive3DIPVariable* fMinusPrev = dynamic_cast<const MultiscaleFractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::previous));
	MultiscaleFractureCohesive3DIPVariable* fPlusCur = dynamic_cast<MultiscaleFractureCohesive3DIPVariable*>(ipsp->getState(IPStateBase::current));
	const MultiscaleFractureCohesive3DIPVariable* fPlusPrev = dynamic_cast<const MultiscaleFractureCohesive3DIPVariable*>(ipsp->getState(IPStateBase::previous));

	if (fMinusCur == NULL or fPlusCur == NULL or fMinusPrev==NULL or fPlusPrev==NULL){
		Msg::Error("In MultiscaleCohesive3DLaw::initCohesiveLaw, MultiscaleFractureCohesive3DIPVariable is not used ");
	}

	if (!fMinusPrev->isbroken() or !fPlusPrev->isbroken() or !fMinusCur->isbroken() or !fPlusCur->isbroken()){
		Msg::Error("all IP variables must be broken at the begining in MultiscaleAdhesive3DLaw::checkCohesiveInsertion");
	}
};
