	//
//
// Description: Class of materials for non linear dg
//
//
// Author:  <L. Noels>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef _NONLOCALDAMAGEDG3DMATERIALLAW_H_
#define _NONLOCALDAMAGEDG3DMATERIALLAW_H_

#include "dG3DMaterialLaw.h"
#include "scalarFunction.h"
#ifndef SWIG
#include "NucleationLaw.h"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include "mlawHyperelasticDamage.h"
#include "mlawNonLocalDamage.h"
#include "mlawNonLocalDamage_Stoch.h"
#include "nonLocalDamageLaw.h"
#include "mlawNonLocalDamageJ2Hyper.h"
#include "mlawNonLocalDamageJ2SmallStrain.h"
#include "mlawNonLocalDamageIsotropicElasticity.h"
#include "mlawNonLocalDamageGurson.h"
#include "mlawNonLocalPorousCoalescence.h"
#include "mlawNonLocalPorousCoupled.h"
#include "mlawNonLocalPorousWithFailure.h"
#include "mlawNonLocalDamageJ2FullyCoupledThermoMechanics.h"
#include "mlawGenericCrackPhaseField.h"
#include "nonLocalDamageDG3DIPVariable.h"
#endif

class virtualNonLocalDamageDG3DMaterialLaw : public dG3DMaterialLaw{
  protected:
    #ifndef SWIG
    dG3DMaterialLaw* _lawBase;
    int numNonlocalVar;
    std::vector<STensor3> characteristicLength;
    #endif //SWIG

  public:
    virtualNonLocalDamageDG3DMaterialLaw(const int num, const dG3DMaterialLaw& base, const int numNLVariable);
    void setNonLocalLengthScale(const int index, const CLengthLaw &cLLaw);
    #ifndef SWIG
    virtualNonLocalDamageDG3DMaterialLaw(const virtualNonLocalDamageDG3DMaterialLaw &source);
    virtual ~virtualNonLocalDamageDG3DMaterialLaw();
    // set the time of _nldlaw
    virtual void setTime(const double t,const double dtime){
      dG3DMaterialLaw::setTime(t,dtime);
      if (_lawBase){
        _lawBase->setTime(t,dtime);
      }
    };
    virtual materialLaw::matname getType() const {return _lawBase->getType();};

    virtual void createIPState(IPStateBase* &ips,bool hasBodyForce,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPVariable* &ipv,bool hasBodyForce,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){ _lawBase->initLaws(maplaw);};
    virtual double soundSpeed() const{return _lawBase->soundSpeed();};
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const;
    virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent =false);
    virtual double scaleFactor() const{return _lawBase->scaleFactor();};
    virtual bool isHighOrder() const { return _lawBase->isHighOrder();};
    virtual bool isNumeric() const {return _lawBase->isNumeric();};
    virtual int getNumberOfExtraDofsDiffusion() const {return _lawBase->getNumberOfExtraDofsDiffusion();}
    virtual double getInitialExtraDofStoredEnergyPerUnitField() const {return _lawBase->getInitialExtraDofStoredEnergyPerUnitField();}
    virtual double getExtraDofStoredEnergyPerUnitField(double T) const {return _lawBase->getExtraDofStoredEnergyPerUnitField(T);}
    virtual double getCharacteristicLength() const {return _lawBase->getCharacteristicLength();};
    virtual materialLaw* clone() const{ return new virtualNonLocalDamageDG3DMaterialLaw(*this);};
    virtual bool withEnergyDissipation() const {return _lawBase->withEnergyDissipation();};
    virtual void setMacroSolver(const nonLinearMechSolver* sv){
      dG3DMaterialLaw::setMacroSolver(sv);
      if (_lawBase != NULL){
        _lawBase->setMacroSolver(sv);
      }
    }
    virtual bool brokenCheck(const IPVariable* ipv) const{return _lawBase->brokenCheck(ipv);};
    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
      return _lawBase->getConstNonLinearSolverMaterialLaw();
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
      return _lawBase->getNonLinearSolverMaterialLaw();
    }
    #endif
};

class NonLocalDamageDG3DMaterialLaw : public dG3DMaterialLaw{
  protected:
    #ifndef SWIG
    mlawNonLocalDamage *_nldlaw; // pointer to allow to choose between LLN style or Gmsh Style. The choice is performed by the constructor (2 constructors with != arguments)
    #endif //SWIG

  public:
    NonLocalDamageDG3DMaterialLaw(const int num, const double rho,
                                const char *propName);
    void setNumberOfNonLocalVariables(int nb) {_nldlaw->setNumberOfNonLocalVariables(nb);}
    void setInitialDmax_Inc(double d) {_nldlaw->setInitialDmax_Inc(d);};
    void setInitialDmax_Mtx(double d) {_nldlaw->setInitialDmax_Mtx(d);};
    void setInitialDmax(double d) {_nldlaw->setInitialDmax(d);};

    #ifndef SWIG
    NonLocalDamageDG3DMaterialLaw(const NonLocalDamageDG3DMaterialLaw &source);
    virtual ~NonLocalDamageDG3DMaterialLaw();

    // set the time of _nldlaw
    virtual void setTime(const double t,const double dtime);
    virtual materialLaw::matname getType() const;
    virtual void createIPState(IPStateBase* &ips, bool hasBodyForce,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPVariable* &ipv,bool hasBodyForce,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
    virtual double soundSpeed() const;// or change value ??
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const;
    virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent =false);
    virtual double scaleFactor() const;
    virtual materialLaw* clone() const{return new NonLocalDamageDG3DMaterialLaw(*this);};
    virtual bool withEnergyDissipation() const;
    virtual void setMacroSolver(const nonLinearMechSolver* sv);
    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
      return _nldlaw;
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
      return _nldlaw;
    }
    #endif
};


class NonLocalDamageDG3DMaterialLaw_Stoch : public NonLocalDamageDG3DMaterialLaw{
  public:
    NonLocalDamageDG3DMaterialLaw_Stoch(const int num, const double rho, const char *propName,
                        const double Ori_x, const double Ori_y, const double Ori_z,
                        const double Lx,const double Ly,const double Lz, 
                        const double dx,const double dy, const double dz, const double euler0= 0.0, const double euler1= 0.0, 
                        const double euler2= 0.0, const char *Geo=NULL, const char *RandProp=NULL);

    NonLocalDamageDG3DMaterialLaw_Stoch(const int num, const double rho, const char *propName,
                        const double Ori_x, const double Ori_y, const double Ori_z,
                        const double Lx,const double Ly,const double Lz,
                        const char *RandProp, const int intpl);


    #ifndef SWIG
    NonLocalDamageDG3DMaterialLaw_Stoch(const NonLocalDamageDG3DMaterialLaw_Stoch &source);
    virtual ~NonLocalDamageDG3DMaterialLaw_Stoch();
    virtual void createIPState(IPStateBase* &ips,bool hasBodyForce,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPVariable* &ipv,bool hasBodyForce,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
    virtual materialLaw* clone() const{return new NonLocalDamageDG3DMaterialLaw_Stoch(*this);};
    #endif
};


class NonLocalDamageHyperelasticDG3DMaterialLaw: public dG3DMaterialLaw{
  protected:
    #ifndef SWIG
		mlawNonlocalDamageHyperelastic* _nlLaw;
    #endif

  public:
		NonLocalDamageHyperelasticDG3DMaterialLaw(const int num, const double rho, const elasticPotential& elP, const CLengthLaw &cLLaw,
                                         const DamageLaw &damLaw);
		#ifndef SWIG
		NonLocalDamageHyperelasticDG3DMaterialLaw(const NonLocalDamageHyperelasticDG3DMaterialLaw& src);
		virtual ~NonLocalDamageHyperelasticDG3DMaterialLaw(){
      if (_nlLaw) delete _nlLaw;
    };
		virtual void setTime(const double t,const double dtime);
    virtual materialLaw::matname getType() const {return _nlLaw->getType();};

    virtual void createIPState(IPStateBase* &ips,bool hasBodyForce,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPVariable* &ipv,bool hasBodyForce,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw) {}
    virtual double soundSpeed() const {return _nlLaw->soundSpeed();};
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const;
    virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent =false);
		virtual double scaleFactor() const {return _nlLaw->scaleFactor();}
		virtual materialLaw* clone() const{return new NonLocalDamageHyperelasticDG3DMaterialLaw(*this);};
    virtual bool withEnergyDissipation() const {return _nlLaw->withEnergyDissipation();};
		virtual void setMacroSolver(const nonLinearMechSolver* sv){
			dG3DMaterialLaw::setMacroSolver(sv);
			_nlLaw->setMacroSolver(sv);
		};
    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
      return _nlLaw;
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
      return _nlLaw;
    }
		#endif
};

class NonLocalAnisotropicDamageHyperelasticDG3DMaterialLaw: public NonLocalDamageHyperelasticDG3DMaterialLaw{
  public:
		NonLocalAnisotropicDamageHyperelasticDG3DMaterialLaw(const int num, const double rho,
                                        const elasticPotential& elP, const CLengthLaw &cLLaw,
                                         const DamageLaw &damLaw);
		#ifndef SWIG
		NonLocalAnisotropicDamageHyperelasticDG3DMaterialLaw(const NonLocalAnisotropicDamageHyperelasticDG3DMaterialLaw& src): NonLocalDamageHyperelasticDG3DMaterialLaw(src){};
		virtual ~NonLocalAnisotropicDamageHyperelasticDG3DMaterialLaw(){
    };
    #endif //SWIG
};

class NonLocalDamageIsotropicElasticityDG3DMaterialLaw : public dG3DMaterialLaw
{
  protected:
    #ifndef SWIG
    mlawNonLocalDamageIsotropicElasticity * _nldIsotropicElasticitylaw;
    #endif //SWIG

  public:
    NonLocalDamageIsotropicElasticityDG3DMaterialLaw(const int num,const double rho,
                                         double E,const double nu, const CLengthLaw &cLLaw,
                                         const DamageLaw &damLaw);
    //  specific functions
    virtual void setNonLocalLimitingMethod(const int num);
    #ifndef SWIG
    NonLocalDamageIsotropicElasticityDG3DMaterialLaw(const NonLocalDamageIsotropicElasticityDG3DMaterialLaw &source);
    virtual ~NonLocalDamageIsotropicElasticityDG3DMaterialLaw();
    virtual void setTime(const double t,const double dtime);
    virtual materialLaw::matname getType() const;

    virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw) {}
    virtual double soundSpeed() const;
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const;
    virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent =false);
		virtual double scaleFactor() const {return _nldIsotropicElasticitylaw->scaleFactor();}
		virtual materialLaw* clone() const{return new NonLocalDamageIsotropicElasticityDG3DMaterialLaw(*this);};
    virtual bool withEnergyDissipation() const {return _nldIsotropicElasticitylaw->withEnergyDissipation();};
		virtual void setMacroSolver(const nonLinearMechSolver* sv){
			dG3DMaterialLaw::setMacroSolver(sv);
			if (_nldIsotropicElasticitylaw != NULL){
				_nldIsotropicElasticitylaw->setMacroSolver(sv);
			}
		};
    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
      return _nldIsotropicElasticitylaw;
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
      return _nldIsotropicElasticitylaw;
    }
    #endif // SWIG
};

class NonLocalDamageJ2SmallStrainDG3DMaterialLaw : public dG3DMaterialLaw
{
  protected:
    #ifndef SWIG
    mlawNonLocalDamageJ2SmallStrain *_nldJ2smallStrain;
    #endif //SWIG
  public:
    NonLocalDamageJ2SmallStrainDG3DMaterialLaw(const int num,const double rho,
                   double E,const double nu,const J2IsotropicHardening &_j2IH,
                   const CLengthLaw &_cLLaw,const DamageLaw &_damLaw,const double tol=1.e-6,
                   const bool matrixBypert = false, const double tolpert=1e-8);
    #ifndef SWIG
    NonLocalDamageJ2SmallStrainDG3DMaterialLaw(const NonLocalDamageJ2SmallStrainDG3DMaterialLaw &source);
    virtual ~NonLocalDamageJ2SmallStrainDG3DMaterialLaw();
    // set the time of _nldlaw
    virtual void setTime(const double t,const double dtime);
    virtual materialLaw::matname getType() const;

    virtual void createIPState(IPStateBase* &ips,bool hasBodyForce,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPVariable* &ipv,bool hasBodyForce,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
    virtual double soundSpeed() const;
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const;
    virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent =false);
    virtual double scaleFactor() const{return _nldJ2smallStrain->scaleFactor();};
    virtual materialLaw* clone() const{ return new NonLocalDamageJ2SmallStrainDG3DMaterialLaw(*this);};
    virtual bool withEnergyDissipation() const {return _nldJ2smallStrain->withEnergyDissipation();};
    virtual void setMacroSolver(const nonLinearMechSolver* sv){
      dG3DMaterialLaw::setMacroSolver(sv);
      if (_nldJ2smallStrain != NULL){
        _nldJ2smallStrain->setMacroSolver(sv);
      }
    }
    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
      return _nldJ2smallStrain;
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
      return _nldJ2smallStrain;
    }
    #endif
};


class NonLocalDamageJ2HyperDG3DMaterialLaw : public dG3DMaterialLaw
{
  protected:
    #ifndef SWIG
    mlawNonLocalDamageJ2Hyper *_nldJ2Hyperlaw;
    #endif //SWIG
  public:
    NonLocalDamageJ2HyperDG3DMaterialLaw(const int num,const double rho,
                   double E,const double nu,const J2IsotropicHardening &_j2IH,
                   const CLengthLaw &_cLLaw,const DamageLaw &_damLaw,const double tol=1.e-6,
                   const bool matrixBypert = false, const double tolpert=1e-8);
    void setStrainOrder(const int order);
    #ifndef SWIG
    NonLocalDamageJ2HyperDG3DMaterialLaw(const NonLocalDamageJ2HyperDG3DMaterialLaw &source);
    virtual ~NonLocalDamageJ2HyperDG3DMaterialLaw();
    // set the time of _nldlaw
    virtual void setTime(const double t,const double dtime);
    virtual materialLaw::matname getType() const;

    virtual void createIPState(IPStateBase* &ips,bool hasBodyForce,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
    virtual double soundSpeed() const;
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const;
    virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent =false);
    virtual double scaleFactor() const{return _nldJ2Hyperlaw->scaleFactor();};
    virtual materialLaw* clone() const{ return new NonLocalDamageJ2HyperDG3DMaterialLaw(*this);};
    virtual bool withEnergyDissipation() const {return _nldJ2Hyperlaw->withEnergyDissipation();};
    virtual void setMacroSolver(const nonLinearMechSolver* sv){
      dG3DMaterialLaw::setMacroSolver(sv);
      if (_nldJ2Hyperlaw != NULL){
        _nldJ2Hyperlaw->setMacroSolver(sv);
      }
    }
    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
      return _nldJ2Hyperlaw;
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
      return _nldJ2Hyperlaw;
    }
    #endif
};

class NonLocalPorousDuctileDamageDG3DMaterialLaw : public dG3DMaterialLaw{
  protected:
    #ifndef SWIG
    mlawNonLocalPorosity *_nldPorous; // pointer to allow to choose between LLN style or Gmsh Style. The choice is performed by the constructor (2 constructors with != arguments)
    std::map<int, double> _imposedInitialPorosity;
    STensor3              linearK;
    STensor3		  Stiff_alphaDilatation;
    bool _useI1J2J3Implementation;
    bool _tangentByPerturbationDuringCoalescence;
    double _pertFactor;
    #endif //SWIG

  public:
    #ifndef SWIG
    NonLocalPorousDuctileDamageDG3DMaterialLaw(const int num, const double rho, const bool init = true);
    NonLocalPorousDuctileDamageDG3DMaterialLaw(const NonLocalPorousDuctileDamageDG3DMaterialLaw &source);
    virtual ~NonLocalPorousDuctileDamageDG3DMaterialLaw();

    mlawNonLocalPorosity* getNonLocalPorosityLaw() {return _nldPorous;};
    const mlawNonLocalPorosity* getNonLocalPorosityLaw () const {return _nldPorous;};
    // set the time of _nldlaw
    virtual void setTime(const double t,const double dtime){
      dG3DMaterialLaw::setTime(t,dtime);
      _nldPorous->setTime(t,dtime);
    }
    virtual materialLaw::matname getType() const {return _nldPorous->getType();}

    virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw);
    virtual double soundSpeed() const{return _nldPorous->soundSpeed();} // or change value ??
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const;

    virtual double scaleFactor() const {return _nldPorous->scaleFactor();}

    virtual bool withEnergyDissipation() const {return _nldPorous->withEnergyDissipation();};
    virtual void setMacroSolver(const nonLinearMechSolver* sv){
      dG3DMaterialLaw::setMacroSolver(sv);
      if (_nldPorous != NULL){
        _nldPorous->setMacroSolver(sv);
      }
    }
    virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent =false);
    virtual void forceCoalescence(IPVariable*ipv, const IPVariable*ipvprev) const;
    virtual bool checkCrackCriterion(IPVariable* q1, const IPVariable *q0, const SVector3& normal, double delayFactor) const;
    virtual materialLaw* clone() const  = 0;
    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
      return _nldPorous;
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
      return _nldPorous;
    }
    #endif // SWIG
    void setStressFormulation(const int fm);
    void activateVoidNucleationDuringCoalescence(const bool fl);
    void useI1J2J3Implementation(const bool fl);
    void setTangentByPerturbationDuringCoalescence(bool fl, double pr=1e-8);
    void setOrderForLogExp(const int newOrder);
    void setViscosityLaw(const viscosityLaw& vico);
    void setNucleationLaw(const NucleationLaw& nuclLaw);
    void setNucleationLaw(const NucleationFunctionLaw& added_function, const NucleationCriterionLaw* added_criterion = NULL);
    void setScatterredInitialPorosity(const double fmin, const double fmax);
    void setCoalescenceLaw(const CoalescenceLaw& added_coalesLaw);
    void setVoidEvolutionLaw(const voidStateEvolutionLaw& added_evolutionLaw);
    void setSubStepping(const bool fl, const int maxNumStep);
    void setPostBlockedDissipationBehavior(const int method);
    void setFailureTolerance(const double NewFailureTol);
    void setLocalFailurePorosity(const double newFvMax);
    void setCorrectedRegularizedFunction(const scalarFunction& fct);
    void setNonLocalMethod(const int i);
    void setShearPorosityGrowthFactor(const double k);
    void setStressTriaxialityFunction_kw(const scalarFunction& fct);
    void setLodeFunction(const scalarFunction& fct);
    void clearCLengthLaw();
    void setCLengthLaw(const CLengthLaw& clength);
    void setInitialPorosityOnElement(const int ele, const double fInit);
    void setPlasticInstabilityFunctionLaw(const PlasticInstabilityFunctionLaw &plIns);
    // temperature dependence
    void setReferenceT(const double T);
    void setTemperatureFunction_rho(const scalarFunction& Tfunc);
    void setTemperatureFunction_E(const scalarFunction& Tfunc);
    void setTemperatureFunction_nu(const scalarFunction& Tfunc);
    void setTemperatureFunction_alp(const scalarFunction& Tfunc);
    void setThermomechanicsCoupling(const bool fl);
    void setThermalExpansionCoefficient(const double alp);
    void setReferenceThermalConductivity(const double KK);
    void setTemperatureFunction_ThermalConductivity(const scalarFunction& Tfunc);
    void setReferenceCp(const double Cp);
    void setTemperatureFunction_Cp(const scalarFunction& Tfunc);
    void setThermalEstimationPreviousConfig(const bool flag);
    void setTaylorQuineyFactor(const double f);
};


class NonLocalDamageGursonDG3DMaterialLaw : public NonLocalPorousDuctileDamageDG3DMaterialLaw
{


public:
    NonLocalDamageGursonDG3DMaterialLaw(const int num,const double rho,
                   const double E,const double nu,
                   const double q1, const double q2, const double q3, const double fVinitial,
                   const J2IsotropicHardening &_j2IH,
                   const double tol=1.e-6, const bool matrixByPert=false, const double pert = 1e-8);

    NonLocalDamageGursonDG3DMaterialLaw(const int num,const double rho,
                   const double E,const double nu,
                   const double q1, const double q2, const double q3, const double fVinitial,
                   const J2IsotropicHardening &_j2IH,
                   const CLengthLaw &_cLLaw, const double tol=1.e-6, const bool matrixByPert=false, const double pert = 1e-8);

    NonLocalDamageGursonDG3DMaterialLaw(const int num,const double rho,
                   const double E,const double nu,
                   const double q1, const double q2, const double q3, const double fVinitial,
                   const J2IsotropicHardening &_j2IH,
                   const CLengthLaw &_cLLaw, double TaylorQuineyFactor,
                   const double KK,const double cp,const double alp,
                   const double tol=1.e-6, const bool matrixByPert=false, const double pert = 1e-8);


    void setTemperatureFunction_q1(const scalarFunction& Tfunc);
    void setTemperatureFunction_q2(const scalarFunction& Tfunc);
    void setTemperatureFunction_q3(const scalarFunction& Tfunc);

    #ifndef SWIG
    NonLocalDamageGursonDG3DMaterialLaw(const NonLocalDamageGursonDG3DMaterialLaw &source);
    virtual ~NonLocalDamageGursonDG3DMaterialLaw();
    virtual materialLaw* clone() const{ return new NonLocalDamageGursonDG3DMaterialLaw(*this);};
    #endif
};

class NonLocalDamageGursonHill48DG3DMaterialLaw : public NonLocalPorousDuctileDamageDG3DMaterialLaw
{


public:
    NonLocalDamageGursonHill48DG3DMaterialLaw(const int num,const double rho,
                   const double E,const double nu,
                   const double q1, const double q2, const double q3, const double fVinitial,
                   const J2IsotropicHardening &_j2IH,
                   const double tol=1.e-6, const bool matrixByPert=false, const double pert = 1e-8);

    NonLocalDamageGursonHill48DG3DMaterialLaw(const int num,const double rho,
                   const double E,const double nu,
                   const double q1, const double q2, const double q3, const double fVinitial,
                   const J2IsotropicHardening &_j2IH,
                   const CLengthLaw &_cLLaw, const double tol=1.e-6, const bool matrixByPert=false, const double pert = 1e-8);

    NonLocalDamageGursonHill48DG3DMaterialLaw(const int num,const double rho,
                   const double E,const double nu,
                   const double q1, const double q2, const double q3, const double fVinitial,
                   const J2IsotropicHardening &_j2IH,
                   const CLengthLaw &_cLLaw, double TaylorQuineyFactor,
                   const double KK,const double cp,const double alp,
                   const double tol=1.e-6, const bool matrixByPert=false, const double pert = 1e-8);


    void setTemperatureFunction_q1(const scalarFunction& Tfunc);
    void setTemperatureFunction_q2(const scalarFunction& Tfunc);
    void setTemperatureFunction_q3(const scalarFunction& Tfunc);
    
    void setYieldParameters(const std::vector<double>& params);
    void setYieldParameters(const fullVector<double>& params);

    #ifndef SWIG
    NonLocalDamageGursonHill48DG3DMaterialLaw(const NonLocalDamageGursonHill48DG3DMaterialLaw &source);
    virtual ~NonLocalDamageGursonHill48DG3DMaterialLaw();
    virtual materialLaw* clone() const{ return new NonLocalDamageGursonHill48DG3DMaterialLaw(*this);};
    #endif
};



class NonLocalPorousThomasonDG3DMaterialLaw : public NonLocalPorousDuctileDamageDG3DMaterialLaw
{
  public:
    NonLocalPorousThomasonDG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH,
                      const double tol=1.e-8, const bool matrixByPert = false, const double pert = 1e-8);

    NonLocalPorousThomasonDG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                      const double tol=1.e-8, const bool matrixByPert = false, const double pert = 1e-8);
    
    NonLocalPorousThomasonDG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                      double TaylorQuineyFactor, const double KK,const double cp,const double alp,
                      const double tol=1.e-8, const bool matrixByPert = false, const double pert = 1e-8);

    void setYieldSurfaceExponent(const double newN);
    #ifndef SWIG
    NonLocalPorousThomasonDG3DMaterialLaw(const NonLocalPorousThomasonDG3DMaterialLaw &source);
    virtual ~NonLocalPorousThomasonDG3DMaterialLaw();
    virtual materialLaw* clone() const{ return new NonLocalPorousThomasonDG3DMaterialLaw(*this);};
    #endif
};

class NonLocalPorousThomasonWithMPSDG3DMaterialLaw : public NonLocalPorousDuctileDamageDG3DMaterialLaw
{
  public:
    NonLocalPorousThomasonWithMPSDG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH,
                      const double tol=1.e-8, const bool matrixByPert = false, const double pert = 1e-8);

    NonLocalPorousThomasonWithMPSDG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                      const double tol=1.e-8, const bool matrixByPert = false, const double pert = 1e-8);
    
    NonLocalPorousThomasonWithMPSDG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                      double TaylorQuineyFactor, const double KK,const double cp,const double alp,
                      const double tol=1.e-8, const bool matrixByPert = false, const double pert = 1e-8);

    void setYieldSurfaceExponent(const double newN);
    #ifndef SWIG
    NonLocalPorousThomasonWithMPSDG3DMaterialLaw(const NonLocalPorousThomasonWithMPSDG3DMaterialLaw &source);
    virtual ~NonLocalPorousThomasonWithMPSDG3DMaterialLaw();
    virtual materialLaw* clone() const{ return new NonLocalPorousThomasonWithMPSDG3DMaterialLaw(*this);};
    #endif
};

class NonLocalPorousThomasonWithMSSDG3DMaterialLaw : public NonLocalPorousDuctileDamageDG3DMaterialLaw
{
  public:
    NonLocalPorousThomasonWithMSSDG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH,
                      const double tol=1.e-8, const bool matrixByPert = false, const double pert = 1e-8);

    NonLocalPorousThomasonWithMSSDG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                      const double tol=1.e-8, const bool matrixByPert = false, const double pert = 1e-8);
    
    NonLocalPorousThomasonWithMSSDG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                      double TaylorQuineyFactor, const double KK,const double cp,const double alp,
                      const double tol=1.e-8, const bool matrixByPert = false, const double pert = 1e-8);

    #ifndef SWIG
    NonLocalPorousThomasonWithMSSDG3DMaterialLaw(const NonLocalPorousThomasonWithMSSDG3DMaterialLaw &source);
    virtual ~NonLocalPorousThomasonWithMSSDG3DMaterialLaw();
    virtual materialLaw* clone() const{ return new NonLocalPorousThomasonWithMSSDG3DMaterialLaw(*this);};
    #endif
};

class NonLocalPorousCoupledDG3DMaterialLaw : public NonLocalPorousDuctileDamageDG3DMaterialLaw
{
  public:
    NonLocalPorousCoupledDG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double q1, const double q2, const double q3,
                      const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH,
                      const double tol=1.e-8, const bool matrixbyPerturbation = false, const double pert = 1e-8);

    NonLocalPorousCoupledDG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double q1, const double q2, const double q3,
                      const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                      const double tol=1.e-8, const bool matrixbyPerturbation = false, const double pert = 1e-8);

    NonLocalPorousCoupledDG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double q1, const double q2, const double q3,
                      const double fVinitial, const double lambda0, const double kappa, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                      const double tol=1.e-8, const bool matrixbyPerturbation = false, const double pert = 1e-8);
    
    NonLocalPorousCoupledDG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double q1, const double q2, const double q3,
                      const double fVinitial, const double lambda0, const double kappa, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                      double TaylorQuineyFactor, const double KK,const double cp,const double alp,
                      const double tol=1.e-8, const bool matrixbyPerturbation = false, const double pert = 1e-8);

    void setYieldSurfaceExponent(const double newN);
    void setCrackTransition(const bool fl, const double beta=0.);
    void setLodeOffset(const bool fl);
    void setCftOffset(const bool fl);
    void setYieldOffset(const bool fl);
    void setBulkCouplingBehaviour(const bool fl);
    void setCoalescenceTolerance(const double t);
    void useTwoYieldRegularization(const bool fl, const double n);
    
    void setGrowthVoidEvolutionLaw(const voidStateEvolutionLaw& voidtateLaw);
    void setCoalescenceVoidEvolutionLaw(const voidStateEvolutionLaw& voidtateLaw);

    #ifndef SWIG
    NonLocalPorousCoupledDG3DMaterialLaw(const NonLocalPorousCoupledDG3DMaterialLaw &source);
    virtual ~NonLocalPorousCoupledDG3DMaterialLaw();
    virtual materialLaw* clone() const{ return new NonLocalPorousCoupledDG3DMaterialLaw(*this);};
    #endif
};

class NonLocalPorousCoupledWithMPSDG3DMaterialLaw : public NonLocalPorousCoupledDG3DMaterialLaw
{
  public:
    NonLocalPorousCoupledWithMPSDG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double q1, const double q2, const double q3,
                      const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH,
                      const double tol=1.e-8, const bool matrixbyPerturbation = false, const double pert = 1e-8);

    NonLocalPorousCoupledWithMPSDG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double q1, const double q2, const double q3,
                      const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                      const double tol=1.e-8, const bool matrixbyPerturbation = false, const double pert = 1e-8);

    NonLocalPorousCoupledWithMPSDG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double q1, const double q2, const double q3,
                      const double fVinitial, const double lambda0, const double kappa, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                      const double tol=1.e-8, const bool matrixbyPerturbation = false, const double pert = 1e-8);
    
    NonLocalPorousCoupledWithMPSDG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double q1, const double q2, const double q3,
                      const double fVinitial, const double lambda0, const double kappa, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                      double TaylorQuineyFactor, const double KK,const double cp,const double alp,
                      const double tol=1.e-8, const bool matrixbyPerturbation = false, const double pert = 1e-8);
    
    #ifndef SWIG
    NonLocalPorousCoupledWithMPSDG3DMaterialLaw(const NonLocalPorousCoupledWithMPSDG3DMaterialLaw &source);
    virtual ~NonLocalPorousCoupledWithMPSDG3DMaterialLaw();
    virtual materialLaw* clone() const{ return new NonLocalPorousCoupledWithMPSDG3DMaterialLaw(*this);};
    #endif
};

class NonLocalPorousCoupledWithMPSHill48DG3DMaterialLaw : public NonLocalPorousCoupledDG3DMaterialLaw
{
  public:
    NonLocalPorousCoupledWithMPSHill48DG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double q1, const double q2, const double q3,
                      const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH,
                      const double tol=1.e-8, const bool matrixbyPerturbation = false, const double pert = 1e-8);

    NonLocalPorousCoupledWithMPSHill48DG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double q1, const double q2, const double q3,
                      const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                      const double tol=1.e-8, const bool matrixbyPerturbation = false, const double pert = 1e-8);

    NonLocalPorousCoupledWithMPSHill48DG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double q1, const double q2, const double q3,
                      const double fVinitial, const double lambda0, const double kappa, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                      const double tol=1.e-8, const bool matrixbyPerturbation = false, const double pert = 1e-8);
    
    NonLocalPorousCoupledWithMPSHill48DG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double q1, const double q2, const double q3,
                      const double fVinitial, const double lambda0, const double kappa, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                      double TaylorQuineyFactor, const double KK,const double cp,const double alp,
                      const double tol=1.e-8, const bool matrixbyPerturbation = false, const double pert = 1e-8);
    void setYieldParameters(const std::vector<double>& params);
    void setYieldParameters(const fullVector<double>& params);
    #ifndef SWIG
    NonLocalPorousCoupledWithMPSHill48DG3DMaterialLaw(const NonLocalPorousCoupledWithMPSHill48DG3DMaterialLaw &source);
    virtual ~NonLocalPorousCoupledWithMPSHill48DG3DMaterialLaw();
    virtual materialLaw* clone() const{ return new NonLocalPorousCoupledWithMPSHill48DG3DMaterialLaw(*this);};
    #endif
};

class NonLocalPorousCoupledWithMPSAndMSSDG3DMaterialLaw : public NonLocalPorousCoupledDG3DMaterialLaw
{
  public:
    NonLocalPorousCoupledWithMPSAndMSSDG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double q1, const double q2, const double q3,
                      const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH,
                      const double tol=1.e-8, const bool matrixbyPerturbation = false, const double pert = 1e-8);

    NonLocalPorousCoupledWithMPSAndMSSDG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double q1, const double q2, const double q3,
                      const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                      const double tol=1.e-8, const bool matrixbyPerturbation = false, const double pert = 1e-8);

    NonLocalPorousCoupledWithMPSAndMSSDG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double q1, const double q2, const double q3,
                      const double fVinitial, const double lambda0, const double kappa, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                      const double tol=1.e-8, const bool matrixbyPerturbation = false, const double pert = 1e-8);
    
    NonLocalPorousCoupledWithMPSAndMSSDG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double q1, const double q2, const double q3,
                      const double fVinitial, const double lambda0, const double kappa, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                      double TaylorQuineyFactor, const double KK,const double cp,const double alp,
                      const double tol=1.e-8, const bool matrixbyPerturbation = false, const double pert = 1e-8);
    void setThomasonSmoothFactor(const double fact);
    void setShearSmoothFactor(const double fact);
    void setShearFactor(double g);
    void setVoidLigamentExponent(double e);
    void freezeCoalescenceState(const bool fl);
    void setNeckingCoalescenceLaw(const CoalescenceLaw& added_coalsLaw); 
    void setShearCoalescenceLaw(const CoalescenceLaw& added_coalsLaw); 
    void setShearVoidEvolutionLaw(const voidStateEvolutionLaw& voidtateLaw);
    #ifndef SWIG
    NonLocalPorousCoupledWithMPSAndMSSDG3DMaterialLaw(const NonLocalPorousCoupledWithMPSAndMSSDG3DMaterialLaw &source);
    virtual ~NonLocalPorousCoupledWithMPSAndMSSDG3DMaterialLaw();
    virtual materialLaw* clone() const{ return new NonLocalPorousCoupledWithMPSAndMSSDG3DMaterialLaw(*this);};
    #endif
};


class NonLocalPorousCoupledWithMPSAndMSSHill48DG3DMaterialLaw : public NonLocalPorousCoupledDG3DMaterialLaw
{
  public:
    NonLocalPorousCoupledWithMPSAndMSSHill48DG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double q1, const double q2, const double q3,
                      const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH,
                      const double tol=1.e-8, const bool matrixbyPerturbation = false, const double pert = 1e-8);

    NonLocalPorousCoupledWithMPSAndMSSHill48DG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double q1, const double q2, const double q3,
                      const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                      const double tol=1.e-8, const bool matrixbyPerturbation = false, const double pert = 1e-8);

    NonLocalPorousCoupledWithMPSAndMSSHill48DG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double q1, const double q2, const double q3,
                      const double fVinitial, const double lambda0, const double kappa, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                      const double tol=1.e-8, const bool matrixbyPerturbation = false, const double pert = 1e-8);
    
    NonLocalPorousCoupledWithMPSAndMSSHill48DG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double q1, const double q2, const double q3,
                      const double fVinitial, const double lambda0, const double kappa, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                      double TaylorQuineyFactor, const double KK,const double cp,const double alp,
                      const double tol=1.e-8, const bool matrixbyPerturbation = false, const double pert = 1e-8);
    void setThomasonSmoothFactor(const double fact);
    void setShearSmoothFactor(const double fact);
    void setShearFactor(double g);
    void setVoidLigamentExponent(double e);
    void freezeCoalescenceState(const bool fl);
    void setNeckingCoalescenceLaw(const CoalescenceLaw& added_coalsLaw); 
    void setShearCoalescenceLaw(const CoalescenceLaw& added_coalsLaw); 
    void setShearVoidEvolutionLaw(const voidStateEvolutionLaw& voidtateLaw);
    void setYieldParameters(const std::vector<double>& params);
    void setYieldParameters(const fullVector<double>& params);
    #ifndef SWIG
    NonLocalPorousCoupledWithMPSAndMSSHill48DG3DMaterialLaw(const NonLocalPorousCoupledWithMPSAndMSSHill48DG3DMaterialLaw &source);
    virtual ~NonLocalPorousCoupledWithMPSAndMSSHill48DG3DMaterialLaw();
    virtual materialLaw* clone() const{ return new NonLocalPorousCoupledWithMPSAndMSSHill48DG3DMaterialLaw(*this);};
    #endif
};

class NonLocalPorousDuctileDamageWithCleavageDG3DMaterialLaw : public dG3DMaterialLaw{
  protected:
    #ifndef SWIG
    mlawNonLocalPorousWithCleavageFailure* _nlPorousWithCleavage;
    #endif // SWIG

  public:
    NonLocalPorousDuctileDamageWithCleavageDG3DMaterialLaw(const int num, NonLocalPorousDuctileDamageDG3DMaterialLaw& law,
                            const CLengthLaw &cLLaw,const DamageLaw &damLaw,double sigmac);
    NonLocalPorousDuctileDamageWithCleavageDG3DMaterialLaw(const int num, NonLocalPorousDuctileDamageDG3DMaterialLaw& law,
                            const CLengthLaw &cLLaw,const DamageLaw &damLaw, const FailureCriterionBase& failureLaw);
    #ifndef SWIG
    NonLocalPorousDuctileDamageWithCleavageDG3DMaterialLaw(const NonLocalPorousDuctileDamageWithCleavageDG3DMaterialLaw& src);
    virtual ~NonLocalPorousDuctileDamageWithCleavageDG3DMaterialLaw();

    // set the time of _nldlaw
    virtual void setTime(const double t,const double dtime){
      dG3DMaterialLaw::setTime(t,dtime);
      _nlPorousWithCleavage->setTime(t,dtime);
    }
    virtual materialLaw::matname getType() const {return _nlPorousWithCleavage->getType();}

    virtual void createIPState(IPStateBase* &ips,bool hasBodyForce,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPVariable* &ipv,bool hasBodyForce,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){
      _nlPorousWithCleavage->initLaws(maplaw);
    }
    virtual double soundSpeed() const{return _nlPorousWithCleavage->soundSpeed();} // or change value ??
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const;

    virtual double scaleFactor() const {return _nlPorousWithCleavage->scaleFactor();}

    virtual bool withEnergyDissipation() const {return _nlPorousWithCleavage->withEnergyDissipation();};
    virtual void setMacroSolver(const nonLinearMechSolver* sv){
      dG3DMaterialLaw::setMacroSolver(sv);
      if (_nlPorousWithCleavage != NULL){
        _nlPorousWithCleavage->setMacroSolver(sv);
      }
    }
    virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent =false);
    virtual materialLaw* clone() const  {return new NonLocalPorousDuctileDamageWithCleavageDG3DMaterialLaw(*this);};
    
    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
      return _nlPorousWithCleavage;
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
      return _nlPorousWithCleavage;
    }
    #endif // SWIG
};

class NonLocalDamageJ2FullyCoupledDG3DMaterialLaw : public dG3DMaterialLaw
{
  protected:
    #ifndef SWIG
    mlawNonLocalDamageJ2FullyCoupledThermoMechanics *_nldJ2Hyperlaw;
    #endif //SWIG
  public:
    NonLocalDamageJ2FullyCoupledDG3DMaterialLaw(const int num,const double rho,
                   double E,const double nu, const double sy0, const double h,
                   const CLengthLaw &_cLLaw,const DamageLaw &_damLaw,const double tol=1.e-6,
                   const bool matrixBypert = false, const double tolpert=1e-8);
    void setStrainOrder(const int order);
    void setReferenceTemperature(const double Tr);
    void setIsotropicHardeningLaw(const J2IsotropicHardening& isoHard);

    void setReferenceThermalExpansionCoefficient(const double al);
    void setTemperatureFunction_ThermalExpansionCoefficient(const scalarFunction& Tfunc);

    void setReferenceThermalConductivity(const double KK);
    void setTemperatureFunction_ThermalConductivity(const scalarFunction& Tfunc);

    void setReferenceCp(const double Cp);
    void setTemperatureFunction_Cp(const scalarFunction& Tfunc);

    void setTemperatureFunction_Hardening(const scalarFunction& Tfunc);
    void setTemperatureFunction_InitialYieldStress(const scalarFunction& Tfunc);
    void setTemperatureFunction_BulkModulus(const scalarFunction& Tfunc);
    void setTemperatureFunction_ShearModulus(const scalarFunction& Tfunc);
    void setTaylorQuineyFactor(const double f);
    void setThermalEstimationPreviousConfig(const bool flag);

    #ifndef SWIG
    NonLocalDamageJ2FullyCoupledDG3DMaterialLaw(const NonLocalDamageJ2FullyCoupledDG3DMaterialLaw &source);
    virtual ~NonLocalDamageJ2FullyCoupledDG3DMaterialLaw();
    // set the time of _nldlaw
    virtual void setTime(const double t,const double dtime);
    virtual materialLaw::matname getType() const;

    virtual void createIPState(IPStateBase* &ips,bool hasBodyForce,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPVariable* &ipv,bool hasBodyForce,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
    virtual double soundSpeed() const;
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const;
    virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent =false);
    virtual double scaleFactor() const{return _nldJ2Hyperlaw->scaleFactor();};
    virtual int getNumberOfExtraDofsDiffusion() const {return 1;}
    virtual double getInitialExtraDofStoredEnergyPerUnitField() const;
    virtual double getExtraDofStoredEnergyPerUnitField(double T) const;
    virtual materialLaw* clone() const{ return new NonLocalDamageJ2FullyCoupledDG3DMaterialLaw(*this);};
    virtual bool withEnergyDissipation() const {return _nldJ2Hyperlaw->withEnergyDissipation();};
    virtual void setMacroSolver(const nonLinearMechSolver* sv){
      dG3DMaterialLaw::setMacroSolver(sv);
      if (_nldJ2Hyperlaw != NULL){
        _nldJ2Hyperlaw->setMacroSolver(sv);
      }
    }
    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
      return _nldJ2Hyperlaw;
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
      return _nldJ2Hyperlaw;
    }
    #endif
};



class NonlocalDamageTorchANNBasedDG3DMaterialLaw : public dG3DMaterialLaw{
  public:
    enum KinematicInput{EGL=1,U=2,smallStrain=3,EGLInc=4,UInc=5,smallStrainInc=6};  // EGL - Green Lagraneg strain, U-Biot strain, smallStrain=small strain; Inc for increments
  protected:
    #ifndef SWIG
    int _numberOfInput;
    int _numberOfInternalVariables_stress;
    int _numberOfInternalVariables_elTang;
    int _numberOfInternalVariables_NonDamTang;
    double _initialValue_h;
    bool _NeedExtraNorm;
    bool _DoubleInput;
    int _numNonlocalVar;
    std::vector< CLengthLaw * > cLLaw;
    materialLaw *_mecalocalLaw;
    bool _RNN;
#if defined(HAVE_TORCH)
    torch::jit::script::Module module_stress;
    torch::jit::script::Module module_elTang;
    torch::jit::script::Module module_NonDamTang;
#endif
    double _EXXmean;       //Green Lagrange strain Normalization
    double _EXXstd;
    double _EXYmean;
    double _EXYstd;
    double _EYYmean;
    double _EYYstd;
    double _EYZmean;
    double _EYZstd;
    double _EZZmean;
    double _EZZstd;
    double _EZXmean;
    double _EZXstd;

    double _SXXmean;
    double _SXXstd;
    double _SXYmean;
    double _SXYstd;
    double _SYYmean;
    double _SYYstd;
    double _SYZmean;
    double _SYZstd;
    double _SZZmean;
    double _SZZstd;
    double _SZXmean;
    double _SZXstd;

    double _elTangCompxxxxmean;
    double _elTangCompxxxxstd;
    double _elTangCompyyxxmean;
    double _elTangCompyyxxstd;
    double _elTangCompzzxxmean;
    double _elTangCompzzxxstd;   
    double _elTangCompxyxymean;
    double _elTangCompxyxystd;
    double _elTangCompxzxzmean;
    double _elTangCompxzxzstd;
    double _elTangCompyyyymean;
    double _elTangCompyyyystd;
    double _elTangCompzzyymean;
    double _elTangCompzzyystd;
    double _elTangCompyzyzmean;
    double _elTangCompyzyzstd;
    double _elTangCompzzzzmean;
    double _elTangCompzzzzstd;
    
    double _NonDamTangCompxxxxmean;
    double _NonDamTangCompxxxxstd;
    double _NonDamTangCompyyxxmean;
    double _NonDamTangCompyyxxstd;
    double _NonDamTangCompzzxxmean;
    double _NonDamTangCompzzxxstd;   
    double _NonDamTangCompxyxymean;
    double _NonDamTangCompxyxystd;
    double _NonDamTangCompxzxzmean;
    double _NonDamTangCompxzxzstd;
    double _NonDamTangCompyyyymean;
    double _NonDamTangCompyyyystd;
    double _NonDamTangCompzzyymean;
    double _NonDamTangCompzzyystd;
    double _NonDamTangCompyzyzmean;
    double _NonDamTangCompyzyzstd;
    double _NonDamTangCompzzzzmean;
    double _NonDamTangCompzzzzstd;
    
    
    
    bool _tangentByPerturbation;
    double _pertTol;
    
    int _Id0000;
    int _Id0100;
    int _Id0200;
    int _Id1100;
    int _Id1200;
    int _Id2200;
    int _Id0101;
    int _Id0201;
    int _Id1101;
    int _Id1201;
    int _Id2201;
    int _Id0202;
    int _Id1102;
    int _Id1202;
    int _Id2202;
    int _Id1111;
    int _Id1211;
    int _Id2211;
    int _Id1212;
    int _Id2212;
    int _Id2222;
    
    int _Ie00;
    int _Ie01;
    int _Ie02;
    int _Ie11;
    int _Ie12;
    int _Ie22;
    
#if defined(HAVE_TORCH)
    torch::Tensor VXX;
    torch::Tensor VXY;
    torch::Tensor VYY;
    torch::Tensor VYZ;
    torch::Tensor VZZ;
    torch::Tensor VZX;
    
    torch::Tensor EffVXX;
    torch::Tensor EffVXY;
    torch::Tensor EffVYY;
    torch::Tensor EffVYZ;
    torch::Tensor EffVZZ;
    torch::Tensor EffVZX;
    
    torch::Tensor VLocal;    
#endif

    KinematicInput _kinematicInput; // using to speicfy the input kinematic variable, EGL is chosen by default
    // note: input =EGL (0.5*(FT*F-I))) -> ouput =second PK
    //       input =U (sqr(FTF)-I) ->  ouput =second PK
    //       input =smallStrain (0.5(FT+F)-I)-> ouput =cauchy=P
	#endif

  public:
    NonlocalDamageTorchANNBasedDG3DMaterialLaw(const int num, const double rho, const int numberOfInput, const int numNonlocalVar,
                const int numInternalVars_stress, const int numInternalVars_elTang, const int numInternalVars_NonDamTang, const char* nameTorch_stress, const char* nameTorch_elTang,const char* nameTorch_NonDamTang, const double EXXmean, const double EXXstd, const double EXYmean,
                const double EXYstd, const double EYYmean, const double EYYstd, const double EYZmean, const double EYZstd, const double EZZmean,
                const double EZZstd, const double EZXmean, const double EZXstd, 
                const double SXXmean, const double SXXstd, const double SXYmean, const double SXYstd, const double SYYmean, const double SYYstd, const double SYZmean, const double SYZstd, const double SZZmean, const double SZZstd, const double SZXmean, const double SZXstd, 
                const double elTangCompxxxxmean, const double elTangCompxxxxstd, const double elTangCompyyxxmean, const double elTangCompyyxxstd, const double elTangCompzzxxmean, const double elTangCompzzxxstd,  const double elTangCompxyxymean, const double elTangCompxyxystd, const double elTangCompxzxzmean, const double elTangCompxzxzstd, const double elTangCompyyyymean, const double elTangCompyyyystd, const double elTangCompzzyymean, const double elTangCompzzyystd, const double elTangCompyzyzmean, const double elTangCompyzyzstd, const double elTangCompzzzzmean, const double elTangCompzzzzstd, 
                const double NonDamTangCompxxxxmean, const double NonDamTangCompxxxxstd, const double NonDamTangCompyyxxmean, const double NonDamTangCompyyxxstd, const double NonDamTangCompzzxxmean, const double NonDamTangCompzzxxstd,  const double NonDamTangCompxyxymean, const double NonDamTangCompxyxystd, const double NonDamTangCompxzxzmean, const double NonDamTangCompxzxzstd, const double NonDamTangCompyyyymean, const double NonDamTangCompyyyystd, const double NonDamTangCompzzyymean, const double NonDamTangCompzzyystd, const double NonDamTangCompyzyzmean, const double NonDamTangCompyzyzstd, const double NonDamTangCompzzzzmean, const double NonDamTangCompzzzzstd, 
                bool pert=false, double tol = 1e-5);


   void setNonLocalLengthScale(const int index, const CLengthLaw &cLLaw);     //need some changes?
   virtual const std::vector<CLengthLaw *> getCLengthLaw() const {return cLLaw; };
   void setKinematicInput(const int i);
   void setNeedExtraNorm (const bool NeedExtraNorm){
	_NeedExtraNorm = NeedExtraNorm;
	if(_NeedExtraNorm) Msg::Info("Extra Norm is applied");
        };
   void setDoubleInput (const bool DoubleInput){
		    _DoubleInput = DoubleInput;
		    if(_DoubleInput) Msg::Info("Double Input is applied");
                };


   void setInitialHValue(double initialValue_h) {_initialValue_h=initialValue_h;};

   void setRNN(bool RNN)
   {
     _RNN=RNN;
     if (_RNN)
     {
       Msg::Info("RNN is used");
     }
   }
   
   void setNonLocalD0000(bool d){if (not d){_Id0000=0;}};
   void setNonLocalD0100(bool d){if (not d){_Id0100=0;}};
   void setNonLocalD0200(bool d){if (not d){_Id0200=0;}};
   void setNonLocalD1100(bool d){if (not d){_Id1100=0;}};
   void setNonLocalD1200(bool d){if (not d){_Id1200=0;}};
   void setNonLocalD2200(bool d){if (not d){_Id2200=0;}};
   void setNonLocalD0101(bool d){if (not d){_Id0101=0;}};
   void setNonLocalD0201(bool d){if (not d){_Id0201=0;}};
   void setNonLocalD1101(bool d){if (not d){_Id1101=0;}};
   void setNonLocalD1201(bool d){if (not d){_Id1201=0;}};
   void setNonLocalD2201(bool d){if (not d){_Id2201=0;}};
   void setNonLocalD0202(bool d){if (not d){_Id0202=0;}};
   void setNonLocalD1102(bool d){if (not d){_Id1102=0;}};
   void setNonLocalD1202(bool d){if (not d){_Id1202=0;}};
   void setNonLocalD2202(bool d){if (not d){_Id2202=0;}};
   void setNonLocalD1111(bool d){if (not d){_Id1111=0;}};
   void setNonLocalD1211(bool d){if (not d){_Id1211=0;}};
   void setNonLocalD2211(bool d){if (not d){_Id2211=0;}};
   void setNonLocalD1212(bool d){if (not d){_Id1212=0;}};
   void setNonLocalD2212(bool d){if (not d){_Id2212=0;}};
   void setNonLocalD2222(bool d){if (not d){_Id2222=0;}};
   
   void setNonLocalEplast00(bool d){if (not d){_Ie00=0;}};
   void setNonLocalEplast01(bool d){if (not d){_Ie01=0;}};
   void setNonLocalEplast02(bool d){if (not d){_Ie02=0;}};
   void setNonLocalEplast11(bool d){if (not d){_Ie11=0;}};
   void setNonLocalEplast12(bool d){if (not d){_Ie12=0;}};
   void setNonLocalEplast22(bool d){if (not d){_Ie22=0;}};
   
   void setMechanicalLocalMaterialLaw(const dG3DMaterialLaw *mlaw){
   _mecalocalLaw=NULL;
   elasticStiffness=mlaw->elasticStiffness;
   if(_mecalocalLaw != NULL){
     delete _mecalocalLaw;
     _mecalocalLaw = NULL;}
   _mecalocalLaw=mlaw->getConstNonLinearSolverMaterialLaw()->clone();
   };  
   
    const materialLaw &getConstRefMechanicalLocalMaterialLaw() const
    {
      if(_mecalocalLaw == NULL)
      {
        Msg::Error("getConstRefMechanicalMaterialLaw: Mechanic local law is null");
      }
      return *_mecalocalLaw;
    }
    
    materialLaw &getRefMechanicalLocalMaterialLaw()
    {
      if(_mecalocalLaw == NULL)
      {
        Msg::Error("getRefMechanicalMaterialLaw: Mechanic local law is null");
      }
      return *_mecalocalLaw;
    }
    
    materialLaw::matname getTypeMecaLocalLaw() const
    {
      return _mecalocalLaw->getType();
    }
        
        
    #ifndef SWIG
   NonlocalDamageTorchANNBasedDG3DMaterialLaw(const NonlocalDamageTorchANNBasedDG3DMaterialLaw& src);
   virtual ~NonlocalDamageTorchANNBasedDG3DMaterialLaw();
   virtual materialLaw::matname getType() const {return materialLaw::NonlocalDamageTorchANN;}
   virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  // To allow initialization of bulk ip in case of fracture
  
   virtual void createIPVariable(IPVariable* &ipv,  bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const; 
   virtual void initLaws(const std::map<int,materialLaw*> &maplaw);
   virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
   virtual double scaleFactor() const {return 1.;}
   virtual double soundSpeed() const;
   virtual materialLaw* clone() const {return new NonlocalDamageTorchANNBasedDG3DMaterialLaw(*this);};
   virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing
   virtual bool withEnergyDissipation() const {return true;};

   virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
      Msg::Error("getConstNonLinearSolverMaterialLaw in NonlocalDamageTorchANNBasedDG3DMaterialLaw does not exist");
      return NULL;
    }
   virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
      Msg::Error("getNonLinearSolverMaterialLaw in NonlocalDamageTorchANNBasedDG3DMaterialLaw does not exist");
      return NULL;
    }

  private:
    /*void convertFullMatrixToSTensor43(const fullMatrix<double>& DSDE, STensor43& K) const;
    void convertFullMatrixToSTensor3(const fullMatrix<double>& DVarDE, STensor3& K) const;
    void invertMatrix(int NbElement,fullMatrix<double>& mat,fullMatrix<double>& matinv);*/
#if defined(HAVE_TORCH)
    void InverseNormalize_stress(const torch::Tensor& S_norm, STensor3& S) const;
    void InverseNormalize_elTang(const torch::Tensor& elTang_norm, STensor43& elTangPred) const;
    void InverseNormalize_NonDamTang(const torch::Tensor& NonDamTang_norm, STensor43& NonDamTangPred) const;
    void Normalize_strain(const STensor3& E1Tensor, vector<float>& E_norm) const;
    void computation_basis_D(STensor43& A0, STensor43& A1, STensor43& A2, STensor43& A3, STensor43& A4, STensor43& A5, STensor43& A6, STensor43& A7, STensor43& A8, STensor43& A9, STensor43& A10, STensor43& A11, STensor43& A12, STensor43& A13, STensor43& A14, STensor43& A15, STensor43& A16, STensor43& A17, STensor43& A18, STensor43& A19, STensor43& A20);
    void computation_basis_Eplast(STensor3& B0, STensor3& B1, STensor3& B2, STensor3& B3, STensor3& B4, STensor3& B5);
    void computation_Eplast(const STensor3& E1Tensor, const STensor3& S, const STensor43& elTangPred, STensor3& Eplast);
    void computation_D(const STensor43& NonDamTangPred, const STensor43& elTangPred, STensor43& D);
    void computation_Eplast_l(const STensor3& B0, const STensor3& B1, const STensor3& B2, const STensor3& B3, const STensor3& B4, const STensor3& B5, const STensor3& Eplast, STensor3& Eplast_l);
    void computation_D_l(const STensor43& A0, const STensor43& A1, const STensor43& A2, const STensor43& A3, const STensor43& A4, const STensor43& A5, const STensor43& A6, const STensor43& A7, const STensor43& A8, const STensor43& A9, const STensor43& A10, const STensor43& A11, const STensor43& A12, const STensor43& A13, const STensor43& A14, const STensor43& A15, const STensor43& A16, const STensor43& A17, const STensor43& A18, const STensor43& A19, const STensor43& A20, const STensor43& D, STensor43& D_l);
    void computation_D_nl__Eplast_nl(const STensor43& A0, const STensor43& A1, const STensor43& A2, const STensor43& A3, const STensor43& A4, const STensor43& A5, const STensor43& A6, const STensor43& A7, const STensor43& A8, const STensor43& A9, const STensor43& A10, const STensor43& A11, const STensor43& A12, const STensor43& A13, const STensor43& A14, const STensor43& A15, const STensor43& A16, const STensor43& A17, const STensor43& A18, const STensor43& A19, const STensor43& A20, const STensor3& B0, const STensor3& B1, const STensor3& B2, const STensor3& B3, const STensor3& B4, const STensor3& B5, const STensor43& D, const STensor3& Eplast, STensor3& Eplast_nl, STensor43& D_nl, nonLocalDamageTorchANNDG3DIPVariableDG3DIPVariable* ipvcur);
    void RNNstress_stiff(const STensor3& E0Tensor, const STensor3& E1Tensor, const torch::Tensor& h_stress_0, torch::Tensor& h_stress_1, const torch::Tensor& h_elTang_0, torch::Tensor& h_elTang_1, const torch::Tensor& h_NonDamTang_0, torch::Tensor& h_NonDamTang_1, STensor3& S, 
                        STensor43& NonDamTangPred,STensor43& elTangPred, STensor43& D, STensor3& Eplast, const bool stiff, fullMatrix<double>& DSDE, const STensor3& B0, const STensor3& B1, const STensor3& B2, const STensor3& B3, const STensor3& B4, const STensor3& B5, STensor3& Eplast_l);
            
#endif
    #endif //SWIG

};

//TO DO 
class GenericCrackPhaseFieldDG3DMaterialLaw : public dG3DMaterialLaw{
  protected:
    mlawGenericCrackPhaseField *_nldlaw; // pointer to allow to choose between LLN style or Gmsh Style. The choice is performed by the constructor (2 constructors with != arguments)

  public:
    GenericCrackPhaseFieldDG3DMaterialLaw(const int num, const double rho, const CLengthLaw &cLLaw, const double gc);
    
    // void setNumberOfNonLocalVariables(int nb) {_nldlaw->setNumberOfNonLocalVariables(nb);}
    // void setInitialDmax_Inc(double d) {_nldlaw->setInitialDmax_Inc(d);};
    // void setInitialDmax_Mtx(double d) {_nldlaw->setInitialDmax_Mtx(d);};
    // void setInitialDmax(double d) {_nldlaw->setInitialDmax(d);};
    
    virtual ~GenericCrackPhaseFieldDG3DMaterialLaw();
    
    const materialLaw &getConstRefMechanicalMaterialLaw() const;
    materialLaw &getRefMechanicalMaterialLaw();
    void setMechanicalMaterialLaw(const dG3DMaterialLaw *mlaw);

  #ifndef SWIG
    GenericCrackPhaseFieldDG3DMaterialLaw(const GenericCrackPhaseFieldDG3DMaterialLaw &source);
    
    // set the time of _nldlaw
    virtual void setTime(const double t,const double dtime);
    virtual materialLaw::matname getType() const;
    virtual void createIPState(IPStateBase* &ips, bool hasBodyForce,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPVariable* &ipv,bool hasBodyForce,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
    virtual double soundSpeed() const;// or change value ??
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const;
    virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent =false);
    virtual double scaleFactor() const;
    virtual materialLaw* clone() const{return new GenericCrackPhaseFieldDG3DMaterialLaw(*this);};
    virtual bool withEnergyDissipation() const;
    virtual void setMacroSolver(const nonLinearMechSolver* sv);
    
    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
      return _nldlaw;
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
      return _nldlaw;
    }
#endif
};


#endif //_NONLOCALDAMAGEDG3DMATERIALLAW_H_
