//
// C++ Interface: terms
//
// Description: FilterDof for Dg 3D
//
//
// Author:  <L. Noels>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
#ifndef _DG3DFILTERDOFCOMPONENT_H_
#define _DG3DFILTERDOFCOMPONENT_H_
#include "dG3DDof3IntType.h"
class DG3DFilterDofComponent :public FilterDof
{
  int comp;
 public :
  DG3DFilterDofComponent(int comp_): comp(comp_) {}
  virtual bool operator()(Dof key)
  {
    int type=key.getType();
    int icomp,iphys,inode;
    dG3DDof3IntType::getThreeIntsFromType(type, icomp, iphys,inode);
    if (icomp==comp) return true;
    return false;
  }
  virtual void filter(const std::vector<Dof> R, std::vector<Dof> &Rfilter){
    Rfilter.clear();
    for(int i=0; i<R.size();i++)
      if(this->operator()(R[i])) Rfilter.push_back(R[i]);
  }
};

#endif
