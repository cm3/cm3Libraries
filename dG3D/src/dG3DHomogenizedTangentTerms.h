//
// C++ Interface: homogenized tangent terms
//
//
//
// Author:  <V.-D. NGUYEN>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef DG3DHOMOGENIZEDTANGENTTERMS_H
#define DG3DHOMOGENIZEDTANGENTTERMS_H

#include "dG3DTerms.h"

class dG3DHomogenizedTangent : public BilinearTermBase{
  protected :
    const IPField *_ipf;
    const partDomain* _dom;
		
		//homogenized quantities P, Q, irrEnerg, --> range first_ <=row < last_
		int nRows;
		int firstRowP, lastRowP;
		int firstRowQ, lastRowQ;
		int firstRowIrEnergy, lastRowIrEnergy;
		int firstRowFD, lastRowFD;
		int firstRowFlux, lastRowFlux;
		int firstRowMecaSource, lastRowMecaSource;
		
  public :
    dG3DHomogenizedTangent(const partDomain* dom, const IPField* ipf) : BilinearTermBase(),
                          _ipf(ipf),_dom(dom){
														
			nRows = 0;
			firstRowP = 0;
			lastRowP = firstRowP+9;
                        firstRowFD = 0; lastRowFD=0;
		        firstRowFlux = 0; lastRowFlux=0;
		        firstRowMecaSource=0; lastRowMecaSource=0;

			nRows = lastRowP;
			if (_dom->getHomogenizationOrder() == 2){
				firstRowQ = nRows;
				lastRowQ = nRows+27;
				nRows += 27;
			}
			if (_dom->getExtractIrreversibleEnergyFlag()){
				firstRowIrEnergy =nRows;
				lastRowIrEnergy = nRows+1;
				nRows += 1;
			}
			if (_dom->isExtractCohesiveLawFromMicroDamage()){
				firstRowFD = nRows;
				lastRowFD = nRows+9;
				nRows += 9;
			}
                        if(_dom->getNumConstitutiveExtraDofDiffusionVariable()>0  ){
 			  firstRowFlux = nRows;
			  lastRowFlux = nRows+3*_dom->getNumConstitutiveExtraDofDiffusionVariable();
			  nRows += 3*_dom->getNumConstitutiveExtraDofDiffusionVariable();
			
			  firstRowMecaSource = nRows;
			  lastRowMecaSource = nRows+_dom->getNumConstitutiveExtraDofDiffusionVariable();
			  nRows += _dom->getNumConstitutiveExtraDofDiffusionVariable();
                        }
		}
    virtual ~dG3DHomogenizedTangent(){}
    virtual void get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const;
		virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const
		{
			Msg::Error("Define me get by gauss point dG3DHomogenizedTangent");
		}
    virtual BilinearTermBase* clone () const{
      return new dG3DHomogenizedTangent(_dom,_ipf);
    }
};

class dG3DBodyForceTangent : public BilinearTermBase{
  protected :
    const IPField *_ipf;
    const partDomain* _dom;
		
    //homogenized quantities F, G, irrEnerg, --> range first_ <=row < last_
    int nCols;
    int firstColF, lastColF;
    int firstColG, lastColG;
    
  public :
    dG3DBodyForceTangent(const partDomain* dom, const IPField* ipf) : BilinearTermBase(), _ipf(ipf),_dom(dom){										
      nCols = 0;
      firstColF = 0;
      lastColF = firstColF+9; 
      nCols = lastColF;
      if (_dom->getHomogenizationOrder() == 2){
     	firstColG = nCols;
	lastColG= nCols+27;
	nCols += 27;
      }
    }
    virtual ~dG3DBodyForceTangent(){}
    virtual void get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const;
    virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const
    {
	Msg::Error("Define me get by gauss point dG3DBodyForceTangent");
    }
    virtual BilinearTermBase* clone () const{
    return new dG3DBodyForceTangent(_dom,_ipf);
    }
};


class dG3DBodyForceTermPathFollowing : public LinearTermBase<double>{
  protected :
    const IPField *_ipf;
    const partDomain* _dom;
		    
  public :
    dG3DBodyForceTermPathFollowing(const partDomain* dom, const IPField* ipf) : LinearTermBase<double>(), _ipf(ipf),_dom(dom)
    {										
    }
    virtual ~dG3DBodyForceTermPathFollowing(){}
    virtual const bool isData() const {return false;}
    virtual void get(MElement *ele,int npts,IntPt *GP,fullVector<double> &m) const;
    virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullVector<double> > &mv) const
    {
	Msg::Error("Define me get by gauss point dG3DBodyForceTermPathFollowing");
    }
    virtual LinearTermBase<double>* clone () const{
      dG3DBodyForceTermPathFollowing* BodyForceTerm = new dG3DBodyForceTermPathFollowing(_dom,_ipf);
      return BodyForceTerm;
    }
    virtual void set(const fullVector<double> *datafield){}
};


class dG3DHomogenizedElasticTangent : public dG3DHomogenizedTangent{
  protected:
    bool removeDamage;
  public:
    dG3DHomogenizedElasticTangent(const partDomain* dom, const IPField* ipf) : dG3DHomogenizedTangent(dom,ipf), removeDamage(false){}
    virtual ~dG3DHomogenizedElasticTangent(){}
    virtual void get(MElement *ele, int npts, IntPt *GP, fullMatrix<double> &m) const;
    virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const
    {
      Msg::Error("Define me get by gauss point dG3DHomogenizedElasticTangent");
    }
    virtual BilinearTermBase* clone () const{
      return new dG3DHomogenizedElasticTangent(_dom,_ipf);
    }
};

class dG3DHomogenizedUndamagedElasticTangent : public dG3DHomogenizedElasticTangent{
  public:
    dG3DHomogenizedUndamagedElasticTangent(const partDomain* dom, const IPField* ipf) : dG3DHomogenizedElasticTangent(dom,ipf)
    {
      removeDamage=true;
    }
    virtual ~dG3DHomogenizedUndamagedElasticTangent(){}
    //virtual void get(MElement *ele, int npts, IntPt *GP, fullMatrix<double> &m) const;
    virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const
    {
      Msg::Error("Define me get by gauss point dG3DHomogenizedUndamagedElasticTangent");
    }
    virtual BilinearTermBase* clone () const{
      return new dG3DHomogenizedUndamagedElasticTangent(_dom,_ipf);
    }
};



/*class dG3DNonLocalHomogenizedTangent : public dG3DHomogenizedTangent{
	protected:
		
  public :
    dG3DNonLocalHomogenizedTangent(const partDomain* dom, const IPField* ipf):
																dG3DHomogenizedTangent(dom,ipf){
		}
    virtual ~dG3DNonLocalHomogenizedTangent(){}
    virtual void get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const;
		virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const
		{
			Msg::Error("Define me get by gauss point dG3DNonLocalHomogenizedTangent");
		}
    virtual BilinearTermBase* clone () const{
      return new dG3DNonLocalHomogenizedTangent(_dom,_ipf);
    }
};

class dG3DExtraDofHomogenizedTangent : public dG3DHomogenizedTangent{
	protected:
		int firstRowFlux, lastRowFlux;
		int firstRowMecaSource, lastRowMecaSource;
		
	public:
		dG3DExtraDofHomogenizedTangent(const partDomain* dom, const IPField* ipf) :
													dG3DHomogenizedTangent(dom,ipf){
			firstRowFlux = nRows;
			lastRowFlux = nRows+3;
			nRows += 3;
			
			firstRowMecaSource = nRows;
			lastRowMecaSource = nRows+1;
			nRows += 1;
			
		};
    virtual ~dG3DExtraDofHomogenizedTangent(){}
    virtual void get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const;
		virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const
		{
			Msg::Error("Define me get by gauss point dG3DExtraDofHomogenizedTangent");
		}
    virtual BilinearTermBase* clone () const{
      return new dG3DExtraDofHomogenizedTangent(_dom,_ipf);
    }
	
};*/

#endif //DG3DHOMOGENIZEDTANGENTTERMS_H

