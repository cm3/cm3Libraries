
#include "dG3DCohesiveBandMaterialLaw.h"
#include "FractureCohesiveDG3DIPVariable.h"
#include "dG3DCohesiveBandIPVariable.h"
#include "nonLinearMechSolver.h"
#include "nonLocalDamageDG3DIPVariable.h"
#include "failureDetection.h"
#include "FractureCohesiveDG3DMaterialLaw.h"


CohesiveBand3DLaw::CohesiveBand3DLaw(const int num, const double sigmac, const double Dc, const double h,  const double mu,
    const double fractureStrengthFactorMin, const double fractureStrengthFactorMax, const double beta, const double Kp,
    const bool damageBasedCriterion, double gradFactor, const bool useInitialFmean, const double deltaOffset):
        Cohesive3DLaw(num,true),
        _sigmac(sigmac), _mu(mu),_fscmin(fractureStrengthFactorMin), _fscmax(fractureStrengthFactorMax), _beta(beta),
        _Kp(Kp), _Dc(Dc),_damageBasedCriterion(damageBasedCriterion), _gradFactor(gradFactor),_useInitialFmean(useInitialFmean),
        _deltaOffset(deltaOffset),_h(h){};


CohesiveBand3DLaw::CohesiveBand3DLaw(const CohesiveBand3DLaw& source):
        Cohesive3DLaw(source),
        _sigmac(source._sigmac), _mu(source._mu), _fscmin(source._fscmin), _fscmax(source._fscmax), _beta(source._beta),
        _Kp(source._Kp),_Dc(source._Dc), _damageBasedCriterion(source._damageBasedCriterion),_gradFactor(source._gradFactor),
        _useInitialFmean(source._useInitialFmean),_deltaOffset(source._deltaOffset), _h(source._h){};


CohesiveBand3DLaw::~CohesiveBand3DLaw(){}


void CohesiveBand3DLaw::initLaws(const std::map<int,materialLaw*> &maplaw){};


void CohesiveBand3DLaw::createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_,
                                          const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  bool withGradJump = false;
  if (_gradFactor > 0)
  {
    withGradJump = true; 
  }
  IPVariable *ipvi = new CohesiveBand3DIPVariable(withGradJump);
  IPVariable *ipv1 = new CohesiveBand3DIPVariable(withGradJump);
  IPVariable *ipv2 = new CohesiveBand3DIPVariable(withGradJump);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void CohesiveBand3DLaw::createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  bool withGradJump = false;
  if (_gradFactor > 0)
  {
    withGradJump = true; 
  }
  ipv = new CohesiveBand3DIPVariable(withGradJump);
}


void CohesiveBand3DLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  // Check if cohesive zone is still active
  FractureCohesive3DIPVariable *fipv = static_cast < FractureCohesive3DIPVariable *> (ipv);
  const FractureCohesive3DIPVariable *fipvprev = static_cast < const FractureCohesive3DIPVariable *> (ipvp);

  if (fipvprev->isDeleted()){
    fipv->Delete(true);
  }
  else{
    if (fipv->get(IPField::DAMAGE) > 0.99999)
    {
      fipv->Delete(true);

      if (!fipvprev->isDeleted()){Msg::Error("Cohesive band gauss point deleted");}
    }
    else{
      fipv->Delete(false);
    }
  }
};


void CohesiveBand3DLaw::transferInterfaceDataToBulk(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff) const
{
// Transfer strains and others data before stress and interface force computation

  // Get fracture ipv
  FractureCohesive3DIPVariable *fipv = static_cast < FractureCohesive3DIPVariable *> (ipv);
  const FractureCohesive3DIPVariable *fipvprev = static_cast < const FractureCohesive3DIPVariable *> (ipvprev);

  // Set F_interface (current F_bulk by default without fracture)
  STensor3& F = fipv->getRefToDeformationGradient();

  /* In case of fracture: update variables and kinematic data for cohesive layer */
  if (fipv->isbroken())
  {
    // Saving current bulk strain
    static STensor3 F_bulk;
    F_bulk = F;

    /* Update data from previous steps */
    fipv->getRefToBulkDeformationGradientAtFailureOnset() = fipvprev->getConstRefToBulkDeformationGradientAtFailureOnset();
    fipv->setNonLocalToLocal(true);

    // Get cohesive ipv
    CohesiveBand3DIPVariable* cohIpv = dynamic_cast<CohesiveBand3DIPVariable*>(fipv->getIPvFrac());
    const CohesiveBand3DIPVariable* cohIpvprev = dynamic_cast<const CohesiveBand3DIPVariable*>(fipvprev->getIPvFrac());
    if (cohIpv == NULL){Msg::Error("CohesiveBand3DIPVariable must be used in LinearCohesive3DLaw::stress");}


    /* Get and update kinematic data */
    // Compute jump for cohesive layer
    SVector3& ujump0 = cohIpv->getRefToDGJump();              // Transfer DG jump at crack insertion due to DG formalism
    ujump0 = cohIpvprev->getConstRefToDGJump();

    SVector3& jump = cohIpv->getRefToCohesiveJump();          // Effective cohesive jump
    const SVector3& ujump = fipv->getConstRefToJump();        // Actual jump from DG formalism
    jump = ujump;
    jump -= ujump0;

    // Get jump gradient
    STensor3& Gradjump0 = cohIpv->getRefToJumpGradientAtFailureOnset();    // Transfer gradJump at insertion time due to DG formalism
    Gradjump0 = cohIpvprev->getConstRefToJumpGradientAtFailureOnset();
    const STensor3& Gradjump = fipv->getConstRefToJumpGradient();         // Get actual gradJump

    // Get cohesive basis unitary vector: take value from previous to avoid derivatives of current vectors
    const SVector3& n_current = cohIpvprev->getConstRefToCohesiveNormal();
    const SVector3& t_current = cohIpvprev->getConstRefToCohesiveTangent();
    const SVector3& s_current = cohIpvprev->getConstRefToCohesiveBiTangent();
    const SVector3& n_ref = cohIpv->getConstRefToCohesiveReferenceNormal();
    const SVector3& t_ref = cohIpv->getConstRefToCohesiveReferenceTangent();
    const SVector3& s_ref = cohIpv->getConstRefToCohesiveReferenceBiTangent();
    //Msg::Info("n current %e %e %e and n ref %e %e %e", n_current(0), n_current(1), n_current(2), n_ref(0), n_ref(1), n_ref(2) );


    // Compute jump component and set tension flag
    double jumpN(0.), jumpT(0.), jumpS(0.);
    for(int i=0; i<3;i++)
    {
      jumpN += jump(i)*n_current(i);
      jumpT += jump(i)*t_current(i);
      jumpS += jump(i)*s_current(i);
    }
    if(jumpN >= 0.){fipv->setTensionFlag(true);}
    else{fipv->setTensionFlag(false);}




    /* Add contribution from jump */
    for (int i=0; i<3; i++)
    {
        for (int j=0; j<3; j++)
        {
            F(i,j) += (jump(i))*n_ref(j)/_h;
        }
    }


    /* Add contribution from jump gradient */
    for (int i=0; i<3; i++)
    {
        for (int j=0; j<3; j++)
        {
            F(i,j) += _gradFactor*(Gradjump(i,j) - Gradjump0(i,j));

        }
    }


    // Compute tangent derivatives
    if(stiff)
    {
      // partial F_band_ij / partiak F bulk_kl = delta_ik delta_jl
      STensor43& dFband_dF = cohIpv->getRefToDInterfaceDeformationGradientDDeformationGradient();
      STensorOperation::zero(dFband_dF);
      for (int i=0; i<3; i++)
      {
        for (int j=0; j<3; j++)
        {
          for (int k=0; k<3; k++)
          {
            for (int l=0; l<3; l++)
            {
              if(i==k and j==l)
              {
                dFband_dF(i,j,k,l) = 1.;
              }
            }
          }
        }
      }

      // partial F_band_ij / partial jump_k = n_j / h
      STensor33& dFband_djump = cohIpv->getRefToDInterfaceDeformationGradientDJump();
      STensorOperation::zero(dFband_djump);
      for (int i=0; i<3; i++)
      {
        for (int j=0; j<3; j++)
        {
          for (int k=0; k<3; k++)
          {
            if(i==k)
            {
              dFband_djump(i,j,k) = n_ref(j)/_h;
            }
          }
        }
      }
      // partial F_band_ij / partial grad jump _kl = _gradFactor delta_ik delta_jl
      STensor43& dFband_dGradjump = cohIpv->getRefToDInterfaceDeformationGradientDJumpGradient();
      STensorOperation::zero(dFband_dGradjump);
      for (int i=0; i<3; i++)
      {
        for (int j=0; j<3; j++)
        {
          for (int k=0; k<3; k++)
          {
            for (int l=0; l<3; l++)
            {
              if(i==k and j==l)
              {
                dFband_dGradjump(i,j,k,l) = _gradFactor;
              }
            }
          }
        }
      }
   }

   /* Update deformation gradient by replacing by initial normal part with initial
   (at crack insertion time) deformation gradient tensor if asked */
   if(_useInitialFmean)
   {
     // Get initial deformation gradient
     const STensor3& F_bulk0 = fipv->getConstRefToBulkDeformationGradientAtFailureOnset();

     // Determine positive direction of tangential loading
     double gamma_t(0.), gamma_s(0.);
     for (int i=0; i<3; i++){
         for (int j=0; j<3; j++)
         {
         gamma_t += t_current(i)*F_bulk0(i,j)*n_ref(j);
         gamma_s += s_current(i)*F_bulk0(i,j)*n_ref(j);
         }
     }
     if (gamma_t < 0.){gamma_t = -1.;}
     else {gamma_t = 1.;}
     if (gamma_s < 0.){gamma_s = -1.;}
     else {gamma_s = 1.;}

     // Determine if crack is loading/unloading in specific directions
     double factor_Fn(0.), factor_Ft(0.), factor_Fs(0.);
     double& delta0 = cohIpv->getRefToOpeningOffset();
     delta0 = cohIpvprev->getConstRefToOpeningOffset();

        // Check in normal direction
     const double& deltamax_prev_n = cohIpvprev->getConstRefToNormalMaxOpening();
     if (jumpN+delta0 >= deltamax_prev_n)
     {
            // In case of increasing opening: update maximal opening and keep loading factor to 1
         cohIpv->getRefToNormalMaxOpening() = jumpN+delta0;
         factor_Fn = 1.;
     }
     else
     {
            // In case of decreasing opening: keep previous maximal opening value and compute loading factor
         cohIpv->getRefToNormalMaxOpening() = deltamax_prev_n;
         factor_Fn = (jumpN+delta0)/deltamax_prev_n;
        //if (factor_Ft < -1.){factor_Ft = -1.;}
     }

        // Check in tangent direction
     const double& deltamax_prev_t = cohIpvprev->getConstRefToTangentMaxOpening();
     double delta_t = jumpT*gamma_t+delta0;
     if (delta_t >= deltamax_prev_t)
     {
         cohIpv->getRefToTangentMaxOpening() = delta_t;
         factor_Ft = 1.;
     }
     else if ( -delta_t >= deltamax_prev_t)
     {
         cohIpv->getRefToTangentMaxOpening() = -delta_t;
         factor_Ft = -1.;
     }
     else
     {
         cohIpv->getRefToTangentMaxOpening() = deltamax_prev_t;
         factor_Ft = delta_t/deltamax_prev_t;
     }
        // Check in bitangent direction
     const double& deltamax_prev_s = cohIpvprev->getConstRefToBiTangentMaxOpening();
     double delta_s = jumpS*gamma_s+delta0;
     if (delta_s >= deltamax_prev_s)
     {
         cohIpv->getRefToBiTangentMaxOpening() = delta_s;
         factor_Fs = 1.;
     }
     else if (-delta_s >= deltamax_prev_s)
     {
         cohIpv->getRefToBiTangentMaxOpening() = -delta_s;
         factor_Fs = -1.;
     }
     else
     {
         cohIpv->getRefToBiTangentMaxOpening() = deltamax_prev_s;
         factor_Fs = delta_s/deltamax_prev_s;
     }


     // Compute projected correction
     double dF0_nn(0.), dF0_tn(0.), dF0_sn(0.);
     for (int i=0; i<3; i++)
     {
       for (int j=0; j<3; j++)
       {
         if(i==j)
         {
            dF0_nn += n_current(i)*((F_bulk0(i,j)-1.)*factor_Fn - (F_bulk(i,j)-1.))*n_ref(j);
            dF0_tn += t_current(i)*((F_bulk0(i,j)-1.)*factor_Ft - (F_bulk(i,j)-1.))*n_ref(j);
            dF0_sn += s_current(i)*((F_bulk0(i,j)-1.)*factor_Fs - (F_bulk(i,j)-1.))*n_ref(j);
         }
         else
         {
            dF0_nn += n_current(i)*(F_bulk0(i,j)*factor_Fn - F_bulk(i,j))*n_ref(j);
            dF0_tn += t_current(i)*(F_bulk0(i,j)*factor_Ft - F_bulk(i,j))*n_ref(j);
            dF0_sn += s_current(i)*(F_bulk0(i,j)*factor_Fs - F_bulk(i,j))*n_ref(j);
         }
       }
     }
     // Add then correction to F in interface basis
     for (int i=0; i<3; i++)
     {
       for (int j=0; j<3; j++)
       {
          F(i,j) += n_current(i)*dF0_nn*n_ref(j);
          F(i,j) += t_current(i)*dF0_tn*n_ref(j);
          F(i,j) += s_current(i)*dF0_sn*n_ref(j);
       }
     }

     // Update tangents
     if(stiff)
     {
       STensor43& dFband_dF = cohIpv->getRefToDInterfaceDeformationGradientDDeformationGradient();
       for (int i=0; i<3; i++)
       {
         for (int j=0; j<3; j++)
         {
           for (int k=0; k<3; k++)
           {
             for (int l=0; l<3; l++)
             {
               dFband_dF(i,j,k,l) += -n_current(i)*n_current(k)*n_ref(l)*n_ref(j)
                                     -t_current(i)*t_current(k)*n_ref(l)*n_ref(j)
                                     -s_current(i)*s_current(k)*n_ref(l)*n_ref(j);
             }
           }
         }
       }

       STensor33& dFband_djump = cohIpv->getRefToDInterfaceDeformationGradientDJump();
       double dF0du_n(0.), dF0du_t(0.), dF0du_s(0.);
       STensor3 I(1.);

       for (int l=0; l<3; l++)
       {
         for (int p=0; p<3; p++)
         {
           if (factor_Fn < 1.) { dF0du_n += n_current(l)*n_ref(p)*(F_bulk0(l,p)-I(l,p))/deltamax_prev_n; };
           if (factor_Ft < 1. & factor_Ft > -1.) { dF0du_t += t_current(l)*n_ref(p)*(F_bulk0(l,p)-I(l,p))*gamma_t/deltamax_prev_t; };
           if (factor_Fs < 1. & factor_Fs > -1.) { dF0du_s += s_current(l)*n_ref(p)*(F_bulk0(l,p)-I(l,p))*gamma_s/deltamax_prev_s; };
         }
       }



       for (int i=0; i<3; i++)
       {
         for (int j=0; j<3; j++)
         {
           for (int k=0; k<3; k++)
           {
             dFband_djump(i,j,k) += n_current(i)*n_ref(j)*n_current(k)*dF0du_n
                                   +t_current(i)*n_ref(j)*t_current(k)*dF0du_t
                                   +s_current(i)*n_ref(j)*s_current(k)*dF0du_s;
           }
         }
       }
    }
    }
  }
};





void CohesiveBand3DLaw::stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff, const bool checkfrac, const bool dTangent)
{
// Compute stress tensor and interface force after crack insertion
  // Get fracture ipv
  FractureCohesive3DIPVariable *fipv = dynamic_cast < FractureCohesive3DIPVariable *> (ipv);
  const FractureCohesive3DIPVariable *fipvprev = dynamic_cast < const FractureCohesive3DIPVariable *> (ipvprev);

  /* As the bulk part (stress + ipv) is already computed, the interface part is estimated from bulk part as follows: */

  // Cohesive data
  CohesiveBand3DIPVariable* cohIpv = dynamic_cast<CohesiveBand3DIPVariable*>(fipv->getIPvFrac());
  const CohesiveBand3DIPVariable* cohIpvprev = dynamic_cast<const CohesiveBand3DIPVariable*>(fipvprev->getIPvFrac());

  /* In case of full fracture */
  if (fipvprev->isDeleted())
  {

    // Full fracture: no force
    STensorOperation::zero(fipv->getRefToInterfaceForce());
    STensorOperation::zero(fipv->getRefToDInterfaceForceDjump());
    STensorOperation::zero(fipv->getRefToDInterfaceForceDDeformationGradient());
    STensorOperation::zero(fipv->getRefToDInterfaceForceDjumpGradient());

    // Avoid penetration if crack closure with a penality force
    if (!fipv->ifTension())
    {
      //Msg::Error("Add penalty force after deletion ! \n");
      // Cohesive normal: take value from previous to avoid derivatives
      const SVector3& n_current = cohIpvprev->getConstRefToCohesiveNormal();

      // Add a normal force proportionnal to the normal jump and the penality parameter
      SVector3& interfaceForce = fipv->getRefToInterfaceForce();
      const SVector3& jump = cohIpv->getConstRefToCohesiveJump();

      // Compute normal part of jump
      double jumpN = 0.;
      for(int i=0; i<3;i++)
      {
        jumpN += jump(i)*n_current(i);
      }
      const double& delta0 = cohIpv->getConstRefToOpeningOffset();
      if (jumpN+delta0 < 0.)
      {
        //Msg::Error("Add penalty term in case of crack closing + compression");
        // Compute penalty force
        for (int i=0; i<3; i++)
        {
          interfaceForce(i) += -_Kp*n_current(i)*(jumpN+delta0)*(jumpN+delta0);
          //interfaceForce(i) += _Kp*n_current(i)*(jumpN+delta0);
        }

        // Compute tangent
        if (stiff)
        {
          STensor3& DinterfaceForceDjump = fipv->getRefToDInterfaceForceDjump();
          for (int i=0; i<3; i++)
          {
            for (int j=0; j<3; j++)
            {
              //DinterfaceForceDjump(i,j) += _Kp*n_current(i)*n_current(j);
              DinterfaceForceDjump(i,j) += -_Kp*n_current(i)*n_current(j)*2.0*(jumpN+delta0);
            }
          }
        }
      }
    }

    // Path following only
    cohIpv->getRefToIrreversibleEnergy() = cohIpvprev->irreversibleEnergy();
    if (stiff)
    {
      STensorOperation::zero(cohIpv->getRefToDIrreversibleEnergyDDeformationGradient());
      STensorOperation::zero(cohIpv->getRefToDIrreversibleEnergyDJump());
      STensorOperation::zero(cohIpv->getRefToDIrreversibleEnergyDJumpGradient());
    }
  }
  else
  {
  /* If cohesive zone still active */
    //Get cohesive normal
        // Reference out normal to compute interface traction
    const SVector3& n_ref = cohIpv->getConstRefToCohesiveReferenceNormal();
        // Previous outward normal is used to avoid its derivatives
    const SVector3& n_current = cohIpvprev->getConstRefToCohesiveNormal();


    // Compute interface force from band stress tensor
    SVector3& interfaceForce = fipv->getRefToInterfaceForce();
    STensorOperation::zero(interfaceForce);
    const STensor3& P = fipv->getConstRefToFirstPiolaKirchhoffStress();
    for (int i=0; i<3; i++)
    {
        for (int j=0; j<3; j++)
        {
            interfaceForce(i) += P(i,j)*n_ref(j);
        }
    }

    // In case of crack closure in compression : add penalty term
    //double traction_force(0.);
    //for (int i=0; i<3; i++){ traction_force += interfaceForce(i)*n_current(i);}
    if (!fipv->ifTension())
    {
        // Compute normal part
        double jumpN(0.);
        const SVector3& jump = cohIpv->getConstRefToCohesiveJump();
        for(int i=0; i<3;i++)
        {
            jumpN += jump(i)*n_current(i);
        }
        const double& delta0 = cohIpv->getConstRefToOpeningOffset();
        if (jumpN+delta0 < 0.)
        {
            //Msg::Error("Add penalty term in case of crack closing + compression");
            // Compute penalty force
            for (int i=0; i<3; i++)
            {
                interfaceForce(i) += -_Kp*n_current(i)*(jumpN+delta0)*(jumpN+delta0);
                //interfaceForce(i) += _Kp*n_current(i)*(jumpN+delta0);
            }
        }
    }

    /* Compute tangent data in case of active cohesive zone */
    if (stiff)
    {
      // Dependance of traction on the deformation gradient
      STensor33& DinterfaceForceDF = fipv->getRefToDInterfaceForceDDeformationGradient();
      STensorOperation::zero(DinterfaceForceDF);

      const STensor43&  dP_dFband = fipv->getConstRefToTangentModuli();
      const STensor43&  dFband_dF = cohIpv->getConstRefToDInterfaceDeformationGradientDDeformationGradient();
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              for (int m=0; m<3; m++){
                for (int n=0; n<3; n++){
                  DinterfaceForceDF(i,j,k) += n_ref(l)*dP_dFband(i,l,m,n)*dFband_dF(m,n,j,k);
                }
              }
            }
          }
        }
      }

      // Dependance of traction on the jump
      STensor3& DinterfaceforceDjump = fipv->getRefToDInterfaceForceDjump();
      STensorOperation::zero(DinterfaceforceDjump);

      const STensor33&  dFband_djump = cohIpv->getConstRefToDInterfaceDeformationGradientDJump();
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              for (int m=0; m<3; m++){
                  DinterfaceforceDjump(i,k) += n_ref(j)*dP_dFband(i,j,l,m)*dFband_djump(l,m,k);
              }
            }
          }
        }
      }

      // Dependance of traction on the gradient jump
      STensor33& DinterfaceforceDjumpGrad = fipv->getRefToDInterfaceForceDjumpGradient();
      STensorOperation::zero(DinterfaceforceDjumpGrad);

      const STensor43& dFband_dGradjump = cohIpv->getConstRefToDInterfaceDeformationGradientDJumpGradient();
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              for (int m=0; m<3; m++){
                for (int n=0; n<3; n++){
                  DinterfaceforceDjumpGrad(i,j,k) += n_ref(l)*dP_dFband(i,l,m,n)*dFband_dGradjump(m,n,j,k);
                }
              }
            }
          }
        }
      }

      // Penality term in case of crack closure in compression
      if ( (!fipv->ifTension())  )
      {
        const SVector3& jump = cohIpv->getConstRefToCohesiveJump();
        double jumpN(0.);
        for(int i=0; i<3;i++){jumpN += jump(i)*n_current(i);}

        const double& delta0 = cohIpv->getConstRefToOpeningOffset();
        if(jumpN+delta0 < 0.)
        {
          for (int i=0; i<3; i++)
          {
            for (int j=0; j<3; j++)
            {
              //DinterfaceforceDjump(i,j) += _Kp*n_current(i)*n_current(j);
              DinterfaceforceDjump(i,j) += -_Kp*n_current(i)*n_current(j)*2.0*(jumpN+delta0);
            }
          }
        }
      }
    }
    /*
    // Path following only
    cohIpv->getRefToIrreversibleEnergy() = cohIpvprev->irreversibleEnergy() + (fipv->getIPvBulk()->irreversibleEnergy()-fipv->getIPvBulk()->irreversibleEnergy())*_h;
    if (stiff)
    {
      STensor3& dirrEnergy_dF = cohIpv->getRefToDIrreversibleEnergyDDeformationGradient();
      STensorOperation::zero(dirrEnergy_dF);

      const STensor3& dirrEnergy_dFband = fipv->getConstRefToDIrreversibleEnergyDDeformationGradient();
      const STensor43& dFband_dF = cohIpv->getConstRefToDInterfaceDeformationGradientDDeformationGradient();
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              dirrEnergy_dF(i,j) += dirrEnergy_dFband(k,l)*dFband_dF(k,l,i,j);
            }
          }
        }
      }
      dirrEnergy_dF *= _h;


      SVector3& dirrEnergy_djump = cohIpv->getRefToDIrreversibleEnergyDJump();
      STensorOperation::zero(dirrEnergy_djump);
      const STensor33&  dFband_djump = cohIpv->getConstRefToDInterfaceDeformationGradientDJump();
      for (int i=0; i<3; i++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            dirrEnergy_djump(i) += dirrEnergy_dFband(k,l)*dFband_djump(k,l,i);
          }
        }
      }
      dirrEnergy_djump *= _h;


      STensor3& dirrEnergy_dGradjump = cohIpv->getRefToDIrreversibleEnergyDJumpGradient();
      STensorOperation::zero(dirrEnergy_dGradjump);
      const STensor43& dFband_dGradjump = cohIpv->getConstRefToDInterfaceDeformationGradientDJumpGradient();
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              dirrEnergy_dGradjump(i,j) += dirrEnergy_dFband(k,l)*dFband_dGradjump(k,l,i,j);
            }
          }
        }
      }
      dirrEnergy_dGradjump *= _h;

      //Temporaire !!!!!!!!!!
      fipv->getRefToDIrreversibleEnergyDDeformationGradient() = cohIpv->getConstRefToDIrreversibleEnergyDDeformationGradient();
    }*/


    // Path following only - alternative way - not working yet
    SVector3 interfaceForce_prev(0.);
    STensorOperation::zero(interfaceForce_prev);
    const SVector3& jump = cohIpv->getConstRefToCohesiveJump();
    const SVector3& jump_prev = cohIpvprev->getConstRefToCohesiveJump();
    const STensor3& P_prev = fipvprev->getConstRefToFirstPiolaKirchhoffStress();
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        interfaceForce_prev(i) += P_prev(i,j)*n_ref(j);
      }
    }

    cohIpv->getRefToIrreversibleEnergy() = cohIpvprev->irreversibleEnergy();
    for (int k=0; k<3; k++)
    {
      cohIpv->getRefToIrreversibleEnergy() += 0.5*( interfaceForce(k)*(jump(k)-jump_prev(k)) - jump(k)*(interfaceForce(k)- interfaceForce_prev(k)) );
    }
    if (stiff)
    {
      STensor3& dirrEnergy_dF = cohIpv->getRefToDIrreversibleEnergyDDeformationGradient();
      STensorOperation::zero(dirrEnergy_dF);

      const STensor33& DinterfaceForceDF = fipv->getConstRefToDInterfaceForceDDeformationGradient();
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){
            dirrEnergy_dF(i,j) += 0.5*( DinterfaceForceDF(k,i,j)*(jump(k)-jump_prev(k)) - jump(k)*DinterfaceForceDF(k,i,j) );
          }
        }
      }

      SVector3& dirrEnergy_djump = cohIpv->getRefToDIrreversibleEnergyDJump();
      STensorOperation::zero(dirrEnergy_djump);

      const STensor3& DinterfaceforceDjump = fipv->getConstRefToDInterfaceForceDjump();
      for (int i=0; i<3; i++){
        for (int k=0; k<3; k++){
          dirrEnergy_djump(i) += 0.5*( DinterfaceforceDjump(k,i)*(jump(k)-jump_prev(k)) - jump(k)*DinterfaceforceDjump(k,i) );
        }
        dirrEnergy_djump(i) += 0.5*( interfaceForce_prev(i) );
      }
      //dirrEnergy_djump *= _h;


      STensor3& dirrEnergy_dGradjump = cohIpv->getRefToDIrreversibleEnergyDJumpGradient();
      const STensor33& DinterfaceforceDjumpGrad = fipv->getConstRefToDInterfaceForceDjumpGradient();
      STensorOperation::zero(dirrEnergy_dGradjump);
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){
            dirrEnergy_dGradjump(i,j) += 0.5*( DinterfaceforceDjumpGrad(k,i,j)*(jump(k)-jump_prev(k)) - jump(k)*DinterfaceforceDjumpGrad(k,i,j) );
          }
        }
      }
    }
  }
};



void CohesiveBand3DLaw::checkCohesiveInsertion(IPStateBase* ipsm, IPStateBase* ipsp, const bool forcedInsert) const
{
  // check failure onset from both negative and postive IPStates
  FractureCohesive3DIPVariable* fMinusCur = dynamic_cast<FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::current));
  FractureCohesive3DIPVariable* fPlusCur = dynamic_cast<FractureCohesive3DIPVariable*>(ipsp->getState(IPStateBase::current));
  const FractureCohesive3DIPVariable* fPlusPrev = dynamic_cast<const FractureCohesive3DIPVariable*>(ipsp->getState(IPStateBase::previous));
  const FractureCohesive3DIPVariable* fMinusPrev = dynamic_cast<const FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::previous));
  if (fMinusPrev->isbroken() or fPlusPrev->isbroken())
  {
    Msg::Warning("Previous IPs are broken in CohesiveBand3DLaw::checkCohesiveInsertion");
    fMinusCur->broken();
    fPlusCur->broken();
    return;
  }

  // get random scatter and average factor
  double facminus = fMinusCur->fractureStrengthFactor();    // scatter factor
  double facplus =  fPlusCur->fractureStrengthFactor();
  double gamma = 0.5;                                       // average factor

  /* Compute critical damage value */
  const double effDc = (gamma*facminus+ (1.-gamma)*facplus) * _Dc;
  // Get effective damage
  double damageMinus = fMinusCur->get(IPField::DAMAGE);
  double damagePlus = fPlusCur->get(IPField::DAMAGE);
  double damage = gamma*damageMinus + (1.-gamma)*damagePlus;


  /* Compute critical stress value */
  const double effsigc = (gamma*facminus+ (1.-gamma)*facplus) * _sigmac;
  // get Cauchy stress on negative IP
  static STensor3 cauchyMinus;
  fMinusCur->getCauchyStress(cauchyMinus);
  // get Cauchy stress on positive IP
  static STensor3 cauchyPlus;
  fPlusCur->getCauchyStress(cauchyPlus);
  // get mean value using gamma
  static STensor3 cauchyAverage;
  for (int i=0; i<3; i++)
  {
    for (int j=0; j<3; j++)
    {
      cauchyAverage(i,j) = gamma*cauchyMinus(i,j) + (1.-gamma)*cauchyPlus(i,j);
    }
  }

  /* Decomposition of traction vector */
  double tractionSq = 0.;   // square interface force
  double snor = 0.;         // normal part
  double tau = 0.;          // sliding part
  static SVector3 cohesiveNormal; // because this step is check to activate the cohesive law for the next time step
                                  // current outward normal is used
  cohesiveNormal = fMinusCur->getConstRefToCurrentOutwardNormal();
  cohesiveNormal.normalize();

  static SVector3 force;   // interface force vector
  for (int i=0; i<3; i++)
  {
    force(i) = 0.;
    for (int j=0; j<3; j++)
    {
      force(i) += cauchyAverage(i,j)*cohesiveNormal(j);
    }
  }

  for (int i=0; i<3; i++)
  {
    tractionSq += force(i)*force(i);
    snor += force(i)*cohesiveNormal(i);
  }
  tau = sqrt(tractionSq-snor*snor);
  double traction = sqrt(tractionSq); // norm of traction vector

  /* compute effective stress Camacho and Ortiz) */
  bool ift = true;  // tension flag
  double seff = 0.; // effective stress when failure

  if(snor >= 0.)
  {
    // traction case
    ift = true;
    seff = sqrt(snor*snor+tau*tau/(_beta*_beta));
  }
  else
  {
    // compression case
    ift = false;

    //no fracture in compression if mu<0
    if(_mu < 0.)
    {
      seff = 0.;
    }
    else
    {
      double temp = fabs(tau)-_mu*fabs(snor); // incorporate friction force
      if (temp > 0.)
      {
          seff = temp/_beta;
      }
      else
      {
          seff = 0.;
      }
    }
  }


  //broken check according to effective sttress (Ling Wu)
  bool broken = false;
  if (_damageBasedCriterion)
  {
    if (damage > effDc)
    {
      broken = true;
    }
  }
  else
  {
    if ( (seff/(1.0-damage)) > effsigc )
    {
      broken = true;
    }
  }


  if (broken or forcedInsert)
  {
    /* If fracture detected */
    if (forcedInsert){Msg::Info("Force crack insertion at one GP");};
    Msg::Info("Broken damage = %e",damage);
    fMinusCur->broken();
    fPlusCur->broken();

    if (ift==false){Msg::Error(" ! Crack insertion in compression !");};


    // activate nonlocal to local transition
    fMinusCur->setNonLocalToLocal(true);
    fPlusCur->setNonLocalToLocal(true);

    // initialize cohesive law in negative part
    Cohesive3DIPVariableBase* coMinus = fMinusCur->getIPvFrac();
    fMinusCur->initializeFracture(fMinusCur->getConstRefToJump(),getKp(),seff,0.,0.,0.,1.,ift,cauchyMinus,getKp(),cohesiveNormal,force,
                                fMinusCur->getIPvBulk());

    // positive part take a same value as negative cohesive part
    Cohesive3DIPVariableBase* coPlus = fPlusCur->getIPvFrac();
    coPlus->initializeFracture(fPlusCur->getConstRefToJump(),getKp(),seff,0.,0.,0.,1.,ift,cauchyPlus,getKp(),cohesiveNormal,force,
                                fPlusCur->getIPvBulk());

    // set initial jump offset
    coMinus->getRefToOpeningOffset() = _deltaOffset;
    coPlus->getRefToOpeningOffset() = _deltaOffset;
  }
  else
  {
    /* no fracture detected */
    fMinusCur->nobroken();
    fPlusCur->nobroken();
  }
};


// -------------- PorosityCohesiveBand3DLaw
PorosityCohesiveBand3DLaw::PorosityCohesiveBand3DLaw(const int num, const double h, const double Kp, const bool accountNormalBulk):
    Cohesive3DLaw(num,true),
    _h(h),_Kp(Kp),
    _accountNormalBulk(accountNormalBulk), _enabledNegativeJumpN_limit(false),_maxPenetrationjumpN(-1.e-8),
    _enableDeletionOfBadConditionedIPVs(false), _bulkLaw(NULL),_gradFactor(0.),
    _crackDelayingFactor(1.)
{

};

void PorosityCohesiveBand3DLaw::accountJumpGradientAtInterfaces(const double factor)
{
  _gradFactor = factor;
  if (_gradFactor >0)
  {
    Msg::Info("grad of jump will be accounted for at interfaces with factor = %e",_gradFactor);
  }
}


PorosityCohesiveBand3DLaw::PorosityCohesiveBand3DLaw(const PorosityCohesiveBand3DLaw& src):
    Cohesive3DLaw(src),
    _h(src._h),_Kp(src._Kp),
    _accountNormalBulk(src._accountNormalBulk), _enabledNegativeJumpN_limit(src._enabledNegativeJumpN_limit),
    _maxPenetrationjumpN(src._maxPenetrationjumpN),_enableDeletionOfBadConditionedIPVs(src._enableDeletionOfBadConditionedIPVs),
    _bulkLaw(src._bulkLaw),_gradFactor(src._gradFactor),_crackDelayingFactor(src._crackDelayingFactor)
{
};


void PorosityCohesiveBand3DLaw::setBulkLaw(const NonLocalPorousDuctileDamageDG3DMaterialLaw& bulkLaw)
{
  printf("set bulk law for the interface law %d \n",this->getNum());;
  _bulkLaw = &bulkLaw;
};

void PorosityCohesiveBand3DLaw::initLaws(const std::map<int,materialLaw*> &maplaw)
{
  if (_bulkLaw == NULL)
  {
    printf("initialize PorosityCohesiveBand3DLaw::initLaws\n");
    for(std::map<int,materialLaw*>::const_iterator it = maplaw.begin(); it != maplaw.end(); ++it)
    {
      // find fracture law
      const FractureByCohesive3DLaw* mlawFrac = dynamic_cast<const FractureByCohesive3DLaw*>(it->second);
      if (mlawFrac != NULL)
      {
        if (mlawFrac->fractureLawNumber() == this->getNum())
        {
          for(std::map<int,materialLaw*>::const_iterator it2 = maplaw.begin(); it2 != maplaw.end(); ++it2)
          {
            if (mlawFrac->bulkLawNumber() == it2->first)
            {
              _bulkLaw = dynamic_cast<const NonLocalPorousDuctileDamageDG3DMaterialLaw*>(it2->second);
              break;
            }
          }
          
          if (_bulkLaw == NULL)
          {
            Msg::Error("found bulk law %d but type is not correct",mlawFrac->getBulkLaw()->getNum());
          }
          else
          {
            Msg::Info("found bulk law");
          }
          break;
        }
      }
    }
    if (_bulkLaw == NULL)
    {
      Msg::Error("bulk law used with interface law %d cannot be found",this->getNum());
    }
  }
};


void PorosityCohesiveBand3DLaw::setMaximalAdmissiblePenetrationJump(bool activationFlag, double maxNegJump){
  _enabledNegativeJumpN_limit = activationFlag;
  _maxPenetrationjumpN = maxNegJump;
  if (maxNegJump > 0.){
    Msg::Error("PorosityCohesiveBand3DLaw::the admissible value shloud be negative instead of %e",maxNegJump);
  }

  if (_enabledNegativeJumpN_limit){
    Msg::Info("PorosityCohesiveBand3DLaw:: the normal penetration jump is limited to %e",maxNegJump);
  }
  else{
     Msg::Info("PorosityCohesiveBand3DLaw:: the normal penetration jump is not limited");
  }
}

void PorosityCohesiveBand3DLaw::setDeletionOfBadConditionnedIPV(bool activationFlag){
  _enableDeletionOfBadConditionedIPVs = activationFlag;
  if (_enableDeletionOfBadConditionedIPVs){
    Msg::Info("PorosityCohesiveBand3DLaw:: the bad-conditionned ipvs will be deleted");
  }
  else{
     Msg::Info("PorosityCohesiveBand3DLaw:: the bad-conditionned ipvs will NOT be deleted");
  }
}


void PorosityCohesiveBand3DLaw::createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_,
                                              const MElement *ele, const int nbFF_, const IntPt *GP,
                                              const int gpt) const
{
  bool withGrad = false;
  if (_gradFactor > 0)
    withGrad = true;
  IPVariable *ipvi = new CohesiveBand3DIPVariable(withGrad);
  IPVariable *ipv1 = new CohesiveBand3DIPVariable(withGrad);
  IPVariable *ipv2 = new CohesiveBand3DIPVariable(withGrad);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}


void PorosityCohesiveBand3DLaw::createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  bool withGrad = false;
  if (_gradFactor > 0)
    withGrad = true;
  ipv = new CohesiveBand3DIPVariable(withGrad);
}

void PorosityCohesiveBand3DLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const
{
  // Determine if the ipv is deleted or not.
  /// if deleted, nothing is computed, except strains and penalty terms
  /// otherwise, compute interface force and their derivatives from ipv bulk information

  // get needed ipv
  FractureCohesive3DIPVariable *fipv = static_cast < FractureCohesive3DIPVariable *> (ipv);
  const FractureCohesive3DIPVariable *fipvprev = static_cast < const FractureCohesive3DIPVariable *> (ipvp);

  if (fipvprev->isDeleted()){
    // If ipv is already deleted, it always stays deleted
    fipv->Delete(true);
  }
  else{
    // If the ipv is not deleted, check if a criterion is satisfied

    // get needed ipvbulk
    const	nonLocalPorosityDG3DIPVariable* ipvbulk = dynamic_cast<nonLocalPorosityDG3DIPVariable*>(fipv->getIPvBulk());

    // 1st deletion criterion:
      // interfaceIP should be deleted after satisfying failure criterion
    if (ipvbulk->getIPNonLocalPorosity()->isFailed()){
      fipv->Delete(true);
      Msg::Info("ipv is deleted");
    }
    // 2nd deletion criterion:
      // interfaceIP should be deleted if considred as bad conditionned
    else if (_enableDeletionOfBadConditionedIPVs){
      if (ipvbulk->getIPNonLocalPorosity()->getYieldPorosity() < 0.75*ipvbulk->getIPNonLocalPorosity()->getInitialPorosity()){
        fipv->Delete(true);
        Msg::Error("ipv is deleted due to too much compression: fv= %e", ipvbulk->getIPNonLocalPorosity()->getYieldPorosity());
      }
      else if(ipvbulk->getJ() < 1.e-8){
        Msg::Error("ipv is deleted due to negative jacobian: J= %e", ipvbulk->getJ());
        fipv->Delete(true);
      }
    }
    else{
      // if none of criterion is satisfied : the ipv is not deleted
      fipv->Delete(false);
    }

  }
};


void PorosityCohesiveBand3DLaw::stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff, const bool checkfrac, const bool dTangent)
{
  // get ipvfrac
  FractureCohesive3DIPVariable *fipv = static_cast < FractureCohesive3DIPVariable *> (ipv);
  const FractureCohesive3DIPVariable *fipvprev = static_cast < const FractureCohesive3DIPVariable *> (ipvprev);

  // If the element is deleted, no force, excepted for contact (but not implemented)
  if (fipv->isDeleted())
  {
    // everything is zero
    STensorOperation::zero(fipv->getRefToInterfaceForce());
    fipv->getRefToIrreversibleEnergy() = 0.;

    // but do something if contact
    static SVector3 curN;
    curN = fipvprev->getConstRefToCurrentOutwardNormal();
    curN.normalize();
    SVector3& T = fipv->getRefToInterfaceForce();
    const SVector3& ujump = fipv->getConstRefToJump();
    const SVector3& ujumpDG = fipv->getConstRefToDGJump();
    double realUJumpNormal = dot(ujump,curN) - dot(ujumpDG,curN);
    if (realUJumpNormal < 0.){
      fipv->setTensionFlag(false);
      if(fipvprev->ifTension()) Msg::Info("Penalty term added");
      for (int i=0; i<3; i++){
        T(i) += _Kp*realUJumpNormal*curN(i);
        //T(i) -= _Kp*realUJumpNormal*realUJumpNormal*curN(i);
      }
    }
    else{
      fipv->setTensionFlag(true);
    }


    if (stiff){
      // Everything equal zero unless contact
      STensorOperation::zero(fipv->getRefToDInterfaceForceDDeformationGradient());
      STensorOperation::zero(fipv->getRefToDInterfaceForceDjump());
      STensorOperation::zero(fipv->getRefToDInterfaceForceDjumpGradient());

      if (realUJumpNormal < 0.){
        STensor3& dTdjump = fipv->getRefToDInterfaceForceDjump();
        for (int i=0; i<3; i++){
          for (int j=0; j<3; j++){
            dTdjump(i,j) += _Kp*curN(i)*curN(j);
            //dTdjump(i,j) -= 2.*_Kp*realUJumpNormal*curN(i)*curN(j);
          }
        }
      }

      if (getMacroSolver()->withPathFollowing()){
        STensorOperation::zero(fipv->getRefToDIrreversibleEnergyDDeformationGradient());
        STensorOperation::zero(fipv->getRefToDIrreversibleEnergyDJump());
      }
    }
  }
  else
  {
    if (fipv->isbroken())
    {
      // get data
      dG3DIPVariableBase* mipv = static_cast<dG3DIPVariableBase*>(fipv->getIPvBulk());
      const dG3DIPVariableBase* mipvprev = static_cast<const dG3DIPVariableBase*>(fipvprev->getIPvBulk());

      CohesiveBand3DIPVariable* cohmipv = dynamic_cast<CohesiveBand3DIPVariable*>(fipv->getIPvFrac());
      const CohesiveBand3DIPVariable* cohmipvprev = dynamic_cast<const CohesiveBand3DIPVariable*>(fipvprev->getIPvFrac());

      static SVector3 refN;
      refN = fipv->getConstRefToReferenceOutwardNormal();
      refN.normalize();

      static SVector3 curN;
      curN = fipvprev->getConstRefToCurrentOutwardNormal();
      curN.normalize();

      // Compute traction force from band stress tensor
      const STensor3& P = fipv->getConstRefToFirstPiolaKirchhoffStress();
      SVector3& T = fipv->getRefToInterfaceForce();
      for (int i=0; i<3; i++){
        T(i) = 0.;
        for (int j=0; j<3; j++){
          T(i) += P(i,j)*refN(j);
        }
      }

      // add penalty to avoid penetration
      const SVector3& ujump = fipv->getConstRefToJump();
      const SVector3& ujumpDG = fipv->getConstRefToDGJump();

      double realUJumpNormal = dot(ujump,curN) - dot(ujumpDG,curN);
      if (realUJumpNormal < 0.){
        fipv->setTensionFlag(false);
        if(fipvprev->ifTension()) Msg::Info("Add penalty term");
        for (int i=0; i<3; i++){
          T(i) += _Kp*realUJumpNormal*curN(i);
          //T(i) -= _Kp*realUJumpNormal*realUJumpNormal*curN(i);
        }
      }
      else{
        fipv->setTensionFlag(true);
      }


      double& irr = fipv->getRefToIrreversibleEnergy();
      irr = mipv->irreversibleEnergy()*_h; //  irreversive energy store at interface IP after failure

      if (stiff)
      {
        // Compute interface forces derivatives from kinematic derivatives and constitutive tangent tensor
        // get kinematic derivatives
        const STensor43& dFinterfacedF = fipv->getConstRefToDInterfaceDeformationGradientDDeformationGradient();
        const STensor33& dFinterfacedJump = fipv->getConstRefToDInterfaceDeformationGradientDJump();
        const STensor43& dFinterfacedGradJump = cohmipv->getConstRefToDInterfaceDeformationGradientDJumpGradient();
        // get constitutive tangent
        const STensor43& dPdFinf = fipv->getConstRefToTangentModuli();
        // get interface force derivatives
        STensor33& dTdF = fipv->getRefToDInterfaceForceDDeformationGradient();
        STensor3& dTdjump = fipv->getRefToDInterfaceForceDjump();
        STensor33& dTdGradjump = fipv->getRefToDInterfaceForceDjumpGradient();

        // set to zero and compute...
        STensorOperation::zero(dTdF);
        STensorOperation::zero(dTdjump);
        STensorOperation::zero(dTdGradjump);

        for (int i=0; i<3; i++){
          for (int j=0; j<3; j++){
            for (int p=0; p<3; p++){
              for (int q=0; q<3; q++){
                for (int k=0; k<3; k++){
                  dTdjump(i,k) += dPdFinf(i,j,p,q)*refN(j)*dFinterfacedJump(p,q,k);
                  for (int l=0; l<3; l++){
                    dTdF(i,k,l) += dPdFinf(i,j,p,q)*refN(j)*dFinterfacedF(p,q,k,l);
                    dTdGradjump(i,k,l) += dPdFinf(i,j,p,q)*refN(j)*dFinterfacedGradJump(p,q,k,l);
                  }
                }
              }
            }
          }
        }

        if (realUJumpNormal < 0.){
          for (int i=0; i<3; i++){
            for (int j=0; j<3; j++){
              dTdjump(i,j) += _Kp*curN(i)*curN(j);
              //dTdjump(i,j) -= 2.*_Kp*realUJumpNormal*curN(i)*curN(j);
            }
          }
        };

				if (getMacroSolver()->withPathFollowing()){
					STensor3& dirrEnergDF = fipv->getRefToDIrreversibleEnergyDDeformationGradient();
					SVector3& dirrEnergDJump = fipv->getRefToDIrreversibleEnergyDJump();
		      STensorOperation::zero(dirrEnergDF);
		      STensorOperation::zero(dirrEnergDJump);
					const STensor3& dirrEnergdFinf =  mipv->getRefToDIrreversibleEnergyDDeformationGradient();
					for (int i=0; i<3; i++){
						for (int j=0; j<3; j++){
							for (int k=0; k<3; k++){
								dirrEnergDJump(k) += _h*dirrEnergdFinf(i,j)*dFinterfacedJump(i,j,k);
								for (int l=0; l<3; l++){
									dirrEnergDF(k,l) += _h*dirrEnergdFinf(i,j)*dFinterfacedF(i,j,k,l);
								}
							}
						}
					}
				}

		  }
		}
	}

};



void PorosityCohesiveBand3DLaw::setCrackDelayingFactor(double fact)
{
  _crackDelayingFactor = fact;
  Msg::Info("crack delaying factor %f is used",fact);
};

void PorosityCohesiveBand3DLaw::checkCohesiveInsertion(IPStateBase* ipsm, IPStateBase* ipsp, const bool forcedInsert) const
{
  // check failure onset from both negative and postive IPStates
	FractureCohesive3DIPVariable* fMinusCur = dynamic_cast<FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::current));
	const FractureCohesive3DIPVariable* fMinusPrev = dynamic_cast<const FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::previous));
	FractureCohesive3DIPVariable* fPlusCur = dynamic_cast<FractureCohesive3DIPVariable*>(ipsp->getState(IPStateBase::current));
	const FractureCohesive3DIPVariable* fPlusPrev = dynamic_cast<const FractureCohesive3DIPVariable*>(ipsp->getState(IPStateBase::previous));

	if (fMinusCur == NULL or fPlusCur == NULL or fMinusPrev==NULL or fPlusPrev==NULL){
		Msg::Error("FractureCohesive3DIPVariable must be used in  NonlocalPorosityCohesiveBand3DLaw::checkCohesiveInsertion");
	}

	if (fMinusPrev->isbroken() or fPlusPrev->isbroken()){
		Msg::Warning("Previous IPs are broken");
		fMinusCur->broken();
		fPlusCur->broken();
		return;
	}
  
  const nonLocalPorosityDG3DIPVariable* ipvmprev = dynamic_cast<const nonLocalPorosityDG3DIPVariable*>(fMinusPrev->getIPvBulk());
  const nonLocalPorosityDG3DIPVariable* ipvpprev = dynamic_cast<const nonLocalPorosityDG3DIPVariable*>(fPlusPrev->getIPvBulk());

  nonLocalPorosityDG3DIPVariable* ipvm = dynamic_cast<nonLocalPorosityDG3DIPVariable*>(fMinusCur->getIPvBulk());
  nonLocalPorosityDG3DIPVariable* ipvp = dynamic_cast<nonLocalPorosityDG3DIPVariable*>(fPlusCur->getIPvBulk());

  if (ipvm == NULL or ipvp == NULL){
    Msg::Error("nonLocalPorosityDG3DIPVariable must be used in NonlocalPorosityCohesiveBand3DLaw::checkCohesiveInsertion");
  }

  static SVector3 normDir;
  normDir = fPlusCur->getConstRefToCurrentOutwardNormal();
  normDir.normalize();
  bool minusFailed = _bulkLaw->checkCrackCriterion(ipvm,ipvmprev,normDir,_crackDelayingFactor);
  bool plusFailed = _bulkLaw->checkCrackCriterion(ipvp,ipvpprev,normDir,_crackDelayingFactor);

  if (minusFailed or plusFailed or forcedInsert)
  {
    if (minusFailed or plusFailed)
    {
      printf("rank %d: cohesive element is inserted \n",Msg::GetCommRank());
    }
    else if (forcedInsert)
    {
      printf("rank %d: cohesive element is forcedly inserted \n",Msg::GetCommRank());
    }
    fMinusCur->broken();
    fPlusCur->broken();
    
    // activate nonlocal to local transition
    fMinusCur->setNonLocalToLocal(true);
    fPlusCur->setNonLocalToLocal(true);
    
    // Get IP coalescence
    IPCoalescence& ipcolm = ipvm->getIPNonLocalPorosity()->getRefToIPCoalescence();
    IPCoalescence& ipcolp = ipvp->getIPNonLocalPorosity()->getRefToIPCoalescence();

    // Force IP to be in a coalescence state if not already in that state
    if (!ipcolm.getCoalescenceOnsetFlag())
    {
      ipcolm.getRefToCoalescenceMode() = ipcolp.getCoalescenceMode();
      _bulkLaw->forceCoalescence(ipvm,ipvmprev);
    }
    if (!ipcolp.getCoalescenceOnsetFlag())
    {
      ipcolp.getRefToCoalescenceMode() = ipcolm.getCoalescenceMode();
      _bulkLaw->forceCoalescence(ipvp,ipvpprev);
    }

    fMinusCur->getOnsetCriterion() = _bulkLaw->getNonLocalPorosityLaw()->getOnsetCriterion(ipvm->getIPNonLocalPorosity());
    fPlusCur->getOnsetCriterion() = _bulkLaw->getNonLocalPorosityLaw()->getOnsetCriterion(ipvp->getIPNonLocalPorosity());

    // initialize data for cohesive law in negative part
    static SVector3 zeroVec;
    static STensor3 zeroTen;
    Cohesive3DIPVariableBase* coMinus = fMinusCur->getIPvFrac();
    coMinus->initializeFracture(fMinusCur->getConstRefToJump(),getKp(),0.,0.,0.,0.,0.,true,zeroTen,getKp(),normDir,zeroVec,
                            fMinusCur->getIPvBulk());

    // positive part take a same value as negative cohesive part
    Cohesive3DIPVariableBase* coPlus = fPlusCur->getIPvFrac();
    coPlus->initializeFracture(fPlusCur->getConstRefToJump(),getKp(),0.,0.,0.,0.,0.,true,zeroTen,getKp(),normDir,zeroVec,
                            fPlusCur->getIPvBulk());

    /*
    // Check if ellipticity is lost in the direction of the crack using FailureDetection
    double dPdF_in_dirN_minus, dPdF_in_dirN_plus;
    static STensor43 DPDFm, DPDFp;
    DPDFm = ipvm->getConstRefToTangentModuli();
    DPDFp = ipvp->getConstRefToTangentModuli();
    if(_bulkLaw->getNonLocalPorosityLaw()->getNonLocalMethod() == 0){
      // Nothing to add
    }
    else if(_bulkLaw->getNonLocalPorosityLaw()->getNonLocalMethod() == 1){
      const STensor3& dPdfVm = ipvm->getConstRefToDStressDNonLocalVariable()[0];
      const STensor3& dfVdFm = ipvm->getConstRefToDLocalVariableDStrain()[0];
      const STensor3& dPdfVp = ipvp->getConstRefToDStressDNonLocalVariable()[0];
      const STensor3& dfVdFp = ipvp->getConstRefToDLocalVariableDStrain()[0];
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              DPDFm(i,j,k,l) += dPdfVm(i,j)*dfVdFm(k,l);
              DPDFp(i,j,k,l) += dPdfVp(i,j)*dfVdFp(k,l);
            }
          }
        }
      }
    }
    #ifdef _DEBUG
    else{
      Msg::Error("PorosityCohesiveBand3DLaw::  Ellipticity check for non-local method %d is not implemented", _bulkLaw->getNonLocalPorosityLaw()->getNonLocalMethod());
    }
    #endif // _DEBUG

    FailureDetection::getLostSolutionUniquenessCriterionFollowingDirection(3,DPDFm,normDir,dPdF_in_dirN_minus);
    if (dPdF_in_dirN_minus > 0.){
      Msg::Warning("PorosityCohesiveBand3DLaw:: insertion wo loss of ellipticity: N.dPdFp.N= %e",dPdF_in_dirN_plus);
    }
    FailureDetection::getLostSolutionUniquenessCriterionFollowingDirection(3,DPDFp,normDir,dPdF_in_dirN_plus);
    if (dPdF_in_dirN_plus > 0.){
      Msg::Warning("PorosityCohesiveBand3DLaw:: insertion wo loss of ellipticity: N.dPdFp.N= %e",dPdF_in_dirN_plus);
    }
     */

  }
  else
  {
    fMinusCur->nobroken();
    fPlusCur->nobroken();

    fMinusCur->setNonLocalToLocal(false);
    fPlusCur->setNonLocalToLocal(false);
  };
};



void PorosityCohesiveBand3DLaw::transferInterfaceDataToBulk(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff) const
{
  // Update kinematic data if the cohesive zone is active (== if broken), otherwise, nothing to do
  FractureCohesive3DIPVariable *fipv = static_cast < FractureCohesive3DIPVariable *> (ipv);
  const FractureCohesive3DIPVariable *fipvprev = static_cast < const FractureCohesive3DIPVariable *> (ipvprev);

  if (fipv->isbroken()){
    CohesiveBand3DIPVariable* cohIpv = dynamic_cast<CohesiveBand3DIPVariable*>(fipv->getIPvFrac());
    const CohesiveBand3DIPVariable* cohIpvprev = dynamic_cast<const CohesiveBand3DIPVariable*>(fipvprev->getIPvFrac());

    // Update deformation gradient for bulk law
    STensor3& F = fipv->getRefToDeformationGradient();

    // - start from previous
    F = fipvprev->getConstRefToDeformationGradient();

    // - add increment of the bulk F
    const STensor3& Fb0 = fipvprev->getBulkDeformationGradient();
    const STensor3& Fb = fipv->getBulkDeformationGradient();
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        F(i,j) += Fb(i,j) - Fb0(i,j);
      }
    }

    // - subtract normal contribution of the bulk F if asked
    static SVector3 refN;
    refN = fipv->getConstRefToReferenceOutwardNormal();
    refN.normalize();

    if (!_accountNormalBulk){
      static SVector3 dFNormal;
      for (int i=0; i<3; i++){
        dFNormal(i) = 0.;
        for (int j=0; j<3; j++){
          dFNormal(i) += (Fb(i,j) - Fb0(i,j))*refN(j);
        }
      }
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          F(i,j) -= dFNormal(i)*refN(j);
        }
      }
    }

    // - add jump contribution
    const SVector3& ujump = fipv->getConstRefToJump();
    const SVector3& ujump0 = fipvprev->getConstRefToJump();
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        F(i,j) +=  (ujump(i) - ujump0(i))*refN(j)/_h;
      }
    }

    // - substract normal part if asked
    static SVector3 curN;
    curN = fipvprev->getConstRefToCurrentOutwardNormal();
    curN.normalize();
    double jumpN = dot(ujump,curN);

    if(_enabledNegativeJumpN_limit){
      if(jumpN < _maxPenetrationjumpN){
        for (int i=0; i<3; i++){
          for (int j=0; j<3; j++){
            F(i,j) -= (jumpN-_maxPenetrationjumpN)*curN(i)*refN(j)/_h;
          }
        }
      }
    }

    // - add jump gradient contribution
    const STensor3& Gradjump = fipv->getConstRefToJumpGradient();
    const STensor3& Gradjump0 = fipvprev->getConstRefToJumpGradient();
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        F(i,j) += _gradFactor*(Gradjump(i,j) - Gradjump0(i,j));
      }
    }

    if(!fipvprev->isDeleted()){
      double detF = STensorOperation::determinantSTensor3(F);
      if (detF < 1.e-10){Msg::Error("PorosityCohesiveBand3DLaw:: Negative jacobian= %e", detF);};
    }

    // Compute derivatives
    if (stiff)
    {
      STensor43& dFinterfacedF = fipv->getRefToDInterfaceDeformationGradientDDeformationGradient();
      STensor33& dFinterfacedJump = fipv->getRefToDInterfaceDeformationGradientDJump();
      STensor43& dFinterfacedgradJump = cohIpv->getRefToDInterfaceDeformationGradientDJumpGradient();

      STensorOperation::zero(dFinterfacedF);
      STensorOperation::zero(dFinterfacedJump);
      STensorOperation::zero(dFinterfacedgradJump);

      static STensor3 I(1.);
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){
            dFinterfacedJump(i,j,k) += I(i,k)*refN(j)/_h;

            if(_enabledNegativeJumpN_limit){
              if(jumpN < _maxPenetrationjumpN){
                dFinterfacedJump(i,j,k) -= curN(k)*curN(i)*refN(j)/_h;
              }
            }

            for (int l=0; l<3; l++){
              dFinterfacedF(i,j,k,l) += I(i,k)*I(j,l);
              dFinterfacedgradJump(i,j,k,l) += _gradFactor*I(i,k)*I(j,l);
              if (!_accountNormalBulk){
                dFinterfacedF(i,j,k,l) -= I(i,k)*refN(j)*refN(l);
              }
            }
          }
        }
      }
    }
  }
};














PorosityWithLossOfEllipcitiyCohesiveBand3DLaw::PorosityWithLossOfEllipcitiyCohesiveBand3DLaw(const int num, const double h, const double Kp, const bool accountNormalBulk):
    PorosityCohesiveBand3DLaw(num,h,Kp,accountNormalBulk),
    _crackWithCoales(false), _tolOnCriterion(0.)
{

};

PorosityWithLossOfEllipcitiyCohesiveBand3DLaw::PorosityWithLossOfEllipcitiyCohesiveBand3DLaw(const PorosityWithLossOfEllipcitiyCohesiveBand3DLaw& src):
    PorosityCohesiveBand3DLaw(src),
    _crackWithCoales(src._crackWithCoales), _tolOnCriterion(src._tolOnCriterion)
{

};

void PorosityWithLossOfEllipcitiyCohesiveBand3DLaw::setToleranceOnCriterion(const double newTol, const bool crackWithCoales){
  _tolOnCriterion = newTol;
  _crackWithCoales = crackWithCoales;
}



void PorosityWithLossOfEllipcitiyCohesiveBand3DLaw::checkCohesiveInsertion(IPStateBase* ipsm, IPStateBase* ipsp, const bool forcedInsertion) const
{
  // check failure onset from both negative and postive IPStates

  // Get the differents Ipv types
  /// Get Frac Ipv
  FractureCohesive3DIPVariable* fMinusCur = dynamic_cast<FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::current));
  const FractureCohesive3DIPVariable* fMinusPrev = dynamic_cast<const FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::previous));
  FractureCohesive3DIPVariable* fPlusCur = dynamic_cast<FractureCohesive3DIPVariable*>(ipsp->getState(IPStateBase::current));
  const FractureCohesive3DIPVariable* fPlusPrev = dynamic_cast<const FractureCohesive3DIPVariable*>(ipsp->getState(IPStateBase::previous));
  
  
  /// Get nonLocalPorous Ipv
  const nonLocalPorosityDG3DIPVariable* ipvmprev = dynamic_cast<const nonLocalPorosityDG3DIPVariable*>(fMinusPrev->getIPvBulk());
  const nonLocalPorosityDG3DIPVariable* ipvpprev = dynamic_cast<const nonLocalPorosityDG3DIPVariable*>(fPlusPrev->getIPvBulk());
  
  /// Get nonLocalPorous Ipv
  nonLocalPorosityDG3DIPVariable* ipvm = dynamic_cast<nonLocalPorosityDG3DIPVariable*>(fMinusCur->getIPvBulk());
  nonLocalPorosityDG3DIPVariable* ipvp = dynamic_cast<nonLocalPorosityDG3DIPVariable*>(fPlusCur->getIPvBulk());

  /// Check if data are appropriate
  #ifdef _DEBUG
  if (fMinusCur == NULL or fPlusCur == NULL or fMinusPrev==NULL or fPlusPrev==NULL){
    Msg::Error("FractureCohesive3DIPVariable must be used in  NonlocalPorosityCohesiveBand3DLaw::checkCohesiveInsertion");
  }

  if (fMinusPrev->isbroken() or fPlusPrev->isbroken()){
    Msg::Warning("Previous IPs are broken");
    fMinusCur->broken();
    fPlusCur->broken();
    return;
  }
  if (ipvm == NULL or ipvp == NULL){
    Msg::Error("nonLocalPorosityDG3DIPVariable must be used in NonlocalPorosityCohesiveBand3DLaw::checkCohesiveInsertion");
  }
  #endif // _DEBUG

  /// Get IP coalescence
  IPCoalescence& ipcolm = ipvm->getIPNonLocalPorosity()->getRefToIPCoalescence();
  IPCoalescence& ipcolp = ipvp->getIPNonLocalPorosity()->getRefToIPCoalescence();


  // Crack insertion flag
  bool detectedInsertion = false; // ==True if insertion is required

  // Compute the criterion = det(N.dP/DF.N)
  double dPdF_in_dirN_minus, dPdF_in_dirN_plus; // sought value
  static SVector3 normDir;
  normDir = fPlusCur->getConstRefToCurrentOutwardNormal();
  normDir.normalize();

  static STensor43 DPDFm, DPDFp;
  DPDFm = ipvm->getConstRefToTangentModuli();
  DPDFp = ipvp->getConstRefToTangentModuli();

  // Add suited terms in function if the non-local method used
  if(_bulkLaw->getNonLocalPorosityLaw()->getNonLocalMethod() == 0){
    // Nothing to add
  }
  else if(_bulkLaw->getNonLocalPorosityLaw()->getNonLocalMethod() == 1){
    const STensor3& dPdfVm = ipvm->getConstRefToDStressDNonLocalVariable()[0];
    const STensor3& dfVdFm = ipvm->getConstRefToDLocalVariableDStrain()[0];
    const STensor3& dPdfVp = ipvp->getConstRefToDStressDNonLocalVariable()[0];
    const STensor3& dfVdFp = ipvp->getConstRefToDLocalVariableDStrain()[0];
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            DPDFm(i,j,k,l) += dPdfVm(i,j)*dfVdFm(k,l);
            DPDFp(i,j,k,l) += dPdfVp(i,j)*dfVdFp(k,l);
          }
        }
      }
    }
  }
  else{
    Msg::Error("PorosityWithLossOfEllipcitiyCohesiveBand3DLaw::  Ellipticity check for non-local method %d is not implemented", _bulkLaw->getNonLocalPorosityLaw()->getNonLocalMethod());
  }

  FailureDetection::getLostSolutionUniquenessCriterionFollowingDirection(3,DPDFm,normDir,dPdF_in_dirN_minus);
  FailureDetection::getLostSolutionUniquenessCriterionFollowingDirection(3,DPDFp,normDir,dPdF_in_dirN_plus);


  // Check if insertion is detected on one side, otherwise flag detectedInsertion stays == false
  if(dPdF_in_dirN_minus < -_tolOnCriterion){
    if(_crackWithCoales){
      // Coalescence is required to insert a crack
      if (ipcolm.getCoalescenceOnsetFlag()){
        detectedInsertion = true;
      }
    }
    else{
      // Coalescence is not required to insert a crack
      detectedInsertion = true;
    }
  }

  if(dPdF_in_dirN_plus < -_tolOnCriterion){
    if(_crackWithCoales){
      // Coalescence is required to insert a crack
      if (ipcolp.getCoalescenceOnsetFlag()){
        detectedInsertion = true;
      }
    }
    else{
      // Coalescence is not required to insert a crack
      detectedInsertion = true;
    }
  }

  // Check if crack insertion is requested (because coalescence occurs or it is forced to occur)
  if (detectedInsertion or forcedInsertion){
    printf("rank %d: cohesive element is inserted \n",Msg::GetCommRank());
    // set to broken state and activate nonlocal to local transition
    fMinusCur->broken();
    fPlusCur->broken();
    fMinusCur->setNonLocalToLocal(true);
    fPlusCur->setNonLocalToLocal(true);

    // Force IP to be in a coalescence state if not already in that state
    if (!ipcolm.getCoalescenceOnsetFlag())
    {
      _bulkLaw->forceCoalescence(ipvm,ipvmprev);
    }
    if (!ipcolp.getCoalescenceOnsetFlag())
    {
      _bulkLaw->forceCoalescence(ipvp,ipvpprev);
    }

    fMinusCur->getOnsetCriterion() = _bulkLaw->getNonLocalPorosityLaw()->getOnsetCriterion(ipvm->getIPNonLocalPorosity());
    fPlusCur->getOnsetCriterion() = _bulkLaw->getNonLocalPorosityLaw()->getOnsetCriterion(ipvp->getIPNonLocalPorosity());

    // Output display
    Msg::Warning("PorosityWithLossOfEllipcitiyCohesiveBand3DLaw:: insertion: N.dPdF.N= %e %e",dPdF_in_dirN_minus,dPdF_in_dirN_plus);




    // Initialize data for cohesive law in negative part
    static SVector3 zeroVec;
    static STensor3 zeroTen;

    Cohesive3DIPVariableBase* coMinus = fMinusCur->getIPvFrac();
    coMinus->initializeFracture(fMinusCur->getConstRefToJump(),getKp(),0.,0.,0.,0.,0.,true,zeroTen,getKp(),normDir,zeroVec,
                            fMinusCur->getIPvBulk());

    // positive part take a same value as negative cohesive part
    Cohesive3DIPVariableBase* coPlus = fPlusCur->getIPvFrac();
    coPlus->initializeFracture(fPlusCur->getConstRefToJump(),getKp(),0.,0.,0.,0.,0.,true,zeroTen,getKp(),normDir,zeroVec,
                            fPlusCur->getIPvBulk());
  }
  else{
    ipcolm.getRefToCoalescenceOnsetFlag() = false;
    ipcolp.getRefToCoalescenceOnsetFlag() = false;

    fMinusCur->nobroken();
    fPlusCur->nobroken();

    fMinusCur->setNonLocalToLocal(false);
    fPlusCur->setNonLocalToLocal(false);
  };
};




  
