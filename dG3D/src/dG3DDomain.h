//
// C++ Interface: partDomain
//
// Description: Interface class for dg 3D
//
// Author:  <L. Noels>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef DG3DDOMAIN_H_
#define DG3DDOMAIN_H_
#ifndef SWIG
#include "dG3DFunctionSpace.h"
#include "dG3DMaterialLaw.h"
#include "dG3DMaterialLaw.h"
#include "dG3DIPVariable.h"
#include "partDomain.h"
#include "interfaceQuadrature.h"
#include "ipField.h"
#endif
class dG3DDomain : public dgPartDomain{
 public :
  enum GaussType { Gauss=0, Lobatto=1};

 protected:
#ifndef SWIG
  bool evaluateMecaField;
  std::vector<bool> evaluateExtraDofDiffusionField;
  std::vector<bool> evaluateNonLocalField;
  std::vector<bool> evaluateCurlField;
  
  materialLaw *_mlaw;
  int _lnum; // unique number to exactly identify materialLaw
  int _dim; //dimension
  double _beta1; // stabilization parameters
  //
  double _sts; // variable to scale the explicit time step by 1/sqrt(beta) for dg stabilization
  bool _evalStiff; // = true when compute matrix by perturbation
  //
  bool _isHighOrder; // = true if highorder used
  double _nonLocalBeta;
  bool _nonLocalContinuity;
  double _nonLocalEqRatio;

  //constitutiveExtraDofDiffusion
  double _constitutiveExtraDofDiffusionBeta;
  bool _constitutiveExtraDofDiffusionContinuity;
  double _constitutiveExtraDofDiffusionEqRatio;
  bool _constitutiveExtraDofDiffusionUseEnergyConjugatedField;
  bool _constitutiveExtraDofDiffusionAccountFieldSource;
  bool _constitutiveExtraDofDiffusionAccountMecaSource;

  // curl variable
  double _constitutiveCurlEqRatio;
  bool _constitutiveCurlAccountFieldSource;
  bool _constitutiveCurlAccountMecaSource;
  //
  bool _incrementNonlocalBased;
	bool _planeStressState; //
  //
  FunctionSpaceBase *_space;
  //
  int _gaussorderbulk, _gaussorderbound; // To allow user to change the number of Gauss Point default =-1 (not used) if != -1 user's value
  //
  interfaceQuadratureBase *_interQuad; // interface quadrature rule
  GaussType _gqt; // classic or Lobatto

  // activate strain substepting
  // 0 - by default = no substepting
  // 1 - linear steping for interface ipv's only
  // 2 - linear steping for both bulk and interfaces ipv's
  int _subSteppingMethod;
  int _subStepNumber; // number of substep

  // Damage blocking and forcing insertion managing
  int _bulkDamageBlockMethod;
  // method allows blocking bulk damage when inserting cohesive law at an interfaceIP
  // -1 (by default)   no block
  //0 - block bulk damage when cohesive crack is inserted at fist interface IP
  //1 - block bulk damage when cohesive cracks are inserted at all interface IP

  bool _forceInterfaceElementBroken; // true if all IPs at interface IPs are broken when N ips are broken
  double _failureIPRatio;            // to define the value of N
  // _failureIPRatio = 1 (by default) interface element is broken if all Ips are broken
  // _failureIPRatio = 0 interface element is broken if first IP is broken
  // a value in (0 1) can be used
  //
  double _EqMultiFieldFactor;

  // crack injection
  bool _imposeCrackFlag;
  std::set<TwoNum> _imposedInterfaceCrack; // pair is positive and negative
  bool _accountPathFollowing;

#endif // SWIG


 public:
  dG3DDomain(const int tag, const int phys, const int sp_, const int lnum, const int fdg, const int dim =3, int nonLocalVar=0, int nonConstitutiveExtraDOFVar=0, int nonCurlData=0, bool hasBodyForceForHO=false, int BFModuliType=0);
  virtual void stabilityParameters(const double b1=10.);
  virtual void matrixByPerturbation(const int iinter, const double eps=1e-8);
  virtual void matrixByPerturbation(const int ibulk, const int iinter, const int ivirt, const double eps=1e-8);
  virtual void gaussIntegration(const GaussType gqt,const int orderbulk, const int orderbound);
  virtual void setBulkDamageBlockedMethod(const int method);
  virtual void forceCohesiveInsertionAllIPs(const bool flg, const double fratio);
  virtual void strainSubstep(const int method, const int numstep);
  virtual void setMultipleFieldEqRatio(const double val);

  virtual void setNewIdForComp(const int comp, const int type);

  virtual void setImposedCrack(const bool);
  virtual void imposeCrack(const int em, const int ep);

  virtual void accountPathFollowing(const bool fl);
  virtual void setNonLocalStabilityParameters(const double b2=10., bool continuity=true)
  {
     _nonLocalBeta = b2;
     _nonLocalContinuity = continuity;
  }
  virtual void setNonLocalEqRatio(const double eq)
  {
    _nonLocalEqRatio=eq;
  }
  virtual void setConstitutiveExtraDofDiffusionStabilityParameters(const double b2=10., bool continuity=true)
  {
     _constitutiveExtraDofDiffusionBeta = b2;
     _constitutiveExtraDofDiffusionContinuity = continuity;
  }
  virtual void setConstitutiveExtraDofDiffusionEqRatio(const double eq)
  {
    _constitutiveExtraDofDiffusionEqRatio=eq;
  }
  virtual void setConstitutiveCurlEqRatio(const double eq)
  {
    _constitutiveCurlEqRatio=eq;
  }
  virtual void setConstitutiveExtraDofDiffusionUseEnergyConjugatedField(bool useFtFv) {_constitutiveExtraDofDiffusionUseEnergyConjugatedField=useFtFv;}
  virtual void setConstitutiveExtraDofDiffusionAccountSource(const bool fieldSource, const bool mecaSource)
  {
    _constitutiveExtraDofDiffusionAccountFieldSource = fieldSource;
    _constitutiveExtraDofDiffusionAccountMecaSource = mecaSource;
  }

  virtual void setConstitutiveCurlAccountSource(const bool sourceVectorField, const bool mecaFieldSource)
  {
    _constitutiveCurlAccountFieldSource = sourceVectorField;
    _constitutiveCurlAccountMecaSource = mecaFieldSource;
  }

  virtual void setIncrementNonlocalBased(const bool fl);
	virtual void setPlaneStressState(const bool fl);
  
  virtual void setMaterialLawNumber(const int num);
  
  virtual void usePrimaryShapeFunction(const int comp);
  virtual void setHierarchicalOrder(const int comp, const int order);

  virtual void setEvaluateMecaField(bool cp) {evaluateMecaField=cp;}
  virtual void setEvaluateExtraDofDiffusionField(bool cp, int ind) { if(ind<evaluateExtraDofDiffusionField.size()) evaluateExtraDofDiffusionField[ind]=cp;}
  virtual void setEvaluateNonLocalField(bool cp, int ind) { if(ind<evaluateNonLocalField.size()) evaluateNonLocalField[ind]=cp;}
  virtual void setEvaluateCurlField(bool cp, int ind) { if(ind<evaluateCurlField.size()) evaluateCurlField[ind]=cp;}

  
#ifndef SWIG
  dG3DDomain(const dG3DDomain &source);
  virtual ~dG3DDomain();
  virtual int getDim() const {return _dim;}
  virtual int getStressDimension() const;
  virtual bool getAccountPathFollowingFlag() const;

  virtual void checkInternalState(IPField* ipf) const;
  virtual void checkFailure(IPField* aips) const;

  virtual void initialIPVariable(AllIPState *aips,const unknownField *ufield,const IPStateBase::whichState ws, bool stiff);

  virtual void computeStrain(MElement *e,  const int npts, IntPt *GP,
                             AllIPState::ipstateElementContainer *vips,
                             IPStateBase::whichState ws, fullVector<double> &disp, bool useBarF) const;
  virtual bool computeIpv(AllIPState *aips,MElement *e, IPStateBase::whichState ws,
                                  materialLaw *mlaw,fullVector<double> &disp, bool stiff);
  virtual void computeStrain(AllIPState *aips,MInterfaceElement *ie, IntPt *GP,const IPStateBase::whichState ws,
                                        partDomain* efMinus, partDomain *efPlus, fullVector<double> &dispm,
                                        fullVector<double> &dispp, const bool virt,
                                        bool useBarF);
  virtual bool computeIpv(AllIPState *aips,MInterfaceElement *ie, IntPt *GP,const IPStateBase::whichState ws,
                                  partDomain* efMinus, partDomain *efPlus,materialLaw *mlawminus,
                                  materialLaw *mlawplus,fullVector<double> &dispm,
                                  fullVector<double> &dispp, const bool virt, bool stiff,const bool checkfrac=true);
  virtual void computedFmdFM(MElement *e, IntPt *GP, AllIPState::ipstateElementContainer *vips, 
                               std::map<Dof, STensor3> &dUdF, std::vector<Dof> &R, const bool useBarF, IPStateBase::whichState ws) const;                                
  virtual void computedFmdFM(AllIPState *aips, MInterfaceElement *ie, IntPt *GP, partDomain* efMinus, partDomain *efPlus,
                               std::map<Dof, STensor3> &dUdF, std::vector<Dof> &Rm, std::vector<Dof> &Rp,const bool useBarF, const IPStateBase::whichState ws) const;
  virtual void computedFmdGM(MElement *e, IntPt *GP, AllIPState::ipstateElementContainer *vips, 
                               std::map<Dof, STensor33> &dUdG, std::vector<Dof> &R, const bool useBarF, IPStateBase::whichState ws) const;                                
  virtual void computedFmdGM(AllIPState *aips, MInterfaceElement *ie, IntPt *GP, partDomain* efMinus, partDomain *efPlus,
                               std::map<Dof, STensor33> &dUdG, std::vector<Dof> &Rm, std::vector<Dof> &Rp,const bool useBarF, const IPStateBase::whichState ws) const;
  virtual void setDeformationGradientGradient(AllIPState *aips, const STensor33 &GM,const IPStateBase::whichState ws); 
  virtual void setdFmdFM(AllIPState *aips, std::map<Dof, STensor3> &dUdF,const IPStateBase::whichState ws);    
  virtual void setdFmdGM(AllIPState *aips, std::map<Dof, STensor33> &dUdG,const IPStateBase::whichState ws);  
  virtual void setValuesForBodyForce(AllIPState *aips, const IPStateBase::whichState ws);                           
  virtual void computeAllIPStrain(AllIPState *aips,const unknownField *ufield,const IPStateBase::whichState ws, const bool virt);
  virtual bool computeIPVariable(AllIPState *aips,const unknownField *ufield,const IPStateBase::whichState ws, bool stiff);
  virtual void setMaterialLaw(const std::map<int,materialLaw*> &maplaw);
  virtual materialLaw* getMaterialLaw(){return _mlaw;}
  virtual const materialLaw* getMaterialLaw() const{return _mlaw;}
  virtual materialLaw* getMaterialLawMinus(){return _mlaw;}
  virtual const materialLaw* getMaterialLawMinus() const{return _mlaw;}
  virtual materialLaw* getMaterialLawPlus(){return _mlaw;}
  virtual const materialLaw* getMaterialLawPlus() const{return _mlaw;}
  virtual int getLawNum() const{return _lnum;}
  virtual int getMinusLawNum() const{return _lnum;}
  virtual int getPlusLawNum() const{return _lnum;}
  virtual void setGaussIntegrationRule();
  virtual void getNoMassDofs(std::set<Dof>& DofVector) const; // get back the dof for which the predictor must be set to zero
  virtual void createTerms(unknownField *uf,IPField*ip);
  virtual void initializeTerms(unknownField *uf,IPField *ip);
  virtual LinearTermBase<double>* createNeumannTerm(FunctionSpace<double> *spneu,  const elementGroup* g,
                                                    const simpleFunctionTime<double>* f,const unknownField *uf,
                                                    const IPField * ip, const nonLinearBoundaryCondition::location onWhat,
                                                    const nonLinearNeumannBC::NeumannBCType neumann_type, const int comp) const;
  virtual BilinearTermBase* createNeumannMatrixTerm(FunctionSpace<double> *spneu, const elementGroup* g,
                                                      const simpleFunctionTime<double>* f, const unknownField *uf,
                                                      const IPField * ip, const nonLinearBoundaryCondition::location onWhat,
                                                      const nonLinearNeumannBC::NeumannBCType neumann_type, const int comp) const;
  virtual double scaleTimeStep() const {return _sts;}
  virtual FunctionSpaceBase* getFunctionSpace(){return _space;}
  virtual const FunctionSpaceBase* getFunctionSpace() const {return _space;}
  virtual const partDomain* getMinusDomain() const{return this;}
  virtual const partDomain* getPlusDomain() const{return this;}
  virtual partDomain* getMinusDomain() {return this;}
  virtual partDomain* getPlusDomain() {return this;}
  virtual void createInterface(manageInterface &maninter);
  virtual MElement* createVirtualInterface(IElement *ie) const;
  virtual MElement* createInterface(IElement *ie1, IElement *ie2) const;
  virtual const double stabilityParameter(const int i) const;
  virtual void allocateInterfaceMPI(const int rankOtherPart, const elementGroup &groupOtherPart,dgPartDomain** dgdom);
  virtual void createInterfaceMPI(const int rankOtherPart, const elementGroup &groupOtherPart,dgPartDomain* &dgdom);
  // Add for BC
  virtual FilterDof* createFilterDof(const int comp) const;
  virtual interfaceQuadratureBase* getInterfaceQuadrature() const{return _interQuad;};
  virtual FunctionSpaceBase* getSpaceForBC(const nonLinearBoundaryCondition::type bc_type,
                                           const nonLinearBoundaryCondition::location bc_location,
                                           const nonLinearNeumannBC::NeumannBCType neumann_type,
                                           const int dof_comp,
                                           const mixedFunctionSpaceBase::DofType dofType,
                                           const elementGroup *groupBC,const int domcomp) const;
  virtual FunctionSpaceBase* getSpaceForBC(const nonLinearBoundaryCondition::type bc_type, 
                                            const nonLinearBoundaryCondition::location bc_location,
                                            const nonLinearNeumannBC::NeumannBCType neumann_type,
                                            const int dof_comp,
                                            const mixedFunctionSpaceBase::DofType dofType,
                                            const elementGroup *groupBC) const
  {
      return this->getSpaceForBC(bc_type,bc_location, neumann_type,dof_comp,dofType,groupBC,3+getNumNonLocalVariable()+getNumConstitutiveExtraDofDiffusionVariable());
  }
  virtual QuadratureBase* getQuadratureRulesForNeumannBC(nonLinearNeumannBC &neu) const;
  virtual partDomain* clone() const {return new dG3DDomain(*this);};
  #if defined(HAVE_MPI)
  virtual bool computeIPVariableMPI(AllIPState *aips,const unknownField *ufield,const IPStateBase::whichState ws, bool stiff);
  #endif
  virtual double getNonLocalStabilityParameter() const { return _nonLocalBeta;}
  virtual bool   getNonLocalContinuity() const { return _nonLocalContinuity;}
  virtual double getNonLocalEqRatio() const { return _nonLocalEqRatio;}
  //

  virtual bool getConstitutiveExtraDofDiffusionUseEnergyConjugatedField() const { return _constitutiveExtraDofDiffusionUseEnergyConjugatedField;}
  virtual double getConstitutiveExtraDofDiffusionStabilityParameter() const { return _constitutiveExtraDofDiffusionBeta;}
  virtual bool   getConstitutiveExtraDofDiffusionContinuity() const { return  _constitutiveExtraDofDiffusionContinuity;}
  virtual double getConstitutiveExtraDofDiffusionEqRatio() const { return _constitutiveExtraDofDiffusionEqRatio;}
  virtual bool getConstitutiveExtraDofDiffusionAccountFieldSource() const {return _constitutiveExtraDofDiffusionAccountFieldSource;};
  virtual bool getConstitutiveExtraDofDiffusionAccountMecaSource() const{return _constitutiveExtraDofDiffusionAccountMecaSource;};
  virtual double getConstitutiveCurlEqRatio() const { return _constitutiveCurlEqRatio;}
  virtual bool getConstitutiveCurlAccountFieldSource() const {return _constitutiveCurlAccountFieldSource;}
  virtual bool getConstitutiveCurlAccountMecaSource() const {return _constitutiveCurlAccountMecaSource;}

  virtual double getConstitutiveExtraDofDiffusionEqRatio(const int index) const {
    if (index == 0) return getConstitutiveExtraDofDiffusionEqRatio();
    else{
      Msg::Error("ExtraDofDiffusionDG3DDomain::getConstitutiveExtraDofDiffusionEqRatio is not defined");
        return -1.;
    }
  };

#endif
};

class interDomainBase{
  protected :
    #ifndef SWIG
    bool _lawFromBulkPart; // by default false,
     // true if mlawminus and mlawplus are taken from bulk part instead of whole
     // compound law
     // material law is not necessary belong to the the minus and plus domain
    materialLaw* _mlawMinus;
    materialLaw* _mlawPlus;
     // this flag is used when considering problem with FractureByCohesive3DLaw
    partDomain *_domMinus;
    partDomain *_domPlus;
    int _bulkNum; // material bulk law at interface if underlying material does not belong to positive an negative domain
                  // if not _bulkNum = 0
    #endif // SWIG
  public :
    interDomainBase(partDomain *dom1,partDomain *dom2, int bulkNum);
    void setLawFromBulkPartFlag(const bool fl);
    #ifndef SWIG
    interDomainBase(const interDomainBase &source);
    virtual ~interDomainBase(){}
    #endif
};

class interDomainBetween3D : public interDomainBase, public dG3DDomain
{
 public:
  interDomainBetween3D(const int tag, partDomain *dom1, partDomain *dom2, const int lnum=0, const int bnum=0);

#ifndef SWIG
  interDomainBetween3D(const interDomainBetween3D &source);
  virtual ~interDomainBetween3D(){}
  
  virtual void setMaterialLaw(const std::map<int,materialLaw*> &maplaw);
  // FOLLOWING FUNCTION SHOULD STAY HERE BECAUSE OF THE DOUBLE DERIVATION
  virtual materialLaw* getMaterialLaw(){return _mlaw;}
  virtual const materialLaw* getMaterialLaw() const{return _mlaw;}
  virtual materialLaw* getMaterialLawMinus(){return _mlawMinus;}
  virtual const materialLaw* getMaterialLawMinus() const{return _mlawMinus;}
  virtual materialLaw* getMaterialLawPlus(){return _mlawPlus;}
  virtual const materialLaw* getMaterialLawPlus() const{return _mlawPlus;}
  virtual int getLawNum() const{return _lnum;}
  // FOLLOWING FUNCTION SHOULD STAY HERE BECAUSE OF THE DOUBLE DERIVATION
  virtual int getMinusLawNum() const{return _domMinus->getLawNum();}
  virtual int getPlusLawNum() const{return _domPlus->getLawNum();}
  virtual const partDomain* getMinusDomain() const{return _domMinus;}
  virtual const partDomain* getPlusDomain() const{return _domPlus;}
  virtual partDomain* getMinusDomain(){return _domMinus;}
  virtual partDomain* getPlusDomain() {return _domPlus;}

  virtual void createTerms(unknownField *uf,IPField*ip);
  // as there is no elerment on a interDomain empty function. The interface element on an interDomain
  // has to be created via its two domains.
  virtual void createInterface(manageInterface &maninter){}
  virtual partDomain* clone() const {return new interDomainBetween3D(*this);};
#endif
};


class ThermoMechanicsDG3DDomain : public dG3DDomain{
 protected:

 public:
  ThermoMechanicsDG3DDomain(const int tag, const int phys, const int sp_, const int lnum, const int fdg, const double ThermoMechanicsEqRatio, const int dim =3);

  virtual void ThermoMechanicsStabilityParameters(const double b2=10., bool continuity=true)
  {
     setConstitutiveExtraDofDiffusionStabilityParameters(b2,continuity);
  }

#ifndef SWIG
  ThermoMechanicsDG3DDomain(const ThermoMechanicsDG3DDomain &source);
  virtual ~ThermoMechanicsDG3DDomain(){}
  virtual double getThermoMechanicsStabilityParameter() const { return getConstitutiveExtraDofDiffusionStabilityParameter(); }
  virtual bool   getThermoMechanicsContinuity() const { return getConstitutiveExtraDofDiffusionContinuity();}
  virtual double getThermoMechanicsEqRatio() const { return getConstitutiveExtraDofDiffusionEqRatio();}
  virtual partDomain* clone() const{return new ThermoMechanicsDG3DDomain(*this);};
#endif
};

class ThermoMechanicsInterDomainBetween3D : public interDomainBetween3D {
 protected:

 public:
  ThermoMechanicsInterDomainBetween3D(const int tag, partDomain *dom1, partDomain *dom2, const double ThermoMechanicsEqRatio, const int lnum=0, const int bnum = 0);
  virtual void ThermoMechanicsStabilityParameters(const double b2=10., bool continuity=true)
  {
     interDomainBetween3D::setConstitutiveExtraDofDiffusionStabilityParameters(b2,continuity);
  }
#ifndef SWIG
  ThermoMechanicsInterDomainBetween3D(const ThermoMechanicsInterDomainBetween3D &source);
  virtual ~ThermoMechanicsInterDomainBetween3D(){}
  virtual double getThermoMechanicsEqRatio() const { return getConstitutiveExtraDofDiffusionEqRatio();}
  virtual double getThermoMechanicsStabilityParameter() const { return getConstitutiveExtraDofDiffusionStabilityParameter(); }
  virtual bool   getThermoMechanicsContinuity() const { return getConstitutiveExtraDofDiffusionContinuity();}

  // as there is no elerment on a interDomain empty function. The interface element on an interDomain
  // has to be created via its two domains.
  //virtual void createInterface(manageInterface &maninter){}
  virtual partDomain* clone() const{return new ThermoMechanicsInterDomainBetween3D(*this);};

#endif
};


class ElecTherMechDG3DDomain : public dG3DDomain{
 protected:
  //double _ElecTherMechBeta;
  //bool _continuity;
  //double _ElecTherMechEqRatio;
  //bool _useFtFv;

 public:


  ElecTherMechDG3DDomain(const int tag, const int phys, const int sp_, const int lnum, const int fdg, const double ElecTherMechEqRatio, const int dim =3);
  virtual void ElecTherMechStabilityParameters(const double b2=10., bool continuity=true)
  {
     setConstitutiveExtraDofDiffusionStabilityParameters(b2,continuity);
  }
        //virtual void useFtFv(bool ftfv) {_constitutiveExtraDofDiffusionUseEnergyConjugatedField=ftfv;}
#ifndef SWIG
  ElecTherMechDG3DDomain(const ElecTherMechDG3DDomain &source);
  virtual ~ElecTherMechDG3DDomain(){}
  virtual double getElecTherMechStabilityParameter() const { return getConstitutiveExtraDofDiffusionStabilityParameter();}
  virtual bool   getElecTherMechContinuity() const { return getConstitutiveExtraDofDiffusionContinuity();}
  virtual double getElecTherMechEqRatio() const { return getConstitutiveExtraDofDiffusionEqRatio();}
/*
  virtual double getConstitutiveExtraDofDiffusionEqRatio(const int index) const {
    if (index == 0) return getElecTherMechEqRatio();
    else{
      Msg::Error("ElecTherMechDG3DDomain::getConstitutiveExtraDofDiffusionEqRatio is not defined");
      return -1.;
    }
  };
  virtual double getNonConstitutiveExtraDofEqRatio(const int index) const {
    if (index == 0) return getElecTherMechEqRatio();
    else{
      Msg::Error("ElecTherMechDG3DDomain::getNonConstitutiveExtraDofEqRatio is not defined");
      return -1.;
    }
  };

  virtual bool getUseFtFv() const { return _constitutiveExtraDofDiffusionUseEnergyConjugatedField;}

  virtual FunctionSpaceBase* getSpaceForBC(const nonLinearBoundaryCondition::type bc_type, const nonLinearBoundaryCondition::location bc_location,
                                          const int dof_comp,const elementGroup *groupBC) const
  {
    return dG3DDomain::getSpaceForBC(bc_type,bc_location,dof_comp,groupBC,3+getNumNonLocalVariable()+2);
  }
  virtual void createTerms(unknownField *uf,IPField*ip);
  virtual LinearTermBase<double>* createNeumannTerm(FunctionSpace<double> *spneu,  const elementGroup* g,
                                                                    const simpleFunctionTime<double>* f,const unknownField *uf,
                                                                    const IPField * ip, nonLinearBoundaryCondition::location onWhat) const;

  virtual void computeStrain(MElement *e, const int npts_bulk, IntPt *GP,
                             AllIPState::ipstateElementContainer *vips,
                             IPStateBase::whichState ws, fullVector<double> &disp) const;
  virtual void computeStrain(AllIPState *aips,MInterfaceElement *ie, IntPt *GP,const IPStateBase::whichState ws,
                                        partDomain* efMinus, partDomain *efPlus, fullVector<double> &dispm,
                                        fullVector<double> &dispp, const bool virt);

  virtual void allocateInterfaceMPI(const int rankOtherPart, const elementGroup &groupOtherPart,dgPartDomain** dgdom);*/

  virtual partDomain* clone() const{return new ElecTherMechDG3DDomain(*this);};

#endif
};

class ElecTherMechInterDomainBetween3D : public interDomainBase, public ElecTherMechDG3DDomain{
 public:

  ElecTherMechInterDomainBetween3D(const int tag, partDomain *dom1, partDomain *dom2, const double ElecTherMechEqRatio, const int lnum=0, const int bnum=0);
  virtual void ElecTherMechStabilityParameters(const double b2=10., bool continuity=true)
  {
     setConstitutiveExtraDofDiffusionStabilityParameters(b2,continuity);
  }
  #ifndef SWIG
	ElecTherMechInterDomainBetween3D(const ElecTherMechInterDomainBetween3D &source);
  virtual ~ElecTherMechInterDomainBetween3D(){}
  virtual double getElecTherMechStabilityParameter() const { return getConstitutiveExtraDofDiffusionStabilityParameter();}
  virtual bool   getElecTherMechContinuity() const { return getConstitutiveExtraDofDiffusionContinuity();}
  virtual double getElecTherMechEqRatio() const { return getConstitutiveExtraDofDiffusionEqRatio();}
/*

  //virtual void setGaussIntegrationRule();
  virtual void computeAllIPStrain(AllIPState *aips,const unknownField *ufield,const IPStateBase::whichState ws, const bool virt);
  virtual void computeIPVariable(AllIPState *aips,const unknownField *ufield,const IPStateBase::whichState ws,
                                  bool stiff=true);

  virtual void setMaterialLaw(const std::map<int,materialLaw*> &maplaw);
  // FOLLOWING FUNCTION SHOULD STAY HERE BECAUSE OF THE DOUBLE DERIVATION
  virtual materialLaw* getMaterialLaw(){return _mlaw;}
  virtual const materialLaw* getMaterialLaw() const{return _mlaw;}
  virtual materialLaw* getMaterialLawMinus(){return _mlawMinus;}
  virtual const materialLaw* getMaterialLawMinus() const{return _mlawMinus;}
  virtual materialLaw* getMaterialLawPlus(){return _mlawPlus;}
  virtual const materialLaw* getMaterialLawPlus() const{return _mlawPlus;}
  virtual int getLawNum() const{return _lnum;}
  // FOLLOWING FUNCTION SHOULD STAY HERE BECAUSE OF THE DOUBLE DERIVATION
  virtual int getMinusLawNum() const{return _domMinus->getLawNum();}
  virtual int getPlusLawNum() const{return _domPlus->getLawNum();}
  virtual const partDomain* getMinusDomain() const{return _domMinus;}
  virtual const partDomain* getPlusDomain() const{return _domPlus;}
  virtual partDomain* getMinusDomain(){return _domMinus;}
  virtual partDomain* getPlusDomain() {return _domPlus;}
  void initializeTerms(unknownField *uf,IPField *ip);

  virtual void createTerms(unknownField *uf,IPField*ip);
  // as there is no elerment on a interDomain empty function. The interface element on an interDomain
  // has to be created via its two domains.
  virtual void createInterface(manageInterface &maninter){}
  //required as pure virtual
  virtual void allocateInterfaceMPI(const int rankOtherPart, const elementGroup &groupOtherPart,dgPartDomain** dgdom)
  { Msg::Error("ElecTherMechInterDomainBetween3D::allocateInterfaceMPI should not be called");  };*/

	virtual partDomain* clone() const{return new ElecTherMechInterDomainBetween3D(*this);};

#endif
};


class ElecMagTherMechDG3DDomain : public dG3DDomain
{
    public:

    ElecMagTherMechDG3DDomain(const int tag, const int phys, const int sp_, const int lnum, const int fdg,
    const double ElecMagTherMechEqRatio, const int dim =3);
    virtual void ElecMagTherMechStabilityParameters(const double b2=10., bool continuity=true)
    {
        setConstitutiveExtraDofDiffusionStabilityParameters(b2,continuity);
    }

    #ifndef SWIG
    ElecMagTherMechDG3DDomain(const ElecMagTherMechDG3DDomain &source);
    virtual ~ElecMagTherMechDG3DDomain(){}
    
    virtual void computeStrain(MElement *e,  const int npts, IntPt *GP,
                             AllIPState::ipstateElementContainer *vips,
                             IPStateBase::whichState ws, fullVector<double> &disp, bool useBarF) const;
                             
    virtual double getElecMagTherMechStabilityParameter() const { return getConstitutiveExtraDofDiffusionStabilityParameter();}
    virtual bool   getElecMagTherMechContinuity() const { return getConstitutiveExtraDofDiffusionContinuity();}
    virtual double getElecMagTherMechEqRatio() const { return getConstitutiveExtraDofDiffusionEqRatio();}
    virtual partDomain* clone() const{return new ElecMagTherMechDG3DDomain(*this);}
    #endif

};

class ElecMagTherMechInterDomainBetween3D : public interDomainBase, public ElecMagTherMechDG3DDomain
{
    public:
    ElecMagTherMechInterDomainBetween3D(const int tag, partDomain *dom1, partDomain *dom2,
    const double ElecMagTherMechEqRatio, const int lnum=0, const int bnum=0);
    virtual void ElecMagTherMechStabilityParameters(const double b2=10., bool continuity=true)
    {
        setConstitutiveExtraDofDiffusionStabilityParameters(b2,continuity);
    }
    #ifndef SWIG
    ElecMagTherMechInterDomainBetween3D(const ElecMagTherMechInterDomainBetween3D &source);
    virtual ~ElecMagTherMechInterDomainBetween3D(){}
    virtual double getElecMagTherMechStabilityParameter() const { return getConstitutiveExtraDofDiffusionStabilityParameter();}
    virtual bool   getElecMagTherMechContinuity() const { return getConstitutiveExtraDofDiffusionContinuity();}
    virtual double getElecMagTherMechEqRatio() const { return getConstitutiveExtraDofDiffusionEqRatio();}
    virtual partDomain* clone() const{return new ElecMagTherMechInterDomainBetween3D(*this);}
    #endif // SWIG

  };

double computFieldValue(const std::vector<TensorialTraits<double>::ValType> &Val, int pos, int n,
                      const fullVector<double> & disp);
double computeJumpField(const std::vector<TensorialTraits<double>::ValType> &Val_m, int pos_m, int n_m,
                      const std::vector<TensorialTraits<double>::ValType> &Val_p, int pos_p, const int n_p,
                      const fullVector<double> & dispm,const fullVector<double> & dispp);
                      
void computeJump(const std::vector<TensorialTraits<double>::ValType> &Val_m,const int n_m,
                      const std::vector<TensorialTraits<double>::ValType> &Val_p,
                      const int n_p,const fullVector<double> & dispm,const fullVector<double> & dispp,SVector3 &ujump);

void computeNonLocalJump(const std::vector<TensorialTraits<double>::ValType> &Val_m,const int n_m,
                      const std::vector<TensorialTraits<double>::ValType> &Val_p,
                      const int n_p,const fullVector<double> & dispm,const fullVector<double> & dispp, double &epsjump,
                      const int idx = 0);

void computeExtraFieldJump(const int field_index,const std::vector<TensorialTraits<double>::ValType> &Val_m,const int n_m,
                      const std::vector<TensorialTraits<double>::ValType> &Val_p,
                      const int n_p,const fullVector<double> & dispm,const fullVector<double> & dispp, double &temperaturejump);

void computeftjump(const int index_field,const std::vector<TensorialTraits<double>::ValType> &Val_m,const int n_m,
                      const std::vector<TensorialTraits<double>::ValType> &Val_p,
                      const int n_p,const fullVector<double> & dispm,const fullVector<double> & dispp, double &ftjump, double &dftjumpdTp, double &dftjumpdTm);

void computefvjump(const int index_fieldT,const int index_fieldv,const std::vector<TensorialTraits<double>::ValType> &Val_m,const int n_m,
                  const std::vector<TensorialTraits<double>::ValType> &Val_p,const int n_p,const fullVector<double> & dispm,const fullVector<double> & dispp,
		  double &fvjump, double &dfvjumpdTp, double &dfvjumpdTm, double &dfvjumpdvp, double &dfvjumpdvm);


#endif // DG3DDOMAIN_H_
