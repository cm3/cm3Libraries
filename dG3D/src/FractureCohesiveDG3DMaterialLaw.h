//
//
// Description: Class of materials for non linear dg
//
//
// Author:  <L. Noels>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef _FRACTURECOHESIVEDG3DMATERIALLAW_H_
#define _FRACTURECOHESIVEDG3DMATERIALLAW_H_

#include "dG3DMaterialLaw.h"
#include "dG3DCohesiveMaterialLaw.h"
#include "FractureCohesiveDG3DIPVariable.h"
#include "numericalMaterial.h"

class FractureByCohesive3DLaw : public dG3DMaterialLaw, public  fractureBy2Laws<dG3DMaterialLaw,Cohesive3DLaw>
{
 public:
  static double frand(const double a,const double b);
 public:
  FractureByCohesive3DLaw(const int num,const int nbulk, const int ncoh);
#ifndef SWIG
  FractureByCohesive3DLaw(const FractureByCohesive3DLaw &source);
  virtual ~FractureByCohesive3DLaw(){}
  virtual matname getType() const{return materialLaw::fracture;}

  virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
  virtual void initialBroken(IPStateBase *ips) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw)
  {
    if(!_initialized){
      fractureBy2Laws<dG3DMaterialLaw,Cohesive3DLaw>::initLaws(maplaw);
      _initialized = true;
      _mbulk->calledBy2lawFracture();
    }
  }
  virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
  virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent =false);
  virtual bool fullBroken(const IPVariable *ipv) const
  {
    const Cohesive3DIPVariableBase *ipvc = (dynamic_cast<const FractureCohesive3DIPVariable*>(ipv))->getIPvFrac();
    if(ipvc !=NULL)  return _mfrac->fullBroken(ipvc); // not initialized --> no fracture
    return false;
  }
  virtual double density() const{return _mbulk->density();}
  virtual double soundSpeed() const{return _mbulk->soundSpeed();}
  virtual void setElasticStiffness(IPVariable* ipv) const {
    _mbulk->setElasticStiffness(ipv);
  }
  virtual bool brokenCheck(const IPVariable* ipv) const {
    // check on bulk for broken
    return _mbulk->brokenCheck(ipv);
  }
	virtual void initialIPVariable(IPVariable* ipv, bool stiff){
		_mbulk->initialIPVariable(ipv,stiff);
	}
	virtual double scaleFactor() const{return _mbulk->scaleFactor();};
	virtual materialLaw* clone() const{ return new FractureByCohesive3DLaw(*this);};
	virtual bool withEnergyDissipation() const {
    return (_mbulk->withEnergyDissipation() or _mfrac->withEnergyDissipation());
  }
  virtual bool isHighOrder() const { return _mbulk->isHighOrder();};

  virtual int getNumberOfExtraDofsDiffusion() const {return _mbulk->getNumberOfExtraDofsDiffusion();}
  virtual double getInitialExtraDofStoredEnergyPerUnitField() const {return _mbulk->getInitialExtraDofStoredEnergyPerUnitField();}
  virtual double getExtraDofStoredEnergyPerUnitField(double T) const {return _mbulk->getExtraDofStoredEnergyPerUnitField(T);}
  virtual double getCharacteristicLength() const {return _mfrac->getCharacteristicLength();};

	virtual void setMacroSolver(const nonLinearMechSolver* sv){
		dG3DMaterialLaw::setMacroSolver(sv);
		if (_mbulk != NULL){
			_mbulk->setMacroSolver(sv);
		}
		if (_mfrac != NULL){
			_mfrac->setMacroSolver(sv);
		}
	}
  
  virtual const materialLaw* getConstNonLinearSolverMaterialLaw() const {
    // Send back the result from the bulk law.
    return _mbulk->getConstNonLinearSolverMaterialLaw();
  };
  
  virtual materialLaw* getNonLinearSolverMaterialLaw() {
    // Send back the result from the bulk law.
    return _mbulk->getNonLinearSolverMaterialLaw();
  };

#endif // SWIG
};

class dG3DMultiscaleMaterialLawBase;
class GeneralMultiscaleBulkFollwedCohesive3DLaw;
class MultiscaleFractureByCohesive3DLaw : public FractureByCohesive3DLaw,
                                          public numericalMaterialBase{

  #ifndef SWIG
  protected:
    dG3DMultiscaleMaterialLawBase* _nummat; // point to numerical mat of bulk law
    GeneralMultiscaleBulkFollwedCohesive3DLaw* _interfaceLaw;
  #endif // SWIG

  public:
    MultiscaleFractureByCohesive3DLaw(const int num,const int nbulk, const int ncoh);
    #ifndef SWIG
    MultiscaleFractureByCohesive3DLaw(const MultiscaleFractureByCohesive3DLaw& src);
    virtual ~MultiscaleFractureByCohesive3DLaw(){}

    virtual void initLaws(const std::map<int,materialLaw*> &maplaw);
		virtual void initialBroken(IPStateBase* ips) const;
    virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPState(const bool isSolve, IPStateBase* &ips,bool hasBodyForce, const bool* state =NULL,
                                 const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt=0) const;

    virtual nonLinearMechSolver* createMicroSolver(const int ele, const int gpt) const ;

    virtual bool isNumeric() const {return true;};

    virtual void printMeshIdMap() const;
    //this function provides an id for a GP
		virtual void assignMeshId(const int e, const int gpt);
		virtual void setMeshId(const int e, const int gpt, const int id);
		// this function get mesh id
		virtual int getMeshId(const int e, const int gpt) const;
		#if defined(HAVE_MPI)
		// these function used for comunicate MPI
		virtual int getNumberValuesMPI() const;
		virtual void fillValuesMPI(int* val) const;
		virtual void getValuesMPI(const int* val, const int size);
		#endif //HAVE_MPI
		virtual materialLaw* clone() const {return new MultiscaleFractureByCohesive3DLaw(*this);};

		virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent =false);
    
    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
      Msg::Error("getConstNonLinearSolverMaterialLaw in MultiscaleFractureByCohesive3DLaw does not exist");
      return NULL;
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
      Msg::Error("getNonLinearSolverMaterialLaw in MultiscaleFractureByCohesive3DLaw does not exist");
      return NULL;
    }
    #endif // SWIG
};



#endif // _FRACTURECOHESIVEDG3DMATERIALLAW_H_
