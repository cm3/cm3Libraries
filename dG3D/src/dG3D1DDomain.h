//
// C++ Interface: 1D domain
//
// Description: Class of terms for dg
//
//
// Author:  <V.D Nguyen>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef DG3D1DDOMAIN_H_
#define DG3D1DDOMAIN_H_
#include "dG3DDomain.h"
#include "scalarFunction.h"

class dG3D1DDomain : public dG3DDomain{
  protected:
    #ifndef SWIG
    enum State{UniaxialStrain=0, UniaxialStress =1,ConstantTriaxiality=2};
    scalarFunction* _crossSectionDistribution;
    State _state;
    double _T; // stress triaxiality
    double _ThermoMechanicsEqRatio;
    #endif //SWIG
  public:
    dG3D1DDomain(const int tag, const int phys, const int sp_, const int lnum, const int fdg, int numNL=0, int numExtraDOF=0);
    void setCrossSectionDistribution(const scalarFunction* fct);
    void setCrossSection(const double val);
    void setState(const int s);
    void setStressTriaxiality(const double T);

    virtual void ThermoMechanicsStabilityParameters(const double b2=10., bool continuity=true)
    {
        setConstitutiveExtraDofDiffusionStabilityParameters(b2,continuity);
    }
    void setThermoMechanicsEqRatio(const double ThermoMechanicsEqRatio){_ThermoMechanicsEqRatio = ThermoMechanicsEqRatio;}
    virtual double getThermoMechanicsStabilityParameter() const { return getConstitutiveExtraDofDiffusionStabilityParameter(); }
    virtual bool   getThermoMechanicsContinuity() const { return getConstitutiveExtraDofDiffusionContinuity();}
    virtual double getThermoMechanicsEqRatio() const { return getConstitutiveExtraDofDiffusionEqRatio();}

    #ifndef SWIG
    dG3D1DDomain(const dG3D1DDomain& src);
    virtual ~dG3D1DDomain();
    virtual void createTerms(unknownField *uf,IPField*ip);
    virtual partDomain* clone() const {return new dG3D1DDomain(*this);};

    virtual bool computeIpv(AllIPState *aips,MElement *e, IPStateBase::whichState ws,
                                  materialLaw *mlaw,fullVector<double> &disp, bool stiff);
    virtual bool computeIpv(AllIPState *aips,MInterfaceElement *ie, IntPt *GP,const IPStateBase::whichState ws,
                                  partDomain* efMinus, partDomain *efPlus,materialLaw *mlawminus,
                                  materialLaw *mlawplus,fullVector<double> &dispm,
                                  fullVector<double> &dispp, const bool virt, bool stiff,const bool checkfrac=true){
      Msg::Error("dG3D1DDomain::computeIpv has not been implemented");
      return false;
    };

    #endif //SWIG
};

#endif //DG3D1DDOMAIN_H_
