//
// C++ Interface: deep material network
//
//
// Author:  <V.-D. Nguyen>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef _DG3DDEEPMATERIALNETWORK_H_
#define _DG3DDEEPMATERIALNETWORK_H_

#include "DeepMaterialNetworks.h"
#include "MaterialNode.h"
#include "materialLaw.h"

#ifndef SWIG
class dG3DVoidCohesiveIPState : public cohesiveIPState
{
  protected:
    std::vector<SVector3> _allJumps, _allJumpsPrev, _allJumpsInit;
  
  public:
    dG3DVoidCohesiveIPState(int numCoh): _allJumps(numCoh,SVector3(0.)),_allJumpsPrev(numCoh,SVector3(0.)),_allJumpsInit(numCoh,SVector3(0.)) {};
    dG3DVoidCohesiveIPState(const dG3DVoidCohesiveIPState& src): _allJumps(src._allJumps), 
                        _allJumpsPrev(src._allJumpsPrev), _allJumpsInit(src._allJumpsInit) {};
    virtual ~dG3DVoidCohesiveIPState(){};
        
    virtual void reset() ;
    virtual SVector3& getRefToJump(int index) ;;
    virtual const SVector3& getConstRefToJump(int index) const;;
    virtual const SVector3& getConstRefToInterfaceForce(int index) const;
    virtual const STensor3& getConstRefToDInterfaceForceDJump(int index) const;
    
    virtual void nextStep();
    virtual void resetToPreviousStep();
    virtual void copy(const IPStateBase::whichState ws1, const IPStateBase::whichState ws2);
};

class dG3DCohesiveIPState : public cohesiveIPState
{
  protected:
    std::vector<IPStateBase*> _allState;
  
  public:
    dG3DCohesiveIPState();
    dG3DCohesiveIPState(const dG3DCohesiveIPState& src);
    virtual ~dG3DCohesiveIPState();
    
    std::vector<IPStateBase*>& getRefToAllState() {return _allState;};
    const std::vector<IPStateBase*>& getConstRefToAllState() const {return _allState;};
    
    virtual IPStateBase* getStateWithIndex(int index);
    virtual const IPStateBase* getStateWithIndex(int index) const;
    virtual void reset();
    virtual SVector3& getRefToJump(int index);
    virtual const SVector3& getConstRefToJump(int index) const;
    virtual const SVector3& getConstRefToInterfaceForce(int index) const;
    virtual const STensor3& getConstRefToDInterfaceForceDJump(int index) const;
    
    virtual void nextStep();
    virtual void resetToPreviousStep();
    virtual void copy(const IPStateBase::whichState ws1, const IPStateBase::whichState ws2);
};
#endif //SWIG

class dG3DDeepMaterialNetwork : public DeepMaterialNetwork
{
  #ifndef SWIG
  protected:
    std::map<int, materialLaw*> _mlaw;
    std::map<std::pair<int,int>, materialLaw*> _fracLaw;
    bool _state;
  #endif //SWIG
  public:
    dG3DDeepMaterialNetwork();
    dG3DDeepMaterialNetwork(const Tree& T);
    dG3DDeepMaterialNetwork(const std::string fname);
    virtual ~dG3DDeepMaterialNetwork();
    void addLaw(int matIndex, materialLaw& l1);
    void setTime(double tcur,double timeStep);
  #ifndef SWIG
  protected:
    virtual void setMacroSolver(const nonLinearMechSolver* sv);
    virtual void createIPState(MaterialNode* node);
    virtual void stressLocal(MaterialNode* node);
    virtual void getDIPCompDFLocal(const IPStateBase* ips, IPField::Output comp, double& val, STensor3& DvalDF);
    
  private:
    void createVoidIPState(IPStateBase*& ips, const bool* state_) const;
  #endif //SWIG
};
#endif //_DG3DDEEPMATERIALNETWORK_H_
