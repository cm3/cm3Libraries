//
// Description:  rigid contact domain
//
//
// Author:  <Van Dung NGUYEN>, (C) 2015
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef DG3DCONTACTDOMAIN_H_
#define DG3DCONTACTDOMAIN_H_

#include "contactDomain.h"
#include "defoDefoContactDomain.h"

class dG3DRigidCylinderContact : public rigidCylinderContactDomain{
 public:
  dG3DRigidCylinderContact(const int tag, const int physMaster, const int physSlave, const int physpt1,
                       const int physpt2,const double penalty,const double h,const double rho,elementFilter *fslave=NULL) :
                           rigidCylinderContactDomain(tag,physMaster,physSlave,physpt1,physpt2,penalty,h,rho,fslave){}

  dG3DRigidCylinderContact(const int tag, const int dimMaster, const int physMaster, const int dimSlave, const int physSlave, const int physpt1,
                       const int physpt2,const double penalty,const double h,const double rho,elementFilter *fslave=NULL):
                       rigidCylinderContactDomain(tag,dimMaster,physMaster,dimSlave,physSlave,physpt1,physpt2,penalty,h,rho,fslave){};
  dG3DRigidCylinderContact(const dG3DRigidCylinderContact &source) : rigidCylinderContactDomain(source){}
  virtual ~dG3DRigidCylinderContact(){}
#ifndef SWIG
  virtual void setDomainAndFunctionSpace(partDomain *dom);
#endif // SWIG
};

// physptBot is the center of cylindrical part at its end opposite to the cone
class dG3DRigidConeContact : public rigidConeContactDomain{
 public:
  dG3DRigidConeContact(const int tag, const int physMaster, const int physSlave,const int physptBase,
                            const int physptTop, const int physptBot,const double penalty,const double bradius,const double h,
                            const double rho,elementFilter *fslave=NULL) : rigidConeContactDomain(tag,physMaster,physSlave,physptBase,
                                                                       physptTop,physptBot,penalty,bradius,h,rho,fslave){}
  dG3DRigidConeContact(const int tag, const int dimMaster, const int physMaster, const int dimSlave, const int physSlave,const int physptBase,
                            const int physptTop, const int physptBot,const double penalty,const double bradius,const double h,
                            const double rho,elementFilter *fslave=NULL) :
                            rigidConeContactDomain(tag,dimMaster,physMaster,dimSlave, physSlave,physptBase,
                                                                       physptTop,physptBot,penalty,bradius,h,rho,fslave){}
 #ifndef SWIG
  dG3DRigidConeContact(const dG3DRigidConeContact &source) : rigidConeContactDomain(source){}
  virtual ~dG3DRigidConeContact(){}
  virtual void setDomainAndFunctionSpace(partDomain *dom);
 #endif // SWIG
};

class dG3DRigidPlaneContactDomain : public rigidPlaneContactDomain{
  public:
    dG3DRigidPlaneContactDomain(const int tag, const int dimMaster, const int phyMaster,
                            const int dimSlave, const int phySlave, const int phyPointBase,
                            const double nx, const double ny, const double nz, const double penalty, const double thick,
                            const double rho, elementFilter *fslave=NULL);
   #ifndef SWIG
    dG3DRigidPlaneContactDomain(const dG3DRigidPlaneContactDomain& src);


    virtual ~dG3DRigidPlaneContactDomain(){}
    virtual void setDomainAndFunctionSpace(partDomain *dom);
  #endif // SWIG
};

class dG3DRigidSphereContactDomain : public rigidSphereContactDomain{
 public:
  dG3DRigidSphereContactDomain(const int tag, const int dimMaster, const int physMaster, 
                       const int dimSlave, const int physSlave, const int physPointBase,
                       const double radius, const double penalty, const double rho, elementFilter *fslave=NULL);

  dG3DRigidSphereContactDomain(const dG3DRigidSphereContactDomain& src);
  virtual ~dG3DRigidSphereContactDomain(){}
#ifndef SWIG
    virtual void setDomainAndFunctionSpace(partDomain *dom);
#endif // SWIG
};

class dG3DNodeOnSurfaceContactDomain : public nodeOnSurfaceDefoDefoContactDomain
{
  public:
    dG3DNodeOnSurfaceContactDomain(const int tag, const int dimMaster, const int phyMaster,
                            const int dimSlave, const int phySlave, const double penalty, const double thick,
                            elementFilter *fslave=NULL);
   void setMatrixByPerturbation(bool pert, double p=1.e-8)
   {
     setStiffByPerturbation(pert);
     setPerturbation(p);
   }
   #ifndef SWIG
    dG3DNodeOnSurfaceContactDomain(const dG3DNodeOnSurfaceContactDomain& src);


    virtual ~dG3DNodeOnSurfaceContactDomain(){}
    virtual void setSlaveDomain(partDomain *dom){ Msg::Error("dG3DNodeOnSurfaceContactDomain::setSlaveDomain needs to put slave and master at the same time");};
    virtual void insertSlaveMasterDomainsAndFunctionSpace(partDomain *slaveDom, elementGroup *gSlave, partDomain *masterDom, elementGroup *gMaster);
  #endif // SWIG
};

#endif // DG3DCONTACTDOMAIN_H_
