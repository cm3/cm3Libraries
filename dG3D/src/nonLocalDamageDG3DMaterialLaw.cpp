//
// C++ Interface: materialLaw
//
// Description: Class with definition of materialLaw for shell
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "nonLocalDamageDG3DMaterialLaw.h"
#include "ipstate.h"
#include "MInterfaceElement.h"
#include "ipField.h"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <string>
#include "nonLocalDamageDG3DIPVariable.h"
#include "dG3DCohesiveIPVariable.h"
#include "FractureCohesiveDG3DIPVariable.h"
#include "STensorOperations.h"
#include "nonLinearMechSolver.h"



virtualNonLocalDamageDG3DMaterialLaw::virtualNonLocalDamageDG3DMaterialLaw(const int num, const dG3DMaterialLaw& base, const int numNLVariable):
  dG3DMaterialLaw(num,base.density(),true), numNonlocalVar(numNLVariable)
{
  elasticStiffness  = (base.elasticStiffness);
  TotalelasticStiffness = (base.TotalelasticStiffness);
  _lawBase = dynamic_cast<dG3DMaterialLaw*>(base.clone());
  characteristicLength.resize(numNLVariable,STensor3(0.));
};

virtualNonLocalDamageDG3DMaterialLaw::virtualNonLocalDamageDG3DMaterialLaw(const virtualNonLocalDamageDG3DMaterialLaw &source):
  dG3DMaterialLaw(source), numNonlocalVar(source.numNonlocalVar),characteristicLength(source.characteristicLength){
  _lawBase = NULL;
  if (source._lawBase){
    _lawBase = dynamic_cast<dG3DMaterialLaw*>(source._lawBase->clone());
  }
};

virtualNonLocalDamageDG3DMaterialLaw::~virtualNonLocalDamageDG3DMaterialLaw(){
  if (_lawBase != NULL) delete _lawBase; _lawBase = NULL;
};

void virtualNonLocalDamageDG3DMaterialLaw::setNonLocalLengthScale(const int index, const CLengthLaw &cLLaw){
  IPCLength* ipCL = NULL;
  cLLaw.createIPVariable(ipCL);
  cLLaw.computeCL(0.,*ipCL);
  characteristicLength[index] = ipCL->getConstRefToCL();
  characteristicLength[index].print("characteristicLength");
  if (ipCL!= NULL) delete ipCL;
};

void virtualNonLocalDamageDG3DMaterialLaw::createIPState(IPStateBase* &ips,bool hasBodyForce,const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  IPStateBase* ipsBase= NULL;
  _lawBase->createIPState(ipsBase,hasBodyForce,state_,ele,nbFF_,GP,gpt);

  dG3DIPVariableBase* dgi = static_cast<dG3DIPVariableBase*>(ipsBase->getState(IPStateBase::initial));
  dG3DIPVariableBase* dg1 = static_cast<dG3DIPVariableBase*>(ipsBase->getState(IPStateBase::previous));
  dG3DIPVariableBase* dg2 = static_cast<dG3DIPVariableBase*>(ipsBase->getState(IPStateBase::current));


  IPVariable* ipvi = new  virtualNonLocalDamageDG3DIPVariable(numNonlocalVar,dgi,characteristicLength);
  IPVariable* ipv1 = new  virtualNonLocalDamageDG3DIPVariable(numNonlocalVar,dg1,characteristicLength);
  IPVariable* ipv2 = new  virtualNonLocalDamageDG3DIPVariable(numNonlocalVar,dg2,characteristicLength);

  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
  delete ipsBase;
}

void virtualNonLocalDamageDG3DMaterialLaw::createIPVariable(IPVariable* &ipv,bool hasBodyForce,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  IPVariable* ipvBase = NULL;
  _lawBase->createIPVariable(ipvBase, hasBodyForce,ele,nbFF_,GP,gpt);
  dG3DIPVariableBase* dgipv = static_cast<dG3DIPVariableBase*>(ipvBase);
  if(ipv !=NULL) delete ipv;
  ipv = new  virtualNonLocalDamageDG3DIPVariable(numNonlocalVar,dgipv,characteristicLength);
  delete ipvBase;
}

void virtualNonLocalDamageDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  /* get ipvariable */
  virtualNonLocalDamageDG3DIPVariable* ipvcur;
  const virtualNonLocalDamageDG3DIPVariable* ipvprev;

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<virtualNonLocalDamageDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const virtualNonLocalDamageDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<virtualNonLocalDamageDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const virtualNonLocalDamageDG3DIPVariable*>(ipvp);
  }
  _lawBase->checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());
};

void virtualNonLocalDamageDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  /* get ipvariable */
  virtualNonLocalDamageDG3DIPVariable* ipvcur;
  const virtualNonLocalDamageDG3DIPVariable* ipvprev;

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<virtualNonLocalDamageDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const virtualNonLocalDamageDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<virtualNonLocalDamageDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const virtualNonLocalDamageDG3DIPVariable*>(ipvp);
  }

  IPVariable* dgIPvCur = static_cast<IPVariable*>(ipvcur->getIPVBase());
  const IPVariable* dgIPvPrev = static_cast<const IPVariable*>(ipvprev->getIPVBase());

  _lawBase->stress(dgIPvCur,dgIPvPrev,stiff,checkfrac, dTangent);
};



NonLocalDamageDG3DMaterialLaw::NonLocalDamageDG3DMaterialLaw(const int num, const double rho, const char *propName) :
                                                              dG3DMaterialLaw(num,rho,true)
{
  _nldlaw = new mlawNonLocalDamage(num,rho,propName);
  double nu = _nldlaw->poissonRatio();
  double mu = _nldlaw->shearModulus();
  double E = 2.*mu*(1.+nu);
  fillElasticStiffness(E, nu, elasticStiffness);
}

NonLocalDamageDG3DMaterialLaw::~NonLocalDamageDG3DMaterialLaw()
{
  if (_nldlaw !=NULL) delete _nldlaw;
  _nldlaw = NULL;
};


NonLocalDamageDG3DMaterialLaw::NonLocalDamageDG3DMaterialLaw(const NonLocalDamageDG3DMaterialLaw &source) :
                                                             dG3DMaterialLaw(source)
{
	_nldlaw = NULL;
	if (source._nldlaw != NULL){
		_nldlaw = dynamic_cast<mlawNonLocalDamage*>(source._nldlaw->clone());
	}

}

void NonLocalDamageDG3DMaterialLaw::setTime(const double t,const double dtime){
  dG3DMaterialLaw::setTime(t,dtime);
  _nldlaw->setTime(t,dtime);
  return;
}

materialLaw::matname NonLocalDamageDG3DMaterialLaw::getType() const {return _nldlaw->getType();}

bool NonLocalDamageDG3DMaterialLaw::withEnergyDissipation() const {return _nldlaw->withEnergyDissipation();};

void NonLocalDamageDG3DMaterialLaw::createIPState(IPStateBase* &ips,bool hasBodyForce,const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;

  int nsdv = _nldlaw->getNsdv();
  int nlVar= _nldlaw->getNbNonLocalVariables();
  IPVariable* ipvi = new  nonLocalDamageDG3DIPVariable(nsdv,nlVar,hasBodyForce,inter);
  IPVariable* ipv1 = new  nonLocalDamageDG3DIPVariable(nsdv,nlVar,hasBodyForce,inter);
  IPVariable* ipv2 = new  nonLocalDamageDG3DIPVariable(nsdv,nlVar,hasBodyForce,inter);

  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
  _nldlaw->createIPState((static_cast <nonLocalDamageDG3DIPVariable*> (ipvi))->getIPNonLocalDamage(),
                         (static_cast <nonLocalDamageDG3DIPVariable*> (ipv1))->getIPNonLocalDamage(),
             			 (static_cast <nonLocalDamageDG3DIPVariable*> (ipv2))->getIPNonLocalDamage());

}

void NonLocalDamageDG3DMaterialLaw::createIPVariable(IPVariable* &ipv,bool hasBodyForce,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL) inter=false;

  ipv = new  nonLocalDamageDG3DIPVariable(_nldlaw->getNsdv(),_nldlaw->getNbNonLocalVariables(),hasBodyForce,inter);

  IPNonLocalDamage * ipvnl = static_cast <nonLocalDamageDG3DIPVariable*>(ipv)->getIPNonLocalDamage();
  _nldlaw->createIPVariable(ipvnl,hasBodyForce,ele,nbFF_,GP,gpt);
}


double NonLocalDamageDG3DMaterialLaw::soundSpeed() const{return _nldlaw->soundSpeed();}

void NonLocalDamageDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  /* get ipvariable */
  nonLocalDamageDG3DIPVariable* ipvcur; //= static_cast < nonLocalDamageDG3DIPVariable *> (ipv);
  const nonLocalDamageDG3DIPVariable* ipvprev; //= static_cast <const  nonLocalDamageDG3DIPVariable *> (ipvp);

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<nonLocalDamageDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const nonLocalDamageDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<nonLocalDamageDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const nonLocalDamageDG3DIPVariable*>(ipvp);
  }
  _nldlaw->checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());

};

void NonLocalDamageDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  /* get ipvariable */
  nonLocalDamageDG3DIPVariable* ipvcur; //= static_cast < nonLocalDamageDG3DIPVariable *> (ipv);
  const nonLocalDamageDG3DIPVariable* ipvprev; //= static_cast <const  nonLocalDamageDG3DIPVariable *> (ipvp);

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<nonLocalDamageDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const nonLocalDamageDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<nonLocalDamageDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const nonLocalDamageDG3DIPVariable*>(ipvp);
  }

  /* strain xyz */
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

  /* data for J2 law */
  IPNonLocalDamage* q1 = ipvcur->getIPNonLocalDamage();
  const IPNonLocalDamage* q0 = ipvprev->getIPNonLocalDamage();

  /* compute stress */
  if(ipvcur-> getNumberNonLocalVariable()>0)
    _nldlaw->constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),
                        ipvcur->getRefToDLocalVariableDStrain(),
                        ipvcur->getRefToDStressDNonLocalVariable(),
                        ipvcur->getRefToDLocalVariableDNonLocalVariable(), 
                        stiff);
  else
    _nldlaw->constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),
                         stiff);


  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);

}

double NonLocalDamageDG3DMaterialLaw::scaleFactor() const{return _nldlaw->scaleFactor();};

void NonLocalDamageDG3DMaterialLaw::setMacroSolver(const nonLinearMechSolver* sv){
	dG3DMaterialLaw::setMacroSolver(sv);
	if (_nldlaw != NULL){
		_nldlaw->setMacroSolver(sv);
	}
};

//**** NonLocalDamageDG3DMaterialLaw_Stochastic
NonLocalDamageDG3DMaterialLaw_Stoch::NonLocalDamageDG3DMaterialLaw_Stoch(const int num, const double rho, const char *propName,
                     const double Ori_x, const double Ori_y, const double Ori_z,const double Lx,const double Ly,const double Lz,
                     const double dx, const double dy, const double dz,  const double euler0,const double euler1,const double euler2,
                     const char *Geo, const char *RandProp) : NonLocalDamageDG3DMaterialLaw(num,rho,propName){
  if(_nldlaw != NULL) delete _nldlaw;
  _nldlaw = new mlawNonLocalDamage_Stoch(num,rho,propName,Ori_x, Ori_y, Ori_z, Lx, Ly, Lz, dx, dy, dz,
                                        euler0,euler1,euler2, Geo, RandProp);

  double nu = _nldlaw->poissonRatio();
  double mu = _nldlaw->shearModulus();
  double E = 2.*mu*(1.+nu);
  fillElasticStiffness(E, nu, elasticStiffness);
}

NonLocalDamageDG3DMaterialLaw_Stoch::NonLocalDamageDG3DMaterialLaw_Stoch(const int num, const double rho, const char *propName, const double Ori_x, const double Ori_y, const double Ori_z,const double Lx,const double Ly,const double Lz, const char *RandProp, const int intpl) : NonLocalDamageDG3DMaterialLaw(num,rho,propName){
  if(_nldlaw != NULL) delete _nldlaw;
  _nldlaw = new mlawNonLocalDamage_Stoch(num,rho,propName,Ori_x, Ori_y, Ori_z, Lx, Ly, Lz, RandProp,intpl);
  double nu = _nldlaw->poissonRatio();
  double mu = _nldlaw->shearModulus();
  double E = 2.*mu*(1.+nu);
  fillElasticStiffness(E, nu, elasticStiffness);
}



NonLocalDamageDG3DMaterialLaw_Stoch::~NonLocalDamageDG3DMaterialLaw_Stoch()
{

 if (_nldlaw !=NULL) delete _nldlaw;
  _nldlaw = NULL;

};


NonLocalDamageDG3DMaterialLaw_Stoch::NonLocalDamageDG3DMaterialLaw_Stoch(const NonLocalDamageDG3DMaterialLaw_Stoch &source) :
                                                             NonLocalDamageDG3DMaterialLaw(source) {}

void NonLocalDamageDG3DMaterialLaw_Stoch::createIPState(IPStateBase* &ips,bool hasBodyForce,const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  SPoint3 pt_Global;
  ele->pnt(GP->pt[0],GP->pt[1],GP->pt[2],pt_Global);
  SVector3 GaussP(pt_Global);

  int nsdv = _nldlaw->getNsdv();
  int nlVar= _nldlaw->getNbNonLocalVariables();
  IPVariable* ipvi = new  nonLocalDamageDG3DIPVariable(nsdv,nlVar,hasBodyForce, inter);
  IPVariable* ipv1 = new  nonLocalDamageDG3DIPVariable(nsdv,nlVar, hasBodyForce,inter);
  IPVariable* ipv2 = new  nonLocalDamageDG3DIPVariable(nsdv,nlVar,hasBodyForce, inter);

  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
 static_cast<mlawNonLocalDamage_Stoch*>(_nldlaw)->createIPState(GaussP, (static_cast <nonLocalDamageDG3DIPVariable*> (ipvi))->getIPNonLocalDamage(),
                         (static_cast <nonLocalDamageDG3DIPVariable*> (ipv1))->getIPNonLocalDamage(),
             			 (static_cast <nonLocalDamageDG3DIPVariable*> (ipv2))->getIPNonLocalDamage());

}

void NonLocalDamageDG3DMaterialLaw_Stoch::createIPVariable(IPVariable* &ipv,bool hasBodyForce,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL) inter=false;
  SPoint3 pt_Global;
  ele->pnt(GP->pt[0],GP->pt[1],GP->pt[2],pt_Global);
  SVector3 GaussP(pt_Global);

  ipv = new  nonLocalDamageDG3DIPVariable(_nldlaw->getNsdv(),_nldlaw->getNbNonLocalVariables(),hasBodyForce,inter);

  IPNonLocalDamage * ipvnl = static_cast <nonLocalDamageDG3DIPVariable*>(ipv)->getIPNonLocalDamage();
  static_cast<mlawNonLocalDamage_Stoch*>(_nldlaw)->createIPVariable(GaussP, ipvnl, hasBodyForce,ele,nbFF_,GP,gpt);
}
// *********

NonLocalDamageHyperelasticDG3DMaterialLaw::NonLocalDamageHyperelasticDG3DMaterialLaw(const int num, const double rho, const elasticPotential& elP, const CLengthLaw &cLLaw,
                                         const DamageLaw &damLaw): dG3DMaterialLaw(num,rho,true){
  _nlLaw = new mlawNonlocalDamageHyperelastic(num,rho,elP,cLLaw,damLaw);

  double E = elP.getYoungModulus();
	double nu = elP.getPoissonRatio();
	fillElasticStiffness(E, nu, elasticStiffness);
};
NonLocalDamageHyperelasticDG3DMaterialLaw::NonLocalDamageHyperelasticDG3DMaterialLaw(const NonLocalDamageHyperelasticDG3DMaterialLaw& src):
dG3DMaterialLaw(src)
{
  _nlLaw = NULL;
  if (src._nlLaw != NULL){
    _nlLaw = dynamic_cast<mlawNonlocalDamageHyperelastic*>(src._nlLaw->clone());
  }
};
void NonLocalDamageHyperelasticDG3DMaterialLaw::setTime(const double t,const double dtime)
{
	dG3DMaterialLaw::setTime(t,dtime);
	_nlLaw->setTime(t,dtime);
}

void NonLocalDamageHyperelasticDG3DMaterialLaw::createIPState(IPStateBase* &ips,bool hasBodyForce,const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
	bool inter=true;
	const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
	if(iele==NULL) inter=false;

	IPVariable* ipvi = new  nonLocalDamageIsotropicElasticityDG3DIPVariable(*_nlLaw,hasBodyForce,inter);
	IPVariable* ipv1 = new  nonLocalDamageIsotropicElasticityDG3DIPVariable(*_nlLaw,hasBodyForce,inter);
	IPVariable* ipv2 = new  nonLocalDamageIsotropicElasticityDG3DIPVariable(*_nlLaw,hasBodyForce,inter);
	if(ips != NULL) delete ips;
	ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void NonLocalDamageHyperelasticDG3DMaterialLaw::createIPVariable(IPVariable* &ipv,bool hasBodyForce,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
	bool inter=true;
	const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
	if(iele == NULL) inter=false;

	if(ipv !=NULL) delete ipv;
	ipv = new  nonLocalDamageIsotropicElasticityDG3DIPVariable(*_nlLaw,hasBodyForce,inter);
}

void NonLocalDamageHyperelasticDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  /* get ipvariable */
  nonLocalDamageIsotropicElasticityDG3DIPVariable* ipvcur; //= static_cast < nonLocalDamageDG3DIPVariable *> (ipv);
  const nonLocalDamageIsotropicElasticityDG3DIPVariable* ipvprev; //= static_cast <const  nonLocalDamageDG3DIPVariable *> (ipvp);

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {
      ipvcur = static_cast<nonLocalDamageIsotropicElasticityDG3DIPVariable*>(ipvtmp->getIPvBulk());
      const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
      ipvprev = static_cast<const nonLocalDamageIsotropicElasticityDG3DIPVariable*>(ipvtmp2->getIPvBulk());
  }
  else
  {
      ipvcur = static_cast<nonLocalDamageIsotropicElasticityDG3DIPVariable*>(ipv);
      ipvprev = static_cast<const nonLocalDamageIsotropicElasticityDG3DIPVariable*>(ipvp);
  }

  _nlLaw->checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());

};

void NonLocalDamageHyperelasticDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
    /* get ipvariable */
    nonLocalDamageIsotropicElasticityDG3DIPVariable* ipvcur; //= static_cast < nonLocalDamageDG3DIPVariable *> (ipv);
    const nonLocalDamageIsotropicElasticityDG3DIPVariable* ipvprev; //= static_cast <const  nonLocalDamageDG3DIPVariable *> (ipvp);

    FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
    if(ipvtmp !=NULL)
		{
				ipvcur = static_cast<nonLocalDamageIsotropicElasticityDG3DIPVariable*>(ipvtmp->getIPvBulk());
				const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
				ipvprev = static_cast<const nonLocalDamageIsotropicElasticityDG3DIPVariable*>(ipvtmp2->getIPvBulk());
		}
    else
		{
				ipvcur = static_cast<nonLocalDamageIsotropicElasticityDG3DIPVariable*>(ipv);
				ipvprev = static_cast<const nonLocalDamageIsotropicElasticityDG3DIPVariable*>(ipvp);
		}

    /* Deformation gradient F */
    const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
    const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

    /* data for non local isotropic elastic law */
    IPNonLocalDamageIsotropicElasticity* q1 = ipvcur->getIPNonLocalDamageIsotropicElasticity();
    const IPNonLocalDamageIsotropicElasticity* q0 = ipvprev->getIPNonLocalDamageIsotropicElasticity();

    /* compute stress */
    _nlLaw->constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),
                                 ipvcur->getRefToDLocalVariableDStrain()[0],ipvcur->getRefToDStressDNonLocalVariable()[0],
                                 ipvcur->getRefToDLocalVariableDNonLocalVariable()(0,0),stiff);

    ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);   // Is it correct ????

}

NonLocalAnisotropicDamageHyperelasticDG3DMaterialLaw::NonLocalAnisotropicDamageHyperelasticDG3DMaterialLaw(const int num, const double rho,
                                        const elasticPotential& elP, const CLengthLaw &cLLaw,
                                         const DamageLaw &damLaw): NonLocalDamageHyperelasticDG3DMaterialLaw(num,rho,elP,cLLaw,damLaw){
  if (_nlLaw != NULL) delete _nlLaw;
  _nlLaw = new mlawNonlocalAnisotropicDamageHyperelastic(num,rho,elP,cLLaw,damLaw);
};


NonLocalDamageIsotropicElasticityDG3DMaterialLaw::NonLocalDamageIsotropicElasticityDG3DMaterialLaw(
    const int num,const double rho, double E,const double nu, const CLengthLaw &cLLaw,const DamageLaw &damLaw) :
    dG3DMaterialLaw(num,rho,true)
{
    _nldIsotropicElasticitylaw = new mlawNonLocalDamageIsotropicElasticity(num,rho,E,nu,cLLaw,damLaw);
    fillElasticStiffness(E, nu, elasticStiffness); // Is it usefull ?
}

NonLocalDamageIsotropicElasticityDG3DMaterialLaw::~NonLocalDamageIsotropicElasticityDG3DMaterialLaw()
{
		if (_nldIsotropicElasticitylaw!=NULL){
			delete _nldIsotropicElasticitylaw;
			_nldIsotropicElasticitylaw = NULL;
		}
};

NonLocalDamageIsotropicElasticityDG3DMaterialLaw::NonLocalDamageIsotropicElasticityDG3DMaterialLaw(const NonLocalDamageIsotropicElasticityDG3DMaterialLaw &source) :
    dG3DMaterialLaw(source)
{
	_nldIsotropicElasticitylaw = NULL;
	if (source._nldIsotropicElasticitylaw != NULL){
    _nldIsotropicElasticitylaw = new mlawNonLocalDamageIsotropicElasticity(*(source._nldIsotropicElasticitylaw));
	}
}

void NonLocalDamageIsotropicElasticityDG3DMaterialLaw::setNonLocalLimitingMethod(const int num){_nldIsotropicElasticitylaw->setNonLocalLimitingMethod(num);};

void NonLocalDamageIsotropicElasticityDG3DMaterialLaw::setTime(const double t,const double dtime)
{
    dG3DMaterialLaw::setTime(t,dtime);
    _nldIsotropicElasticitylaw->setTime(t,dtime);
}

materialLaw::matname NonLocalDamageIsotropicElasticityDG3DMaterialLaw::getType() const
{
    return _nldIsotropicElasticitylaw->getType();
}

void NonLocalDamageIsotropicElasticityDG3DMaterialLaw::createIPState(IPStateBase* &ips,bool hasBodyForce,const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
    // check if interface element or not
    bool inter=true;
    const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
    if(iele==NULL) inter=false;

    IPVariable* ipvi = new  nonLocalDamageIsotropicElasticityDG3DIPVariable(*_nldIsotropicElasticitylaw,hasBodyForce,inter);
    IPVariable* ipv1 = new  nonLocalDamageIsotropicElasticityDG3DIPVariable(*_nldIsotropicElasticitylaw,hasBodyForce,inter);
    IPVariable* ipv2 = new  nonLocalDamageIsotropicElasticityDG3DIPVariable(*_nldIsotropicElasticitylaw,hasBodyForce,inter);
    if(ips != NULL) delete ips;
    ips = new IP3State(state_,ipvi,ipv1,ipv2);

    _nldIsotropicElasticitylaw->createIPState((static_cast <nonLocalDamageIsotropicElasticityDG3DIPVariable*> (ipvi))->getIPNonLocalDamageIsotropicElasticity(),
                                  (static_cast <nonLocalDamageIsotropicElasticityDG3DIPVariable*> (ipv1))->getIPNonLocalDamageIsotropicElasticity(),
                                  (static_cast <nonLocalDamageIsotropicElasticityDG3DIPVariable*> (ipv2))->getIPNonLocalDamageIsotropicElasticity());

}

void NonLocalDamageIsotropicElasticityDG3DMaterialLaw::createIPVariable(IPVariable* &ipv,bool hasBodyForce,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
    // check if interface element or not
    bool inter=true;
    const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
    if(iele == NULL) inter=false;

    if(ipv !=NULL) delete ipv;
    ipv = new  nonLocalDamageIsotropicElasticityDG3DIPVariable(*_nldIsotropicElasticitylaw,hasBodyForce,inter);
    IPNonLocalDamageIsotropicElasticity * ipvnl = static_cast <nonLocalDamageIsotropicElasticityDG3DIPVariable*>(ipv)->getIPNonLocalDamageIsotropicElasticity();
    _nldIsotropicElasticitylaw->createIPVariable(ipvnl);
}

double NonLocalDamageIsotropicElasticityDG3DMaterialLaw::soundSpeed() const
{
    return _nldIsotropicElasticitylaw->soundSpeed();
}

void NonLocalDamageIsotropicElasticityDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  /* get ipvariable */
    nonLocalDamageIsotropicElasticityDG3DIPVariable* ipvcur; //= static_cast < nonLocalDamageDG3DIPVariable *> (ipv);
    const nonLocalDamageIsotropicElasticityDG3DIPVariable* ipvprev; //= static_cast <const  nonLocalDamageDG3DIPVariable *> (ipvp);

    FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
    if(ipvtmp !=NULL)
        {

            ipvcur = static_cast<nonLocalDamageIsotropicElasticityDG3DIPVariable*>(ipvtmp->getIPvBulk());
            const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
            ipvprev = static_cast<const nonLocalDamageIsotropicElasticityDG3DIPVariable*>(ipvtmp2->getIPvBulk());
        }
    else
        {
            ipvcur = static_cast<nonLocalDamageIsotropicElasticityDG3DIPVariable*>(ipv);
            ipvprev = static_cast<const nonLocalDamageIsotropicElasticityDG3DIPVariable*>(ipvp);
        }

    _nldIsotropicElasticitylaw->checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());
};

void NonLocalDamageIsotropicElasticityDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
    /* get ipvariable */
    nonLocalDamageIsotropicElasticityDG3DIPVariable* ipvcur; //= static_cast < nonLocalDamageDG3DIPVariable *> (ipv);
    const nonLocalDamageIsotropicElasticityDG3DIPVariable* ipvprev; //= static_cast <const  nonLocalDamageDG3DIPVariable *> (ipvp);

    FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
    if(ipvtmp !=NULL)
        {

            ipvcur = static_cast<nonLocalDamageIsotropicElasticityDG3DIPVariable*>(ipvtmp->getIPvBulk());
            const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
            ipvprev = static_cast<const nonLocalDamageIsotropicElasticityDG3DIPVariable*>(ipvtmp2->getIPvBulk());
        }
    else
        {
            ipvcur = static_cast<nonLocalDamageIsotropicElasticityDG3DIPVariable*>(ipv);
            ipvprev = static_cast<const nonLocalDamageIsotropicElasticityDG3DIPVariable*>(ipvp);
        }

    /* Deformation gradient F */
    const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
    const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

    /* data for non local isotropic elastic law */
    IPNonLocalDamageIsotropicElasticity* q1 = ipvcur->getIPNonLocalDamageIsotropicElasticity();
    const IPNonLocalDamageIsotropicElasticity* q0 = ipvprev->getIPNonLocalDamageIsotropicElasticity();

    /* compute stress */
    _nldIsotropicElasticitylaw->constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),
                                 ipvcur->getRefToDLocalVariableDStrain()[0],ipvcur->getRefToDStressDNonLocalVariable()[0],
                                 ipvcur->getRefToDLocalVariableDNonLocalVariable()(0,0),stiff);

    ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);   // Is it correct ????

}

//
//
NonLocalDamageJ2SmallStrainDG3DMaterialLaw::NonLocalDamageJ2SmallStrainDG3DMaterialLaw(const int num,const double rho,
                   double E,const double nu,const J2IsotropicHardening &_j2IH,
                    const CLengthLaw &_cLLaw,const DamageLaw &_damLaw,const double tol,
                    const bool matrixBypert, const double tolPert) :
                                                              dG3DMaterialLaw(num,rho,true)
{
  _nldJ2smallStrain = new mlawNonLocalDamageJ2SmallStrain(num,E,nu,rho,_j2IH,_cLLaw,_damLaw,tol,matrixBypert,tolPert);
  fillElasticStiffness(E, nu, elasticStiffness);
}

NonLocalDamageJ2SmallStrainDG3DMaterialLaw::~NonLocalDamageJ2SmallStrainDG3DMaterialLaw() {
	if (_nldJ2smallStrain!=NULL){
		delete _nldJ2smallStrain;
		_nldJ2smallStrain = NULL;
	}
};


NonLocalDamageJ2SmallStrainDG3DMaterialLaw::NonLocalDamageJ2SmallStrainDG3DMaterialLaw(const NonLocalDamageJ2SmallStrainDG3DMaterialLaw &source) :
                                                             dG3DMaterialLaw(source)
{
	_nldJ2smallStrain = NULL;
	if (source._nldJ2smallStrain != NULL){
		_nldJ2smallStrain = new mlawNonLocalDamageJ2SmallStrain(*(source._nldJ2smallStrain));
	}
}

void NonLocalDamageJ2SmallStrainDG3DMaterialLaw::setTime(const double t,const double dtime){
  dG3DMaterialLaw::setTime(t,dtime);
  _nldJ2smallStrain->setTime(t,dtime);
}

materialLaw::matname
NonLocalDamageJ2SmallStrainDG3DMaterialLaw::getType() const {return _nldJ2smallStrain->getType();}

void NonLocalDamageJ2SmallStrainDG3DMaterialLaw::createIPState(IPStateBase* &ips,bool hasBodyForce,const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  IPVariable* ipvi = new  nonLocalDamageJ2HyperDG3DIPVariable(*_nldJ2smallStrain,hasBodyForce,inter);
  IPVariable* ipv1 = new  nonLocalDamageJ2HyperDG3DIPVariable(*_nldJ2smallStrain,hasBodyForce,inter);
  IPVariable* ipv2 = new  nonLocalDamageJ2HyperDG3DIPVariable(*_nldJ2smallStrain,hasBodyForce,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
  _nldJ2smallStrain->createIPState((static_cast <nonLocalDamageJ2HyperDG3DIPVariable*> (ipvi))->getIPNonLocalDamageJ2Hyper(),
                         (static_cast <nonLocalDamageJ2HyperDG3DIPVariable*> (ipv1))->getIPNonLocalDamageJ2Hyper(),
             			 (static_cast <nonLocalDamageJ2HyperDG3DIPVariable*> (ipv2))->getIPNonLocalDamageJ2Hyper());

}

void NonLocalDamageJ2SmallStrainDG3DMaterialLaw::createIPVariable(IPVariable* &ipv,bool hasBodyForce,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  ipv = new  nonLocalDamageJ2HyperDG3DIPVariable(*_nldJ2smallStrain,hasBodyForce,inter);
  IPNonLocalDamageJ2Hyper * ipvnl = static_cast <nonLocalDamageJ2HyperDG3DIPVariable*>(ipv)->getIPNonLocalDamageJ2Hyper();
  _nldJ2smallStrain->createIPVariable(ipvnl);
}

double
NonLocalDamageJ2SmallStrainDG3DMaterialLaw::soundSpeed() const{return _nldJ2smallStrain->soundSpeed();} // or change value ??


void NonLocalDamageJ2SmallStrainDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  /* get ipvariable */
  nonLocalDamageJ2HyperDG3DIPVariable* ipvcur; //= static_cast < nonLocalDamageDG3DIPVariable *> (ipv);
  const nonLocalDamageJ2HyperDG3DIPVariable* ipvprev; //= static_cast <const  nonLocalDamageDG3DIPVariable *> (ipvp);

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<nonLocalDamageJ2HyperDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const nonLocalDamageJ2HyperDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<nonLocalDamageJ2HyperDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const nonLocalDamageJ2HyperDG3DIPVariable*>(ipvp);
  }

  _nldJ2smallStrain->checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());
};

void NonLocalDamageJ2SmallStrainDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  /* get ipvariable */
  nonLocalDamageJ2HyperDG3DIPVariable* ipvcur; //= static_cast < nonLocalDamageDG3DIPVariable *> (ipv);
  const nonLocalDamageJ2HyperDG3DIPVariable* ipvprev; //= static_cast <const  nonLocalDamageDG3DIPVariable *> (ipvp);

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<nonLocalDamageJ2HyperDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const nonLocalDamageJ2HyperDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<nonLocalDamageJ2HyperDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const nonLocalDamageJ2HyperDG3DIPVariable*>(ipvp);
  }

  /* strain xyz */
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

  /* data for J2 law */
  IPNonLocalDamageJ2Hyper* q1 = ipvcur->getIPNonLocalDamageJ2Hyper();
  const IPNonLocalDamageJ2Hyper* q0 = ipvprev->getIPNonLocalDamageJ2Hyper();

  STensor43& elasticL = ipvcur->getRefToElasticTangentModuli();

  /* compute stress */
  _nldJ2smallStrain->constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),
                        ipvcur->getRefToDLocalVariableDStrain()[0],ipvcur->getRefToDStressDNonLocalVariable()[0],
                        ipvcur->getRefToDLocalVariableDNonLocalVariable()(0,0),stiff,&elasticL);

  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);

}

//
NonLocalDamageJ2HyperDG3DMaterialLaw::NonLocalDamageJ2HyperDG3DMaterialLaw(const int num,const double rho,
                   double E,const double nu,const J2IsotropicHardening &_j2IH,
                    const CLengthLaw &_cLLaw,const DamageLaw &_damLaw,const double tol,
                    const bool matrixBypert, const double tolPert) :
                                                              dG3DMaterialLaw(num,rho,true)
{
  _nldJ2Hyperlaw = new mlawNonLocalDamageJ2Hyper(num,E,nu,rho,_j2IH,_cLLaw,_damLaw,tol,matrixBypert,tolPert);
  fillElasticStiffness(E, nu, elasticStiffness);
}

NonLocalDamageJ2HyperDG3DMaterialLaw::~NonLocalDamageJ2HyperDG3DMaterialLaw() {
	if (_nldJ2Hyperlaw!=NULL){
		delete _nldJ2Hyperlaw;
		_nldJ2Hyperlaw = NULL;
	}
};


NonLocalDamageJ2HyperDG3DMaterialLaw::NonLocalDamageJ2HyperDG3DMaterialLaw(const NonLocalDamageJ2HyperDG3DMaterialLaw &source) :
                                                             dG3DMaterialLaw(source)
{
	_nldJ2Hyperlaw = NULL;
	if (source._nldJ2Hyperlaw != NULL){
		_nldJ2Hyperlaw = new mlawNonLocalDamageJ2Hyper(*(source._nldJ2Hyperlaw));
	}
}

void NonLocalDamageJ2HyperDG3DMaterialLaw::setStrainOrder(const int order){
  _nldJ2Hyperlaw->setStrainOrder(order);
};

void
NonLocalDamageJ2HyperDG3DMaterialLaw::setTime(const double t,const double dtime){
  dG3DMaterialLaw::setTime(t,dtime);
  _nldJ2Hyperlaw->setTime(t,dtime);
}

materialLaw::matname
NonLocalDamageJ2HyperDG3DMaterialLaw::getType() const {return _nldJ2Hyperlaw->getType();}

void NonLocalDamageJ2HyperDG3DMaterialLaw::createIPState(IPStateBase* &ips,bool hasBodyForce,const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  IPVariable* ipvi = new  nonLocalDamageJ2HyperDG3DIPVariable(*_nldJ2Hyperlaw,hasBodyForce,inter);
  IPVariable* ipv1 = new  nonLocalDamageJ2HyperDG3DIPVariable(*_nldJ2Hyperlaw,hasBodyForce,inter);
  IPVariable* ipv2 = new  nonLocalDamageJ2HyperDG3DIPVariable(*_nldJ2Hyperlaw,hasBodyForce,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
  _nldJ2Hyperlaw->createIPState((static_cast <nonLocalDamageJ2HyperDG3DIPVariable*> (ipvi))->getIPNonLocalDamageJ2Hyper(),
                         (static_cast <nonLocalDamageJ2HyperDG3DIPVariable*> (ipv1))->getIPNonLocalDamageJ2Hyper(),
             			 (static_cast <nonLocalDamageJ2HyperDG3DIPVariable*> (ipv2))->getIPNonLocalDamageJ2Hyper());

}

void NonLocalDamageJ2HyperDG3DMaterialLaw::createIPVariable(IPVariable* &ipv,bool hasBodyForce,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  ipv = new  nonLocalDamageJ2HyperDG3DIPVariable(*_nldJ2Hyperlaw,hasBodyForce,inter);
  IPNonLocalDamageJ2Hyper * ipvnl = static_cast <nonLocalDamageJ2HyperDG3DIPVariable*>(ipv)->getIPNonLocalDamageJ2Hyper();
  _nldJ2Hyperlaw->createIPVariable(ipvnl);
}

double
NonLocalDamageJ2HyperDG3DMaterialLaw::soundSpeed() const{return _nldJ2Hyperlaw->soundSpeed();} // or change value ??

void NonLocalDamageJ2HyperDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  /* get ipvariable */
  nonLocalDamageJ2HyperDG3DIPVariable* ipvcur; //= static_cast < nonLocalDamageDG3DIPVariable *> (ipv);
  const nonLocalDamageJ2HyperDG3DIPVariable* ipvprev; //= static_cast <const  nonLocalDamageDG3DIPVariable *> (ipvp);

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<nonLocalDamageJ2HyperDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const nonLocalDamageJ2HyperDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<nonLocalDamageJ2HyperDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const nonLocalDamageJ2HyperDG3DIPVariable*>(ipvp);
  }
  _nldJ2Hyperlaw->checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());
};
void NonLocalDamageJ2HyperDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  /* get ipvariable */
  nonLocalDamageJ2HyperDG3DIPVariable* ipvcur; //= static_cast < nonLocalDamageDG3DIPVariable *> (ipv);
  const nonLocalDamageJ2HyperDG3DIPVariable* ipvprev; //= static_cast <const  nonLocalDamageDG3DIPVariable *> (ipvp);

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<nonLocalDamageJ2HyperDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const nonLocalDamageJ2HyperDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<nonLocalDamageJ2HyperDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const nonLocalDamageJ2HyperDG3DIPVariable*>(ipvp);
  }

  /* strain xyz */
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

  /* data for J2 law */
  IPNonLocalDamageJ2Hyper* q1 = ipvcur->getIPNonLocalDamageJ2Hyper();
  const IPNonLocalDamageJ2Hyper* q0 = ipvprev->getIPNonLocalDamageJ2Hyper();

  STensor43& elasticL = ipvcur->getRefToElasticTangentModuli();

  /* compute stress */
  _nldJ2Hyperlaw->constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),
                        ipvcur->getRefToDLocalVariableDStrain()[0],ipvcur->getRefToDStressDNonLocalVariable()[0],
                        ipvcur->getRefToDLocalVariableDNonLocalVariable()(0,0),stiff,&elasticL);

  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);

}

//

NonLocalPorousDuctileDamageDG3DMaterialLaw::NonLocalPorousDuctileDamageDG3DMaterialLaw(const int num, const double rho, const bool init):
  dG3DMaterialLaw(num,rho,init), _nldPorous(NULL),_useI1J2J3Implementation(false),_tangentByPerturbationDuringCoalescence(false),_pertFactor(1e-8){};
NonLocalPorousDuctileDamageDG3DMaterialLaw::NonLocalPorousDuctileDamageDG3DMaterialLaw(const NonLocalPorousDuctileDamageDG3DMaterialLaw &source):
  dG3DMaterialLaw(source),_imposedInitialPorosity(source._imposedInitialPorosity),linearK(source.linearK),Stiff_alphaDilatation(source.Stiff_alphaDilatation),
  _useI1J2J3Implementation(source._useI1J2J3Implementation),_tangentByPerturbationDuringCoalescence(source._tangentByPerturbationDuringCoalescence),
  _pertFactor(source._pertFactor)
{
  if (source._nldPorous != NULL){
    _nldPorous =dynamic_cast<mlawNonLocalPorosity*>(source._nldPorous->clone());
  }
  else{
    _nldPorous = NULL;
  }
};
NonLocalPorousDuctileDamageDG3DMaterialLaw::~NonLocalPorousDuctileDamageDG3DMaterialLaw(){
  if (_nldPorous!=NULL) {delete _nldPorous; _nldPorous = NULL;};
};


void NonLocalPorousDuctileDamageDG3DMaterialLaw::useI1J2J3Implementation(const bool fl){
  _useI1J2J3Implementation = fl;
  if (_useI1J2J3Implementation){
    Msg::Info("I1J2J3 implementation is used !!!!!!!!!!");
  }
}

void NonLocalPorousDuctileDamageDG3DMaterialLaw::setTangentByPerturbationDuringCoalescence(bool fl, double pr)
{
  _tangentByPerturbationDuringCoalescence = fl;
  _pertFactor = pr;
  if (fl)
  {
    printf("using tangent by pertubrtaion during coalescence\n");
  }
}

void NonLocalPorousDuctileDamageDG3DMaterialLaw::setStressFormulation(const int fm){
  _nldPorous->setStressFormulation(fm);
}

void NonLocalPorousDuctileDamageDG3DMaterialLaw::activateVoidNucleationDuringCoalescence(const bool fl){
  _nldPorous->activateNucleationDuringCoalescence(fl);
};

void NonLocalPorousDuctileDamageDG3DMaterialLaw::setOrderForLogExp(const int newOrder){
  _nldPorous->setOrderForLogExp(newOrder);
};
void NonLocalPorousDuctileDamageDG3DMaterialLaw::setViscosityLaw(const viscosityLaw& visco){
  _nldPorous->setViscoPlasticLaw(visco);
};

void NonLocalPorousDuctileDamageDG3DMaterialLaw::setNucleationLaw(const NucleationLaw& nuclLaw)
{
  _nldPorous->setNucleationLaw(nuclLaw);
};

void NonLocalPorousDuctileDamageDG3DMaterialLaw::setNucleationLaw(const NucleationFunctionLaw& added_function, const NucleationCriterionLaw* added_criterion)
{
  _nldPorous->setNucleationLaw(added_function, added_criterion);
};

void NonLocalPorousDuctileDamageDG3DMaterialLaw::setScatterredInitialPorosity(const double fmin, const double fmax){
    _nldPorous->setScatterredInitialPorosity(fmin,fmax);
};
void NonLocalPorousDuctileDamageDG3DMaterialLaw::setCoalescenceLaw(const CoalescenceLaw& added_coalesLaw){
  _nldPorous->setCoalescenceLaw(added_coalesLaw);
};

void NonLocalPorousDuctileDamageDG3DMaterialLaw::setVoidEvolutionLaw(const voidStateEvolutionLaw& added_evolutionLaw){
  _nldPorous->setVoidEvolutionLaw(added_evolutionLaw);
};

void NonLocalPorousDuctileDamageDG3DMaterialLaw::setSubStepping(const bool fl, const int maxNumStep){
  _nldPorous->setSubStepping(fl,maxNumStep);
}

void NonLocalPorousDuctileDamageDG3DMaterialLaw::setPostBlockedDissipationBehavior(const int method){
  _nldPorous->setPostBlockedDissipationBehavior(method);
};

void NonLocalPorousDuctileDamageDG3DMaterialLaw::setFailureTolerance(const double NewFailureTol){
  _nldPorous->setFailureTolerance(NewFailureTol);
};

void NonLocalPorousDuctileDamageDG3DMaterialLaw::setLocalFailurePorosity(const double newFvMax){
  _nldPorous->setLocalFailurePorosity(newFvMax);
};

void NonLocalPorousDuctileDamageDG3DMaterialLaw::setCorrectedRegularizedFunction(const scalarFunction& fct){
  _nldPorous->setCorrectedRegularizedFunction(fct);
};

void NonLocalPorousDuctileDamageDG3DMaterialLaw::setNonLocalMethod(const int i) {
  _nldPorous->setNonLocalMethod(i);
};

void NonLocalPorousDuctileDamageDG3DMaterialLaw::setShearPorosityGrowthFactor(const double k){
  Msg::Info("set shear contribution in porosity growth kw = %e",k);
  _nldPorous->setShearPorosityGrowth_Factor(k);
};

void NonLocalPorousDuctileDamageDG3DMaterialLaw::setStressTriaxialityFunction_kw(const scalarFunction& fct){
  _nldPorous->setShearPorosityGrowth_StressTriaxialityFunction(fct);
};

void NonLocalPorousDuctileDamageDG3DMaterialLaw::setLodeFunction(const scalarFunction& fct){
  _nldPorous->setShearPorosityGrowth_LodeFunction(fct);
};

void NonLocalPorousDuctileDamageDG3DMaterialLaw::clearCLengthLaw(){
  _nldPorous->clearCLengthLaw();
};
void NonLocalPorousDuctileDamageDG3DMaterialLaw::setCLengthLaw(const CLengthLaw& clength){
  _nldPorous->setCLengthLaw(clength);
};

void NonLocalPorousDuctileDamageDG3DMaterialLaw::setReferenceT(const double T){
  _nldPorous->setReferenceT(T);
};

void NonLocalPorousDuctileDamageDG3DMaterialLaw::setInitialPorosityOnElement(const int ele, const double fInit){
  Msg::Info("set initial porosity: ele = %d f0 = %e",ele,fInit);
  _imposedInitialPorosity[ele] = fInit;
};
void NonLocalPorousDuctileDamageDG3DMaterialLaw::setPlasticInstabilityFunctionLaw(const PlasticInstabilityFunctionLaw &plIns)
{
  _nldPorous->setPlasticInstabilityFunctionLaw(plIns);
}
void NonLocalPorousDuctileDamageDG3DMaterialLaw::setTemperatureFunction_rho(const scalarFunction& Tfunc)
{
  _nldPorous->setTemperatureFunction_rho(Tfunc);
};

void NonLocalPorousDuctileDamageDG3DMaterialLaw::setTemperatureFunction_E(const scalarFunction& Tfunc)
{
  _nldPorous->setTemperatureFunction_E(Tfunc);
};

void NonLocalPorousDuctileDamageDG3DMaterialLaw::setTemperatureFunction_nu(const scalarFunction& Tfunc)
{
  _nldPorous->setTemperatureFunction_nu(Tfunc);
};

void NonLocalPorousDuctileDamageDG3DMaterialLaw::setTemperatureFunction_alp(const scalarFunction& Tfunc)
{
  _nldPorous->setTemperatureFunction_alphaDilatation(Tfunc);
};

void NonLocalPorousDuctileDamageDG3DMaterialLaw::setThermomechanicsCoupling(const bool fl){
  _nldPorous->setThermomechanicsCoupling(fl);
};
void NonLocalPorousDuctileDamageDG3DMaterialLaw::setThermalExpansionCoefficient(const double alp){
  _nldPorous->setThermalExpansionCoefficient(alp);
};
void NonLocalPorousDuctileDamageDG3DMaterialLaw::setReferenceThermalConductivity(const double KK){
  _nldPorous->setReferenceThermalConductivity(KK);
};
void NonLocalPorousDuctileDamageDG3DMaterialLaw::setTemperatureFunction_ThermalConductivity(const scalarFunction& Tfunc){
  _nldPorous->setTemperatureFunction_ThermalConductivity(Tfunc);
};
void NonLocalPorousDuctileDamageDG3DMaterialLaw::setReferenceCp(const double Cp){
  _nldPorous->setReferenceCp(Cp);
};
void NonLocalPorousDuctileDamageDG3DMaterialLaw::setTemperatureFunction_Cp(const scalarFunction& Tfunc){
  _nldPorous->setTemperatureFunction_cp(Tfunc);
};
void NonLocalPorousDuctileDamageDG3DMaterialLaw::setThermalEstimationPreviousConfig(const bool flag){
  _nldPorous->setThermalEstimationPreviousConfig(flag);
};
void NonLocalPorousDuctileDamageDG3DMaterialLaw::setTaylorQuineyFactor(const double f){
  _nldPorous->setTaylorQuineyFactor(f);
};

void NonLocalPorousDuctileDamageDG3DMaterialLaw::createIPState(IPStateBase* &ips,bool hasBodyForce,const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  double fvInit = _nldPorous->generateARandomInitialPorosityValue();
  std::map<int,double>::const_iterator it = _imposedInitialPorosity.find(ele->getNum());
  if (it != _imposedInitialPorosity.end()){
    fvInit = it->second;
  }

  int numNL=_nldPorous->getNumNonLocalVariables();
  int numExtraDof=0;
  if(_nldPorous->isThermomechanicallyCoupled())
    numExtraDof=1;

  IPVariable* ipvi = new  nonLocalPorosityDG3DIPVariable(*_nldPorous,fvInit,hasBodyForce,inter,numNL,numExtraDof);
  IPVariable* ipv1 = new  nonLocalPorosityDG3DIPVariable(*_nldPorous,fvInit,hasBodyForce,inter,numNL,numExtraDof);
  IPVariable* ipv2 = new  nonLocalPorosityDG3DIPVariable(*_nldPorous,fvInit,hasBodyForce,inter,numNL,numExtraDof);

  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void NonLocalPorousDuctileDamageDG3DMaterialLaw::createIPVariable(IPVariable* &ipv,bool hasBodyForce,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  double fvInit = _nldPorous->generateARandomInitialPorosityValue();
  std::map<int,double>::const_iterator it = _imposedInitialPorosity.find(ele->getNum());
  if (it != _imposedInitialPorosity.end()){
    fvInit = it->second;
  }
  int numNL=_nldPorous->getNumNonLocalVariables();
  int numExtraDof=0;
  if(_nldPorous->isThermomechanicallyCoupled())
    numExtraDof=1;
  
  if(ipv !=NULL) delete ipv;
  ipv = new  nonLocalPorosityDG3DIPVariable(*_nldPorous,fvInit,hasBodyForce,inter,numNL,numExtraDof);
}


void NonLocalPorousDuctileDamageDG3DMaterialLaw::initLaws(const std::map<int,materialLaw*> &maplaw){
  _nldPorous->initLaws( maplaw );
}


void NonLocalPorousDuctileDamageDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  /* get ipvariable */

  nonLocalPorosityDG3DIPVariable* ipvcur;
  const nonLocalPorosityDG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);

  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<nonLocalPorosityDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const nonLocalPorosityDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<nonLocalPorosityDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const nonLocalPorosityDG3DIPVariable*>(ipvp);
  }
  _nldPorous->checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());
};

bool NonLocalPorousDuctileDamageDG3DMaterialLaw::checkCrackCriterion(IPVariable* ipv, const IPVariable *ipvp, const SVector3& normal, double delayFactor) const
{
  nonLocalPorosityDG3DIPVariable* ipvcur;
  const nonLocalPorosityDG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {
    ipvcur = static_cast<nonLocalPorosityDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const nonLocalPorosityDG3DIPVariable*>(ipvtmp2->getIPvBulk());
  }
  else
  {
    ipvcur = static_cast<nonLocalPorosityDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const nonLocalPorosityDG3DIPVariable*>(ipvp);
  }
  
   /* data for J2 law */
  IPNonLocalPorosity* q1 = ipvcur->getIPNonLocalPorosity();
  const IPNonLocalPorosity* q0 = ipvprev->getIPNonLocalPorosity();
  const STensor3& Fcur = ipvcur->getConstRefToDeformationGradient();
  if(_nldPorous->isThermomechanicallyCoupled())
  {
    double T = ipvcur->getConstRefToField(0);
    return _nldPorous->checkCrackCriterion(q1,q0,Fcur,normal,delayFactor,&T);
  }
  else
  {
    return _nldPorous->checkCrackCriterion(q1,q0,Fcur,normal,delayFactor, NULL);
  }
  return false;
}

void NonLocalPorousDuctileDamageDG3DMaterialLaw::forceCoalescence(IPVariable*ipv, const IPVariable* ipvp) const
{
  nonLocalPorosityDG3DIPVariable* ipvcur;
  const nonLocalPorosityDG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);

  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<nonLocalPorosityDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const nonLocalPorosityDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<nonLocalPorosityDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const nonLocalPorosityDG3DIPVariable*>(ipvp);
  }
  
   /* data for J2 law */
  IPNonLocalPorosity* q1 = ipvcur->getIPNonLocalPorosity();
  const IPNonLocalPorosity* q0 = ipvprev->getIPNonLocalPorosity();
  if(_nldPorous->isThermomechanicallyCoupled())
  {
    double T = ipvcur->getConstRefToField(0);
    _nldPorous->forceCoalescence(q1,q0,&T);
  }
  else
  {
    _nldPorous->forceCoalescence(q1,q0,NULL);
  }
  
};

void NonLocalPorousDuctileDamageDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  /* get ipvariable */

  nonLocalPorosityDG3DIPVariable* ipvcur;
  const nonLocalPorosityDG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);

  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<nonLocalPorosityDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const nonLocalPorosityDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<nonLocalPorosityDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const nonLocalPorosityDG3DIPVariable*>(ipvp);
  }


  /* strain xyz */
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

  /* data for J2 law */
  IPNonLocalPorosity* q1 = ipvcur->getIPNonLocalPorosity();
  const IPNonLocalPorosity* q0 = ipvprev->getIPNonLocalPorosity();

  // set additional kinematic variables
	q1->setRefToCurrentOutwardNormal(ipvcur->getConstRefToCurrentOutwardNormal());
	q1->setRefToReferenceOutwardNormal(ipvcur->getConstRefToReferenceOutwardNormal());
  q1->setRefToDeformationGradient(ipvcur->getConstRefToDeformationGradient());
  
  
  if (_tangentByPerturbationDuringCoalescence)
  {
    if (q1->getConstRefToIPCoalescence().getCoalescenceActiveFlag())
    {
      _nldPorous->setTangentByPerturbation(true,_pertFactor);
    }
    else
    {
      _nldPorous->setTangentByPerturbation(false,_pertFactor);
    }
  }

  /* compute stress */
  if(!_nldPorous->isThermomechanicallyCoupled())
  {
    if (_useI1J2J3Implementation){
      if (_nldPorous->getNonLocalMethod() == mlawNonLocalPorosity::LOCAL){
          _nldPorous->I1J2J3_constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),stiff);
      }
      else
      {
        _nldPorous->I1J2J3_constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),  
                          ipvcur->getRefToDLocalVariableDStrain(),ipvcur->getRefToDStressDNonLocalVariable(),
                          ipvcur->getRefToDLocalVariableDNonLocalVariable(),stiff);
      }
    }
    else{
      if (_nldPorous->getNonLocalMethod() == mlawNonLocalPorosity::LOCAL){
        _nldPorous->constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),stiff);
      }
      else {
        _nldPorous->constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),
                          ipvcur->getRefToDLocalVariableDStrain(),ipvcur->getRefToDStressDNonLocalVariable(),
                          ipvcur->getRefToDLocalVariableDNonLocalVariable(),stiff);
      }
    }

  }
  else
  {
    // set temperature
    q1->setTemperature(ipvcur->getConstRefToField(0));
    if (_nldPorous->getNonLocalMethod() == mlawNonLocalPorosity::LOCAL){
      Msg::Error("local model for thermomechanics has not been implemented!!!");
    }
    else{
      _nldPorous->constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,
                             ipvcur->getRefToTangentModuli(), ipvcur->getRefToDLocalVariableDStrain(),
                             ipvcur->getRefToDStressDNonLocalVariable(), ipvcur->getRefToDLocalVariableDNonLocalVariable(),
                             stiff,ipvcur->getRefTodPdField()[0],ipvcur->getRefTodPdGradField()[0],
                             ipvcur->getRefTodLocalVariableDExtraDofDiffusionField(),
                             ipvprev->getConstRefToField(0),ipvcur->getConstRefToField(0),// T0 and T
                             ipvprev->getConstRefToGradField()[0],ipvcur->getConstRefToGradField()[0],// gradT0 and gradT
                             ipvcur->getRefToFlux()[0],ipvcur->getRefTodFluxdF()[0],
                             ipvcur->getRefTodFluxdNonLocalVariable()[0], ipvcur->getRefTodFluxdField()[0][0],
                             ipvcur->getRefTodFluxdGradField()[0][0],// flux and derivatives
                             ipvcur->getRefToFieldSource()(0),
                             ipvcur->getRefTodFieldSourcedF()[0],ipvcur->getRefTodFieldSourcedNonLocalVariable(),
                             ipvcur->getRefTodFieldSourcedField()(0,0),ipvcur->getRefTodFieldSourcedGradField()[0][0],//thermal source
                             ipvcur->getRefToMechanicalSource()(0), // mechanical source
                             ipvcur->getRefTodMechanicalSourcedF()[0],
                             ipvcur->getRefTodMechanicalSourcedNonLocalVariable(),
                              ipvcur->getRefTodMechanicalSourcedField()(0,0),ipvcur->getRefTodMechanicalSourcedGradField()[0][0]
                             );
    }
    ipvcur->setRefToLinearConductivity(this->linearK);
    ipvcur->setRefToStiff_alphaDilatation(this->Stiff_alphaDilatation);
  };

  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);
};


NonLocalDamageGursonDG3DMaterialLaw::NonLocalDamageGursonDG3DMaterialLaw(const int num,const double rho,
                    const double E,const double nu,
                    const double q1, const double q2, const double q3, const double fVinitial,
                    const J2IsotropicHardening &_j2IH,
                    const double tol,
                    const bool matrixByPert, const double pert) :
      NonLocalPorousDuctileDamageDG3DMaterialLaw(num,rho,true)
{
  ZeroCLengthLaw zeroL(num);
  _nldPorous = new mlawNonLocalDamageGurson(num,E,nu,rho,q1, q2, q3,
                                              fVinitial,_j2IH,zeroL,tol,matrixByPert,pert);
  _nldPorous->setNonLocalMethod(0); // local model
  fillElasticStiffness(E, nu, elasticStiffness);
}



NonLocalDamageGursonDG3DMaterialLaw::NonLocalDamageGursonDG3DMaterialLaw(const int num,const double rho,
                    const double E,const double nu,
                    const double q1, const double q2, const double q3, const double fVinitial,
                    const J2IsotropicHardening &_j2IH,
                    const CLengthLaw &_cLLaw, const double tol,
                    const bool matrixByPert, const double pert) :
   NonLocalPorousDuctileDamageDG3DMaterialLaw(num,rho,true)
{
  _nldPorous = new mlawNonLocalDamageGurson(num,E,nu,rho,q1, q2, q3,
                                              fVinitial,_j2IH,_cLLaw,tol,matrixByPert,pert);
  fillElasticStiffness(E, nu, elasticStiffness);
}



NonLocalDamageGursonDG3DMaterialLaw::NonLocalDamageGursonDG3DMaterialLaw(const int num,const double rho,
                   const double E,const double nu,
                   const double q1, const double q2, const double q3, const double fVinitial,
                   const J2IsotropicHardening &_j2IH,
                   const CLengthLaw &_cLLaw, double TaylorQuineyFactor,
                   const double KK,const double cp,const double alp,
                   const double tol, const bool matrixByPert,const double pert) :
   NonLocalPorousDuctileDamageDG3DMaterialLaw(num,rho,true)
{

  _nldPorous = new mlawNonLocalDamageGurson(num,E,nu,rho,q1, q2, q3,
                                              fVinitial,_j2IH,_cLLaw,tol,matrixByPert,pert);
  _nldPorous->setThermomechanicsCoupling(true);
  _nldPorous->setTaylorQuineyFactor(TaylorQuineyFactor);
  _nldPorous->setReferenceThermalConductivity(KK);
  _nldPorous->setReferenceCp(cp);
  _nldPorous->setThermalExpansionCoefficient(alp);
  //
  Stiff_alphaDilatation=_nldPorous->getStiff_alphaDilatation();
  linearK  = _nldPorous->getInitialConductivityTensor();
  fillElasticStiffness(E, nu, elasticStiffness);
}


NonLocalDamageGursonDG3DMaterialLaw::NonLocalDamageGursonDG3DMaterialLaw(const NonLocalDamageGursonDG3DMaterialLaw &source) :
  NonLocalPorousDuctileDamageDG3DMaterialLaw(source)
{

}

NonLocalDamageGursonDG3DMaterialLaw::~NonLocalDamageGursonDG3DMaterialLaw(){}


void NonLocalDamageGursonDG3DMaterialLaw::setTemperatureFunction_q1(const scalarFunction& Tfunc)
{
  static_cast<mlawNonLocalDamageGurson*>(_nldPorous)->setTemperatureFunction_q1(Tfunc);
};

void NonLocalDamageGursonDG3DMaterialLaw::setTemperatureFunction_q2(const scalarFunction& Tfunc)
{
  static_cast<mlawNonLocalDamageGurson*>(_nldPorous)->setTemperatureFunction_q2(Tfunc);
};

void NonLocalDamageGursonDG3DMaterialLaw::setTemperatureFunction_q3(const scalarFunction& Tfunc)
{
  static_cast<mlawNonLocalDamageGurson*>(_nldPorous)->setTemperatureFunction_q3(Tfunc);
};


NonLocalDamageGursonHill48DG3DMaterialLaw::NonLocalDamageGursonHill48DG3DMaterialLaw(const int num,const double rho,
                    const double E,const double nu,
                    const double q1, const double q2, const double q3, const double fVinitial,
                    const J2IsotropicHardening &_j2IH,
                    const double tol,
                    const bool matrixByPert, const double pert) :
      NonLocalPorousDuctileDamageDG3DMaterialLaw(num,rho,true)
{
  ZeroCLengthLaw zeroL(num);
  _nldPorous = new mlawNonLocalDamageGursonHill48(num,E,nu,rho,q1, q2, q3,
                                              fVinitial,_j2IH,zeroL,tol,matrixByPert,pert);
  _nldPorous->setNonLocalMethod(0); // local model
  fillElasticStiffness(E, nu, elasticStiffness);
  _useI1J2J3Implementation = true;
}



NonLocalDamageGursonHill48DG3DMaterialLaw::NonLocalDamageGursonHill48DG3DMaterialLaw(const int num,const double rho,
                    const double E,const double nu,
                    const double q1, const double q2, const double q3, const double fVinitial,
                    const J2IsotropicHardening &_j2IH,
                    const CLengthLaw &_cLLaw, const double tol,
                    const bool matrixByPert, const double pert) :
   NonLocalPorousDuctileDamageDG3DMaterialLaw(num,rho,true)
{
  _nldPorous = new mlawNonLocalDamageGursonHill48(num,E,nu,rho,q1, q2, q3,
                                              fVinitial,_j2IH,_cLLaw,tol,matrixByPert,pert);
  fillElasticStiffness(E, nu, elasticStiffness);
  _nldPorous->setNonLocalMethod(2);
  _useI1J2J3Implementation = true;
}


NonLocalDamageGursonHill48DG3DMaterialLaw::NonLocalDamageGursonHill48DG3DMaterialLaw(const int num,const double rho,
                   const double E,const double nu,
                   const double q1, const double q2, const double q3, const double fVinitial,
                   const J2IsotropicHardening &_j2IH,
                   const CLengthLaw &_cLLaw, double TaylorQuineyFactor,
                   const double KK,const double cp,const double alp,
                   const double tol, const bool matrixByPert,const double pert) :
   NonLocalPorousDuctileDamageDG3DMaterialLaw(num,rho,true)
{

  _nldPorous = new mlawNonLocalDamageGursonHill48(num,E,nu,rho,q1, q2, q3,
                                              fVinitial,_j2IH,_cLLaw,tol,matrixByPert,pert);
  _nldPorous->setThermomechanicsCoupling(true);
  _nldPorous->setTaylorQuineyFactor(TaylorQuineyFactor);
  _nldPorous->setReferenceThermalConductivity(KK);
  _nldPorous->setReferenceCp(cp);
  _nldPorous->setThermalExpansionCoefficient(alp);
  //
  Stiff_alphaDilatation=_nldPorous->getStiff_alphaDilatation();
  linearK  = _nldPorous->getInitialConductivityTensor();
  fillElasticStiffness(E, nu, elasticStiffness);
  _nldPorous->setNonLocalMethod(2);
  _useI1J2J3Implementation = true;
}


NonLocalDamageGursonHill48DG3DMaterialLaw::NonLocalDamageGursonHill48DG3DMaterialLaw(const NonLocalDamageGursonHill48DG3DMaterialLaw &source) :
  NonLocalPorousDuctileDamageDG3DMaterialLaw(source)
{

}

NonLocalDamageGursonHill48DG3DMaterialLaw::~NonLocalDamageGursonHill48DG3DMaterialLaw(){}

void NonLocalDamageGursonHill48DG3DMaterialLaw::setYieldParameters(const std::vector<double>& params)
{
  static_cast<mlawNonLocalDamageGursonHill48*>(_nldPorous)->setYieldParameters(params);
}
void NonLocalDamageGursonHill48DG3DMaterialLaw::setYieldParameters(const fullVector<double>& params)
{
  std::vector<double> params_vec(params.size());
  for (int i=0; i< params.size(); i++)
  {
    params_vec[i] = params(i);
  }
  static_cast<mlawNonLocalDamageGursonHill48*>(_nldPorous)->setYieldParameters(params_vec);
};

void NonLocalDamageGursonHill48DG3DMaterialLaw::setTemperatureFunction_q1(const scalarFunction& Tfunc)
{
  static_cast<mlawNonLocalDamageGursonHill48*>(_nldPorous)->setTemperatureFunction_q1(Tfunc);
};

void NonLocalDamageGursonHill48DG3DMaterialLaw::setTemperatureFunction_q2(const scalarFunction& Tfunc)
{
  static_cast<mlawNonLocalDamageGursonHill48*>(_nldPorous)->setTemperatureFunction_q2(Tfunc);
};

void NonLocalDamageGursonHill48DG3DMaterialLaw::setTemperatureFunction_q3(const scalarFunction& Tfunc)
{
  static_cast<mlawNonLocalDamageGursonHill48*>(_nldPorous)->setTemperatureFunction_q3(Tfunc);
};


NonLocalPorousThomasonDG3DMaterialLaw::NonLocalPorousThomasonDG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH,
                      const double tol, const bool matrixByPert, const double pert):
                                                              NonLocalPorousDuctileDamageDG3DMaterialLaw(num,rho,true)
{
  ZeroCLengthLaw zeroL(num);
  _nldPorous = new mlawNonLocalPorousThomasonLaw(num,E,nu,rho,fVinitial,lambda0,j2IH,zeroL,tol,matrixByPert,pert);
  _nldPorous->setNonLocalMethod(0); // local model
  fillElasticStiffness(E, nu, elasticStiffness);
}


NonLocalPorousThomasonDG3DMaterialLaw::NonLocalPorousThomasonDG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                      const double tol, const bool matrixByPert, const double pert):
                                                              NonLocalPorousDuctileDamageDG3DMaterialLaw(num,rho,true)
{
  _nldPorous = new mlawNonLocalPorousThomasonLaw(num,E,nu,rho,fVinitial,lambda0,j2IH,cLLaw,tol,matrixByPert,pert);
  fillElasticStiffness(E, nu, elasticStiffness);
}

NonLocalPorousThomasonDG3DMaterialLaw::NonLocalPorousThomasonDG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                      double TaylorQuineyFactor, const double KK,const double cp,const double alp,
                      const double tol, const bool matrixByPert, const double pert):
                                                              NonLocalPorousDuctileDamageDG3DMaterialLaw(num,rho,true)
{
  _nldPorous = new mlawNonLocalPorousThomasonLaw(num,E,nu,rho,fVinitial,lambda0,j2IH,cLLaw,tol,matrixByPert,pert);
  _nldPorous->setThermomechanicsCoupling(true);
  _nldPorous->setTaylorQuineyFactor(TaylorQuineyFactor);
  _nldPorous->setReferenceThermalConductivity(KK);
  _nldPorous->setReferenceCp(cp);
  _nldPorous->setThermalExpansionCoefficient(alp);
  //
  Stiff_alphaDilatation=_nldPorous->getStiff_alphaDilatation();
  linearK  = _nldPorous->getInitialConductivityTensor();
  fillElasticStiffness(E, nu, elasticStiffness);
}



NonLocalPorousThomasonDG3DMaterialLaw::NonLocalPorousThomasonDG3DMaterialLaw(const NonLocalPorousThomasonDG3DMaterialLaw &source) :
                                                             NonLocalPorousDuctileDamageDG3DMaterialLaw(source)
{

}

NonLocalPorousThomasonDG3DMaterialLaw::~NonLocalPorousThomasonDG3DMaterialLaw(){

}
void NonLocalPorousThomasonDG3DMaterialLaw::setYieldSurfaceExponent(const double newN)
{
  static_cast<mlawNonLocalPorousThomasonLaw*>(_nldPorous)->setYieldSurfaceExponent(newN);
};

NonLocalPorousThomasonWithMPSDG3DMaterialLaw::NonLocalPorousThomasonWithMPSDG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH,
                      const double tol, const bool matrixByPert, const double pert):
                                                              NonLocalPorousDuctileDamageDG3DMaterialLaw(num,rho,true)
{
  ZeroCLengthLaw zeroL(num);
  _nldPorous = new mlawNonLocalPorousThomasonLawWithMPS(num,E,nu,rho,fVinitial,lambda0,j2IH,zeroL,tol,matrixByPert,pert);
  _nldPorous->setNonLocalMethod(0); // local model
  fillElasticStiffness(E, nu, elasticStiffness);
  _useI1J2J3Implementation = true;
}


NonLocalPorousThomasonWithMPSDG3DMaterialLaw::NonLocalPorousThomasonWithMPSDG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                      const double tol, const bool matrixByPert, const double pert):
                                                              NonLocalPorousDuctileDamageDG3DMaterialLaw(num,rho,true)
{
  _nldPorous = new mlawNonLocalPorousThomasonLawWithMPS(num,E,nu,rho,fVinitial,lambda0,j2IH,cLLaw,tol,matrixByPert,pert);
  fillElasticStiffness(E, nu, elasticStiffness);
  _nldPorous->setNonLocalMethod(2);
  _useI1J2J3Implementation = true;
}

NonLocalPorousThomasonWithMPSDG3DMaterialLaw::NonLocalPorousThomasonWithMPSDG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                      double TaylorQuineyFactor, const double KK,const double cp,const double alp,
                      const double tol, const bool matrixByPert, const double pert):
                                                              NonLocalPorousDuctileDamageDG3DMaterialLaw(num,rho,true)
{
  _nldPorous = new mlawNonLocalPorousThomasonLawWithMPS(num,E,nu,rho,fVinitial,lambda0,j2IH,cLLaw,tol,matrixByPert,pert);
  _nldPorous->setThermomechanicsCoupling(true);
  _nldPorous->setTaylorQuineyFactor(TaylorQuineyFactor);
  _nldPorous->setReferenceThermalConductivity(KK);
  _nldPorous->setReferenceCp(cp);
  _nldPorous->setThermalExpansionCoefficient(alp);
  //
  Stiff_alphaDilatation=_nldPorous->getStiff_alphaDilatation();
  linearK  = _nldPorous->getInitialConductivityTensor();
  fillElasticStiffness(E, nu, elasticStiffness);
  _nldPorous->setNonLocalMethod(2);
  _useI1J2J3Implementation = true;
}



NonLocalPorousThomasonWithMPSDG3DMaterialLaw::NonLocalPorousThomasonWithMPSDG3DMaterialLaw(const NonLocalPorousThomasonWithMPSDG3DMaterialLaw &source) :
                                                             NonLocalPorousDuctileDamageDG3DMaterialLaw(source)
{

}

NonLocalPorousThomasonWithMPSDG3DMaterialLaw::~NonLocalPorousThomasonWithMPSDG3DMaterialLaw(){

}
void NonLocalPorousThomasonWithMPSDG3DMaterialLaw::setYieldSurfaceExponent(const double newN)
{
  static_cast<mlawNonLocalPorousThomasonLaw*>(_nldPorous)->setYieldSurfaceExponent(newN);
};

//
NonLocalPorousThomasonWithMSSDG3DMaterialLaw::NonLocalPorousThomasonWithMSSDG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH,
                      const double tol, const bool matrixByPert, const double pert):
                                                              NonLocalPorousDuctileDamageDG3DMaterialLaw(num,rho,true)
{
  ZeroCLengthLaw zeroL(num);
  _nldPorous = new mlawNonLocalPorousThomasonLawWithMSS(num,E,nu,rho,fVinitial,lambda0,j2IH,zeroL,tol,matrixByPert,pert);
  _nldPorous->setNonLocalMethod(0);
  fillElasticStiffness(E, nu, elasticStiffness);
  _useI1J2J3Implementation = true;
}


NonLocalPorousThomasonWithMSSDG3DMaterialLaw::NonLocalPorousThomasonWithMSSDG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                      const double tol, const bool matrixByPert, const double pert):
                                                              NonLocalPorousDuctileDamageDG3DMaterialLaw(num,rho,true)
{
  _nldPorous = new mlawNonLocalPorousThomasonLawWithMSS(num,E,nu,rho,fVinitial,lambda0,j2IH,cLLaw,tol,matrixByPert,pert);
  fillElasticStiffness(E, nu, elasticStiffness);
  _nldPorous->setNonLocalMethod(2);
  _useI1J2J3Implementation = true;
}

NonLocalPorousThomasonWithMSSDG3DMaterialLaw::NonLocalPorousThomasonWithMSSDG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                      double TaylorQuineyFactor, const double KK,const double cp,const double alp,
                      const double tol, const bool matrixByPert, const double pert):
                                                              NonLocalPorousDuctileDamageDG3DMaterialLaw(num,rho,true)
{
  _nldPorous = new mlawNonLocalPorousThomasonLawWithMSS(num,E,nu,rho,fVinitial,lambda0,j2IH,cLLaw,tol,matrixByPert,pert);
  _nldPorous->setThermomechanicsCoupling(true);
  _nldPorous->setTaylorQuineyFactor(TaylorQuineyFactor);
  _nldPorous->setReferenceThermalConductivity(KK);
  _nldPorous->setReferenceCp(cp);
  _nldPorous->setThermalExpansionCoefficient(alp);
  //
  Stiff_alphaDilatation=_nldPorous->getStiff_alphaDilatation();
  linearK  = _nldPorous->getInitialConductivityTensor();
  fillElasticStiffness(E, nu, elasticStiffness);
  _nldPorous->setNonLocalMethod(2);
  _useI1J2J3Implementation = true;
}



NonLocalPorousThomasonWithMSSDG3DMaterialLaw::NonLocalPorousThomasonWithMSSDG3DMaterialLaw(const NonLocalPorousThomasonWithMSSDG3DMaterialLaw &source) :
                                                             NonLocalPorousDuctileDamageDG3DMaterialLaw(source)
{

}

NonLocalPorousThomasonWithMSSDG3DMaterialLaw::~NonLocalPorousThomasonWithMSSDG3DMaterialLaw(){

}

//

NonLocalPorousCoupledDG3DMaterialLaw::NonLocalPorousCoupledDG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double q1, const double q2, const double q3,
                      const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH,
                      const double tol, const bool matrixbyPerturbation, const double pert):
                                NonLocalPorousDuctileDamageDG3DMaterialLaw(num,rho,true)
{
  ZeroCLengthLaw zeroL(num);
  _nldPorous = new mlawNonLocalPorousCoupledLaw(num, E, nu, rho, q1, q2, q3, fVinitial, lambda0,
                                                    j2IH, zeroL, tol, matrixbyPerturbation, pert);
  _nldPorous->setNonLocalMethod(0); // local model
  fillElasticStiffness(E, nu, elasticStiffness);
}

//
NonLocalPorousCoupledDG3DMaterialLaw::NonLocalPorousCoupledDG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double q1, const double q2, const double q3,
                      const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                      const double tol, const bool matrixbyPerturbation, const double pert):
                                NonLocalPorousDuctileDamageDG3DMaterialLaw(num,rho,true)
{
  _nldPorous = new mlawNonLocalPorousCoupledLaw(num, E, nu, rho, q1, q2, q3, fVinitial, lambda0,
                                                    j2IH, cLLaw, tol, matrixbyPerturbation, pert);
  fillElasticStiffness(E, nu, elasticStiffness);
}

//
NonLocalPorousCoupledDG3DMaterialLaw::NonLocalPorousCoupledDG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double q1, const double q2, const double q3,
                      const double fVinitial, const double lambda0, const double kappa, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                      const double tol, const bool matrixbyPerturbation, const double pert):
                                NonLocalPorousDuctileDamageDG3DMaterialLaw(num,rho,true)
{
  _nldPorous = new mlawNonLocalPorousCoupledLaw(num, E, nu, rho, q1, q2, q3, fVinitial, lambda0, kappa,
                                                    j2IH, cLLaw, tol, matrixbyPerturbation, pert);
  fillElasticStiffness(E, nu, elasticStiffness);
}

NonLocalPorousCoupledDG3DMaterialLaw::NonLocalPorousCoupledDG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double q1, const double q2, const double q3,
                      const double fVinitial, const double lambda0, const double kappa, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                      double TaylorQuineyFactor, const double KK,const double cp,const double alp,
                      const double tol, const bool matrixbyPerturbation, const double pert):
                                NonLocalPorousDuctileDamageDG3DMaterialLaw(num,rho,true)
{
  _nldPorous = new mlawNonLocalPorousCoupledLaw(num, E, nu, rho, q1, q2, q3, fVinitial, lambda0, kappa,
                                                    j2IH, cLLaw, tol, matrixbyPerturbation, pert);
  _nldPorous->setThermomechanicsCoupling(true);
  _nldPorous->setTaylorQuineyFactor(TaylorQuineyFactor);
  _nldPorous->setReferenceThermalConductivity(KK);
  _nldPorous->setReferenceCp(cp);
  _nldPorous->setThermalExpansionCoefficient(alp);
  //
  Stiff_alphaDilatation=_nldPorous->getStiff_alphaDilatation();
  linearK  = _nldPorous->getInitialConductivityTensor();
  fillElasticStiffness(E, nu, elasticStiffness);
}

NonLocalPorousCoupledDG3DMaterialLaw::NonLocalPorousCoupledDG3DMaterialLaw(const NonLocalPorousCoupledDG3DMaterialLaw &source) :
                                                             NonLocalPorousDuctileDamageDG3DMaterialLaw(source)
{

}

NonLocalPorousCoupledDG3DMaterialLaw::~NonLocalPorousCoupledDG3DMaterialLaw(){

}

void NonLocalPorousCoupledDG3DMaterialLaw::setYieldSurfaceExponent(const double newN){
  static_cast<mlawNonLocalPorousCoupledLaw*>(_nldPorous)->setYieldSurfaceExponent(newN);
};

void NonLocalPorousCoupledDG3DMaterialLaw::setYieldOffset(const bool fl){
  static_cast<mlawNonLocalPorousCoupledLaw*>(_nldPorous)->setYieldOffset(fl);
};

void NonLocalPorousCoupledDG3DMaterialLaw::setBulkCouplingBehaviour(const bool fl)
{
  static_cast<mlawNonLocalPorousCoupledLaw*>(_nldPorous)->setBulkCouplingBehaviour(fl);
}

void NonLocalPorousCoupledDG3DMaterialLaw::setCftOffset(const bool fl){
  static_cast<mlawNonLocalPorousCoupledLaw*>(_nldPorous)->setCftOffset(fl);
};

void NonLocalPorousCoupledDG3DMaterialLaw::setLodeOffset(const bool fl){
  static_cast<mlawNonLocalPorousCoupledLaw*>(_nldPorous)->setLodeOffset(fl);
}
void NonLocalPorousCoupledDG3DMaterialLaw::useTwoYieldRegularization(const bool fl, const double n)
{
  static_cast<mlawNonLocalPorousCoupledLaw*>(_nldPorous)->useTwoYieldRegularization(fl,n);
}

void NonLocalPorousCoupledDG3DMaterialLaw::setCrackTransition(const bool fl, const double beta){
  static_cast<mlawNonLocalPorousCoupledLaw*>(_nldPorous)->setCrackTransition(fl,beta);
}

void NonLocalPorousCoupledDG3DMaterialLaw::setCoalescenceTolerance(const double t){
	static_cast<mlawNonLocalPorousCoupledLaw*>(_nldPorous)->setCoalescenceTolerance(t);
};

void NonLocalPorousCoupledDG3DMaterialLaw::setGrowthVoidEvolutionLaw(const voidStateEvolutionLaw& voidtateLaw)
{
  static_cast<mlawNonLocalPorousCoupledLaw*>(_nldPorous)->setGrowthVoidEvolutionLaw(voidtateLaw);
};
void NonLocalPorousCoupledDG3DMaterialLaw::setCoalescenceVoidEvolutionLaw(const voidStateEvolutionLaw& voidtateLaw)
{
  static_cast<mlawNonLocalPorousCoupledLaw*>(_nldPorous)->setCoalescenceVoidEvolutionLaw(voidtateLaw);
};


NonLocalPorousCoupledWithMPSDG3DMaterialLaw::NonLocalPorousCoupledWithMPSDG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double q1, const double q2, const double q3,
                      const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH,
                      const double tol, const bool matrixbyPerturbation, const double pert):
                                NonLocalPorousCoupledDG3DMaterialLaw(num,E,nu,rho,q1,q2,q3,fVinitial,lambda0,j2IH,tol,matrixbyPerturbation,pert)
{
  
  if (_nldPorous != NULL) delete _nldPorous;
  ZeroCLengthLaw zeroL(num);
  _nldPorous = new mlawNonLocalPorousCoupledLawWithMPS(num, E, nu, rho, q1, q2, q3, fVinitial, lambda0,
                                                    j2IH, zeroL, tol, matrixbyPerturbation, pert);
  _nldPorous->setNonLocalMethod(0);
  fillElasticStiffness(E, nu, elasticStiffness);
  _useI1J2J3Implementation = true;
}

//
NonLocalPorousCoupledWithMPSDG3DMaterialLaw::NonLocalPorousCoupledWithMPSDG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double q1, const double q2, const double q3,
                      const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                      const double tol, const bool matrixbyPerturbation, const double pert):
                                NonLocalPorousCoupledDG3DMaterialLaw(num,E,nu,rho,q1,q2,q3,fVinitial,lambda0,j2IH,cLLaw,tol,matrixbyPerturbation,pert)
{
  if (_nldPorous != NULL) delete _nldPorous;
  _nldPorous = new mlawNonLocalPorousCoupledLawWithMPS(num, E, nu, rho, q1, q2, q3, fVinitial, lambda0,
                                                    j2IH, cLLaw, tol, matrixbyPerturbation, pert);
  _nldPorous->setNonLocalMethod(2);
  fillElasticStiffness(E, nu, elasticStiffness);
  _useI1J2J3Implementation = true;
}

//
NonLocalPorousCoupledWithMPSDG3DMaterialLaw::NonLocalPorousCoupledWithMPSDG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double q1, const double q2, const double q3,
                      const double fVinitial, const double lambda0, const double kappa, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                      const double tol, const bool matrixbyPerturbation, const double pert):
                                NonLocalPorousCoupledDG3DMaterialLaw(num,E,nu,rho,q1,q2,q3,fVinitial,lambda0,kappa,j2IH,cLLaw,tol,matrixbyPerturbation,pert)
{
  if (_nldPorous != NULL) delete _nldPorous;
  _nldPorous = new mlawNonLocalPorousCoupledLawWithMPS(num, E, nu, rho, q1, q2, q3, fVinitial, lambda0, kappa,
                                                    j2IH, cLLaw, tol, matrixbyPerturbation, pert);
  _nldPorous->setNonLocalMethod(2);
  _useI1J2J3Implementation = true;
  fillElasticStiffness(E, nu, elasticStiffness);
}

NonLocalPorousCoupledWithMPSDG3DMaterialLaw::NonLocalPorousCoupledWithMPSDG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double q1, const double q2, const double q3,
                      const double fVinitial, const double lambda0, const double kappa, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                      double TaylorQuineyFactor, const double KK,const double cp,const double alp,
                      const double tol, const bool matrixbyPerturbation, const double pert):
                                NonLocalPorousCoupledDG3DMaterialLaw(num,E,nu,rho,q1,q2,q3,fVinitial,lambda0,kappa,j2IH,cLLaw,TaylorQuineyFactor,KK,cp,alp,tol,matrixbyPerturbation,pert)
{
  if (_nldPorous != NULL) delete _nldPorous;
  _nldPorous = new mlawNonLocalPorousCoupledLawWithMPS(num, E, nu, rho, q1, q2, q3, fVinitial, lambda0, kappa,
                                                    j2IH, cLLaw, tol, matrixbyPerturbation, pert);
  _useI1J2J3Implementation = true;
  _nldPorous->setNonLocalMethod(2);
  _nldPorous->setThermomechanicsCoupling(true);
  _nldPorous->setTaylorQuineyFactor(TaylorQuineyFactor);
  _nldPorous->setReferenceThermalConductivity(KK);
  _nldPorous->setReferenceCp(cp);
  _nldPorous->setThermalExpansionCoefficient(alp);
  //
  Stiff_alphaDilatation=_nldPorous->getStiff_alphaDilatation();
  linearK  = _nldPorous->getInitialConductivityTensor();
  fillElasticStiffness(E, nu, elasticStiffness);
}

NonLocalPorousCoupledWithMPSDG3DMaterialLaw::NonLocalPorousCoupledWithMPSDG3DMaterialLaw(const NonLocalPorousCoupledWithMPSDG3DMaterialLaw &source) :
                                                             NonLocalPorousCoupledDG3DMaterialLaw(source)
{

}

NonLocalPorousCoupledWithMPSDG3DMaterialLaw::~NonLocalPorousCoupledWithMPSDG3DMaterialLaw(){

};


NonLocalPorousCoupledWithMPSHill48DG3DMaterialLaw::NonLocalPorousCoupledWithMPSHill48DG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double q1, const double q2, const double q3,
                      const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH,
                      const double tol, const bool matrixbyPerturbation, const double pert):
                                NonLocalPorousCoupledDG3DMaterialLaw(num,E,nu,rho,q1,q2,q3,fVinitial,lambda0,j2IH,tol,matrixbyPerturbation,pert)
{
  
  if (_nldPorous != NULL) delete _nldPorous;
  ZeroCLengthLaw zeroL(num);
  _nldPorous = new mlawNonLocalPorousCoupledHill48LawWithMPS(num, E, nu, rho, q1, q2, q3, fVinitial, lambda0,
                                                    j2IH, zeroL, tol, matrixbyPerturbation, pert);
  _nldPorous->setNonLocalMethod(0);
  fillElasticStiffness(E, nu, elasticStiffness);
  _useI1J2J3Implementation = true;
}

//
NonLocalPorousCoupledWithMPSHill48DG3DMaterialLaw::NonLocalPorousCoupledWithMPSHill48DG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double q1, const double q2, const double q3,
                      const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                      const double tol, const bool matrixbyPerturbation, const double pert):
                                NonLocalPorousCoupledDG3DMaterialLaw(num,E,nu,rho,q1,q2,q3,fVinitial,lambda0,j2IH,cLLaw,tol,matrixbyPerturbation,pert)
{
  if (_nldPorous != NULL) delete _nldPorous;
  _nldPorous = new mlawNonLocalPorousCoupledHill48LawWithMPS(num, E, nu, rho, q1, q2, q3, fVinitial, lambda0,
                                                    j2IH, cLLaw, tol, matrixbyPerturbation, pert);
  _nldPorous->setNonLocalMethod(2);
  fillElasticStiffness(E, nu, elasticStiffness);
  _useI1J2J3Implementation = true;
}

//
NonLocalPorousCoupledWithMPSHill48DG3DMaterialLaw::NonLocalPorousCoupledWithMPSHill48DG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double q1, const double q2, const double q3,
                      const double fVinitial, const double lambda0, const double kappa, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                      const double tol, const bool matrixbyPerturbation, const double pert):
                                NonLocalPorousCoupledDG3DMaterialLaw(num,E,nu,rho,q1,q2,q3,fVinitial,lambda0,kappa,j2IH,cLLaw,tol,matrixbyPerturbation,pert)
{
  if (_nldPorous != NULL) delete _nldPorous;
  _nldPorous = new mlawNonLocalPorousCoupledHill48LawWithMPS(num, E, nu, rho, q1, q2, q3, fVinitial, lambda0, kappa,
                                                    j2IH, cLLaw, tol, matrixbyPerturbation, pert);
  _nldPorous->setNonLocalMethod(2);
  _useI1J2J3Implementation = true;
  fillElasticStiffness(E, nu, elasticStiffness);
}

NonLocalPorousCoupledWithMPSHill48DG3DMaterialLaw::NonLocalPorousCoupledWithMPSHill48DG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double q1, const double q2, const double q3,
                      const double fVinitial, const double lambda0, const double kappa, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                      double TaylorQuineyFactor, const double KK,const double cp,const double alp,
                      const double tol, const bool matrixbyPerturbation, const double pert):
                                NonLocalPorousCoupledDG3DMaterialLaw(num,E,nu,rho,q1,q2,q3,fVinitial,lambda0,kappa,j2IH,cLLaw,TaylorQuineyFactor,KK,cp,alp,tol,matrixbyPerturbation,pert)
{
  if (_nldPorous != NULL) delete _nldPorous;
  _nldPorous = new mlawNonLocalPorousCoupledHill48LawWithMPS(num, E, nu, rho, q1, q2, q3, fVinitial, lambda0, kappa,
                                                    j2IH, cLLaw, tol, matrixbyPerturbation, pert);
  _useI1J2J3Implementation = true;
  _nldPorous->setNonLocalMethod(2);
  _nldPorous->setThermomechanicsCoupling(true);
  _nldPorous->setTaylorQuineyFactor(TaylorQuineyFactor);
  _nldPorous->setReferenceThermalConductivity(KK);
  _nldPorous->setReferenceCp(cp);
  _nldPorous->setThermalExpansionCoefficient(alp);
  //
  Stiff_alphaDilatation=_nldPorous->getStiff_alphaDilatation();
  linearK  = _nldPorous->getInitialConductivityTensor();
  fillElasticStiffness(E, nu, elasticStiffness);
}

NonLocalPorousCoupledWithMPSHill48DG3DMaterialLaw::NonLocalPorousCoupledWithMPSHill48DG3DMaterialLaw(const NonLocalPorousCoupledWithMPSHill48DG3DMaterialLaw &source) :
                                                             NonLocalPorousCoupledDG3DMaterialLaw(source)
{

}

NonLocalPorousCoupledWithMPSHill48DG3DMaterialLaw::~NonLocalPorousCoupledWithMPSHill48DG3DMaterialLaw(){

};

void NonLocalPorousCoupledWithMPSHill48DG3DMaterialLaw::setYieldParameters(const std::vector<double>& params)
{
  static_cast<mlawNonLocalPorousCoupledHill48LawWithMPS*>(_nldPorous)->setYieldParameters(params);
};

void NonLocalPorousCoupledWithMPSHill48DG3DMaterialLaw::setYieldParameters(const fullVector<double>& params)
{
  std::vector<double> params_vec(params.size());
  for (int i=0; i< params.size(); i++)
  {
    params_vec[i] = params(i);
  }
  static_cast<mlawNonLocalPorousCoupledHill48LawWithMPS*>(_nldPorous)->setYieldParameters(params_vec);
};

//
NonLocalPorousCoupledWithMPSAndMSSDG3DMaterialLaw::NonLocalPorousCoupledWithMPSAndMSSDG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double q1, const double q2, const double q3,
                      const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH,
                      const double tol, const bool matrixbyPerturbation, const double pert):
                                NonLocalPorousCoupledDG3DMaterialLaw(num,E,nu,rho,q1,q2,q3,fVinitial,lambda0,j2IH,tol,matrixbyPerturbation,pert)
{
  
  if (_nldPorous != NULL) delete _nldPorous;
  ZeroCLengthLaw zeroL(num);
  _nldPorous = new mlawNonLocalPorousCoupledLawWithMPSAndMSS(num, E, nu, rho, q1, q2, q3, fVinitial, lambda0,
                                                    j2IH, zeroL, tol, matrixbyPerturbation, pert);
  _nldPorous->setNonLocalMethod(0); // local model
  fillElasticStiffness(E, nu, elasticStiffness);
  _useI1J2J3Implementation = true;
}

//
NonLocalPorousCoupledWithMPSAndMSSDG3DMaterialLaw::NonLocalPorousCoupledWithMPSAndMSSDG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double q1, const double q2, const double q3,
                      const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                      const double tol, const bool matrixbyPerturbation, const double pert):
                                NonLocalPorousCoupledDG3DMaterialLaw(num,E,nu,rho,q1,q2,q3,fVinitial,lambda0,j2IH,cLLaw,tol,matrixbyPerturbation,pert)
{
  if (_nldPorous != NULL) delete _nldPorous;
  _nldPorous = new mlawNonLocalPorousCoupledLawWithMPSAndMSS(num, E, nu, rho, q1, q2, q3, fVinitial, lambda0,
                                                    j2IH, cLLaw, tol, matrixbyPerturbation, pert);
  fillElasticStiffness(E, nu, elasticStiffness);
  _useI1J2J3Implementation = true;
  _nldPorous->setNonLocalMethod(2);
}

//
NonLocalPorousCoupledWithMPSAndMSSDG3DMaterialLaw::NonLocalPorousCoupledWithMPSAndMSSDG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double q1, const double q2, const double q3,
                      const double fVinitial, const double lambda0, const double kappa, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                      const double tol, const bool matrixbyPerturbation, const double pert):
                                NonLocalPorousCoupledDG3DMaterialLaw(num,E,nu,rho,q1,q2,q3,fVinitial,lambda0,kappa,j2IH,cLLaw,tol,matrixbyPerturbation,pert)
{
  if (_nldPorous != NULL) delete _nldPorous;
  _nldPorous = new mlawNonLocalPorousCoupledLawWithMPSAndMSS(num, E, nu, rho, q1, q2, q3, fVinitial, lambda0, kappa,
                                                    j2IH, cLLaw, tol, matrixbyPerturbation, pert);
  _useI1J2J3Implementation = true;
  _nldPorous->setNonLocalMethod(2);
  fillElasticStiffness(E, nu, elasticStiffness);
}

NonLocalPorousCoupledWithMPSAndMSSDG3DMaterialLaw::NonLocalPorousCoupledWithMPSAndMSSDG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double q1, const double q2, const double q3,
                      const double fVinitial, const double lambda0, const double kappa, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                      double TaylorQuineyFactor, const double KK,const double cp,const double alp,
                      const double tol, const bool matrixbyPerturbation, const double pert):
                                NonLocalPorousCoupledDG3DMaterialLaw(num,E,nu,rho,q1,q2,q3,fVinitial,lambda0,kappa,j2IH,cLLaw,TaylorQuineyFactor,KK,cp,alp,tol,matrixbyPerturbation,pert)
{
  if (_nldPorous != NULL) delete _nldPorous;
  _nldPorous = new mlawNonLocalPorousCoupledLawWithMPSAndMSS(num, E, nu, rho, q1, q2, q3, fVinitial, lambda0, kappa,
                                                    j2IH, cLLaw, tol, matrixbyPerturbation, pert);
  _useI1J2J3Implementation = true;
  _nldPorous->setNonLocalMethod(2);
  _nldPorous->setThermomechanicsCoupling(true);
  _nldPorous->setTaylorQuineyFactor(TaylorQuineyFactor);
  _nldPorous->setReferenceThermalConductivity(KK);
  _nldPorous->setReferenceCp(cp);
  _nldPorous->setThermalExpansionCoefficient(alp);
  //
  Stiff_alphaDilatation=_nldPorous->getStiff_alphaDilatation();
  linearK  = _nldPorous->getInitialConductivityTensor();
  fillElasticStiffness(E, nu, elasticStiffness);
}

NonLocalPorousCoupledWithMPSAndMSSDG3DMaterialLaw::NonLocalPorousCoupledWithMPSAndMSSDG3DMaterialLaw(const NonLocalPorousCoupledWithMPSAndMSSDG3DMaterialLaw &source) :
                                                             NonLocalPorousCoupledDG3DMaterialLaw(source)
{

}

NonLocalPorousCoupledWithMPSAndMSSDG3DMaterialLaw::~NonLocalPorousCoupledWithMPSAndMSSDG3DMaterialLaw(){

}

void NonLocalPorousCoupledWithMPSAndMSSDG3DMaterialLaw::setThomasonSmoothFactor(const double fact)
{
  static_cast<mlawNonLocalPorousCoupledLawWithMPSAndMSS*>(_nldPorous)->setThomasonSmoothFactor(fact);
}
void NonLocalPorousCoupledWithMPSAndMSSDG3DMaterialLaw::setShearSmoothFactor(const double fact)
{
  static_cast<mlawNonLocalPorousCoupledLawWithMPSAndMSS*>(_nldPorous)->setShearSmoothFactor(fact);
}

void NonLocalPorousCoupledWithMPSAndMSSDG3DMaterialLaw::setShearFactor(double g)
{
  static_cast<mlawNonLocalPorousCoupledLawWithMPSAndMSS*>(_nldPorous)->setShearFactor(g);
}

void NonLocalPorousCoupledWithMPSAndMSSDG3DMaterialLaw::setVoidLigamentExponent(double e)
{
  static_cast<mlawNonLocalPorousCoupledLawWithMPSAndMSS*>(_nldPorous)->setVoidLigamentExponent(e);
}

void NonLocalPorousCoupledWithMPSAndMSSDG3DMaterialLaw::freezeCoalescenceState(const bool fl)
{
  static_cast<mlawNonLocalPorousCoupledLawWithMPSAndMSS*>(_nldPorous)->freezeCoalescenceState(fl);
}

void NonLocalPorousCoupledWithMPSAndMSSDG3DMaterialLaw::setNeckingCoalescenceLaw(const CoalescenceLaw& added_coalsLaw)
{
  static_cast<mlawNonLocalPorousCoupledLawWithMPSAndMSS*>(_nldPorous)->setNeckingCoalescenceLaw(added_coalsLaw);
};
void NonLocalPorousCoupledWithMPSAndMSSDG3DMaterialLaw::setShearCoalescenceLaw(const CoalescenceLaw& added_coalsLaw)
{
  static_cast<mlawNonLocalPorousCoupledLawWithMPSAndMSS*>(_nldPorous)->setShearCoalescenceLaw(added_coalsLaw);
};
void NonLocalPorousCoupledWithMPSAndMSSDG3DMaterialLaw::setShearVoidEvolutionLaw(const voidStateEvolutionLaw& voidtateLaw)
{
  static_cast<mlawNonLocalPorousCoupledLawWithMPSAndMSS*>(_nldPorous)->setShearVoidEvolutionLaw(voidtateLaw);
};

//
NonLocalPorousCoupledWithMPSAndMSSHill48DG3DMaterialLaw::NonLocalPorousCoupledWithMPSAndMSSHill48DG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double q1, const double q2, const double q3,
                      const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH,
                      const double tol, const bool matrixbyPerturbation, const double pert):
                                NonLocalPorousCoupledDG3DMaterialLaw(num,E,nu,rho,q1,q2,q3,fVinitial,lambda0,j2IH,tol,matrixbyPerturbation,pert)
{
  
  if (_nldPorous != NULL) delete _nldPorous;
  ZeroCLengthLaw zeroL(num);
  _nldPorous = new mlawNonLocalPorousCoupledHill48LawWithMPSAndMSS(num, E, nu, rho, q1, q2, q3, fVinitial, lambda0,
                                                    j2IH, zeroL, tol, matrixbyPerturbation, pert);
  _nldPorous->setNonLocalMethod(0); // local model
  fillElasticStiffness(E, nu, elasticStiffness);
  _useI1J2J3Implementation = true;
}

//
NonLocalPorousCoupledWithMPSAndMSSHill48DG3DMaterialLaw::NonLocalPorousCoupledWithMPSAndMSSHill48DG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double q1, const double q2, const double q3,
                      const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                      const double tol, const bool matrixbyPerturbation, const double pert):
                                NonLocalPorousCoupledDG3DMaterialLaw(num,E,nu,rho,q1,q2,q3,fVinitial,lambda0,j2IH,cLLaw,tol,matrixbyPerturbation,pert)
{
  if (_nldPorous != NULL) delete _nldPorous;
  _nldPorous = new mlawNonLocalPorousCoupledHill48LawWithMPSAndMSS(num, E, nu, rho, q1, q2, q3, fVinitial, lambda0,
                                                    j2IH, cLLaw, tol, matrixbyPerturbation, pert);
  fillElasticStiffness(E, nu, elasticStiffness);
  _useI1J2J3Implementation = true;
  _nldPorous->setNonLocalMethod(2);
}

//
NonLocalPorousCoupledWithMPSAndMSSHill48DG3DMaterialLaw::NonLocalPorousCoupledWithMPSAndMSSHill48DG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double q1, const double q2, const double q3,
                      const double fVinitial, const double lambda0, const double kappa, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                      const double tol, const bool matrixbyPerturbation, const double pert):
                                NonLocalPorousCoupledDG3DMaterialLaw(num,E,nu,rho,q1,q2,q3,fVinitial,lambda0,kappa,j2IH,cLLaw,tol,matrixbyPerturbation,pert)
{
  if (_nldPorous != NULL) delete _nldPorous;
  _nldPorous = new mlawNonLocalPorousCoupledHill48LawWithMPSAndMSS(num, E, nu, rho, q1, q2, q3, fVinitial, lambda0, kappa,
                                                    j2IH, cLLaw, tol, matrixbyPerturbation, pert);
  _useI1J2J3Implementation = true;
  _nldPorous->setNonLocalMethod(2);
  fillElasticStiffness(E, nu, elasticStiffness);
}

NonLocalPorousCoupledWithMPSAndMSSHill48DG3DMaterialLaw::NonLocalPorousCoupledWithMPSAndMSSHill48DG3DMaterialLaw(const int num,const double E,const double nu, const double rho,
                      const double q1, const double q2, const double q3,
                      const double fVinitial, const double lambda0, const double kappa, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                      double TaylorQuineyFactor, const double KK,const double cp,const double alp,
                      const double tol, const bool matrixbyPerturbation, const double pert):
                                NonLocalPorousCoupledDG3DMaterialLaw(num,E,nu,rho,q1,q2,q3,fVinitial,lambda0,kappa,j2IH,cLLaw,TaylorQuineyFactor,KK,cp,alp,tol,matrixbyPerturbation,pert)
{
  if (_nldPorous != NULL) delete _nldPorous;
  _nldPorous = new mlawNonLocalPorousCoupledHill48LawWithMPSAndMSS(num, E, nu, rho, q1, q2, q3, fVinitial, lambda0, kappa,
                                                    j2IH, cLLaw, tol, matrixbyPerturbation, pert);
  _useI1J2J3Implementation = true;
  _nldPorous->setNonLocalMethod(2);
  _nldPorous->setThermomechanicsCoupling(true);
  _nldPorous->setTaylorQuineyFactor(TaylorQuineyFactor);
  _nldPorous->setReferenceThermalConductivity(KK);
  _nldPorous->setReferenceCp(cp);
  _nldPorous->setThermalExpansionCoefficient(alp);
  //
  Stiff_alphaDilatation=_nldPorous->getStiff_alphaDilatation();
  linearK  = _nldPorous->getInitialConductivityTensor();
  fillElasticStiffness(E, nu, elasticStiffness);
}

NonLocalPorousCoupledWithMPSAndMSSHill48DG3DMaterialLaw::NonLocalPorousCoupledWithMPSAndMSSHill48DG3DMaterialLaw(const NonLocalPorousCoupledWithMPSAndMSSHill48DG3DMaterialLaw &source) :
                                                             NonLocalPorousCoupledDG3DMaterialLaw(source)
{

}

NonLocalPorousCoupledWithMPSAndMSSHill48DG3DMaterialLaw::~NonLocalPorousCoupledWithMPSAndMSSHill48DG3DMaterialLaw(){

}

void NonLocalPorousCoupledWithMPSAndMSSHill48DG3DMaterialLaw::setThomasonSmoothFactor(const double fact)
{
  static_cast<mlawNonLocalPorousCoupledHill48LawWithMPSAndMSS*>(_nldPorous)->setThomasonSmoothFactor(fact);
}
void NonLocalPorousCoupledWithMPSAndMSSHill48DG3DMaterialLaw::setShearSmoothFactor(const double fact)
{
  static_cast<mlawNonLocalPorousCoupledHill48LawWithMPSAndMSS*>(_nldPorous)->setShearSmoothFactor(fact);
}

void NonLocalPorousCoupledWithMPSAndMSSHill48DG3DMaterialLaw::setShearFactor(double g)
{
  static_cast<mlawNonLocalPorousCoupledHill48LawWithMPSAndMSS*>(_nldPorous)->setShearFactor(g);
}

void NonLocalPorousCoupledWithMPSAndMSSHill48DG3DMaterialLaw::setVoidLigamentExponent(double e)
{
  static_cast<mlawNonLocalPorousCoupledHill48LawWithMPSAndMSS*>(_nldPorous)->setVoidLigamentExponent(e);
}

void NonLocalPorousCoupledWithMPSAndMSSHill48DG3DMaterialLaw::freezeCoalescenceState(const bool fl)
{
  static_cast<mlawNonLocalPorousCoupledHill48LawWithMPSAndMSS*>(_nldPorous)->freezeCoalescenceState(fl);
}

void NonLocalPorousCoupledWithMPSAndMSSHill48DG3DMaterialLaw::setNeckingCoalescenceLaw(const CoalescenceLaw& added_coalsLaw)
{
  static_cast<mlawNonLocalPorousCoupledHill48LawWithMPSAndMSS*>(_nldPorous)->setNeckingCoalescenceLaw(added_coalsLaw);
};
void NonLocalPorousCoupledWithMPSAndMSSHill48DG3DMaterialLaw::setShearCoalescenceLaw(const CoalescenceLaw& added_coalsLaw)
{
  static_cast<mlawNonLocalPorousCoupledHill48LawWithMPSAndMSS*>(_nldPorous)->setShearCoalescenceLaw(added_coalsLaw);
};
void NonLocalPorousCoupledWithMPSAndMSSHill48DG3DMaterialLaw::setShearVoidEvolutionLaw(const voidStateEvolutionLaw& voidtateLaw)
{
  static_cast<mlawNonLocalPorousCoupledHill48LawWithMPSAndMSS*>(_nldPorous)->setShearVoidEvolutionLaw(voidtateLaw);
};
void NonLocalPorousCoupledWithMPSAndMSSHill48DG3DMaterialLaw::setYieldParameters(const std::vector<double>& params)
{
  static_cast<mlawNonLocalPorousCoupledHill48LawWithMPSAndMSS*>(_nldPorous)->setYieldParameters(params);
};

void NonLocalPorousCoupledWithMPSAndMSSHill48DG3DMaterialLaw::setYieldParameters(const fullVector<double>& params)
{
  std::vector<double> params_vec(params.size());
  for (int i=0; i< params.size(); i++)
  {
    params_vec[i] = params(i);
  }
  static_cast<mlawNonLocalPorousCoupledHill48LawWithMPSAndMSS*>(_nldPorous)->setYieldParameters(params_vec);
};

NonLocalPorousDuctileDamageWithCleavageDG3DMaterialLaw::NonLocalPorousDuctileDamageWithCleavageDG3DMaterialLaw(const int num, NonLocalPorousDuctileDamageDG3DMaterialLaw& law,
  const CLengthLaw &cLLaw,const DamageLaw &damLaw,double sigmac):
  dG3DMaterialLaw(num,law.density(),true){
  _nlPorousWithCleavage = new mlawNonLocalPorousWithCleavageFailure(*law.getNonLocalPorosityLaw(),cLLaw,damLaw,sigmac);
};

NonLocalPorousDuctileDamageWithCleavageDG3DMaterialLaw::NonLocalPorousDuctileDamageWithCleavageDG3DMaterialLaw(const int num, NonLocalPorousDuctileDamageDG3DMaterialLaw& law,
  const CLengthLaw &cLLaw,const DamageLaw &damLaw, const FailureCriterionBase& failureLaw):
  dG3DMaterialLaw(num,law.density(),true){
  _nlPorousWithCleavage = new mlawNonLocalPorousWithCleavageFailure(*law.getNonLocalPorosityLaw(),cLLaw,damLaw,failureLaw);
};

NonLocalPorousDuctileDamageWithCleavageDG3DMaterialLaw::NonLocalPorousDuctileDamageWithCleavageDG3DMaterialLaw(const NonLocalPorousDuctileDamageWithCleavageDG3DMaterialLaw& src):
dG3DMaterialLaw(src){
  if (src._nlPorousWithCleavage != NULL){
    _nlPorousWithCleavage = dynamic_cast<mlawNonLocalPorousWithCleavageFailure*>(src._nlPorousWithCleavage->clone());
  }
  else{
    _nlPorousWithCleavage = NULL;
  }
};

NonLocalPorousDuctileDamageWithCleavageDG3DMaterialLaw::~NonLocalPorousDuctileDamageWithCleavageDG3DMaterialLaw(){
  if (_nlPorousWithCleavage !=NULL){delete _nlPorousWithCleavage; _nlPorousWithCleavage = NULL;};
};

void NonLocalPorousDuctileDamageWithCleavageDG3DMaterialLaw::createIPState(IPStateBase* &ips,bool hasBodyForce,const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  double fvInit = _nlPorousWithCleavage->generateARandomInitialPorosityValue();

  int numNL=_nlPorousWithCleavage->getNumNonLocalVariables();
  int numExtraDof=0;

  IPVariable* ipvi = new  nonLocalPorosityWithCleavageDG3DIPVariable(*_nlPorousWithCleavage,fvInit,hasBodyForce,inter,numNL,numExtraDof);
  IPVariable* ipv1 = new  nonLocalPorosityWithCleavageDG3DIPVariable(*_nlPorousWithCleavage,fvInit,hasBodyForce,inter,numNL,numExtraDof);
  IPVariable* ipv2 = new  nonLocalPorosityWithCleavageDG3DIPVariable(*_nlPorousWithCleavage,fvInit,hasBodyForce,inter,numNL,numExtraDof);

  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void NonLocalPorousDuctileDamageWithCleavageDG3DMaterialLaw::createIPVariable(IPVariable* &ipv,bool hasBodyForce,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  double fvInit = _nlPorousWithCleavage->generateARandomInitialPorosityValue();
  int numNL=_nlPorousWithCleavage->getNumNonLocalVariables();
  int numExtraDof=0;

  ipv = new  nonLocalPorosityWithCleavageDG3DIPVariable(*_nlPorousWithCleavage,fvInit,hasBodyForce,inter,numNL,numExtraDof);

}

void NonLocalPorousDuctileDamageWithCleavageDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  /* get ipvariable */

  nonLocalPorosityWithCleavageDG3DIPVariable* ipvcur;
  const nonLocalPorosityWithCleavageDG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);

  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<nonLocalPorosityWithCleavageDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const nonLocalPorosityWithCleavageDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<nonLocalPorosityWithCleavageDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const nonLocalPorosityWithCleavageDG3DIPVariable*>(ipvp);
  }
  _nlPorousWithCleavage->checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());
};

void NonLocalPorousDuctileDamageWithCleavageDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  /* get ipvariable */

  nonLocalPorosityWithCleavageDG3DIPVariable* ipvcur;
  const nonLocalPorosityWithCleavageDG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);

  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<nonLocalPorosityWithCleavageDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const nonLocalPorosityWithCleavageDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<nonLocalPorosityWithCleavageDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const nonLocalPorosityWithCleavageDG3DIPVariable*>(ipvp);
  }


  /* strain xyz */
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

  /* data for J2 law */
  IPNonLocalPorosityWithCleavage* q1 = ipvcur->getIPNonLocalPorosityWithCleavage();
  const IPNonLocalPorosityWithCleavage* q0 = ipvprev->getIPNonLocalPorosityWithCleavage();

  // set additional kinematic variables
	q1->setRefToCurrentOutwardNormal(ipvcur->getConstRefToCurrentOutwardNormal());
	q1->setRefToReferenceOutwardNormal(ipvcur->getConstRefToReferenceOutwardNormal());
  q1->setRefToDeformationGradient(ipvcur->getConstRefToDeformationGradient());


  /* compute stress */

  if (_nlPorousWithCleavage->getNonLocalMethod() == mlawNonLocalPorosity::LOCAL){
    _nlPorousWithCleavage->getNonLocalPorosityLaw()->constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),stiff);
  }
  else {
    _nlPorousWithCleavage->predictorCorrector(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),
                      ipvcur->getRefToDLocalVariableDStrain(),ipvcur->getRefToDStressDNonLocalVariable(),
                      ipvcur->getRefToDLocalVariableDNonLocalVariable(),stiff);
  }


};

//
NonLocalDamageJ2FullyCoupledDG3DMaterialLaw::NonLocalDamageJ2FullyCoupledDG3DMaterialLaw(const int num,const double rho,
                   double E,const double nu, const double sy0, const double h,
                    const CLengthLaw &_cLLaw,const DamageLaw &_damLaw,const double tol,
                    const bool matrixBypert, const double tolPert) :
                                                              dG3DMaterialLaw(num,rho,true)
{
  _nldJ2Hyperlaw = new mlawNonLocalDamageJ2FullyCoupledThermoMechanics(num,E,nu,rho,sy0,h,_cLLaw,_damLaw,tol,matrixBypert,tolPert);
  fillElasticStiffness(E, nu, elasticStiffness);
}

NonLocalDamageJ2FullyCoupledDG3DMaterialLaw::~NonLocalDamageJ2FullyCoupledDG3DMaterialLaw() {
	if (_nldJ2Hyperlaw!=NULL){
		delete _nldJ2Hyperlaw;
		_nldJ2Hyperlaw = NULL;
	}
};


NonLocalDamageJ2FullyCoupledDG3DMaterialLaw::NonLocalDamageJ2FullyCoupledDG3DMaterialLaw(const NonLocalDamageJ2FullyCoupledDG3DMaterialLaw &source) :
                                                             dG3DMaterialLaw(source)
{
	_nldJ2Hyperlaw = NULL;
	if (source._nldJ2Hyperlaw != NULL){
		_nldJ2Hyperlaw = new mlawNonLocalDamageJ2FullyCoupledThermoMechanics(*(source._nldJ2Hyperlaw));
	}
}

void NonLocalDamageJ2FullyCoupledDG3DMaterialLaw::setStrainOrder(const int order){
  _nldJ2Hyperlaw->setStrainOrder(order);
};
void NonLocalDamageJ2FullyCoupledDG3DMaterialLaw::setReferenceTemperature(const double Tr){
  _nldJ2Hyperlaw->setReferenceTemperature(Tr);
};

void NonLocalDamageJ2FullyCoupledDG3DMaterialLaw::setIsotropicHardeningLaw(const J2IsotropicHardening& isoHard){
  _nldJ2Hyperlaw->setIsotropicHardeningLaw(isoHard);
};

void NonLocalDamageJ2FullyCoupledDG3DMaterialLaw::setReferenceThermalExpansionCoefficient(const double al){
  _nldJ2Hyperlaw->setReferenceThermalExpansionCoefficient(al);
};
void NonLocalDamageJ2FullyCoupledDG3DMaterialLaw::setTemperatureFunction_ThermalExpansionCoefficient(const scalarFunction& Tfunc){
  _nldJ2Hyperlaw->setTemperatureFunction_ThermalExpansionCoefficient(Tfunc);
};

void NonLocalDamageJ2FullyCoupledDG3DMaterialLaw::setReferenceThermalConductivity(const double KK){
  _nldJ2Hyperlaw->setReferenceThermalConductivity(KK);
};
void NonLocalDamageJ2FullyCoupledDG3DMaterialLaw::setTemperatureFunction_ThermalConductivity(const scalarFunction& Tfunc){
  _nldJ2Hyperlaw->setTemperatureFunction_ThermalConductivity(Tfunc);
};

void NonLocalDamageJ2FullyCoupledDG3DMaterialLaw::setReferenceCp(const double Cp){
  _nldJ2Hyperlaw->setReferenceCp(Cp);
};
void NonLocalDamageJ2FullyCoupledDG3DMaterialLaw::setTemperatureFunction_Cp(const scalarFunction& Tfunc){
  _nldJ2Hyperlaw->setTemperatureFunction_Cp(Tfunc);
};

void NonLocalDamageJ2FullyCoupledDG3DMaterialLaw::setTemperatureFunction_Hardening(const scalarFunction& Tfunc){
  _nldJ2Hyperlaw->setTemperatureFunction_Hardening(Tfunc);
};
void NonLocalDamageJ2FullyCoupledDG3DMaterialLaw::setTemperatureFunction_InitialYieldStress(const scalarFunction& Tfunc){
  _nldJ2Hyperlaw->setTemperatureFunction_InitialYieldStress(Tfunc);
};
void NonLocalDamageJ2FullyCoupledDG3DMaterialLaw::setTemperatureFunction_BulkModulus(const scalarFunction& Tfunc){
  _nldJ2Hyperlaw->setTemperatureFunction_BulkModulus(Tfunc);
};
void NonLocalDamageJ2FullyCoupledDG3DMaterialLaw::setTemperatureFunction_ShearModulus(const scalarFunction& Tfunc){
  _nldJ2Hyperlaw->setTemperatureFunction_ShearModulus(Tfunc);
};
void NonLocalDamageJ2FullyCoupledDG3DMaterialLaw::setTaylorQuineyFactor(const double f){
  _nldJ2Hyperlaw->setTaylorQuineyFactor(f);
};
void NonLocalDamageJ2FullyCoupledDG3DMaterialLaw::setThermalEstimationPreviousConfig(const bool flag){
  _nldJ2Hyperlaw->setThermalEstimationPreviousConfig(flag);
}

void
NonLocalDamageJ2FullyCoupledDG3DMaterialLaw::setTime(const double t,const double dtime){
  dG3DMaterialLaw::setTime(t,dtime);
  _nldJ2Hyperlaw->setTime(t,dtime);
}

materialLaw::matname
NonLocalDamageJ2FullyCoupledDG3DMaterialLaw::getType() const {return _nldJ2Hyperlaw->getType();}

void NonLocalDamageJ2FullyCoupledDG3DMaterialLaw::createIPState(IPStateBase* &ips,bool hasBodyForce,const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  IPVariable* ipvi = new  nonLocalDamageJ2FullyCoupledDG3DIPVariable(*_nldJ2Hyperlaw,hasBodyForce,inter);
  IPVariable* ipv1 = new  nonLocalDamageJ2FullyCoupledDG3DIPVariable(*_nldJ2Hyperlaw,hasBodyForce,inter);
  IPVariable* ipv2 = new  nonLocalDamageJ2FullyCoupledDG3DIPVariable(*_nldJ2Hyperlaw,hasBodyForce,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
  _nldJ2Hyperlaw->createIPState((static_cast <nonLocalDamageJ2FullyCoupledDG3DIPVariable*> (ipvi))->getIPNonLocalDamageJ2FullyCoupledThermoMechanics(),
                         (static_cast <nonLocalDamageJ2FullyCoupledDG3DIPVariable*> (ipv1))->getIPNonLocalDamageJ2FullyCoupledThermoMechanics(),
             			 (static_cast <nonLocalDamageJ2FullyCoupledDG3DIPVariable*> (ipv2))->getIPNonLocalDamageJ2FullyCoupledThermoMechanics());

}

void NonLocalDamageJ2FullyCoupledDG3DMaterialLaw::createIPVariable(IPVariable* &ipv,bool hasBodyForce,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  ipv = new  nonLocalDamageJ2FullyCoupledDG3DIPVariable(*_nldJ2Hyperlaw,hasBodyForce,inter);
  IPNonLocalDamageJ2FullyCoupledThermoMechanics * ipvnl = static_cast <nonLocalDamageJ2FullyCoupledDG3DIPVariable*>(ipv)->getIPNonLocalDamageJ2FullyCoupledThermoMechanics();
  _nldJ2Hyperlaw->createIPVariable(ipvnl);
}

double
NonLocalDamageJ2FullyCoupledDG3DMaterialLaw::soundSpeed() const{return _nldJ2Hyperlaw->soundSpeed();} // or change value ??

void NonLocalDamageJ2FullyCoupledDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  /* get ipvariable */
  nonLocalDamageJ2FullyCoupledDG3DIPVariable* ipvcur; //= static_cast < nonLocalDamageDG3DIPVariable *> (ipv);
  const nonLocalDamageJ2FullyCoupledDG3DIPVariable* ipvprev; //= static_cast <const  nonLocalDamageDG3DIPVariable *> (ipvp);

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<nonLocalDamageJ2FullyCoupledDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const nonLocalDamageJ2FullyCoupledDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<nonLocalDamageJ2FullyCoupledDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const nonLocalDamageJ2FullyCoupledDG3DIPVariable*>(ipvp);
  }
  _nldJ2Hyperlaw->checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());
};
void NonLocalDamageJ2FullyCoupledDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  /* get ipvariable */
  nonLocalDamageJ2FullyCoupledDG3DIPVariable* ipvcur; //= static_cast < nonLocalDamageDG3DIPVariable *> (ipv);
  const nonLocalDamageJ2FullyCoupledDG3DIPVariable* ipvprev; //= static_cast <const  nonLocalDamageDG3DIPVariable *> (ipvp);

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<nonLocalDamageJ2FullyCoupledDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const nonLocalDamageJ2FullyCoupledDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<nonLocalDamageJ2FullyCoupledDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const nonLocalDamageJ2FullyCoupledDG3DIPVariable*>(ipvp);
  }

  /* strain xyz */
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

  /* data for J2 law */
  IPNonLocalDamageJ2FullyCoupledThermoMechanics* q1 = ipvcur->getIPNonLocalDamageJ2FullyCoupledThermoMechanics();
  const IPNonLocalDamageJ2FullyCoupledThermoMechanics* q0 = ipvprev->getIPNonLocalDamageJ2FullyCoupledThermoMechanics();

  double& Cp = ipvcur->getRefToExtraDofFieldCapacityPerUnitField()(0);
  //STensor43& elasticL = ipvcur->getRefToElasticTangentModuli();
  /* compute stress */
  _nldJ2Hyperlaw->constitutive(F0,F1, ipvcur->getRefToFirstPiolaKirchhoffStress(),
                              q0,q1,
                              ipvcur->getRefToTangentModuli(),  ipvcur->getRefToDLocalVariableDStrain()[0],
                              ipvcur->getRefToDStressDNonLocalVariable()[0],
                              ipvcur->getRefToDLocalVariableDNonLocalVariable()(0,0),
                              stiff,
                              ipvcur->getRefTodPdField()[0], ipvcur->getRefTodPdGradField()[0],  ipvcur->getRefTodLocalVariableDExtraDofDiffusionField()(0,0),
                              ipvprev->getConstRefToField(0),ipvcur->getConstRefToField(0),
                              ipvprev->getConstRefToGradField()[0],ipvcur->getConstRefToGradField()[0],
                              ipvcur->getRefToFlux()[0],
                              ipvcur->getRefTodFluxdF()[0], ipvcur->getRefTodFluxdNonLocalVariable()[0][0],
                              ipvcur->getRefTodFluxdField()[0][0], ipvcur->getRefTodFluxdGradField()[0][0],
                              ipvcur->getRefToFieldSource()(0), 
                              ipvcur->getRefTodFieldSourcedF()[0], ipvcur->getRefTodFieldSourcedNonLocalVariable()(0,0),
                              ipvcur->getRefTodFieldSourcedField()(0,0), ipvcur->getRefTodFieldSourcedGradField()[0][0],  
                              ipvcur->getRefToMechanicalSource()(0),
                              ipvcur->getRefTodMechanicalSourcedF()[0],ipvcur->getRefTodMechanicalSourcedNonLocalVariable()(0,0),
                              ipvcur->getRefTodMechanicalSourcedField()(0,0), ipvcur->getRefTodMechanicalSourcedGradField()[0][0]);
                            
                              
  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);

  ipvcur->setConstRefToLinearK(_nldJ2Hyperlaw->getConductivityTensor(),0,0);
  ipvcur->setConstRefToLinearSymmetrizationCoupling(_nldJ2Hyperlaw->getStiff_alphaDilatation(),0);
  Cp = _nldJ2Hyperlaw->getExtraDofStoredEnergyPerUnitField(ipvcur->getConstRefToField(0));

}
double NonLocalDamageJ2FullyCoupledDG3DMaterialLaw::getInitialExtraDofStoredEnergyPerUnitField() const{
  return _nldJ2Hyperlaw->getInitialExtraDofStoredEnergyPerUnitField();
};

double NonLocalDamageJ2FullyCoupledDG3DMaterialLaw::getExtraDofStoredEnergyPerUnitField(double T) const
{
 return _nldJ2Hyperlaw->getExtraDofStoredEnergyPerUnitField(T);

}




NonlocalDamageTorchANNBasedDG3DMaterialLaw::NonlocalDamageTorchANNBasedDG3DMaterialLaw(const int num, const double rho, const int numberOfInput, const int numNonlocalVar,
                const int numInternalVars_stress, const int numInternalVars_elTang, const int numInternalVars_NonDamTang, const char* nameTorch_stress, const char* nameTorch_elTang, const char* nameTorch_NonDamTang, const double EXXmean, const double EXXstd, const double EXYmean,
                const double EXYstd, const double EYYmean, const double EYYstd, const double EYZmean, const double EYZstd, const double EZZmean,
                const double EZZstd, const double EZXmean, const double EZXstd, 
                const double SXXmean, const double SXXstd, const double SXYmean,
                const double SXYstd, const double SYYmean, const double SYYstd, const double SYZmean, const double SYZstd, const double SZZmean,
                const double SZZstd, const double SZXmean, const double SZXstd,  
                const double elTangCompxxxxmean, const double elTangCompxxxxstd, const double elTangCompyyxxmean, const double elTangCompyyxxstd, const double elTangCompzzxxmean, const double elTangCompzzxxstd,  const double elTangCompxyxymean, const double elTangCompxyxystd, const double elTangCompxzxzmean, const double elTangCompxzxzstd, const double elTangCompyyyymean, const double elTangCompyyyystd, const double elTangCompzzyymean, const double elTangCompzzyystd, const double elTangCompyzyzmean, const double elTangCompyzyzstd, const double elTangCompzzzzmean, const double elTangCompzzzzstd, 
                const double NonDamTangCompxxxxmean, const double NonDamTangCompxxxxstd, const double NonDamTangCompyyxxmean, const double NonDamTangCompyyxxstd, const double NonDamTangCompzzxxmean, const double NonDamTangCompzzxxstd,  const double NonDamTangCompxyxymean, const double NonDamTangCompxyxystd, const double NonDamTangCompxzxzmean, const double NonDamTangCompxzxzstd, const double NonDamTangCompyyyymean, const double NonDamTangCompyyyystd, const double NonDamTangCompzzyymean, const double NonDamTangCompzzyystd, const double NonDamTangCompyzyzmean, const double NonDamTangCompyzyzstd, const double NonDamTangCompzzzzmean, const double NonDamTangCompzzzzstd, 
                bool pert, double tol):
                dG3DMaterialLaw(num,rho,false), _numberOfInput(numberOfInput),_numNonlocalVar(numNonlocalVar), _numberOfInternalVariables_stress(numInternalVars_stress), _numberOfInternalVariables_elTang(numInternalVars_elTang), _numberOfInternalVariables_NonDamTang(numInternalVars_NonDamTang),
                _initialValue_h(-1.), _EXXmean(EXXmean), _EXXstd(EXXstd), _EXYmean(EXYmean), _EXYstd(EXYstd), _EYYmean(EYYmean), _EYYstd(EYYstd),
                _EYZmean(EYZmean), _EYZstd(EYZstd), _EZZmean(EZZmean), _EZZstd(EZZstd), _EZXmean(EZXmean), _EZXstd(EZXstd), 
                _SXXmean(SXXmean), _SXXstd(SXXstd),_SXYmean(SXYmean), _SXYstd(SXYstd), _SYYmean(SYYmean), _SYYstd(SYYstd), _SYZmean(SYZmean), _SYZstd(SYZstd), _SZZmean(SZZmean), _SZZstd(SZZstd),
                _SZXmean(SZXmean), _SZXstd(SZXstd), 
                _elTangCompxxxxmean(elTangCompxxxxmean), _elTangCompxxxxstd(elTangCompxxxxstd), _elTangCompyyxxmean(elTangCompyyxxmean), _elTangCompyyxxstd(elTangCompyyxxstd), _elTangCompzzxxmean(elTangCompzzxxmean), _elTangCompzzxxstd(elTangCompzzxxstd), _elTangCompxyxymean(elTangCompxyxymean), _elTangCompxyxystd(elTangCompxyxystd), _elTangCompxzxzmean(elTangCompxzxzmean), _elTangCompxzxzstd(elTangCompxzxzstd), _elTangCompyyyymean(elTangCompyyyymean), _elTangCompyyyystd(elTangCompyyyystd), _elTangCompzzyymean(elTangCompzzyymean), _elTangCompzzyystd(elTangCompzzyystd), _elTangCompyzyzmean(elTangCompyzyzmean), _elTangCompyzyzstd(elTangCompyzyzstd), _elTangCompzzzzmean(elTangCompzzzzmean), _elTangCompzzzzstd(elTangCompzzzzstd),   
                _NonDamTangCompxxxxmean(NonDamTangCompxxxxmean), _NonDamTangCompxxxxstd(NonDamTangCompxxxxstd), _NonDamTangCompyyxxmean(NonDamTangCompyyxxmean), _NonDamTangCompyyxxstd(NonDamTangCompyyxxstd), _NonDamTangCompzzxxmean(NonDamTangCompzzxxmean), _NonDamTangCompzzxxstd(NonDamTangCompzzxxstd), _NonDamTangCompxyxymean(NonDamTangCompxyxymean), _NonDamTangCompxyxystd(NonDamTangCompxyxystd), _NonDamTangCompxzxzmean(NonDamTangCompxzxzmean), _NonDamTangCompxzxzstd(NonDamTangCompxzxzstd), _NonDamTangCompyyyymean(NonDamTangCompyyyymean), _NonDamTangCompyyyystd(NonDamTangCompyyyystd), _NonDamTangCompzzyymean(NonDamTangCompzzyymean), _NonDamTangCompzzyystd(NonDamTangCompzzyystd), _NonDamTangCompyzyzmean(NonDamTangCompyzyzmean), _NonDamTangCompyzyzstd(NonDamTangCompyzyzstd), _NonDamTangCompzzzzmean(NonDamTangCompzzzzmean), _NonDamTangCompzzzzstd(NonDamTangCompzzzzstd),       
                _tangentByPerturbation(pert), _pertTol(tol), _kinematicInput(EGL),_NeedExtraNorm(false),_DoubleInput(false),_RNN(true),_Ie00(1), _Ie01(1), _Ie02(1), _Ie11(1), _Ie12(1), _Ie22(1), _Id0000(1), _Id0100(1), _Id0200(1), _Id1100(1), _Id1200(1), _Id2200(1), _Id0101(1), _Id0201(1), _Id1101(1), _Id1201(1), _Id2201(1), _Id0202(1), _Id1102(1), _Id1202(1), _Id2202(1), _Id1111(1), _Id1211(1), _Id2211(1), _Id1212(1), _Id2212(1), _Id2222(1)
{
   _mecalocalLaw = NULL;
   cLLaw.resize(_numNonlocalVar);
   for(int i=0; i<_numNonlocalVar; i++)
     cLLaw[i]=NULL;

#if defined(HAVE_TORCH)
    try{
      module_stress = torch::jit::load(nameTorch_stress); 
      module_elTang = torch::jit::load(nameTorch_elTang); 
      module_NonDamTang = torch::jit::load(nameTorch_NonDamTang); 
    }
    catch (const c10::Error& e) {
      Msg::Error("error loading the model");
    }
    module_stress.eval();
    module_elTang.eval();
    module_NonDamTang.eval();

   if(_numberOfInput == 3){ // for 2D case    
        VXX = torch::zeros({1, 1, 9});
        VXY = torch::zeros({1, 1, 9});
        VYY = torch::zeros({1, 1, 9});
        VZZ = torch::zeros({1, 1, 9});
        EffVXX = torch::zeros({1, 1, 9});
        EffVXY = torch::zeros({1, 1, 9});
        EffVYY = torch::zeros({1, 1, 9});
        EffVZZ = torch::zeros({1, 1, 9});
        VLocal = torch::zeros({1, 1, 9});

        VXX[0][0][0] = 1.0;
        VXY[0][0][1] = 1.0;
        VYY[0][0][2] = 1.0;
        VZZ[0][0][3] = 1.0;
        EffVXX[0][0][4] = 1.0;
        EffVXY[0][0][5] = 1.0;
        EffVYY[0][0][6] = 1.0;
        EffVZZ[0][0][7] = 1.0;
        VLocal[0][0][8] = 1.0;
     }
     else if(_numberOfInput == 6){ // for 3D case
        VXX = torch::zeros({1, 1, 13});
        VXY = torch::zeros({1, 1, 13});
        VYY = torch::zeros({1, 1, 13});
        VYZ = torch::zeros({1, 1, 13});
        VZZ = torch::zeros({1, 1, 13});
        VZX = torch::zeros({1, 1, 13});
        
        EffVXX = torch::zeros({1, 1, 13});
        EffVXY = torch::zeros({1, 1, 13});
        EffVYY = torch::zeros({1, 1, 13});
        EffVYZ = torch::zeros({1, 1, 13});
        EffVZZ = torch::zeros({1, 1, 13});
        EffVZX = torch::zeros({1, 1, 13});
        
        VLocal = torch::zeros({1, 1, 13});

        VXX[0][0][0] = 1.0;
        VYY[0][0][1] = 1.0;
        VZZ[0][0][2] = 1.0;
        VXY[0][0][3] = 1.0;
        VZX[0][0][4] = 1.0;
        VYZ[0][0][5] = 1.0;
        
        EffVXX[0][0][6] = 1.0;
        EffVYY[0][0][7] = 1.0;
        EffVZZ[0][0][8] = 1.0;
        EffVXY[0][0][9] = 1.0;
        EffVZX[0][0][10] = 1.0;
        EffVYZ[0][0][11] = 1.0;
        
        VLocal[0][0][12] = 1.0;
   }     
   
   // fill elastic stiffness from torch if no mecalocalmateriallaw defined
   
#else
  Msg::Error("NOT COMPILED WITH TORCH");
#endif

}


NonlocalDamageTorchANNBasedDG3DMaterialLaw::NonlocalDamageTorchANNBasedDG3DMaterialLaw(const NonlocalDamageTorchANNBasedDG3DMaterialLaw& src):
      dG3DMaterialLaw(src), _numberOfInput(src._numberOfInput), _numNonlocalVar(src._numNonlocalVar), _numberOfInternalVariables_stress(src._numberOfInternalVariables_stress), _numberOfInternalVariables_elTang(src._numberOfInternalVariables_elTang), _numberOfInternalVariables_NonDamTang(src._numberOfInternalVariables_NonDamTang),
      _initialValue_h(src._initialValue_h),
                _EXXmean(src._EXXmean), _EXXstd(src._EXXstd), _EXYmean(src._EXYmean), _EXYstd(src._EXYstd), _EYYmean(src._EYYmean), _EYYstd(src._EYYstd),
                _EYZmean(src._EYZmean), _EYZstd(src._EYZstd), _EZZmean(src._EZZmean), _EZZstd(src._EZZstd), _EZXmean(src._EZXmean), _EZXstd(src._EZXstd),
                _SXXmean(src._SXXmean), _SXXstd(src._SXXstd), _SXYmean(src._SXYmean), _SXYstd(src._SXYstd), _SYYmean(src._SYYmean), _SYYstd(src._SYYstd),
                _SYZmean(src._SYZmean), _SYZstd(src._SYZstd), _SZZmean(src._SZZmean), _SZZstd(src._SZZstd), _SZXmean(src._SZXmean), _SZXstd(src._SZXstd), 
                _elTangCompxxxxmean(src._elTangCompxxxxmean), _elTangCompxxxxstd(src._elTangCompxxxxstd), _elTangCompyyxxmean(src._elTangCompyyxxmean), _elTangCompyyxxstd(src._elTangCompyyxxstd),    _elTangCompzzxxmean(src._elTangCompzzxxmean), _elTangCompzzxxstd(src._elTangCompzzxxstd), _elTangCompxyxymean(src._elTangCompxyxymean), _elTangCompxyxystd(src._elTangCompxyxystd), _elTangCompxzxzmean(src._elTangCompxzxzmean), _elTangCompxzxzstd(src._elTangCompxzxzstd), _elTangCompyyyymean(src._elTangCompyyyymean), _elTangCompyyyystd(src._elTangCompyyyystd), _elTangCompzzyymean(src._elTangCompzzyymean), _elTangCompzzyystd(src._elTangCompzzyystd), _elTangCompyzyzmean(src._elTangCompyzyzmean), _elTangCompyzyzstd(src._elTangCompyzyzstd), _elTangCompzzzzmean(src._elTangCompzzzzmean), _elTangCompzzzzstd(src._elTangCompzzzzstd),       
                _NonDamTangCompxxxxmean(src._NonDamTangCompxxxxmean), _NonDamTangCompxxxxstd(src._NonDamTangCompxxxxstd), _NonDamTangCompyyxxmean(src._NonDamTangCompyyxxmean), _NonDamTangCompyyxxstd(src._NonDamTangCompyyxxstd),    _NonDamTangCompzzxxmean(src._NonDamTangCompzzxxmean), _NonDamTangCompzzxxstd(src._NonDamTangCompzzxxstd), _NonDamTangCompxyxymean(src._NonDamTangCompxyxymean), _NonDamTangCompxyxystd(src._NonDamTangCompxyxystd), _NonDamTangCompxzxzmean(src._NonDamTangCompxzxzmean), _NonDamTangCompxzxzstd(src._NonDamTangCompxzxzstd), _NonDamTangCompyyyymean(src._NonDamTangCompyyyymean), _NonDamTangCompyyyystd(src._NonDamTangCompyyyystd), _NonDamTangCompzzyymean(src._NonDamTangCompzzyymean), _NonDamTangCompzzyystd(src._NonDamTangCompzzyystd), _NonDamTangCompyzyzmean(src._NonDamTangCompyzyzmean), _NonDamTangCompyzyzstd(src._NonDamTangCompyzyzstd), _NonDamTangCompzzzzmean(src._NonDamTangCompzzzzmean), _NonDamTangCompzzzzstd(src._NonDamTangCompzzzzstd),                                   
                _tangentByPerturbation(src._tangentByPerturbation),_pertTol(src._pertTol), _kinematicInput(src._kinematicInput),_NeedExtraNorm(src._NeedExtraNorm),_DoubleInput(src._DoubleInput), _mecalocalLaw(NULL), _RNN(src._RNN),_Ie00(src._Ie00), _Ie01(src._Ie01), _Ie02(src._Ie02), _Ie11(src._Ie11), _Ie12(src._Ie12), _Ie22(src._Ie22), _Id0000(src._Id0000), _Id0100(src._Id0100), _Id0200(src._Id0200), _Id1100(src._Id1100), _Id1200(src._Id1200), _Id2200(src._Id2200), _Id0101(src._Id0101), _Id0201(src._Id0201), _Id1101(src._Id1101), _Id1201(src._Id1201), _Id2201(src._Id2201), _Id0202(src._Id0202), _Id1102(src._Id1102), _Id1202(src._Id1202), _Id2202(src._Id2202), _Id1111(src._Id1111), _Id1211(src._Id1211), _Id2211(src._Id2211), _Id1212(src._Id1212), _Id2212(src._Id2212), _Id2222(src._Id2222)
{
#if defined(HAVE_TORCH)
       module_stress = src.module_stress;
       module_elTang = src.module_elTang;
       module_NonDamTang = src.module_NonDamTang;
       

       if(_numberOfInput == 3){ // for 2D case
        VXX = src.VXX;
        VXY = src.VXY;
        VYY = src.VYY;
        VZZ = src.VZZ;
        EffVXX = src.EffVXX;
        EffVXY = src.EffVXY;
        EffVYY = src.EffVYY;
        EffVZZ = src.EffVZZ;

       }
       else if(_numberOfInput == 6){ // for 3D case
        VXX = src.VXX;
        VXY = src.VXY;
        VYY = src.VYY;
        VYZ = src.VYZ;
        VZZ = src.VZZ;
        VZX = src.VZX;
        EffVXX = src.EffVXX;
        EffVXY = src.EffVXY;
        EffVYY = src.EffVYY;
        EffVYZ = src.EffVYZ;
        EffVZZ = src.EffVZZ;
        EffVZX = src.EffVZX;
       }
       VLocal = src.VLocal;
#else
  Msg::Error("NOT COMPILED WITH TORCH");
#endif

  for( std::vector< CLengthLaw * > ::const_iterator it=src.cLLaw.begin(); it!=src.cLLaw.end(); it++)
  {
    cLLaw.push_back((*it)->clone());     
  }
  if(src._mecalocalLaw!=NULL)
  {
    if(_mecalocalLaw != NULL)
    {
      delete _mecalocalLaw;
      _mecalocalLaw = NULL;
    }
    _mecalocalLaw=src._mecalocalLaw->clone();
  }
}


NonlocalDamageTorchANNBasedDG3DMaterialLaw::~NonlocalDamageTorchANNBasedDG3DMaterialLaw()
{
  for( std::vector< CLengthLaw * > ::iterator it=cLLaw.begin(); it!=cLLaw.end(); it++)
  {
     if(*it!=NULL) delete *it;
  }
  if(_mecalocalLaw != NULL)
  {
    delete _mecalocalLaw;
    _mecalocalLaw = NULL;
  }
}

void NonlocalDamageTorchANNBasedDG3DMaterialLaw::setNonLocalLengthScale(const int index, const CLengthLaw &_cLLaw){
  cLLaw[index] = _cLLaw.clone();
};


void NonlocalDamageTorchANNBasedDG3DMaterialLaw::setKinematicInput(const int i)
{
  if (i == 1)
  {
    _kinematicInput = EGL;
    Msg::Info("using Green Lagrange strain as kinematic variable");
  }
  else if (i == 2)
  {
    _kinematicInput = U;
    Msg::Info("using Biot strain as kinematic variable");
  }
  else if (i == 3)
  {
    _kinematicInput = smallStrain;
    Msg::Info("using small strain (FT+F)/2-I as kinematic variable");
  }
  else if (i == 4)
  {
    _kinematicInput = EGLInc;
    Msg::Info("using Green Lagrange strain increment as kinematic variable");
  }
  else if (i == 5)
  {
    _kinematicInput = UInc;
    Msg::Info("using Biot strain increment as kinematic variable");
  }
  
  else if (i == 6)
  {
    _kinematicInput = smallStrainInc;
    Msg::Info("using small strain (FT+F)/2-I increment as kinematic variable");
  }
  else
  {
    Msg::Error("input type %d has not been defined",i);
  }
}



void NonlocalDamageTorchANNBasedDG3DMaterialLaw::initLaws(const std::map<int,materialLaw*> &maplaw){
#if defined(HAVE_TORCH)
    if (!_initialized){
        fullMatrix<double> E1(1,6);
        fullVector<double> E1Vector(6);
        for (int i=0;i<6;i++){
          E1Vector(i) = E1(0,i);
        }
        STensor3 E1Tensor;
        
        STensor3 S;
        fullMatrix<double> EffS(1,6);
        std::vector<double> _localVar(_numNonlocalVar);
        STensor43 NonDamTangPred;
        STensor43 elTangPred;  
        STensor43 D;
        STensor3 Eplast;
        STensor3 B0;
        STensor3 B1;
        STensor3 B2;
        STensor3 B3;
        STensor3 B4;
        STensor3 B5;
        STensor3 Eplast_l;

        fullMatrix<double> DSDE(6,6);   
        
        STensorOperation::fromFullVectorToSTensor3(E1Vector, E1Tensor);        
        
        torch::Tensor h_stress_init = _initialValue_h*torch::ones({1, 1, _numberOfInternalVariables_stress});
        torch::Tensor h_elTang_init = _initialValue_h*torch::ones({1, 1, _numberOfInternalVariables_elTang});
        torch::Tensor h_NonDamTang_init = _initialValue_h*torch::ones({1, 1, _numberOfInternalVariables_NonDamTang});
         
        static torch::Tensor h_stress_tmp = torch::zeros({1, 1, _numberOfInternalVariables_stress});  
        static torch::Tensor h_elTang_tmp = torch::zeros({1, 1, _numberOfInternalVariables_elTang});  
        static torch::Tensor h_NonDamTang_tmp = torch::zeros({1, 1, _numberOfInternalVariables_NonDamTang});  
        
       if (_tangentByPerturbation){
       
           static fullMatrix<double> E1_plus(1,6);
           fullVector<double> E1Vector_plus_pert1(6);
           STensor3 E1Tensor_plus_pert1;
           //static std::vector<double> localVar_plus(_numNonlocalVar);
           
           STensor43 NonDamTangPred_plus;
           STensor43 elTangPred_plus;
           STensor3 S_plus;
           STensor43 D_plus;
           STensor3 Eplast_plus;
           static fullMatrix<double> EffS_plus(1,6);

          for (int i=0; i< 6; i++) {
             E1_plus = E1;
             E1_plus(0,i) += _pertTol;
             for (int s=0;s<6;s++){
               E1Vector_plus_pert1(s) = E1_plus(0,s);
             }
             STensorOperation::fromFullVectorToSTensor3(E1Vector_plus_pert1, E1Tensor_plus_pert1);
             RNNstress_stiff(E1Tensor, E1Tensor_plus_pert1,h_stress_init, h_stress_tmp, h_elTang_init, h_elTang_tmp, h_NonDamTang_init, h_NonDamTang_tmp, S_plus, NonDamTangPred_plus,elTangPred_plus, D_plus, Eplast_plus,false, DSDE, B0, B1, B2, B3, B4, B5, Eplast_l);
          }
      }
      else
      {
          RNNstress_stiff(E1Tensor, E1Tensor, h_stress_init, h_stress_tmp, h_elTang_init, h_elTang_tmp, h_NonDamTang_init, h_NonDamTang_tmp, S,NonDamTangPred,elTangPred,D, Eplast, true, DSDE, B0, B1, B2, B3, B4, B5, Eplast_l);
      }
      _initialized = true;
    };
#else
  Msg::Error("NOT COMPILED WITH TORCH");
#endif
};



void NonlocalDamageTorchANNBasedDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element  (const int n, const double initial_h, const int numNonlocal, const std::vector<STensor3>& length,
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  
  IPVariable* ipvi = new  nonLocalDamageTorchANNDG3DIPVariableDG3DIPVariable(cLLaw,_numberOfInternalVariables_stress, _numberOfInternalVariables_elTang, _numberOfInternalVariables_NonDamTang, _initialValue_h,_numNonlocalVar, hasBodyForce,inter);
  IPVariable* ipv1 = new  nonLocalDamageTorchANNDG3DIPVariableDG3DIPVariable(cLLaw,_numberOfInternalVariables_stress,_numberOfInternalVariables_elTang, _numberOfInternalVariables_NonDamTang,_initialValue_h,_numNonlocalVar, hasBodyForce,inter);
  IPVariable* ipv2 = new  nonLocalDamageTorchANNDG3DIPVariableDG3DIPVariable(cLLaw,_numberOfInternalVariables_stress,_numberOfInternalVariables_elTang, _numberOfInternalVariables_NonDamTang,_initialValue_h,_numNonlocalVar, hasBodyForce,inter);
  
  if(ips != NULL) delete ips;

  IPStateBase* ipsmeca=NULL;
  const materialLaw& mloclaw = getConstRefMechanicalLocalMaterialLaw();
  mloclaw.createIPState(ipsmeca,hasBodyForce, state_,ele, nbFF_,GP, gpt);
  //getConstRefMechanicalLocalMaterialLaw().createIPState(ipsmeca,hasBodyForce, state_,ele, nbFF_,GP, gpt);
  std::vector<IPVariable*> allIP;
  ipsmeca->getAllIPVariable(allIP);
  
  dynamic_cast<nonLocalDamageTorchANNDG3DIPVariableDG3DIPVariable*>(ipvi)->setIpMeca(*allIP[0]->clone());
  dynamic_cast<nonLocalDamageTorchANNDG3DIPVariableDG3DIPVariable*>(ipv1)->setIpMeca(*allIP[1]->clone());
  dynamic_cast<nonLocalDamageTorchANNDG3DIPVariableDG3DIPVariable*>(ipv2)->setIpMeca(*allIP[2]->clone());
  
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
  delete ipsmeca;
}



void NonlocalDamageTorchANNBasedDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  ipv = new  nonLocalDamageTorchANNDG3DIPVariableDG3DIPVariable(cLLaw,_numberOfInternalVariables_stress,_numberOfInternalVariables_elTang, _numberOfInternalVariables_NonDamTang,_initialValue_h, _numNonlocalVar, hasBodyForce,inter);
  
  IPStateBase* ipsmeca=NULL;
  const bool state_ = true;
  getConstRefMechanicalLocalMaterialLaw().createIPState(ipsmeca,hasBodyForce, &state_,ele, nbFF_,GP, gpt);
  
  std::vector<IPVariable*> allIP;
  ipsmeca->getAllIPVariable(allIP);
  
  dynamic_cast<nonLocalDamageTorchANNDG3DIPVariableDG3DIPVariable*>(ipv)->setIpMeca(*allIP[0]->clone());
  delete ipsmeca;
}





void NonlocalDamageTorchANNBasedDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
#if defined(HAVE_TORCH)
  /* get ipvariable */
  nonLocalDamageTorchANNDG3DIPVariableDG3DIPVariable* ipvcur = static_cast<nonLocalDamageTorchANNDG3DIPVariableDG3DIPVariable*>(ipv);;
  const nonLocalDamageTorchANNDG3DIPVariableDG3DIPVariable* ipvprev = static_cast<const nonLocalDamageTorchANNDG3DIPVariableDG3DIPVariable*>(ipvp);
  
  int index=0;
  for( std::vector< CLengthLaw * > ::iterator it=cLLaw.begin(); it!=cLLaw.end(); it++,index++)
  {
    double p0 = ipvprev->getConstRefToLocalVariable(index);
    (*it)->computeCL(p0, *(ipvcur->getRefToIPCLength()[index]));
  }
  
  fullMatrix<double>& E1 = ipvcur->getRefToKinematicVariables();
  static fullMatrix<double> E0(1,6);   
  // compute E1
  const STensor3& F = ipvcur->getConstRefToDeformationGradient();
  static STensor3 FTF, Ustretch, R, FTF0, Ustretch0, R0;
  if (_kinematicInput == NonlocalDamageTorchANNBasedDG3DMaterialLaw::EGL)
  {
    STensorOperation::multSTensor3FirstTranspose(F,F,FTF);
    E1(0,0) = 0.5*(FTF(0,0)-1.);
    E1(0,1) = 0.5*(FTF(1,1)-1.);
    E1(0,2) = 0.5*(FTF(2,2)-1.);
    E1(0,3) = 0.5*FTF(0,1);
    E1(0,4) = 0.5*FTF(0,2);
    E1(0,5) = 0.5*FTF(1,2);
    
    if(_NeedExtraNorm){
      const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();
      STensorOperation::multSTensor3FirstTranspose(F0,F0,FTF0);
      E0(0,0) = 0.5*(FTF0(0,0)-1.);
      E0(0,1) = 0.5*(FTF0(1,1)-1.);
      E0(0,2) = 0.5*(FTF0(2,2)-1.);
      E0(0,3) = 0.5*FTF0(0,1);
      E0(0,4) = 0.5*FTF0(0,2);
      E0(0,5) = 0.5*FTF0(1,2);    
    }   
  }
  else if (_kinematicInput == NonlocalDamageTorchANNBasedDG3DMaterialLaw::U)
  {
    STensorOperation::RUDecomposition(F,Ustretch,R);
    E1(0,0) = Ustretch(0,0)-1.;
    E1(0,1) = Ustretch(1,1)-1.;
    E1(0,2) = Ustretch(2,2)-1.;
    E1(0,3) = Ustretch(0,1);
    E1(0,4) = Ustretch(0,2);
    E1(0,5) = Ustretch(1,2);
  }
  else if (_kinematicInput == NonlocalDamageTorchANNBasedDG3DMaterialLaw::smallStrain)
  {
    E1(0,0) = F(0,0)-1.;
    E1(0,1) = F(1,1)-1.;
    E1(0,2) = F(2,2)-1.;
    E1(0,3) = 0.5*(F(0,1)+F(1,0));
    E1(0,4) = 0.5*(F(0,2)+F(2,0));
    E1(0,5) = 0.5*(F(1,2)+F(2,1));
    
   if(_NeedExtraNorm){
      const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();
      E0(0,0) = F0(0,0)-1.;
      E0(0,1) = F0(1,1)-1.;
      E0(0,2) = F0(2,2)-1.;
      E0(0,3) = 0.5*(F0(0,1)+F0(1,0));
      E0(0,4) = 0.5*(F0(0,2)+F0(2,0));
      E0(0,5) = 0.5*(F0(1,2)+F0(2,1));  
    }  
  }
  else if (_kinematicInput == NonlocalDamageTorchANNBasedDG3DMaterialLaw::EGLInc)
  {
    STensorOperation::multSTensor3FirstTranspose(F,F,FTF);
    const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();
    STensorOperation::multSTensor3FirstTranspose(F0,F0,FTF0);
    E1(0,0) = 0.5*(FTF(0,0)-FTF0(0,0));
    E1(0,1) = 0.5*(FTF(1,1)-FTF0(1,1));
    E1(0,2) = 0.5*(FTF(2,2)-FTF0(2,2));
    E1(0,3) = 0.5*(FTF(0,1)-FTF0(0,1));
    E1(0,4) = 0.5*(FTF(0,2)-FTF0(0,2));
    E1(0,5) = 0.5*(FTF(1,2)-FTF0(1,2));
  }
  else if (_kinematicInput == NonlocalDamageTorchANNBasedDG3DMaterialLaw::UInc)
  {
    STensorOperation::RUDecomposition(F,Ustretch,R);
    const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();
    STensorOperation::RUDecomposition(F0,Ustretch0,R0);
    E1(0,0) = Ustretch(0,0)-Ustretch0(0,0);
    E1(0,1) = Ustretch(1,1)-Ustretch0(1,1);
    E1(0,2) = Ustretch(2,2)-Ustretch0(2,2);
    E1(0,3) = Ustretch(0,1)-Ustretch0(0,1);
    E1(0,4) = Ustretch(0,2)-Ustretch0(0,2);
    E1(0,5) = Ustretch(1,2)-Ustretch0(1,2);
  }
  else if (_kinematicInput == NonlocalDamageTorchANNBasedDG3DMaterialLaw::smallStrainInc)
  {
    const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();
    E1(0,0) = F(0,0)-F0(0,0);
    E1(0,1) = F(1,1)-F0(1,1);
    E1(0,2) = F(2,2)-F0(2,2);
    E1(0,3) = 0.5*(F(0,1)+F(1,0)-F0(0,1)-F0(1,0));
    E1(0,4) = 0.5*(F(0,2)+F(2,0)-F0(0,2)-F0(2,0));
    E1(0,5) = 0.5*(F(1,2)+F(2,1)-F0(1,2)-F0(2,1));
  }
  else
  {
    Msg::Error("kinematic type %d has not been defined",_kinematicInput);
  }
  fullVector<double> E0Vector(6);
  for (int i=0;i<6;i++){
    E0Vector(i) = E0(0,i);
  }
  STensor3 E0Tensor;
  STensorOperation::fromFullVectorToSTensor3(E0Vector, E0Tensor);
  
  fullVector<double> E1Vector(6);
  for (int i=0;i<6;i++){
    E1Vector(i) = E1(0,i);
  }
  STensor3 E1Tensor;
  STensorOperation::fromFullVectorToSTensor3(E1Vector, E1Tensor);
  
  /*if (E1Tensor(0,0)>=0.04){
    std::cout << "Exx = " << E1Tensor(0,0) << "\n";   //we check if Exx becomes out of the RNN training range
  }*/

  //code with D0000 and Eplast00 as non local variables
  //beginning step 0: creation of the basis for Eplast and D
    //beginning step 0.1: declaration of the Tensors of the basis
      //beginning step 0.1.1: for the basis for D
  STensor43 A0;
  STensor43 A1;
  STensor43 A2;
  STensor43 A3;
  STensor43 A4;
  STensor43 A5;
  STensor43 A6;
  STensor43 A7;
  STensor43 A8;
  STensor43 A9;
  STensor43 A10;
  STensor43 A11;
  STensor43 A12;
  STensor43 A13;
  STensor43 A14;
  STensor43 A15;
  STensor43 A16;
  STensor43 A17;
  STensor43 A18;
  STensor43 A19;
  STensor43 A20;
      //end step 0.2
      
      //beginning step 0.1.2: for the basis for Eplast
  STensor3 B0;
  STensor3 B1;
  STensor3 B2;
  STensor3 B3;
  STensor3 B4;
  STensor3 B5;
    //end step 0.1
    
    //beginning step 0.2: computation of the two basis
      //beginning step 0.2.1: computation of the basis for D
  computation_basis_D(A0, A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11, A12, A13, A14, A15, A16, A17, A18, A19, A20);   
      //end step 0.2.1    
    
      //beginning step 0.2.2: computation of the basis for Eplast
  computation_basis_Eplast(B0, B1, B2, B3, B4, B5);
      //end step 0.2.2
    //end step 0.2
  //end step 0
  
  //beginning step 1: declaration of current variables
  STensor3 S;
  std::vector<double> _localVar(_numNonlocalVar);
  STensor43& NonDamTangPred = ipvcur->getRefToCel();
  STensor43 elTangPred;
  STensor3 Eplast; 
  STensor3 Eplast_l;
  STensor43 D_l;
  STensor43 Tangent;
  STensor63 dCalgdE;
  
  const STensor43& D0 = ipvprev->getConstRefToLocalDamageTensor43();
  STensor43& D = ipvcur->getRefToLocalDamageTensor43();
  
  const torch::Tensor& h_stress_0 = ipvprev->getConstRefToInternalVariables_stress();
  const torch::Tensor& h_elTang_0 = ipvprev->getConstRefToInternalVariables_elTang();
  const torch::Tensor& h_NonDamTang_0 = ipvprev->getConstRefToInternalVariables_NonDamTang();
  torch::Tensor& h_stress_1 = ipvcur->getRefToInternalVariables_stress(); 
  torch::Tensor& h_elTang_1 = ipvcur->getRefToInternalVariables_elTang(); 
  torch::Tensor& h_NonDamTang_1 = ipvcur->getRefToInternalVariables_NonDamTang(); 
  //end step 1  
  
  
  //beginning step 2: declaration of derivatives computed by perturbation or automatic differentiation
  STensor43& DEplastDE = ipvcur -> getRefToDEplastDE();                 //we can only compute it by perturbation because Eplast is not a value predicted by the RNN
  STensor63 DNonDamTangPredDE;         //only computed by perturbation for now
  STensor63 DDDE;                      //we can only compute it by perturbation because D is not a value predicted by the RNN 
  STensor43 DSDETensor43;              //only computed by perturbation for now    
  STensor63 DD_lDE;
  STensor43 DEplast_lDE;               
  static fullMatrix<double> DSDE(6,6); //only computed by automatic differentiation for now (but the automatic differentiation does not work yet)
  //end step 2 
  
  //beginning step 3: computation of RNN predictions and the values deduced by the RNN predictions (local damage D and plastic strain Eplast) and derivatives computed by perturbation or automatic differentiation
  if (_RNN){
    //perturbation case
    if(_tangentByPerturbation){
  
      //beginning step 3.1: we compute the RNN predictions at this time step and the values deduced by the RNN predictions (local damage D and plastic strain Eplast)
      RNNstress_stiff(E0Tensor, E1Tensor, h_stress_0, h_stress_1, h_elTang_0, h_elTang_1, h_NonDamTang_0, h_NonDamTang_1, S,NonDamTangPred,elTangPred, D, Eplast, false, DSDE, B0, B1, B2, B3, B4, B5, Eplast_l);
   
        //to have D always increasing
      for (int i=0;i<3;i++){
        for (int j=0;j<3;j++){
          for (int k=0;k<3;k++){
            for (int l=0;l<3;l++){
              if (D0(i,j,k,l)>D(i,j,k,l)){
                D(i,j,k,l) = D0(i,j,k,l);
              }
            }
          }
        }
      }
      //end step 3.1
      computation_D_l(A0, A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11, A12, A13, A14, A15, A16, A17, A18, A19, A20, D, D_l);  //computation of D_l
    
    
      if(stiff){
        //beginning step 3.2: declaration of the variables useful for the perturbation
        STensor3 F_plus;
        STensor3 Ft_plus;   //will be F_plus transposed
        STensor3 E1Tensor_plus;
        STensor3 I;
        STensorOperation::unity(I);  //unity tensor
        
        STensor3 S_plus;
        STensor43 NonDamTangPred_plus;
        STensor43 elTangPred_plus;
        STensor43 D_plus; 
        STensor43 D_l_plus;
        STensor3 Eplast_plus;
        STensor3 Eplast_l_plus;
    
        torch::Tensor h_stress_tmp = torch::zeros({1, 1, _numberOfInternalVariables_stress});
        torch::Tensor h_elTang_tmp = torch::zeros({1, 1, _numberOfInternalVariables_elTang});
        torch::Tensor h_NonDamTang_tmp = torch::zeros({1, 1, _numberOfInternalVariables_NonDamTang});
        //end step 3.2
    
    
        //beginning step 3.3: perturbation
        STensorOperation::zero(DEplastDE);
        STensorOperation::zero(DEplast_lDE);
        STensorOperation::zero(DDDE);
        STensorOperation::zero(DNonDamTangPredDE);
        STensorOperation::zero(DD_lDE);
        STensorOperation::zero(DSDETensor43);
        for (int k=0;k<3;k++){
          for (int l=0;l<3;l++){
            F_plus = F;
            F_plus(k,l) += _pertTol;
            /*E1Tensor_plus = E1Tensor;
            E1Tensor_plus(k,l) += _pertTol;
            if (k!=l){
              E1Tensor_plus(l,k) = E1Tensor_plus(k,l);
            }*/
            STensorOperation::transposeSTensor3(F_plus,Ft_plus);  //Ft_plus becomes F_plus transposed 
            for (int i=0;i<3;i++){
              for (int j=0;j<3;j++){
                E1Tensor_plus(i,j)=0.5*(F_plus(i,j)+Ft_plus(i,j))-I(i,j);
              }
            }
            RNNstress_stiff(E0Tensor, E1Tensor_plus, h_stress_0, h_stress_tmp, h_elTang_0, h_elTang_tmp, h_NonDamTang_0, h_NonDamTang_tmp, S_plus,NonDamTangPred_plus,elTangPred_plus, D_plus, Eplast_plus, false, DSDE, B0, B1, B2, B3, B4, B5, Eplast_l_plus);
          
         
            //beginning step 3.3.1: computation of DEplastDE and DEplast_lDE
            for (int i=0;i<3;i++){
              for (int j=0;j<3;j++){
                if (_pertTol>0){
                  DEplastDE(i,j,k,l)+=0.25*(Eplast_plus(i,j) - Eplast(i,j))/_pertTol;
                  DEplastDE(j,i,k,l)+=0.25*(Eplast_plus(i,j) - Eplast(i,j))/_pertTol;
                  DEplastDE(i,j,l,k)+=0.25*(Eplast_plus(i,j) - Eplast(i,j))/_pertTol;
                  DEplastDE(j,i,l,k)+=0.25*(Eplast_plus(i,j) - Eplast(i,j))/_pertTol;
                
                  DEplast_lDE(i,j,k,l)+=0.25*(Eplast_l_plus(i,j) - Eplast_l(i,j))/_pertTol;
                  DEplast_lDE(j,i,k,l)+=0.25*(Eplast_l_plus(i,j) - Eplast_l(i,j))/_pertTol;
                  DEplast_lDE(i,j,l,k)+=0.25*(Eplast_l_plus(i,j) - Eplast_l(i,j))/_pertTol;
                  DEplast_lDE(j,i,l,k)+=0.25*(Eplast_l_plus(i,j) - Eplast_l(i,j))/_pertTol;                
                }
              }
            }
            //end step 3.3.1
          
            //beginning step 3.3.2: to have always D increasing
            for (int i=0;i<3;i++){
              for (int j=0;j<3;j++){
                for (int c=0;c<3;c++){
                  for (int d=0;d<3;d++){
                    if (D0(i,j,c,d)>D_plus(i,j,c,d)){
                      D_plus(i,j,c,d) = D0(i,j,c,d);
                    }
                  }
                }
              }
            }
            //end step 3.3.2

            //beginning step 3.3.3: computation of DNonDamTangPredDE, DD_lDE, DDDE
            computation_D_l(A0, A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11, A12, A13, A14, A15, A16, A17, A18, A19, A20, D_plus, D_l_plus);  //computation of D_l_plus
            for (int i=0;i<3;i++){
              for (int j=0;j<3;j++){
                for (int c=0;c<3;c++){
                  for (int d=0;d<3;d++){
                    if (_pertTol>0){
                      DNonDamTangPredDE(i,j,c,d,k,l)+= 0.125*(NonDamTangPred_plus(i,j,c,d) - NonDamTangPred(i,j,c,d))/_pertTol;  
                      DNonDamTangPredDE(j,i,c,d,k,l)+= 0.125*(NonDamTangPred_plus(i,j,c,d) - NonDamTangPred(i,j,c,d))/_pertTol;  
                      DNonDamTangPredDE(i,j,d,c,k,l)+= 0.125*(NonDamTangPred_plus(i,j,c,d) - NonDamTangPred(i,j,c,d))/_pertTol;  
                      DNonDamTangPredDE(j,i,d,c,k,l)+= 0.125*(NonDamTangPred_plus(i,j,c,d) - NonDamTangPred(i,j,c,d))/_pertTol;  
                      DNonDamTangPredDE(i,j,c,d,l,k)+= 0.125*(NonDamTangPred_plus(i,j,c,d) - NonDamTangPred(i,j,c,d))/_pertTol;  
                      DNonDamTangPredDE(j,i,c,d,l,k)+= 0.125*(NonDamTangPred_plus(i,j,c,d) - NonDamTangPred(i,j,c,d))/_pertTol;  
                      DNonDamTangPredDE(i,j,d,c,l,k)+= 0.125*(NonDamTangPred_plus(i,j,c,d) - NonDamTangPred(i,j,c,d))/_pertTol;  
                      DNonDamTangPredDE(j,i,d,c,l,k)+= 0.125*(NonDamTangPred_plus(i,j,c,d) - NonDamTangPred(i,j,c,d))/_pertTol;                 
                  
                      DD_lDE(i,j,c,d,k,l)+=0.125*(D_l_plus(i,j,c,d) - D_l(i,j,c,d))/_pertTol;
                      DD_lDE(j,i,c,d,k,l)+=0.125*(D_l_plus(i,j,c,d) - D_l(i,j,c,d))/_pertTol;
                      DD_lDE(i,j,d,c,k,l)+=0.125*(D_l_plus(i,j,c,d) - D_l(i,j,c,d))/_pertTol;
                      DD_lDE(j,i,d,c,k,l)+=0.125*(D_l_plus(i,j,c,d) - D_l(i,j,c,d))/_pertTol;
                      DD_lDE(i,j,c,d,l,k)+=0.125*(D_l_plus(i,j,c,d) - D_l(i,j,c,d))/_pertTol;
                      DD_lDE(j,i,c,d,l,k)+=0.125*(D_l_plus(i,j,c,d) - D_l(i,j,c,d))/_pertTol;
                      DD_lDE(i,j,d,c,l,k)+=0.125*(D_l_plus(i,j,c,d) - D_l(i,j,c,d))/_pertTol;
                      DD_lDE(j,i,d,c,l,k)+=0.125*(D_l_plus(i,j,c,d) - D_l(i,j,c,d))/_pertTol;                  
                  
                      DDDE(i,j,c,d,k,l)+=0.125*(D_plus(i,j,c,d) - D(i,j,c,d))/_pertTol;
                      DDDE(j,i,c,d,k,l)+=0.125*(D_plus(i,j,c,d) - D(i,j,c,d))/_pertTol;
                      DDDE(i,j,d,c,k,l)+=0.125*(D_plus(i,j,c,d) - D(i,j,c,d))/_pertTol;
                      DDDE(j,i,d,c,k,l)+=0.125*(D_plus(i,j,c,d) - D(i,j,c,d))/_pertTol;
                      DDDE(i,j,c,d,l,k)+=0.125*(D_plus(i,j,c,d) - D(i,j,c,d))/_pertTol;
                      DDDE(j,i,c,d,l,k)+=0.125*(D_plus(i,j,c,d) - D(i,j,c,d))/_pertTol;
                      DDDE(i,j,d,c,l,k)+=0.125*(D_plus(i,j,c,d) - D(i,j,c,d))/_pertTol;
                      DDDE(j,i,d,c,l,k)+=0.125*(D_plus(i,j,c,d) - D(i,j,c,d))/_pertTol;
                    }
                  }
                }
              }
            }          
            //end step 3.3.3
          
          
            //beginning step 3.3.4: computation of DSDETensor43
            for (int i=0;i<3;i++){
              for (int j=0;j<3;j++){
                if (_pertTol>0){
                  DSDETensor43(i,j,k,l) += 0.25*(S_plus(i,j) - S(i,j))/_pertTol;   
                  DSDETensor43(j,i,k,l) += 0.25*(S_plus(i,j) - S(i,j))/_pertTol;  
                  DSDETensor43(i,j,l,k) += 0.25*(S_plus(i,j) - S(i,j))/_pertTol;  
                  DSDETensor43(j,i,l,k) += 0.25*(S_plus(i,j) - S(i,j))/_pertTol;  
                }
              }
            }           
            //end step 3.3.4
          }
        }
        //end step 3.3
      }
    }else{   //be careful: it is the differentiation automatic case that does not work for now
      RNNstress_stiff(E0Tensor, E1Tensor, h_stress_0, h_stress_1, h_elTang_0, h_elTang_1, h_NonDamTang_0, h_NonDamTang_1, S,NonDamTangPred,elTangPred, D, Eplast, true, DSDE, B0, B1, B2, B3, B4, B5, Eplast_l);
    }
  }
  //end step 3
  else
  {
    const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();
    const IPVariable* qMeca0 = &(ipvprev->getConstRefToIpMeca());
    IPVariable* qMecan = &(ipvcur->getRefToIpMeca());
    STensor43* elasticTangent = &elTangPred;
    STensor63* dCalgdeps = &dCalgdE;
    
    //beginning step 3.1 bis: we compute the values at this time step and the values deduced (local damage D and plastic strain Eplast)
    _mecalocalLaw->constitutive(F0, F, S, qMeca0, qMecan, Tangent, stiff, elasticTangent, true, dCalgdeps);
    NonDamTangPred=elasticStiffness;
    
    computation_D(NonDamTangPred, *elasticTangent, D);  //computation of D
      //to have D always increasing
    for (int i=0;i<3;i++){
      for (int j=0;j<3;j++){
        for (int k=0;k<3;k++){
          for (int l=0;l<3;l++){
            if (D0(i,j,k,l)>D(i,j,k,l)){
              D(i,j,k,l) = D0(i,j,k,l);
            }
          }
        }
      }
    }
    computation_D_l(A0, A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11, A12, A13, A14, A15, A16, A17, A18, A19, A20, D, D_l);  //computation of D_l
    
    computation_Eplast(E1Tensor, S, *elasticTangent, Eplast);  //computation of Eplast
    computation_Eplast_l(B0, B1, B2, B3, B4, B5, Eplast, Eplast_l); //computation of Eplast_l
    //end step 3.1 bis
    
    if (stiff){
      //beginning step 3.2 bis: declaration of the variables useful for the perturbation
      STensor3 F_plus;
      STensor3 Ft_plus;   //will be F_plus transposed
      STensor3 E1Tensor_plus;
      STensor3 I;
      STensorOperation::unity(I);  //unity tensor
      
      STensor3 S_plus;
      IPVariable* qMeca_plus= qMecan->clone();
      STensor43 Tangent_plus;
      STensor43 elTang_plus;
      STensor43* elasticTangent_plus=&elTang_plus;
      STensor43 D_plus; 
      STensor43 D_l_plus;
      STensor3 Eplast_plus;
      STensor3 Eplast_l_plus;
      //end step 3.2 bis
    
      //beginning step 3.3 bis: perturbation
      STensorOperation::zero(DEplastDE);
      STensorOperation::zero(DEplast_lDE);
      STensorOperation::zero(DDDE);
      STensorOperation::zero(DD_lDE);
      STensorOperation::zero(DSDETensor43);
      STensor3 DE_pert;
      STensor43 DEDE_pert;
      STensorOperation::zero(DE_pert);
      STensorOperation::zero(DEDE_pert);
      for (int k=0;k<3;k++){
        for (int l=0;l<3;l++){
          F_plus = F;
          F_plus(k,l) += _pertTol;
          *qMeca_plus=*qMeca0;
          _mecalocalLaw->constitutive(F0, F_plus, S_plus, qMeca0, qMeca_plus, Tangent_plus, stiff, elasticTangent_plus, false, dCalgdeps);
          
          STensorOperation::transposeSTensor3(F_plus,Ft_plus);  //Ft_plus becomes F_plus transposed 
          for (int i=0;i<3;i++){
            for (int j=0;j<3;j++){
              E1Tensor_plus(i,j)=0.5*(F_plus(i,j)+Ft_plus(i,j))-I(i,j);
            }
          }
          
          //beginning step 3.3.0 bis: computation of Eplast_plus and Eplast_l_plus
          computation_Eplast(E1Tensor_plus, S_plus, *elasticTangent_plus, Eplast_plus);
          //DE_pert=E1Tensor_plus;
          computation_Eplast_l(B0, B1, B2, B3, B4, B5, Eplast_plus, Eplast_l_plus); 
          //end step 3.3.0 bis
          
          //beginning step 3.3.1 bis: computation of DEplastDE and DEplast_lDE
          for (int i=0;i<3;i++){
            for (int j=0;j<3;j++){
              if (_pertTol>0){
                DEplastDE(i,j,k,l)+=0.25*(Eplast_plus(i,j) - Eplast(i,j))/_pertTol;
                DEplastDE(j,i,k,l)+=0.25*(Eplast_plus(i,j) - Eplast(i,j))/_pertTol;
                DEplastDE(i,j,l,k)+=0.25*(Eplast_plus(i,j) - Eplast(i,j))/_pertTol;
                DEplastDE(j,i,l,k)+=0.25*(Eplast_plus(i,j) - Eplast(i,j))/_pertTol;
                
                DEplast_lDE(i,j,k,l)+=0.25*(Eplast_l_plus(i,j) - Eplast_l(i,j))/_pertTol;
                DEplast_lDE(j,i,k,l)+=0.25*(Eplast_l_plus(i,j) - Eplast_l(i,j))/_pertTol;
                DEplast_lDE(i,j,l,k)+=0.25*(Eplast_l_plus(i,j) - Eplast_l(i,j))/_pertTol;
                DEplast_lDE(j,i,l,k)+=0.25*(Eplast_l_plus(i,j) - Eplast_l(i,j))/_pertTol;
  
                //DEDE_pert(i,j,k,l)+=0.25*(DE_pert(i,j) - E1Tensor(i,j))/_pertTol;
                //DEDE_pert(j,i,k,l)+=0.25*(DE_pert(i,j) - E1Tensor(i,j))/_pertTol;
                //DEDE_pert(i,j,l,k)+=0.25*(DE_pert(i,j) - E1Tensor(i,j))/_pertTol;
                //DEDE_pert(i,j,l,k)+=0.25*(DE_pert(i,j) - E1Tensor(i,j))/_pertTol;
                
              }
            }
          }
          //end step 3.3.1 bis
          
          //beginning step 3.3.2 bis: computation of D_plus
          computation_D(NonDamTangPred, *elasticTangent_plus, D_plus); 
            //to have always D increasing
          for (int i=0;i<3;i++){
            for (int j=0;j<3;j++){
              for (int c=0;c<3;c++){
                for (int d=0;d<3;d++){
                  if (D0(i,j,c,d)>D_plus(i,j,c,d)){
                    D_plus(i,j,c,d) = D0(i,j,c,d);
                  }
                }
              }
            }
          }
          //end step 3.3.2 bis
          
          //beginning step 3.3.3 bis: computation of DD_lDE, DDDE
          computation_D_l(A0, A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11, A12, A13, A14, A15, A16, A17, A18, A19, A20, D_plus, D_l_plus);  //computation of D_l_plus
          for (int i=0;i<3;i++){
            for (int j=0;j<3;j++){
              for (int c=0;c<3;c++){
                for (int d=0;d<3;d++){
                  if (_pertTol>0){
                    DD_lDE(i,j,c,d,k,l)+=0.125*(D_l_plus(i,j,c,d) - D_l(i,j,c,d))/_pertTol;
                    DD_lDE(j,i,c,d,k,l)+=0.125*(D_l_plus(i,j,c,d) - D_l(i,j,c,d))/_pertTol;
                    DD_lDE(i,j,d,c,k,l)+=0.125*(D_l_plus(i,j,c,d) - D_l(i,j,c,d))/_pertTol;
                    DD_lDE(j,i,d,c,k,l)+=0.125*(D_l_plus(i,j,c,d) - D_l(i,j,c,d))/_pertTol;
                    DD_lDE(i,j,c,d,l,k)+=0.125*(D_l_plus(i,j,c,d) - D_l(i,j,c,d))/_pertTol;
                    DD_lDE(j,i,c,d,l,k)+=0.125*(D_l_plus(i,j,c,d) - D_l(i,j,c,d))/_pertTol;
                    DD_lDE(i,j,d,c,l,k)+=0.125*(D_l_plus(i,j,c,d) - D_l(i,j,c,d))/_pertTol;
                    DD_lDE(j,i,d,c,l,k)+=0.125*(D_l_plus(i,j,c,d) - D_l(i,j,c,d))/_pertTol;                  
                  
                    DDDE(i,j,c,d,k,l)+=0.125*(D_plus(i,j,c,d) - D(i,j,c,d))/_pertTol;
                    DDDE(j,i,c,d,k,l)+=0.125*(D_plus(i,j,c,d) - D(i,j,c,d))/_pertTol;
                    DDDE(i,j,d,c,k,l)+=0.125*(D_plus(i,j,c,d) - D(i,j,c,d))/_pertTol;
                    DDDE(j,i,d,c,k,l)+=0.125*(D_plus(i,j,c,d) - D(i,j,c,d))/_pertTol;
                    DDDE(i,j,c,d,l,k)+=0.125*(D_plus(i,j,c,d) - D(i,j,c,d))/_pertTol;
                    DDDE(j,i,c,d,l,k)+=0.125*(D_plus(i,j,c,d) - D(i,j,c,d))/_pertTol;
                    DDDE(i,j,d,c,l,k)+=0.125*(D_plus(i,j,c,d) - D(i,j,c,d))/_pertTol;
                    DDDE(j,i,d,c,l,k)+=0.125*(D_plus(i,j,c,d) - D(i,j,c,d))/_pertTol;
                  }
                }
              }
            }
          }
          //end step 3.3.3 bis
          
          //beginning step 3.3.4 bis: computation of DSDETensor43
          for (int i=0;i<3;i++){
            for (int j=0;j<3;j++){
              if (_pertTol>0){
                DSDETensor43(i,j,k,l) += 0.25*(S_plus(i,j) - S(i,j))/_pertTol;   
                DSDETensor43(j,i,k,l) += 0.25*(S_plus(i,j) - S(i,j))/_pertTol;  
                DSDETensor43(i,j,l,k) += 0.25*(S_plus(i,j) - S(i,j))/_pertTol;  
                DSDETensor43(j,i,l,k) += 0.25*(S_plus(i,j) - S(i,j))/_pertTol;  
              }
            }
          }          
          //end step 3.3.4
        }
      } 
      //end step 3 bis 
    delete(qMeca_plus);
    //STensor43 DEDE;
    //STensorOperation::unity(DEDE);  //I4 (with 0.5)

    //STensor43 Delta=DEDE;
    //Delta-=DEDE_pert;
    //Delta.print("Delta");
    }
  } 
  
  //beginning step 4: computation of the reconstructed stress   
  STensor3 S_rec;   //the reconstructed stress
  STensor43 D_nl;
  STensor43 D_rec;
  STensor3 Eplast_nl;
  STensor3 Eplast_rec;
  computation_D_nl__Eplast_nl(A0, A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11, A12, A13, A14, A15, A16, A17, A18, A19, A20, B0, B1, B2, B3, B4, B5, D, Eplast, Eplast_nl, D_nl, ipvcur);
  D_rec = D_nl+D_l;
  Eplast_rec = Eplast_nl+Eplast_l;
  
  for (int u=0;u<3;u++){
    for (int v=0;v<3;v++){
      S_rec(u,v)=0;
      for (int p=0;p<3;p++){
        for (int q=0;q<3;q++){
          S_rec(u,v)+=(1-D_rec(u,v,p,q))*NonDamTangPred(u,v,p,q)*(E1Tensor(p,q)-Eplast_rec(p,q));
        }
      }
    }
  }
  //end step 4
  
  
  //beginning step 5: declaration of the derivatives
  STensor43 DEDE;
  STensorOperation::unity(DEDE);  //I4 (with 0.5)
  STensor43 DS_nlDE;
  STensor43 DS_nlDE1;
  STensor43 DS_nlDE2;
  STensor43 DS_nlDE3;
  //end step 5
  
  
  if(stiff){
    //beginning step 6: computation of DS_nlDE
    for (int u=0;u<3;u++){
      for (int v=0;v<3;v++){
        for (int m=0;m<3;m++){
          for (int n=0;n<3;n++){
            //DS_nlDE(u,v,m,n)=0.;
            DS_nlDE1(u,v,m,n)=0.;
            DS_nlDE2(u,v,m,n)=0.;
            DS_nlDE3(u,v,m,n)=0.;
            for (int p=0;p<3;p++){
              for (int q=0;q<3;q++){
                DS_nlDE1(u,v,m,n)=DS_nlDE1(u,v,m,n)-DD_lDE(u,v,p,q,m,n)*NonDamTangPred(u,v,p,q)*(E1Tensor(p,q)-Eplast_rec(p,q));
                DS_nlDE2(u,v,m,n)=DS_nlDE2(u,v,m,n)+(1-D_rec(u,v,p,q))*DNonDamTangPredDE(u,v,p,q,m,n)*(E1Tensor(p,q)-Eplast_rec(p,q));
                DS_nlDE3(u,v,m,n)=DS_nlDE3(u,v,m,n)+(1-D_rec(u,v,p,q))*NonDamTangPred(u,v,p,q)*(DEDE(p,q,m,n)-DEplast_lDE(p,q,m,n));
                /*DS_nlDE(u,v,m,n)=DS_nlDE(u,v,m,n)-DD_lDE(u,v,p,q,m,n)*NonDamTangPred(u,v,p,q)*(E1Tensor(p,q)-Eplast_rec(p,q))
                                                 +(1-D_rec(u,v,p,q))*DNonDamTangPredDE(u,v,p,q,m,n)*(E1Tensor(p,q)-Eplast_rec(p,q))
                                                 +(1-D_rec(u,v,p,q))*NonDamTangPred(u,v,p,q)*(DEDE(p,q,m,n)-DEplast_lDE(p,q,m,n));*/
              }
            }
          }
        }
      }
    }
    DS_nlDE=DS_nlDE1+DS_nlDE2+DS_nlDE3;
    //end step 6
  }
  
  //beginning step 7: we retrieve the values in the internal variables
    //beginning step 7.1: we retrieve the plastic strain
  ipvcur->getRefToEplast()=Eplast;
    //end step 7.1
    
    //beginning step 7.2: we retrieve the plastic strain reconstructed
    ipvcur->getRefToEplastrec()=Eplast_rec;
    //end step 7.2
  //end step 7
  
  //beginning step 8: we retrieve the computed values in the current internal variables
    //small strain case (the only one that works for now)
  if (_kinematicInput == smallStrain || _kinematicInput== smallStrainInc  )
  {
    //beginning step 8.1: we retrieve the stress
    STensor3& P = ipvcur->getRefToFirstPiolaKirchhoffStress();
    P=S_rec; 
    //end step 8.1
    
    //beginning step 8.1.5: we compute the derivatives even if we do not retrieve them yet
    std::vector<STensor3> M(_numNonlocalVar);
    std::vector<STensor3> K(_numNonlocalVar);
    int idx=0;
    if (_Id0000==1){
      for (int k=0;k<3;k++){
        for (int l=0;l<3;l++){
          K[idx](k,l)=DDDE(0,0,0,0,k,l);
          M[idx](k,l)=0;
          for (int p=0;p<3;p++){
            for (int q=0;q<3;q++){
              M[idx](k,l)-=A0(k,l,p,q)*NonDamTangPred(k,l,p,q)*(E1Tensor(p,q)-Eplast_rec(p,q));
            }
          }
        }
      }
      idx+=1;
    }  
    if (_Id0100==1){
      for (int k=0;k<3;k++){
        for (int l=0;l<3;l++){
          K[idx](k,l)=DDDE(0,1,0,0,k,l);
          M[idx](k,l)=0;
          for (int p=0;p<3;p++){
            for (int q=0;q<3;q++){
              M[idx](k,l)-=A1(k,l,p,q)*NonDamTangPred(k,l,p,q)*(E1Tensor(p,q)-Eplast_rec(p,q));
            }
          }
        }
      }
      idx+=1;
    }
    if (_Id0200==1){
      for (int k=0;k<3;k++){
        for (int l=0;l<3;l++){
          K[idx](k,l)=DDDE(0,2,0,0,k,l);
          M[idx](k,l)=0;
          for (int p=0;p<3;p++){
            for (int q=0;q<3;q++){
              M[idx](k,l)-=A2(k,l,p,q)*NonDamTangPred(k,l,p,q)*(E1Tensor(p,q)-Eplast_rec(p,q));
            }
          }
        }
      }
      idx+=1;
    }  
    if (_Id1100==1){
      for (int k=0;k<3;k++){
        for (int l=0;l<3;l++){
          K[idx](k,l)=DDDE(1,1,0,0,k,l);
          M[idx](k,l)=0;
          for (int p=0;p<3;p++){
            for (int q=0;q<3;q++){
              M[idx](k,l)-=A3(k,l,p,q)*NonDamTangPred(k,l,p,q)*(E1Tensor(p,q)-Eplast_rec(p,q));
            }
          }
        }
      }
      idx+=1;
    }
    if (_Id1200==1){
      for (int k=0;k<3;k++){
        for (int l=0;l<3;l++){
          K[idx](k,l)=DDDE(1,2,0,0,k,l);
          M[idx](k,l)=0;
          for (int p=0;p<3;p++){
            for (int q=0;q<3;q++){
              M[idx](k,l)-=A4(k,l,p,q)*NonDamTangPred(k,l,p,q)*(E1Tensor(p,q)-Eplast_rec(p,q));
            }
          }
        }
      }
      idx+=1;
    } 
    if (_Id2200==1){
      for (int k=0;k<3;k++){
        for (int l=0;l<3;l++){
          K[idx](k,l)=DDDE(2,2,0,0,k,l);
          M[idx](k,l)=0;
          for (int p=0;p<3;p++){
            for (int q=0;q<3;q++){
              M[idx](k,l)-=A5(k,l,p,q)*NonDamTangPred(k,l,p,q)*(E1Tensor(p,q)-Eplast_rec(p,q));
            }
          }
        }
      }
      idx+=1;
    }  
    if (_Id0101==1){
      for (int k=0;k<3;k++){
        for (int l=0;l<3;l++){
          K[idx](k,l)=DDDE(0,1,0,1,k,l);
          M[idx](k,l)=0;
          for (int p=0;p<3;p++){
            for (int q=0;q<3;q++){
              M[idx](k,l)-=A6(k,l,p,q)*NonDamTangPred(k,l,p,q)*(E1Tensor(p,q)-Eplast_rec(p,q));
            }
          }
        }
      }
      idx+=1;
    } 
    if (_Id0201==1){
      for (int k=0;k<3;k++){
        for (int l=0;l<3;l++){
          K[idx](k,l)=DDDE(0,2,0,1,k,l);
          M[idx](k,l)=0;
          for (int p=0;p<3;p++){
            for (int q=0;q<3;q++){
              M[idx](k,l)-=A7(k,l,p,q)*NonDamTangPred(k,l,p,q)*(E1Tensor(p,q)-Eplast_rec(p,q));
            }
          }
        }
      }
      idx+=1;
    }   
    if (_Id1101==1){
      for (int k=0;k<3;k++){
        for (int l=0;l<3;l++){
          K[idx](k,l)=DDDE(1,1,0,1,k,l);
          M[idx](k,l)=0;
          for (int p=0;p<3;p++){
            for (int q=0;q<3;q++){
              M[idx](k,l)-=A8(k,l,p,q)*NonDamTangPred(k,l,p,q)*(E1Tensor(p,q)-Eplast_rec(p,q));
            }
          }
        }
      }
      idx+=1;
    } 
    if (_Id1201==1){
      for (int k=0;k<3;k++){
        for (int l=0;l<3;l++){
          K[idx](k,l)=DDDE(1,2,0,1,k,l);
          M[idx](k,l)=0;
          for (int p=0;p<3;p++){
            for (int q=0;q<3;q++){
              M[idx](k,l)-=A9(k,l,p,q)*NonDamTangPred(k,l,p,q)*(E1Tensor(p,q)-Eplast_rec(p,q));
            }
          }
        }
      }
      idx+=1;
    }   
    if (_Id2201==1){
      for (int k=0;k<3;k++){
        for (int l=0;l<3;l++){
          K[idx](k,l)=DDDE(2,2,0,1,k,l);
          M[idx](k,l)=0;
          for (int p=0;p<3;p++){
            for (int q=0;q<3;q++){
              M[idx](k,l)-=A10(k,l,p,q)*NonDamTangPred(k,l,p,q)*(E1Tensor(p,q)-Eplast_rec(p,q));
            }
          }
        }
      }
      idx+=1;
    }  
    if (_Id0202==1){
      for (int k=0;k<3;k++){
        for (int l=0;l<3;l++){
          K[idx](k,l)=DDDE(0,2,0,2,k,l);
          M[idx](k,l)=0;
          for (int p=0;p<3;p++){
            for (int q=0;q<3;q++){
              M[idx](k,l)-=A11(k,l,p,q)*NonDamTangPred(k,l,p,q)*(E1Tensor(p,q)-Eplast_rec(p,q));
            }
          }
        }
      }
      idx+=1;
    } 
    if (_Id1102==1){
      for (int k=0;k<3;k++){
        for (int l=0;l<3;l++){
          K[idx](k,l)=DDDE(1,1,0,2,k,l);
          M[idx](k,l)=0;
          for (int p=0;p<3;p++){
            for (int q=0;q<3;q++){
              M[idx](k,l)-=A12(k,l,p,q)*NonDamTangPred(k,l,p,q)*(E1Tensor(p,q)-Eplast_rec(p,q));
            }
          }
        }
      }
      idx+=1;
    } 
    if (_Id1202==1){
      for (int k=0;k<3;k++){
        for (int l=0;l<3;l++){
          K[idx](k,l)=DDDE(1,2,0,2,k,l);
          M[idx](k,l)=0;
          for (int p=0;p<3;p++){
            for (int q=0;q<3;q++){
              M[idx](k,l)-=A13(k,l,p,q)*NonDamTangPred(k,l,p,q)*(E1Tensor(p,q)-Eplast_rec(p,q));
            }
          }
        }
      }
      idx+=1;
    } 
    if (_Id2202==1){
      for (int k=0;k<3;k++){
        for (int l=0;l<3;l++){
          K[idx](k,l)=DDDE(2,2,0,2,k,l);
          M[idx](k,l)=0;
          for (int p=0;p<3;p++){
            for (int q=0;q<3;q++){
              M[idx](k,l)-=A14(k,l,p,q)*NonDamTangPred(k,l,p,q)*(E1Tensor(p,q)-Eplast_rec(p,q));
            }
          }
        }
      }
      idx+=1;
    } 
    if (_Id1111==1){
      for (int k=0;k<3;k++){
        for (int l=0;l<3;l++){
          K[idx](k,l)=DDDE(1,1,1,1,k,l);
          M[idx](k,l)=0;
          for (int p=0;p<3;p++){
            for (int q=0;q<3;q++){
              M[idx](k,l)-=A15(k,l,p,q)*NonDamTangPred(k,l,p,q)*(E1Tensor(p,q)-Eplast_rec(p,q));
            }
          }
        }
      }
      idx+=1;
    } 
    if (_Id1211==1){
      for (int k=0;k<3;k++){
        for (int l=0;l<3;l++){
          K[idx](k,l)=DDDE(1,2,1,1,k,l);
          M[idx](k,l)=0;
          for (int p=0;p<3;p++){
            for (int q=0;q<3;q++){
              M[idx](k,l)-=A16(k,l,p,q)*NonDamTangPred(k,l,p,q)*(E1Tensor(p,q)-Eplast_rec(p,q));
            }
          }
        }
      }
      idx+=1;
    } 
    if (_Id2211==1){
      for (int k=0;k<3;k++){
        for (int l=0;l<3;l++){
          K[idx](k,l)=DDDE(2,2,1,1,k,l);
          M[idx](k,l)=0;
          for (int p=0;p<3;p++){
            for (int q=0;q<3;q++){
              M[idx](k,l)-=A17(k,l,p,q)*NonDamTangPred(k,l,p,q)*(E1Tensor(p,q)-Eplast_rec(p,q));
            }
          }
        }
      }
      idx+=1;
    } 
    if (_Id1212==1){
      for (int k=0;k<3;k++){
        for (int l=0;l<3;l++){
          K[idx](k,l)=DDDE(1,2,1,2,k,l);
          M[idx](k,l)=0;
          for (int p=0;p<3;p++){
            for (int q=0;q<3;q++){
              M[idx](k,l)-=A18(k,l,p,q)*NonDamTangPred(k,l,p,q)*(E1Tensor(p,q)-Eplast_rec(p,q));
            }
          }
        }
      }
      idx+=1;
    } 
    if (_Id2212==1){
      for (int k=0;k<3;k++){
        for (int l=0;l<3;l++){
          K[idx](k,l)=DDDE(2,2,1,2,k,l);
          M[idx](k,l)=0;
          for (int p=0;p<3;p++){
            for (int q=0;q<3;q++){
              M[idx](k,l)-=A19(k,l,p,q)*NonDamTangPred(k,l,p,q)*(E1Tensor(p,q)-Eplast_rec(p,q));
            }
          }
        }
      }
      idx+=1;
    } 
    if (_Id2222==1){
      for (int k=0;k<3;k++){
        for (int l=0;l<3;l++){
          K[idx](k,l)=DDDE(2,2,2,2,k,l);
          M[idx](k,l)=0;
          for (int p=0;p<3;p++){
            for (int q=0;q<3;q++){
              M[idx](k,l)-=A20(k,l,p,q)*NonDamTangPred(k,l,p,q)*(E1Tensor(p,q)-Eplast_rec(p,q));
            }
          }
        }
      }
      idx+=1;
    } 
    if (_Ie00==1){
      for (int k=0;k<3;k++){
        for (int l=0;l<3;l++){
          K[idx](k,l) = DEplastDE(0,0,k,l);
          M[idx](k,l)=0.;
          for (int p=0;p<3;p++){
            for (int q=0;q<3;q++){
              M[idx](k,l)-=(1-D_rec(k,l,p,q))*NonDamTangPred(k,l,p,q)*B0(p,q);
            }
          }
        }
      }
      idx+=1; 
    } 
    if (_Ie01==1){
      for (int k=0;k<3;k++){
        for (int l=0;l<3;l++){
          K[idx](k,l) = DEplastDE(0,1,k,l);
          M[idx](k,l)=0.;
          for (int p=0;p<3;p++){
            for (int q=0;q<3;q++){
              M[idx](k,l)-=(1-D_rec(k,l,p,q))*NonDamTangPred(k,l,p,q)*B1(p,q);
            }
          }
        }
      }
      idx+=1; 
    } 
    if (_Ie02==1){
      for (int k=0;k<3;k++){
        for (int l=0;l<3;l++){
          K[idx](k,l) = DEplastDE(0,2,k,l);
          M[idx](k,l)=0.;
          for (int p=0;p<3;p++){
            for (int q=0;q<3;q++){
              M[idx](k,l)-=(1-D_rec(k,l,p,q))*NonDamTangPred(k,l,p,q)*B2(p,q);
            }
          }
        }
      }
      idx+=1; 
    }
    if (_Ie11==1){
      for (int k=0;k<3;k++){
        for (int l=0;l<3;l++){
          K[idx](k,l) = DEplastDE(1,1,k,l);
          M[idx](k,l)=0.;
          for (int p=0;p<3;p++){
            for (int q=0;q<3;q++){
              M[idx](k,l)-=(1-D_rec(k,l,p,q))*NonDamTangPred(k,l,p,q)*B3(p,q);
            }
          }
        }
      }
      idx+=1; 
    }
    if (_Ie12==1){
      for (int k=0;k<3;k++){
        for (int l=0;l<3;l++){
          K[idx](k,l) = DEplastDE(1,2,k,l);
          M[idx](k,l)=0.;
          for (int p=0;p<3;p++){
            for (int q=0;q<3;q++){
              M[idx](k,l)-=(1-D_rec(k,l,p,q))*NonDamTangPred(k,l,p,q)*B4(p,q);
            }
          }
        }
      }
      idx+=1; 
    }
    if (_Ie22==1){
      for (int k=0;k<3;k++){
        for (int l=0;l<3;l++){
          K[idx](k,l) = DEplastDE(2,2,k,l);
          M[idx](k,l)=0.;
          for (int p=0;p<3;p++){
            for (int q=0;q<3;q++){
              M[idx](k,l)-=(1-D_rec(k,l,p,q))*NonDamTangPred(k,l,p,q)*B5(p,q);
            }
          }
        }
      }
      idx+=1; 
    }  
    //end step 8.1.5
    
    //beginning step 8.2: we retrieve the derivatives only if they are not computed by perturbation out of the code
    if (stiff){    
      
      fullMatrix<double> & DlocalToDnonlocalVar = ipvcur->getRefToDLocalVariableDNonLocalVariable();
      DlocalToDnonlocalVar(0,0) = 0.0;
          
      STensor43& L = ipvcur->getRefToTangentModuli();
      STensor43 DEDF;
      STensorOperation::unity(DEDF);  //I4 (with 0.5)
      for (int i=0;i<3;i++){
        for (int j=0;j<3;j++){
          for (int k=0;k<3;k++){
            for (int l=0;l<3;l++){
              L(i,j,k,l)=0.;
              for (int c=0;c<3;c++){
                for (int d=0;d<3;d++){
                  L(i,j,k,l)+=DS_nlDE(i,j,c,d)*DEDF(c,d,k,l);
                }
              }
            }
          }
        }
      }  //L is DS_nlDF
      //L=DS_nlDE;
      //STensor43& _DSnlDE = ipvcur->getRefToDSnlDE();  //maybe this line and the following can be removed (it was written for having the view in the msh files)
      //_DSnlDE = DS_nlDE;
       
     //ipvcur->getRefToDLocalVariableDStrain()=K; 
      ipvcur->getRefToDStressDNonLocalVariable() = M;
      std::vector<STensor3> &  K1 = ipvcur->getRefToDLocalVariableDStrain();  
      for (idx=0;idx<_numNonlocalVar;idx++){
        for (int k=0;k<3;k++){
          for (int l=0;l<3;l++){
            K1[idx](k,l)=0;
            for (int c=0;c<3;c++){
              for (int d=0;d<3;d++){
                K1[idx](k,l)+=K[idx](c,d)*DEDF(c,d,k,l);  //to have the derivatives following F and not epsilon
              }
            }
          }
        }
      }
    }
    //end step 8.2
    
  //step 9: for energy-based path Following
    //beginning step 9.1: computation of the plastic energy
    const STensor3& Eplast0 = ipvprev->getConstRefToEplast();
    const STensor3& Eplast_rec0 = ipvprev->getConstRefToEplastrec();
 
    double &defoEnergy=ipvcur->getRefToDefoEnergy();
    defoEnergy=ipvprev->getConstRefToDefoEnergy();
      for (int i=0;i<3;i++){
        for (int j=0;j<3;j++){
          defoEnergy+=S_rec(i,j)*((E1Tensor(i,j)-Eplast_rec(i,j))-(E0Tensor(i,j)-Eplast_rec0(i,j)));
        }
 
    double & plasticEnergy = ipvcur->getRefToPlasticEnergy();
    plasticEnergy = ipvprev->getConstRefToPlasticEnergy();
    for (int i=0;i<3;i++){
      for (int j=0;j<3;j++){
        //plasticEnergy+=S(i,j)*(Eplast(i,j)-Eplast0(i,j));;
        plasticEnergy+=S_rec(i,j)*(Eplast_rec(i,j)-Eplast_rec0(i,j));;
      }
    }
    
    
    //std::cout << "plastEnergyprev = " << _plasticEnergyprev << "\n";
    //std::cout << "plastEnergycur = " << _plasticEnergy << "\n";
    //end step 9.1
    
    //beginning step 9.2: computation of DplastEnergDE
    static STensor3 DplastEnergDE;
    for (int k=0;k<3;k++){
      for (int l=0;l<3;l++){
        DplastEnergDE(k,l)=0;
        for (int i=0;i<3;i++){
          for (int j=0;j<3;j++){
            //DplastEnergDE(k,l)+=DSDETensor43(i,j,k,l)*(Eplast(i,j)-Eplast0(i,j))+S(i,j)*DEplastDE(i,j,k,l);
            DplastEnergDE(k,l) +=DS_nlDE(i,j,k,l)*(Eplast_rec(i,j)-Eplast_rec0(i,j));
            DplastEnergDE(k,l) +=S_rec(i,j)*DEplast_lDE(i,j,k,l); 
          }
        }
      }
    }
    if (ipvprev->getConstRefToPlasticEnergy()>plasticEnergy){
      //std::cout << "warning: the plastic energy decreases" << "\n";
      plasticEnergy=ipvprev->getConstRefToPlasticEnergy();
      for (int k=0;k<3;k++)
        for (int l=0;l<3;l++)
          DplastEnergDE(k,l)=0.;
    }
    
    //computation of DplastEnergDNonLocalVar
    double DplastEnergDNonLocalVar0=0.;
    double DplastEnergDNonLocalVar1=0.;
    for (int i=0;i<3;i++){
      for (int j=0;j<3;j++){
        DplastEnergDNonLocalVar0+=M[0](i,j)*(Eplast_rec(i,j)-Eplast_rec0(i,j));
        DplastEnergDNonLocalVar1+=M[1](i,j)*(Eplast_rec(i,j)-Eplast_rec0(i,j));
      }
    }
    DplastEnergDNonLocalVar1+=S_rec(0,0);   //for a case in which Eplast00 is NonLocalVar1, should be generalized

    static STensor3 DdefoEnergDE;
    for (int k=0;k<3;k++){
      for (int l=0;l<3;l++){
        DdefoEnergDE(k,l)=0;
        for (int i=0;i<3;i++){
          for (int j=0;j<3;j++){
            DdefoEnergDE(k,l)+=DS_nlDE(i,j,k,l)*((E1Tensor(i,j)-Eplast_rec(i,j))-(E0Tensor(i,j)-Eplast_rec0(i,j)))+S_rec(i,j)*(DEDE(i,j,k,l)-DEplast_lDE(i,j,k,l));
          }
        }
      }
    }
    
    //computation of DdefoEnergDNonLocalVar  (case with only two non local variables, should be generalized)
    double DdefoEnergDNonLocalVar0=0.;
    double DdefoEnergDNonLocalVar1=0.;
    for (int i=0;i<3;i++){
      for (int j=0;j<3;j++){
        DdefoEnergDNonLocalVar0+=M[0](i,j)*((E1Tensor(i,j)-Eplast_rec(i,j))-(E0Tensor(i,j)-Eplast_rec0(i,j)));
        DdefoEnergDNonLocalVar1+=M[1](i,j)*((E1Tensor(i,j)-Eplast_rec(i,j))-(E0Tensor(i,j)-Eplast_rec0(i,j)));
      }
    }
    DdefoEnergDNonLocalVar1-=S_rec(0,0);   //for a case in which Eplast00 is NonLocalVar1, should be generalized
    //end step 9.2

    static STensor3 I2(1.);
    if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DEFO_ENERGY){
      ipvcur->getRefToIrreversibleEnergy() = ipvcur->getConstRefToDefoEnergy();
      ipvcur->getRefToDIrreversibleEnergyDF() = DdefoEnergDE; 
      ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(0) = DdefoEnergDNonLocalVar0;
      ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(1) = DdefoEnergDNonLocalVar1;
      
    }
    else if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::PLASTIC_ENERGY){
      ipvcur->getRefToIrreversibleEnergy() = ipvcur->getConstRefToPlasticEnergy();
      ipvcur->getRefToDIrreversibleEnergyDF() = DplastEnergDE; 
      ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(0) = DplastEnergDNonLocalVar0;
      ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(1) = DplastEnergDNonLocalVar1;
    }
    else if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DAMAGE_ENERGY){
      ipvcur->getRefToIrreversibleEnergy() = ipvcur->getConstRefToDamageEnergy();
      ipvcur->getRefToDIrreversibleEnergyDF() = I2; //DpsiDE;
      ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(0) = 0;
      ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(1) = 0;
    }
    else
    {
      ipvcur->getRefToIrreversibleEnergy() = ipvcur->getConstRefToPlasticEnergy()+ipvcur->getConstRefToDamageEnergy();
      ipvcur->getRefToDIrreversibleEnergyDF() = I2; //DplastEnergDE;
      ipvcur->getRefToDIrreversibleEnergyDF() += I2; //DpsiDE;
      ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(0) = 0;
      ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(1) = 0;
    }
    
   }
    
  //end step 9
  }
    //end small strain case
  //end code with D0000 and Eplast00 as non local variables*/
  
  
  /*code with D0000 as non local variable
  //beginning step 1: declaration of current variables
  STensor3 S;
  double psi;
  std::vector<double> _localVar(_numNonlocalVar);
  STensor43& NonDamTangPred = ipvcur->getRefToCel();
  STensor43 elTangPred;
  STensor3 Eplast; 
  
  const STensor43& D0 = ipvprev->getConstRefToLocalDamageTensor43();
  STensor43& D = ipvcur->getRefToLocalDamageTensor43();
  
  const torch::Tensor& h0 = ipvprev->getConstRefToInternalVariables();
  torch::Tensor& h1 = ipvcur->getRefToInternalVariables(); 
  //end step 1
  
  
  //beginning step 2: declaration of derivatives computed by perturbation or automatic differentiation
  STensor43& DEplastDE = ipvcur -> getRefToDEplastDE();                 //we can only compute it by perturbation because Eplast is not a value predicted by the RNN
  STensor63 DNonDamTangPredDE;         //only computed by perturbation for now
  STensor63 DelTangPredDE;             //only computed by perturbation for now
  STensor3 DDDE;                       //dD0000/depsilon (we can only compute it by perturbation because D0000 is not a value predicted by the RNN)
  static fullMatrix<double> DSDE(6,6); //only computed by automatic differentiation for now (but the automatic differentiation does not work yet)
  //end step 2  
  
  
  //beginning step 3: computation of RNN predictions and the values deduced by the RNN predictions (local damage D and plastic strain Eplast) and derivatives computed by perturbation or automatic differentiation
   //perturbation case
  if(_tangentByPerturbation){
  
    //beginning step 3.1: we compute the RNN predictions at this time step and the values deduced by the RNN predictions (local damage D and plastic strain Eplast)
    RNNstress_stiff(E0Tensor, E1Tensor, h0, h1, S,psi, NonDamTangPred,elTangPred, D, Eplast, false, DSDE);
   
      //to have D always increasing
    for (int i=0;i<3;i++){
      for (int j=0;j<3;j++){
        for (int k=0;k<3;k++){
          for (int l=0;l<3;l++){
            if (D0(i,j,k,l)>D(i,j,k,l)){
              D(i,j,k,l) = D0(i,j,k,l);
            }
          }
        }
      }
    }
    //end step 3.1
    
    
    if(stiff){
      //beginning step 3.2: declaration of the variables useful for the perturbation
      STensor3 E1Tensor_plus;
    
      STensor3 S_plus;
      double psi_plus;
      STensor43 NonDamTangPred_plus;
      STensor43 elTangPred_plus;
      STensor43 D_plus; 
      STensor3 Eplast_plus;
    
      torch::Tensor h_tmp = torch::zeros({1, 1, _numberOfInternalVariables});
      //end step 3.2
    
    
      //beginning step 3.3: perturbation
      for (int k=0;k<3;k++){
        for (int l=0;l<3;l++){
          E1Tensor_plus = E1Tensor;
          E1Tensor_plus(k,l) += _pertTol;
          if (k!=l){
            E1Tensor_plus(l,k) = E1Tensor_plus(k,l);
          }
          RNNstress_stiff(E1Tensor, E1Tensor_plus, h1, h_tmp, S_plus,psi_plus, NonDamTangPred_plus,elTangPred_plus, D_plus, Eplast_plus, false, DSDE);
         
          //beginning step 3.3.1: computation of DEplastDE 
          for (int i=0;i<3;i++){
            for (int j=0;j<3;j++){
              if (_pertTol>0){
                DEplastDE(i,j,k,l) = (Eplast_plus(i,j) - Eplast(i,j))/_pertTol; 
              }
            }
          }
          //end step 3.3.1

          //beginning step 3.3.2: computation of DNonDamTangPredDE and DelTangPredDE
          for (int i=0;i<3;i++){
            for (int j=0;j<3;j++){
              for (int c=0;c<3;c++){
                for (int d=0;d<3;d++){
                  if (_pertTol>0){
                    DNonDamTangPredDE(i,j,c,d,k,l) = (NonDamTangPred_plus(i,j,c,d) - NonDamTangPred(i,j,c,d))/_pertTol;
                    DelTangPredDE(i,j,c,d,k,l) = (elTangPred_plus(i,j,c,d) - elTangPred(i,j,c,d))/_pertTol;
                  }
                }
              }
            }
          }
          //end step 3.3.2
        
          //beginning step 3.3.3: computation of DDDE
          if (_pertTol>0){
              //to have always D increasing
            if (D_plus(0,0,0,0)<D(0,0,0,0)){
              D_plus(0,0,0,0)=D(0,0,0,0);      
            }
            DDDE(k,l) = (D_plus(0,0,0,0)-D(0,0,0,0))/_pertTol;
          }
          //end step 3.3.3
        
        }
      }
      //end step 3.3
    }
  }else{   //be careful: it is the differentiation automatic case that does not work for now
    RNNstress_stiff(E0Tensor, E1Tensor, h0, h1, S,psi,NonDamTangPred,elTangPred, D, Eplast, true, DSDE);
  }
  //end step 3
  
  
  //beginning step 4: computation of the reconstructed stress
  STensor3 S_nl;    //the reconstructed stress
  double& localVar = ipvcur->getRefToLocalVariable(0);
  const double& NonlocalVar = ipvcur->getConstRefToNonLocalVariable(0);   
  localVar = D(0,0,0,0);
  
    //beginning step 4.1: computation of S_nl(0,0)
  S_nl(0,0) = (1-NonlocalVar)*NonDamTangPred(0,0,0,0)*(E1Tensor(0,0)-Eplast(0,0));
  for (int c=0;c<3;c++){
    for (int d=0;d<3;d++){
      if (not(c==0 and d==0)){
        S_nl(0,0) += elTangPred(0,0,c,d)*(E1Tensor(c,d)-Eplast(c,d));
      }
    }
  }
    //end step 4.1
    
    //beginning step 4.2: computation of the other components of S_nl which are the same as S, the values predicted by the RNN
  for (int i=0;i<3;i++){
    for (int j=0;j<3;j++){
      if (not (i==0 and j==0)){
        S_nl(i,j) = S(i,j);
      }
    }
  }
    //end step 4.2
   
   ///*beginning part coded by Ludovic
  //beginning step 4.2: computation of the other components of S_nl which are the same as S, the values predicted by the RNN
  for (int i=0;i<3;i++){
    for (int j=0;j<3;j++){
        for (int c=0;c<3;c++){
          for (int d=0;d<3;d++){
            S_nl(i,j) += (1-NonlocalVar)*NonDamTangPred(i,j,c,d)*(E1Tensor(c,d)); //-Eplast(c,d)); // elTangPred(0,0,c,d)*(E1Tensor(c,d)-Eplast(c,d));
          }
      }
    }
  }
  //end part coded by Ludovic

  //end step 4
  

  //beginning step 5: declaration of the derivatives
  STensor43 I4;
  STensor43 DS_nlDE;
  STensor3 DS_nlDVar_nl;
  //end step 5
  

  if(stiff){
    //beginning step 6: computation of the derivatives
      //beginning step 6.1: computation of I4 
    for (int i=0;i<3;i++){
      for (int j=0;j<3;j++){
        for (int k=0;k<3;k++){
          for (int l=0;l<3;l++){
            if ((i==k) and (j==l)){
              I4(i,j,k,l) = 1;
            }
            if ((i==l) and (j==k)){
              I4(i,j,k,l) = 1;
            }
          }
        }
      }
    }
      //end step 6.1

      //beginning step 6.2: computation of DS_nlDE
        //beginning step 6.2.1: computation of DS_nlDE(0,0,k,l) (S_nl(0,0) is the only reconstructed value)
    for (int k=0;k<3;k++){
      for (int l=0;l<3;l++){
        DS_nlDE(0,0,k,l) = (1-NonlocalVar)*DNonDamTangPredDE(0,0,0,0,k,l)*(E1Tensor(0,0)-Eplast(0,0))+(1-NonlocalVar)*NonDamTangPred(0,0,0,0)*(I4(0,0,k,l)-DEplastDE(0,0,k,l));
        for (int c=0;c<3;c++){
          for (int d=0;d<3;d++){
            if (not(c==0 and d==0)){
              DS_nlDE(0,0,k,l)+=DelTangPredDE(0,0,c,d,k,l)*(E1Tensor(c,d)-Eplast(c,d))+elTangPred(0,0,c,d)*(I4(c,d,k,l)-DEplastDE(c,d,k,l));
            }
          }
        }
      }
    }
        //end step 6.2.1
        
        //beginning step 6.2.2: computation of DS_nlDE(i,j,k,l) for (i,j)!=(0,0) (for such (i,j), S_nl(i,j) is the local value)
    for (int k=0;k<3;k++){
      for (int l=0;l<3;l++){
        for (int i=0;i<3;i++){
          for (int j=0;j<3;j++){
            if (not(i==0 and j==0)){
              for (int c=0;c<3;c++){
                for (int d=0;d<3;d++){
                  DS_nlDE(i,j,k,l)+=DelTangPredDE(i,j,c,d,k,l)*(E1Tensor(c,d)-Eplast(c,d))+elTangPred(i,j,c,d)*(I4(c,d,k,l)-DEplastDE(c,d,k,l));
                }
              }
            }
          }
        }
      }
    }
    
    ///*beginning part coded by Ludovic
    for (int k=0;k<3;k++){
      for (int l=0;l<3;l++){
        for (int c=0;c<3;c++){
          for (int d=0;d<3;d++){
            DS_nlDE(c,d,k,l)=0.;
            for (int i=0;i<3;i++){
              for (int j=0;j<3;j++){
                 DS_nlDE(c,d,k,l) += (1-NonlocalVar)*NonDamTangPred(c,d,i,j)*(I4(i,j,k,l)); //-DEplastDE(i,j,k,l));
                 DS_nlDE(c,d,k,l) += (1-NonlocalVar)*DNonDamTangPredDE(c,d,i,j,k,l)*(E1Tensor(i,j)); //-Eplast(i,j));
               }
             }
           }
         }
       }
    }    
    end part coded by Ludovic       
        //end step 6.2.2
      //end step 6.2
      
      //beginning step 6.3: computation of DS_nlDVar_nl (there is a single not-zero value: DS_nlDVar_nl(0,0) as only S_nl(0,0) depends on the non local variable)
    DS_nlDVar_nl(0,0) = -NonDamTangPred(0,0,0,0)*(E1Tensor(0,0)-Eplast(0,0));
    ///*beginning part coded by Ludovic
    for (int k=0;k<3;k++){
      for (int l=0;l<3;l++){
        DS_nlDVar_nl(k,l) = 0.;
        for (int c=0;c<3;c++){
          for (int d=0;d<3;d++){
            DS_nlDVar_nl(k,l) -= NonDamTangPred(k,l,c,d)*(E1Tensor(c,d)); //-Eplast(c,d));
          }
        }
      }
    }
    end part coded by Ludovic
      //end step 6.3
    //end step 6
  }
  
  //beginning step 7: we retrieve the values in the internal variables
    //beginning step 7.1: we retrieve the plastic strain
  STensor3& _Eplast = ipvcur->getRefToEplast();
  _Eplast =Eplast;
    //end step 7.1
  //end step 7
  
  
  //beginning step 8: we retrieve the computed values in the current internal variables
    //small strain case (the only one that works for now)
  if (_kinematicInput == smallStrain || _kinematicInput== smallStrainInc  )
  {
    //beginning step 8.1: we retrieve the stress
    STensor3& P = ipvcur->getRefToFirstPiolaKirchhoffStress();
    P=S_nl; 
    //end step 8.1

    //beginning step 8.2: we retrieve the derivatives only if they are not computed by perturbation out of the code
    if (stiff){    
      fullMatrix<double> & DlocalToDnonlocalVar = ipvcur->getRefToDLocalVariableDNonLocalVariable();
      DlocalToDnonlocalVar(0,0) = 0.0;
          
      STensor43& L = ipvcur->getRefToTangentModuli();
      L = DS_nlDE;
      STensor43& _DSnlDE = ipvcur->getRefToDSnlDE();  //maybe this line and the following can be removed (it was written for having the view in the msh files)
      _DSnlDE = DS_nlDE;
       
      std::vector<STensor3> & M = ipvcur->getRefToDStressDNonLocalVariable();
      M[0] = DS_nlDVar_nl;
        
      std::vector<STensor3> &  K = ipvcur->getRefToDLocalVariableDStrain();    
      K[0] = DDDE;      
    }
    //end step 8.2
  }
    //end small strain case
  end code with D0000 as non local variable*/
  
  
  
  
  
  
  
  
  
  /*code with psi as non local variable
  //beginning step 1: declaration of current variables
  STensor3 S;
  double psi;
  std::vector<double> _localVar(_numNonlocalVar);
  STensor43 NonDamTangPred;
  STensor43 elTangPred;
  STensor3 Eplast; 
  
  const STensor43& D0 = ipvprev->getConstRefToLocalDamageTensor43();
  STensor43& D = ipvcur->getRefToLocalDamageTensor43();
  
  const torch::Tensor& h0 = ipvprev->getConstRefToInternalVariables();
  torch::Tensor& h1 = ipvcur->getRefToInternalVariables(); 
  //end step 1
  
  
  //beginning step 2: declaration of derivatives computed by perturbation or automatic differentiation
  static STensor3 DpsiDE;         //only computed by perturbation for now
  STensor63 DNonDamTangPredDE;         //only computed by perturbation for now
  STensor63 DelTangPredDE;             //only computed by perturbation for now
  STensor43 DEplastDE;                 //we can only compute it by perturbation because it is not a value predicted by the RNN
  static fullMatrix<double> DSDE(6,6); //only computed by automatic differentiation for now (but the automatic differentiation does not work yet)
  //end step 2


  //beginning step 3: computation of RNN predictions and plastic strain and derivatives computed by perturbation or automatic differentiation
   //perturbation case
  if(_tangentByPerturbation){
  
    //beginning step 3.1: we compute the RNN predictions at this time step
    RNNstress_stiff(E0Tensor, E1Tensor, h0, h1, S,psi, NonDamTangPred,elTangPred, D, Eplast, false, DSDE);
    
      //to have the damage energy dissipation always increasing
    const double& localVar0 = ipvprev->getConstRefToLocalVariable(0);     //value at the previous step
    if (psi < localVar0){
      psi = localVar0;
    }  
    
      //to have D always increasing
    for (int i=0;i<3;i++){
      for (int j=0;j<3;j++){
        for (int k=0;k<3;k++){
          for (int l=0;l<3;l++){
            if (D0(i,j,k,l)>D(i,j,k,l)){
              D(i,j,k,l) = D0(i,j,k,l);
            }
          }
        }
      }
    }
    //end step 3.1
    
    
      

    if(stiff){
      //beginning step 3.2: declaration of the variables useful for the perturbation
      STensor3 E1Tensor_plus;
    
      STensor3 S_plus;
      double psi_plus;
      STensor43 NonDamTangPred_plus;
      STensor43 elTangPred_plus;
      STensor43 D_plus; 
      STensor3 Eplast_plus;
    
      torch::Tensor h_tmp = torch::zeros({1, 1, _numberOfInternalVariables});
      //end step 3.2
    
    
      //beginning step 3.3: perturbation
      for (int k=0;k<3;k++){
        for (int l=0;l<3;l++){
          E1Tensor_plus = E1Tensor;
          E1Tensor_plus(k,l) += _pertTol;
          if (k!=l){
            E1Tensor_plus(l,k) = E1Tensor_plus(k,l);
          }
          RNNstress_stiff(E1Tensor, E1Tensor_plus, h1, h_tmp, S_plus,psi_plus, NonDamTangPred_plus,elTangPred_plus, D_plus, Eplast_plus, false, DSDE);
        
          for(int j=0; j<_numNonlocalVar; j++){
            //assumption: to have always a damage energy dissipation variable increasing
            if (psi_plus < psi){
              psi_plus = psi;
            } 
          }
      
      
          //beginning step 3.3.1: computation of DpsiDE 
          if (_pertTol>0){
            DpsiDE(k,l) = (psi_plus-psi)/_pertTol;
          }
          //end step 3.3.1
        
        
          
          //beginning step 3.3.2: computation of DEplastDE 
          for (int i=0;i<3;i++){
            for (int j=0;j<3;j++){
              //Eplast_plus(i,j) = E1Tensor_plus(i,j) - prod_plus(i,j); //TO REMOVE
              if (_pertTol>0){
                DEplastDE(i,j,k,l) = (Eplast_plus(i,j) - Eplast(i,j))/_pertTol; 
              }
            }
          }
          //end step 3.3.2



          //beginning step 3.3.3: computation of DNonDamTangPredDE and DelTangPredDE
          for (int i=0;i<3;i++){
            for (int j=0;j<3;j++){
              for (int c=0;c<3;c++){
                for (int d=0;d<3;d++){
                  if (_pertTol>0){
                    DNonDamTangPredDE(i,j,c,d,k,l) = (NonDamTangPred_plus(i,j,c,d) - NonDamTangPred(i,j,c,d))/_pertTol;
                    DelTangPredDE(i,j,c,d,k,l) = (elTangPred_plus(i,j,c,d) - elTangPred(i,j,c,d))/_pertTol;
                  }
                }
              }
            }
          }
          //end step 3.3.3
        
        
        
        }
      }
      //end step 3.3
    }
  }else{   //be careful: it is the differentiation automatic case that does not work for now
    RNNstress_stiff(E0Tensor, E1Tensor, h0, h1, S,psi,NonDamTangPred,elTangPred, D, Eplast, true, DSDE);
  }
  //end step 3
  
  
  //beginning step 4: computation of reconstructed values (Dreconstructed, the reconstructed elastic stiffness matrix with damage, and reconstructed stress)
  const double& localVar0 = ipvprev->getConstRefToLocalVariable(0);     
  double& localVar = ipvcur->getRefToLocalVariable(0);
  const double& NonlocalVar0 = ipvprev->getConstRefToNonLocalVariable(0);   
  const double& NonlocalVar = ipvcur->getConstRefToNonLocalVariable(0);     
 
  localVar = psi;
  
  double nTlratio = 1.; 
  if (fabs(localVar-localVar0)>0){
    nTlratio = (NonlocalVar -NonlocalVar0)/(localVar -localVar0);
  }         

  
  STensor43& nlD = ipvcur->getRefToNonLocalDamageTensor43();
  const STensor43& nlD0 = ipvprev->getConstRefToNonLocalDamageTensor43();  
  
  double maxNonDamTang = 0;
  for (int i=0;i<3;i++){
    for (int j=0;j<3;j++){
      for (int k=0;k<3;k++){
        for (int l=0;l<3;l++){
          if (fabs(NonDamTangPred(i,j,k,l))>maxNonDamTang){
            maxNonDamTang = fabs(NonDamTangPred(i,j,k,l));
          }
        }
      }
    }
  }
  
    //beginning step 4.1: computation of Dreconstructed (we use an assumption)
  for (int i=0;i<3;i++){
    for (int j=0;j<3;j++){
      for (int k=0;k<3;k++){
        for (int l=0;l<3;l++){
          nlD(i,j,k,l) = nlD0(i,j,k,l)+(D(i,j,k,l) - D0(i,j,k,l))*nTlratio;
        }
      }
    }
  }
    //end step 4.1
  
  
    //beginning step 4.2: computation of the elastic stiffness tangent with damage reconstructed
  STensor43 elTangRec;
  for (int i=0;i<3;i++){
    for (int j=0;j<3;j++){
      for (int k=0;k<3;k++){
        for (int l=0;l<3;l++){
          elTangRec(i,j,k,l) = (1-nlD(i,j,k,l))*NonDamTangPred(i,j,k,l);
        }
      }
    }
  }
    //end step 4.2
   
  
    //beginning step 4.3: computation of the reconstructed stress
  STensor3 S_nl;  
  STensor3 E_minus_Eplast;
  for (int i=0;i<3;i++){
    for (int j=0;j<3;j++){
      E_minus_Eplast(i,j) = E1Tensor(i,j)-Eplast(i,j);
    }
  } 
  STensorOperation::multSTensor3STensor43(E_minus_Eplast, elTangRec, S_nl);
    //end step 4.3
  //end step 4
  

  
  

  //beginning step 5: declaration of the derivatives
  STensor63 DDDE;
  STensor63 DnlDDE;
  STensor43 I4;
  STensor43 DS_nlDE_firstterm;
  STensor43 DS_nlDE_secondterm;
  STensor43 DS_nlDE_thirdterm;
  STensor43 DS_nlDE;
  
  STensor43 DnlDDVar_nl;
  STensor3 DS_nlDVar_nl;
  //end step 5
  
  if(stiff){
    //beginning step 6: computation of the derivatives
      //beginning step 6.1 : computation of DDDE
    for (int i=0;i<3;i++){
      for (int j=0;j<3;j++){
        for (int c=0;c<3;c++){
          for (int d=0;d<3;d++){
            for (int k=0;k<3;k++){
              for (int l=0;l<3;l++){
                if (NonDamTangPred(i,j,c,d)!=0){
                  DDDE(i,j,c,d,k,l) = elTangPred(i,j,c,d)/(NonDamTangPred(i,j,c,d)*NonDamTangPred(i,j,c,d))*DNonDamTangPredDE(i,j,c,d,k,l) - 1./NonDamTangPred(i,j,c,d)*DelTangPredDE(i,j,c,d,k,l);
                }
                //because of the components of D not meaningful
                if (fabs(NonDamTangPred(i,j,c,d))<maxNonDamTang/100){
                  DDDE(i,j,c,d,k,l) = 0;
                }
              }
            }
          }
        }
      }
    }
      //end step 6.1
  
      //beginning step 6.2: computation of DnlDDE
    for (int i=0;i<3;i++){
      for (int j=0;j<3;j++){
        for (int c=0;c<3;c++){
          for (int d=0;d<3;d++){
            for (int k=0;k<3;k++){
              for (int l=0;l<3;l++){
                DnlDDE(i,j,c,d,k,l) = nTlratio*DDDE(i,j,c,d,k,l);
                if (fabs(localVar-localVar0)>0){
                  DnlDDE(i,j,c,d,k,l) += -nTlratio*(D(i,j,c,d)-D0(i,j,c,d))/(localVar-localVar0)*DpsiDE(k,l);
                }
              }
            }
          }
        }
      }
    }
      //end step 6.2
  

      //beginning step 6.3: computation of I4 
    for (int i=0;i<3;i++){
      for (int j=0;j<3;j++){
        for (int k=0;k<3;k++){
          for (int l=0;l<3;l++){
            if ((i==k) and (j==l)){
              I4(i,j,k,l) = 1;
            }
            if ((i==l) and (j==k)){
              I4(i,j,k,l) = 1;
            }
          }
        }
      }
    }
      //end step 6.3

  
      //beginning step 6.4: computation of DS_nlDE
    for (int i=0;i<3;i++){
      for (int j=0;j<3;j++){
        for (int k=0;k<3;k++){
          for (int l=0;l<3;l++){
            DS_nlDE_firstterm(i,j,k,l) = 0;
            DS_nlDE_secondterm(i,j,k,l) = 0;
            DS_nlDE_thirdterm(i,j,k,l) = 0;
            DS_nlDE(i,j,k,l) = 0;
            for (int c=0;c<3;c++){
              for (int d=0;d<3;d++){
                DS_nlDE_firstterm(i,j,k,l) += -DnlDDE(i,j,c,d,k,l)*NonDamTangPred(i,j,c,d)*(E1Tensor(c,d)-Eplast(c,d));
                DS_nlDE_secondterm(i,j,k,l) += (1-nlD(i,j,c,d))*DNonDamTangPredDE(i,j,c,d,k,l)*(E1Tensor(c,d)-Eplast(c,d));
                DS_nlDE_thirdterm(i,j,k,l) += (1 - nlD(i,j,c,d))*NonDamTangPred(i,j,c,d)*(I4(c,d,k,l)-DEplastDE(c,d,k,l));
              }
            }
            DS_nlDE(i,j,k,l) = DS_nlDE_firstterm(i,j,k,l)+DS_nlDE_secondterm(i,j,k,l)+DS_nlDE_thirdterm(i,j,k,l);
          }
        }
      }
    }
      //end step 6.4
  
  
  
      //beginning step 6.5: computation of DS_nlDVar_nl
    STensor43 prod2;  //scalar product of DnlDDVar_nl and the elastic stiffness matrix without damage NonDamTangPred
    for (int i=0;i<3;i++){
      for (int j=0;j<3;j++){
        for (int k=0;k<3;k++){
          for (int l=0;l<3;l++){
            if (fabs(localVar -localVar0)>0){
              DnlDDVar_nl(i,j,k,l) = (D(i,j,k,l)-D0(i,j,k,l))/(localVar - localVar0);
              prod2(i,j,k,l) = -DnlDDVar_nl(i,j,k,l)*NonDamTangPred(i,j,k,l);
            }
          }
        }
      }
    }
    STensorOperation::multSTensor3STensor43(E_minus_Eplast, prod2, DS_nlDVar_nl);
      //end step 6.5
    //end step 6
  } 
  //beginning step 7: we retrieve the computed values in the current internal variables
    //small strain case (the only one that works for now)
  if (_kinematicInput == smallStrain || _kinematicInput== smallStrainInc  )
  {
    //beginning step 7.1: we retrieve the stress
    STensor3& P = ipvcur->getRefToFirstPiolaKirchhoffStress();
    P=S_nl; 
    //end step 7.1

    //beginning step 7.2: we retrieve the derivatives only if they are not computed by perturbation out of the code
    if (stiff){    
      fullMatrix<double> & DlocalToDnonlocalVar = ipvcur->getRefToDLocalVariableDNonLocalVariable();
      DlocalToDnonlocalVar(0,0) = 0.0;
          
      STensor43& L = ipvcur->getRefToTangentModuli();
      L = DS_nlDE;
       
      std::vector<STensor3> & M = ipvcur->getRefToDStressDNonLocalVariable();
      M[0] = DS_nlDVar_nl;
        
      std::vector<STensor3> &  K = ipvcur->getRefToDLocalVariableDStrain();    
      K[0] = DpsiDE;      
    }
    //end step 7.2
  }
    //end small strain case
  end code with psi as non local variable*/
   
  else
  {
     ////////////////////////////////////////////////////////////////////////
     //For now, there is no code for the large strain case
     ////////////////////////////////////////////////////////////////////////
     
     
    //Damage evaluation ---------
    //fullMatrix<double>& D = ipvcur->getRefToLocalDamage();
    //fullMatrix<double>& nlD = ipvcur->getRefToNonLocalDamage();
    //const fullMatrix<double>& D0 = ipvprev->getConstRefToLocalDamage();
    //const fullMatrix<double>& nlD0 = ipvprev->getConstRefToNonLocalDamage();
  
    //double localVar0 = ipvprev->getConstRefToLocalVariable(0);
    //double NonlocalVar0 = ipvprev->getConstRefToNonLocalVariable(0);
    //double NonlocalVar = ipvcur->getConstRefToNonLocalVariable(0);
  
    //double nTlratio = 1.;
    //if(fabs(localVar -localVar0)>0.)      
    //    nTlratio = (NonlocalVar -NonlocalVar0)/(localVar -localVar0);
  
    //for(int j=0; j<6; j++){
    //  if (EffS(0,j) == 0){   //pour eviter la division par zero
    //    D(0,j)=0;
    //    nlD(0,j) = nlD0(0,j);
    //  }else{
    //    D(0,j) = 1.0-(S(0,j)+1.0e-20)/(EffS(0,j)+1.0e-20);
    //    nlD(0,j) = nlD0(0,j) + (D(0,j)-D0(0,j))*nTlratio;
    //  }
    //} 
    //------------------------ end damage--------
    //static fullMatrix<double> S_nl(1,6);
    //for(int j=0; j<6; j++){
      //S_nl(0,j) = EffS(0,j)*(1.0-nlD(0,j));
    //}

  
  
    // get stress
    // output is S00, S11, S22, S01, S02, S12
    static STensor3 secondPK;
    //secondPK(0,0) = S_nl(0,0);
    //secondPK(1,1) = S_nl(0,1);
    //secondPK(2,2) = S_nl(0,2);
    //secondPK(0,1) = S_nl(0,3);
    //secondPK(1,0) = S_nl(0,3);
    //secondPK(0,2) = S_nl(0,4);
    //secondPK(2,0) = S_nl(0,4);
    //secondPK(1,2) = S_nl(0,5);
    //secondPK(2,1) = S_nl(0,5);
    

    STensor3& P = ipvcur->getRefToFirstPiolaKirchhoffStress();
    STensorOperation::multSTensor3(F,secondPK,P);
   /*
   if (stiff){
     static fullMatrix<double> dnlDdE(6,6), DS_nlDE(6,6), DS_nlDVar_nl(1,6);
     for(int i=0; i<6; i++){
       for(int j=0; j<6; j++){
         if(fabs(EffS(0,i))>0.){
           dnlDdE(i,j) = -1.0/EffS(0,i)*DSDE(i,j) + S(0,i)/EffS(0,i)/EffS(0,i)*DeffSDE(i,j);
         }
         else{
           dnlDdE(i,j) = 0.;
         }
           
         if(fabs(localVar -localVar0)>0.){
           dnlDdE(i,j) = nTlratio*(dnlDdE(i,j) - (D(0,i)-D0(0,i))/(localVar -localVar0)*DlocalVarDE(0,j));
         }
         else{
           dnlDdE(i,j) = 0.;
         }
          
         DS_nlDE(i,j) = (1.0-nlD(0,i))*DeffSDE(i,j) - EffS(0,i)*dnlDdE(i,j);         
       }
       if(fabs(localVar -localVar0)>0.){   
         DS_nlDVar_nl(0,i) = -(D(0,i)-D0(0,i))/(localVar -localVar0)*EffS(0,i);
       }
       else{
         DS_nlDVar_nl(0,i) = 0.;
       }
     }
     
     
     
     
     fullMatrix<double> & DlocalToDnonlocalVar = ipvcur->getRefToDLocalVariableDNonLocalVariable();
     DlocalToDnonlocalVar(0,0) = 0.0;
     
     static STensor43 DsecondPKDE; 
     convertFullMatrixToSTensor43(DSDE,DsecondPKDE); 
     
     STensor43& L = ipvcur->getRefToTangentModuli();
     STensorOperation::zero(L);  
     static STensor43 DsecondPKDEGreenLagrange;  
       
     static STensor3 DlocalVarDE0;  
     convertFullMatrixToSTensor3(DlocalVarDE,DlocalVarDE0);   
     std::vector<STensor3> &  K = ipvcur->getRefToDLocalVariableDStrain();        
     STensorOperation::zero(K[0]); 
     static STensor3 DlocalVarDEGreenLagrange;  
       
     if (_kinematicInput == NonlocalDamageTorchANNBasedDG3DMaterialLaw::EGL || _kinematicInput == NonlocalDamageTorchANNBasedDG3DMaterialLaw::EGLInc)
     {
         DsecondPKDEGreenLagrange = DsecondPKDE;
         DlocalVarDEGreenLagrange = DlocalVarDE0;
     }
     else if (_kinematicInput == NonlocalDamageTorchANNBasedDG3DMaterialLaw::U || _kinematicInput == NonlocalDamageTorchANNBasedDG3DMaterialLaw::UInc)
     {
         static STensor3 invU;
         STensorOperation::inverseSTensor3(Ustretch,invU);
         for (int i=0; i<3; i++)
         {
           for (int j=0; j<3; j++)
           {
             for (int k=0; k<3; k++)
             {
               for (int l=0; l<3; l++)
               {
                  DsecondPKDEGreenLagrange(i,j,k,l) = 0.;
                  for (int m=0; m< 3; m++)
                  {
                    DsecondPKDEGreenLagrange(i,j,k,l) += invU(k,m)*DsecondPKDE(i,j,m,l); 
                  }
               }
             }
           }
         }
         
         for (int k=0; k<3; k++)
         {
           for (int l=0; l<3; l++)
           {
             DlocalVarDEGreenLagrange(k,l)=0.0;
             for (int m=0; m< 3; m++)
             {
              DlocalVarDEGreenLagrange(k,l) += invU(k,m)*DlocalVarDE0(m,l);
             }
           }
         }        
    }
     
     

    for (int i=0; i<3; i++)
    {
      for (int j=0; j<3; j++)
      {
        for (int k=0; k<3; k++)
         {
           L(i,j,i,k) += secondPK(k,j);
           for (int l=0; l<3; l++)
            {
              //
              for (int a=0; a<3; a++)
              {
                for (int b=0; b<3; b++)
                {
                  L(i,j,k,l) += F(i,a)*DsecondPKDEGreenLagrange(a,j,l,b)*F(k,b); 
                }
              }
            }
          }
        }
      }
      
      for (int i=0; i<3; i++)
      {
        for (int j=0; j<3; j++)
        {      
          for(int a=0; a<3; a++)
          {
            K[0](i,j) += DlocalVarDEGreenLagrange(j,a)*F(i,a);   //c etait DlocalVarDE dans le cas des petites defos
          }
        }
      }
      
      static STensor3 DsecondPKDVar_nl;
      convertFullMatrixToSTensor3(DS_nlDVar_nl,DsecondPKDVar_nl);
      std::vector<STensor3> & M = ipvcur->getRefToDStressDNonLocalVariable();
      STensorOperation::zero(M[0]); 
      for (int i=0; i<3; i++)
      {
        for (int j=0; j<3; j++)
        {      
          for(int a=0; a<3; a++)
          {
            //M[0](i,j) += F(i,a)*DsecondPKDVar_nl(a,j);   //dans le code initial avec la formulation non locale
            M[0](i,j) += 0.*F(i,a)*DsecondPKDVar_nl(a,j);   //ajout dans le code lorsqu on ne fait pas la formulation non locale
          }
        }
      }
           
    }*/
  } 

  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);  
#else
  Msg::Error("NOT COMPILED WITH TORCH");
#endif
};
#if defined(HAVE_TORCH)


void NonlocalDamageTorchANNBasedDG3DMaterialLaw::RNNstress_stiff(const STensor3& E0Tensor, const STensor3& E1Tensor, const torch::Tensor& h_stress_0, torch::Tensor& h_stress_1, const torch::Tensor& h_elTang_0, torch::Tensor& h_elTang_1, const torch::Tensor& h_NonDamTang_0, torch::Tensor& h_NonDamTang_1, STensor3& S,
         STensor43& NonDamTangPred,STensor43& elTangPred, STensor43& D, STensor3& Eplast, const bool stiff, fullMatrix<double>& DSDE, const STensor3& B0, const STensor3& B1, const STensor3& B2, const STensor3& B3, const STensor3& B4, const STensor3& B5, STensor3& Eplast_l)
{  
    vector<float> E_vec(_numberOfInput),E0_vec(_numberOfInput);  //the current and the previous vectors
    torch::Tensor E_norm, E0_norm,dE_norm;  //the normalized values of E, E0 et de dG (dE_norm)
    if(!stiff){torch::NoGradGuard no_grad;}
    // use RNN to predict stress-------------
    vector<torch::jit::IValue> inputs_stress;
    vector<torch::jit::IValue> inputs_elTang;
    vector<torch::jit::IValue> inputs_NonDamTang;
    Normalize_strain(E1Tensor, E_vec);
   
   
    if(_NeedExtraNorm){
      Normalize_strain(E0Tensor, E0_vec); 
    }

   
    if(_NeedExtraNorm and _DoubleInput){    
      if(stiff){
         //E0_norm = torch::from_blob(E0_vec.data(), {1,1, _numberOfInput}, torch::requires_grad());    //initialement dans le code
         E_norm = torch::from_blob(E_vec.data(), {1,1, _numberOfInput}, torch::requires_grad());
      }
      else{
         //E0_norm = torch::from_blob(E0_vec.data(), {1,1, _numberOfInput}, torch::requires_grad(false));    //initialement dans le code
         E_norm = torch::from_blob(E_vec.data(), {1,1, _numberOfInput}, torch::requires_grad(false));
      } 
      //inputs.push_back(E0_norm);     //initialement dans le code
      inputs_stress.push_back(E_norm);   
      inputs_elTang.push_back(E_norm); 
      inputs_NonDamTang.push_back(E_norm); 
    }
    else{   
      if(stiff){
         E_norm = torch::from_blob(E_vec.data(), {1,1, _numberOfInput}, torch::requires_grad());
      }
      else{
         E_norm = torch::from_blob(E_vec.data(), {1,1, _numberOfInput}, torch::requires_grad(false));
      } 
      inputs_stress.push_back(E_norm); 
      inputs_elTang.push_back(E_norm); 
      inputs_NonDamTang.push_back(E_norm); 
    }

    vector<float> norm_E1(1); //norm of the increment, necessary for the SC cells
    vector<float> DE_vec(_numberOfInput);   
    torch::Tensor norm; 
   
   /*temporarily commented because there is a problem if you want to run the smallStrain case, maybe there are cells that take just G and the norm as inputs
   if(_kinematicInput == NonlocalDamageTorchANNBasedDG3DMaterialLaw::EGLInc or _NeedExtraNorm){     
       if(_kinematicInput == NonlocalDamageTorchANNBasedDG3DMaterialLaw::EGL and _NeedExtraNorm){           
          for (int i = 0; i <_numberOfInput; i++) {
            DE_vec[i] = E_vec[i]-E0_vec[i];
          }
          if(_DoubleInput){
            if(stiff){
               dE_norm = torch::from_blob(DE_vec.data(), {1,1, _numberOfInput}, torch::requires_grad());
            }
            else{
               dE_norm = torch::from_blob(DE_vec.data(), {1,1, _numberOfInput}, torch::requires_grad(false));
            }  
            inputs.push_back(dE_norm);   //dE_norm = dG 
          }
          norm_E1[0] = sqrt(std::inner_product(DE_vec.begin(), DE_vec.end(), DE_vec.begin(), 0.0));          
       } 
       else{
          norm_E1[0] = E1.norm();
       }       
            
      if(stiff){
        norm = torch::from_blob(norm_E1.data(), {1,1}, torch::requires_grad());
      }
      else{
        norm = torch::from_blob(norm_E1.data(), {1,1}, torch::requires_grad(false));
      }    
      inputs.push_back(norm);  
   }*/
 
   //adaptation for a small strain case
    if(_kinematicInput == NonlocalDamageTorchANNBasedDG3DMaterialLaw::smallStrainInc or _NeedExtraNorm){     
       if(_kinematicInput == NonlocalDamageTorchANNBasedDG3DMaterialLaw::smallStrain and _NeedExtraNorm){           
          for (int i = 0; i <_numberOfInput; i++) {
            DE_vec[i] = E_vec[i]-E0_vec[i];
          }
          if(_DoubleInput){
            if(stiff){
               dE_norm = torch::from_blob(DE_vec.data(), {1,1, _numberOfInput}, torch::requires_grad());
            }
            else{
               dE_norm = torch::from_blob(DE_vec.data(), {1,1, _numberOfInput}, torch::requires_grad(false));
            }  
            inputs_stress.push_back(dE_norm);   //dE_norm = dG 
            inputs_elTang.push_back(dE_norm);   //dE_norm = dG 
            inputs_NonDamTang.push_back(dE_norm);   //dE_norm = dG 
          }
          norm_E1[0] = sqrt(std::inner_product(DE_vec.begin(), DE_vec.end(), DE_vec.begin(), 0.0));          
       }

       /*temporarily commented because I have to take the tensor norm
       else{
          norm_E1[0] = E1.norm();
       }*/       
            
      if(stiff){
        norm = torch::from_blob(norm_E1.data(), {1,1}, torch::requires_grad());
      }
      else{
        norm = torch::from_blob(norm_E1.data(), {1,1}, torch::requires_grad(false));
      }    
      inputs_stress.push_back(norm);  
      inputs_elTang.push_back(norm); 
      inputs_NonDamTang.push_back(norm); 
    }    
   
         
    inputs_stress.push_back(h_stress_0);  
    inputs_elTang.push_back(h_elTang_0); 
    inputs_NonDamTang.push_back(h_NonDamTang_0); 
   
   
    auto outputs_stress= module_stress.forward(inputs_stress).toTuple();
    torch::Tensor S_norm = outputs_stress->elements()[0].toTensor();
    h_stress_1 = outputs_stress->elements()[1].toTensor();

    InverseNormalize_stress(S_norm, S); 
    
    
    auto outputs_elTang= module_elTang.forward(inputs_elTang).toTuple();
    torch::Tensor elTang_norm = outputs_elTang->elements()[0].toTensor();
    h_elTang_1 = outputs_elTang->elements()[1].toTensor();

    InverseNormalize_elTang(elTang_norm, elTangPred); 
    
    
    auto outputs_NonDamTang= module_NonDamTang.forward(inputs_NonDamTang).toTuple();
    torch::Tensor NonDamTang_norm = outputs_NonDamTang->elements()[0].toTensor();
    h_NonDamTang_1 = outputs_NonDamTang->elements()[1].toTensor();

    InverseNormalize_NonDamTang(NonDamTang_norm, NonDamTangPred); 


    computation_D(NonDamTangPred, elTangPred, D);         //computation of D 
    computation_Eplast(E1Tensor, S, elTangPred, Eplast);  //computation of Eplast
    computation_Eplast_l(B0, B1, B2, B3, B4, B5, Eplast, Eplast_l); //computation of Eplast_l
   
   
   /*if (_RNN)
   {
   if(stiff){
     for (int i=0; i<6; i++){
       for (int j=0; j<6; j++){
         DSDE(i,j) = 0.0;
       }   
     }  
     if(_kinematicInput != NonlocalDamageTorchANNBasedDG3DMaterialLaw::EGLInc){  
       if(_numberOfInput == 3){                
         S_norm.backward(VXX,true);  
   //  dS/dE                         
         auto EnormGrad_a = E_norm.grad().accessor<float,3>();  
         DSDE(0,0) = EnormGrad_a[0][0][0];
         DSDE(0,1) = EnormGrad_a[0][0][2];
         DSDE(0,3) = EnormGrad_a[0][0][1];        
         E_norm.grad().zero_();
         S_norm.backward(VYY,true);              //  dS/dE
         DSDE(1,0) = EnormGrad_a[0][0][0];
         DSDE(1,1) = EnormGrad_a[0][0][2];
         DSDE(1,3) = EnormGrad_a[0][0][1];
         E_norm.grad().zero_();
         S_norm.backward(VZZ,true);                   //  dS/dE
         DSDE(2,0) = EnormGrad_a[0][0][0];
         DSDE(2,1) = EnormGrad_a[0][0][2];
         DSDE(2,3) = EnormGrad_a[0][0][1];
         E_norm.grad().zero_();
         S_norm.backward(VXY,true);                    //  dS/dE
         DSDE(3,0) = EnormGrad_a[0][0][0];
         DSDE(3,1) = EnormGrad_a[0][0][2];
         DSDE(3,3) = EnormGrad_a[0][0][1];
         E_norm.grad().zero_();*/
         
         /*S_norm.backward(EffVXX,true);  
         DeffSDE(0,0) = EnormGrad_a[0][0][0];
         DeffSDE(0,1) = EnormGrad_a[0][0][2];
         DeffSDE(0,3) = EnormGrad_a[0][0][1];        
         E_norm.grad().zero_();
         S_norm.backward(EffVYY,true);              //  dS/dE
         DeffSDE(1,0) = EnormGrad_a[0][0][0];
         DeffSDE(1,1) = EnormGrad_a[0][0][2];
         DeffSDE(1,3) = EnormGrad_a[0][0][1];
         E_norm.grad().zero_();
         S_norm.backward(EffVZZ,true);                   //  dS/dE
         DeffSDE(2,0) = EnormGrad_a[0][0][0];
         DeffSDE(2,1) = EnormGrad_a[0][0][2];
         DeffSDE(2,3) = EnormGrad_a[0][0][1];
         E_norm.grad().zero_();
         S_norm.backward(EffVXY,true);                    //  dS/dE
         DeffSDE(3,0) = EnormGrad_a[0][0][0];
         DeffSDE(3,1) = EnormGrad_a[0][0][2];
         DeffSDE(3,3) = EnormGrad_a[0][0][1];
         E_norm.grad().zero_();
         
         S_norm.backward(VLocal,true);                    //  dS/dE
         DlocalVarDE(0,0) = EnormGrad_a[0][0][0];
         DlocalVarDE(0,1) = EnormGrad_a[0][0][2];
         DlocalVarDE(0,3) = EnormGrad_a[0][0][1];
         E_norm.grad().zero_();*/         
       //}
       
       /*else if(_numberOfInput == 6){
         S_norm.backward(VXX,true);                   //  dS/dE
         auto EnormGrad_a = E_norm.grad().accessor<float,3>();
         for (int i=0; i<6; i++){
             DSDE(0,i) = EnormGrad_a[0][0][i];
         }
         E_norm.grad().zero_();
         S_norm.backward(VYY,true);                   //  dS/dE
         for (int i=0; i<6; i++){
             DSDE(1,i) = EnormGrad_a[0][0][i];
         }
         E_norm.grad().zero_();
         S_norm.backward(VZZ,true);                   //  dS/dE
         for (int i=0; i<6; i++){
             DSDE(2,i) = EnormGrad_a[0][0][i];
         }
         E_norm.grad().zero_();
         S_norm.backward(VXY,true);                   //  dS/dE
         for (int i=0; i<6; i++){
             DSDE(3,i) = EnormGrad_a[0][0][i];
         }
         E_norm.grad().zero_();
         S_norm.backward(VZX,true);                   //  dS/dE
         for (int i=0; i<6; i++){
             DSDE(4,i) = EnormGrad_a[0][0][i];
         }
         E_norm.grad().zero_();
         S_norm.backward(VYZ,true);                   //  dS/dE
         for (int i=0; i<6; i++){
             DSDE(5,i) = EnormGrad_a[0][0][i];
         }
         E_norm.grad().zero_();*/
         
         /*S_norm.backward(EffVXX,true);                   //  dS/dE
         for (int i=0; i<6; i++){
             DeffSDE(0,i) = EnormGrad_a[0][0][i];
         }
         E_norm.grad().zero_();
         S_norm.backward(EffVYY,true);                   //  dS/dE
         for (int i=0; i<6; i++){
             DeffSDE(1,i) = EnormGrad_a[0][0][i];
         }
         E_norm.grad().zero_();
         S_norm.backward(EffVZZ,true);                   //  dS/dE
         for (int i=0; i<6; i++){
             DeffSDE(2,i) = EnormGrad_a[0][0][i];
         }
         E_norm.grad().zero_();
         S_norm.backward(EffVXY,true);                   //  dS/dE
         for (int i=0; i<6; i++){
             DeffSDE(3,i) = EnormGrad_a[0][0][i];
         }
         E_norm.grad().zero_();
         S_norm.backward(EffVZX,true);                   //  dS/dE
         for (int i=0; i<6; i++){
             DeffSDE(4,i) = EnormGrad_a[0][0][i];
         }
         E_norm.grad().zero_();
         S_norm.backward(EffVYZ,true);                   //  dS/dE
         for (int i=0; i<6; i++){
             DeffSDE(5,i) = EnormGrad_a[0][0][i];
         }
         E_norm.grad().zero_();
         
         S_norm.backward(VLocal,true);                   //  dS/dE
         for (int i=0; i<6; i++){
             DlocalVarDE(0,i) = EnormGrad_a[0][0][i];
         }
         E_norm.grad().zero_();*/
       /*}
     }
     else{   
       double norm_0 = 1.0;
       static vector<float> E_n0(_numberOfInput, 1.0);
       if(norm_E1[0]>1.0e-15){
         norm_0 = norm_E1[0];
         E_n0 = E_vec;
       }
       if(_numberOfInput == 3){               
         S_norm.backward(VXX,true);           //  dS/dE                         
         auto EnormGrad_a = E_norm.grad().accessor<float,3>();
         auto normGrad = norm.grad().accessor<float,2>();
         DSDE(0,0) = EnormGrad_a[0][0][0]/norm_0 + normGrad[0][0]*E_n0[0];
         DSDE(0,1) = EnormGrad_a[0][0][2]/norm_0 + normGrad[0][0]*E_n0[2];
         DSDE(0,3) = EnormGrad_a[0][0][1]/norm_0 + normGrad[0][0]*E_n0[1];   
             
         E_norm.grad().zero_();
         norm.grad().zero_();        
         S_norm.backward(VYY,true);              //  dS/dE
         DSDE(1,0) = EnormGrad_a[0][0][0]/norm_0 + normGrad[0][0]*E_n0[0];
         DSDE(1,1) = EnormGrad_a[0][0][2]/norm_0 + normGrad[0][0]*E_n0[2];
         DSDE(1,3) = EnormGrad_a[0][0][1]/norm_0 + normGrad[0][0]*E_n0[1];         
    
         E_norm.grad().zero_();
         norm.grad().zero_();
         S_norm.backward(VZZ,true);                   //  dS/dE
         DSDE(2,0) = EnormGrad_a[0][0][0]/norm_0 + normGrad[0][0]*E_n0[0];
         DSDE(2,1) = EnormGrad_a[0][0][2]/norm_0 + normGrad[0][0]*E_n0[2];
         DSDE(2,3) = EnormGrad_a[0][0][1]/norm_0 + normGrad[0][0]*E_n0[1];         
            
         E_norm.grad().zero_();
         norm.grad().zero_();
         S_norm.backward(VXY,true);                    //  dS/dE
         DSDE(3,0) = EnormGrad_a[0][0][0]/norm_0 + normGrad[0][0]*E_n0[0];
         DSDE(3,1) = EnormGrad_a[0][0][2]/norm_0 + normGrad[0][0]*E_n0[2];
         DSDE(3,3) = EnormGrad_a[0][0][1]/norm_0 + normGrad[0][0]*E_n0[1]; 
         E_norm.grad().zero_();
         norm.grad().zero_(); */
         
         
         /*S_norm.backward(EffVXX,true);
         DeffSDE(0,0) = EnormGrad_a[0][0][0]/norm_0 + normGrad[0][0]*E_n0[0];
         DeffSDE(0,1) = EnormGrad_a[0][0][2]/norm_0 + normGrad[0][0]*E_n0[2];
         DeffSDE(0,3) = EnormGrad_a[0][0][1]/norm_0 + normGrad[0][0]*E_n0[1];   
             
         E_norm.grad().zero_();
         norm.grad().zero_();        
         S_norm.backward(EffVYY,true);              //  dS/dE
         DeffSDE(1,0) = EnormGrad_a[0][0][0]/norm_0 + normGrad[0][0]*E_n0[0];
         DeffSDE(1,1) = EnormGrad_a[0][0][2]/norm_0 + normGrad[0][0]*E_n0[2];
         DeffSDE(1,3) = EnormGrad_a[0][0][1]/norm_0 + normGrad[0][0]*E_n0[1];         
    
         E_norm.grad().zero_();
         norm.grad().zero_();
         S_norm.backward(EffVZZ,true);                   //  dS/dE
         DeffSDE(2,0) = EnormGrad_a[0][0][0]/norm_0 + normGrad[0][0]*E_n0[0];
         DeffSDE(2,1) = EnormGrad_a[0][0][2]/norm_0 + normGrad[0][0]*E_n0[2];
         DeffSDE(2,3) = EnormGrad_a[0][0][1]/norm_0 + normGrad[0][0]*E_n0[1];         
            
         E_norm.grad().zero_();
         norm.grad().zero_();
         S_norm.backward(EffVXY,true);                    //  dS/dE
         DeffSDE(3,0) = EnormGrad_a[0][0][0]/norm_0 + normGrad[0][0]*E_n0[0];
         DeffSDE(3,1) = EnormGrad_a[0][0][2]/norm_0 + normGrad[0][0]*E_n0[2];
         DeffSDE(3,3) = EnormGrad_a[0][0][1]/norm_0 + normGrad[0][0]*E_n0[1]; 
         E_norm.grad().zero_();
         norm.grad().zero_(); 
         
         S_norm.backward(VLocal,true);                    //  dS/dE
         DlocalVarDE(0,0) = EnormGrad_a[0][0][0]/norm_0 + normGrad[0][0]*E_n0[0];
         DlocalVarDE(0,1) = EnormGrad_a[0][0][2]/norm_0 + normGrad[0][0]*E_n0[2];
         DlocalVarDE(0,3) = EnormGrad_a[0][0][1]/norm_0 + normGrad[0][0]*E_n0[1];
         E_norm.grad().zero_(); 
         norm.grad().zero_();*/    
         
       /*}
       
       else if(_numberOfInput == 6){
         S_norm.backward(VXX,true);                   //  dS/dE
         auto EnormGrad_a = E_norm.grad().accessor<float,3>();
         auto normGrad = norm.grad().accessor<float,2>();
         for (int i=0; i<6; i++){
             DSDE(0,i) = EnormGrad_a[0][0][i]/norm_0 + normGrad[0][0]*E_n0[i];
         }
         E_norm.grad().zero_();
         norm.grad().zero_();
         S_norm.backward(VYY,true);                   //  dS/dE
         for (int i=0; i<6; i++){
             DSDE(1,i) = EnormGrad_a[0][0][i]/norm_0 + normGrad[0][0]*E_n0[i];
         }
         E_norm.grad().zero_();
         norm.grad().zero_();
         S_norm.backward(VZZ,true);                   //  dS/dE
         for (int i=0; i<6; i++){
             DSDE(2,i) = EnormGrad_a[0][0][i]/norm_0 + normGrad[0][0]*E_n0[i];
         }
         E_norm.grad().zero_();
         norm.grad().zero_();
         S_norm.backward(VXY,true);                   //  dS/dE
         for (int i=0; i<6; i++){
             DSDE(3,i) = EnormGrad_a[0][0][i]/norm_0 + normGrad[0][0]*E_n0[i];
         }
         E_norm.grad().zero_();
         norm.grad().zero_();
         S_norm.backward(VZX,true);                   //  dS/dE
         for (int i=0; i<6; i++){
             DSDE(4,i) = EnormGrad_a[0][0][i]/norm_0 + normGrad[0][0]*E_n0[i];
         }
         E_norm.grad().zero_();
         norm.grad().zero_();
         S_norm.backward(VYZ,true);                   //  dS/dE
         for (int i=0; i<6; i++){
             DSDE(5,i) = EnormGrad_a[0][0][i]/norm_0 + normGrad[0][0]*E_n0[i];
         }
         E_norm.grad().zero_();
         norm.grad().zero_(); */
         
         /*S_norm.backward(EffVXX,true);  
         for (int i=0; i<6; i++){
             DeffSDE(0,i) = EnormGrad_a[0][0][i]/norm_0 + normGrad[0][0]*E_n0[i];
         }
         E_norm.grad().zero_();
         norm.grad().zero_();
         S_norm.backward(EffVYY,true);                   //  dS/dE
         for (int i=0; i<6; i++){
             DeffSDE(1,i) = EnormGrad_a[0][0][i]/norm_0 + normGrad[0][0]*E_n0[i];
         }
         E_norm.grad().zero_();
         norm.grad().zero_();
         S_norm.backward(EffVZZ,true);                   //  dS/dE
         for (int i=0; i<6; i++){
             DeffSDE(2,i) = EnormGrad_a[0][0][i]/norm_0 + normGrad[0][0]*E_n0[i];
         }
         E_norm.grad().zero_();
         norm.grad().zero_();
         S_norm.backward(EffVXY,true);                   //  dS/dE
         for (int i=0; i<6; i++){
             DeffSDE(3,i) = EnormGrad_a[0][0][i]/norm_0 + normGrad[0][0]*E_n0[i];
         }
         E_norm.grad().zero_();
         norm.grad().zero_();
         S_norm.backward(EffVZX,true);                   //  dS/dE
         for (int i=0; i<6; i++){
             DeffSDE(4,i) = EnormGrad_a[0][0][i]/norm_0 + normGrad[0][0]*E_n0[i];
         }
         E_norm.grad().zero_();
         norm.grad().zero_();
         S_norm.backward(EffVYZ,true);                   //  dS/dE
         for (int i=0; i<6; i++){
             DeffSDE(5,i) = EnormGrad_a[0][0][i]/norm_0 + normGrad[0][0]*E_n0[i];
         }
         E_norm.grad().zero_();
         norm.grad().zero_(); 
         
         S_norm.backward(VLocal,true);                   //  dS/dE
         for (int i=0; i<6; i++){
             DlocalVarDE(0,i) = EnormGrad_a[0][0][i]/norm_0 + normGrad[0][0]*E_n0[i];
         }
         E_norm.grad().zero_();
         norm.grad().zero_();*/ 
       /*}   
     }


     for (int i=0; i<6; i++){
       DSDE(i,0) = DSDE(i,0)/_EXXstd;
       DSDE(i,1) = DSDE(i,1)/_EYYstd;
       DSDE(i,2) = DSDE(i,2)/_EZZstd;
       DSDE(i,3) = DSDE(i,3)/_EXYstd;
       DSDE(i,4) = DSDE(i,4)/_EZXstd;
       DSDE(i,5) = DSDE(i,5)/_EYZstd;*/
      
       /*DeffSDE(i,0) = DeffSDE(i,0)/_EXXstd;
       DeffSDE(i,1) = DeffSDE(i,1)/_EYYstd;
       DeffSDE(i,2) = DeffSDE(i,2)/_EZZstd;
       DeffSDE(i,3) = DeffSDE(i,3)/_EXYstd;
       DeffSDE(i,4) = DeffSDE(i,4)/_EZXstd;
       DeffSDE(i,5) = DeffSDE(i,5)/_EYZstd;*/
    // }  
      
      /*DlocalVarDE(0,0) = DlocalVarDE(0,0)/_EXXstd;
      DlocalVarDE(0,1) = DlocalVarDE(0,1)/_EYYstd;
      DlocalVarDE(0,2) = DlocalVarDE(0,2)/_EZZstd;
      DlocalVarDE(0,3) = DlocalVarDE(0,3)/_EXYstd;
      DlocalVarDE(0,4) = DlocalVarDE(0,4)/_EZXstd;
      DlocalVarDE(0,5) = DlocalVarDE(0,5)/_EYZstd;*/
   
     /*for (int j=0; j<6; j++){
       DSDE(0,j) = DSDE(0,j)*_SXXstd;
       DSDE(1,j) = DSDE(1,j)*_SYYstd;
       DSDE(2,j) = DSDE(2,j)*_SZZstd;
       DSDE(3,j) = DSDE(3,j)*_SXYstd;
       DSDE(4,j) = DSDE(4,j)*_SZXstd;
       DSDE(5,j) = DSDE(5,j)*_SYZstd;*/
      
       /*DeffSDE(0,j) = DeffSDE(0,j)*_EffSXXstd;
       DeffSDE(1,j) = DeffSDE(1,j)*_EffSYYstd;
       DeffSDE(2,j) = DeffSDE(2,j)*_EffSZZstd;
       DeffSDE(3,j) = DeffSDE(3,j)*_EffSXYstd;
       DeffSDE(4,j) = DeffSDE(4,j)*_EffSZXstd;
       DeffSDE(5,j) = DeffSDE(5,j)*_EffSYZstd;  
      
       DlocalVarDE(0,j) = DlocalVarDE(0,j)*_localVarstd;*/       
     //}
   //}
   //}
}
#endif



/*void NonlocalDamageTorchANNBasedDG3DMaterialLaw::convertFullMatrixToSTensor43(const fullMatrix<double>& DSDE, STensor43& DsecondPKDE) const
{
  if (DSDE.size1() != 6 or DSDE.size2() != 6)
  {
    Msg::Error("data is not compatible ANNBasedDG3DMaterialLaw:convertFullMatrixToSTensor43");
    return;
  }
  DsecondPKDE(0,0,0,0) = DSDE(0,0);
  DsecondPKDE(0,0,1,1) = DSDE(0,1);
  DsecondPKDE(0,0,2,2) = DSDE(0,2);
  DsecondPKDE(0,0,0,1) = 0.5*DSDE(0,3);
  DsecondPKDE(0,0,1,0) = 0.5*DSDE(0,3);
  DsecondPKDE(0,0,0,2) = 0.5*DSDE(0,4);
  DsecondPKDE(0,0,2,0) = 0.5*DSDE(0,4);
  DsecondPKDE(0,0,1,2) = 0.5*DSDE(0,5);
  DsecondPKDE(0,0,2,1) = 0.5*DSDE(0,5);

  DsecondPKDE(1,1,0,0) = DSDE(1,0);
  DsecondPKDE(1,1,1,1) = DSDE(1,1);
  DsecondPKDE(1,1,2,2) = DSDE(1,2);
  DsecondPKDE(1,1,0,1) = 0.5*DSDE(1,3);
  DsecondPKDE(1,1,1,0) = 0.5*DSDE(1,3);
  DsecondPKDE(1,1,0,2) = 0.5*DSDE(1,4);
  DsecondPKDE(1,1,2,0) = 0.5*DSDE(1,4);
  DsecondPKDE(1,1,1,2) = 0.5*DSDE(1,5);
  DsecondPKDE(1,1,2,1) = 0.5*DSDE(1,5);

  DsecondPKDE(2,2,0,0) = DSDE(2,0);
  DsecondPKDE(2,2,1,1) = DSDE(2,1);
  DsecondPKDE(2,2,2,2) = DSDE(2,2);
  DsecondPKDE(2,2,0,1) = 0.5*DSDE(2,3);
  DsecondPKDE(2,2,1,0) = 0.5*DSDE(2,3);
  DsecondPKDE(2,2,0,2) = 0.5*DSDE(2,4);
  DsecondPKDE(2,2,2,0) = 0.5*DSDE(2,4);
  DsecondPKDE(2,2,1,2) = 0.5*DSDE(2,5);
  DsecondPKDE(2,2,2,1) = 0.5*DSDE(2,5);

  DsecondPKDE(0,1,0,0) = DSDE(3,0);
  DsecondPKDE(0,1,1,1) = DSDE(3,1);
  DsecondPKDE(0,1,2,2) = DSDE(3,2);
  DsecondPKDE(0,1,0,1) = 0.5*DSDE(3,3);
  DsecondPKDE(0,1,1,0) = 0.5*DSDE(3,3);
  DsecondPKDE(0,1,0,2) = 0.5*DSDE(3,4);
  DsecondPKDE(0,1,2,0) = 0.5*DSDE(3,4);
  DsecondPKDE(0,1,1,2) = 0.5*DSDE(3,5);
  DsecondPKDE(0,1,2,1) = 0.5*DSDE(3,5);

  DsecondPKDE(1,0,0,0) = DSDE(3,0);
  DsecondPKDE(1,0,1,1) = DSDE(3,1);
  DsecondPKDE(1,0,2,2) = DSDE(3,2);
  DsecondPKDE(1,0,0,1) = 0.5*DSDE(3,3);
  DsecondPKDE(1,0,1,0) = 0.5*DSDE(3,3);
  DsecondPKDE(1,0,0,2) = 0.5*DSDE(3,4);
  DsecondPKDE(1,0,2,0) = 0.5*DSDE(3,4);
  DsecondPKDE(1,0,1,2) = 0.5*DSDE(3,5);
  DsecondPKDE(1,0,2,1) = 0.5*DSDE(3,5);

  DsecondPKDE(0,2,0,0) = DSDE(4,0);
  DsecondPKDE(0,2,1,1) = DSDE(4,1);
  DsecondPKDE(0,2,2,2) = DSDE(4,2);
  DsecondPKDE(0,2,0,1) = 0.5*DSDE(4,3);
  DsecondPKDE(0,2,1,0) = 0.5*DSDE(4,3);
  DsecondPKDE(0,2,0,2) = 0.5*DSDE(4,4);
  DsecondPKDE(0,2,2,0) = 0.5*DSDE(4,4);
  DsecondPKDE(0,2,1,2) = 0.5*DSDE(4,5);
  DsecondPKDE(0,2,2,1) = 0.5*DSDE(4,5);

  DsecondPKDE(2,0,0,0) = DSDE(4,0);
  DsecondPKDE(2,0,1,1) = DSDE(4,1);
  DsecondPKDE(2,0,2,2) = DSDE(4,2);
  DsecondPKDE(2,0,0,1) = 0.5*DSDE(4,3);
  DsecondPKDE(2,0,1,0) = 0.5*DSDE(4,3);
  DsecondPKDE(2,0,0,2) = 0.5*DSDE(4,4);
  DsecondPKDE(2,0,2,0) = 0.5*DSDE(4,4);
  DsecondPKDE(2,0,1,2) = 0.5*DSDE(4,5);
  DsecondPKDE(2,0,2,1) = 0.5*DSDE(4,5);

  DsecondPKDE(1,2,0,0) = DSDE(5,0);
  DsecondPKDE(1,2,1,1) = DSDE(5,1);
  DsecondPKDE(1,2,2,2) = DSDE(5,2);
  DsecondPKDE(1,2,0,1) = 0.5*DSDE(5,3);
  DsecondPKDE(1,2,1,0) = 0.5*DSDE(5,3);
  DsecondPKDE(1,2,0,2) = 0.5*DSDE(5,4);
  DsecondPKDE(1,2,2,0) = 0.5*DSDE(5,4);
  DsecondPKDE(1,2,1,2) = 0.5*DSDE(5,5);
  DsecondPKDE(1,2,2,1) = 0.5*DSDE(5,5);

  DsecondPKDE(2,1,0,0) = DSDE(5,0);
  DsecondPKDE(2,1,1,1) = DSDE(5,1);
  DsecondPKDE(2,1,2,2) = DSDE(5,2);
  DsecondPKDE(2,1,0,1) = 0.5*DSDE(5,3);
  DsecondPKDE(2,1,1,0) = 0.5*DSDE(5,3);
  DsecondPKDE(2,1,0,2) = 0.5*DSDE(5,4);
  DsecondPKDE(2,1,2,0) = 0.5*DSDE(5,4);
  DsecondPKDE(2,1,1,2) = 0.5*DSDE(5,5);
  DsecondPKDE(2,1,2,1) = 0.5*DSDE(5,5);
};*/



/*void NonlocalDamageTorchANNBasedDG3DMaterialLaw::convertFullMatrixToSTensor3(const fullMatrix<double>& DVarDE, STensor3& K) const
{
  if (DVarDE.size1() != 1 or DVarDE.size2() != 6)
  {
    Msg::Error("data is not compatible ANNBasedDG3DMaterialLaw:convertFullMatrixToSTensor3");
    return;
  }
  K(0,0) = DVarDE(0,0);
  K(1,1) = DVarDE(0,1);
  K(2,2) = DVarDE(0,2);
  K(0,1) = 0.5*DVarDE(0,3);
  K(1,0) = 0.5*DVarDE(0,3);
  K(0,2) = 0.5*DVarDE(0,4);
  K(2,0) = 0.5*DVarDE(0,4);
  K(1,2) = 0.5*DVarDE(0,5);
  K(2,1) = 0.5*DVarDE(0,5);  
}*/  


/*void NonlocalDamageTorchANNBasedDG3DMaterialLaw::invertMatrix(int NbElement,fullMatrix<double>& mat,fullMatrix<double>& matinv)
//inversion d une matrice carree mat de taille NbElement*NbElement par la methode du pivot de Gauss
//matinv est la matrice contenant l inverse de la matrice mat
{
  int inversible = 1; //On ne peut pas inverser la matrice si le pivot de Gauss est nul, donc inversible dans ce cas passe a zero
  int k = 0;
  fullMatrix<double> newmat(NbElement,2*NbElement); //matrice qui contient au depart sur les premieres colonnes la matrice a inverser, et sur les dernieres la matrice identite, et a la fin de l algorithme ce sera l inverse
  //remplissage de la matrice newmat
  for (int i=0;i<NbElement;i++){
    for (int j=0;j<NbElement;j++){
      newmat(i,j) = mat(i,j);
    }
    for (int j=NbElement;j<2*NbElement;j++){
      if (i==(j-NbElement)){
        newmat(i,j) = 1;
      }
    }
  }
  
  //debut de l algorithme du pivot de Gauss
  //on parcourt ligne a ligne
  while ((inversible == 1) and (k<NbElement)){
    if (newmat(k,k)!=0){
      float var = newmat(k,k);   //le pivot de Gauss
      for (int j=0;j<2*NbElement;j++){
        newmat(k,j) = newmat(k,j)/var;   //normalisation de la ligne du pivot
      }
      for (int i=0;i<NbElement;i++){
        if (i!=k){
          float var1 = newmat(i,k);
          for (int j=0;j<2*NbElement;j++){
          newmat(i,j) = newmat(i,j) - newmat(k,j)*var1;   //on modifie les autres lignes pour avoir des zeros
          } 
        }
      }
      k = k+1;
    }else{
      inversible = 0; 
    }
    if (inversible == 0){
      std::cout << "matrix not inversible" << "\n";
    }
  }
  
  //on recupere la matrice inverse qui correspond aux dernieres colonnes de la matrice obtenue par cet algorithme
  for (int i=0;i<NbElement;i++){
    for (int j=0;j<NbElement;j++){
      matinv(i,j) = newmat(i,j+NbElement);
    }
  }
}*/

#if defined(HAVE_TORCH)
void NonlocalDamageTorchANNBasedDG3DMaterialLaw::computation_basis_D(STensor43& A0, STensor43& A1, STensor43& A2, STensor43& A3, STensor43& A4, STensor43& A5, STensor43& A6, STensor43& A7, STensor43& A8, STensor43& A9, STensor43& A10, STensor43& A11, STensor43& A12, STensor43& A13, STensor43& A14, STensor43& A15, STensor43& A16, STensor43& A17, STensor43& A18, STensor43& A19, STensor43& A20)
{
  //computation of A0
  A0(0,0,0,0)=1;
  
  //computation of A1
  A1(0,1,0,0)=A1(1,0,0,0)=A1(0,0,0,1)=A1(0,0,1,0)=1;
  
  //computation of A2
  A2(0,2,0,0)=A2(2,0,0,0)=A2(0,0,0,2)=A2(0,0,2,0)=1;
  
  //computation of A3
  A3(1,1,0,0)=A3(0,0,1,1)=1;
  
  //computation of A4
  A4(1,2,0,0)=A4(2,1,0,0)=A4(0,0,1,2)=A4(0,0,2,1)=1;
  
  //computation of A5
  A5(2,2,0,0)=A5(0,0,2,2)=1;
  
  //computation of A6
  A6(0,1,0,1)=A6(1,0,0,1)=A6(0,1,1,0)=A6(1,0,1,0)=1;
  
  //computation of A7
  A7(0,2,0,1)=A7(2,0,0,1)=A7(0,2,1,0)=A7(2,0,1,0)=A7(0,1,0,2)=A7(1,0,0,2)=A7(0,1,2,0)=A7(1,0,2,0)=1;
  
  //computation of A8
  A8(1,1,0,1)=A8(1,1,1,0)=A8(0,1,1,1)=A8(1,0,1,1)=1;
  
  //computation of A9
  A9(1,2,0,1)=A9(2,1,0,1)=A9(1,2,1,0)=A9(2,1,1,0)=A9(0,1,1,2)=A9(1,0,1,2)=A9(0,1,2,1)=A9(1,0,2,1)=1;
  
  //computation of A10
  A10(2,2,0,1)=A10(2,2,1,0)=A10(0,1,2,2)=A10(1,0,2,2)=1;
  
  //computation of A11
  A11(0,2,0,2)=A11(2,0,0,2)=A11(0,2,2,0)=A11(2,0,2,0)=1;
  
  //computation of A12
  A12(1,1,0,2)=A12(1,1,2,0)=A12(0,2,1,1)=A12(2,0,1,1)=1;
  
  //computation of A13
  A13(1,2,0,2)=A13(2,1,0,2)=A13(1,2,2,0)=A13(2,1,2,0)=A13(0,2,1,2)=A13(2,0,1,2)=A13(0,2,2,1)=A13(2,0,2,1)=1;
  
  //computation of A14
  A14(2,2,0,2)=A14(2,2,2,0)=A14(0,2,2,2)=A14(2,0,2,2)=1;
  
  //computation of A15
  A15(1,1,1,1)=1;
  
  //computation of A16
  A16(1,2,1,1)=A16(2,1,1,1)=A16(1,1,1,2)=A16(1,1,2,1)=1;
  
  //computation of A17
  A17(2,2,1,1)=A17(1,1,2,2)=1;
  
  //computation of A18
  A18(1,2,1,2)=A18(2,1,1,2)=A18(1,2,2,1)=A18(2,1,2,1)=1;
  
  //computation of A19
  A19(2,2,1,2)=A19(2,2,2,1)=A19(1,2,2,2)=A19(2,1,2,2)=1;
  
  //computation of A20
  A20(2,2,2,2)=1;
}



void NonlocalDamageTorchANNBasedDG3DMaterialLaw::computation_basis_Eplast(STensor3& B0, STensor3& B1, STensor3& B2, STensor3& B3, STensor3& B4, STensor3& B5)
{
  //computation of B0
  B0(0,0)=1;
  
  //computation of B1
  B1(0,1)=B1(1,0)=1;
  
  //computation of B2
  B2(0,2)=B2(2,0)=1;
  
  //computation of B3
  B3(1,1)=1;
  
  //computation of B4
  B4(1,2)=B4(2,1)=1;
  
  //computation of B5
  B5(2,2)=1;
}


void NonlocalDamageTorchANNBasedDG3DMaterialLaw::computation_D(const STensor43& NonDamTangPred, const STensor43& elTangPred, STensor43& D)
{
  double maxNonDamTang = 0;
  for (int i=0;i<3;i++){
    for (int j=0;j<3;j++){
      for (int k=0;k<3;k++){
        for (int l=0;l<3;l++){
          if (fabs(NonDamTangPred(i,j,k,l))>maxNonDamTang){
            maxNonDamTang = fabs(NonDamTangPred(i,j,k,l));
          }
        }
      }
    }
  }
  
  for (int i=0;i<3;i++){
    for (int j=0;j<3;j++){
      for (int k=0;k<3;k++){
        for (int l=0;l<3;l++){
          if (NonDamTangPred(i,j,k,l)!=0){
            D(i,j,k,l) = 1-elTangPred(i,j,k,l)/NonDamTangPred(i,j,k,l);
          }
           //rule to avoid to have values for some components of D not meaningful because of the components of the elastic stiffness matrices not meaningful in certain loading cases (values very small compared to the other components)
           //we choose to set the components of D at zero corresponding to the components of the elastic tangents smaller than the maximal value divided by 100 (but we could choose another criterion)
          if (fabs(NonDamTangPred(i,j,k,l))<maxNonDamTang/100){
            D(i,j,k,l) = 0;
          }
        }
      }
    }
  }   
}

void NonlocalDamageTorchANNBasedDG3DMaterialLaw::computation_Eplast(const STensor3& E1Tensor, const STensor3& S, const STensor43& elTangPred, STensor3& Eplast)
{
  STensor43 elTangPred_inv;
  STensorOperation::inverseSTensor43(elTangPred, elTangPred_inv);
  
     //the product of the inverse of the elastic stiffness matrix and the stress tensor
  STensor3 prod;
  STensorOperation::multSTensor3STensor43(S, elTangPred_inv, prod);
  
  for (int i=0;i<3;i++)
  {
    for (int j=0;j<3;j++)
    {
      Eplast(i,j) = E1Tensor(i,j) - prod(i,j);
    }
  }
}

void NonlocalDamageTorchANNBasedDG3DMaterialLaw::computation_Eplast_l(const STensor3& B0, const STensor3& B1, const STensor3& B2, const STensor3& B3, const STensor3& B4, const STensor3& B5, const STensor3& Eplast, STensor3& Eplast_l)
{
  for (int i=0;i<3;i++){
    for (int j=0;j<3;j++){
      Eplast_l(i,j)=(1-_Ie00)*Eplast(0,0)*B0(i,j)+(1-_Ie01)*Eplast(0,1)*B1(i,j)+(1-_Ie02)*Eplast(0,2)*B2(i,j)+(1-_Ie11)*Eplast(1,1)*B3(i,j)+(1-_Ie12)*Eplast(1,2)*B4(i,j)+(1-_Ie22)*Eplast(2,2)*B5(i,j);
    }
  }
}

void NonlocalDamageTorchANNBasedDG3DMaterialLaw::computation_D_l(const STensor43& A0, const STensor43& A1, const STensor43& A2, const STensor43& A3, const STensor43& A4, const STensor43& A5, const STensor43& A6, const STensor43& A7, const STensor43& A8, const STensor43& A9, const STensor43& A10, const STensor43& A11, const STensor43& A12, const STensor43& A13, const STensor43& A14, const STensor43& A15, const STensor43& A16, const STensor43& A17, const STensor43& A18, const STensor43& A19, const STensor43& A20, const STensor43& D, STensor43& D_l)
{
  for (int i=0;i<3;i++){
    for (int j=0;j<3;j++){
      for (int c=0;c<3;c++){
        for (int d=0;d<3;d++){
          D_l(i,j,c,d)=(1-_Id0000)*D(0,0,0,0)*A0(i,j,c,d)+(1-_Id0100)*D(0,1,0,0)*A1(i,j,c,d)+(1-_Id0200)*D(0,2,0,0)*A2(i,j,c,d)+(1-_Id1100)*D(1,1,0,0)*A3(i,j,c,d)+(1-_Id1200)*D(1,2,0,0)*A4(i,j,c,d)+(1-_Id2200)*D(2,2,0,0)*A5(i,j,c,d)+(1-_Id0101)*D(0,1,0,1)*A6(i,j,c,d)+(1-_Id0201)*D(0,2,0,1)*A7(i,j,c,d)+(1-_Id1101)*D(1,1,0,1)*A8(i,j,c,d)+(1-_Id1201)*D(1,2,0,1)*A9(i,j,c,d)+(1-_Id2201)*D(2,2,0,1)*A10(i,j,c,d)+(1-_Id0202)*D(0,2,0,2)*A11(i,j,c,d)+(1-_Id1102)*D(1,1,0,2)*A12(i,j,c,d)+(1-_Id1202)*D(1,2,0,2)*A13(i,j,c,d)+(1-_Id2202)*D(2,2,0,2)*A14(i,j,c,d)+(1-_Id1111)*D(1,1,1,1)*A15(i,j,c,d)+(1-_Id1211)*D(1,2,1,1)*A16(i,j,c,d)+(1-_Id2211)*D(2,2,1,1)*A17(i,j,c,d)+(1-_Id1212)*D(1,2,1,2)*A18(i,j,c,d)+(1-_Id2212)*D(2,2,1,2)*A19(i,j,c,d)+(1-_Id2222)*D(2,2,2,2)*A20(i,j,c,d);
        }
      }
    }
  }
}


void NonlocalDamageTorchANNBasedDG3DMaterialLaw::computation_D_nl__Eplast_nl(const STensor43& A0, const STensor43& A1, const STensor43& A2, const STensor43& A3, const STensor43& A4, const STensor43& A5, const STensor43& A6, const STensor43& A7, const STensor43& A8, const STensor43& A9, const STensor43& A10, const STensor43& A11, const STensor43& A12, const STensor43& A13, const STensor43& A14, const STensor43& A15, const STensor43& A16, const STensor43& A17, const STensor43& A18, const STensor43& A19, const STensor43& A20, const STensor3& B0, const STensor3& B1, const STensor3& B2, const STensor3& B3, const STensor3& B4, const STensor3& B5, const STensor43& D, const STensor3& Eplast, STensor3& Eplast_nl, STensor43& D_nl, nonLocalDamageTorchANNDG3DIPVariableDG3DIPVariable* ipvcur)
{
  int idx=0;
  if (_Id0000==1){
    ipvcur->getRefToLocalVariable(idx) = D(0,0,0,0);
    double& D_nl0000 = ipvcur->getRefToNonLocalVariable(idx);
    D_nl = _Id0000*D_nl0000*A0;
    idx+=1;
  }
  if (_Id0100==1){
    ipvcur->getRefToLocalVariable(idx) = D(0,1,0,0);
    double& D_nl0100 = ipvcur->getRefToNonLocalVariable(idx);
    D_nl += _Id0100*D_nl0100*A1;
    idx+=1;
  }
  if (_Id0200==1){
    ipvcur->getRefToLocalVariable(idx) = D(0,2,0,0);
    double& D_nl0200 = ipvcur->getRefToNonLocalVariable(idx);
    D_nl += _Id0200*D_nl0200*A2;
    idx+=1;
  }
  if (_Id1100==1){
    ipvcur->getRefToLocalVariable(idx) = D(1,1,0,0);
    double& D_nl1100 = ipvcur->getRefToNonLocalVariable(idx);
    D_nl += _Id1100*D_nl1100*A3;
    idx+=1;
  }
  if (_Id1200==1){
    ipvcur->getRefToLocalVariable(idx) = D(1,2,0,0);
    double& D_nl1200 = ipvcur->getRefToNonLocalVariable(idx);
    D_nl += _Id1200*D_nl1200*A4;
    idx+=1;
  }
  if (_Id2200==1){
    ipvcur->getRefToLocalVariable(idx) = D(2,2,0,0);
    double& D_nl2200 = ipvcur->getRefToNonLocalVariable(idx);
    D_nl += _Id2200*D_nl2200*A5;
    idx+=1;
  }
  if (_Id0101==1){
    ipvcur->getRefToLocalVariable(idx) = D(0,1,0,1);
    double& D_nl0101 = ipvcur->getRefToNonLocalVariable(idx);
    D_nl += _Id0101*D_nl0101*A6;
    idx+=1;
  }
  if (_Id0201==1){
    ipvcur->getRefToLocalVariable(idx) = D(0,2,0,1);
    double& D_nl0201 = ipvcur->getRefToNonLocalVariable(idx);
    D_nl += _Id0201*D_nl0201*A7;
    idx+=1;
  }
  if (_Id1101==1){
    ipvcur->getRefToLocalVariable(idx) = D(1,1,0,1);
    double& D_nl1101 = ipvcur->getRefToNonLocalVariable(idx);
    D_nl += _Id1101*D_nl1101*A8;
    idx+=1;
  }
  if (_Id1201==1){
    ipvcur->getRefToLocalVariable(idx) = D(1,2,0,1);
    double& D_nl1201 = ipvcur->getRefToNonLocalVariable(idx);
    D_nl += _Id1201*D_nl1201*A9;
    idx+=1;
  }
  if (_Id2201==1){
    ipvcur->getRefToLocalVariable(idx) = D(2,2,0,1);
    double& D_nl2201 = ipvcur->getRefToNonLocalVariable(idx);
    D_nl += _Id2201*D_nl2201*A10;
    idx+=1;
  }
  if (_Id0202==1){
    ipvcur->getRefToLocalVariable(idx) = D(0,2,0,2);
    double& D_nl0202 = ipvcur->getRefToNonLocalVariable(idx);
    D_nl += _Id0202*D_nl0202*A11;
    idx+=1;
  }
  if (_Id1102==1){
    ipvcur->getRefToLocalVariable(idx) = D(1,1,0,2);
    double& D_nl1102 = ipvcur->getRefToNonLocalVariable(idx);
    D_nl += _Id1102*D_nl1102*A12;
    idx+=1;
  }
  if (_Id1202==1){
    ipvcur->getRefToLocalVariable(idx) = D(1,2,0,2);
    double& D_nl1202 = ipvcur->getRefToNonLocalVariable(idx);
    D_nl += _Id1202*D_nl1202*A13;
    idx+=1;
  }
  if (_Id2202==1){
    ipvcur->getRefToLocalVariable(idx) = D(2,2,0,2);
    double& D_nl2202 = ipvcur->getRefToNonLocalVariable(idx);
    D_nl += _Id2202*D_nl2202*A14;
    idx+=1;
  }
  if (_Id1111==1){
    ipvcur->getRefToLocalVariable(idx) = D(1,1,1,1);
    double& D_nl1111 = ipvcur->getRefToNonLocalVariable(idx);
    D_nl += _Id1111*D_nl1111*A15;
    idx+=1;
  }
  if (_Id1211==1){
    ipvcur->getRefToLocalVariable(idx) = D(1,2,1,1);
    double& D_nl1211 = ipvcur->getRefToNonLocalVariable(idx);
    D_nl += _Id1211*D_nl1211*A16;
    idx+=1;
  }
  if (_Id2211==1){
    ipvcur->getRefToLocalVariable(idx) = D(2,2,1,1);
    double& D_nl2211 = ipvcur->getRefToNonLocalVariable(idx);
    D_nl += _Id2211*D_nl2211*A17;
    idx+=1;
  }
  if (_Id1212==1){
    ipvcur->getRefToLocalVariable(idx) = D(1,2,1,2);
    double& D_nl1212 = ipvcur->getRefToNonLocalVariable(idx);
    D_nl += _Id1212*D_nl1212*A18;
    idx+=1;
  }
  if (_Id2212==1){
    ipvcur->getRefToLocalVariable(idx) = D(2,2,1,2);
    double& D_nl2212 = ipvcur->getRefToNonLocalVariable(idx);
    D_nl += _Id2212*D_nl2212*A19;
    idx+=1;
  }
  if (_Id2222==1){
    ipvcur->getRefToLocalVariable(idx) = D(2,2,2,2);
    double& D_nl2222 = ipvcur->getRefToNonLocalVariable(idx);
    D_nl += _Id2222*D_nl2222*A20;
    idx+=1;
  }
  if (_Ie00==1){
    ipvcur->getRefToLocalVariable(idx) = Eplast(0,0);
    double& Eplast_nl00 = ipvcur->getRefToNonLocalVariable(idx);
    Eplast_nl += _Ie00*Eplast_nl00*B0;
    idx+=1;
  }
  if (_Ie01==1){
    ipvcur->getRefToLocalVariable(idx) = Eplast(0,1);
    double& Eplast_nl01 = ipvcur->getRefToNonLocalVariable(idx);
    Eplast_nl += _Ie01*Eplast_nl01*B1;
    idx+=1;
  }
  if (_Ie02==1){
    ipvcur->getRefToLocalVariable(idx) = Eplast(0,2);
    double& Eplast_nl02 = ipvcur->getRefToNonLocalVariable(idx);
    Eplast_nl += _Ie02*Eplast_nl02*B2;
    idx+=1;
  }
  if (_Ie11==1){
    ipvcur->getRefToLocalVariable(idx) = Eplast(1,1);
    double& Eplast_nl11 = ipvcur->getRefToNonLocalVariable(idx);
    Eplast_nl += _Ie11*Eplast_nl11*B3;
    idx+=1;
  }
  if (_Ie12==1){
    ipvcur->getRefToLocalVariable(idx) = Eplast(1,2);
    double& Eplast_nl12 = ipvcur->getRefToNonLocalVariable(idx);
    Eplast_nl += _Ie12*Eplast_nl12*B4;
    idx+=1;
  }
  if (_Ie22==1){
    ipvcur->getRefToLocalVariable(idx) = Eplast(2,2);
    double& Eplast_nl22 = ipvcur->getRefToNonLocalVariable(idx);
    Eplast_nl += _Ie22*Eplast_nl22*B5;
    idx+=1;
  }
  
  
  if (idx>_numNonlocalVar){
    Msg::Error("computation_D_nl__Eplast_nl: number of non local variables > _numNonlocalVar");
  }
}


void NonlocalDamageTorchANNBasedDG3DMaterialLaw::Normalize_strain(const STensor3& E1Tensor, vector<float>& E_norm) const
{
  if(_kinematicInput != NonlocalDamageTorchANNBasedDG3DMaterialLaw::EGLInc){
    if(_numberOfInput == 1){
      E_norm[0] =  (E1Tensor(0,0)- _EXXmean)/_EXXstd;
    }
    else if(_numberOfInput == 3){
        E_norm[0] =  (E1Tensor(0,0)- _EXXmean)/_EXXstd;
        E_norm[1] =  (E1Tensor(0,1)- _EXYmean)/_EXYstd;
        E_norm[2] =  (E1Tensor(1,1)- _EYYmean)/_EYYstd;
    }
    else if(_numberOfInput == 6){
        E_norm[0] =  (E1Tensor(0,0)- _EXXmean)/_EXXstd;
        E_norm[1] =  (E1Tensor(1,1)- _EYYmean)/_EYYstd;
        E_norm[2] =  (E1Tensor(2,2)- _EZZmean)/_EZZstd;
        E_norm[3] =  (E1Tensor(0,1)- _EXYmean)/_EXYstd;
        E_norm[4] =  (E1Tensor(0,2)- _EZXmean)/_EZXstd;
        E_norm[5] =  (E1Tensor(1,2)- _EYZmean)/_EYZstd;
    }
  }
  
  /*temporarily commented: we have to retrieve the tensor norm
  else{  
    double norm_E = E1.norm(); 
    if(norm_E<1.0e-15){norm_E = 1.0;}
    if(_numberOfInput == 1){
      E_norm[0] =  E1(0,0)/norm_E;
    }
    else if(_numberOfInput == 3){
        E_norm[0] =  E1(0,0)/norm_E;
        E_norm[1] =  E1(0,1)/norm_E;
        E_norm[2] =  E1(1,1)/norm_E;
    }
    else if(_numberOfInput == 6){
        E_norm[0] =  E1(0,0)/norm_E;
        E_norm[1] =  E1(1,1)/norm_E;
        E_norm[2] =  E1(2,2)/norm_E;
        E_norm[3] =  E1(0,1)/norm_E;
        E_norm[4] =  E1(0,2)/norm_E;
        E_norm[5] =  E1(1,2)/norm_E;
    }
  }*/    
};



void NonlocalDamageTorchANNBasedDG3DMaterialLaw::InverseNormalize_stress(const torch::Tensor& S_norm, STensor3& S) const
{
    auto Snorm_a = S_norm.accessor<float,3>();
    if(_numberOfInput == 1 or _numberOfInput == 3){
        S(0,0) = Snorm_a[0][0][0]*_SXXstd + _SXXmean;
        S(0,1) = S(1,0) = Snorm_a[0][0][1]*_SXYstd + _SXYmean;
        S(0,2) = S(2,0) = 0;
        S(1,1) = Snorm_a[0][0][2]*_SYYstd + _SYYmean;
        S(1,2) = S(2,1) = 0;
        S(2,2) = Snorm_a[0][0][3]*_SZZstd + _SZZmean;
          }
    else if(_numberOfInput == 6){
    //be careful: the 3D case is not ready yet
    }
};


void NonlocalDamageTorchANNBasedDG3DMaterialLaw::InverseNormalize_elTang(const torch::Tensor& elTang_norm, STensor43& elTangPred) const
{
    auto elTangnorm_a = elTang_norm.accessor<float,3>();
    if(_numberOfInput == 1 or _numberOfInput == 3){
                       
        elTangPred(0,0,0,0)= elTangnorm_a[0][0][0]*_elTangCompxxxxstd + _elTangCompxxxxmean;                             
        elTangPred(0,0,1,1)= elTangPred(1,1,0,0) = elTangnorm_a[0][0][1]*_elTangCompyyxxstd + _elTangCompyyxxmean;     //we use the major symmetries
        elTangPred(0,0,2,2)= elTangPred(2,2,0,0) = elTangnorm_a[0][0][2]*_elTangCompzzxxstd + _elTangCompzzxxmean;     //we use the major symmetries
        elTangPred(0,0,0,1)=elTangPred(0,0,1,0)= 0;                                                               
        elTangPred(0,0,0,2)=elTangPred(0,0,2,0)= 0;
        elTangPred(0,0,1,2)=elTangPred(0,0,2,1)= 0;


        elTangPred(1,1,1,1)= elTangnorm_a[0][0][4]*_elTangCompyyyystd + _elTangCompyyyymean;
        elTangPred(1,1,2,2)= elTangPred(2,2,1,1) = elTangnorm_a[0][0][5]*_elTangCompzzyystd + _elTangCompzzyymean;    //we use the major symmetries
        elTangPred(1,1,0,1)=elTangPred(1,1,1,0)= 0;                                                               
        elTangPred(1,1,0,2)=elTangPred(1,1,2,0)= 0;
        elTangPred(1,1,1,2)=elTangPred(1,1,2,1)= 0;

        
        elTangPred(0,1,0,0)=elTangPred(1,0,0,0)= 0;                                                               
        elTangPred(0,1,1,1)=elTangPred(1,0,1,1)= 0;                                                               
        elTangPred(0,1,2,2)=elTangPred(1,0,2,2)= elTangPred(2,2,0,1)=elTangPred(2,2,1,0) = 0;                     
        elTangPred(0,1,0,1)=elTangPred(0,1,1,0)=elTangPred(1,0,0,1)=elTangPred(1,0,1,0)= elTangnorm_a[0][0][3]*_elTangCompxyxystd + _elTangCompxyxymean;
        elTangPred(0,1,0,2)=elTangPred(0,1,2,0)=elTangPred(1,0,0,2)=elTangPred(1,0,2,0)= 0;
        elTangPred(0,1,1,2)=elTangPred(0,1,2,1)=elTangPred(1,0,1,2)=elTangPred(1,0,2,1)= 0;


        elTangPred(2,2,2,2)= 123000;
        elTangPred(2,2,0,2)=elTangPred(2,2,2,0)= 0;
        elTangPred(2,2,1,2)=elTangPred(2,2,2,1)= 0;


        elTangPred(0,2,0,0)=elTangPred(2,0,0,0)= 0;
        elTangPred(0,2,1,1)=elTangPred(2,0,1,1)= 0;
        elTangPred(0,2,2,2)=elTangPred(2,0,2,2)= 0;
        elTangPred(0,2,0,1)=elTangPred(0,2,1,0)=elTangPred(2,0,0,1)=elTangPred(2,0,1,0)= 0;
        elTangPred(0,2,0,2)=elTangPred(0,2,2,0)=elTangPred(2,0,0,2)=elTangPred(2,0,2,0)= 16000;
        elTangPred(0,2,1,2)=elTangPred(0,2,2,1)=elTangPred(2,0,1,2)=elTangPred(2,0,2,1)= 1600;


        elTangPred(1,2,0,0)=elTangPred(2,1,0,0)= 0;
        elTangPred(1,2,1,1)=elTangPred(2,1,1,1)= 0;
        elTangPred(1,2,2,2)=elTangPred(2,1,2,2)= 0;
        elTangPred(1,2,0,1)=elTangPred(1,2,1,0)=elTangPred(2,1,0,1)=elTangPred(2,1,1,0)= 0;
        elTangPred(1,2,0,2)=elTangPred(1,2,2,0)=elTangPred(2,1,0,2)=elTangPred(2,1,2,0)= 0;
        elTangPred(1,2,1,2)=elTangPred(1,2,2,1)=elTangPred(2,1,1,2)=elTangPred(2,1,2,1)= 16000; 
          }
    else if(_numberOfInput == 6){
    //be careful: the 3D case is not ready yet
    }
};


void NonlocalDamageTorchANNBasedDG3DMaterialLaw::InverseNormalize_NonDamTang(const torch::Tensor& NonDamTang_norm, STensor43& NonDamTangPred) const
{
    auto NonDamTangnorm_a = NonDamTang_norm.accessor<float,3>();
    if(_numberOfInput == 1 or _numberOfInput == 3){
                       
        NonDamTangPred(0,0,0,0)= NonDamTangnorm_a[0][0][0]*_NonDamTangCompxxxxstd + _NonDamTangCompxxxxmean;                             
        NonDamTangPred(0,0,1,1)= NonDamTangPred(1,1,0,0) = NonDamTangnorm_a[0][0][1]*_NonDamTangCompyyxxstd + _NonDamTangCompyyxxmean;     //we use the major symmetries
        NonDamTangPred(0,0,2,2)= NonDamTangPred(2,2,0,0) = NonDamTangnorm_a[0][0][2]*_NonDamTangCompzzxxstd + _NonDamTangCompzzxxmean;     //we use the major symmetries
        NonDamTangPred(0,0,0,1)=NonDamTangPred(0,0,1,0)= 0;                                                               
        NonDamTangPred(0,0,0,2)=NonDamTangPred(0,0,2,0)= 0;
        NonDamTangPred(0,0,1,2)=NonDamTangPred(0,0,2,1)= 0;


        NonDamTangPred(1,1,1,1)= NonDamTangnorm_a[0][0][4]*_NonDamTangCompyyyystd + _NonDamTangCompyyyymean;
        NonDamTangPred(1,1,2,2)= NonDamTangPred(2,2,1,1) = NonDamTangnorm_a[0][0][5]*_NonDamTangCompzzyystd + _NonDamTangCompzzyymean;    //we use the major symmetries
        NonDamTangPred(1,1,0,1)=NonDamTangPred(1,1,1,0)= 0;                                                               
        NonDamTangPred(1,1,0,2)=NonDamTangPred(1,1,2,0)= 0;
        NonDamTangPred(1,1,1,2)=NonDamTangPred(1,1,2,1)= 0;

        
        NonDamTangPred(0,1,0,0)=NonDamTangPred(1,0,0,0)= 0;                                                               
        NonDamTangPred(0,1,1,1)=NonDamTangPred(1,0,1,1)= 0;                                                               
        NonDamTangPred(0,1,2,2)=NonDamTangPred(1,0,2,2)= NonDamTangPred(2,2,0,1)=NonDamTangPred(2,2,1,0) = 0;                     
        NonDamTangPred(0,1,0,1)=NonDamTangPred(0,1,1,0)=NonDamTangPred(1,0,0,1)=NonDamTangPred(1,0,1,0)= NonDamTangnorm_a[0][0][3]*_NonDamTangCompxyxystd + _NonDamTangCompxyxymean;
        NonDamTangPred(0,1,0,2)=NonDamTangPred(0,1,2,0)=NonDamTangPred(1,0,0,2)=NonDamTangPred(1,0,2,0)= 0;
        NonDamTangPred(0,1,1,2)=NonDamTangPred(0,1,2,1)=NonDamTangPred(1,0,1,2)=NonDamTangPred(1,0,2,1)= 0;


        NonDamTangPred(2,2,2,2)= 123000;
        NonDamTangPred(2,2,0,2)=NonDamTangPred(2,2,2,0)= 0;
        NonDamTangPred(2,2,1,2)=NonDamTangPred(2,2,2,1)= 0;


        NonDamTangPred(0,2,0,0)=NonDamTangPred(2,0,0,0)= 0;
        NonDamTangPred(0,2,1,1)=NonDamTangPred(2,0,1,1)= 0;
        NonDamTangPred(0,2,2,2)=NonDamTangPred(2,0,2,2)= 0;
        NonDamTangPred(0,2,0,1)=NonDamTangPred(0,2,1,0)=NonDamTangPred(2,0,0,1)=NonDamTangPred(2,0,1,0)= 0;
        NonDamTangPred(0,2,0,2)=NonDamTangPred(0,2,2,0)=NonDamTangPred(2,0,0,2)=NonDamTangPred(2,0,2,0)= 16000;
        NonDamTangPred(0,2,1,2)=NonDamTangPred(0,2,2,1)=NonDamTangPred(2,0,1,2)=NonDamTangPred(2,0,2,1)= 1600;


        NonDamTangPred(1,2,0,0)=NonDamTangPred(2,1,0,0)= 0;
        NonDamTangPred(1,2,1,1)=NonDamTangPred(2,1,1,1)= 0;
        NonDamTangPred(1,2,2,2)=NonDamTangPred(2,1,2,2)= 0;
        NonDamTangPred(1,2,0,1)=NonDamTangPred(1,2,1,0)=NonDamTangPred(2,1,0,1)=NonDamTangPred(2,1,1,0)= 0;
        NonDamTangPred(1,2,0,2)=NonDamTangPred(1,2,2,0)=NonDamTangPred(2,1,0,2)=NonDamTangPred(2,1,2,0)= 0;
        NonDamTangPred(1,2,1,2)=NonDamTangPred(1,2,2,1)=NonDamTangPred(2,1,1,2)=NonDamTangPred(2,1,2,1)= 16000; 
          }
    else if(_numberOfInput == 6){
    //be careful: the 3D case is not ready yet
    }
};

#endif
double NonlocalDamageTorchANNBasedDG3DMaterialLaw::soundSpeed() const
{
  Msg::Error("NonlocalDamageTorchANNBasedDG3DMaterialLaw::soundSpeed is not defined !!!");
  return 0.;
};
//

// TO DO WITH PF
GenericCrackPhaseFieldDG3DMaterialLaw::GenericCrackPhaseFieldDG3DMaterialLaw(const int num, const double rho, const CLengthLaw &cLLaw, const double gc) :
                                                              dG3DMaterialLaw(num,rho,true)
{
  _nldlaw = new mlawGenericCrackPhaseField(num, rho, cLLaw, gc);
  // double nu = _nldlaw->poissonRatio();
  // double mu = _nldlaw->shearModulus();
  // double E = 2.*mu*(1.+nu);
  // fillElasticStiffness(E, nu, elasticStiffness);
}


GenericCrackPhaseFieldDG3DMaterialLaw::GenericCrackPhaseFieldDG3DMaterialLaw(const GenericCrackPhaseFieldDG3DMaterialLaw &source) :
                                                             dG3DMaterialLaw(source)
{
	_nldlaw = NULL;
	if (source._nldlaw != NULL){
		// _nldlaw = dynamic_cast<mlawGenericCrackPhaseField*>(source._nldlaw->clone());
    _nldlaw = new mlawGenericCrackPhaseField(*(source._nldlaw));
	}

}

GenericCrackPhaseFieldDG3DMaterialLaw::~GenericCrackPhaseFieldDG3DMaterialLaw()
{
  if (_nldlaw !=NULL){
     delete _nldlaw;
    _nldlaw = NULL;
  }
};

void GenericCrackPhaseFieldDG3DMaterialLaw::setTime(const double t,const double dtime){
  dG3DMaterialLaw::setTime(t,dtime);
  _nldlaw->setTime(t,dtime);
}

materialLaw::matname GenericCrackPhaseFieldDG3DMaterialLaw::getType() const {return _nldlaw->getType();}
bool GenericCrackPhaseFieldDG3DMaterialLaw::withEnergyDissipation() const {return _nldlaw->withEnergyDissipation();};

void GenericCrackPhaseFieldDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
    // check interface element
    bool inter=true;
    const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
    if(iele==NULL) inter=false;

    IPGenericCrackPhaseField* ipvimeca=NULL;
    _nldlaw->createIPVariable(ipvimeca, hasBodyForce, ele,nbFF_, GP, gpt);
    IPGenericCrackPhaseField* ipv1meca=NULL;
    _nldlaw->createIPVariable(ipv1meca, hasBodyForce, ele,nbFF_, GP, gpt);
    IPGenericCrackPhaseField* ipv2meca=NULL;
    _nldlaw->createIPVariable(ipv2meca, hasBodyForce, ele,nbFF_, GP, gpt);

    int nlVar = _nldlaw->getNbNonLocalVariables();

    IPVariable* ipvi = new  genericCrackPhaseFieldDG3DIPVariable(ipvimeca, nlVar, hasBodyForce,inter);
    IPVariable* ipv1 = new  genericCrackPhaseFieldDG3DIPVariable(ipv1meca, nlVar, hasBodyForce,inter);
    IPVariable* ipv2 = new  genericCrackPhaseFieldDG3DIPVariable(ipv2meca, nlVar, hasBodyForce,inter);

    if(ips != NULL) delete ips;
    ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void GenericCrackPhaseFieldDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
    if(ipv !=NULL) delete ipv;
    bool inter=true;
    const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
    if(iele == NULL){
        inter=false;
    }

    IPGenericCrackPhaseField* ipvimeca =NULL;
    _nldlaw->createIPVariable(ipvimeca, hasBodyForce, ele,nbFF_, GP, gpt);
    int nlVar = _nldlaw->getNbNonLocalVariables();
    ipv = new  genericCrackPhaseFieldDG3DIPVariable(ipvimeca, nlVar, hasBodyForce,inter);

    dynamic_cast<genericCrackPhaseFieldDG3DIPVariable*>(ipv)->setIpMeca(ipvimeca->getRefToIpMeca());
}



// void NonLocalDamageDG3DMaterialLaw::createIPVariable(IPVariable* &ipv,bool hasBodyForce,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
// {
//   if(ipv !=NULL) delete ipv;
//   bool inter=true;
//   const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
//   if(iele == NULL) inter=false;

//   ipv = new  nonLocalDamageDG3DIPVariable(_nldlaw->getNsdv(),_nldlaw->getNbNonLocalVariables(),hasBodyForce,inter);

//   IPNonLocalDamage * ipvnl = static_cast <nonLocalDamageDG3DIPVariable*>(ipv)->getIPNonLocalDamage();
//   _nldlaw->createIPVariable(ipvnl,hasBodyForce,ele,nbFF_,GP,gpt);
// }


double GenericCrackPhaseFieldDG3DMaterialLaw::soundSpeed() const{return _nldlaw->soundSpeed();}

void GenericCrackPhaseFieldDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  /* get ipvariable */
  genericCrackPhaseFieldDG3DIPVariable* ipvcur; //= static_cast < nonLocalDamageDG3DIPVariable *> (ipv);
  const genericCrackPhaseFieldDG3DIPVariable* ipvprev; //= static_cast <const  nonLocalDamageDG3DIPVariable *> (ipvp);

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<genericCrackPhaseFieldDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const genericCrackPhaseFieldDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<genericCrackPhaseFieldDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const genericCrackPhaseFieldDG3DIPVariable*>(ipvp);
  }
  _nldlaw->checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());

};

void GenericCrackPhaseFieldDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  /* get ipvariable */
  genericCrackPhaseFieldDG3DIPVariable* ipvcur; //= static_cast < nonLocalDamageDG3DIPVariable *> (ipv);
  const genericCrackPhaseFieldDG3DIPVariable* ipvprev; //= static_cast <const  nonLocalDamageDG3DIPVariable *> (ipvp);

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<genericCrackPhaseFieldDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const genericCrackPhaseFieldDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<genericCrackPhaseFieldDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const genericCrackPhaseFieldDG3DIPVariable*>(ipvp);
  }

  /* strain xyz */
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

  /* data for J2 law */
  IPGenericCrackPhaseField* q1 = ipvcur->getRefToIPGenericCrackPhaseField();
  const IPGenericCrackPhaseField* q0 = ipvprev->getConstRefToIPGenericCrackPhaseField();

  /* compute stress */
  if(ipvcur-> getNumberNonLocalVariable()>0)
    _nldlaw->constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),
                        ipvcur->getRefToDLocalVariableDStrain(),
                        ipvcur->getRefToDStressDNonLocalVariable(),
                        ipvcur->getRefToDLocalVariableDNonLocalVariable(), 
                        stiff);
  else
    _nldlaw->constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),
                         stiff);


  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);

}

double GenericCrackPhaseFieldDG3DMaterialLaw::scaleFactor() const{return _nldlaw->scaleFactor();};

void GenericCrackPhaseFieldDG3DMaterialLaw::setMacroSolver(const nonLinearMechSolver* sv){
	dG3DMaterialLaw::setMacroSolver(sv);
	if (_nldlaw != NULL){
		_nldlaw->setMacroSolver(sv);
	}
};


const materialLaw &GenericCrackPhaseFieldDG3DMaterialLaw::getConstRefMechanicalMaterialLaw() const
{
    return _nldlaw->getConstRefMechanicalMaterialLaw();
}
materialLaw &GenericCrackPhaseFieldDG3DMaterialLaw::getRefMechanicalMaterialLaw()
{
    return _nldlaw->getRefMechanicalMaterialLaw();
}

void GenericCrackPhaseFieldDG3DMaterialLaw::setMechanicalMaterialLaw(const dG3DMaterialLaw *mlaw)
{
    _nldlaw->setMechanicalMaterialLaw(mlaw->getConstNonLinearSolverMaterialLaw());

    //_lawGenericRM->ElasticStiffness(&elasticStiffness);
    elasticStiffness=mlaw->elasticStiffness;
    
}
