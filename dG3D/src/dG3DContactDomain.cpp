//
// Description:  rigid contact domain
//
//
// Author:  <Van Dung NGUYEN>, (C) 2015
//
// Copyright: See COPYING file that comes with this distribution
//

#include "dG3DContactDomain.h"
#include "dG3DContactFunctionSpace.h"

void dG3DRigidCylinderContact::setDomainAndFunctionSpace(partDomain *dom){
  _dom = dom;
  FunctionSpace<double> *spdom = static_cast<FunctionSpace<double>*>(dom->getFunctionSpace());
  if(_space != NULL) delete _space;
  _space = new dG3DRigidContactSpace(_phys,spdom,this->getGC());
};

void dG3DRigidConeContact::setDomainAndFunctionSpace(partDomain *dom){
  _dom = dom;
  FunctionSpace<double> *spdom = static_cast<FunctionSpace<double>*>(dom->getFunctionSpace());
  if(_space != NULL) delete _space;
  _space = new dG3DRigidContactSpace(_phys,spdom,this->getGC());
};


dG3DRigidPlaneContactDomain::dG3DRigidPlaneContactDomain(const int tag, const int dimMaster, const int phyMaster,
                            const int dimSlave, const int phySlave, const int phyPointBase,
                            const double nx, const double ny, const double nz, const double penalty, const double thick,
                            const double rho, elementFilter *fslave):
                            rigidPlaneContactDomain(tag, dimMaster, phyMaster, dimSlave, phySlave,phyPointBase,
                            nx,ny,nz, penalty, thick,rho, fslave){};

dG3DRigidPlaneContactDomain::dG3DRigidPlaneContactDomain(const dG3DRigidPlaneContactDomain& src):rigidPlaneContactDomain(src){};

void dG3DRigidPlaneContactDomain::setDomainAndFunctionSpace(partDomain *dom){
  _dom = dom;
  FunctionSpace<double> *spdom = static_cast<FunctionSpace<double>*>(dom->getFunctionSpace());
  if(_space != NULL) delete _space;
  _space = new dG3DRigidContactSpace(_phys,spdom,this->getGC());
};

dG3DRigidSphereContactDomain::dG3DRigidSphereContactDomain(const int tag, const int dimMaster, const int physMaster,
                             const int dimSlave, const int physSlave, const int physPointBase,
                             const double radius, const double penalty, const double rho, elementFilter *fslave):                            
                             rigidSphereContactDomain(tag, dimMaster, physMaster, dimSlave, physSlave, physPointBase,
                             radius, penalty, rho, fslave){};

dG3DRigidSphereContactDomain::dG3DRigidSphereContactDomain(const dG3DRigidSphereContactDomain& src):rigidSphereContactDomain(src){};

void dG3DRigidSphereContactDomain::setDomainAndFunctionSpace(partDomain *dom){
  _dom = dom;
  FunctionSpace<double> *spdom = static_cast<FunctionSpace<double>*>(dom->getFunctionSpace());
  if(_space != NULL) delete _space;
  _space = new dG3DRigidContactSpace(_phys,spdom,this->getGC());
};

dG3DNodeOnSurfaceContactDomain::dG3DNodeOnSurfaceContactDomain(const int tag, const int dimMaster, const int phyMaster,
                            const int dimSlave, const int phySlave, const double penalty, const double thick,
                            elementFilter *fslave):
                            nodeOnSurfaceDefoDefoContactDomain(tag, dimMaster, phyMaster, dimSlave, phySlave, penalty, thick,fslave){};

dG3DNodeOnSurfaceContactDomain::dG3DNodeOnSurfaceContactDomain(const dG3DNodeOnSurfaceContactDomain& src):nodeOnSurfaceDefoDefoContactDomain(src){};


void dG3DNodeOnSurfaceContactDomain::insertSlaveMasterDomainsAndFunctionSpace(partDomain *slaveDom, elementGroup *gSlave, partDomain *masterDom, elementGroup *gMaster)
{
  _vdomSlave = slaveDom;
  FunctionSpace<double> *spSlave; 

  nonLinearBoundaryCondition::location bc_location;
  if(getDimMaster()==2)
  {
    bc_location=nonLinearBoundaryCondition::ON_FACE;
    if(getDimSlave()==3)
      spSlave= static_cast<FunctionSpace<double>*>(_vdomSlave->getFunctionSpace());
    else if(getDimSlave()==2)
    {
      nonLinearNeumannBC itbc;
      itbc.onWhat=bc_location;
      itbc._NeumannBCType = nonLinearNeumannBC::SCALAR_FLUX; //or should be PRESSURE?
      itbc.g = new elementGroup(*gSlave); //new elementGroup (getDimMaster(), getPhysMaster()); //or use gMaster?
      itbc._tag=getPhysSlave();
      spSlave= static_cast<FunctionSpace<double>*>(slaveDom->getSpaceForBC(nonLinearBoundaryCondition::NEUMANN,bc_location, nonLinearNeumannBC::SCALAR_FLUX,
                                                        0,mixedFunctionSpaceBase::DOF_STANDARD,gSlave));
    }
    else
      Msg::Error("dG3DNodeOnSurfaceContactDomain::insertMasterDomainAndFunctionSpace not implemented for this dimension");
 
  }
  else if(getDimMaster()==1)
  {
    bc_location=nonLinearBoundaryCondition::ON_EDGE;
    if(getDimSlave()==2)
      spSlave= static_cast<FunctionSpace<double>*>(_vdomSlave->getFunctionSpace());
    else if(getDimSlave()==1)
    {
      nonLinearNeumannBC itbc;
      itbc.onWhat=bc_location;
      itbc._NeumannBCType = nonLinearNeumannBC::SCALAR_FLUX; //or should be PRESSURE?
      itbc.g = new elementGroup(*gSlave); //new elementGroup (getDimMaster(), getPhysMaster()); //or use gMaster?
      itbc._tag=getPhysSlave();
      spSlave= static_cast<FunctionSpace<double>*>(slaveDom->getSpaceForBC(nonLinearBoundaryCondition::NEUMANN,bc_location, nonLinearNeumannBC::SCALAR_FLUX,
                                                        0,mixedFunctionSpaceBase::DOF_STANDARD,gSlave));
    }
    else
      Msg::Error("dG3DNodeOnSurfaceContactDomain::insertMasterDomainAndFunctionSpace not implemented for this dimension");
  }
  else
    Msg::Error("dG3DNodeOnSurfaceContactDomain::insertMasterDomainAndFunctionSpace not implemented for this dimension");

  nonLinearNeumannBC itbc;
  itbc.onWhat=bc_location;
  itbc._NeumannBCType = nonLinearNeumannBC::SCALAR_FLUX; //or should be PRESSURE?
  itbc.g = new elementGroup(*gMaster); //new elementGroup (getDimMaster(), getPhysMaster()); //or use gMaster?
  itbc._tag=getPhysMaster();
  

  //we use scalar flux to have all the componant  //or should be PRESSURE?
  FunctionSpaceBase *spMaster = masterDom->getSpaceForBC(nonLinearBoundaryCondition::NEUMANN,bc_location, nonLinearNeumannBC::SCALAR_FLUX,
                                                        0,mixedFunctionSpaceBase::DOF_STANDARD,gMaster);
  dG3DDefoDefoContactSpace *sp = new dG3DDefoDefoContactSpace(getPhysMaster(), spSlave, spMaster);

  QuadratureBase *integ = masterDom->getQuadratureRulesForNeumannBC(itbc);

  _vspace.push_back(sp); //here we want to use the defoDefoContactFunctionSpace

  std::set<contactElement*> gCE;
  int nb=0;
  for (elementGroup::elementContainer::const_iterator iteSlave = gSlave->begin(); iteSlave != gSlave->end(); iteSlave++){
    for (elementGroup::elementContainer::const_iterator iteMaster = gMaster->begin(); iteMaster != gMaster->end(); iteMaster++){
      bool commonNode=false; //avoid elements with commont verteces with autocontact
      int nSlave=(iteSlave->second)->getNumVertices();
      int nMaster=(iteMaster->second)->getNumVertices();
      for(int iSlave=0; iSlave<nSlave; iSlave++)
      {
        for(int iMaster=0; iMaster<nMaster; iMaster++)
        {
          if((iteSlave->second)->getVertex(iSlave)->getNum()==(iteMaster->second)->getVertex(iMaster)->getNum())
          {
            commonNode==true;
            break;
          }
        }
        if(commonNode)
          break;
      }
      if(!commonNode)
      {
        contactElement *ce =new nodeOnSurfaceContactElement(nb,iteMaster->second,iteSlave->second);
        gCE.insert(ce);
      }
    }
  }
  _group.push_back(gCE);

  _vquadratureMaster.push_back(integ);
  _vdomMaster.push_back(masterDom); // Used to create the term after
 
}

