//
//
// Description: Class of materials for non linear dg
//
//
// Author:  <L. Noels>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "FractureCohesiveDG3DMaterialLaw.h"
#include "dG3DMultiscaleCohesiveMaterialLaw.h"
#include "MInterfaceElement.h"
#include "nonLinearMechSolver.h"
#include "dG3DMultiscaleMaterialLaw.h"

// function to generate random factor for fracture strength
// srand initiated in constructor of NonLinearMechSolver
double FractureByCohesive3DLaw::frand(const double a,const double b){
  return (rand()/(double)RAND_MAX) * (b-a) + a;
}

FractureByCohesive3DLaw::FractureByCohesive3DLaw(const int num,const int nbulk,
                                                       const int ncoh) : dG3DMaterialLaw(num,0,false),
                                                                          fractureBy2Laws<dG3DMaterialLaw,Cohesive3DLaw>(num,nbulk,ncoh)

{
}
FractureByCohesive3DLaw::FractureByCohesive3DLaw(const FractureByCohesive3DLaw &source) :
                                                                          dG3DMaterialLaw(source),
                                                                          fractureBy2Laws<dG3DMaterialLaw,Cohesive3DLaw>(source)

{
}

void FractureByCohesive3DLaw::createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_,
                                          const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // FractureCohesive3DIPVariable are created only on interface element. On bulk element bulk IPvariable are created
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele!=NULL)
  {
    const double fscmin = _mfrac->getFractureStrengthFactorMin();
    const double fscmax = _mfrac->getFractureStrengthFactorMax();
    double fsc = frand(fscmin,fscmax);
    IPVariable* ipvi = new FractureCohesive3DIPVariable(fsc);
    IPVariable* ipv1 = new FractureCohesive3DIPVariable(fsc);
    IPVariable* ipv2 = new FractureCohesive3DIPVariable(fsc);
    FractureCohesive3DIPVariable *ipvf = static_cast<FractureCohesive3DIPVariable*>(ipvi);
    IPVariable* ipv=NULL;
    _mbulk->createIPVariable(ipv,hasBodyForce, ele,nbFF_,GP,gpt);
    ipvf->setIPvBulk(ipv);
    ipvf = static_cast<FractureCohesive3DIPVariable*>(ipv1);
    ipv=NULL;
    _mbulk->createIPVariable(ipv, hasBodyForce, ele,nbFF_,GP,gpt);
    ipvf->setIPvBulk(ipv);
    ipvf = static_cast<FractureCohesive3DIPVariable*>(ipv2);
    ipv=NULL;
    _mbulk->createIPVariable(ipv, hasBodyForce, ele,nbFF_,GP,gpt);
    ipvf->setIPvBulk(ipv);
    //to have the same random variables at the three states


    // Why cohesive IPvariable are created here ?
    ipvf = static_cast<FractureCohesive3DIPVariable*>(ipvi);
    IPVariable* ipvc=NULL;
    _mfrac->createIPVariable(ipvc, hasBodyForce, ele,nbFF_,GP,gpt);
    ipvf->setIPvFrac(ipvc);
    ipvf = static_cast<FractureCohesive3DIPVariable*>(ipv1);
    ipvc=NULL;
    _mfrac->createIPVariable(ipvc, hasBodyForce, ele,nbFF_,GP,gpt);
    ipvf->setIPvFrac(ipvc);
    ipvf = static_cast<FractureCohesive3DIPVariable*>(ipv2);
    ipvc=NULL;
    _mfrac->createIPVariable(ipvc, hasBodyForce, ele,nbFF_,GP,gpt);
    ipvf->setIPvFrac(ipvc);

    if(ips != NULL) delete ips;
    ips = new IP3State(state_,ipvi,ipv1,ipv2);
  }
  else
  {
    _mbulk->createIPState(ips, hasBodyForce, state_,ele,nbFF_,GP,gpt);
  }
}

void FractureByCohesive3DLaw::createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele!=NULL) // No fracture on bulk points
  {
    if(ipv != NULL) delete ipv;
    ipv = new FractureCohesive3DIPVariable();
    FractureCohesive3DIPVariable *ipvf = static_cast<FractureCohesive3DIPVariable*>(ipv);
    IPVariable* ipv2;
    _mbulk->createIPVariable(ipv2, hasBodyForce, ele,nbFF_,GP,gpt);
    ipvf->setIPvBulk(ipv2);
    IPVariable* ipv3;
    _mfrac->createIPVariable(ipv3, hasBodyForce, ele,nbFF_,GP,gpt);
    ipvf->setIPvFrac(ipv3);
  }
  else
  {
    _mbulk->createIPVariable(ipv, hasBodyForce, ele,nbFF_,GP,gpt);
  }
}

void FractureByCohesive3DLaw::initialBroken(IPStateBase *ips) const
{
  FractureCohesive3DIPVariable *ipvfcur = static_cast<FractureCohesive3DIPVariable*>(ips->getState(IPStateBase::current));
  FractureCohesive3DIPVariable *ipvfprev = static_cast<FractureCohesive3DIPVariable*>(ips->getState(IPStateBase::previous));
  FractureCohesive3DIPVariable *ipvfinit = static_cast<FractureCohesive3DIPVariable*>(ips->getState(IPStateBase::initial));
  ipvfcur->broken();
  ipvfinit->broken();
  ipvfprev->broken();
  static STensor3 cauchy;
  static SVector3 f0;
  static SVector3 du;
  static SVector3 normDir;
  normDir = ipvfprev->getConstRefToCurrentOutwardNormal();
  normDir.normalize();

  ipvfcur->initializeFracture(du,_mfrac->getKp(),_mfrac->getSigmac(),_mfrac->getSigmac(),0.,_mfrac->getGc(),_mfrac->getBeta(),true,cauchy,
                          _mfrac->getKn(),normDir,f0,ipvfcur->getIPvBulk());
  ipvfprev->initializeFracture(du,_mfrac->getKp(),_mfrac->getSigmac(),_mfrac->getSigmac(),0.,_mfrac->getGc(),_mfrac->getBeta(),true,cauchy,
                          _mfrac->getKn(),normDir,f0,ipvfprev->getIPvBulk());
  ipvfinit->initializeFracture(du,_mfrac->getKp(),_mfrac->getSigmac(),_mfrac->getSigmac(),0.,_mfrac->getGc(),_mfrac->getBeta(),true,cauchy,
                          _mfrac->getKn(),normDir,f0,ipvfinit->getIPvBulk());
}


void FractureByCohesive3DLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  dG3DIPVariableBase* dgipv = static_cast<dG3DIPVariableBase*>(ipv);
	const dG3DIPVariableBase* dgipvprev = static_cast<const dG3DIPVariableBase*>(ipvp);
  // Now check for fracture on interface
  // for interface
  if(dgipv->isInterface())
  {
		FractureCohesive3DIPVariable *ipvfcur = static_cast<FractureCohesive3DIPVariable*>(ipv);
		const FractureCohesive3DIPVariable *ipvfprev = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    _mbulk->checkInternalState(ipvfcur->getIPvBulk(),ipvfprev->getIPvBulk());
    if (ipvfcur->isbroken()){
      _mfrac->checkInternalState(ipvfcur,ipvfprev);
    }
  }
  else{
    _mbulk->checkInternalState(ipv,ipvp);
  }
};

void FractureByCohesive3DLaw::stress(IPVariable*ipv, const IPVariable*ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
	dG3DIPVariableBase* dgipv = static_cast<dG3DIPVariableBase*>(ipv);
	const dG3DIPVariableBase* dgipvprev = static_cast<const dG3DIPVariableBase*>(ipvp);
  // Now check for fracture on interface
  // for interface
  if(dgipv->isInterface() and checkfrac)
  {
		FractureCohesive3DIPVariable *ipvfcur = static_cast<FractureCohesive3DIPVariable*>(ipv);
		const FractureCohesive3DIPVariable *ipvfprev = static_cast<const FractureCohesive3DIPVariable*>(ipvp);

    //Msg::Info("Interface");
    // set data from inteface to bulk if nessessary
    if (ipvfcur->isbroken()){
      _mfrac->transferInterfaceDataToBulk(ipv,ipvp,stiff);
    }

    // compute bulk ipvariable at interface
		// if ipv is fully broken, bulk computation is nolonger necessary
		// interface law still used to prevent penetration
		if (!ipvfcur->isDeleted()){
    	_mbulk->stress(ipv, ipvp, stiff, checkfrac, dTangent);
		}

    // compute interface ip at interface
    if (ipvfcur->isbroken()){
      // if IP is previously broken
      _mfrac->stress(ipv, ipvp, stiff, checkfrac, dTangent);
    }
    //Msg::Info("OK Interface - ------------");
  }
  else{
    // for builk ip inside element
    _mbulk->stress(ipv, ipvp, stiff, checkfrac, dTangent);
  }
};


MultiscaleFractureByCohesive3DLaw::MultiscaleFractureByCohesive3DLaw(const int num,const int nbulk, const int ncoh):
      FractureByCohesive3DLaw(num,nbulk,ncoh),numericalMaterialBase(),_nummat(NULL),_interfaceLaw(NULL){};
MultiscaleFractureByCohesive3DLaw::MultiscaleFractureByCohesive3DLaw(const MultiscaleFractureByCohesive3DLaw& src):
      FractureByCohesive3DLaw(src),numericalMaterialBase(src),_nummat(src._nummat),_interfaceLaw(src._interfaceLaw){};

void MultiscaleFractureByCohesive3DLaw::initLaws(const std::map<int,materialLaw*> &maplaw){
  if(!this->_initialized){
    FractureByCohesive3DLaw::initLaws(maplaw);
    _initialized = true;

    _nummat = dynamic_cast<dG3DMultiscaleMaterialLawBase*>(this->getBulkLaw());
    _interfaceLaw = dynamic_cast<GeneralMultiscaleBulkFollwedCohesive3DLaw*>(this->getFractureLaw());

    if (_nummat == NULL) Msg::Error("bulk law is not a multiscale one in MultiscaleFractureByCohesive3DLaw::initLaws");

  }
};

void MultiscaleFractureByCohesive3DLaw::createIPState(const bool isSolve, IPStateBase* &ips,bool hasBodyForce, const bool* state_,
                                          const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele!=NULL)
  {
    const double fscmin = _mfrac->getFractureStrengthFactorMin();
    const double fscmax = _mfrac->getFractureStrengthFactorMax();
    double fsc = frand(fscmin,fscmax);
    IPVariable* ipvi = new MultiscaleFractureCohesive3DIPVariable(fsc);
    IPVariable* ipv1 = new MultiscaleFractureCohesive3DIPVariable(fsc);
    IPVariable* ipv2 = new MultiscaleFractureCohesive3DIPVariable(fsc);
    FractureCohesive3DIPVariable *ipvf = static_cast<FractureCohesive3DIPVariable*>(ipvi);
    IPVariable* ipv=NULL;
    _mbulk->createIPVariable(ipv, hasBodyForce, ele,nbFF_,GP,gpt);
    ipvf->setIPvBulk(ipv);

    ipvf = static_cast<FractureCohesive3DIPVariable*>(ipv1);
    ipv=NULL;
    _mbulk->createIPVariable(ipv, hasBodyForce, ele,nbFF_,GP,gpt);
    ipvf->setIPvBulk(ipv);
    ipvf = static_cast<FractureCohesive3DIPVariable*>(ipv2);
    ipv=NULL;
    _mbulk->createIPVariable(ipv, hasBodyForce, ele,nbFF_,GP,gpt);
    ipvf->setIPvBulk(ipv);

    // Why cohesive IPvariable are created here ?
    ipvf = static_cast<FractureCohesive3DIPVariable*>(ipvi);
    IPVariable* ipvc=NULL;
    _mfrac->createIPVariable(ipvc, hasBodyForce, ele,nbFF_,GP,gpt);
    ipvf->setIPvFrac(ipvc);
    ipvf = static_cast<FractureCohesive3DIPVariable*>(ipv1);
    ipvc=NULL;
    _mfrac->createIPVariable(ipvc, hasBodyForce, ele,nbFF_,GP,gpt);
    ipvf->setIPvFrac(ipvc);
    ipvf = static_cast<FractureCohesive3DIPVariable*>(ipv2);
    ipvc=NULL;
    _mfrac->createIPVariable(ipvc, hasBodyForce, ele,nbFF_,GP,gpt);
    ipvf->setIPvFrac(ipvc);

    if(ips != NULL) delete ips;
		ips = new IP3State(state_,ipvi,ipv1,ipv2);

    if (isSolve){
			 // if bulk law is a multiscale law
			int el = ele->getNum();
			nonLinearMechSolver* solver   = this->createMicroSolver(el,gpt);
			if (_interfaceLaw != NULL){
        bool withNormal = _interfaceLaw->getCheckFailureOnsetWithInterfaceNormal();
        solver->setCheckFailureOnset(true,withNormal); // set check failure onset
				solver->setLostSolutionUniquenssTolerance(_interfaceLaw->getLostSolutionUniquenssTolerance());
        solver->setRVELengthInNormalDirection(_interfaceLaw->getCharacteristicLength());

				SVector3 n, t, b;
				// get macrosolver
				const nonLinearMechSolver* macroSolver = _mbulk->getMacroSolver();
				if (macroSolver== NULL){
					Msg::Error("macro solver has not set yet");
				}

        macroSolver->getLocalBasis(ele,gpt,n,t,b);
        if (withNormal){
          solver->setLocalizationNormal(n);
				}
				//
				//printf("rank %d normal of interface: ele %d gp %d: n = [%f %f %f] \n",Msg::GetCommRank(),ele->getNum(),gpt,n[0],n[1],n[2]);
				if (_interfaceLaw->getRotationOfRVEFollowingInterfaceLocalBasis() and !solver->GModelIsRotated()){
					solver->rotateModel(n,t,b);
				}

        // update microbc with local basis
				nonLinearMicroBC* mbc  = solver->getMicroBC();
				if (mbc->getType() == nonLinearMicroBC::OrthogonalDirectionalMixedBC){
					printf("set pbc %d n = %f %f %f t= %f %f %f",Msg::GetCommRank(),n[0],n[1],n[2],t[0],t[1],t[2]);
					nonLinearOrthogonalMixedBCDirectionFollowing* mixedBC = static_cast<nonLinearOrthogonalMixedBCDirectionFollowing*>(mbc);
					mixedBC->clearDirs();
					if (mbc->getDim() == 2){
						mixedBC->setKUBCDirection(t);
						mixedBC->setSUBCDirection(n);
					}
					else if (mbc->getDim() == 3){
						mixedBC->setKUBCDirection(t);
						mixedBC->setSUBCDirection(n);
						mixedBC->setKUBCDirection(b);
					}
				}
				else if (mbc->getType() == nonLinearMicroBC::ShiftedPBC){
					nonLinearShiftedPeriodicBC* pbc = static_cast<nonLinearShiftedPeriodicBC*>(mbc);
					if (mbc->getDim() == 2){
            pbc->setShiftPBCNormal(n);
					}
					else{
						Msg::Error("shifted BC is not defined for 3D problem");
					}
				}

				//  change to new BC
				if (_interfaceLaw->isSwitchedToFailureBC()){
					printf("set microBC for failure\n");
					solver->setSwitchMicroBCFlag(true);
					solver->addMicroBCForFailure(_interfaceLaw->getMicroBCFailure());


					nonLinearMicroBC* failureBC = solver->getMicroBCFailure();

					if (failureBC->getType() == nonLinearMicroBC::OrthogonalDirectionalMixedBC){
						printf("set OrthogonalDirectionalMixedBC %d n = %f %f %f t= %f %f %f",Msg::GetCommRank(),n[0],n[1],n[2],t[0],t[1],t[2]);
						nonLinearOrthogonalMixedBCDirectionFollowing* failuremixedBC = static_cast<nonLinearOrthogonalMixedBCDirectionFollowing*>(mbc);
						failuremixedBC->clearDirs();
						if (failureBC->getDim() == 2){
							failuremixedBC->setKUBCDirection(t);
							failuremixedBC->setSUBCDirection(n);
						}
						else if (failureBC->getDim() == 3){
							failuremixedBC->setKUBCDirection(t);
							failuremixedBC->setSUBCDirection(n);
							failuremixedBC->setKUBCDirection(b);
						}
					}

				}
			}
      else{
        Msg::Error("GeneralMultiscaleBulkFollwedCohesive3DLaw must be used in MultiscaleFractureByCohesive3DLaw::createIPState");
      }
			solver->initMicroSolver();
			ips->setMicroSolver(solver);
    }

  }
  else{
    _nummat->createIPState(isSolve,ips, hasBodyForce, state_,ele,nbFF_,GP,gpt);
  }
}

void MultiscaleFractureByCohesive3DLaw::createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state,
                                 const MElement *ele, const int nbFF, const IntPt *GP, const int gpt) const{
  MultiscaleFractureByCohesive3DLaw::createIPState(true,ips, hasBodyForce, state,ele,nbFF,GP,gpt);
};

void MultiscaleFractureByCohesive3DLaw::initialBroken(IPStateBase *ips) const
{
  FractureByCohesive3DLaw::initialBroken(ips);
	nonLinearMechSolver* solver = ips->getMicroSolver();
	if (solver != NULL){
		solver->brokenSolver(true);
	}
}

void MultiscaleFractureByCohesive3DLaw::stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff, const bool checkfrac, const bool dTangent){

  FractureByCohesive3DLaw::stress(ipv,ipvprev,stiff,checkfrac,dTangent);
};

nonLinearMechSolver* MultiscaleFractureByCohesive3DLaw::createMicroSolver(const int ele, const int gpt) const {
  if (_nummat) return _nummat->createMicroSolver(ele,gpt);
  else return NULL;
};


void MultiscaleFractureByCohesive3DLaw::printMeshIdMap() const {
  if (_nummat) _nummat->printMeshIdMap();
};
//this function provides an id for a GP
void MultiscaleFractureByCohesive3DLaw::assignMeshId(const int e, const int gpt) {
  if (_nummat) _nummat->assignMeshId(e,gpt);
};
void MultiscaleFractureByCohesive3DLaw::setMeshId(const int e, const int gpt, const int id){
  if (_nummat) _nummat->setMeshId(e,gpt,id);
};
// this function get mesh id
int MultiscaleFractureByCohesive3DLaw::getMeshId(const int e, const int gpt) const {
  if (_nummat) return _nummat->getMeshId(e,gpt);
  else return 0;
};
#if defined(HAVE_MPI)
// these function used for comunicate MPI
int MultiscaleFractureByCohesive3DLaw::getNumberValuesMPI() const {
  if (_nummat) return _nummat->getNumberValuesMPI();
  else return 0;
};
void MultiscaleFractureByCohesive3DLaw::fillValuesMPI(int* val) const {
  if (_nummat) _nummat->fillValuesMPI(val);
};
void MultiscaleFractureByCohesive3DLaw::getValuesMPI(const int* val, const int size){
  if (_nummat) _nummat->getValuesMPI(val,size);
};
#endif //HAVE_MPI
