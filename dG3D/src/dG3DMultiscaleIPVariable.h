//
// Author: Van Dung NGUYEN, (C) 2016
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef DG3DMULTISCALEIPVARIABLE_H_
#define DG3DMULTISCALEIPVARIABLE_H_

#include "dG3DIPVariable.h"
#include "FractureCohesiveDG3DIPVariable.h"

class dG3DMultiscaleIPVariable : public dG3DIPVariable{
	protected:
		bool _solverBroken;
    bool _dissipationBlocked;
		double _elasticEnergy;
		double _plasticEnergy;
		double _irreversibleEnergy;
		STensor3 _DirreversibleEnergyDF;
    double _lostEllipticity; // failure criterion

  public:
    dG3DMultiscaleIPVariable(const bool createForceForHO, const bool oninter) : dG3DIPVariable(createForceForHO,oninter),
		_solverBroken(false),_dissipationBlocked(false),
		_elasticEnergy(0.),_plasticEnergy(0.),_irreversibleEnergy(0.),_DirreversibleEnergyDF(0.),
    _lostEllipticity(0.){}
    dG3DMultiscaleIPVariable(const dG3DMultiscaleIPVariable& src) : dG3DIPVariable(src),
		_solverBroken(src._solverBroken),_dissipationBlocked(src._dissipationBlocked),
    _elasticEnergy(src._elasticEnergy),
		_plasticEnergy(src._plasticEnergy),_irreversibleEnergy(src._irreversibleEnergy),
    _DirreversibleEnergyDF(src._DirreversibleEnergyDF),_lostEllipticity(src._lostEllipticity){};
    virtual dG3DMultiscaleIPVariable& operator=(const IPVariable& src){
      dG3DIPVariable::operator=(src);
      const dG3DMultiscaleIPVariable* psrc = dynamic_cast<const dG3DMultiscaleIPVariable*>(&src);
      if (psrc != NULL){
				_solverBroken = psrc->_solverBroken;
        _dissipationBlocked = psrc->_dissipationBlocked;
				_elasticEnergy = psrc->_elasticEnergy;
				_plasticEnergy = psrc->_plasticEnergy;
				_irreversibleEnergy = psrc->_irreversibleEnergy;
				_DirreversibleEnergyDF = psrc->_DirreversibleEnergyDF;
        _lostEllipticity = psrc->_lostEllipticity;
      }
      return *this;
    };
    virtual ~dG3DMultiscaleIPVariable(){}

    virtual IPVariable* getInternalData() {return NULL;};
    virtual const IPVariable* getInternalData()  const {return NULL;};

    virtual double& getRefToLostEllipticityCriterion() {return _lostEllipticity;};
    virtual const double& getConstRefToLostEllipticityCriterion() const {return _lostEllipticity;};

    virtual bool dissipationIsBlocked() const { return _dissipationBlocked;};
    virtual void blockDissipation(const bool bl);

		virtual void brokenSolver(const bool bl);
    virtual bool solverIsBroken() const {return _solverBroken;};

		virtual double defoEnergy() const { return _elasticEnergy;};
		virtual double& getRefToDefoEnergy() { return _elasticEnergy;};

		virtual double plasticEnergy() const { return _plasticEnergy;};
		virtual double& getRefToPlasticEnergy() { return _plasticEnergy;};

		virtual double irreversibleEnergy() const { return _irreversibleEnergy;};
		virtual double& getRefToIrreversibleEnergy() { return _irreversibleEnergy;};

		virtual const STensor3& getConstRefToDIrreversibleEnergyDDeformationGradient() const { return _DirreversibleEnergyDF;};

		virtual STensor3& getRefToDIrreversibleEnergyDDeformationGradient() { return _DirreversibleEnergyDF;};

    virtual double get(const int comp) const;

    virtual IPVariable* clone() const {return new dG3DMultiscaleIPVariable(*this);};
    virtual void restart(){
      dG3DIPVariable::restart();
			restartManager::restart(_solverBroken);
      restartManager::restart(_dissipationBlocked);
			restartManager::restart(_elasticEnergy);
			restartManager::restart(_plasticEnergy);
			restartManager::restart(_irreversibleEnergy);
			restartManager::restart(_DirreversibleEnergyDF);
      restartManager::restart(_lostEllipticity);
    };



    #if defined(HAVE_MPI)
    virtual int getMacroNumberElementDataSendToMicroProblem() const;
      // get number of values obtained by microscopic analysis to send to macroscopic analysis
    virtual int getMicroNumberElementDataSendToMacroProblem() const;
      // get macroscopic kinematic data to send to microscopic problem
    virtual void getMacroDataSendToMicroProblem(double* val) const;
      // get computed data obtaind by microscopic analysis to send to macroscopic analysis
    virtual void getMicroDataToMacroProblem(double* val) const;
      // set the received data from microscopic analysis to microscopic analysis
    virtual void setReceivedMacroDataToMicroProblem(const double* val);
      // set the received data from microscopic analysis to macroscopic analysis
    virtual void setReceivedMicroDataToMacroProblem(const double* val);
    #endif

};


class MultiscaleThermoMechanicsDG3DIPVariable : public ThermoMechanicsDG3DIPVariableBase{
	protected:


	public:
		MultiscaleThermoMechanicsDG3DIPVariable(const bool createForceForHO, const bool oninter): ThermoMechanicsDG3DIPVariableBase(createForceForHO,oninter){};
		MultiscaleThermoMechanicsDG3DIPVariable(const MultiscaleThermoMechanicsDG3DIPVariable& src):
							ThermoMechanicsDG3DIPVariableBase(src){}
		virtual MultiscaleThermoMechanicsDG3DIPVariable& operator = (const IPVariable& src){
			ThermoMechanicsDG3DIPVariableBase::operator =(src);
			return *this;
		};
		virtual ~MultiscaleThermoMechanicsDG3DIPVariable(){};

		virtual IPVariable* getInternalData() {return NULL;};
    virtual const IPVariable* getInternalData()  const {return NULL;};

		virtual double defoEnergy() const;
		virtual double getInternalEnergyExtraDofDiffusion() const;
		virtual double plasticEnergy() const;
    virtual double damageEnergy() const {return 0.;};

		virtual IPVariable* clone() const {return new MultiscaleThermoMechanicsDG3DIPVariable(*this);};
		virtual void restart(){
			MultiscaleThermoMechanicsDG3DIPVariable::restart();
		}

		#if defined(HAVE_MPI)
		// using in multiscale analysis with MPI
		// get number of values to set the kinematics of microscopic problem
		virtual int getMacroNumberElementDataSendToMicroProblem() const;
		// get number of values obtained by microscopic analysis to send to macroscopic analysis
		virtual int getMicroNumberElementDataSendToMacroProblem() const ;
		// get macroscopic kinematic data to send to microscopic problem
		virtual void getMacroDataSendToMicroProblem(double* val) const;
		// get computed data obtaind by microscopic analysis to send to macroscopic analysis
		virtual void getMicroDataToMacroProblem(double* val) const ;
		// set the received data from microscopic analysis to microscopic analysis
		virtual void setReceivedMacroDataToMicroProblem(const double* val);
		// set the received data from microscopic analysis to macroscopic analysis
		virtual void setReceivedMicroDataToMacroProblem(const double* val);
		#endif
};

#endif // DG3DMULTISCALEIPVARIABLE_H_
