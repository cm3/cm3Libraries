//
// C++ Interface: terms
//
// Description: Class of terms for dg non linear shell
//
//
// Author:  <V.D. Nguyen>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef AXISYMMETRICDG3DTERMS_H
#define AXISYMMETRICDG3DTERMS_H

#include "dG3DTerms.h"
#include <math.h>
#include "highOrderTensor.h"

class axisymmetricG3DFiniteStrainsPressureTerm : public g3DLinearTerm<double>
{
 protected:
  const simpleFunctionTime<double> *Load;
  const unknownField* _ufield;
  
 public :
  axisymmetricG3DFiniteStrainsPressureTerm(FunctionSpace<double>& space1_, const unknownField* ufield,
                          const simpleFunctionTime<double> *Load_) : g3DLinearTerm<double>(space1_),Load(Load_), _ufield(ufield)
  {

  }
  virtual ~axisymmetricG3DFiniteStrainsPressureTerm()
  {
  
  }
  virtual void set(const fullVector<double> *datafield){}
  virtual const bool isData() const{return false;}

  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullVector<double> > &mv) const
  {
    Msg::Error("Define me get by integration point axisymmetricG3DFiniteStrainsPressureTerm");
  }
  virtual void get(MElement *ele,int npts,IntPt *GP,fullVector<double> &m) const;
  virtual LinearTermBase<double>* clone () const
  {
    axisymmetricG3DFiniteStrainsPressureTerm* pressTerm =  new axisymmetricG3DFiniteStrainsPressureTerm(this->space1,_ufield,Load);
    pressTerm->setDim(this->getDim());
    pressTerm->setNonLocalEqRatio(this->getNonLocalEqRatio());
    pressTerm->setNumNonLocalVariable(this->getNumNonLocalVariable());
    pressTerm->setNumConstitutiveExtraDofDiffusionVariable(this->getNumConstitutiveExtraDofDiffusionVariable());
    pressTerm->setConstitutiveExtraDofDiffusionEqRatio(this->getConstitutiveExtraDofDiffusionEqRatio());
    pressTerm->setConstitutiveExtraDofDiffusionUseEnergyConjugatedField(this->getConstitutiveExtraDofDiffusionUseEnergyConjugatedField());
    pressTerm->setConstitutiveExtraDofDiffusionAccountFieldSource(this->getConstitutiveExtraDofDiffusionAccountFieldSource());
    pressTerm->setConstitutiveExtraDofDiffusionAccountMecaSource(this->getConstitutiveExtraDofDiffusionAccountMecaSource());
    return pressTerm;
  }
};

class axisymmetricG3DFiniteStrainsPressureBilinearTerm : public g3DBilinearTerm<double,double>
{
 protected:
  const simpleFunctionTime<double> *Load;
  const unknownField* _ufield;

 public :
  axisymmetricG3DFiniteStrainsPressureBilinearTerm(FunctionSpace<double>& space1_, const unknownField* ufield,
                          const simpleFunctionTime<double> *Load_) : g3DBilinearTerm<double,double>(space1_,space1_),
                            Load(Load_),_ufield(ufield)
  {

  }
  virtual ~axisymmetricG3DFiniteStrainsPressureBilinearTerm()
  {
  }
  
  virtual void set(const fullVector<double> *datafield){}
  virtual const bool isData() const{return false;}


  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const
  {
    Msg::Error("Define me get by integration point axisymmetricG3DFiniteStrainsPressureBilinearTerm");
  }
  virtual void get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const;
  
  virtual BilinearTermBase* clone () const
  {
    axisymmetricG3DFiniteStrainsPressureBilinearTerm* pressBiTerm  = new axisymmetricG3DFiniteStrainsPressureBilinearTerm(this->space1,_ufield,Load);
    pressBiTerm->setDim(this->getDim());
    pressBiTerm->setNonLocalEqRatio(this->getNonLocalEqRatio());
    pressBiTerm->setNumNonLocalVariable(this->getNumNonLocalVariable());
    pressBiTerm->setNumConstitutiveExtraDofDiffusionVariable(this->getNumConstitutiveExtraDofDiffusionVariable());
    pressBiTerm->setConstitutiveExtraDofDiffusionEqRatio(this->getConstitutiveExtraDofDiffusionEqRatio());
    pressBiTerm->setConstitutiveExtraDofDiffusionUseEnergyConjugatedField(this->getConstitutiveExtraDofDiffusionUseEnergyConjugatedField());
    pressBiTerm->setConstitutiveExtraDofDiffusionAccountFieldSource(this->getConstitutiveExtraDofDiffusionAccountFieldSource());
    pressBiTerm->setConstitutiveExtraDofDiffusionAccountMecaSource(this->getConstitutiveExtraDofDiffusionAccountMecaSource());
    return pressBiTerm;
  }
};


class axisymmetricG3DLoadTerm : public g3DLinearTerm<double>
{
  protected:
    const simpleFunctionTime<double> *Load;
    int comp;
    
  public :
    axisymmetricG3DLoadTerm(FunctionSpace<double>& space1_, const simpleFunctionTime<double> *Load_, const int c) : g3DLinearTerm<double>(space1_),Load(Load_),comp(c){}
    virtual ~axisymmetricG3DLoadTerm() {}
    virtual void set(const fullVector<double> *datafield){}
    virtual const bool isData() const{return false;}
    virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullVector<double> > &mv) const
    {
      Msg::Error("Define me get by integration point axisymmetricG3DLoadTerm");
    }
    virtual void get(MElement *ele,int npts,IntPt *GP,fullVector<double> &m) const;
    virtual LinearTermBase<double>* clone () const
    {
      axisymmetricG3DLoadTerm* loadterm=new axisymmetricG3DLoadTerm(this->space1,this->Load, comp);
      loadterm->setDim(this->getDim());
      loadterm->setNonLocalEqRatio(this->getNonLocalEqRatio());
      loadterm->setNumNonLocalVariable(this->getNumNonLocalVariable());
      loadterm->setNumConstitutiveExtraDofDiffusionVariable(this->getNumConstitutiveExtraDofDiffusionVariable());
      loadterm->setConstitutiveExtraDofDiffusionEqRatio(this->getConstitutiveExtraDofDiffusionEqRatio());
      loadterm->setConstitutiveExtraDofDiffusionUseEnergyConjugatedField(this->getConstitutiveExtraDofDiffusionUseEnergyConjugatedField());
      loadterm->setConstitutiveExtraDofDiffusionAccountFieldSource(this->getConstitutiveExtraDofDiffusionAccountFieldSource());
      loadterm->setConstitutiveExtraDofDiffusionAccountMecaSource(this->getConstitutiveExtraDofDiffusionAccountMecaSource());
      return loadterm;
    }
};





class axisymmetricDG3DForceBulk : public g3DLinearTerm<double>
{
  protected:
    const dG3DMaterialLaw* _mlaw;
    const IPField *_ipf;
    const bool _fullDg;
    const bool _incrementNonlocalBased;
    
  public:
    axisymmetricDG3DForceBulk(FunctionSpace<double>& space1_,const materialLaw* mlaw,bool FullDG, const IPField *ip, const bool increBased): g3DLinearTerm<double>(space1_),
                                                                                 _fullDg(FullDG),
                                                                                 _ipf(ip),
                                                                                 _mlaw(static_cast<const dG3DMaterialLaw*>(mlaw)),
                                                                                 _incrementNonlocalBased(increBased){};
    virtual ~axisymmetricDG3DForceBulk(){}
    
    virtual void get(MElement *ele,int npts,IntPt *GP,fullVector<double> &m) const;
    virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullVector<double> > &mv) const
    {
      Msg::Error("Define me get by integration point axisymmetricDG3DForceBulk");
    }
    virtual const bool isData() const {return false;}
    virtual void set(const fullVector<double> *datafield){}
  
    
    virtual LinearTermBase<double>* clone () const
    {
      axisymmetricDG3DForceBulk *DG=new axisymmetricDG3DForceBulk(space1,_mlaw,_fullDg,_ipf,_incrementNonlocalBased);
      DG->setDim(this->getDim());
      DG->setNonLocalEqRatio(this->getNonLocalEqRatio());
      DG->setNumNonLocalVariable(this->getNumNonLocalVariable());
      DG->setNumConstitutiveExtraDofDiffusionVariable(this->getNumConstitutiveExtraDofDiffusionVariable());
      DG->setConstitutiveExtraDofDiffusionEqRatio(this->getConstitutiveExtraDofDiffusionEqRatio());
      DG->setConstitutiveExtraDofDiffusionUseEnergyConjugatedField(this->getConstitutiveExtraDofDiffusionUseEnergyConjugatedField());
      DG->setConstitutiveExtraDofDiffusionAccountFieldSource(this->getConstitutiveExtraDofDiffusionAccountFieldSource());
      DG->setConstitutiveExtraDofDiffusionAccountMecaSource(this->getConstitutiveExtraDofDiffusionAccountMecaSource());
      return DG;
    }
    
};  

class axisymmetricDG3DStiffnessBulk : public g3DBilinearTerm<double,double>
{
  protected:
    const dG3DMaterialLaw* _mlaw;
    const IPField *_ipf;
    const bool _fullDg;
    bool sym;
    mutable const dG3DIPVariableBase* vipv[256]; // max 256 Gauss' point ??

    
  public:
    axisymmetricDG3DStiffnessBulk(FunctionSpace<double>& space1_,FunctionSpace<double>& space2_, const materialLaw* mlaw,
                                  bool FullDG, const IPField *ip) :  g3DBilinearTerm<double,double>(space1_,space2_),
                                                            _fullDg(FullDG), _ipf(ip), _mlaw(static_cast< const dG3DMaterialLaw*>(mlaw))
    {
      sym=(&space1_==&space2_);
    }
    axisymmetricDG3DStiffnessBulk(FunctionSpace<double>& space1_, const materialLaw* mlaw,
                                  bool FullDG,const IPField *ip) : g3DBilinearTerm<double,double>(space1_,space1_),
                                                             _fullDg(FullDG), _ipf(ip),
                                                             _mlaw(static_cast<const dG3DMaterialLaw*>(mlaw))
    {
      sym=true;
    }
    virtual ~axisymmetricDG3DStiffnessBulk(){}     
    virtual const bool isData() const {return false;}
    virtual void set(const fullVector<double> *datafield){}                         
    virtual void get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const;
    virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const
    {
      Msg::Error("Define me get by gauss point axisymmetricDG3DStiffnessBulk");
    }
    virtual BilinearTermBase* clone () const
    {
      axisymmetricDG3DStiffnessBulk *DG= new axisymmetricDG3DStiffnessBulk(space1,_mlaw,_fullDg,_ipf);
      DG->setDim(this->getDim());
      DG->setNonLocalEqRatio(this->getNonLocalEqRatio());
      DG->setNumNonLocalVariable(this->getNumNonLocalVariable());
      DG->setNumConstitutiveExtraDofDiffusionVariable(this->getNumConstitutiveExtraDofDiffusionVariable());
      DG->setConstitutiveExtraDofDiffusionEqRatio(this->getConstitutiveExtraDofDiffusionEqRatio());
      DG->setConstitutiveExtraDofDiffusionUseEnergyConjugatedField(this->getConstitutiveExtraDofDiffusionUseEnergyConjugatedField());
      DG->setConstitutiveExtraDofDiffusionAccountFieldSource(this->getConstitutiveExtraDofDiffusionAccountFieldSource());
      DG->setConstitutiveExtraDofDiffusionAccountMecaSource(this->getConstitutiveExtraDofDiffusionAccountMecaSource());
      return DG;
    }
};

class axisymmetricMass3D : public mass3D{
  public:
    axisymmetricMass3D(FunctionSpace<double> &space1_,
                  materialLaw *mlaw, int nlVar): mass3D(space1_,mlaw,nlVar){}
    axisymmetricMass3D(FunctionSpace<double> &space1_, FunctionSpace<double> &space2_,
                   materialLaw *mlaw, int nlVar): mass3D(space1_,space2_,mlaw, nlVar){}
    axisymmetricMass3D(FunctionSpace<double> &space1_, FunctionSpace<double> &space2_,
                   double rho, double nlRho, int nlVar): mass3D(space1_,space2_,rho, nlRho, nlVar){};
    virtual void get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const;
    virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const
    {
      Msg::Error("Define me get by integration point axisymmetricMass3D");
    }
    virtual BilinearTermBase* clone () const
    {
      return new axisymmetricMass3D(space1,space2,_rho, _rhoNL, _numNonLocalVariable);
    }
};





class axisymmetricDG3DForceInter : public dG3DForceInter
{
 protected:
  // nothing new compared to classical dG3Dterm

public:
  axisymmetricDG3DForceInter(FunctionSpace<double>& space1_, FunctionSpace<double>* space2_,
                                const materialLaw *mlawMinus, const materialLaw *mlawPlus,interfaceQuadratureBase *iquad,
                                double beta1, bool fdg, 
                                const IPField *ip, const bool increBased) :
          dG3DForceInter(space1_, space2_, mlawMinus, mlawPlus, iquad, beta1, fdg, ip,increBased){};
  virtual ~axisymmetricDG3DForceInter(){};
  virtual void get(MElement *ele, int npts, IntPt *GP, fullVector<double> &v)const;
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullVector<double> > &mv) const
  {
    Msg::Error("Define me get by integration point axisymmetricDG3DForceInter");
  }
  virtual LinearTermBase<double>* clone () const
  {
    axisymmetricDG3DForceInter* DG = new axisymmetricDG3DForceInter(*_minusSpace,_plusSpace,_mlawMinus,_mlawPlus,_interQuad,_stabilityParameter,_fullDg,_ipf,_incrementNonlocalBased); 
    // we need a copy constructor or to pass all the parameters in the constructor
    DG->setDim(this->getDim());
    DG->setNonLocalEqRatio(this->getNonLocalEqRatio());
    DG->setNumNonLocalVariable(this->getNumNonLocalVariable());
    DG->setNonLocalContinuity(this->getNonLocalContinuity());
    DG->setNonLocalStabilityParameter(this->getNonLocalStabilityParameter());
    DG->setNumConstitutiveExtraDofDiffusionVariable(this->getNumConstitutiveExtraDofDiffusionVariable());
    DG->setConstitutiveExtraDofDiffusionEqRatio(this->getConstitutiveExtraDofDiffusionEqRatio());
    DG->setConstitutiveExtraDofDiffusionUseEnergyConjugatedField(this->getConstitutiveExtraDofDiffusionUseEnergyConjugatedField());
    DG->setConstitutiveExtraDofDiffusionAccountFieldSource(this->getConstitutiveExtraDofDiffusionAccountFieldSource());
    DG->setConstitutiveExtraDofDiffusionAccountMecaSource(this->getConstitutiveExtraDofDiffusionAccountMecaSource());
    DG->setConstitutiveExtraDofDiffusionStabilityParameter(this->getConstitutiveExtraDofDiffusionStabilityParameter());
    DG->setConstitutiveExtraDofDiffusionContinuity(this->getConstitutiveExtraDofDiffusionContinuity());
    return DG;
  }
};







class axisymmetricDG3DStiffnessInter : public dG3DStiffnessInter
{
protected:
 // nothing new compared to classical dG3Dterm

public:
  axisymmetricDG3DStiffnessInter(FunctionSpace<double>& space1_,FunctionSpace<double>& space2_,
                                    const materialLaw* mlawMinus, const materialLaw* mlawPlus,interfaceQuadratureBase *iquad,
                                    double beta1_, const IPField *ip, unknownField *uf,
                                    bool fulldg, const bool increBased, double eps=1.e-6) :
            dG3DStiffnessInter(space1_,space2_, mlawMinus, mlawPlus, iquad, beta1_, ip, uf, fulldg, increBased, eps)
{
  
};
  virtual ~axisymmetricDG3DStiffnessInter() {}
  virtual void get(MElement *ele,int npts,IntPt *GP, fullMatrix<double> &m)const;
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const
  {
    Msg::Error("Define me ?? get by integration point dG3DStiffnessInter");
  }
  virtual BilinearTermBase* clone () const
  {
    axisymmetricDG3DStiffnessInter* DG = new axisymmetricDG3DStiffnessInter(space1,space2,_mlawMinus,_mlawPlus,_interQuad,_stabilityParameter,
                                              _ipf,_ufield,_fullDg, _incrementNonlocalBased,_perturbation); // we need a copy constructor or to pass all the parameters in the constructor
    DG->setDim(this->getDim());
    DG->setNonLocalEqRatio(this->getNonLocalEqRatio());
    DG->setNumNonLocalVariable(this->getNumNonLocalVariable());
    DG->setNonLocalContinuity(this->getNonLocalContinuity());
    DG->setNonLocalStabilityParameter(this->getNonLocalStabilityParameter());
    DG->setNumConstitutiveExtraDofDiffusionVariable(this->getNumConstitutiveExtraDofDiffusionVariable());
    DG->setConstitutiveExtraDofDiffusionEqRatio(this->getConstitutiveExtraDofDiffusionEqRatio());
    DG->setConstitutiveExtraDofDiffusionUseEnergyConjugatedField(this->getConstitutiveExtraDofDiffusionUseEnergyConjugatedField());
    DG->setConstitutiveExtraDofDiffusionAccountFieldSource(this->getConstitutiveExtraDofDiffusionAccountFieldSource());
    DG->setConstitutiveExtraDofDiffusionAccountMecaSource(this->getConstitutiveExtraDofDiffusionAccountMecaSource());
    DG->setConstitutiveExtraDofDiffusionStabilityParameter(this->getConstitutiveExtraDofDiffusionStabilityParameter());
    DG->setConstitutiveExtraDofDiffusionContinuity(this->getConstitutiveExtraDofDiffusionContinuity());
    return DG;
  }
};
#endif //AXISYMMETRICDG3DTERMS_H

