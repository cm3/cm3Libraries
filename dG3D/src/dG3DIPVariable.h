//
// C++ Interface: ipvariable
//
// Description: Class with definition of ipvarible for dG3D
//
//
// Author:  <L. Noels>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef DG3DIPVARIABLE_H_
#define DG3DIPVARIABLE_H_
#include "NonLinearSolverConfig.h"
#include "ipvariable.h"
#include "ipVEVPMFH.h"
#include "ipField.h"
#include "STensor43.h"
#include "STensor33.h"
#include "ipJ2linear.h"
#include "ipVUMATinterface.h"
#include "ipTransverseIsotropic.h"
#include "ipTransverseIsoCurvature.h"
#include "ipTransverseIsoUserDirection.h"
#include "ipTransverseIsoYarnB.h"
#include "ipAnisotropic.h"
#include "ipAnisotropicStoch.h"
#include "ipTFA.h"
#include "ipFiniteStrain.h"
#include "ipJ2ThermoMechanics.h"
#include "ipLinearThermoMechanics.h"
#include "ipSMP.h"
#include "ipEOS.h"
#include "ipCrystalPlasticity.h"
#include "ipGursonUMAT.h"
#include "ipVEVPUMAT.h"
#include "ipIMDEACPUMAT.h"
#include "ipViscoelastic.h"
#include "ipAnIsotropicElecTherMech.h"
#include "ipLinearElecTherMech.h"
#include "ipLinearElecMagTherMech.h"
#include "ipLinearElecMagInductor.h"
#include "mlawJ2linear.h"
#include "mlawAnisotropicPlasticity.h"
#include "mlawJ2VMSmallStrain.h"
#include "mlawJ2ThermoMechanics.h"
#include "mlawLocalDamageJ2Hyper.h"
#include "mlawLinearThermoMechanics.h"
#include "mlawSMP.h"
#include "mlawPhenomenologicalSMP.h"
#include "mlawViscoelastic.h"
#include "mlawLinearElecTherMech.h"
#include "mlawLinearElecMagTherMech.h"
#include "mlawAnIsotropicElecTherMech.h"
#include "mlawElecSMP.h"
#include "ipElecSMP.h"
#include "ipCohesive.h"
#include "ipAnIsotropicTherMech.h"
#include "extraDofIPVariable.h"
#include "mlawHyperelasticDamage.h"
#include "ipHyperelastic.h"
#include "mlawHyperelastic.h"
#include "mlawLinearElecMagInductor.h"
#include "mlawLocalDamageJ2SmallStrain.h"
#include "mlawLocalDamageIsotropicElasticity.h"
#include "ipGenericTM.h"
#include "ipGenericRM.h"
#include "mlawGenericTM.h"
#include "mlawGenericRM.h"
#include "ipElecMagGenericThermoMech.h"
#include "mlawElecMagGenericThermoMech.h"
#include "ipElecMagInductor.h"
#include "mlawElecMagInductor.h"
#include "ipNonLinearTVM.h"
#include "mlawNonLinearTVM.h"
#include "ipNonLinearTVE.h"
#include "ipNonLinearTVP.h"
#include "mlawNonLinearTVP.h"
#include "mlawNonLinearTVENonLinearTVP.h"
#include "ipHyperelasticPotential.h"

#if defined(HAVE_TORCH)
#include <torch/torch.h>
#include <torch/script.h>
#endif

class dG3DIPVariableBase : public ipFiniteStrain
{
 public:
  dG3DIPVariableBase() : ipFiniteStrain(){}
  dG3DIPVariableBase(const dG3DIPVariableBase &source);
  virtual dG3DIPVariableBase& operator=(const IPVariable &source);
  virtual ~dG3DIPVariableBase(){}

  virtual const bool isInterface() const=0;

  virtual IPVariable* getInternalData() =0;
  virtual const IPVariable* getInternalData()  const =0;

  // Archiving data
  virtual double get(const int i) const=0;
  virtual double defoEnergy() const=0;
  virtual double plasticEnergy() const=0;
  virtual double damageEnergy() const=0;
  virtual double vonMises() const=0;
  virtual void getCauchyStress(STensor3 &cauchy) const=0;
  virtual double getJ(int dim=3) const=0;
  virtual double getBarJ() const=0;
  virtual double &getRefToBarJ()=0;
  virtual double getLocalJ() const=0;
  virtual double &getRefToLocalJ()=0;
  virtual void getBackLocalDeformationGradient(STensor3& localF) const=0;

  virtual const STensor3 &getConstRefToDeformationGradient() const=0;
  virtual STensor3 &getRefToDeformationGradient()=0;

  virtual const STensor3 &getConstRefToFirstPiolaKirchhoffStress() const=0;
  virtual STensor3 &getRefToFirstPiolaKirchhoffStress()=0;

  virtual const STensor43 &getConstRefToTangentModuli() const=0;
  virtual STensor43 &getRefToTangentModuli()=0;

  virtual const STensor43 &getConstRefToElasticTangentModuli() const = 0;
  virtual STensor43 &getRefToElasticTangentModuli() = 0;


  virtual const STensor43 &getConstRefToDGElasticTangentModuli() const=0;
  virtual void setRefToDGElasticTangentModuli(const STensor43 &eT)=0;

  virtual const SVector3 &getConstRefToReferenceOutwardNormal() const=0;
  virtual SVector3 &getRefToReferenceOutwardNormal()=0;

  virtual const SVector3 &getConstRefToCurrentOutwardNormal() const=0;
  virtual SVector3 &getRefToCurrentOutwardNormal()=0;

  virtual const SVector3 &getConstRefToJump() const=0;
  virtual SVector3 &getRefToJump()=0;

  // non local
  virtual int getNumberNonLocalVariable() const =0;
  //
  virtual double getConstRefToLocalVariable(const int idex) const = 0;
  virtual double& getRefToLocalVariable(const int idex) = 0;
  //
  virtual double getConstRefToNonLocalVariable(const int idex) const = 0;
  virtual double& getRefToNonLocalVariable(const int idex) = 0;
  //
  virtual double getConstRefToNonLocalVariableInMaterialLaw(const int idex) const = 0;
  virtual double& getRefToNonLocalVariableInMaterialLaw(const int idex) = 0;
  //
  virtual const std::vector<STensor3> &getConstRefToDLocalVariableDStrain() const = 0;
  virtual std::vector<STensor3> &getRefToDLocalVariableDStrain() = 0;

  virtual const std::vector<STensor3> &getConstRefToDStressDNonLocalVariable() const = 0;
  virtual std::vector<STensor3> &getRefToDStressDNonLocalVariable() = 0;

  virtual const fullMatrix<double> &getConstRefToDLocalVariableDNonLocalVariable() const = 0;
  virtual fullMatrix<double> &getRefToDLocalVariableDNonLocalVariable() = 0;

  virtual const fullVector<double> &getConstRefToNonLocalJump() const = 0;
  virtual fullVector<double> &getRefToNonLocalJump() = 0;

  virtual const std::vector<SVector3> &getConstRefToGradNonLocalVariable() const = 0;
  virtual std::vector<SVector3> &getRefToGradNonLocalVariable() = 0;
  virtual const fullMatrix<double> & getConstRefTodLocalVariableDExtraDofDiffusionField() const=0;
  virtual fullMatrix<double> & getRefTodLocalVariableDExtraDofDiffusionField()=0;
  virtual const std::vector< std::vector< SVector3 > > & getConstRefTodLocalVariabledVectorPotential() const = 0;
  virtual std::vector< std::vector< SVector3 > > & getRefTodLocalVariabledVectorPotential() = 0;
  virtual const std::vector< std::vector< SVector3 > > & getConstRefTodLocalVariabledVectorCurl() const = 0;
  virtual std::vector< std::vector< SVector3 > > & getRefTodLocalVariabledVectorCurl() = 0;

  virtual const STensor3 &getConstRefToCharacteristicLengthMatrix(const int idx) const = 0;
  // higher order body force
  virtual bool hasBodyForceForHO() const =0;
  virtual const STensor33 &getConstRefToGM()const=0;
  virtual STensor33 &getRefToGM()=0;
  virtual const SVector3 &getConstRefToBm() const=0;
  virtual SVector3 &getRefToBm()=0;
  virtual const STensor33 &getConstRefTodBmdFm() const=0;
  virtual STensor33 &getRefTodBmdFm()=0;
  virtual const STensor43 &getConstRefTodBmdGM() const=0;
  virtual STensor43 &getRefTodBmdGM()=0;
  virtual const STensor43 &getConstRefToDFmdFM() const=0;
  virtual STensor43 &getRefToDFmdFM()=0;
  virtual const STensor53 &getConstRefToDFmdGM() const=0;
  virtual STensor53 &getRefToDFmdGM()=0;
  virtual const STensor63 &getConstRefTodCmdFm() const=0;
  virtual STensor63 &getRefTodCmdFm()=0;
  virtual STensor63 *getPtrTodCmdFm()=0;
  virtual const STensor43 &getConstRefTodPmdFM() const=0;
  virtual STensor43 &getRefTodPmdFM()=0;
  //-----------------get 1st piola before AV ------------//

  virtual const STensor3 &getConstRefToFirstPiolaKirchhoffStressb4AV() const=0;
  virtual STensor3 &getRefToFirstPiolaKirchhoffStressb4AV()=0;

  virtual void setNonLocalToLocal(const bool fl){};
  virtual bool getNonLocalToLocal() const {return false;};

  // for multiple field formulation
  // incompatible jump
  virtual const SVector3& getConstRefToIncompatibleJump() const = 0;
  virtual SVector3& getRefToIncompatibleJump() = 0;

  // for path following based on  irreversibe energt
  virtual double irreversibleEnergy() const = 0;
  virtual double& getRefToIrreversibleEnergy() = 0;

  virtual const STensor3& getConstRefToDIrreversibleEnergyDDeformationGradient() const = 0;
  virtual STensor3& getRefToDIrreversibleEnergyDDeformationGradient() = 0;

  // irreversible ennergy depend on nonlocal variale
  virtual const double& getConstRefToDIrreversibleEnergyDNonLocalVariable(const int index) const =0;
  virtual double& getRefToDIrreversibleEnergyDNonLocalVariable(const int index) =0;



  //extraDof
  virtual int getNumConstitutiveExtraDofDiffusionVariable() const=0;
  virtual double getConstRefToField(const int idex) const=0;
  virtual double  &getRefToField(const int idex)=0;
  virtual const std::vector<SVector3>  &getConstRefToGradField() const=0;
  virtual std::vector<SVector3>  &getRefToGradField()=0;
  virtual const std::vector<SVector3>  &getConstRefToFlux() const=0;
  virtual std::vector<SVector3>  &getRefToFlux()=0;
  virtual const std::vector<SVector3> &getConstRefToElectricDisplacement() const=0;
  virtual std::vector<SVector3> &getRefToElectricDisplacement()=0;
  virtual const std::vector<STensor3>  &getConstRefTodPdField() const=0;
  virtual std::vector<STensor3>  &getRefTodPdField()=0;
  virtual const std::vector<STensor33>  &getConstRefTodPdGradField() const=0;
  virtual std::vector<STensor33>  &getRefTodPdGradField()=0;
  virtual const std::vector<std::vector<STensor3> >  &getConstRefTodFluxdGradField() const=0;
  virtual std::vector<std::vector<STensor3> >  &getRefTodFluxdGradField()=0;
  virtual const std::vector<std::vector<SVector3> >  &getConstRefTodFluxdField() const=0;
  virtual std::vector<std::vector<SVector3> >  &getRefTodFluxdField()=0;
  virtual const std::vector<STensor33>  &getConstRefTodFluxdF() const=0;
  virtual std::vector<STensor33>   &getRefTodFluxdF()=0;
  virtual const std::vector< std::vector<SVector3> > &getConstRefTodElecDisplacementdField() const=0;
  virtual std::vector< std::vector<SVector3> > &getRefTodElecDisplacementdField()=0;
  virtual const std::vector< std::vector<STensor3> > &getConstRefTodElecDisplacementdGradField() const=0;
  virtual std::vector< std::vector<STensor3> > &getRefTodElecDisplacementdGradField()=0;
  virtual const std::vector<STensor33> &getConstRefTodElecDisplacementdF() const=0;
  virtual std::vector<STensor33> &getRefTodElecDisplacementdF()=0;
  virtual const std::vector<std::vector<const STensor3*> > &getConstRefToLinearK() const =0;
  virtual void setConstRefToLinearK(const STensor3 &eT, int i, int j) =0;
  virtual const std::vector<std::vector<std::vector<STensor3> > >  &getConstRefTodLinearKdField() const=0;
  virtual std::vector<std::vector<std::vector<STensor3> > >  &getRefTodLinearKdField()=0;
  virtual const std::vector< std::vector<STensor43> >  &getConstRefTodLinearKdF() const=0;
  virtual std::vector< std::vector<STensor43> >  &getRefTodLinearKdF()=0;
/*  virtual const std::vector<std::vector<const STensor3*> > &getConstRefToEnergyK() const =0;
  virtual void setConstRefToEnergyK(const STensor3 &eT, int i, int j) =0;
  virtual const std::vector<std::vector<std::vector<STensor3> > >  &getConstRefTodEnergyKdField() const=0;
  virtual std::vector<std::vector<std::vector<STensor3> > >  &getRefTodEnergyKdField()=0;
  virtual const std::vector< std::vector<STensor43> >  &getConstRefTodEnergyKdF() const=0;
  virtual std::vector<std::vector<STensor43>>  &getRefTodEnergyKdF()=0; */
  virtual const std::vector< std::vector<STensor43> >  &getConstRefTodFluxdGradFielddF() const=0;
  virtual std::vector<std::vector<STensor43> >  &getRefTodFluxdGradFielddF()=0;
  virtual const std::vector<std::vector<std::vector<STensor3> > >  &getConstRefTodFluxdGradFielddField() const=0;
  virtual std::vector<std::vector<std::vector<STensor3> > >  &getRefTodFluxdGradFielddField()=0;
  virtual const fullVector<double>  &getConstRefToFieldSource() const=0;
  virtual fullVector<double>  &getRefToFieldSource()=0;
  virtual const fullMatrix<double>  &getConstRefTodFieldSourcedField() const=0;
  virtual fullMatrix<double>  &getRefTodFieldSourcedField()=0;
  virtual const std::vector<std::vector<SVector3> >  &getConstRefTodFieldSourcedGradField() const=0;
  virtual std::vector<std::vector<SVector3> >  &getRefTodFieldSourcedGradField()=0;
  virtual const std::vector<STensor3>  &getConstRefTodFieldSourcedF() const=0;
  virtual std::vector<STensor3>  &getRefTodFieldSourcedF()=0;
  virtual const fullVector<double>  &getConstRefToMechanicalSource() const=0;
  virtual fullVector<double>  &getRefToMechanicalSource()=0;
  virtual const fullMatrix<double>  &getConstRefTodMechanicalSourcedField() const=0;
  virtual fullMatrix<double>  &getRefTodMechanicalSourcedField()=0;
  virtual const std::vector<std::vector<SVector3> >  &getConstRefTodMechanicalSourcedGradField() const=0;
  virtual std::vector<std::vector<SVector3> >  &getRefTodMechanicalSourcedGradField()=0;
  virtual const std::vector<STensor3>  &getConstRefTodMechanicalSourcedF() const=0;
  virtual std::vector<STensor3>  &getRefTodMechanicalSourcedF()=0;
  virtual const fullVector<double>  &getConstRefToExtraDofFieldCapacityPerUnitField() const=0;
  virtual fullVector<double>  &getRefToExtraDofFieldCapacityPerUnitField()=0;
  virtual const fullVector<double> & getConstRefToEMFieldSource() const=0;
  virtual fullVector<double> & getRefToEMFieldSource()=0;
  virtual const fullMatrix<double> & getConstRefTodEMFieldSourcedField() const=0;
  virtual fullMatrix<double> & getRefTodEMFieldSourcedField()=0;
  virtual const std::vector< std::vector< SVector3 > > & getConstRefTodEMFieldSourcedGradField() const=0;
  virtual std::vector< std::vector< SVector3 > > & getRefTodEMFieldSourcedGradField()=0;

  virtual const fullVector<double>  &getConstRefToFieldJump() const=0;
  virtual fullVector<double>  &getRefToFieldJump()=0;
  virtual const fullVector<double>  &getConstRefToOneOverFieldJump() const=0;
  virtual fullVector<double>  &getRefToOneOverFieldJump()=0;
  virtual const fullMatrix<double>  &getConstRefTodOneOverFieldJumpdFieldm() const=0;
  virtual fullMatrix<double>  &getRefTodOneOverFieldJumpdFieldm()=0;
  virtual const fullMatrix<double>  &getConstRefTodOneOverFieldJumpdFieldp() const=0;
  virtual fullMatrix<double>  &getRefTodOneOverFieldJumpdFieldp()=0;
  virtual const std::vector<SVector3>  &getConstRefToInterfaceFlux() const=0;
  virtual std::vector<SVector3>  &getRefToInterfaceFlux()=0;
  virtual const std::vector<const STensor3*> &getConstRefToLinearSymmetrizationCoupling() const =0;
  virtual void setConstRefToLinearSymmetrizationCoupling(const STensor3 &eT, int i) =0;

  virtual const std::vector< std::vector< STensor3 > > & getConstRefTodFluxdVectorPotential() const=0;
  virtual const std::vector< std::vector< STensor3 > > & getConstRefTodFluxdVectorCurl() const=0;
  virtual const std::vector< std::vector< SVector3 > > & getConstRefTodFieldSourcedVectorPotential() const=0;
  virtual const std::vector< std::vector< SVector3 > > & getConstRefTodMechanicalSourcedVectorPotential() const=0;
  virtual const std::vector< std::vector< SVector3 > > & getConstRefTodFieldSourcedVectorCurl() const=0;
  virtual const std::vector< std::vector< SVector3 > > & getConstRefTodMechanicalSourcedVectorCurl() const=0;

  virtual std::vector< std::vector< STensor3 > > & getRefTodFluxdVectorPotential()=0;
  virtual std::vector< std::vector< STensor3 > > & getRefTodFluxdVectorCurl()=0;
  virtual std::vector< std::vector< SVector3 > > & getRefTodFieldSourcedVectorPotential()=0;
  virtual std::vector< std::vector< SVector3 > > & getRefTodMechanicalSourcedVectorPotential()=0;
  virtual std::vector< std::vector< SVector3 > > & getRefTodFieldSourcedVectorCurl()=0;
  virtual std::vector< std::vector< SVector3 > > & getRefTodMechanicalSourcedVectorCurl()=0;
  virtual const std::vector< std::vector< SVector3 > > & getConstRefTodEMFieldSourcedVectorPotential() const=0;
  virtual std::vector< std::vector< SVector3 > > & getRefTodEMFieldSourcedVectorPotential()=0;
  virtual const std::vector< std::vector< SVector3 > > & getConstRefTodEMFieldSourcedVectorCurl() const=0;
  virtual std::vector< std::vector< SVector3 > > & getRefTodEMFieldSourcedVectorCurl()=0;
  virtual const std::vector<std::vector< STensor3 > > & getConstRefTodElecDisplacementdVectorPotential() const=0;
  virtual const std::vector<std::vector< STensor3 > > & getConstRefTodElecDisplacementdVectorCurl() const=0;
  virtual std::vector<std::vector< STensor3 > > & getRefTodElecDisplacementdVectorPotential()=0;
  virtual std::vector<std::vector< STensor3 > > & getRefTodElecDisplacementdVectorCurl()=0;

  virtual const std::vector< STensor3 > & getConstRefTodEMFieldSourcedF() const=0;
  virtual std::vector< STensor3 > & getRefTodEMFieldSourcedF()=0;

  virtual const std::vector<std::vector<SVector3> > & getConstRefTodFluxdNonLocalVariable() const=0;
  virtual std::vector<std::vector<SVector3> > & getRefTodFluxdNonLocalVariable()=0;
  virtual const fullMatrix<double> &getConstRefTodFieldSourcedNonLocalVariable() const=0;
  virtual fullMatrix<double> &getRefTodFieldSourcedNonLocalVariable()=0;
  virtual const fullMatrix<double> &getConstRefTodMechanicalSourcedNonLocalVariable() const=0;
  virtual fullMatrix<double> &getRefTodMechanicalSourcedNonLocalVariable()=0;
  virtual const fullMatrix<double> & getConstRefTodEMFieldSourcedNonLocalVariable() const=0;
  virtual fullMatrix<double> & getRefTodEMFieldSourcedNonLocalVariable()=0;
  virtual const std::vector< std::vector<SVector3> > &getConstRefTodElecDisplacementdNonLocalVariable() const=0;
  virtual std::vector< std::vector<SVector3> > &getRefTodElecDisplacementdNonLocalVariable()=0;
  // curl data
  virtual int getNumConstitutiveCurlVariable() const=0;
  virtual const SVector3 & getConstRefToVectorPotential(const int idex) const=0;
  virtual SVector3 & getRefToVectorPotential(const int idex)=0;
  virtual const SVector3 & getConstRefToVectorCurl(const int idex) const=0;
  virtual SVector3 & getRefToVectorCurl(const int idex)=0;
  virtual const SVector3 & getConstRefToVectorField(const int idex) const=0;
  virtual SVector3 & getRefToVectorField(const int idex)=0;
  virtual const SVector3 & getConstRefToSourceVectorField(const int idex) const=0;
  virtual SVector3 & getRefToSourceVectorField(const int idex)=0;
  virtual const SVector3 & getConstRefToMechanicalFieldSource(const int idex) const=0;
  virtual SVector3 & getRefToMechanicalFieldSource(const int idex)=0;

  virtual const std::vector<std::vector< STensor3 > > & getConstRefTodVectorFielddVectorPotential() const=0;
  virtual const std::vector<std::vector< STensor3 > > & getConstRefTodVectorFielddVectorCurl() const=0;
  virtual const std::vector< STensor33 > & getConstRefTodVectorFielddF() const=0;
  virtual const std::vector<std::vector< SVector3 > > & getConstRefTodVectorFielddExtraDofField() const=0;
  virtual const std::vector<std::vector< STensor3 > > & getConstRefTodVectorFielddGradExtraDofField() const=0;
  virtual const std::vector< STensor33 > & getConstRefTodPdVectorCurl() const=0;
  virtual const std::vector< STensor33 > & getConstRefTodPdVectorPotential() const=0;

  virtual std::vector<std::vector< STensor3 > > & getRefTodVectorFielddVectorPotential()=0;
  virtual std::vector<std::vector< STensor3 > > & getRefTodVectorFielddVectorCurl()=0;
  virtual std::vector< STensor33 > & getRefTodVectorFielddF()=0;
  virtual std::vector<std::vector< SVector3 > > & getRefTodVectorFielddExtraDofField()=0;
  virtual std::vector<std::vector< STensor3 > > & getRefTodVectorFielddGradExtraDofField()=0;
  virtual std::vector< STensor33 > & getRefTodPdVectorCurl()=0;
  virtual std::vector< STensor33 > & getRefTodPdVectorPotential()=0;

  virtual const std::vector<std::vector< SVector3 > > & getConstRefTodVectorFielddNonLocalVariable() const=0;
  virtual std::vector<std::vector< SVector3 > > & getRefTodVectorFielddNonLocalVariable()=0;
  virtual const std::vector< std::vector< SVector3 > > & getConstRefTodSourceVectorFielddNonLocalVariable() const=0;
  virtual std::vector< std::vector< SVector3 > > & getRefTodSourceVectorFielddNonLocalVariable()=0;

  virtual const std::vector< std::vector< SVector3 > > & getConstRefTodSourceVectorFielddExtraDofField() const=0;
  virtual std::vector< std::vector< SVector3 > > & getRefTodSourceVectorFielddExtraDofField()=0;
  virtual const std::vector< std::vector< STensor3 > > & getConstRefTodSourceVectorFielddGradExtraDofField() const=0;
  virtual std::vector< std::vector< STensor3 > > & getRefTodSourceVectorFielddGradExtraDofField()=0;
  virtual const std::vector< std::vector< STensor3 > > & getConstRefTodMechanicalFieldSourcedVectorPotential() const=0;
  virtual std::vector< std::vector< STensor3 > > & getRefTodMechanicalFieldSourcedVectorPotential()=0;
  virtual const std::vector< std::vector< STensor3 > > & getConstRefTodMechanicalFieldSourcedVectorCurl() const=0;
  virtual std::vector< std::vector< STensor3 > > & getRefTodMechanicalFieldSourcedVectorCurl()=0;
  virtual const std::vector< STensor33 > & getConstRefTodSourceVectorFielddF() const=0;
  virtual std::vector< STensor33 > & getRefTodSourceVectorFielddF()=0;
  virtual const std::vector< STensor33 > & getConstRefTodMechanicalFieldSourcedF() const=0;
  virtual std::vector< STensor33 > & getRefTodMechanicalFieldSourcedF()=0;
  virtual const std::vector< std::vector< SVector3 > > & getConstRefTodMechanicalFieldSourcedExtraDofField() const=0;
  virtual std::vector< std::vector< SVector3 > > & getRefTodMechanicalFieldSourcedExtraDofField()=0;
  virtual const std::vector< std::vector< STensor3 > > & getConstRefTodMechanicalFieldSourcedGradExtraDofField() const=0;
  virtual std::vector< std::vector< STensor3 > > & getRefTodMechanicalFieldSourcedGradExtraDofField()=0;
  virtual const std::vector< std::vector< SVector3 > > & getConstRefTodMechanicalFieldSourcedNonLocalVariable() const=0;
  virtual std::vector< std::vector< SVector3 > > & getRefTodMechanicalFieldSourcedNonLocalVariable()=0;
  virtual const SVector3 & getConstRefTodSourceVectorFielddt(const int idex) const=0;
  virtual SVector3 & getRefTodSourceVectorFielddt(const int idex)=0;
  virtual const std::vector< std::vector< STensor3 > > & getConstRefTodSourceVectorFielddVectorCurl() const=0;
  virtual std::vector< std::vector< STensor3 > > & getRefTodSourceVectorFielddVectorCurl()=0;
  virtual const std::vector< std::vector< STensor3 > > & getConstRefTodSourceVectorFielddVectorPotential() const=0;
  virtual std::vector< std::vector< STensor3 > > & getRefTodSourceVectorFielddVectorPotential()=0;
  //Energy Conjugated Field (fT & fv)

  virtual double getConstRefToEnergyConjugatedField(const int idex) const=0;
  virtual double  &getRefToEnergyConjugatedField(const int idex)=0;
  virtual const std::vector<SVector3>  &getConstRefToGradEnergyConjugatedField() const=0;
  virtual std::vector<SVector3>  &getRefToGradEnergyConjugatedField()=0;
  virtual const  std::vector<std::vector<double> >  &getConstRefTodFielddEnergyConjugatedField() const=0;
  virtual std::vector<std::vector<double> >  &getRefTodFielddEnergyConjugatedField()=0;
  virtual const std::vector<std::vector<SVector3> >  &getConstRefTodGradFielddEnergyConjugatedField() const=0;
  virtual std::vector<std::vector<SVector3> >  &getRefTodGradFielddEnergyConjugatedField()=0;
  virtual const std::vector<std::vector<STensor3> >  &getConstRefTodGradFielddGradEnergyConjugatedField() const=0;
  virtual std::vector<std::vector<STensor3> >  &getRefTodGradFielddGradEnergyConjugatedField()=0;
  virtual const fullVector<double>  &getConstRefToEnergyConjugatedFieldJump() const=0;
  virtual fullVector<double>  &getRefToEnergyConjugatedFieldJump()=0;
  virtual const  std::vector<std::vector<double> >  &getConstRefTodEnergyConjugatedFieldJumpdFieldm() const=0;
  virtual std::vector<std::vector<double> >  &getRefTodEnergyConjugatedFieldJumpdFieldm()=0;
  virtual const  std::vector<std::vector<double> >  &getConstRefTodEnergyConjugatedFieldJumpdFieldp() const=0;
  virtual std::vector<std::vector<double> >  &getRefTodEnergyConjugatedFieldJumpdFieldp()=0;

  virtual void restart(){}
};

class bodyForceForHO{
  public:

    STensor33 GM;
    STensor43 dFmdFM;
    STensor53 dFmdGM;
    SVector3 Bm;
    STensor33 dBmdFm;
    STensor43 dBmdGM;
    STensor63 dCmdFm;
    STensor43 dPmdFM;

  public:
    bodyForceForHO();
    bodyForceForHO(const bodyForceForHO& src);
    bodyForceForHO& operator = (const bodyForceForHO& src);
    virtual ~bodyForceForHO(){};
    void restart();

};


class nonlocalData{
  public:
    int numNonLocalVariable; // number of nonlocal variables
    int numConstitutiveExtraDofDiffusionVariable; //number of extra dof variables
    int numConstitutiveCurlVariable; //number of extra curl variables
    // for coupling
    std::vector<STensor3> dLocalVariableDStrain;
    std::vector<STensor3> dStressDNonLocalVariable;
    fullMatrix<double> dLocalVariableDNonLocalVariable;

    // for dg: jump in non local epl
    fullVector<double> nonLocalJump;

    //
    fullVector<double> nonLocalVariable;
    std::vector<SVector3> gradNonLocalVariable;

    //coupling with constitutiveExtraDof
    fullMatrix<double> dLocalVariableDExtraDofDiffusionField;

    // coupling with consititutiveCurlData
    std::vector< std::vector<SVector3> > dLocalVariabledVectorPotential;
    std::vector< std::vector<SVector3> > dLocalVariabledVectorCurl;


  public:
    nonlocalData(const int numVer, const int numExtraDof=0, const int numCurlVar=0);
    nonlocalData(const nonlocalData& src);
    nonlocalData& operator = (const nonlocalData& src);
    virtual ~nonlocalData(){};
    void restart();
    virtual int getNumNonLocalVariable() const {return numNonLocalVariable;}
    virtual int getConstitutiveExtraDofDiffusionVariable() const {return numConstitutiveExtraDofDiffusionVariable;}
    virtual int getConstitutiveCurlVariable() const {return numConstitutiveCurlVariable;}

};


class constitutiveExtraDofDiffusionData{
 public:
  int numConstitutiveExtraDofDiffusionVariable; //number of extra dof variables
  int numNonLocalVariable; //number of extra dof variables
  int numConstitutiveCurlVariable; //number of extra curl variables
  bool useOfEnergy;
  bool useOfEnergyConjugatedField;

  fullVector<double>           field;
  std::vector<SVector3>        gradField;
  std::vector<SVector3>        flux;
  std::vector< SVector3 >      electricDisplacement; // Vector field D
  std::vector<STensor3>        dPdField;
  std::vector<STensor33>       dPdGradField;
  std::vector<std::vector<STensor3> >        dFluxdGradField;
  std::vector<std::vector<SVector3> >        dFluxdField;
  std::vector<STensor33>       dFluxdF;
  std::vector<std::vector< SVector3 > > dElecDisplacementdField;
  std::vector<std::vector< STensor3 > > dElecDisplacementdGradField;
  std::vector<STensor33> dElecDisplacementdF;
  std::vector<std::vector<const STensor3*> > linearK;
  fullVector<double>           fieldSource;  //related to cp*dT/dt
  fullMatrix<double>           dFieldSourcedField;
  std::vector<std::vector<SVector3> >        dFieldSourcedGradField;
  std::vector<STensor3>        dFieldSourcedF;

  fullVector<double>           mechanicalSource; // related to convert mechanical energy to heat
  fullMatrix<double>           dMechanicalSourcedField;
  std::vector<std::vector<SVector3> >        dMechanicalSourcedGradField;
  std::vector<STensor3>        dMechanicalSourcedF;

  fullVector<double>           fieldCapacityPerUnitField; // field capacity

  fullVector<double>           emFieldSource; // w_AV Eddy current + hysteresis loss
  fullMatrix<double>           demFieldSourcedField;
  std::vector< std::vector< SVector3 > > demFieldSourcedGradField;

  // for dg: jump in non local epl
  fullVector<double>           fieldJump;
  fullVector<double>           oneOverFieldJump;
  fullMatrix<double>           dOneOverFieldJumpdFieldm;
  fullMatrix<double>           dOneOverFieldJumpdFieldp;
  std::vector<SVector3>        interfaceFlux;
  std::vector< const STensor3* > linearSymmetrizationCoupling;

  //coupling with nonLocal
  std::vector<std::vector<SVector3> >       dFluxdNonLocalVariable;
  fullMatrix<double>          dFieldSourcedNonLocalVariable;
  fullMatrix<double>          dMechanicalSourcedNonLocalVariable;
  fullMatrix< double >        dEMFieldSourcedNonLocalVariable;
  std::vector<std::vector< SVector3 > > dElecDisplacementdNonLocalVariable;

  //coupling with curl
  std::vector< std::vector< STensor3 > > dFluxdVectorPotential;
  std::vector< std::vector< STensor3 > > dFluxdVectorCurl;
  std::vector< std::vector< SVector3 > > dFieldSourcedVectorPotential;
  std::vector< std::vector< SVector3 > > dMechanicalSourcedVectorPotential;
  std::vector< std::vector< SVector3 > > dFieldSourcedVectorCurl;
  std::vector< std::vector< SVector3 > > dMechanicalSourcedVectorCurl;
  std::vector< std::vector< SVector3 > > demFieldSourcedVectorPotential;
  std::vector< std::vector< SVector3 > > demFieldSourcedVectorCurl;
  std::vector< std::vector< STensor3 > > dElecDisplacementdVectorCurl;
  std::vector< std::vector< STensor3 > > dElecDisplacementdVectorPotential;

  //coupling with mech
  std::vector< STensor3 > demFieldSourcedF;

  // Use of energy

 /* std::vector<std::vector<const STensor3*> > EnergyK;
  std::vector<std::vector<std::vector<STensor3>>> dEnergyKdField;
  std::vector<std::vector<STensor43>> dEnergyKdF;*/
  std::vector<std::vector<STensor43> > dlinearKdF;
  std::vector<std::vector<std::vector<STensor3> > > dlinearKdField;
  std::vector<std::vector<std::vector<STensor3> > > dFluxdGradFielddField;
  std::vector<std::vector<STensor43> > dFluxdGradFielddF;


  //use of conjugated fields (fT & fv)

  fullVector<double>           energyConjugatedField;
  std::vector<SVector3>        gradEnergyConjugatedField;
  std::vector<std::vector<double> >          dFielddEnergyConjugatedField;
  std::vector<std::vector<SVector3> >        dGradFielddEnergyConjugatedField;
  std::vector<std::vector<STensor3> >        dGradFielddGradEnergyConjugatedField;
  fullVector<double>           energyConjugatedFieldJump;
  std::vector<std::vector<double> >          dEnergyConjugatedFieldJumpdFieldm;
  std::vector<std::vector<double> >          dEnergyConjugatedFieldJumpdFieldp;


 public:
  constitutiveExtraDofDiffusionData(const int numVer, const int numNonLocal=0, const int numCurlVar=0, bool useOfEnergy = false, bool useOfEnergyConjugatedField=false);
  constitutiveExtraDofDiffusionData(const constitutiveExtraDofDiffusionData& src);
  constitutiveExtraDofDiffusionData& operator = (const constitutiveExtraDofDiffusionData& src);
  virtual ~constitutiveExtraDofDiffusionData(){};
  void restart();
  virtual int getConstitutiveExtraDofDiffusionVariable() const {return numConstitutiveExtraDofDiffusionVariable;}
  virtual int getNumNonLocalVariable() const {return numNonLocalVariable;}
  virtual int getConstitutiveCurlVariable() const {return numConstitutiveCurlVariable;}
  virtual bool useConjugatedField() const {return useOfEnergyConjugatedField;}

};

class constitutiveCurlData{
 public:
  int numConstitutiveCurlVariable; //number of extra curl variables
  int numConstitutiveExtraDofDiffusionVariable; //number of extra dof variables
  int numNonLocalVariable; //number of extra dof variables

  std::vector< SVector3 >           vectorPotential; // Vector magnetic potential A
  std::vector< SVector3 >           vectorCurl;  // Curl A := B, magnetic induction
  std::vector< SVector3 >           vectorField; // Vector magnetic field H
  std::vector< SVector3 >           inductorSourceVectorField; // current density applied in inductor js0
  std::vector< SVector3 >           sourceVectorField; // Vector magnetic field source (T, grad V, F)
  std::vector< SVector3 >           dSourceVectorFielddt; // time derivative
  std::vector< std::vector< STensor3 > > dSourceVectorFielddVectorCurl;
  std::vector< std::vector< STensor3 > > dSourceVectorFielddVectorPotential;

  std::vector<std::vector< STensor3 > > dVectorFielddVectorPotential;
  std::vector<std::vector< STensor3 > > dVectorFielddVectorCurl;

  //coupling with mech
  std::vector< STensor33 > dPdVectorPotential;
  std::vector< STensor33 > dPdVectorCurl;
  std::vector< STensor33 > dVectorFielddF;
  std::vector< STensor33 >  dSourceVectorFielddF;
  std::vector< STensor33 >  dMechanicalFieldSourcedF;

  std::vector< SVector3 >  mechanicalFieldSource;   // EM force proportional to freq (V, B, F)
  std::vector< std::vector< STensor3 > > dMechanicalFieldSourcedVectorPotential;
  std::vector< std::vector< STensor3 > > dMechanicalFieldSourcedVectorCurl;

  //coupling with extraDof
  std::vector<std::vector< SVector3 > > dVectorFielddExtraDofField;
  std::vector<std::vector< STensor3 > > dVectorFielddGradExtraDofField;
  std::vector< std::vector< SVector3 > > dSourceVectorFielddExtraDofField;
  std::vector< std::vector< STensor3 > > dSourceVectorFielddGradExtraDofField;
  std::vector< std::vector< SVector3 > > dMechanicalFieldSourcedExtraDofField;
  std::vector< std::vector< STensor3 > > dMechanicalFieldSourcedGradExtraDofField;

  //coupling with nonLocal
  std::vector<std::vector< SVector3 > >   dVectorFielddNonLocalVariable;
  std::vector< std::vector< SVector3 > >  dSourceVectorFielddNonLocalVariable;
  std::vector< std::vector< SVector3 > >  dMechanicalFieldSourcedNonLocalVariable;


 public:
  constitutiveCurlData(const int numCurlVar=0, const int numNonLocal=0, const int numExtraDofDiffusion=0);
  constitutiveCurlData(const constitutiveCurlData& src);
  constitutiveCurlData& operator=(const constitutiveCurlData& src);
  virtual ~constitutiveCurlData(){};
  void restart();
  virtual int getConstitutiveExtraDofDiffusionVariable() const {return numConstitutiveExtraDofDiffusionVariable;}
  virtual int getNumNonLocalVariable() const {return numNonLocalVariable;}
  virtual int getNumConstitutiveCurlVariable() const {return numConstitutiveCurlVariable;}
};



class dG3DIPVariable : public dG3DIPVariableBase
{
 private:
  double                       barJ;
  double                       localJ;
  STensor3                     deformationGradient; //we actually store (barJ/J)^1/3 F when using barF method
  STensor3                     firstPiolaKirchhoffStress;
  STensor43                    tangentModuli; //include geometric terms
  STensor43                    elasticTangentModuli; //elastic tangent operators by blocking all dissipation mechanism
  const STensor43              *DGElasticTangentModuli;

  // for dG
  bool _oninter;
  SVector3                     referenceOutwardNormal;
  SVector3                     currentOutwardNormal;

  SVector3                     jump;
  SVector3                     incompatibleJump;

  STensor3 firstPiolaKirchhoffStressb4AV;

protected:
    nonlocalData *_nonlocalData;
    constitutiveExtraDofDiffusionData *_constitutiveExtraDofDiffusionData;
    constitutiveCurlData *_constitutiveCurlData;
    bodyForceForHO       *_bodyForceForHO;

 public:
  dG3DIPVariable(const bool createBodyForceHO, const bool oninter, int numNonLocal=0, int extraDofDiffusion=0,
  int curlVar=0, bool useOfEnergy=false, bool useOfEnergyConjugatedField=false); // A empty constructor is mandatory to build FractureCohesive3DIPVariable OK like this as the fracture is on interface??
  dG3DIPVariable(const dG3DIPVariable &source);
  virtual dG3DIPVariable &operator = (const IPVariable &_source);
  virtual ~dG3DIPVariable()
  {
    if(_nonlocalData!=NULL) delete _nonlocalData;
    if(_constitutiveExtraDofDiffusionData!=NULL) delete _constitutiveExtraDofDiffusionData;
    if(_constitutiveCurlData!=NULL) delete _constitutiveCurlData;
    if(_bodyForceForHO!=NULL) delete _bodyForceForHO;
  }
  virtual const bool isInterface() const {return _oninter;}

  virtual IPVariable* getInternalData() =0;
  virtual const IPVariable* getInternalData()  const =0;

  // Archiving data
  virtual double get(const int i) const;
  virtual double & getRef(const int i);
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const;
  virtual double damageEnergy() const{return 0.;};
  virtual double vonMises() const;
  virtual void getCauchyStress(STensor3 &cauchy) const;
  virtual double getJ(int dim=3) const;

  virtual double getBarJ() const {return barJ;};
  virtual double &getRefToBarJ() {return barJ;};
  virtual double getLocalJ() const {return localJ;};
  virtual double &getRefToLocalJ() {return localJ;};
  virtual void getBackLocalDeformationGradient(STensor3& localF) const;

  virtual const STensor3 &getConstRefToDeformationGradient() const {return deformationGradient;}
  virtual STensor3 &getRefToDeformationGradient() {return deformationGradient;}

  virtual const STensor3 &getConstRefToFirstPiolaKirchhoffStress() const {return firstPiolaKirchhoffStress;}
  virtual STensor3 &getRefToFirstPiolaKirchhoffStress() {return firstPiolaKirchhoffStress;}

  virtual const STensor43 &getConstRefToTangentModuli() const {return tangentModuli;}
  virtual STensor43 &getRefToTangentModuli() {return tangentModuli;}

  virtual const STensor43 &getConstRefToElasticTangentModuli() const {return elasticTangentModuli;}
  virtual STensor43 &getRefToElasticTangentModuli() {return elasticTangentModuli;}


  virtual const STensor43 &getConstRefToDGElasticTangentModuli() const {return *DGElasticTangentModuli;}
  virtual void setRefToDGElasticTangentModuli(const STensor43 &eT) {
                                                                  DGElasticTangentModuli=&eT;
                                                                 }

  virtual const SVector3 &getConstRefToReferenceOutwardNormal() const {return referenceOutwardNormal;}
  virtual SVector3 &getRefToReferenceOutwardNormal() {return referenceOutwardNormal;}

  virtual const SVector3 &getConstRefToCurrentOutwardNormal() const {return currentOutwardNormal;}
  virtual SVector3 &getRefToCurrentOutwardNormal() {return currentOutwardNormal;}

  virtual const SVector3 &getConstRefToJump() const {return jump;}
  virtual SVector3 &getRefToJump() {return jump;}

  virtual const SVector3& getConstRefToIncompatibleJump() const {return incompatibleJump;};
  virtual SVector3& getRefToIncompatibleJump() {return incompatibleJump;};

  // higher order body force
  virtual bool hasBodyForceForHO() const
  {
    if(_bodyForceForHO==NULL)
      return false;
    else
      return true;
  }
  virtual const STensor33 &getConstRefToGM() const
  {
    if(hasBodyForceForHO())
      return _bodyForceForHO->GM;
    else
    {
      Msg::Error("getConstRefToGM is not defined");
      static STensor33 VoidGM;
      return VoidGM;
    }
  }
  virtual STensor33 &getRefToGM()
  {
    if(hasBodyForceForHO())
      return _bodyForceForHO->GM;
    else
    {
      Msg::Error("getRefToGM is not defined");
      static STensor33 VoidGM;
      return VoidGM;
    }
  }

  virtual const STensor43 &getConstRefToDFmdFM() const
  {
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM;
    else{
      Msg::Error("getConstRefToDFmdFM is not defined");
    static STensor43 VoiddFmdFM;
    return VoiddFmdFM;
    }
  }
  virtual STensor43 &getRefToDFmdFM()
  {
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM;
    else
    {
      Msg::Error("getRefToDFmdFM is not defined");
      static STensor43 VoiddFmdFM;
      return VoiddFmdFM;
    }
  }

  virtual const STensor53 &getConstRefToDFmdGM() const
  {
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdGM;
    else{
      Msg::Error("getConstRefToDFmdGM is not defined");
    static STensor53 VoiddFmdGM;
    return VoiddFmdGM;
    }
  }
  virtual STensor53 &getRefToDFmdGM()
  {
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdGM;
    else
    {
      Msg::Error("getRefToDFmdGM is not defined");
      static STensor53 VoiddFmdGM;
      return VoiddFmdGM;
    }
  }

virtual const SVector3 &getConstRefToBm() const
  {
    if(hasBodyForceForHO())
      return _bodyForceForHO->Bm;
    else
    {
      Msg::Error("getConstRefToBm is not defined");
      static SVector3 VoidBm;
      return VoidBm;
    }
  }
virtual SVector3 &getRefToBm()
  {
    if(hasBodyForceForHO())
      return _bodyForceForHO->Bm;
    else
    {
      Msg::Error("getRefToBm is not defined");
      static SVector3 VoidBm;
      return VoidBm;
    }
  }

virtual const STensor43 &getConstRefTodBmdGM() const
  {
    if(hasBodyForceForHO())
      return _bodyForceForHO->dBmdGM;
    else{
      Msg::Error("getConstRefTodBmdGM is not defined");
    static STensor43 VoiddBmdGM;
    return VoiddBmdGM;
    }
  }
  virtual STensor43 &getRefTodBmdGM()
  {
    if(hasBodyForceForHO())
      return _bodyForceForHO->dBmdGM;
    else{
      Msg::Error("getConstRefTodBmdGM is not defined");
      static STensor43 VoiddBmdGM;
      return VoiddBmdGM;
    }
  }


virtual const STensor33 &getConstRefTodBmdFm() const
  {
    if(hasBodyForceForHO())
      return _bodyForceForHO->dBmdFm;
    else
    {
      Msg::Error("getConstRefTodBmdFm is not defined");
      static STensor33 VoiddBmdFm;
      return VoiddBmdFm;
    }
  }
virtual STensor33 &getRefTodBmdFm()
  {
    if(hasBodyForceForHO())
      return _bodyForceForHO->dBmdFm;
    else
    {
      Msg::Error("getRefTodBmdFm is not defined");
      static STensor33 VoiddBmdFm;
      return VoiddBmdFm;
    }
  }

virtual const STensor43 &getConstRefTodPmdFM() const
  {
    if(hasBodyForceForHO())
      return _bodyForceForHO->dPmdFM;
    else
    {
      Msg::Error("getConstRefTodPmdFM is not defined");
      static STensor43 VoiddPmdFM;
      return VoiddPmdFM;
    }
  }

virtual STensor43 &getRefTodPmdFM()
  {
    if(hasBodyForceForHO())
      return _bodyForceForHO->dPmdFM;
    else
    {
      Msg::Error("getRefTodPmdFM is not defined");
      static STensor43 VoiddPmdFM;
      return VoiddPmdFM;
    }
  }

virtual const STensor63 &getConstRefTodCmdFm() const
  {
    if(hasBodyForceForHO())
      return _bodyForceForHO->dCmdFm;
    else
    {
      Msg::Error("getConstRefTodCmdFm is not defined");
      static STensor63 VoiddCmdFm;
      return VoiddCmdFm;
    }
  }
virtual STensor63 &getRefTodCmdFm()
  {
    if(hasBodyForceForHO())
      return _bodyForceForHO->dCmdFm;
    else
    {
      Msg::Error("getRefTodCmdFm is not defined");
      static STensor63 VoiddCmdFm;
      return VoiddCmdFm;
    }
  }
virtual STensor63 *getPtrTodCmdFm()
  {
    if(hasBodyForceForHO())
      return &(_bodyForceForHO->dCmdFm);
    else
      return NULL;
  }

virtual void computeBodyForce(const STensor33& G, SVector3& B) const;
virtual void setValueForBodyForce(const STensor43& K, const int _ModuliType);

  // non local

  virtual int getNumberNonLocalVariable() const
  {
    if(_nonlocalData==NULL)
      return 0;
    else
      return _nonlocalData->getNumNonLocalVariable();
  };
  //

  virtual double getConstRefToLocalVariable(const int idex) const {Msg::Error("getConstRefToLocalVariable not defined"); return 0.;};
  virtual double& getRefToLocalVariable(const int idex) {Msg::Error("getRefToLocalVariable not defined"); static double a=0; return a;};
  //
  virtual double getConstRefToNonLocalVariableInMaterialLaw(const int idex) const {Msg::Error("getConstRefToNonLocalVariable not defined"); static double a=0; return a;};
  virtual double& getRefToNonLocalVariableInMaterialLaw(const int idex) {Msg::Error("getRefToNonLocalVariable not defined"); static double a=0; return a;};
  //
  virtual double getConstRefToNonLocalVariable(const int idex) const
  {
    if(_nonlocalData==NULL)
      Msg::Error("getConstRefToNonLocalVariable(): _nonlocalData not created");
    return _nonlocalData->nonLocalVariable(idex);
  };
  virtual double& getRefToNonLocalVariable(const int idex)
  {
    if(_nonlocalData==NULL)
      Msg::Error("getRefToNonLocalVariable(): _nonlocalData not created");
    return _nonlocalData->nonLocalVariable(idex);
  };
  //
  virtual const std::vector<STensor3> &getConstRefToDLocalVariableDStrain() const
  {
    if(_nonlocalData==NULL)
      Msg::Error("getConstRefToDLocalVariableDStrain(): _nonlocalData not created");
    return _nonlocalData->dLocalVariableDStrain;
  }
  virtual std::vector<STensor3> &getRefToDLocalVariableDStrain()
  {
    if(_nonlocalData==NULL)
      Msg::Error("getRefToDLocalVariableDStrain(): _nonlocalData not created");
    return _nonlocalData->dLocalVariableDStrain;
  }

  virtual const std::vector<STensor3> &getConstRefToDStressDNonLocalVariable() const
  {
    if(_nonlocalData==NULL)
      Msg::Error("getConstRefToDStressDNonLocalVariable(): _nonlocalData not created");
    return _nonlocalData->dStressDNonLocalVariable;
  }
  virtual std::vector<STensor3> &getRefToDStressDNonLocalVariable()
  {
    if(_nonlocalData==NULL)
      Msg::Error("getRefToDStressDNonLocalVariable(): _nonlocalData not created");
    return _nonlocalData->dStressDNonLocalVariable;
  }

  virtual const fullMatrix<double> &getConstRefToDLocalVariableDNonLocalVariable() const
  {
    if(_nonlocalData==NULL)
      Msg::Error("getConstRefToDLocalVariableDNonLocalVariable(): _nonlocalData not created");
    return _nonlocalData->dLocalVariableDNonLocalVariable;
  }
  virtual fullMatrix<double> &getRefToDLocalVariableDNonLocalVariable()
  {
    if(_nonlocalData==NULL)
      Msg::Error("getRefToDLocalVariableDNonLocalVariable(): _nonlocalData not created");
    return _nonlocalData->dLocalVariableDNonLocalVariable;
  }

  virtual const fullVector<double> &getConstRefToNonLocalJump() const
  {
    if(_nonlocalData==NULL)
      Msg::Error("getConstRefToNonLocalJump(): _nonlocalData not created");
    return _nonlocalData->nonLocalJump;
  }
  virtual fullVector<double> &getRefToNonLocalJump()
  {
    if(_nonlocalData==NULL)
      Msg::Error("getRefToNonLocalJump(): _nonlocalData not created");
    return _nonlocalData->nonLocalJump;
  }

  virtual const std::vector<SVector3> &getConstRefToGradNonLocalVariable() const
  {
    if(_nonlocalData==NULL)
      Msg::Error("getConstRefToGradNonLocalVariable(): _nonlocalData not created");
    return _nonlocalData->gradNonLocalVariable;
  }
  virtual std::vector<SVector3> &getRefToGradNonLocalVariable()
  {
    if(_nonlocalData==NULL)
      Msg::Error("getRefToGradNonLocalVariable(): _nonlocalData not created");
    return _nonlocalData->gradNonLocalVariable;
  }
  //coupling non local with constitutiveExtraDof
  virtual const fullMatrix<double> & getConstRefTodLocalVariableDExtraDofDiffusionField() const
  {
    if(_nonlocalData==NULL)
      Msg::Error("getConstRefTodLocalVariableDExtraDofDiffusionField(): _nonlocalData not created");
    return _nonlocalData->dLocalVariableDExtraDofDiffusionField;
  }
  virtual fullMatrix<double> & getRefTodLocalVariableDExtraDofDiffusionField()
  {
    if(_nonlocalData==NULL)
      Msg::Error("getRefTodLocalVariableDExtraDofDiffusionField(): _nonlocalData not created");
    return _nonlocalData->dLocalVariableDExtraDofDiffusionField;
  }

  // coupling nonlocal with constitutiveCurlData
  virtual const std::vector< std::vector< SVector3 > > & getConstRefTodLocalVariabledVectorPotential() const
  {
      if(_nonlocalData == NULL)
          Msg::Error("getConstRefTodLocalVariabledVectorPotential(): _nonLocalData not created");
      return _nonlocalData->dLocalVariabledVectorPotential;
  }
    virtual std::vector<std::vector< SVector3 > > & getRefTodLocalVariabledVectorPotential()
    {
        if(_nonlocalData == NULL)
            Msg::Error("getRefTodLocalVariabledVectorPotential(): _nonlocalData not created");
        return _nonlocalData->dLocalVariabledVectorPotential;
    }

  virtual const std::vector< std::vector< SVector3 > > & getConstRefTodLocalVariabledVectorCurl() const
  {
    if(_nonlocalData == NULL)
      Msg::Error("getConstRefTodLocalVariabledVectorCurl(): _nonLocalData not created");
    return _nonlocalData->dLocalVariabledVectorCurl;
  }
  virtual std::vector<std::vector< SVector3 > > & getRefTodLocalVariabledVectorCurl()
  {
    if(_nonlocalData == NULL)
      Msg::Error("getRefTodLocalVariabledVectorCurl(): _nonlocalData not created");
    return _nonlocalData->dLocalVariabledVectorCurl;
  }

  virtual const STensor3 &getConstRefToCharacteristicLengthMatrix(const int idx) const {Msg::Error("getConstRefToCharacteristicLengthMatrix not defined");  static STensor3 a; return a;};

  //-----------------get 1st piola before AV ------------//
  virtual const STensor3 &getConstRefToFirstPiolaKirchhoffStressb4AV() const {return firstPiolaKirchhoffStressb4AV;}
  virtual STensor3 &getRefToFirstPiolaKirchhoffStressb4AV() {return firstPiolaKirchhoffStressb4AV;}

  // for path following based on  irreversibe energt
  virtual double irreversibleEnergy() const {return 0.;};
  virtual double& getRefToIrreversibleEnergy() {
    Msg::Error("dG3DIPVariable::getRefToIrreversibleEnergy has not been implemented");
    static double a=0;
    return a;
  };

  virtual const STensor3& getConstRefToDIrreversibleEnergyDDeformationGradient() const {
    Msg::Error("dG3DIPVariable::getConstRefToDIrreversibleEnergyDDeformationGradient has not been implemented");
    static STensor3 a;
    return a;
  };
  virtual STensor3& getRefToDIrreversibleEnergyDDeformationGradient() {
    Msg::Error("dG3DIPVariable::getConstRefToDIrreversibleEnergyDDeformationGradient has not been implemented");
    static STensor3 a;
    return a;
  };

  // irreversible ennergy depend on nonlocal variale
  virtual const double& getConstRefToDIrreversibleEnergyDNonLocalVariable(const int index) const {
    Msg::Error("dG3DIPVariable::getConstRefToDIrreversibleEnergyDNonLocalVariable has not been implemented");
    static double a=0;
    return a;
  };
  virtual double& getRefToDIrreversibleEnergyDNonLocalVariable(const int index) {
    Msg::Error("dG3DIPVariable::getRefToDIrreversibleEnergyDNonLocalVariable has not been implemented");
    static double a=0;
    return a;
  };
  //
  // Extra Dof

  virtual int getNumConstitutiveExtraDofDiffusionVariable() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      return 0;
    else
      return _constitutiveExtraDofDiffusionData->getConstitutiveExtraDofDiffusionVariable();
  }

  virtual double getConstRefToField(const int idex) const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefToField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->field(idex);
  }
  virtual double  &getRefToField(const int idex)
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefToField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->field(idex);
  }
  virtual const std::vector<SVector3>  &getConstRefToGradField() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefToGradField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->gradField;
  }
  virtual std::vector<SVector3>  &getRefToGradField()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefToGradField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->gradField;
  }
  virtual const std::vector<SVector3>  &getConstRefToFlux() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefToFlux() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->flux;
  }
  virtual std::vector<SVector3>  &getRefToFlux()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefToFlux() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->flux;
  }
  virtual const std::vector<SVector3> &getConstRefToElectricDisplacement() const
  {
      if(_constitutiveExtraDofDiffusionData==NULL)
          Msg::Error("getConstRefToElectricDisplacement() : _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->electricDisplacement;
  }
  virtual std::vector<SVector3> &getRefToElectricDisplacement()
  {
      if(_constitutiveExtraDofDiffusionData==NULL)
          Msg::Error("getRefToElectricDisplacement() : _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->electricDisplacement;
  }
  virtual const std::vector<STensor3>  &getConstRefTodPdField() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodPdField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dPdField;
  }
  virtual std::vector<STensor3>  &getRefTodPdField()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodPdField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dPdField;
  }
  virtual const std::vector<STensor33>  &getConstRefTodPdGradField() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodPdGradField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dPdGradField;
  }
  virtual std::vector<STensor33>  &getRefTodPdGradField()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodPdGradField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dPdGradField;
  }
  virtual const std::vector< std::vector<STensor3> >  &getConstRefTodFluxdGradField() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodFluxdGradField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFluxdGradField;
  }
  virtual std::vector<std::vector<STensor3> >  &getRefTodFluxdGradField()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodFluxdGradField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFluxdGradField;
  }
  virtual const std::vector<std::vector<SVector3> >  &getConstRefTodFluxdField() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodFluxdField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFluxdField;
  }
  virtual std::vector<std::vector<SVector3> >  &getRefTodFluxdField()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodFluxdField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFluxdField;
  }
  virtual const std::vector<STensor33>   &getConstRefTodFluxdF() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodFluxdF() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFluxdF;
  }
  virtual std::vector<STensor33>   &getRefTodFluxdF()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodFluxdF() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFluxdF;
  }
  virtual const std::vector< std::vector<SVector3> > &getConstRefTodElecDisplacementdField() const
  {
      if(_constitutiveExtraDofDiffusionData==NULL)
          Msg::Error("getConstRefTodElecDisplacementdField() : _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->dElecDisplacementdField;
  }
  virtual std::vector< std::vector<SVector3> > &getRefTodElecDisplacementdField()
  {
      if(_constitutiveExtraDofDiffusionData==NULL)
          Msg::Error("getRefTodElecDisplacementdField() : _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->dElecDisplacementdField;
  }
  virtual const std::vector< std::vector<STensor3> > &getConstRefTodElecDisplacementdGradField() const
  {
      if(_constitutiveExtraDofDiffusionData==NULL)
          Msg::Error("getConstRefTodElecDisplacementdGradField() : _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->dElecDisplacementdGradField;
  }
  virtual std::vector< std::vector<STensor3> > &getRefTodElecDisplacementdGradField()
  {
      if(_constitutiveExtraDofDiffusionData==NULL)
          Msg::Error("getRefTodElecDisplacementdGradField() : _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->dElecDisplacementdGradField;
  }
  virtual const std::vector<STensor33> &getConstRefTodElecDisplacementdF() const
  {
      if(_constitutiveExtraDofDiffusionData==NULL)
          Msg::Error("getConstRefTodElecDisplacementdF() : _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->dElecDisplacementdF;
  }
  virtual std::vector<STensor33> &getRefTodElecDisplacementdF()
  {
      if(_constitutiveExtraDofDiffusionData==NULL)
          Msg::Error("getRefTodElecDisplacementdF() : _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->dElecDisplacementdF;
  }
  virtual const std::vector<std::vector<const STensor3*> > &getConstRefToLinearK() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTolinearK() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->linearK;
  }
  virtual void setConstRefToLinearK(const STensor3 &eT, int i, int j)
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("setConstRefTolinearK() : _constitutiveExtraDofDiffusionData not created");
    else
    {
      if(i < getNumConstitutiveExtraDofDiffusionVariable() && j< getNumConstitutiveExtraDofDiffusionVariable())
         _constitutiveExtraDofDiffusionData->linearK[i][j] = &eT;
      else
         Msg::Error("setConstRefTolinearK() : index out of range");
    }
  }

  // coupling constitutive extra dof with curl
    virtual const std::vector< std::vector< STensor3 > > & getConstRefTodFluxdVectorPotential() const
    {
        if(_constitutiveExtraDofDiffusionData==NULL)
            Msg::Error("getConstRefTodFluxdVectorPotential() : _constitutiveExtraDofDiffusionData not created");
        return _constitutiveExtraDofDiffusionData->dFluxdVectorPotential;
    }

    virtual std::vector<std::vector< STensor3 > > & getRefTodFluxdVectorPotential()
    {
        if(_constitutiveExtraDofDiffusionData==NULL)
            Msg::Error("getRefTodFluxdVectorPotential() : _constitutiveExtraDofDiffusionData not created");
        return _constitutiveExtraDofDiffusionData->dFluxdVectorPotential;
    }

  virtual const std::vector< std::vector< STensor3 > > & getConstRefTodFluxdVectorCurl() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodFluxdVectorCurl() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFluxdVectorCurl;
  }

    virtual const std::vector< std::vector< SVector3 > > & getConstRefTodFieldSourcedVectorPotential() const
    {
        if(_constitutiveExtraDofDiffusionData==NULL)
            Msg::Error("getConstRefTodFieldSourcedVectorPotential() : _constitutiveExtraDofDiffusionData not created");
        return _constitutiveExtraDofDiffusionData->dFieldSourcedVectorPotential;
    }

    virtual const std::vector< std::vector< SVector3 > > & getConstRefTodMechanicalSourcedVectorPotential() const
    {
        if(_constitutiveExtraDofDiffusionData==NULL)
            Msg::Error("getConstRefTodMechanicalSourcedVectorPotential() : _constitutiveExtraDofDiffusionData not created");
        return _constitutiveExtraDofDiffusionData->dMechanicalSourcedVectorPotential;
    }

  virtual const std::vector< std::vector< SVector3 > > & getConstRefTodFieldSourcedVectorCurl() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodFieldSourcedVectorCurl() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFieldSourcedVectorCurl;
  }

  virtual const std::vector< std::vector< SVector3 > > & getConstRefTodMechanicalSourcedVectorCurl() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodMechanicalSourcedVectorCurl() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dMechanicalSourcedVectorCurl;
  }

  virtual std::vector<std::vector< STensor3 > > & getRefTodFluxdVectorCurl()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodFluxdVectorCurl() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFluxdVectorCurl;
  }

    virtual std::vector< std::vector< SVector3 > > & getRefTodFieldSourcedVectorPotential()
    {
        if(_constitutiveExtraDofDiffusionData==NULL)
            Msg::Error("getRefTodFieldSourcedVectorPotential() : _constitutiveExtraDofDiffusionData not created");
        return _constitutiveExtraDofDiffusionData->dFieldSourcedVectorPotential;
    }

    virtual std::vector< std::vector< SVector3 > > & getRefTodMechanicalSourcedVectorPotential()
    {
        if(_constitutiveExtraDofDiffusionData==NULL)
            Msg::Error("getRefTodMechanicalSourcedVectorPotential() : _constitutiveExtraDofDiffusionData not created");
        return _constitutiveExtraDofDiffusionData->dMechanicalSourcedVectorPotential;
    }

  virtual std::vector< std::vector< SVector3 > > & getRefTodFieldSourcedVectorCurl()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodFieldSourcedVectorCurl() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFieldSourcedVectorCurl;
  }

  virtual std::vector< std::vector< SVector3 > > & getRefTodMechanicalSourcedVectorCurl()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodMechanicalSourcedVectorCurl() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dMechanicalSourcedVectorCurl;
  }
    virtual const std::vector<std::vector< STensor3 > > & getConstRefTodElecDisplacementdVectorPotential() const
    {
        if(_constitutiveExtraDofDiffusionData==NULL)
            Msg::Error("getConstRefTodElecDisplacementdVectorPotential() : _constitutiveExtraDofDiffusionData not created");
        return _constitutiveExtraDofDiffusionData->dElecDisplacementdVectorPotential;
    }
    virtual const std::vector<std::vector< STensor3 > > & getConstRefTodElecDisplacementdVectorCurl() const
    {
        if(_constitutiveExtraDofDiffusionData==NULL)
            Msg::Error("getConstRefTodElecDisplacementdVectorCurl() : _constitutiveExtraDofDiffusionData not created");
        return _constitutiveExtraDofDiffusionData->dElecDisplacementdVectorCurl;
    }
    virtual std::vector<std::vector< STensor3 > > & getRefTodElecDisplacementdVectorPotential()
    {
        if(_constitutiveExtraDofDiffusionData==NULL)
            Msg::Error("getRefTodElecDisplacementdVectorPotential() : _constitutiveExtraDofDiffusionData not created");
        return _constitutiveExtraDofDiffusionData->dElecDisplacementdVectorPotential;
    }
    virtual std::vector<std::vector< STensor3 > > & getRefTodElecDisplacementdVectorCurl()
    {
        if(_constitutiveExtraDofDiffusionData==NULL)
            Msg::Error("getRefTodElecDisplacementdVectorCurl() : _constitutiveExtraDofDiffusionData not created");
        return _constitutiveExtraDofDiffusionData->dElecDisplacementdVectorCurl;
    }

  virtual const fullVector<double> & getConstRefToEMFieldSource() const
  {
      if (_constitutiveExtraDofDiffusionData == NULL)
          Msg::Error("getConstRefToEMFieldSource(): _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->emFieldSource;
  }

  virtual fullVector<double> & getRefToEMFieldSource()
  {
      if (_constitutiveExtraDofDiffusionData == NULL)
          Msg::Error("getRefToEMFieldSource(): _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->emFieldSource;
  }

  virtual const fullMatrix<double> & getConstRefTodEMFieldSourcedField() const
  {
      if (_constitutiveExtraDofDiffusionData == NULL)
          Msg::Error("getConstRefTodEMFieldSourcedField(): _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->demFieldSourcedField;
  }

  virtual fullMatrix<double> & getRefTodEMFieldSourcedField()
  {
      if (_constitutiveExtraDofDiffusionData == NULL)
          Msg::Error("getRefTodEMFieldSourcedField(): _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->demFieldSourcedField;
  }

  virtual const std::vector< std::vector< SVector3 > > & getConstRefTodEMFieldSourcedGradField() const
  {
      if (_constitutiveExtraDofDiffusionData == NULL)
          Msg::Error("getConstRefTodEMFieldSourcedGradField(): _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->demFieldSourcedGradField;
  }

  virtual std::vector< std::vector< SVector3 > > & getRefTodEMFieldSourcedGradField()
  {
      if (_constitutiveExtraDofDiffusionData == NULL)
          Msg::Error("getRefTodEMFieldSourcedGradField(): _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->demFieldSourcedGradField;
  }

  virtual const std::vector< std::vector< SVector3 > > & getConstRefTodEMFieldSourcedVectorPotential() const
  {
      if (_constitutiveExtraDofDiffusionData == NULL)
          Msg::Error("getConstRefTodEMFieldSourcedVectorPotential(): _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->demFieldSourcedVectorPotential;
  }

  virtual std::vector< std::vector< SVector3 > > & getRefTodEMFieldSourcedVectorPotential()
  {
      if (_constitutiveExtraDofDiffusionData == NULL)
          Msg::Error("getRefTodEMFieldSourcedVectorPotential(): _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->demFieldSourcedVectorPotential;
  }

  virtual const std::vector< std::vector< SVector3 > > & getConstRefTodEMFieldSourcedVectorCurl() const
  {
      if (_constitutiveExtraDofDiffusionData == NULL)
          Msg::Error("getConstRefTodEMFieldSourcedVectorCurl(): _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->demFieldSourcedVectorCurl;
  }

  virtual std::vector< std::vector< SVector3 > > & getRefTodEMFieldSourcedVectorCurl()
  {
      if (_constitutiveExtraDofDiffusionData == NULL)
          Msg::Error("getRefTodEMFieldSourcedVectorCurl(): _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->demFieldSourcedVectorCurl;
  }

  virtual const std::vector< STensor3 > & getConstRefTodEMFieldSourcedF() const
  {
      if (_constitutiveExtraDofDiffusionData == NULL)
          Msg::Error("getConstRefTodEMFieldSourcedF(): _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->demFieldSourcedF;
  }

  virtual std::vector< STensor3 > & getRefTodEMFieldSourcedF()
  {
      if (_constitutiveExtraDofDiffusionData == NULL)
          Msg::Error("getRefTodEMFieldSourcedF(): _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->demFieldSourcedF;
  }

/*  virtual const std::vector<std::vector<const STensor3*> > &getConstRefToEnergyK() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefToEnergyK() : _constitutiveExtraDofDiffusionData not created");
    else
      return _constitutiveExtraDofDiffusionData->EnergyK;
  }
  virtual void setConstRefToEnergyK(const STensor3 &eT, int i, int j)
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("setConstRefToEnergyK() : _constitutiveExtraDofDiffusionData not created");
    else
    {
      if(i < getNumConstitutiveExtraDofDiffusionVariable() && j< getNumConstitutiveExtraDofDiffusionVariable())
         _constitutiveExtraDofDiffusionData->EnergyK[i][j] = &eT;
      else
         Msg::Error("setConstRefToEnergyK() : index out of range");
    }
  }
  virtual const std::vector< std::vector<std::vector<STensor3> > >  &getConstRefTodEnergyKdField() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodEnergyKdField() : _constitutiveExtraDofDiffusionData not created");
    else
      return _constitutiveExtraDofDiffusionData->dEnergyKdField;
  }
  virtual std::vector< std::vector<std::vector<STensor3> > >  &getRefTodEnergyKdField()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodEnergyKdField() : _constitutiveExtraDofDiffusionData not created");
    else
      return _constitutiveExtraDofDiffusionData->dEnergyKdField;
  }
  virtual const std::vector<std::vector<STensor43>>  &getConstRefTodEnergyKdF() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodEnergyKdF() : _constitutiveExtraDofDiffusionData not created");
    else
      return _constitutiveExtraDofDiffusionData->dEnergyKdF;
  }
  virtual std::vector<std::vector<STensor43>>  &getRefTodEnergyKdF()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodEnergyKdF() : _constitutiveExtraDofDiffusionData not created");
    else
      return _constitutiveExtraDofDiffusionData->dEnergyKdF;
  }*/
  virtual const std::vector< std::vector<std::vector<STensor3> > >  &getConstRefTodFluxdGradFielddField() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodFluxdGradFielddField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFluxdGradFielddField;
  }
  virtual std::vector< std::vector<std::vector<STensor3> > >  &getRefTodFluxdGradFielddField()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodFluxdGradFielddField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFluxdGradFielddField;
  }
  virtual const std::vector<std::vector<STensor43> >  &getConstRefTodFluxdGradFielddF() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodFluxdGradFielddF() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFluxdGradFielddF;
  }
  virtual std::vector<std::vector<STensor43> >  &getRefTodFluxdGradFielddF()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodFluxdGradFielddF() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFluxdGradFielddF;
  }
  virtual const std::vector< std::vector< std::vector<STensor3> > >  &getConstRefTodLinearKdField() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodLinearKdField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dlinearKdField;
  }
  virtual std::vector< std::vector< std::vector<STensor3> > >  &getRefTodLinearKdField()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodLinearKdField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dlinearKdField;
  }
  virtual const std::vector< std::vector<STensor43> >  &getConstRefTodLinearKdF() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodLinearKdF() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dlinearKdF;
  }
  virtual std::vector< std::vector<STensor43> >  &getRefTodLinearKdF()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodLinearKdF() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dlinearKdF;
  }
  virtual const fullVector<double>  &getConstRefToFieldSource() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefToFieldSource() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->fieldSource;
  }
  virtual fullVector<double>  &getRefToFieldSource()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefToFieldSource() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->fieldSource;
  }
  virtual const fullMatrix<double>  &getConstRefTodFieldSourcedField() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodFieldSourcedField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFieldSourcedField;
  }
  virtual fullMatrix<double>  &getRefTodFieldSourcedField()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodFieldSourcedField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFieldSourcedField;
  }
  virtual const std::vector<std::vector<SVector3> >  &getConstRefTodFieldSourcedGradField() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodFluxdField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFieldSourcedGradField;
  }
  virtual std::vector<std::vector<SVector3> >  &getRefTodFieldSourcedGradField()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodFieldSourcedGradField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFieldSourcedGradField;
  }
  virtual const std::vector<STensor3>  &getConstRefTodFieldSourcedF() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodFieldSourcedF() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFieldSourcedF;
  }
  virtual std::vector<STensor3>  &getRefTodFieldSourcedF()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodFieldSourcedF() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFieldSourcedF;
  }
  virtual const fullVector<double>  &getConstRefToMechanicalSource() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefToMechanicalSource() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->mechanicalSource;
  }
  virtual fullVector<double>  &getRefToMechanicalSource()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefToMechanicalSource() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->mechanicalSource;
  }
  virtual const fullMatrix<double>  &getConstRefTodMechanicalSourcedField() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodMechanicalSourcedField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dMechanicalSourcedField;
  }
  virtual fullMatrix<double>  &getRefTodMechanicalSourcedField()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodMechanicalSourcedField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dMechanicalSourcedField;
  }
  virtual const std::vector<std::vector<SVector3> >  &getConstRefTodMechanicalSourcedGradField() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodMechanicalSourcedGradField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dMechanicalSourcedGradField;
  }
  virtual std::vector<std::vector<SVector3> >  &getRefTodMechanicalSourcedGradField()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodMechanicalSourcedGradField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dMechanicalSourcedGradField;
  }
  virtual const std::vector<STensor3>  &getConstRefTodMechanicalSourcedF() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodMechanicalSourcedF() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dMechanicalSourcedF;
  }
  virtual std::vector<STensor3>  &getRefTodMechanicalSourcedF()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodMechanicalSourcedF() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dMechanicalSourcedF;
  }
  virtual const fullVector<double>  &getConstRefToExtraDofFieldCapacityPerUnitField() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefToExtraDofFieldCapacityPerUnitField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->fieldCapacityPerUnitField;
  }
  virtual fullVector<double>  &getRefToExtraDofFieldCapacityPerUnitField()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefToExtraDofFieldCapacityPerUnitField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->fieldCapacityPerUnitField;
  }
  virtual const fullVector<double>  &getConstRefToFieldJump() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefToFieldJump() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->fieldJump;
  }
  virtual fullVector<double>  &getRefToFieldJump()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefToFieldJump() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->fieldJump;
  }
  virtual const fullVector<double>  &getConstRefToOneOverFieldJump() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefToOneOverFieldJump() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->oneOverFieldJump;
  }
  virtual fullVector<double>  &getRefToOneOverFieldJump()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefToOneOverFieldJump() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->oneOverFieldJump;
  }
  virtual const fullMatrix<double>  &getConstRefTodOneOverFieldJumpdFieldm() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodOneOverFieldJumpdFieldm : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dOneOverFieldJumpdFieldm;
  }
  virtual fullMatrix<double>  &getRefTodOneOverFieldJumpdFieldm()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodOneOverFieldJumpdFieldm() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dOneOverFieldJumpdFieldm;
  }
  virtual const fullMatrix<double>  &getConstRefTodOneOverFieldJumpdFieldp() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodOneOverFieldJumpdFieldp : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dOneOverFieldJumpdFieldp;
  }
  virtual fullMatrix<double>  &getRefTodOneOverFieldJumpdFieldp()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodOneOverFieldJumpdFieldp() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dOneOverFieldJumpdFieldp;
  }
  virtual const std::vector<SVector3>  &getConstRefToInterfaceFlux() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefToInterfaceFlux : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->interfaceFlux;
  }
  virtual std::vector<SVector3>  &getRefToInterfaceFlux()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefToInterfaceFlux() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->interfaceFlux;
  }
  virtual const std::vector<const STensor3*> &getConstRefToLinearSymmetrizationCoupling() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefToLinearSymmetrizationCoupling() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->linearSymmetrizationCoupling;
  }
  virtual void setConstRefToLinearSymmetrizationCoupling(const STensor3 &eT, int i)
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("setConstRefToLinearSymmetrizationCoupling() : _constitutiveExtraDofDiffusionData not created");
    else
    {
      if(i < getNumConstitutiveExtraDofDiffusionVariable())
         _constitutiveExtraDofDiffusionData->linearSymmetrizationCoupling[i] = &eT;
      else
         Msg::Error("setConstRefToLinearSymmetrizationCoupling() : index out of range");
    }
  }
  //coupling constitutive extra dof with nonLocal
  virtual const std::vector<std::vector<SVector3> > & getConstRefTodFluxdNonLocalVariable() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodFluxdNonLocalVariable() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFluxdNonLocalVariable;
  }
  virtual std::vector<std::vector<SVector3> > & getRefTodFluxdNonLocalVariable()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodFluxdNonLocalVariable() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFluxdNonLocalVariable;
  }
  virtual const fullMatrix<double> &getConstRefTodFieldSourcedNonLocalVariable() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodFieldSourcedNonLocalVariable() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFieldSourcedNonLocalVariable;
  }
  virtual fullMatrix<double> &getRefTodFieldSourcedNonLocalVariable()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodFieldSourcedNonLocalVariable() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFieldSourcedNonLocalVariable;
  }
  virtual const fullMatrix<double> &getConstRefTodMechanicalSourcedNonLocalVariable() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodMechanicalSourcedNonLocalVariable() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dMechanicalSourcedNonLocalVariable;
  }
  virtual fullMatrix<double> &getRefTodMechanicalSourcedNonLocalVariable()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodMechanicalSourcedNonLocalVariable() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dMechanicalSourcedNonLocalVariable;
  }

  virtual const fullMatrix<double> & getConstRefTodEMFieldSourcedNonLocalVariable() const
  {
      if (_constitutiveExtraDofDiffusionData == NULL)
          Msg::Error("getConstRefTodEMFieldSourcedNonLocalVariable(): _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->dEMFieldSourcedNonLocalVariable;
  }

  virtual fullMatrix<double> & getRefTodEMFieldSourcedNonLocalVariable()
  {
      if (_constitutiveExtraDofDiffusionData == NULL)
          Msg::Error("getRefTodEMFieldSourcedNonLocalVariable(): _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->dEMFieldSourcedNonLocalVariable;
  }
  virtual const std::vector< std::vector<SVector3> > &getConstRefTodElecDisplacementdNonLocalVariable() const
  {
      if (_constitutiveExtraDofDiffusionData == NULL)
          Msg::Error("getConstRefTodElecDisplacementdNonLocalVariable(): _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->dElecDisplacementdNonLocalVariable;
  }
  virtual std::vector< std::vector<SVector3> > &getRefTodElecDisplacementdNonLocalVariable()
  {
      if (_constitutiveExtraDofDiffusionData == NULL)
          Msg::Error("getRefTodElecDisplacementdNonLocalVariable(): _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->dElecDisplacementdNonLocalVariable;
  }

  // coupling curl data with nonlocal
  virtual const std::vector<std::vector< SVector3 > > & getConstRefTodVectorFielddNonLocalVariable() const
  {
    if (_constitutiveCurlData == NULL)
        Msg::Error("getConstRefTodVectorFielddNonLocalVariable() : _constitutiveCurlData not created");
    return _constitutiveCurlData->dVectorFielddNonLocalVariable;
  }

  virtual std::vector<std::vector< SVector3 > > & getRefTodVectorFielddNonLocalVariable()
  {
    if (_constitutiveCurlData == NULL)
        Msg::Error("getRefTodVectorFielddNonLocalVariable() : _constitutiveCurlData not created");
    return _constitutiveCurlData->dVectorFielddNonLocalVariable;
  }

  virtual const std::vector< std::vector< SVector3 > > & getConstRefTodSourceVectorFielddNonLocalVariable() const
  {
    if (_constitutiveCurlData == NULL)
        Msg::Error("getConstRefTodSourceVectorFielddNonLocalVariable() : _constitutiveCurlData not created");
    return _constitutiveCurlData->dSourceVectorFielddNonLocalVariable;
  }

  virtual std::vector< std::vector< SVector3 > > & getRefTodSourceVectorFielddNonLocalVariable()
  {
    if (_constitutiveCurlData == NULL)
        Msg::Error("getRefTodSourceVectorFielddNonLocalVariable() : _constitutiveCurlData not created");
    return _constitutiveCurlData->dSourceVectorFielddNonLocalVariable;
  }

  virtual const std::vector< std::vector< SVector3 > > & getConstRefTodMechanicalFieldSourcedNonLocalVariable() const
  {
    if (_constitutiveCurlData == NULL)
        Msg::Error("getConstRefTodMechanicalFieldSourcedNonLocalVariable() : _constitutiveCurlData not created");
    return _constitutiveCurlData->dMechanicalFieldSourcedNonLocalVariable;
  }

  virtual std::vector< std::vector< SVector3 > > & getRefTodMechanicalFieldSourcedNonLocalVariable()
  {
    if (_constitutiveCurlData == NULL)
        Msg::Error("getRefTodMechanicalFieldSourcedNonLocalVariable() : _constitutiveCurlData not created");
    return _constitutiveCurlData->dMechanicalFieldSourcedNonLocalVariable;
  }

  // curl data
  virtual int getNumConstitutiveCurlVariable() const
  {
    if(_constitutiveCurlData==NULL)
      return 0;
    else
      return _constitutiveCurlData->getNumConstitutiveCurlVariable();
  }

  virtual const SVector3 & getConstRefToVectorPotential(const int idex) const
  {
    if(_constitutiveCurlData==NULL)
      Msg::Error("getConstRefToVectorPotential() : _constitutiveCurlData not created");
    return _constitutiveCurlData->vectorPotential[idex];
  }

  virtual SVector3 & getRefToVectorPotential(const int idex)
  {
    if(_constitutiveCurlData==NULL)
      Msg::Error("getRefToVectorPotential() : _constitutiveCurlData not created");
    return _constitutiveCurlData->vectorPotential[idex];
  }

  virtual const SVector3 & getConstRefToVectorCurl(const int idex) const
  {
    if(_constitutiveCurlData==NULL)
      Msg::Error("getConstRefToVectorCurl() : _constitutiveCurlData not created");
    return _constitutiveCurlData->vectorCurl[idex];
  }

  virtual SVector3 & getRefToVectorCurl(const int idex)
  {
    if(_constitutiveCurlData==NULL)
      Msg::Error("getRefToVectorCurl() : _constitutiveCurlData not created");
    return _constitutiveCurlData->vectorCurl[idex];
  }

  virtual const SVector3 & getConstRefToVectorField(const int idex) const
  {
    if(_constitutiveCurlData==NULL)
      Msg::Error("getConstRefToVectorField() : _constitutiveCurlData not created");
    return _constitutiveCurlData->vectorField[idex];
  }

  virtual SVector3 & getRefToVectorField(const int idex)
  {
    if(_constitutiveCurlData==NULL)
      Msg::Error("getRefToVectorField() : _constitutiveCurlData not created");
    return _constitutiveCurlData->vectorField[idex];
  }

    virtual const SVector3 & getConstRefToInductorSourceVectorField(const int idex) const
    {
        if (_constitutiveCurlData == NULL)
            Msg::Error("getConstRefToInductorSourceVectorField() : _constitutiveCurlData not created");
        return _constitutiveCurlData->inductorSourceVectorField[idex];
    }

    virtual SVector3 & getRefToInductorSourceVectorField(const int idex)
    {
        if (_constitutiveCurlData == NULL)
            Msg::Error("getRefToInductorSourceVectorField() : _constitutiveCurlData not created");
        return _constitutiveCurlData->inductorSourceVectorField[idex];
    }

  virtual const SVector3 & getConstRefToSourceVectorField(const int idex) const
  {
    if (_constitutiveCurlData == NULL)
        Msg::Error("getConstRefToSourceVectorField() : _constitutiveCurlData not created");
    return _constitutiveCurlData->sourceVectorField[idex];
  }

  virtual SVector3 & getRefToSourceVectorField(const int idex)
  {
    if (_constitutiveCurlData == NULL)
        Msg::Error("getRefToSourceVectorField() : _constitutiveCurlData not created");
    return _constitutiveCurlData->sourceVectorField[idex];
  }

  virtual const SVector3 & getConstRefTodSourceVectorFielddt(const int idex) const
  {
    if (_constitutiveCurlData == NULL)
      Msg::Error("getConstRefTodSourceVectorFielddt(): _constitutiveCurlData not created");
    return _constitutiveCurlData->dSourceVectorFielddt[idex];
  }

  virtual SVector3 & getRefTodSourceVectorFielddt(const int idex)
  {
    if (_constitutiveCurlData == NULL)
      Msg::Error("getRefTodSourceVectorFielddt(): _constitutiveCurlData not created");
    return _constitutiveCurlData->dSourceVectorFielddt[idex];
  }

    virtual const std::vector< std::vector< STensor3 > > & getConstRefTodSourceVectorFielddVectorPotential() const
    {
        if (_constitutiveCurlData == NULL)
            Msg::Error("getConstRefTodSourceVectorFielddVectorPotential() : _constitutiveCurlData not created");
        return _constitutiveCurlData->dSourceVectorFielddVectorPotential;
    }

    virtual std::vector< std::vector< STensor3 > > & getRefTodSourceVectorFielddVectorPotential()
    {
        if (_constitutiveCurlData == NULL)
            Msg::Error("getRefTodSourceVectorFielddVectorPotential() : _constitutiveCurlData not created");
        return _constitutiveCurlData->dSourceVectorFielddVectorPotential;
    }

  virtual const std::vector< std::vector< STensor3 > > & getConstRefTodSourceVectorFielddVectorCurl() const
  {
    if (_constitutiveCurlData == NULL)
        Msg::Error("getConstRefTodSourceVectorFielddVectorCurl() : _constitutiveCurlData not created");
    return _constitutiveCurlData->dSourceVectorFielddVectorCurl;
  }

  virtual std::vector< std::vector< STensor3 > > & getRefTodSourceVectorFielddVectorCurl()
  {
    if (_constitutiveCurlData == NULL)
        Msg::Error("getRefTodSourceVectorFielddVectorCurl() : _constitutiveCurlData not created");
    return _constitutiveCurlData->dSourceVectorFielddVectorCurl;
  }

  virtual const SVector3 & getConstRefToMechanicalFieldSource(const int idex) const
  {
    if (_constitutiveCurlData == NULL)
        Msg::Error("getConstRefToMechanicalFieldSource() : _constitutiveCurlData not created");
    return _constitutiveCurlData->mechanicalFieldSource[idex];
  }

  virtual SVector3 & getRefToMechanicalFieldSource(const int idex)
  {
    if (_constitutiveCurlData == NULL)
        Msg::Error("getRefToMechanicalFieldSource() : _constitutiveCurlData not created");
    return _constitutiveCurlData->mechanicalFieldSource[idex];
  }

    virtual const std::vector<std::vector< STensor3 > > & getConstRefTodVectorFielddVectorPotential() const
    {
        if(_constitutiveCurlData==NULL)
            Msg::Error("getConstRefTodVectorFielddVectorPotential() : _constitutiveCurlData not created");
        return _constitutiveCurlData->dVectorFielddVectorPotential;
    }

  virtual const std::vector<std::vector< STensor3 > > & getConstRefTodVectorFielddVectorCurl() const
  {
    if(_constitutiveCurlData==NULL)
      Msg::Error("getConstRefTodVectorFielddVectorCurl() : _constitutiveCurlData not created");
     return _constitutiveCurlData->dVectorFielddVectorCurl;
  }

  virtual const std::vector< STensor33 > & getConstRefTodVectorFielddF() const
  {
    if(_constitutiveCurlData==NULL)
      Msg::Error("getConstRefTodVectorFielddF() : _constitutiveCurlData not created");
    return _constitutiveCurlData->dVectorFielddF;
  }

  virtual const std::vector<std::vector< SVector3 > > & getConstRefTodVectorFielddExtraDofField() const
  {
    if(_constitutiveCurlData==NULL)
      Msg::Error("getConstRefTodVectorFielddExtraDofField() : _constitutiveCurlData not created");
    return _constitutiveCurlData->dVectorFielddExtraDofField;
  }

  virtual const std::vector<std::vector< STensor3 > > & getConstRefTodVectorFielddGradExtraDofField() const
  {
    if(_constitutiveCurlData==NULL)
      Msg::Error("getConstRefTodVectorFielddGradExtraDofField() : _constitutiveCurlData not created");
    return _constitutiveCurlData->dVectorFielddGradExtraDofField;
  }

  virtual const std::vector< STensor33 > & getConstRefTodPdVectorCurl() const
  {
    if(_constitutiveCurlData==NULL)
      Msg::Error("getConstRefTodPdVectorCurl() : _constitutiveCurlData not created");
    return _constitutiveCurlData->dPdVectorCurl;
  }

    virtual const std::vector< STensor33 > & getConstRefTodPdVectorPotential() const
    {
        if(_constitutiveCurlData==NULL)
            Msg::Error("getConstRefTodPdVectorPotential() : _constitutiveCurlData not created");
        return _constitutiveCurlData->dPdVectorPotential;
    }

    virtual std::vector<std::vector< STensor3 > > & getRefTodVectorFielddVectorPotential()
    {
        if(_constitutiveCurlData==NULL)
            Msg::Error("getRefTodVectorFielddVectorPotential() : _constitutiveCurlData not created");
        return _constitutiveCurlData->dVectorFielddVectorPotential;
    }

  virtual std::vector<std::vector< STensor3 > > & getRefTodVectorFielddVectorCurl()
  {
    if(_constitutiveCurlData==NULL)
      Msg::Error("getRefTodVectorFielddVectorCurl() : _constitutiveCurlData not created");
    return _constitutiveCurlData->dVectorFielddVectorCurl;
  }

  virtual std::vector< STensor33 > & getRefTodVectorFielddF()
  {
    if(_constitutiveCurlData==NULL)
      Msg::Error("getRefTodVectorFielddF() : _constitutiveCurlData not created");
    return _constitutiveCurlData->dVectorFielddF;
  }

  virtual std::vector<std::vector< SVector3 > > & getRefTodVectorFielddExtraDofField()
  {
    if(_constitutiveCurlData==NULL)
      Msg::Error("getRefTodVectorFielddExtraDofField() : _constitutiveCurlData not created");
    return _constitutiveCurlData->dVectorFielddExtraDofField;
  }

  virtual std::vector<std::vector< STensor3 > > & getRefTodVectorFielddGradExtraDofField()
  {
    if(_constitutiveCurlData==NULL)
      Msg::Error("getRefTodVectorFielddGradExtraDofField() : _constitutiveCurlData not created");
    return _constitutiveCurlData->dVectorFielddGradExtraDofField;
  }

  virtual std::vector< STensor33 > & getRefTodPdVectorCurl()
  {
    if(_constitutiveCurlData==NULL)
      Msg::Error("getRefTodPdVectorCurl() : _constitutiveCurlData not created");
    return _constitutiveCurlData->dPdVectorCurl;
  }

    virtual std::vector< STensor33 > & getRefTodPdVectorPotential()
    {
        if(_constitutiveCurlData==NULL)
            Msg::Error("getRefTodPdVectorPotential() : _constitutiveCurlData not created");
        return _constitutiveCurlData->dPdVectorPotential;
    }

    virtual const std::vector< std::vector< STensor3 > > & getConstRefTodMechanicalFieldSourcedVectorPotential() const
    {
        if (_constitutiveCurlData == NULL)
            Msg::Error("getConstRefTodMechanicalFieldSourcedVectorPotential() : _constitutiveCurlData not created");
        return _constitutiveCurlData->dMechanicalFieldSourcedVectorPotential;
    }

    virtual std::vector< std::vector< STensor3 > > & getRefTodMechanicalFieldSourcedVectorPotential()
    {
        if (_constitutiveCurlData == NULL)
            Msg::Error("getRefTodMechanicalFieldSourcedVectorPotential() : _constitutiveCurlData not created");
        return _constitutiveCurlData->dMechanicalFieldSourcedVectorPotential;
    }

  virtual const std::vector< std::vector< STensor3 > > & getConstRefTodMechanicalFieldSourcedVectorCurl() const
  {
    if (_constitutiveCurlData == NULL)
        Msg::Error("getConstRefTodMechanicalFieldSourcedVectorCurl() : _constitutiveCurlData not created");
    return _constitutiveCurlData->dMechanicalFieldSourcedVectorCurl;
  }

  virtual std::vector< std::vector< STensor3 > > & getRefTodMechanicalFieldSourcedVectorCurl()
  {
    if (_constitutiveCurlData == NULL)
        Msg::Error("getRefTodMechanicalFieldSourcedVectorCurl() : _constitutiveCurlData not created");
   return _constitutiveCurlData->dMechanicalFieldSourcedVectorCurl;
  }

  // coupling curl data with extra dof
  virtual const std::vector< std::vector< SVector3 > > & getConstRefTodSourceVectorFielddExtraDofField() const
  {
    if (_constitutiveCurlData == NULL)
        Msg::Error("getConstRefTodSourceVectorFielddExtraDofField() : _constitutiveCurlData not created");
    return _constitutiveCurlData->dSourceVectorFielddExtraDofField;
  }

  virtual std::vector< std::vector< SVector3 > > & getRefTodSourceVectorFielddExtraDofField()
  {
    if (_constitutiveCurlData == NULL)
        Msg::Error("getRefTodSourceVectorFielddExtraDofField() : _constitutiveCurlData not created");
    return _constitutiveCurlData->dSourceVectorFielddExtraDofField;
  }

  virtual const std::vector< std::vector< STensor3 > > & getConstRefTodSourceVectorFielddGradExtraDofField() const
  {
    if (_constitutiveCurlData == NULL)
        Msg::Error("getConstRefTodSourceVectorFielddGradExtraDofField() : _constitutiveCurlData not created");
    return _constitutiveCurlData->dSourceVectorFielddGradExtraDofField;
  }

  virtual std::vector< std::vector< STensor3 > > & getRefTodSourceVectorFielddGradExtraDofField()
  {
    if (_constitutiveCurlData == NULL)
        Msg::Error("getRefTodSourceVectorFielddGradExtraDofField() : _constitutiveCurlData not created");
    return _constitutiveCurlData->dSourceVectorFielddGradExtraDofField;
  }

  virtual const std::vector< std::vector< SVector3 > > & getConstRefTodMechanicalFieldSourcedExtraDofField() const
  {
    if (_constitutiveCurlData == NULL)
        Msg::Error("getConstRefTodMechanicalFieldSourcedExtraDofField() : _constitutiveCurlData not created");
    return _constitutiveCurlData->dMechanicalFieldSourcedExtraDofField;
  }

  virtual std::vector< std::vector< SVector3 > > & getRefTodMechanicalFieldSourcedExtraDofField()
  {
    if (_constitutiveCurlData == NULL)
        Msg::Error("getRefTodMechanicalFieldSourcedExtraDofField() : _constitutiveCurlData not created");
    return _constitutiveCurlData->dMechanicalFieldSourcedExtraDofField;
  }

  virtual const std::vector< std::vector< STensor3 > > & getConstRefTodMechanicalFieldSourcedGradExtraDofField() const
  {
    if (_constitutiveCurlData == NULL)
        Msg::Error("getConstRefTodMechanicalFieldSourcedGradExtraDofField() : _constitutiveCurlData not created");
    return _constitutiveCurlData->dMechanicalFieldSourcedGradExtraDofField;
  }

  virtual std::vector< std::vector< STensor3 > > & getRefTodMechanicalFieldSourcedGradExtraDofField()
  {
    if (_constitutiveCurlData == NULL)
        Msg::Error("getRefTodMechanicalFieldSourcedGradExtraDofField() : _constitutiveCurlData not created");
    return _constitutiveCurlData->dMechanicalFieldSourcedGradExtraDofField;
  }

  virtual const std::vector< STensor33 > & getConstRefTodSourceVectorFielddF() const
  {
    if (_constitutiveCurlData == NULL)
        Msg::Error("getConstRefTodSourceVectorFielddF() : _constitutiveCurlData not created");
    return _constitutiveCurlData->dSourceVectorFielddF;
  }

  virtual std::vector< STensor33 > & getRefTodSourceVectorFielddF()
  {
    if (_constitutiveCurlData == NULL)
        Msg::Error("getRefTodSourceVectorFielddF() : _constitutiveCurlData not created");
    return _constitutiveCurlData->dSourceVectorFielddF;
  }

  virtual const std::vector< STensor33 > & getConstRefTodMechanicalFieldSourcedF() const
  {
    if (_constitutiveCurlData == NULL)
        Msg::Error("getConstRefTodMechanicalFieldSourcedF() : _constitutiveCurlData not created");
    return _constitutiveCurlData->dMechanicalFieldSourcedF;
  }

  virtual std::vector< STensor33 > & getRefTodMechanicalFieldSourcedF()
  {
    if (_constitutiveCurlData == NULL)
        Msg::Error("getRefTodMechanicalFieldSourcedF() : _constitutiveCurlData not created");
    return _constitutiveCurlData->dMechanicalFieldSourcedF;
  }


  //Energy Conjugated Fiedl (fT & fv)

  virtual double getConstRefToEnergyConjugatedField(const int idex) const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefToEnergyConjugatedField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->energyConjugatedField(idex);
  }
  virtual double   &getRefToEnergyConjugatedField(const int idex)
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefToEnergyConjugatedField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->energyConjugatedField(idex);
  }
  virtual const std::vector<SVector3>  &getConstRefToGradEnergyConjugatedField() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefToGradEnergyConjugatedField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->gradEnergyConjugatedField;
  }
  virtual std::vector<SVector3>  &getRefToGradEnergyConjugatedField()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefToGradEnergyConjugatedField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->gradEnergyConjugatedField;
  }
  virtual const  std::vector< std::vector<double> >  &getConstRefTodFielddEnergyConjugatedField() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodFielddEnergyConjugatedField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFielddEnergyConjugatedField;
  }
  virtual std::vector< std::vector<double> >  &getRefTodFielddEnergyConjugatedField()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodFielddEnergyConjugatedField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFielddEnergyConjugatedField;
  }
  virtual const std::vector< std::vector<SVector3> >  &getConstRefTodGradFielddEnergyConjugatedField() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodGradFielddEnergyConjugatedField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dGradFielddEnergyConjugatedField;
  }
  virtual std::vector< std::vector<SVector3> >  &getRefTodGradFielddEnergyConjugatedField()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodGradFielddEnergyConjugatedField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dGradFielddEnergyConjugatedField;
  }
  virtual const std::vector< std::vector<STensor3> >  &getConstRefTodGradFielddGradEnergyConjugatedField() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodGradFielddGradEnergyConjugatedField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dGradFielddGradEnergyConjugatedField;
  }
  virtual std::vector< std::vector<STensor3> >  &getRefTodGradFielddGradEnergyConjugatedField()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodGradFielddGradEnergyConjugatedField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dGradFielddGradEnergyConjugatedField;
  }
  virtual const fullVector<double>  &getConstRefToEnergyConjugatedFieldJump() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefToEnergyConjugatedFieldJump() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->energyConjugatedFieldJump;
  }
  virtual fullVector<double>  &getRefToEnergyConjugatedFieldJump()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefToEnergyConjugatedFieldJump() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->energyConjugatedFieldJump;
  }
  virtual const  std::vector< std::vector<double> >  &getConstRefTodEnergyConjugatedFieldJumpdFieldm() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodEnergyConjugatedFieldJumpdFieldm() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dEnergyConjugatedFieldJumpdFieldm;
  }
  virtual std::vector< std::vector<double> >  &getRefTodEnergyConjugatedFieldJumpdFieldm()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodEnergyConjugatedFieldJumpdFieldm() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dEnergyConjugatedFieldJumpdFieldm;
  }
  virtual const  std::vector< std::vector<double> >  &getConstRefTodEnergyConjugatedFieldJumpdFieldp() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodEnergyConjugatedFieldJumpdFieldp() : _constitutiveExtraDofDiffusionData not created");
   return _constitutiveExtraDofDiffusionData->dEnergyConjugatedFieldJumpdFieldp;
  }
  virtual std::vector< std::vector<double> >  &getRefTodEnergyConjugatedFieldJumpdFieldp()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodEnergyConjugatedFieldJumpdFieldp() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dEnergyConjugatedFieldJumpdFieldp;
  }


  virtual IPVariable* clone() const =0;
  virtual void restart();
};

class DeepMaterialNetwork;
class DMNBasedDG3DIPVariable :public dG3DIPVariable
{
  protected:
    DeepMaterialNetwork* _DMN;

  public:
    DMNBasedDG3DIPVariable(DeepMaterialNetwork* d, const bool createBodyForceHO, const bool oninter);
    DMNBasedDG3DIPVariable(const DMNBasedDG3DIPVariable& src);
    virtual DMNBasedDG3DIPVariable& operator =(const IPVariable& src);
    virtual ~DMNBasedDG3DIPVariable();
    virtual double get(const int comp) const;
    virtual void restart();
    DeepMaterialNetwork* getDeepMaterialNetwork();
    const DeepMaterialNetwork* getDeepMaterialNetwork() const;

    virtual bool withDeepMN() const {return true;};
    virtual void getAllDeepMNs(std::vector<DeepMaterialNetwork*>& allDMN)
    {
      allDMN.resize(1);
      allDMN[0] = _DMN;
    };

    virtual IPVariable* getInternalData() {return NULL;};
    virtual const IPVariable* getInternalData()  const {return NULL;};
    virtual IPVariable* clone() const {return new DMNBasedDG3DIPVariable(*this);};
};

class dG3DIPVariableMultiple : public dG3DIPVariable
{
  protected:
    int _numIP;
    std::vector<IPVariable*> _IPVector;
    std::vector<double> _IPWeights;

  public:
    dG3DIPVariableMultiple(int numIPs, const bool createBodyForceHO, const bool oninter);
    dG3DIPVariableMultiple(const dG3DIPVariableMultiple& src);
    virtual dG3DIPVariableMultiple& operator =(const IPVariable& src);
    virtual ~dG3DIPVariableMultiple();

    virtual double defoEnergy() const;
    virtual double plasticEnergy() const;
    virtual double damageEnergy() const;
    virtual void setLocation(const IPVariable::LOCATION loc)
    {
      dG3DIPVariable::setLocation(loc);
      for (int j=0; j< _numIP; j++)
      {
        _IPVector[j]->setLocation(loc);
      };
    };

    int getNumIPs() const {return _numIP;};
    void addIPv(int loc, IPVariable* ipv, double w);
    IPVariable* getIPv(int loc) {return _IPVector[loc];};
    const IPVariable* getIPv(int loc) const {return _IPVector[loc];};
    double getWeight(int loc) const {return _IPWeights[loc];};

    virtual double get(const int comp) const;
    virtual void restart();
    virtual bool withDeepMN() const;
    virtual void getAllDeepMNs(std::vector<DeepMaterialNetwork*>& allDMN);
    virtual IPVariable* getInternalData() {return NULL;};
    virtual const IPVariable* getInternalData()  const {return NULL;};
    virtual IPVariable* clone() const {return new dG3DIPVariableMultiple(*this);};
};

class ANNBasedDG3DIPVariable :public dG3DIPVariable
{
  protected:
    // use matrix with 1 row
    fullMatrix<double> _internalVars; // internal variable
    fullMatrix<double> _kinematicVariables; // kinematic variable
    fullMatrix<double> _stressVars; // stress variables

  public:
    ANNBasedDG3DIPVariable(const int n, const bool createBodyForceHO, const bool oninter);
    ANNBasedDG3DIPVariable(const ANNBasedDG3DIPVariable &source);
    virtual ANNBasedDG3DIPVariable& operator =(const IPVariable& src);
    virtual ~ANNBasedDG3DIPVariable();

    virtual const fullMatrix<double>& getConstRefToInternalVariables() const {return _internalVars;};
    virtual fullMatrix<double>& getRefToInternalVariables() {return _internalVars;};

    virtual const fullMatrix<double>& getConstRefToKinematicVariables() const {return _kinematicVariables;};
    virtual fullMatrix<double>& getRefToKinematicVariables() {return _kinematicVariables;};

    virtual const fullMatrix<double>& getConstRefToStressVariables() const {return _stressVars;};
    virtual fullMatrix<double>& getRefToStressVariables() {return _stressVars;};

    virtual IPVariable* getInternalData() {return NULL;};
    virtual const IPVariable* getInternalData()  const {return NULL;};
    virtual IPVariable* clone() const {return new ANNBasedDG3DIPVariable(*this);};
    virtual double get(const int comp) const;
    virtual void restart();
};


class StochDMNDG3DIPVariable : public dG3DIPVariable
{
  protected:
    int _NRoot, _NInterface;
    std::vector<IPVariable*> _IPVector;
    fullVector<double> Fvec;
    fullVector<double> Pvec;
    fullVector<double> a;

 public:
    StochDMNDG3DIPVariable(const int NRoot,const int NInterface, const bool createBodyForceHO, const bool oninter);
    StochDMNDG3DIPVariable(const StochDMNDG3DIPVariable& src);
    virtual StochDMNDG3DIPVariable& operator =(const IPVariable& src);
    virtual ~StochDMNDG3DIPVariable();

    void InitializeFluctuationVector() {a.setAll(0.0);};
    virtual double defoEnergy() const;
    virtual double plasticEnergy() const;
    virtual double damageEnergy() const;

    void addIPv(int i, IPVariable* ipv);
    IPVariable* getIPv(int i) {return _IPVector[i];};
    const IPVariable* getIPv(int i) const {return _IPVector[i];};

    virtual double get(const int comp) const;
    virtual fullVector<double>& getRefToDeformationGradientVect() {return Fvec;};
    virtual const fullVector<double>&  getConstRefToDeformationGradientVect() const {return Fvec;};
    virtual fullVector<double>& getRefToFirstPiolaKirchhoffStressVect() {return Pvec;};
    virtual const fullVector<double>& getConstRefToFirstPiolaKirchhoffStressVect() const {return Pvec;};
    virtual fullVector<double>& getRefToInterfaceFluctuationVect() {return a;};
    virtual const fullVector<double>& getConstRefToInterfaceFluctuationVect() const {return a;};



    virtual IPVariable* getInternalData() {return NULL;};
    virtual const IPVariable* getInternalData()  const {return NULL;};
    virtual IPVariable* clone() const {return new StochDMNDG3DIPVariable(*this);};
    virtual void restart();
};


class torchANNBasedDG3DIPVariable :public dG3DIPVariable
{
  protected:
    // use torch tensor with 1 row
    int _n;
    double _initial_h;
#if defined(HAVE_TORCH)
    torch::Tensor _internalVars; // internal variable
#else
    double _internalVars;
#endif
    fullMatrix<double> _kinematicVariables; // kinematic variable
    fullMatrix<double> restart_internalVars; // internal variable
    fullMatrix<double> _extraVariables; // variables other than kinematic and hidden needed at GP @Mohib

  public:
    //torchANNBasedDG3DIPVariable(const int n, const double initial_h, const bool createBodyForceHO,  const bool oninter); // Retundend init signature. Kept for legacy @Mohib
    // Overloading for handling additional params at GP @Mohib
    torchANNBasedDG3DIPVariable(const int n, const double initial_h, const bool createBodyForceHO,  const bool oninter, const int extraInput=0, const std::vector<double> initialValue_Extra={});
    torchANNBasedDG3DIPVariable(const torchANNBasedDG3DIPVariable &source);
    virtual torchANNBasedDG3DIPVariable& operator =(const IPVariable& src);
    virtual ~torchANNBasedDG3DIPVariable();

#if defined(HAVE_TORCH)
    virtual const torch::Tensor& getConstRefToInternalVariables() const {return _internalVars;};
    virtual torch::Tensor& getRefToInternalVariables() {return _internalVars;};
#endif

    virtual const fullMatrix<double>& getConstRefToKinematicVariables() const {return _kinematicVariables;};
    virtual fullMatrix<double>& getRefToKinematicVariables() {return _kinematicVariables;};

    // getter for extra params at GP @Mohib
    virtual const fullMatrix<double>& getConstRefToExtraVariables() const {return _extraVariables;};
    virtual fullMatrix<double>& getRefToExtraVariables() {return _extraVariables;};

    virtual IPVariable* getInternalData() {return NULL;};
    virtual const IPVariable* getInternalData()  const {return NULL;};
    virtual IPVariable* clone() const {return new torchANNBasedDG3DIPVariable(*this);};
    virtual double get(const int comp) const;
    virtual void restart();

};

class LinearElasticDG3DIPVariable :public dG3DIPVariable{
  protected:

  public:
    LinearElasticDG3DIPVariable(const bool createBodyForceHO, const bool oninter):dG3DIPVariable(createBodyForceHO,oninter){}; // A empty constructor is mandatory to build FractureCohesive3DIPVariable OK like this as the fracture is on interface??
    LinearElasticDG3DIPVariable(const LinearElasticDG3DIPVariable &source):dG3DIPVariable(source){};
    virtual LinearElasticDG3DIPVariable &operator = (const IPVariable &_source){
      dG3DIPVariable::operator =(_source);
			return *this;
    };
    virtual ~LinearElasticDG3DIPVariable()
    {

    }

    virtual IPVariable* getInternalData() {return NULL;};
    virtual const IPVariable* getInternalData()  const {return NULL;};

    virtual void getCauchyStress(STensor3 &cauchy) const{
      cauchy= this->getConstRefToFirstPiolaKirchhoffStress();
    };
    virtual IPVariable* clone() const {return new LinearElasticDG3DIPVariable(*this);};
};

class HyperElasticDG3DIPVariable :public dG3DIPVariable{
  protected:
    IPHyperelasticPotential* _ipv;
  public:
    HyperElasticDG3DIPVariable(const bool createBodyForceHO, const bool oninter):dG3DIPVariable(createBodyForceHO,oninter){
        _ipv= new IPHyperelasticPotential();
    }; // A empty constructor is mandatory to build FractureCohesive3DIPVariable OK like this as the fracture is on interface??
    HyperElasticDG3DIPVariable(const HyperElasticDG3DIPVariable &source):dG3DIPVariable(source)
    {
      _ipv=NULL;
      if (source._ipv != NULL)
      {
        _ipv = dynamic_cast<IPHyperelasticPotential*>(source._ipv->clone());
      }
    };
    virtual HyperElasticDG3DIPVariable &operator = (const IPVariable &_source){
      dG3DIPVariable::operator =(_source);
      const HyperElasticDG3DIPVariable* psrc = dynamic_cast<const HyperElasticDG3DIPVariable*>(&_source);
      if (psrc != NULL)
      {
        if (psrc->_ipv != NULL)
        {
          _ipv = dynamic_cast<IPHyperelasticPotential*>(psrc->_ipv->clone());
        }
      }
			return *this;
    };
    virtual ~HyperElasticDG3DIPVariable()
    {
      if (_ipv != NULL)
      {
        delete _ipv; _ipv=NULL;
      }
    }
    const IPHyperelasticPotential* getIPHyperelasticPotential() const {return _ipv;};
    IPHyperelasticPotential* getIPHyperelasticPotential() {return _ipv;};
    virtual IPVariable* getInternalData() {return _ipv;};
    virtual const IPVariable* getInternalData()  const {return _ipv;};
    virtual double defoEnergy() const {return _ipv->defoEnergy();};
    virtual double& getRefToDefoEnergy() {return _ipv->getRefToDeforEnergy();};
    virtual void restart()
    {
      dG3DIPVariable::restart();
      _ipv->restart();
    }
    virtual IPVariable* clone() const {return new HyperElasticDG3DIPVariable(*this);};
};


class dG3DIPVariableVoid :public dG3DIPVariable{
  protected:

  public:
    dG3DIPVariableVoid(const bool createBodyForceHO, const bool oninter):dG3DIPVariable(createBodyForceHO,oninter){}; // A empty constructor is mandatory to build FractureCohesive3DIPVariable OK like this as the fracture is on interface??
    dG3DIPVariableVoid(const dG3DIPVariableVoid &source):dG3DIPVariable(source){};
    virtual dG3DIPVariableVoid &operator = (const IPVariable &_source){
      dG3DIPVariable::operator =(_source);
			return *this;
    };
    virtual ~dG3DIPVariableVoid()
    {

    }
    virtual IPVariable* getInternalData() {return NULL;};
    virtual const IPVariable* getInternalData()  const {return NULL;};

    virtual void getCauchyStress(STensor3 &cauchy) const{
      STensorOperation::zero(cauchy);
    };
    virtual IPVariable* clone() const {return new dG3DIPVariableVoid(*this);};
};

class J2SmallStrainDG3DIPVariable : public dG3DIPVariable // or store data in a different way
{
 protected:
  IPJ2linear *_j2ipv;
 public:
  J2SmallStrainDG3DIPVariable(const mlawJ2VMSmallStrain &_j2law, const bool createBodyForceHO, const bool oninter);
  J2SmallStrainDG3DIPVariable(const J2SmallStrainDG3DIPVariable &source);
  virtual J2SmallStrainDG3DIPVariable& operator=(const IPVariable &source);
  virtual ~J2SmallStrainDG3DIPVariable(){if(_j2ipv!=NULL) {delete _j2ipv; _j2ipv=NULL;}};

  virtual void setLocation(const IPVariable::LOCATION loc) {
    dG3DIPVariable::setLocation(loc);
    if (_j2ipv)
      _j2ipv->setLocation(loc);
  };

  virtual IPVariable* getInternalData() {return _j2ipv;};
  virtual const IPVariable* getInternalData()  const {return _j2ipv;};

 /* specific function */
  IPJ2linear* getIPJ2linear(){return _j2ipv;}
  const IPJ2linear* getIPJ2linear() const{return _j2ipv;}
  virtual double get(const int i) const;
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const;
  virtual double damageEnergy() const {return _j2ipv->damageEnergy();};

  virtual bool dissipationIsActive() const {return _j2ipv->dissipationIsActive();};
  virtual void blockDissipation(const bool fl){   _j2ipv->blockDissipation(fl);}
  virtual bool dissipationIsBlocked() const { return _j2ipv->dissipationIsBlocked();}

  // for path following based on  irreversibe energt
  virtual double irreversibleEnergy() const {return _j2ipv->irreversibleEnergy();};
  virtual double& getRefToIrreversibleEnergy() {return _j2ipv->getRefToIrreversibleEnergy();};

  virtual const STensor3& getConstRefToDIrreversibleEnergyDDeformationGradient() const {return _j2ipv->getConstRefToDIrreversibleEnergyDF();};
  virtual STensor3& getRefToDIrreversibleEnergyDDeformationGradient() {return _j2ipv->getRefToDIrreversibleEnergyDF();};

  virtual IPVariable* clone() const {return new J2SmallStrainDG3DIPVariable(*this);};
  virtual void restart();
};

class AnisotropicPlasticityDG3DIPVariable : public dG3DIPVariable // or store data in a different way
{
 protected:
  IPAnisotropicPlasticity *_ipv;
 public:
  AnisotropicPlasticityDG3DIPVariable(const mlawAnisotropicPlasticity &_anisoLaw, const bool createBodyForceHO, const bool oninter);
  AnisotropicPlasticityDG3DIPVariable(const AnisotropicPlasticityDG3DIPVariable &source);
  virtual AnisotropicPlasticityDG3DIPVariable& operator=(const IPVariable &source);
  virtual ~AnisotropicPlasticityDG3DIPVariable(){if(_ipv!=NULL) {delete _ipv; _ipv=NULL;}};

  virtual void setLocation(const IPVariable::LOCATION loc) {
    dG3DIPVariable::setLocation(loc);
    if (_ipv)
      _ipv->setLocation(loc);
  };

  virtual IPVariable* getInternalData() {return _ipv;};
  virtual const IPVariable* getInternalData()  const {return _ipv;};

 /* specific function */
  IPAnisotropicPlasticity* getIPAnisotropicPlasticity(){return _ipv;}
  const IPAnisotropicPlasticity* getIPAnisotropicPlasticity() const{return _ipv;}
  virtual double get(const int i) const;
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const;
  virtual double damageEnergy() const {return _ipv->damageEnergy();};

  virtual bool dissipationIsActive() const {return _ipv->dissipationIsActive();};
  virtual void blockDissipation(const bool fl){   _ipv->blockDissipation(fl);}
  virtual bool dissipationIsBlocked() const { return _ipv->dissipationIsBlocked();}

  // for path following based on  irreversibe energt
  virtual double irreversibleEnergy() const {return _ipv->irreversibleEnergy();};
  virtual double& getRefToIrreversibleEnergy() {return _ipv->getRefToIrreversibleEnergy();};

  virtual const STensor3& getConstRefToDIrreversibleEnergyDDeformationGradient() const {return _ipv->getConstRefToDIrreversibleEnergyDF();};
  virtual STensor3& getRefToDIrreversibleEnergyDDeformationGradient() {return _ipv->getRefToDIrreversibleEnergyDF();};

  virtual IPVariable* clone() const {return new AnisotropicPlasticityDG3DIPVariable(*this);};
  virtual void restart();
};

class J2LinearDG3DIPVariable : public dG3DIPVariable // or store data in a different way
{
 protected:
  IPJ2linear *_j2ipv;
 public:
  J2LinearDG3DIPVariable(const mlawJ2linear &_j2law, const bool createBodyForceHO, const bool oninter);
  J2LinearDG3DIPVariable(const J2LinearDG3DIPVariable &source);
  virtual J2LinearDG3DIPVariable& operator=(const IPVariable &source);
  virtual ~J2LinearDG3DIPVariable(){if(_j2ipv!=NULL) {delete _j2ipv; _j2ipv=NULL;}};

  virtual void setLocation(const IPVariable::LOCATION loc) {
    dG3DIPVariable::setLocation(loc);
    if (_j2ipv)
      _j2ipv->setLocation(loc);
  };

  virtual IPVariable* getInternalData() {return _j2ipv;};
  virtual const IPVariable* getInternalData()  const {return _j2ipv;};

 /* specific function */
  IPJ2linear* getIPJ2linear(){return _j2ipv;}
  const IPJ2linear* getIPJ2linear() const{return _j2ipv;}
  virtual double get(const int i) const;
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const;
  virtual double damageEnergy() const {return _j2ipv->damageEnergy();};

  virtual bool dissipationIsActive() const {return _j2ipv->dissipationIsActive();};
  virtual void blockDissipation(const bool fl){   _j2ipv->blockDissipation(fl);}
  virtual bool dissipationIsBlocked() const { return _j2ipv->dissipationIsBlocked();}

  // for path following based on  irreversibe energt
  virtual double irreversibleEnergy() const {return _j2ipv->irreversibleEnergy();};
  virtual double& getRefToIrreversibleEnergy() {return _j2ipv->getRefToIrreversibleEnergy();};

  virtual const STensor3& getConstRefToDIrreversibleEnergyDDeformationGradient() const {return _j2ipv->getConstRefToDIrreversibleEnergyDF();};
  virtual STensor3& getRefToDIrreversibleEnergyDDeformationGradient() {return _j2ipv->getRefToDIrreversibleEnergyDF();};

  virtual IPVariable* clone() const {return new J2LinearDG3DIPVariable(*this);};
  virtual void restart();
};

class HyperViscoElasticdG3DIPVariable : public dG3DIPVariable{
  protected:
    IPHyperViscoElastic * _ipViscoElastic;

public:
    HyperViscoElasticdG3DIPVariable(const mlawHyperViscoElastic& viscoLaw, const bool createBodyForceHO, const bool oninter);
    HyperViscoElasticdG3DIPVariable(const HyperViscoElasticdG3DIPVariable& src);
    virtual HyperViscoElasticdG3DIPVariable& operator = (const IPVariable& src);
    virtual ~HyperViscoElasticdG3DIPVariable();

    virtual IPVariable* getInternalData() {return _ipViscoElastic;};
    virtual const IPVariable* getInternalData()  const {return _ipViscoElastic;};

    IPHyperViscoElastic* getIPHyperViscoElastic() {return _ipViscoElastic;};
    const IPHyperViscoElastic* getIPHyperViscoElastic() const {return _ipViscoElastic;};

    virtual void setLocation(const IPVariable::LOCATION loc) {
      dG3DIPVariable::setLocation(loc);
      if (_ipViscoElastic)
        _ipViscoElastic->setLocation(loc);
    };

    virtual double get(const int i) const;

    // for path following based on  irreversibe energt
    virtual double irreversibleEnergy() const {return _ipViscoElastic->irreversibleEnergy();};
    virtual double& getRefToIrreversibleEnergy() {return _ipViscoElastic->getRefToIrreversibleEnergy();};

    virtual const STensor3& getConstRefToDIrreversibleEnergyDDeformationGradient() const {return _ipViscoElastic->getConstRefToDIrreversibleEnergyDF();};
    virtual STensor3& getRefToDIrreversibleEnergyDDeformationGradient() {return _ipViscoElastic->getRefToDIrreversibleEnergyDF();};

    virtual double defoEnergy() const;

    virtual IPVariable* clone() const {return new HyperViscoElasticdG3DIPVariable(*this);};
    virtual void restart();
};

class HyperViscoElastoPlasticdG3DIPVariable : public dG3DIPVariable{
  protected:
    IPHyperViscoElastoPlastic* _ipViscoElastoPlastic;

  public:
    HyperViscoElastoPlasticdG3DIPVariable(const mlawPowerYieldHyper& viscoEPLaw, const bool createBodyForceHO, const bool oninter);
    HyperViscoElastoPlasticdG3DIPVariable(const HyperViscoElastoPlasticdG3DIPVariable& src);
    virtual HyperViscoElastoPlasticdG3DIPVariable& operator = (const IPVariable& src);
    virtual ~HyperViscoElastoPlasticdG3DIPVariable();

    virtual IPVariable* getInternalData() {return _ipViscoElastoPlastic;};
    virtual const IPVariable* getInternalData()  const {return _ipViscoElastoPlastic;};

    IPHyperViscoElastoPlastic* getIPHyperViscoElastoPlastic() {return _ipViscoElastoPlastic;}
    const IPHyperViscoElastoPlastic* getIPHyperViscoElastoPlastic() const {return _ipViscoElastoPlastic;}

    virtual void setLocation(const IPVariable::LOCATION loc) {
      dG3DIPVariable::setLocation(loc);
      if (_ipViscoElastoPlastic)
        _ipViscoElastoPlastic->setLocation(loc);
    };

    virtual double get(const int i) const;

    virtual bool dissipationIsActive() const {return _ipViscoElastoPlastic->dissipationIsActive();};
    virtual void blockDissipation(const bool fl){   _ipViscoElastoPlastic->blockDissipation(fl);}
    virtual bool dissipationIsBlocked() const { return _ipViscoElastoPlastic->dissipationIsBlocked();}

    // for path following based on  irreversibe energt
    virtual double irreversibleEnergy() const {return _ipViscoElastoPlastic->irreversibleEnergy();};
    virtual double& getRefToIrreversibleEnergy() {return _ipViscoElastoPlastic->getRefToIrreversibleEnergy();};

    virtual const STensor3& getConstRefToDIrreversibleEnergyDDeformationGradient() const {return _ipViscoElastoPlastic->getConstRefToDIrreversibleEnergyDF();};
    virtual STensor3& getRefToDIrreversibleEnergyDDeformationGradient() {return _ipViscoElastoPlastic->getRefToDIrreversibleEnergyDF();};

    virtual double defoEnergy() const {return _ipViscoElastoPlastic->defoEnergy();};
    virtual double plasticEnergy() const {return _ipViscoElastoPlastic->plasticEnergy();};
    virtual double damageEnergy() const {return _ipViscoElastoPlastic->damageEnergy();};

    virtual IPVariable* clone() const {return new HyperViscoElastoPlasticdG3DIPVariable(*this);};
    virtual void restart();
};

class localDamageJ2HyperDG3DIPVariable : public dG3DIPVariable{

 protected:
  IPLocalDamageJ2Hyper *_nldJ2Hyperipv;

 public:
  localDamageJ2HyperDG3DIPVariable(const mlawLocalDamageJ2Hyper &_j2law, const bool createBodyForceHO,const bool oninter);
  localDamageJ2HyperDG3DIPVariable(const mlawLocalDamageJ2SmallStrain &_j2law, const bool createBodyForceHO, const bool oninter);
  localDamageJ2HyperDG3DIPVariable(const localDamageJ2HyperDG3DIPVariable &source);
  virtual localDamageJ2HyperDG3DIPVariable& operator=(const IPVariable &source);

  virtual IPVariable* getInternalData() {return _nldJ2Hyperipv;};
  virtual const IPVariable* getInternalData()  const {return _nldJ2Hyperipv;};

 /* specific function */
  IPLocalDamageJ2Hyper* getIPLocalDamageJ2Hyper(){return _nldJ2Hyperipv;}
  const IPLocalDamageJ2Hyper* getIPLocalDamageJ2Hyper() const{return _nldJ2Hyperipv;}
  virtual ~localDamageJ2HyperDG3DIPVariable()
  {
    if (_nldJ2Hyperipv) delete _nldJ2Hyperipv;
    _nldJ2Hyperipv = NULL;
  }

  virtual void setLocation(const IPVariable::LOCATION loc) {
    dG3DIPVariable::setLocation(loc);
    if (_nldJ2Hyperipv)
      _nldJ2Hyperipv->setLocation(loc);
  };

  virtual bool dissipationIsActive() const {return _nldJ2Hyperipv->dissipationIsActive();};
  virtual void blockDissipation(const bool fl){   _nldJ2Hyperipv->blockDissipation(fl);}
  virtual bool dissipationIsBlocked() const { return _nldJ2Hyperipv->dissipationIsBlocked();}

  virtual double get(const int i) const;
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const;
  virtual double damageEnergy() const {return _nldJ2Hyperipv->damageEnergy();};

  // for path following based on  irreversibe energt
  virtual double irreversibleEnergy() const {return _nldJ2Hyperipv->irreversibleEnergy();};
  virtual double& getRefToIrreversibleEnergy() {return _nldJ2Hyperipv->getRefToIrreversibleEnergy();};

  virtual const STensor3& getConstRefToDIrreversibleEnergyDDeformationGradient() const {return _nldJ2Hyperipv->getConstRefToDIrreversibleEnergyDF();};
  virtual STensor3& getRefToDIrreversibleEnergyDDeformationGradient() {return _nldJ2Hyperipv->getRefToDIrreversibleEnergyDF();};

  virtual IPVariable* clone() const {return new localDamageJ2HyperDG3DIPVariable(*this);};
  virtual void restart();
};

class localDamageIsotropicElasticityDG3DIPVariable : public dG3DIPVariable
{

 protected:
  IPLocalDamageIsotropicElasticity *_localIPV;

 public:
  localDamageIsotropicElasticityDG3DIPVariable(const mlawLocalDamageHyperelastic & elasticLaw, const bool createBodyForceHO,const bool oninter);
  localDamageIsotropicElasticityDG3DIPVariable(const mlawLocalDamageIsotropicElasticity & elasticLaw, const bool createBodyForceHO, const bool oninter);
  localDamageIsotropicElasticityDG3DIPVariable(const localDamageIsotropicElasticityDG3DIPVariable &source);
  virtual localDamageIsotropicElasticityDG3DIPVariable& operator=(const IPVariable &source);

  virtual IPVariable* getInternalData() {return _localIPV;};
  virtual const IPVariable* getInternalData()  const {return _localIPV;};

 /* specific function */
  IPLocalDamageIsotropicElasticity* getIPLocalDamageIsotropicElasticity(){return _localIPV;}
  const IPLocalDamageIsotropicElasticity* getIPLocalDamageIsotropicElasticity() const{return _localIPV;}
  virtual ~localDamageIsotropicElasticityDG3DIPVariable()
  {
    if (_localIPV){
     delete _localIPV;
     _localIPV = NULL;
    }
  }

  virtual void setLocation(const IPVariable::LOCATION loc) {
    dG3DIPVariable::setLocation(loc);
    if (_localIPV)
      _localIPV->setLocation(loc);
  };

  virtual bool dissipationIsActive() const {return _localIPV->dissipationIsActive();};

  virtual void blockDissipation(const bool fl){
    if (_localIPV) _localIPV->blockDissipation(fl);
  }
  virtual bool dissipationIsBlocked() const {
    if (_localIPV) return _localIPV->dissipationIsBlocked();
    else return false;
  }

  virtual double get(const int i) const;
  virtual double defoEnergy() const {return _localIPV->defoEnergy();};
  virtual double plasticEnergy() const{return _localIPV->plasticEnergy();};
  virtual double damageEnergy() const {return _localIPV->damageEnergy();};

  // for path following based on irreversibe energt
  virtual double irreversibleEnergy() const {return _localIPV->irreversibleEnergy();};
  virtual double& getRefToIrreversibleEnergy() {return _localIPV->getRefToIrreversibleEnergy();};

  virtual const STensor3& getConstRefToDIrreversibleEnergyDDeformationGradient() const
    {return _localIPV->getConstRefToDIrreversibleEnergyDF();};
  virtual STensor3& getRefToDIrreversibleEnergyDDeformationGradient()
    {return _localIPV->getRefToDIrreversibleEnergyDF();};

  virtual IPVariable* clone() const {return new localDamageIsotropicElasticityDG3DIPVariable(*this);};
  virtual void restart();
};


class VEVPMFHDG3DIPVariable : public dG3DIPVariable
{
 protected:
  IPVEVPMFH* _VEVPipv;
 public:
  VEVPMFHDG3DIPVariable(const bool createBodyForceHO, const bool oninter);
  VEVPMFHDG3DIPVariable(const VEVPMFHDG3DIPVariable &source);
  virtual VEVPMFHDG3DIPVariable& operator=(const IPVariable &source);
  virtual ~VEVPMFHDG3DIPVariable(){if(_VEVPipv!=NULL) { delete _VEVPipv; _VEVPipv=NULL;}};

  virtual void setLocation(const IPVariable::LOCATION loc) {
    dG3DIPVariable::setLocation(loc);
    if (_VEVPipv)
       _VEVPipv->setLocation(loc);
  };

  virtual IPVariable* getInternalData() {return _VEVPipv;};
  virtual const IPVariable* getInternalData()  const {return _VEVPipv;};

 /* specific function */
  IPVEVPMFH* getIPVEVPMFH(){return _VEVPipv;}
  const IPVEVPMFH* getIPVEVPMFH() const{return _VEVPipv;}
  virtual double get(const int i) const;
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const;
  virtual IPVariable* clone() const {return new VEVPMFHDG3DIPVariable(*this);};
  virtual void restart();
};



class ViscoelasticDG3DIPVariable : public dG3DIPVariable
{
 protected:
  IPViscoelastic *_Visipv;
 public:
  ViscoelasticDG3DIPVariable(const mlawViscoelastic &_Vislaw, const bool createBodyForceHO, const bool oninter);
  ViscoelasticDG3DIPVariable(const ViscoelasticDG3DIPVariable &source);
  virtual ViscoelasticDG3DIPVariable& operator=(const IPVariable &source);
  virtual ~ViscoelasticDG3DIPVariable(){if(_Visipv!=NULL) { delete _Visipv; _Visipv=NULL;}};

  virtual void setLocation(const IPVariable::LOCATION loc) {
    dG3DIPVariable::setLocation(loc);
    if (_Visipv)
      _Visipv->setLocation(loc);
  };

  virtual IPVariable* getInternalData() {return _Visipv;};
  virtual const IPVariable* getInternalData()  const {return _Visipv;};

 /* specific function */
  IPViscoelastic* getIPViscoelastic(){return _Visipv;} //output is an instance of IPViscoelastic
  const IPViscoelastic* getIPViscoelastic() const{return _Visipv;}
  virtual double get(const int i) const;
  virtual IPVariable* clone() const {return new ViscoelasticDG3DIPVariable(*this);};
  virtual void restart();
};

class EOSDG3DIPVariable : public dG3DIPVariable
{
 protected:
  IPEOS *_eosipv;
  STensor3 _PK1_vis; //viscous PK1 for AV case

 public:

  EOSDG3DIPVariable(double elementSize, const bool createBodyForceHO, const bool oninter);

  EOSDG3DIPVariable(double elementSize,IPVariable *IPv, const bool createBodyForceHO, const bool oninter);

  EOSDG3DIPVariable(const EOSDG3DIPVariable &source);
  virtual EOSDG3DIPVariable& operator=(const IPVariable &source);
  virtual ~EOSDG3DIPVariable(){if(_eosipv!=NULL) {delete _eosipv; _eosipv=NULL;}};

  virtual void setLocation(const IPVariable::LOCATION loc) {
    dG3DIPVariable::setLocation(loc);
    if (_eosipv)
      _eosipv->setLocation(loc);
  };

  virtual IPVariable* getInternalData() {return _eosipv;};
  virtual const IPVariable* getInternalData()  const {return _eosipv;};

 /* specific function */
  IPEOS* getIPEOS(){return _eosipv;}
  const IPEOS* getIPEOS() const{return _eosipv;}
  virtual double get(const int i) const;
  virtual void getCauchyStress(STensor3 &cauchy) const;//new version of Cauchy stress which substracts the AV part
  virtual STensor3 &getFirstPiolaViscous() {return _PK1_vis;}
  virtual IPVariable* clone() const {return new EOSDG3DIPVariable(*this);};
  virtual void restart();
};


class VUMATinterfaceDG3DIPVariable : public dG3DIPVariable
{
 protected:
  IPVUMATinterface *_vumatipv;
 public:
  VUMATinterfaceDG3DIPVariable(int _nsdv, double _size, const bool createBodyForceHO, const bool oninter);
  VUMATinterfaceDG3DIPVariable(const VUMATinterfaceDG3DIPVariable &source);
  virtual VUMATinterfaceDG3DIPVariable& operator=(const IPVariable &source);

  virtual void setLocation(const IPVariable::LOCATION loc) {
    dG3DIPVariable::setLocation(loc);
    if (_vumatipv)
      _vumatipv->setLocation(loc);
  };

  virtual IPVariable* getInternalData() {return _vumatipv;};
  virtual const IPVariable* getInternalData()  const {return _vumatipv;};

 /* specific function */
  IPVUMATinterface* getIPVUMATinterface(){return _vumatipv;}
  const IPVUMATinterface* getIPVUMATinterface() const{return _vumatipv;}
  virtual double get(const int i) const;
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const;
  virtual double damageEnergy() const {return _vumatipv->damageEnergy();}
  virtual ~VUMATinterfaceDG3DIPVariable()
  {
     delete _vumatipv;
     _vumatipv= NULL;
  }
  virtual IPVariable* clone() const {return new VUMATinterfaceDG3DIPVariable(*this);};
  virtual void restart();
};

class TransverseIsotropicDG3DIPVariable : public dG3DIPVariable // or store data in a different way
{
 protected:
  IPTransverseIsotropic _tiipv;
 public:
  TransverseIsotropicDG3DIPVariable(const bool createBodyForceHO, const bool oninter);
  TransverseIsotropicDG3DIPVariable(const TransverseIsotropicDG3DIPVariable &source);
  virtual TransverseIsotropicDG3DIPVariable& operator=(const IPVariable &source);
  virtual ~TransverseIsotropicDG3DIPVariable(){};
  virtual IPVariable* getInternalData() {return &_tiipv;};
  virtual const IPVariable* getInternalData()  const {return &_tiipv;};

  virtual void setLocation(const IPVariable::LOCATION loc) {
    dG3DIPVariable::setLocation(loc);
    _tiipv.setLocation(loc);
  };

 /* specific function */
  IPTransverseIsotropic* getIPTransverseIsotropic(){return &_tiipv;}
  const IPTransverseIsotropic* getIPTransverseIsotropic() const{return &_tiipv;}
  virtual double get(const int i) const;
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const;
  virtual double damageEnergy() const {return _tiipv.damageEnergy();}
  virtual IPVariable* clone() const {return new TransverseIsotropicDG3DIPVariable(*this);};
  virtual void restart();
};

class TransverseIsoUserDirectionDG3DIPVariable : public dG3DIPVariable // or store data in a different way
{
  protected:
    IPTransverseIsoUserDirection _tiipv;
  public:
    TransverseIsoUserDirectionDG3DIPVariable(const SVector3 &dir, const bool createBodyForceHO, const bool oninter);
    TransverseIsoUserDirectionDG3DIPVariable(const TransverseIsoUserDirectionDG3DIPVariable &source);
    virtual TransverseIsoUserDirectionDG3DIPVariable& operator=(const IPVariable &source);

    virtual void setLocation(const IPVariable::LOCATION loc) {
      dG3DIPVariable::setLocation(loc);
      _tiipv.setLocation(loc);
    };

    virtual IPVariable* getInternalData() {return &_tiipv;};
    virtual const IPVariable* getInternalData()  const {return &_tiipv;};

   /* specific function */
    IPTransverseIsoUserDirection* getIPTransverseIsoUserDirection(){return &_tiipv;}
    const IPTransverseIsoUserDirection* getIPTransverseIsoUserDirection() const{return &_tiipv;}
    virtual ~TransverseIsoUserDirectionDG3DIPVariable(){}
    virtual double get(const int i) const;
    virtual double defoEnergy() const;
    virtual double plasticEnergy() const;
    virtual double damageEnergy() const {return _tiipv.damageEnergy();};
    virtual IPVariable* clone() const {return new TransverseIsoUserDirectionDG3DIPVariable(*this);};
    virtual void restart();
};


class TransverseIsoCurvatureDG3DIPVariable : public dG3DIPVariable // or store data in a different way
{
 protected:
  IPTransverseIsoCurvature _tiipv;
 public:
  TransverseIsoCurvatureDG3DIPVariable(const SVector3 &GaussP, const SVector3 &Centroid,
                                       const SVector3 &PointStrart1, const SVector3 &PointStrart2, const double init_Angle,
                                       const bool createBodyForceHO,const bool oninter);
  TransverseIsoCurvatureDG3DIPVariable(const TransverseIsoCurvatureDG3DIPVariable &source);
  virtual TransverseIsoCurvatureDG3DIPVariable& operator=(const IPVariable &source);

  virtual void setLocation(const IPVariable::LOCATION loc) {
    dG3DIPVariable::setLocation(loc);
    _tiipv.setLocation(loc);
  };

  virtual IPVariable* getInternalData() {return &_tiipv;};
  virtual const IPVariable* getInternalData()  const {return &_tiipv;};

 /* specific function */
  IPTransverseIsoCurvature* getIPTransverseIsoCurvature(){return &_tiipv;}
  const IPTransverseIsoCurvature* getIPTransverseIsoCurvature() const{return &_tiipv;}
  virtual ~TransverseIsoCurvatureDG3DIPVariable(){}
  virtual double get(const int i) const;
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const;
  virtual double damageEnergy() const {return _tiipv.damageEnergy();};
  virtual IPVariable* clone() const {return new TransverseIsoCurvatureDG3DIPVariable(*this);};
  virtual void restart();
};

class TransverseIsoYarnBDG3DIPVariable : public dG3DIPVariable // or store data in a different way
{
 protected:
  IPTransverseIsoYarnB _tiipv;
 public:

  TransverseIsoYarnBDG3DIPVariable(const SVector3 &GaussP, const Yarn YD, const bool createBodyForceHO, const bool oninter);
  TransverseIsoYarnBDG3DIPVariable(const SVector3 &GaussP, const bool createBodyForceHO, const bool oninter);
  TransverseIsoYarnBDG3DIPVariable(const SVector3 &GaussP, const vector<double> Dir, const bool createBodyForceHO, const bool oninter);
  TransverseIsoYarnBDG3DIPVariable(const TransverseIsoYarnBDG3DIPVariable &source);

  TransverseIsoYarnBDG3DIPVariable& operator=(const IPVariable &source);


  // specific function
  IPTransverseIsoYarnB* getIPTransverseIsoYarnB(){return &_tiipv;}

  const IPTransverseIsoYarnB* getIPTransverseIsoYarnB() const{return &_tiipv;}
  virtual ~TransverseIsoYarnBDG3DIPVariable(){}
  //virtual double get(const int i) const;
  //virtual double defoEnergy() const;
  //virtual double plasticEnergy() const;

  virtual void setLocation(const IPVariable::LOCATION loc) {
    dG3DIPVariable::setLocation(loc);
    _tiipv.setLocation(loc);
  };
  virtual IPVariable* getInternalData() {return &_tiipv;};
  virtual const IPVariable* getInternalData()  const {return &_tiipv;};


  virtual IPVariable* clone() const {return new TransverseIsoYarnBDG3DIPVariable(*this);};

  virtual void restart();
};

class AnisotropicDG3DIPVariable : public dG3DIPVariable // or store data in a different way
{
 protected:
  IPAnisotropic _tiipv;
 public:
  AnisotropicDG3DIPVariable(const bool createBodyForceHO, const bool oninter);
  AnisotropicDG3DIPVariable(const AnisotropicDG3DIPVariable &source);
  virtual AnisotropicDG3DIPVariable& operator=(const IPVariable &source);

  virtual void setLocation(const IPVariable::LOCATION loc) {
    dG3DIPVariable::setLocation(loc);
    _tiipv.setLocation(loc);
  };

  virtual IPVariable* getInternalData() {return &_tiipv;};
  virtual const IPVariable* getInternalData()  const {return &_tiipv;};

 /* specific function */
  IPAnisotropic* getIPAnisotropic(){return &_tiipv;}
  const IPAnisotropic* getIPAnisotropic() const{return &_tiipv;}
  virtual double get(const int i) const;
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const;
  virtual double damageEnergy() const {return _tiipv.damageEnergy();}
  virtual IPVariable* clone() const {return new AnisotropicDG3DIPVariable(*this);};
  virtual void restart();
};


class AnisotropicStochDG3DIPVariable : public dG3DIPVariable // or store data in a different way
{
 protected:
  IPAnisotropicStoch _tiipv;
 public:
  AnisotropicStochDG3DIPVariable(const SVector3 &GaussP, const fullMatrix<double> &ExMat, const fullMatrix<double> &EyMat,
                                 const fullMatrix<double> &EzMat, const fullMatrix<double> &VxyMat, const fullMatrix<double> &VxzMat,
                                 const fullMatrix<double> &VyzMat, const fullMatrix<double> &MUxyMat, const fullMatrix<double> &MUxzMat,
                                 const fullMatrix<double> &MUyzMat, const double dx, const double dy, const double OrigX,
                                 const double OrigY, const int intpl,
                                 const bool createBodyForceHO, const bool oninter);
  AnisotropicStochDG3DIPVariable(const AnisotropicStochDG3DIPVariable &source);
  virtual AnisotropicStochDG3DIPVariable& operator=(const IPVariable &source);

  virtual void setLocation(const IPVariable::LOCATION loc) {
    dG3DIPVariable::setLocation(loc);
    _tiipv.setLocation(loc);
  };

  virtual IPVariable* getInternalData() {return &_tiipv;};
  virtual const IPVariable* getInternalData()  const {return &_tiipv;};

 /* specific function */
  IPAnisotropicStoch* getIPAnisotropicStoch(){return &_tiipv;}
  const IPAnisotropicStoch* getIPAnisotropicStoch() const{return &_tiipv;}
  virtual double get(const int i) const;
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const;
  virtual double damageEnergy() const {return _tiipv.damageEnergy();};
  virtual IPVariable* clone() const {return new AnisotropicStochDG3DIPVariable(*this);};
  virtual void restart();
};

/*class TFADG3DIPVariable : public dG3DIPVariable // or store data in a different way
{
 protected:
  IPTFA *_tfaipv;
 public:
  TFADG3DIPVariable(IPTFA* ipc, const bool createBodyForceHO, const bool oninter);
  TFADG3DIPVariable(const TFADG3DIPVariable &source);
  virtual TFADG3DIPVariable& operator=(const IPVariable &source);

  virtual void setLocation(const IPVariable::LOCATION loc) {
    dG3DIPVariable::setLocation(loc);
    _tfaipv->setLocation(loc);
  };
  virtual ~TFADG3DIPVariable()
  {
    if (_tfaipv) delete _tfaipv;
    _tfaipv = NULL;
  }
  virtual IPVariable* getInternalData() {return _tfaipv;};
  virtual const IPVariable* getInternalData()  const {return _tfaipv;};

  // specific function
  IPTFA* getIPTFA(){return _tfaipv;}
  const IPTFA* getIPTFA() const{return _tfaipv;}
  virtual double get(const int i) const;
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const;
  virtual double damageEnergy() const {return _tfaipv->damageEnergy();};
  virtual IPVariable* clone() const {return new TFADG3DIPVariable(*this);};
  virtual void restart();
};*/

class GenericTFADG3DIPVariable : public dG3DIPVariable // or store data in a different way
{
 protected:
  GenericIPTFA *_generictfaipv;
 public:
  GenericTFADG3DIPVariable(GenericIPTFA* ipc, const bool createBodyForceHO, const bool oninter);
  GenericTFADG3DIPVariable(const GenericTFADG3DIPVariable &source);
  virtual GenericTFADG3DIPVariable& operator=(const IPVariable &source);

  virtual void setLocation(const IPVariable::LOCATION loc) {
    dG3DIPVariable::setLocation(loc);
    _generictfaipv->setLocation(loc);
  };
  virtual ~GenericTFADG3DIPVariable();

  virtual IPVariable* getInternalData() {return _generictfaipv;};
  virtual const IPVariable* getInternalData()  const {return _generictfaipv;};

  GenericIPTFA* getIPTFA(){return _generictfaipv;}
  const GenericIPTFA* getIPTFA() const{return _generictfaipv;}
  virtual double get(const int i) const;
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const;
  virtual double damageEnergy() const {return _generictfaipv->damageEnergy();};
  virtual IPVariable* clone() const {return new GenericTFADG3DIPVariable(*this);};
  virtual void restart();
};

class TFADG3DIPVariable : public GenericTFADG3DIPVariable // or store data in a different way
{
 protected:
  IPTFA *_tfaipv;
 public:
  TFADG3DIPVariable(IPTFA* ipc, const bool createBodyForceHO, const bool oninter);
  TFADG3DIPVariable(const TFADG3DIPVariable &source);
  virtual TFADG3DIPVariable& operator=(const IPVariable &source);

  virtual void setLocation(const IPVariable::LOCATION loc) {
    dG3DIPVariable::setLocation(loc);
    _tfaipv->setLocation(loc);
  };

  virtual ~TFADG3DIPVariable();

  virtual IPVariable* getInternalData() {return _tfaipv;};
  virtual const IPVariable* getInternalData()  const {return _tfaipv;};

  IPTFA* getIPTFA(){return _tfaipv;}
  const IPTFA* getIPTFA() const{return _tfaipv;}
  virtual double get(const int i) const;
  virtual IPVariable* clone() const {return new TFADG3DIPVariable(*this);};
  virtual void restart();
};

class TFA2LevelsDG3DIPVariable : public GenericTFADG3DIPVariable // or store data in a different way
{
 protected:
  IPTFA2Levels *_tfa2levelsipv;
 public:
  TFA2LevelsDG3DIPVariable(IPTFA2Levels* ipc, const bool createBodyForceHO, const bool oninter);
  TFA2LevelsDG3DIPVariable(const TFA2LevelsDG3DIPVariable &source);
  virtual TFA2LevelsDG3DIPVariable& operator=(const IPVariable &source);

  virtual void setLocation(const IPVariable::LOCATION loc) {
    dG3DIPVariable::setLocation(loc);
    _tfa2levelsipv->setLocation(loc);
  };
  virtual ~TFA2LevelsDG3DIPVariable();

  virtual IPVariable* getInternalData() {return _tfa2levelsipv;};
  virtual const IPVariable* getInternalData()  const {return _tfa2levelsipv;};

  // specific function
  IPTFA2Levels* getIPTFA(){return _tfa2levelsipv;}
  const IPTFA2Levels* getIPTFA() const{return _tfa2levelsipv;}
  virtual double get(const int i) const;
  virtual IPVariable* clone() const {return new TFA2LevelsDG3DIPVariable(*this);};
  virtual void restart();
};




/*
class ExtraDofDiffusionDG3DIPVariableBase : public dG3DIPVariable
{

 protected:


  double                       field;
  SVector3                     gradField;
  SVector3                     flux;
  STensor3                     dPdField;
	STensor33										 dPdGradField;
  STensor3                     dFluxdGradField;
  SVector3                     dFluxdField;
  STensor33                    dFluxdF;
  const STensor3               *linearK;
  double                       fieldSource;  //related to cp*dT/dt
  double                       dFieldSourcedField;
	SVector3                     dFieldSourcedGradField;
  STensor3                     dFieldSourcedF;

	double                       mechanicalSource; // related to convert mechanical energy to heat
  double                       dMechanicalSourcedField;
  SVector3                     dMechanicalSourcedGradField;
  STensor3                     dMechanicalSourcedF;

	double												Cp; // field capacity

 // for dg: jump in non local epl
  double                       fieldJump;
  double                       oneOverFieldJump;
  double                       dOneOverFieldJumpdFieldm;
  double                       dOneOverFieldJumpdFieldp;
  SVector3                     interfaceFlux;
  const STensor3	      *linearSymmetrizationCoupling;


 public:
  ExtraDofDiffusionDG3DIPVariableBase(const bool oninter=false);
  ExtraDofDiffusionDG3DIPVariableBase(const  ExtraDofDiffusionDG3DIPVariableBase &source);
  ExtraDofDiffusionDG3DIPVariableBase& operator=(const IPVariable &source);

  virtual ~ExtraDofDiffusionDG3DIPVariableBase()
  {
  }

  virtual double defoEnergy() const=0;
  virtual double plasticEnergy() const=0;
  virtual double damageEnergy() const = 0;

  virtual double get(const int i) const=0;
  virtual double getInternalEnergyExtraDofDiffusion() const = 0;



  virtual IPVariable* clone() const =0;
  virtual void restart();
};*/


//new ThermoMec
class ThermoMechanicsDG3DIPVariableBase : public dG3DIPVariable, public extraDofIPVariableBase
{

 protected:


 public:
  ThermoMechanicsDG3DIPVariableBase(const bool createBodyForceHO, const bool oninter);
  ThermoMechanicsDG3DIPVariableBase(const ThermoMechanicsDG3DIPVariableBase &source);
  ThermoMechanicsDG3DIPVariableBase& operator=(const IPVariable &source);

  virtual ~ThermoMechanicsDG3DIPVariableBase()
  {
  }
  virtual double getInternalEnergyExtraDofDiffusion() const = 0;
  virtual void getConstitutiveExtraDofFlux(const int index, SVector3& q) const {
    if (index > 0) Msg::Error("constitutive field with index %d is not defined",index);
    q = getConstRefToFlux()[index];
  }
  // constitutive field internal energy
  virtual double getConstitutiveExtraDofInternalEnergy(const int index) const {
    if (index > 0) Msg::Error("constitutive field with index %d is not defined",index);
    return getInternalEnergyExtraDofDiffusion();
  }
  // constitutive field capacity per unit field
  virtual double getConstitutiveExtraDofFieldCapacityPerUnitField(const int index) const {
    if (index > 0) Msg::Error("constitutive field with index %d is not defined",index);
    return getConstRefToExtraDofFieldCapacityPerUnitField()(index);
  };
  // mechanical source converted to this constitutive field
  virtual double getConstitutiveExtraDofMechanicalSource(const int index) const {
    if (index > 0) Msg::Error("constitutive field with index %d is not defined",index);
    return getConstRefToMechanicalSource()(index);
  };
  // non-constitutive field value
  virtual void getNonConstitutiveExtraDofFlux(const int index,SVector3& flux) const{
    Msg::Error("ExtraDofDiffusionDG3DIPVariableBase::getNonConstitutiveExtraDofFlux is not defined");
  }

  // Archiving data to be changed
  virtual const double getConstRefToTemperature() const {return getConstRefToField(0);}
  virtual double &getRefToTemperature() {return getRefToField(0);}

  virtual const SVector3 &getConstRefToGradT() const {return getConstRefToGradField()[0];}
  virtual SVector3 &getRefToGradT() {return getRefToGradField()[0];}

  virtual const SVector3 &getConstRefToThermalFlux() const {return getConstRefToFlux()[0];}
  virtual SVector3 &getRefToThermalFlux() {return getRefToFlux()[0];}

  virtual  STensor3 &getRefTodPdT() {return getRefTodPdField()[0];}

  virtual const  STensor3 &getConstRefTodThermalFluxdGradT() const {return  getConstRefTodFluxdGradField()[0][0];}
  virtual  STensor3 &getRefTodThermalFluxdGradT() {return  getRefTodFluxdGradField()[0][0];}

  virtual const  SVector3 &getConstRefTodThermalFluxdT() const {return  getConstRefTodFluxdField()[0][0];}
  virtual  SVector3 &getRefTodThermalFluxdT() {return  getRefTodFluxdField()[0][0];}

  virtual const  STensor33 &getConstRefTodThermalFluxdF() const {return  getConstRefTodFluxdF()[0];}
  virtual  STensor33 &getRefTodThermalFluxdF() {return  getRefTodFluxdF()[0];}

  virtual const double getConstRefToTemperatureJump() const {return getConstRefToFieldJump()(0);}
  virtual double &getRefToTemperatureJump() {return  getRefToFieldJump()(0);}

  virtual const double getConstRefToOneOverTemperatureJump() const {return getConstRefToEnergyConjugatedFieldJump()(0);}
  virtual double &getRefToOneOverTemperatureJump() {return  getRefToEnergyConjugatedFieldJump()(0);}

  virtual const double getConstRefTodOneOverTemperatureJumpdFieldm() const {return getConstRefTodEnergyConjugatedFieldJumpdFieldm()[0][0];}
  virtual double &getRefTodOneOverTemperatureJumpdFieldm() {return  getRefTodEnergyConjugatedFieldJumpdFieldm()[0][0];}

  virtual const double getConstRefTodOneOverTemperatureJumpdFieldp() const {return getConstRefTodEnergyConjugatedFieldJumpdFieldp()[0][0];}
  virtual double &getRefTodOneOverTemperaturedJumpdFieldp() {return  getRefTodEnergyConjugatedFieldJumpdFieldp()[0][0];}

  virtual const double getConstRefToThermalSource() const {return getConstRefToFieldSource()(0);}
  virtual double &getRefToThermalSource() {return  getRefToFieldSource()(0);}

  virtual const double getConstRefTodThermalSourcedField() const {return getConstRefTodFieldSourcedField()(0,0);}
  virtual double &getRefTodThermalSourcedField() {return  getRefTodFieldSourcedField()(0,0);}

  virtual const  STensor3 &getConstRefTodThermalSourcedF() const {return getConstRefTodFieldSourcedF()[0];}
  virtual  STensor3 &getRefTodThermalSourcedF() {return getRefTodFieldSourcedF()[0];}

  virtual const STensor3 *getConstRefToStiff_alphaDilatation() const {return getConstRefToLinearSymmetrizationCoupling()[0];}
  virtual void setRefToStiff_alphaDilatation(const STensor3 &eT) {setConstRefToLinearSymmetrizationCoupling(eT,0);}

  virtual const STensor3 *getConstRefToLinearConductivity() const {return getConstRefToLinearK()[0][0];}
  virtual void setRefToLinearConductivity(const STensor3 &eT) {setConstRefToLinearK(eT,0,0);}


  virtual double get(const int i) const;
  virtual IPVariable* clone() const =0;
  virtual void restart();
};


class LinearThermoMechanicsDG3DIPVariable : public ThermoMechanicsDG3DIPVariableBase
{

 protected:
  IPLinearThermoMechanics _linearTMIP;
  LinearThermoMechanicsDG3DIPVariable(const bool createBodyForceHO, const bool oninter):ThermoMechanicsDG3DIPVariableBase(createBodyForceHO,oninter) {Msg::Error("LinearThermoMechanicsDG3DIPVariable: Cannot use the default constructor");}
 public:
  LinearThermoMechanicsDG3DIPVariable(double tinitial, const bool createBodyForceHO, const bool oninter);
  LinearThermoMechanicsDG3DIPVariable(const LinearThermoMechanicsDG3DIPVariable &source);
  LinearThermoMechanicsDG3DIPVariable& operator=(const IPVariable &source);

  virtual void setLocation(const IPVariable::LOCATION loc) {
    dG3DIPVariable::setLocation(loc);
    _linearTMIP.setLocation(loc);
  };

  virtual IPVariable* getInternalData() {return &_linearTMIP;};
  virtual const IPVariable* getInternalData()  const {return &_linearTMIP;};

 /* specific function */
  IPLinearThermoMechanics* getIPLinearThermoMechanics(){return &_linearTMIP;}
  const IPLinearThermoMechanics* getIPLinearThermoMechanics() const{return &_linearTMIP;}
  virtual ~LinearThermoMechanicsDG3DIPVariable()
  {
  }
  virtual double get(const int i) const;
  virtual double defoEnergy() const;
  virtual double getInternalEnergyExtraDofDiffusion() const {return _linearTMIP.getThermalEnergy();};
  virtual double plasticEnergy() const;
  virtual double damageEnergy() const {return _linearTMIP.damageEnergy();};
  virtual IPVariable* clone() const {return new LinearThermoMechanicsDG3DIPVariable(*this);};
  virtual void restart();
  virtual int fractureEnergy(double* arrayEnergy) const{arrayEnergy[0]=_linearTMIP.getConstRefToFractureEnergy(); return 1;} //need it to get H1 norm in the interface  e.g. LinearThermoMech
};

class J2ThermoMechanicsDG3DIPVariable : public ThermoMechanicsDG3DIPVariableBase // or store data in a different way
{
 protected:
  IPJ2ThermoMechanics *_j2ipv;
  J2ThermoMechanicsDG3DIPVariable(const mlawJ2ThermoMechanics &_j2law, const bool createBodyForceHO, const bool oninter):ThermoMechanicsDG3DIPVariableBase(createBodyForceHO,oninter) {Msg::Error("J2ThermoMechanicsDG3DIPVariable: Cannot use the default constructor");}
 public:
  J2ThermoMechanicsDG3DIPVariable(double tinitial,const mlawJ2ThermoMechanics &_j2law, const bool createBodyForceHO, const bool oninter);
  J2ThermoMechanicsDG3DIPVariable(const J2ThermoMechanicsDG3DIPVariable &source);
  J2ThermoMechanicsDG3DIPVariable& operator=(const IPVariable &source);
  virtual ~J2ThermoMechanicsDG3DIPVariable(){if(_j2ipv!=NULL) {delete _j2ipv; _j2ipv=NULL;}};

  virtual void setLocation(const IPVariable::LOCATION loc) {
    ThermoMechanicsDG3DIPVariableBase::setLocation(loc);
    if (_j2ipv)
      _j2ipv->setLocation(loc);
  };


  virtual IPVariable* getInternalData() {return _j2ipv;};
  virtual const IPVariable* getInternalData()  const {return _j2ipv;};

 /* specific function */
  IPJ2ThermoMechanics* getIPJ2ThermoMechanics(){return _j2ipv;}
  const IPJ2ThermoMechanics* getIPJ2ThermoMechanics() const{return _j2ipv;}
  virtual double get(const int i) const;
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const;
  virtual double damageEnergy() const {return _j2ipv->damageEnergy();}
  virtual double getInternalEnergyExtraDofDiffusion() const {return _j2ipv->getThermalEnergy();};
  virtual IPVariable* clone() const {return new J2ThermoMechanicsDG3DIPVariable(*this);};
  virtual void restart();
};

class J2FullThermoMechanicsDG3DIPVariable : public ThermoMechanicsDG3DIPVariableBase{
	protected:
		IPJ2ThermoMechanics _j2Thermo;

	public:
		J2FullThermoMechanicsDG3DIPVariable(const J2IsotropicHardening* j2harden, const bool createBodyForceHO, const bool oninter);
		J2FullThermoMechanicsDG3DIPVariable(const J2FullThermoMechanicsDG3DIPVariable& src);
		virtual J2FullThermoMechanicsDG3DIPVariable& operator =(const IPVariable& src);
		virtual ~J2FullThermoMechanicsDG3DIPVariable(){};

		virtual void setLocation(const IPVariable::LOCATION loc) {
			ThermoMechanicsDG3DIPVariableBase::setLocation(loc);
			_j2Thermo.setLocation(loc);
		};

    virtual IPVariable* getInternalData() {return &_j2Thermo;};
    virtual const IPVariable* getInternalData()  const {return &_j2Thermo;};

		IPJ2ThermoMechanics* getIPJ2ThermoMechanics(){return &_j2Thermo;}
		const IPJ2ThermoMechanics* getIPJ2ThermoMechanics() const{return &_j2Thermo;}
		virtual double get(const int i) const;
		virtual double defoEnergy() const {return _j2Thermo.defoEnergy();};
		virtual double plasticEnergy() const {return _j2Thermo.plasticEnergy();};
    virtual double damageEnergy() const {return _j2Thermo.damageEnergy();};
		virtual double getInternalEnergyExtraDofDiffusion() const {return _j2Thermo.getThermalEnergy();};
		virtual IPVariable* clone() const {return new J2FullThermoMechanicsDG3DIPVariable(*this);};
		virtual void restart();
};


class AnIsotropicTherMechDG3DIPVariable : public ThermoMechanicsDG3DIPVariableBase
{

 protected:
  IPAnIsotropicTherMech _AnIsotropicTMIP;

public:
  AnIsotropicTherMechDG3DIPVariable(const bool createBodyForceHO, const bool oninter):ThermoMechanicsDG3DIPVariableBase(createBodyForceHO,oninter) {Msg::Error("AnIsotropicTherMechDG3DIPVariable: Cannot use the default constructor");}
	AnIsotropicTherMechDG3DIPVariable(double tinitial, const bool createBodyForceHO, const bool oninter);
  AnIsotropicTherMechDG3DIPVariable(const AnIsotropicTherMechDG3DIPVariable &source);
  AnIsotropicTherMechDG3DIPVariable& operator=(const IPVariable &source);

  virtual void setLocation(const IPVariable::LOCATION loc) {
    ThermoMechanicsDG3DIPVariableBase::setLocation(loc);
    _AnIsotropicTMIP.setLocation(loc);
  };

  virtual IPVariable* getInternalData() {return &_AnIsotropicTMIP;};
  virtual const IPVariable* getInternalData()  const {return &_AnIsotropicTMIP;};

 /* specific function */
  IPAnIsotropicTherMech* getIPAnIsotropicTherMech(){return &_AnIsotropicTMIP;}
  const IPAnIsotropicTherMech* getIPAnIsotropicTherMech() const{return &_AnIsotropicTMIP;}
  virtual ~AnIsotropicTherMechDG3DIPVariable()
  {
  }
  virtual double get(const int i) const;
  virtual double defoEnergy() const;
  virtual double getInternalEnergyExtraDofDiffusion() const {return _AnIsotropicTMIP.getThermalEnergy();};
  virtual double plasticEnergy() const;
  virtual double damageEnergy() const {return _AnIsotropicTMIP.damageEnergy();};
  virtual IPVariable* clone() const {return new AnIsotropicTherMechDG3DIPVariable(*this);};
  virtual void restart();

};

class SMPDG3DIPVariable : public ThermoMechanicsDG3DIPVariableBase
{
  protected:
  IPSMP *_TMIPSMP;

	public:
	 SMPDG3DIPVariable(const mlawSMP &_lawSMP,const bool createBodyForceHO, const bool oninter):ThermoMechanicsDG3DIPVariableBase(createBodyForceHO,oninter){Msg::Error("MPDG3DIPVariable: Cannot use the default constructor");}
	 SMPDG3DIPVariable(double tinitial,const mlawSMP &_lawSMP, const bool createBodyForceHO, const bool oninter);
	 SMPDG3DIPVariable(const SMPDG3DIPVariable &source);
	 SMPDG3DIPVariable& operator=(const IPVariable &source);


  virtual void setLocation(const IPVariable::LOCATION loc) {
    ThermoMechanicsDG3DIPVariableBase::setLocation(loc);
    if (_TMIPSMP){
      _TMIPSMP->setLocation(loc);
    }
  };

  virtual IPVariable* getInternalData() {return _TMIPSMP;};
  virtual const IPVariable* getInternalData()  const {return _TMIPSMP;};

 /* specific function */
  IPSMP* getIPSMP(){return _TMIPSMP;}
  const IPSMP* getIPSMP() const{return _TMIPSMP;}
  virtual ~SMPDG3DIPVariable()
  {
    if(_TMIPSMP != NULL)
    { delete _TMIPSMP;
      _TMIPSMP = NULL;
    }
  }
  virtual double get(const int i) const;
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const;
  virtual double damageEnergy() const {return _TMIPSMP->damageEnergy();};
  virtual double getInternalEnergyExtraDofDiffusion() const {return _TMIPSMP->getThermalEnergy();};
  virtual double getRefToEquivalentPlasticDefo1() const {return _TMIPSMP->getConstRefToEquivalentPlasticDefo1();};
  virtual IPVariable* clone() const {return new SMPDG3DIPVariable(*this);};
  virtual void restart();
};




//===========/start/Phenomenological===================================================================================
class PhenomenologicalSMPDG3DIPVariable : public ThermoMechanicsDG3DIPVariableBase
{
protected:
    IPPhenomenologicalSMP *_TMIPSMP;

public:
    PhenomenologicalSMPDG3DIPVariable(const mlawPhenomenologicalSMP &_lawSMP, const bool createBodyForceHO, const bool oninter):ThermoMechanicsDG3DIPVariableBase(createBodyForceHO,oninter)
                                        { Msg::Error("MPDG3DIPVariable: Cannot use the default constructor");};
    PhenomenologicalSMPDG3DIPVariable(double tinitial, const mlawPhenomenologicalSMP &_lawSMP, const bool createBodyForceHO, const bool oninter);
    PhenomenologicalSMPDG3DIPVariable(const PhenomenologicalSMPDG3DIPVariable &source);
    PhenomenologicalSMPDG3DIPVariable& operator=(const IPVariable &source);
    virtual ~PhenomenologicalSMPDG3DIPVariable()
    {
        if(_TMIPSMP != NULL)
        {
            delete _TMIPSMP;
            _TMIPSMP = NULL;
        }
    };

    virtual void setLocation(const IPVariable::LOCATION loc)
    {
        ThermoMechanicsDG3DIPVariableBase::setLocation(loc);
        if (_TMIPSMP)
        {
            _TMIPSMP->setLocation(loc);
        }
    };

    virtual IPVariable* getInternalData()               {return _TMIPSMP;};
    virtual const IPVariable* getInternalData() const   {return _TMIPSMP;};

    /* specific function */
    IPPhenomenologicalSMP* getIPPhenomenologicalSMP()               {return _TMIPSMP;};
    const IPPhenomenologicalSMP* getIPPhenomenologicalSMP() const   {return _TMIPSMP;};

    virtual double get(const int i) const;
    virtual double defoEnergy() const;
    virtual double plasticEnergy() const;
    virtual double damageEnergy() const {return _TMIPSMP->damageEnergy();};
    virtual double getInternalEnergyExtraDofDiffusion() const {return _TMIPSMP->getThermalEnergy();};
    virtual IPVariable* clone() const {return new PhenomenologicalSMPDG3DIPVariable(*this);};
    virtual void restart();
};
//===========/end/Phenomenological===================================================================================





class crystalPlasticityDG3DIPVariable : public dG3DIPVariable
{

 protected:
  IPCrystalPlasticity *_ipv;

 public:
  crystalPlasticityDG3DIPVariable(int _nsdv, double size, const bool createBodyForceHO, const bool oninter);
  crystalPlasticityDG3DIPVariable(const crystalPlasticityDG3DIPVariable &source);
  virtual crystalPlasticityDG3DIPVariable& operator=(const IPVariable &source);

 /* specific function */
  IPCrystalPlasticity* getIPCrystalPlasticity(){return _ipv;}
  const IPCrystalPlasticity* getIPCrystalPlasticity() const{return _ipv;}
  virtual ~crystalPlasticityDG3DIPVariable()
  {
    if (_ipv) delete _ipv;
    _ipv = NULL;
  }

  virtual void setLocation(const IPVariable::LOCATION loc) {
    dG3DIPVariable::setLocation(loc);
    if (_ipv)
      _ipv->setLocation(loc);
  };

  virtual IPVariable* getInternalData() {return _ipv;};
  virtual const IPVariable* getInternalData()  const {return _ipv;};

  virtual bool dissipationIsActive() const {return _ipv->dissipationIsActive();};

  virtual void blockDissipation(const bool fl){if (_ipv) _ipv->blockDissipation(fl);};
  virtual bool dissipationIsBlocked() const {
    if (_ipv) return _ipv->dissipationIsBlocked();
    else return false;
  }

  virtual double get(const int i) const;
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const;

  virtual IPVariable* clone() const {return new crystalPlasticityDG3DIPVariable(*this);};
  virtual void restart();
};

class VEVPUMATDG3DIPVariable : public dG3DIPVariable
{

 protected:
  IPVEVPUMAT *_ipv;

 public:
  VEVPUMATDG3DIPVariable(int _nsdv, double size, const bool createBodyForceHO, const bool oninter);
  VEVPUMATDG3DIPVariable(const VEVPUMATDG3DIPVariable &source);
  virtual VEVPUMATDG3DIPVariable& operator=(const IPVariable &source);

 /* specific function */
  IPVEVPUMAT* getIPVEVPUMAT(){return _ipv;}
  const IPVEVPUMAT* getIPVEVPUMAT() const{return _ipv;}
  virtual ~VEVPUMATDG3DIPVariable()
  {
    if (_ipv) delete _ipv;
    _ipv = NULL;
  }

  virtual void setLocation(const IPVariable::LOCATION loc) {
    dG3DIPVariable::setLocation(loc);
    if (_ipv)
      _ipv->setLocation(loc);
  };

  virtual IPVariable* getInternalData() {return _ipv;};
  virtual const IPVariable* getInternalData()  const {return _ipv;};

  virtual bool dissipationIsActive() const {return _ipv->dissipationIsActive();};

  virtual void blockDissipation(const bool fl){if (_ipv) _ipv->blockDissipation(fl);};
  virtual bool dissipationIsBlocked() const {
    if (_ipv) return _ipv->dissipationIsBlocked();
    else return false;
  }

  virtual double get(const int i) const;
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const;

  virtual IPVariable* clone() const {return new VEVPUMATDG3DIPVariable(*this);};
  virtual void restart();
};

class IMDEACPUMATDG3DIPVariable : public dG3DIPVariable
{

 protected:
  IPIMDEACPUMAT *_ipv;

 public:
  IMDEACPUMATDG3DIPVariable(int _nsdv, double size, const bool createBodyForceHO, const bool oninter);
  IMDEACPUMATDG3DIPVariable(const IMDEACPUMATDG3DIPVariable &source);
  virtual IMDEACPUMATDG3DIPVariable& operator=(const IPVariable &source);

 /* specific function */
  IPIMDEACPUMAT* getIPIMDEACPUMAT(){return _ipv;}
  const IPIMDEACPUMAT* getIPIMDEACPUMAT() const{return _ipv;}
  virtual ~IMDEACPUMATDG3DIPVariable()
  {
    if (_ipv) delete _ipv;
    _ipv = NULL;
  }

  virtual void setLocation(const IPVariable::LOCATION loc) {
    dG3DIPVariable::setLocation(loc);
    if (_ipv)
      _ipv->setLocation(loc);
  };

  virtual IPVariable* getInternalData() {return _ipv;};
  virtual const IPVariable* getInternalData()  const {return _ipv;};

  virtual bool dissipationIsActive() const {return _ipv->dissipationIsActive();};

  virtual void blockDissipation(const bool fl){if (_ipv) _ipv->blockDissipation(fl);};
  virtual bool dissipationIsBlocked() const {
    if (_ipv) return _ipv->dissipationIsBlocked();
    else return false;
  }

  virtual double get(const int i) const;
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const;

  virtual IPVariable* clone() const {return new IMDEACPUMATDG3DIPVariable(*this);};
  virtual void restart();
};

class gursonUMATDG3DIPVariable : public dG3DIPVariable
{

 protected:
  IPGursonUMAT *_ipv;

 public:
  gursonUMATDG3DIPVariable(int _nsdv, double size, const bool createBodyForceHO, const bool oninter);
  gursonUMATDG3DIPVariable(const gursonUMATDG3DIPVariable &source);
  virtual gursonUMATDG3DIPVariable& operator=(const IPVariable &source);

 /* specific function */
  IPGursonUMAT* getIPGursonUMAT(){return _ipv;}
  const IPGursonUMAT* getIPGursonUMAT() const{return _ipv;}
  virtual ~gursonUMATDG3DIPVariable()
  {
    if (_ipv) delete _ipv;
    _ipv = NULL;
  }

  virtual void setLocation(const IPVariable::LOCATION loc) {
    dG3DIPVariable::setLocation(loc);
    if (_ipv)
      _ipv->setLocation(loc);
  };

  virtual IPVariable* getInternalData() {return _ipv;};
  virtual const IPVariable* getInternalData()  const {return _ipv;};

  virtual bool dissipationIsActive() const {return _ipv->dissipationIsActive();};

  virtual void blockDissipation(const bool fl){if (_ipv) _ipv->blockDissipation(fl);};
  virtual bool dissipationIsBlocked() const {
    if (_ipv) return _ipv->dissipationIsBlocked();
    else return false;
  }

  virtual double get(const int i) const;
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const;

  virtual IPVariable* clone() const {return new gursonUMATDG3DIPVariable(*this);};
  virtual void restart();
};


class ElecTherMechDG3DIPVariableBase : public dG3DIPVariable
{

 protected:
/*
 // double                      Temperature;
 //SVector3                     gradT;
 //SVector3                     fluxT;
 //STensor3                     dPdT;
 //STensor3                     dFluxTdGradT;
 //SVector3                     dFluxTdT;
 //STensor3                     dFluxTdGradV;
 //SVector3                     dFluxTdV;
 //STensor33                    dFluxTdF;
 // const STensor3               *lineark10;
 // const STensor3               *lineark20;
 // STensor3                     k10;
 //STensor3                     k20;
  //STensor3                     dk10dT;
  //STensor3                     dk20dT;
  //STensor3                     dk10dv;

  //STensor43                    dk10dF;
  //STensor43                    dk20dF;
 // for dg: jump in non local epl
 // double                       TemperatureJump;
  //SVector3                     interfaceFluxT; //should be interfaceFluxJy


  //double                       Voltage;
  //SVector3                     gradV;
  //SVector3                     fluxV;
  //STensor3                     dPdV;
  //STensor3                     dFluxVdGradV;
  //SVector3                     dFluxVdV;
  //STensor3                     dFluxVdGradT;
  //SVector3                     dFluxVdT;
  //STensor33                    dFluxVdF;
  //const STensor3               *linearl10;
  //const STensor3               *linearl20;
  //STensor3                     l10;
  //STensor3                     l20;
  //STensor3                     dl10dT;
  //STensor3                     dl20dT;
  //STensor3                     dl20dv;

  //STensor43                    dl10dF;
  //STensor43                    dl20dF;

  //double                       thermalSource;
  //double                       dThermalSourcedT;
  //STensor3                     dThermalSourcedF;
 // for dg: jump in non local epl
  //double                       VoltageJump;
  //SVector3                     interfaceFluxV;

  //double                       fvJump;
  //double                       fTJump;
  //double                       dfvjumpdvm;
  //double                       dfvjumpdvp;
  //double                       dfvjumpdTm;
  //double                       dfvjumpdTp;
 //double                       dfTjumpdTm;
 //double                       dfTjumpdTp;

  //STensor3 			dgradVdgradfV;
  //STensor3 			dgradVdgradfT;
  //STensor3 			dgradTdgradfV;
  //STensor3 			dgradTdgradfT;
  //double 			dVdfV;
  //double 			dVdfT;
  //double 			dTdfV;
  //double 			dTdfT;
  //SVector3 			dgradVdfV;
  //SVector3  			dgradVdfT;
  //SVector3  			dgradTdfT;
  //SVector3  			dgradTdfV;

  //double                       fT;
  //double                       fV;
  //SVector3                      gradfT;
  //SVector3                      gradfV;

  //SVector3 			fluxjy;
  //SVector3 			djydV;
  //STensor3 			djydgradV;
  //SVector3 			djydT;
  //STensor3 			djydgradT;
  //STensor33 			djydF;
  //STensor3 			jy1;
  //STensor3 			djy1dT;
  //STensor3 			djy1dV;
  //STensor43			djy1dF;
 // const STensor3		*Stiff_alphaDilatation;

*/

 public:
  ElecTherMechDG3DIPVariableBase(const bool createBodyForceHO, const bool oninter);
  ElecTherMechDG3DIPVariableBase(const  ElecTherMechDG3DIPVariableBase &source);
  ElecTherMechDG3DIPVariableBase& operator=(const IPVariable &source);

  virtual ~ElecTherMechDG3DIPVariableBase()
  {

  }

  virtual double defoEnergy() const=0;
  virtual double plasticEnergy() const=0;
  virtual double damageEnergy() const = 0;
  virtual double get(const int i) const;

  // Archiving data to be changed

  virtual const double getConstRefToTemperature() const {return getConstRefToField(0);}
  virtual double &getRefToTemperature() {return getRefToField(0);}

  virtual const SVector3 &getConstRefToGradT() const {return getConstRefToGradField()[0];}
  virtual SVector3 &getRefToGradT() {return getRefToGradField()[0];}

  virtual const SVector3 &getConstRefToFluxT() const
      {Msg::Error("ElecTherMechDG3DIPVariableBase::getConstRefToFluxT should not be used; use getConstRefToFluxEnergy"); static SVector3 a; return a;}
  virtual SVector3 &getRefToFluxT()
      {Msg::Error("ElecTherMechDG3DIPVariableBase::getRefToFluxT should not be used; use getRefToFluxEnergy");static SVector3 a; return a;}

  virtual  const STensor3 &getConstRefTodPdT() const {return getConstRefTodPdField()[0];}
  virtual  STensor3 &getRefTodPdT() {return getRefTodPdField()[0];}

 /* virtual const  STensor3 &getConstRefTodThermalFluxdGradT() const {return  getConstRefTodFluxdGradField()[0][0];}
  virtual  STensor3 &getRefTodThermalFluxdGradT() {return  getRefTodFluxdGradField()[0][0];}*/

  virtual const SVector3 & getConstRefTodFluxTdT() const
  {
    Msg::Error("ElecTherMechDG3DIPVariableBase::getConstRefTodFluxTdT should not be used; use getConstRefTodFluxEnergydT");
    static SVector3 a; return a;
  }
  virtual SVector3 & getRefTodFluxTdT()
  {
    Msg::Error("ElecTherMechDG3DIPVariableBase::getRefTodFluxTdT should not be used; use getRefTodFluxEnergydT");
    static SVector3 a; return a;
  }

  virtual const  SVector3 &getConstRefTodFluxTdV() const {return  getConstRefTodFluxdField()[0][1];}
  virtual  SVector3 &getRefTodFluxTdV() {return  getRefTodFluxdField()[0][1];}

  virtual const  STensor3 & getConstRefTodFluxTdGradT() const
  {
    Msg::Error("ElecTherMechDG3DIPVariableBase::getConstRefTodFluxTdGradT should not be used; use getConstRefTodFluxEnergydGradT");
    static STensor3 a; return a;
  }//dqdgradT,
  virtual  STensor3 &getRefTodFluxTdGradT()
  {
    Msg::Error("ElecTherMechDG3DIPVariableBase::getRefTodFluxTdGradT should not be used; use getRefTodFluxEnergydGradT");
    static STensor3 a; return a;
  }

  virtual const  STensor3 &getConstRefTodFluxTdGradV() const {return  getConstRefTodFluxdGradField()[0][1];}//dqdgradV,
  virtual  STensor3 &getRefTodFluxTdGradV() {return  getRefTodFluxdGradField()[0][1];}

  virtual const  STensor33 &getConstRefTodFluxTdF() const
  {
    Msg::Error("ElecTherMechDG3DIPVariableBase::getConstRefTodFluxTdF should not be used; use getConstRefTodFluxEnergydF");
    static STensor33 a; return a;
  }
  virtual  STensor33 &getRefTodFluxTdF()
  {
    Msg::Error("ElecTherMechDG3DIPVariableBase::getRefTodFluxTdF should not be used; use getRefTodFluxEnergydF");
    static STensor33 a; return a;
  }

/* virtual const  SVector3 &getConstRefTodFluxTdT() const {return  getConstRefTodFluxdField()[0][0];}
  virtual  SVector3 &getRefTodFluxTdT() {return  getRefTodFluxdField()[0][0];}*/

/*  virtual const  STensor33 &getConstRefTodThermalFluxdF() const {return  getConstRefTodFluxdF()[0];}
  virtual  STensor33 &getRefTodThermalFluxdF() {return  getRefTodFluxdF()[0];}*/

  /*virtual const STensor3 &getConstRefTolineark10() const {return *lineark10;}
  virtual void setRefTolineark10(const STensor3 &eT1) {lineark10=&eT1;}

  virtual const STensor3 &getConstRefTolineark20() const {return *lineark20;}
  virtual void setRefTolineark20(const STensor3 &eT2) {lineark20=&eT2;}*/

 /* virtual const  STensor3 &getConstRefToElecConductivity() const {return  l10;}
  virtual  STensor3 &getRefToElecConductivity() {return  l10;}

  virtual const  STensor3 &getConstRefToCrossElecConductivity() const {return  l20;}
  virtual  STensor3 &getRefToCrossElecConductivity() {return  l20;}*/

  virtual const  STensor3 *getConstRefToElecConductivity() const {return getConstRefToLinearK()[1][1];}
  virtual void setRefToElecConductivity(const STensor3 &eT) {return  setConstRefToLinearK(eT,1,1);}

  virtual const  STensor3 *getConstRefToCrossElecConductivity() const {return getConstRefToLinearK()[1][0];}
  virtual void setRefToCrossElecConductivity(const STensor3 &eT) {return  setConstRefToLinearK(eT,1,0);}

  virtual const  STensor3 *getConstRefToTherConductivity() const {return getConstRefToLinearK()[0][0];}
  virtual void setRefToTherConductivity(const STensor3 &eT) {return  setConstRefToLinearK(eT,0,0);}

  virtual const  STensor3 *getConstRefToCrossTherConductivity() const {return getConstRefToLinearK()[0][1];}
  virtual void setRefToCrossTherConductivity(const STensor3 &eT) {return  setConstRefToLinearK(eT,0,1);}

  virtual const  STensor3 &getConstRefTodElecConductivitydT() const {return  getConstRefTodLinearKdField()[1][1][0];}
  virtual  STensor3 &getRefTodElecConductivitydT() {return  getRefTodLinearKdField()[1][1][0];}

  virtual const  STensor3 &getConstRefTodCrossElecConductivitydT() const {return  getConstRefTodLinearKdField()[1][0][0];}
  virtual  STensor3 &getRefTodCrossElecConductivitydT() {return  getRefTodLinearKdField()[1][0][0];}

  virtual const  STensor3 &getConstRefTodTherConductivitydT() const {return  getConstRefTodLinearKdField()[0][0][0];}
  virtual  STensor3 &getRefTodTherConductivitydT() {return  getRefTodLinearKdField()[0][0][0];}

  virtual const  STensor3 &getConstRefTodCrossTherConductivitydT() const {return  getConstRefTodLinearKdField()[0][1][0];}
  virtual  STensor3 &getRefTodCrossTherConductivitydT() {return  getRefTodLinearKdField()[0][1][0];}

  virtual const  STensor3 &getConstRefTodTherConductivitydv() const {return  getConstRefTodLinearKdField()[0][0][1];}
  virtual  STensor3 &getRefTodTherConductivitydv() {return  getRefTodLinearKdField()[0][0][1];}

  virtual const  STensor3 &getConstRefTodCrossTherConductivitydv() const {return  getConstRefTodLinearKdField()[0][1][1];}
  virtual  STensor3 &getRefTodCrossTherConductivitydv() {return  getRefTodLinearKdField()[0][1][1];}

  virtual const  STensor3 &getConstRefTodElecConductivitydv() const {return  getConstRefTodLinearKdField()[1][1][1];}
  virtual  STensor3 &getRefTodElecConductivitydv() {return  getRefTodLinearKdField()[1][1][1];}

  virtual const  STensor3 &getConstRefTodCrossElecConductivitydv() const {return  getConstRefTodLinearKdField()[1][0][1];}
  virtual  STensor3 &getRefTodCrossElecConductivitydv() {return  getRefTodLinearKdField()[1][0][1];}

  virtual const  STensor43 &getConstRefTodElecConductivitydF() const {return  getConstRefTodLinearKdF()[1][1];}
  virtual  STensor43 &getRefTodElecConductivitydF() {return  getRefTodLinearKdF()[1][1];}

  virtual const  STensor43 &getConstRefTodCrossElecConductivitydF() const {return  getConstRefTodLinearKdF()[1][0];}
  virtual  STensor43 &getRefTodCrossElecConductivitydF() {return  getRefTodLinearKdF()[1][0];}

  virtual const  STensor43 &getConstRefTodTherConductivitydF() const {return  getConstRefTodLinearKdF()[0][0];}
  virtual  STensor43 &getRefTodTherConductivitydF() {return  getRefTodLinearKdF()[0][0];}

  virtual const  STensor43 &getConstRefTodCrossTherConductivitydF() const {return  getConstRefTodLinearKdF()[0][1];}
  virtual  STensor43 &getRefTodCrossTherConductivitydF() {return  getRefTodLinearKdF()[0][1];}

  virtual const double getConstRefTofvJump() const {return getConstRefToEnergyConjugatedFieldJump()(1);}
  virtual double &getRefTofvJump() {return  getRefToEnergyConjugatedFieldJump()(1);}

  virtual const double getConstRefTofTJump() const {return getConstRefToEnergyConjugatedFieldJump()(0);}
  virtual double &getRefTofTJump() {return  getRefToEnergyConjugatedFieldJump()(0);}

  virtual const double getConstRefToOneOverTemperatureJump() const {return getConstRefToEnergyConjugatedFieldJump()(0);}
  virtual double &getRefToOneOverTemperatureJump() {return  getRefToEnergyConjugatedFieldJump()(0);}

  virtual const double getConstRefTodOneOverTemperatureJumpdTm() const {return getConstRefTodEnergyConjugatedFieldJumpdFieldm()[0][0];}
  virtual double &getRefTodOneOverTemperatureJumpdTm() {return  getRefTodEnergyConjugatedFieldJumpdFieldm()[0][0];}

  virtual const double getConstRefTodOneOverTemperatureJumpdTp() const {return getConstRefTodEnergyConjugatedFieldJumpdFieldp()[0][0];}
  virtual double &getRefTodOneOverTemperatureJumpdTp() {return  getRefTodEnergyConjugatedFieldJumpdFieldp()[0][0];}

  virtual const double &getConstRefTodfvjumpdvm() const {return getConstRefTodEnergyConjugatedFieldJumpdFieldm()[1][1];}
  virtual double &getRefTodfvjumpdvm() {return  getRefTodEnergyConjugatedFieldJumpdFieldm()[1][1];}

  virtual const double &getConstRefTodfvjumpdvp() const {return getConstRefTodEnergyConjugatedFieldJumpdFieldp()[1][1];}
  virtual double &getRefTodfvjumpdvp() {return  getRefTodEnergyConjugatedFieldJumpdFieldp()[1][1];}

  virtual const double &getConstRefTodfvjumpdTm() const {return getConstRefTodEnergyConjugatedFieldJumpdFieldm()[1][0];}
  virtual double &getRefTodfvjumpdTm() {return  getRefTodEnergyConjugatedFieldJumpdFieldm()[1][0];}

  virtual const double &getConstRefTodfvjumpdTp() const {return getConstRefTodEnergyConjugatedFieldJumpdFieldp()[1][0];}
  virtual double &getRefTodfvjumpdTp() {return  getRefTodEnergyConjugatedFieldJumpdFieldp()[1][0];}

  virtual const double getConstRefToTemperatureJump() const {return getConstRefToFieldJump()(0);}
  virtual double &getRefToTemperatureJump() {return  getRefToFieldJump()(0);}

  virtual const SVector3 &getConstRefTointerfaceFluxT() const {return getConstRefToInterfaceFlux()[0];}   // i didn't use it
  virtual SVector3 &getRefTointerfaceFluxT() {return getRefToInterfaceFlux()[0];}

  // for V
  virtual const double getConstRefToVoltage() const {return  getConstRefToField(1);}
  virtual double &getRefToVoltage() {return  getRefToField(1);}

  virtual const SVector3 &getConstRefToGradV() const {return getConstRefToGradField()[1];}
  virtual SVector3 &getRefToGradV() {return getRefToGradField()[1];}

  virtual const SVector3 &getConstRefToFluxV() const {return getConstRefToFlux()[1];}
  virtual SVector3 &getRefToFluxV() {return getRefToFlux()[1];}

  virtual const  STensor3 &getConstRefTodPdV() const {return getConstRefTodPdField()[1];}
  virtual  STensor3 &getRefTodPdV() {return getRefTodPdField()[1];}

  virtual const  STensor3 &getConstRefTodFluxVdGradV() const {return  getConstRefTodFluxdGradField()[1][1];}//dedgradV
  virtual  STensor3 &getRefTodFluxVdGradV() {return  getRefTodFluxdGradField()[1][1];}

  virtual const  STensor3 &getConstRefTodFluxVdGradT() const {return  getConstRefTodFluxdGradField()[1][0];}//dedgradT
  virtual  STensor3 &getRefTodFluxVdGradT() {return  getRefTodFluxdGradField()[1][0];}

  virtual const  SVector3 &getConstRefTodFluxVdV() const {return  getConstRefTodFluxdField()[1][1];}//dedv
  virtual  SVector3 &getRefTodFluxVdV() {return  getRefTodFluxdField()[1][1];}

  virtual const  SVector3 &getConstRefTodFluxVdT() const {return  getConstRefTodFluxdField()[1][0];}//dedv
  virtual  SVector3 &getRefTodFluxVdT() {return  getRefTodFluxdField()[1][0];}

  virtual const  STensor33 &getConstRefTodFluxVdF() const {return  getConstRefTodFluxdF()[1];}//dedf
  virtual  STensor33 &getRefTodFluxVdF() {return  getRefTodFluxdF()[1];}

 /* virtual const STensor3 &getConstRefTolinearl10() const {return *linearl10;}
  virtual void setRefTolinearl10(const STensor3 &eV1) {linearl10=&eV1;}

  virtual const STensor3 &getConstRefTolinearl20() const {return *linearl20;}
  virtual void setRefTolinearl20(const STensor3 &eV2) {linearl20=&eV2;}*/

  virtual const double getConstRefToVoltageJump() const {return getConstRefToFieldJump()(1);}
  virtual double &getRefToVoltageJump() {return  getRefToFieldJump()(1);}

  virtual const SVector3 &getConstRefTointerfaceFluxV() const {return getConstRefToInterfaceFlux()[1];}  // i didn't use it
  virtual SVector3 &getRefTointerfaceFluxV() {return getRefToInterfaceFlux()[1];}

  virtual const double getConstRefToThermalSource() const {return getConstRefToFieldSource()(0);}
  virtual double &getRefToThermalSource() {return  getRefToFieldSource()(0);}

  virtual const double getConstRefTodThermalSourcedT() const {return getConstRefTodFieldSourcedField()(0,0);}
  virtual double &getRefTodThermalSourcedT() {return  getRefTodFieldSourcedField()(0,0);}

  virtual const double getConstRefTodThermalSourcedV() const {return getConstRefTodFieldSourcedField()(0,1);}
  virtual double &getRefTodThermalSourcedV() {return  getRefTodFieldSourcedField()(0,1);}

  virtual const  STensor3 &getConstRefTodThermalSourcedF() const {return getConstRefTodFieldSourcedF()[0];}
  virtual  STensor3 &getRefTodThermalSourcedF() {return getRefTodFieldSourcedF()[0];}

 /* virtual const  SVector3 &getConstRefTodThermalSourcedGradV() const {return  dThermalSourcedGradV;}//dwdgradV
  virtual  SVector3 &getRefTodThermalSourcedGradV() {return  dThermalSourcedGradV;}

  virtual const  SVector3 &getConstRefTodThermalSourcedGradT() const {return  dThermalSourcedGradT;}//dwdgradT
  virtual  SVector3 &getRefTodThermalSourcedGradT() {return  dThermalSourcedGradT;}*/

  virtual const double getConstRefToVoltageSource() const {return getConstRefToFieldSource()(1);}
  virtual double &getRefToVoltageSource() {return  getRefToFieldSource()(1);}

  virtual const double getConstRefTodVoltageSourcedT() const {return getConstRefTodFieldSourcedField()(1,0);}
  virtual double &getRefTodVoltageSourcedT() {return  getRefTodFieldSourcedField()(1,0);}

  virtual const double getConstRefTodVoltageSourcedV() const {return getConstRefTodFieldSourcedField()(1,1);}
  virtual double &getRefTodVoltageSourcedV() {return  getRefTodFieldSourcedField()(1,1);}

  virtual const  STensor3 &getConstRefTodVoltageSourcedF() const {return getConstRefTodFieldSourcedF()[1];}
  virtual  STensor3 &getRefTodVoltageSourcedF() {return getRefTodFieldSourcedF()[1];}

  virtual const double getConstRefTofT() const {return  getConstRefToEnergyConjugatedField(0);}
  virtual double &getRefTofT() {return getRefToEnergyConjugatedField(0);}

  virtual const SVector3 &getConstRefToGradfT() const {return getConstRefToGradEnergyConjugatedField()[0];}
  virtual SVector3 &getRefToGradfT() {return getRefToGradEnergyConjugatedField()[0];}

  virtual const double getConstRefTofV() const {return  getConstRefToEnergyConjugatedField(1);}
  virtual double &getRefTofV() {return  getRefToEnergyConjugatedField(1);}

  virtual const SVector3 &getConstRefToGradfV() const {return getConstRefToGradEnergyConjugatedField()[1];}
  virtual SVector3 &getRefToGradfV() {return getRefToGradEnergyConjugatedField()[1];}

  virtual const STensor3 &getConstRefTodgradVdgradfV() const {return getConstRefTodGradFielddGradEnergyConjugatedField()[1][1];}
  virtual STensor3 &getRefTodgradVdgradfV() {return  getRefTodGradFielddGradEnergyConjugatedField()[1][1];}

  virtual const STensor3 &getConstRefTodgradVdgradfT() const {return getConstRefTodGradFielddGradEnergyConjugatedField()[1][0];}
  virtual STensor3 &getRefTodgradVdgradfT() {return  getRefTodGradFielddGradEnergyConjugatedField()[1][0];}

  virtual const STensor3 &getConstRefTodgradTdgradfT() const {return getConstRefTodGradFielddGradEnergyConjugatedField()[0][0];}
  virtual STensor3 &getRefTodgradTdgradfT() {return  getRefTodGradFielddGradEnergyConjugatedField()[0][0];}

  virtual const STensor3 &getConstRefTodgradTdgradfV() const {return getConstRefTodGradFielddGradEnergyConjugatedField()[0][1];}
  virtual STensor3 &getRefTodgradTdgradfV() {return  getRefTodGradFielddGradEnergyConjugatedField()[0][1];}

  virtual const double &getConstRefTodVdfV() const {return getConstRefTodFielddEnergyConjugatedField()[1][1];}
  virtual double &getRefTodVdfV() {return  getRefTodFielddEnergyConjugatedField()[1][1];}

  virtual const double &getConstRefTodVdfT() const {return getConstRefTodFielddEnergyConjugatedField()[1][0];}
  virtual double &getRefTodVdfT() {return  getRefTodFielddEnergyConjugatedField()[1][0];}

   virtual const double &getConstRefTodTdfT() const {return getConstRefTodFielddEnergyConjugatedField()[0][0];}
  virtual double &getRefTodTdfT() {return  getRefTodFielddEnergyConjugatedField()[0][0];}

  virtual const double &getConstRefTodTdfV() const {return getConstRefTodFielddEnergyConjugatedField()[0][1];}
  virtual double &getRefTodTdfV() {return  getRefTodFielddEnergyConjugatedField()[0][1];}

    virtual const SVector3  &getConstRefTodgradVdfV() const {return getConstRefTodGradFielddEnergyConjugatedField()[1][1];}
  virtual SVector3  &getRefTodgradVdfV() {return  getRefTodGradFielddEnergyConjugatedField()[1][1];}

  virtual const SVector3  &getConstRefTodgradVdfT() const {return getConstRefTodGradFielddEnergyConjugatedField()[1][0];}
  virtual SVector3  &getRefTodgradVdfT() {return  getRefTodGradFielddEnergyConjugatedField()[1][0];}

   virtual const SVector3  &getConstRefTodgradTdfT() const {return getConstRefTodGradFielddEnergyConjugatedField()[0][0];}
  virtual SVector3  &getRefTodgradTdfT() {return  getRefTodGradFielddEnergyConjugatedField()[0][0];}

  virtual const SVector3  &getConstRefTodgradTdfV() const {return getConstRefTodGradFielddEnergyConjugatedField()[0][1];}
  virtual SVector3  &getRefTodgradTdfV() {return  getRefTodGradFielddEnergyConjugatedField()[0][1];}

   virtual const SVector3  &getConstRefToFluxEnergy() const {return getConstRefToFlux()[0];}
   virtual SVector3 &getRefToFluxEnergy(){return  getRefToFlux()[0];}

   virtual const SVector3  &getConstRefTodFluxEnergydV() const {return getConstRefTodFluxdField()[0][1];}
   virtual SVector3 &getRefTodFluxEnergydV(){return  getRefTodFluxdField()[0][1];}

   virtual const STensor3 &getConstRefTodFluxEnergydGradV() const {return getConstRefTodFluxdGradField()[0][1];}
   virtual STensor3 &getRefTodFluxEnergydGradV(){return  getRefTodFluxdGradField()[0][1];}

   virtual const SVector3  &getConstRefTodFluxEnergydT() const {return getConstRefTodFluxdField()[0][0];}
   virtual SVector3 &getRefTodFluxEnergydT(){return  getRefTodFluxdField()[0][0];}

   virtual const STensor3 &getConstRefTodFluxEnergydGradT() const {return getConstRefTodFluxdGradField()[0][0];}
   virtual STensor3 &getRefTodFluxEnergydGradT(){return  getRefTodFluxdGradField()[0][0];}

   virtual const STensor33 &getConstRefTodFluxEnergydF() const {return getConstRefTodFluxdF()[0];}
   virtual STensor33 &getRefTodFluxEnergydF(){return  getRefTodFluxdF()[0];}

 /*  virtual const STensor3 *getConstRefToEnergyConductivity() const {return getConstRefToEnergyK()[0][0];}
   virtual void setRefToEnergyConductivity(const STensor3 &eT){return  setConstRefToEnergyK(eT,0,0);}

   virtual const STensor3 &getConstRefTodEnergyConductivitydT() const {return getConstRefTodEnergyKdField()[0][0][0];}
   virtual STensor3 &getRefTodEnergyConductivitydT(){return  getRefTodEnergyKdField()[0][0][0];}

   virtual const STensor3 &getConstRefTodEnergyConductivitydV() const {return getConstRefTodEnergyKdField()[0][0][1];}
   virtual STensor3 &getRefTodEnergyConductivitydV(){return getRefTodEnergyKdField()[0][0][1];}

   virtual const STensor43 &getConstRefTodEnergyConductivitydF() const {return getConstRefTodEnergyKdF()[0][0];}
   virtual STensor43 &getRefTodEnergyConductivitydF(){return getRefTodEnergyKdF()[0][0];}*/

   virtual const STensor3 &getConstRefTodFluxEnergydGradVdT() const {return getConstRefTodFluxdGradFielddField()[0][1][0];}
   virtual STensor3 &getRefTodFluxEnergydGradVdT(){return  getRefTodFluxdGradFielddField()[0][1][0];}

   virtual const STensor3 &getConstRefTodFluxEnergydGradVdV() const {return getConstRefTodFluxdGradFielddField()[0][1][1];}
   virtual STensor3 &getRefTodFluxEnergydGradVdV(){return getRefTodFluxdGradFielddField()[0][1][1];}

   virtual const STensor3 &getConstRefTodFluxEnergydGradTdT() const {return getConstRefTodFluxdGradFielddField()[0][0][0];}
   virtual STensor3 &getRefTodFluxEnergydGradTdT(){return  getRefTodFluxdGradFielddField()[0][0][0];}

   virtual const STensor3 &getConstRefTodFluxEnergydGradTdV() const {return getConstRefTodFluxdGradFielddField()[0][0][1];}
   virtual STensor3 &getRefTodFluxEnergydGradTdV(){return getRefTodFluxdGradFielddField()[0][0][1];}

   virtual const STensor43 &getConstRefTodFluxEnergydGradTdF() const {return getConstRefTodFluxdGradFielddF()[0][0];}
   virtual STensor43 &getRefTodFluxEnergydGradTdF(){return getRefTodFluxdGradFielddF()[0][0];}

   virtual const STensor43 &getConstRefTodFluxEnergydGradVdF() const {return getConstRefTodFluxdGradFielddF()[0][1];}
   virtual STensor43 &getRefTodFluxEnergydGradVdF(){return getRefTodFluxdGradFielddF()[0][1];}

   virtual const STensor3 *getConstRefToStiff_alphaDilatation() const {return getConstRefToLinearSymmetrizationCoupling()[0];}
   virtual void setRefToStiff_alphaDilatation(const STensor3 &eT) {setConstRefToLinearSymmetrizationCoupling(eT,0);}

   virtual IPVariable* clone() const =0;
   virtual void restart();
};


class LinearElecTherMechDG3DIPVariable : public ElecTherMechDG3DIPVariableBase
{

 protected:
  IPLinearElecTherMech _linearETMIP;


public:
  LinearElecTherMechDG3DIPVariable(const bool createBodyForceHO, const bool oninter):ElecTherMechDG3DIPVariableBase(createBodyForceHO,oninter){Msg::Error("LinearElecTherMechDG3DIPVariable: Cannot use the default constructor");}
  LinearElecTherMechDG3DIPVariable(double tinitial,double vinitial, const bool createBodyForceHO, const bool oninter);
  LinearElecTherMechDG3DIPVariable(const LinearElecTherMechDG3DIPVariable &source);
  LinearElecTherMechDG3DIPVariable& operator=(const IPVariable &source);

  virtual void setLocation(const IPVariable::LOCATION loc) {
    ElecTherMechDG3DIPVariableBase::setLocation(loc);
    _linearETMIP.setLocation(loc);
  };

  virtual IPVariable* getInternalData() {return &_linearETMIP;};
  virtual const IPVariable* getInternalData()  const {return &_linearETMIP;};


  IPLinearElecTherMech* getIPLinearElecTherMech(){return &_linearETMIP;}
  const IPLinearElecTherMech* getIPLinearElecTherMech() const{return &_linearETMIP;}
  virtual ~LinearElecTherMechDG3DIPVariable()
  {
  }
  virtual double get(const int i) const;
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const;
  virtual double damageEnergy() const {return _linearETMIP.damageEnergy();}
  virtual IPVariable* clone() const {return new LinearElecTherMechDG3DIPVariable(*this);};
  virtual void restart();
};



// ElectroMagTherMech IPVariable Base class
class ElecMagTherMechDG3DIPVariableBase : public dG3DIPVariable
{
    public:
    ElecMagTherMechDG3DIPVariableBase(const bool createBodyForceHO, const bool oninter);
    ElecMagTherMechDG3DIPVariableBase(const  ElecMagTherMechDG3DIPVariableBase &source);
    ElecMagTherMechDG3DIPVariableBase& operator=(const IPVariable &source);

    virtual ~ElecMagTherMechDG3DIPVariableBase()
    {}

    virtual double defoEnergy() const=0;
    virtual double plasticEnergy() const=0;
    virtual double damageEnergy() const = 0;
    virtual double get(const int i) const;
    virtual double & getRef(const int i);

    // T
    virtual const double getConstRefToTemperature() const {return getConstRefToField(0);}
    virtual double &getRefToTemperature() {return getRefToField(0);}

    // GradT
    virtual const SVector3 &getConstRefToGradT() const {return getConstRefToGradField()[0];}
    virtual SVector3 &getRefToGradT() {return getRefToGradField()[0];}

    //dPdT
    virtual  const STensor3 &getConstRefTodPdT() const {return getConstRefTodPdField()[0];}
    virtual  STensor3 &getRefTodPdT() {return getRefTodPdField()[0];}

    // FluxT = q
    virtual const SVector3 &getConstRefToFluxT() const
    {
        return getConstRefToFlux()[0];
    }
    virtual SVector3 &getRefToFluxT()
    {
        return getRefToFlux()[0];
    }

    // dFluxTdV
    virtual const  SVector3 &getConstRefTodFluxTdV() const {return  getConstRefTodFluxdField()[0][1];}
    virtual  SVector3 &getRefTodFluxTdV() {return  getRefTodFluxdField()[0][1];}

    // dFluxTdGradV
    virtual const  STensor3 &getConstRefTodFluxTdGradV() const {return  getConstRefTodFluxdGradField()[0][1];}//dqdgradV,
    virtual  STensor3 &getRefTodFluxTdGradV() {return  getRefTodFluxdGradField()[0][1];}

    // dFluxTdT
    virtual const SVector3 & getConstRefTodFluxTdT() const
    {
        return getConstRefTodFluxdField()[0][0];
    }
    virtual SVector3 & getRefTodFluxTdT()
    {
        return getRefTodFluxdField()[0][0];
    }

    // dFluxTdGradT
    virtual const STensor3 & getConstRefTodFluxTdGradT() const
    {
        return getConstRefTodFluxdGradField()[0][0];
    }
    virtual STensor3 & getRefTodFluxTdGradT()
    {
        return getRefTodFluxdGradField()[0][0];
    }

    // dFluxTdF
    virtual const STensor33 & getConstRefTodFluxTdF() const
    {
        return getConstRefTodFluxdF()[0];
    }
    virtual STensor33 & getRefTodFluxTdF()
    {
        return getRefTodFluxdF()[0];
    }

    // dElecDisplacementdT
    virtual const SVector3 &getConstRefTodElecDisplacementdT() const
    {
        return getConstRefTodElecDisplacementdField()[1][0];
    }
    virtual SVector3 &getRefTodElecDisplacementdT()
    {
        return getRefTodElecDisplacementdField()[1][0];
    }

    // dElecDisplacementdGradT
    virtual const STensor3 &getConstRefTodElecDisplacementdGradT() const
    {
        return getConstRefTodElecDisplacementdGradField()[1][0];
    }
    virtual STensor3 &getRefTodElecDisplacementdGradT()
    {
        return getRefTodElecDisplacementdGradField()[1][0];
    }

    // dElecDisplacementdV
    virtual const SVector3 &getConstRefTodElecDisplacementdV() const
    {
        return getConstRefTodElecDisplacementdField()[1][1];
    }
    virtual SVector3 &getRefTodElecDisplacementdV()
    {
        return getRefTodElecDisplacementdField()[1][1];
    }

    // dElecDisplacementdGradV
    virtual const STensor3 &getConstRefTodElecDisplacementdGradV() const
    {
        return getConstRefTodElecDisplacementdGradField()[1][1];
    }
    virtual STensor3 &getRefTodElecDisplacementdGradV()
    {
        return getRefTodElecDisplacementdGradField()[1][1];
    }

    // dElecDisplacementdF
    virtual const STensor33 &getConstRefTodElecDispldF() const
    {
        return getConstRefTodElecDisplacementdF()[1];
    }
    virtual STensor33 &getRefTodElecDispldF()
    {
        return getRefTodElecDisplacementdF()[1];
    }

    // ElecCond
    virtual const  STensor3 *getConstRefToElecConductivity() const {return getConstRefToLinearK()[1][1];}
    virtual void setRefToElecConductivity(const STensor3 &eT) {return  setConstRefToLinearK(eT,1,1);}

    // CrossElecCond
    virtual const  STensor3 *getConstRefToCrossElecConductivity() const {return getConstRefToLinearK()[1][0];}
    virtual void setRefToCrossElecConductivity(const STensor3 &eT) {return  setConstRefToLinearK(eT,1,0);}

    // TherCond
    virtual const  STensor3 *getConstRefToTherConductivity() const {return getConstRefToLinearK()[0][0];}
    virtual void setRefToTherConductivity(const STensor3 &eT) {return  setConstRefToLinearK(eT,0,0);}

    // CrossTherCond
    virtual const  STensor3 *getConstRefToCrossTherConductivity() const {return getConstRefToLinearK()[0][1];}
    virtual void setRefToCrossTherConductivity(const STensor3 &eT) {return  setConstRefToLinearK(eT,0,1);}

    // dElecConddT
    virtual const  STensor3 &getConstRefTodElecConductivitydT() const {return  getConstRefTodLinearKdField()[1][1][0];}
    virtual  STensor3 &getRefTodElecConductivitydT() {return  getRefTodLinearKdField()[1][1][0];}

    // dCrossElecConddT
    virtual const  STensor3 &getConstRefTodCrossElecConductivitydT() const {return  getConstRefTodLinearKdField()[1][0][0];}
    virtual  STensor3 &getRefTodCrossElecConductivitydT() {return  getRefTodLinearKdField()[1][0][0];}

    // dTherConddT
    virtual const  STensor3 &getConstRefTodTherConductivitydT() const {return  getConstRefTodLinearKdField()[0][0][0];}
    virtual  STensor3 &getRefTodTherConductivitydT() {return  getRefTodLinearKdField()[0][0][0];}

    // dCrossTherConddT
    virtual const  STensor3 &getConstRefTodCrossTherConductivitydT() const {return  getConstRefTodLinearKdField()[0][1][0];}
    virtual  STensor3 &getRefTodCrossTherConductivitydT() {return  getRefTodLinearKdField()[0][1][0];}

    // dTherConddv
    virtual const  STensor3 &getConstRefTodTherConductivitydv() const {return  getConstRefTodLinearKdField()[0][0][1];}
    virtual  STensor3 &getRefTodTherConductivitydv() {return  getRefTodLinearKdField()[0][0][1];}

    // dCrossTherConddv
    virtual const  STensor3 &getConstRefTodCrossTherConductivitydv() const {return  getConstRefTodLinearKdField()[0][1][1];}
    virtual  STensor3 &getRefTodCrossTherConductivitydv() {return  getRefTodLinearKdField()[0][1][1];}

    // dElecConddv
    virtual const  STensor3 &getConstRefTodElecConductivitydv() const {return  getConstRefTodLinearKdField()[1][1][1];}
    virtual  STensor3 &getRefTodElecConductivitydv() {return  getRefTodLinearKdField()[1][1][1];}

    // dCrossElecConddv
    virtual const  STensor3 &getConstRefTodCrossElecConductivitydv() const {return  getConstRefTodLinearKdField()[1][0][1];}
    virtual  STensor3 &getRefTodCrossElecConductivitydv() {return  getRefTodLinearKdField()[1][0][1];}

    // dElecConddF
    virtual const  STensor43 &getConstRefTodElecConductivitydF() const {return  getConstRefTodLinearKdF()[1][1];}
    virtual  STensor43 &getRefTodElecConductivitydF() {return  getRefTodLinearKdF()[1][1];}

    // dCrossElecConddF
    virtual const  STensor43 &getConstRefTodCrossElecConductivitydF() const {return  getConstRefTodLinearKdF()[1][0];}
    virtual  STensor43 &getRefTodCrossElecConductivitydF() {return  getRefTodLinearKdF()[1][0];}

    // dTherConddF
    virtual const  STensor43 &getConstRefTodTherConductivitydF() const {return  getConstRefTodLinearKdF()[0][0];}
    virtual  STensor43 &getRefTodTherConductivitydF() {return  getRefTodLinearKdF()[0][0];}

    // dCrossTherConddF
    virtual const  STensor43 &getConstRefTodCrossTherConductivitydF() const {return  getConstRefTodLinearKdF()[0][1];}
    virtual  STensor43 &getRefTodCrossTherConductivitydF() {return  getRefTodLinearKdF()[0][1];}

    // fvJump
    virtual const double getConstRefTofvJump() const {return getConstRefToEnergyConjugatedFieldJump()(1);}
    virtual double &getRefTofvJump() {return  getRefToEnergyConjugatedFieldJump()(1);}

    // ftJump
    virtual const double getConstRefTofTJump() const {return getConstRefToEnergyConjugatedFieldJump()(0);}
    virtual double &getRefTofTJump() {return  getRefToEnergyConjugatedFieldJump()(0);}

    // 1/T Jump
    virtual const double getConstRefToOneOverTemperatureJump() const {return getConstRefToEnergyConjugatedFieldJump()(0);}
    virtual double &getRefToOneOverTemperatureJump() {return  getRefToEnergyConjugatedFieldJump()(0);}

    // d1/TJumpdTm
    virtual const double getConstRefTodOneOverTemperatureJumpdTm() const {return getConstRefTodEnergyConjugatedFieldJumpdFieldm()[0][0];}
    virtual double &getRefTodOneOverTemperatureJumpdTm() {return  getRefTodEnergyConjugatedFieldJumpdFieldm()[0][0];}

    // d1TJumpdTp
    virtual const double getConstRefTodOneOverTemperatureJumpdTp() const {return getConstRefTodEnergyConjugatedFieldJumpdFieldp()[0][0];}
    virtual double &getRefTodOneOverTemperatureJumpdTp() {return  getRefTodEnergyConjugatedFieldJumpdFieldp()[0][0];}

    // dfVJumpdVm
    virtual const double &getConstRefTodfvjumpdvm() const {return getConstRefTodEnergyConjugatedFieldJumpdFieldm()[1][1];}
    virtual double &getRefTodfvjumpdvm() {return  getRefTodEnergyConjugatedFieldJumpdFieldm()[1][1];}

    // dfVJumpdVp
    virtual const double &getConstRefTodfvjumpdvp() const {return getConstRefTodEnergyConjugatedFieldJumpdFieldp()[1][1];}
    virtual double &getRefTodfvjumpdvp() {return  getRefTodEnergyConjugatedFieldJumpdFieldp()[1][1];}

    // dfVJumpdTm
    virtual const double &getConstRefTodfvjumpdTm() const {return getConstRefTodEnergyConjugatedFieldJumpdFieldm()[1][0];}
    virtual double &getRefTodfvjumpdTm() {return  getRefTodEnergyConjugatedFieldJumpdFieldm()[1][0];}

    // dfVJumpdTp
    virtual const double &getConstRefTodfvjumpdTp() const {return getConstRefTodEnergyConjugatedFieldJumpdFieldp()[1][0];}
    virtual double &getRefTodfvjumpdTp() {return  getRefTodEnergyConjugatedFieldJumpdFieldp()[1][0];}

    // TJump
    virtual const double getConstRefToTemperatureJump() const {return getConstRefToFieldJump()(0);}
    virtual double &getRefToTemperatureJump() {return  getRefToFieldJump()(0);}

    // interfaceFluxT
    virtual const SVector3 &getConstRefTointerfaceFluxT() const {return getConstRefToInterfaceFlux()[0];}   // i didn't use it
    virtual SVector3 &getRefTointerfaceFluxT() {return getRefToInterfaceFlux()[0];}

    // for V
    virtual const double getConstRefToVoltage() const {return  getConstRefToField(1);}
    virtual double &getRefToVoltage() {return  getRefToField(1);}

    // GradV
    virtual const SVector3 &getConstRefToGradV() const {return getConstRefToGradField()[1];}
    virtual SVector3 &getRefToGradV() {return getRefToGradField()[1];}

    // Fluxje
    virtual const SVector3 &getConstRefToFluxje() const {return getConstRefToFlux()[1];}
    virtual SVector3 &getRefToFluxje() {return getRefToFlux()[1];}

    // dPdV
    virtual const  STensor3 &getConstRefTodPdV() const {return getConstRefTodPdField()[1];}
    virtual  STensor3 &getRefTodPdV() {return getRefTodPdField()[1];}

    // dPdGradV
    virtual const STensor33 &getConstRefTodPdGradV() const {return getConstRefTodPdGradField()[1];}
    virtual STensor33 &getRefTodPdGradV() {return getRefTodPdGradField()[1];}

    // dFluxjedGradV
    virtual const  STensor3 &getConstRefTodFluxjedGradV() const {return  getConstRefTodFluxdGradField()[1][1];}//dedgradV
    virtual  STensor3 &getRefTodFluxjedGradV() {return  getRefTodFluxdGradField()[1][1];}

    // dFluxjedGradT
    virtual const  STensor3 &getConstRefTodFluxjedGradT() const {return  getConstRefTodFluxdGradField()[1][0];}//dedgradT
    virtual  STensor3 &getRefTodFluxjedGradT() {return  getRefTodFluxdGradField()[1][0];}

    // dFluxjedV
    virtual const  SVector3 &getConstRefTodFluxjedV() const {return  getConstRefTodFluxdField()[1][1];}//dedv
    virtual  SVector3 &getRefTodFluxjedV() {return  getRefTodFluxdField()[1][1];}

    // dFluxjedT
    virtual const  SVector3 &getConstRefTodFluxjedT() const {return  getConstRefTodFluxdField()[1][0];}//dedv
    virtual  SVector3 &getRefTodFluxjedT() {return  getRefTodFluxdField()[1][0];}

    // dFluxjedF
    virtual const  STensor33 &getConstRefTodFluxjedF() const {return  getConstRefTodFluxdF()[1];}//dedf
    virtual  STensor33 &getRefTodFluxjedF() {return  getRefTodFluxdF()[1];}

    // VJump
    virtual const double getConstRefToVoltageJump() const {return getConstRefToFieldJump()(1);}
    virtual double &getRefToVoltageJump() {return  getRefToFieldJump()(1);}

    // interfaceFluxje
    virtual const SVector3 &getConstRefTointerfaceFluxje() const {return getConstRefToInterfaceFlux()[1];}  // i didn't use it
    virtual SVector3 &getRefTointerfaceFluxje() {return getRefToInterfaceFlux()[1];}

    // ThermalSource
    virtual const double getConstRefToThermalSource() const {return getConstRefToFieldSource()(0);}
    virtual double &getRefToThermalSource() {return  getRefToFieldSource()(0);}

    // dThermalSourcedT
    virtual const double getConstRefTodThermalSourcedT() const {return getConstRefTodFieldSourcedField()(0,0);}
    virtual double &getRefTodThermalSourcedT() {return  getRefTodFieldSourcedField()(0,0);}

    // dThermalSourcedV
    virtual const double getConstRefTodThermalSourcedV() const {return getConstRefTodFieldSourcedField()(0,1);}
    virtual double &getRefTodThermalSourcedV() {return  getRefTodFieldSourcedField()(0,1);}

    // dThermalSourcedF
    virtual const  STensor3 &getConstRefTodThermalSourcedF() const {return getConstRefTodFieldSourcedF()[0];}
    virtual  STensor3 &getRefTodThermalSourcedF() {return getRefTodFieldSourcedF()[0];}

    // VoltageSource
    virtual const double getConstRefToVoltageSource() const {return getConstRefToFieldSource()(1);}
    virtual double &getRefToVoltageSource() {return  getRefToFieldSource()(1);}

    virtual const double getConstRefTodVoltageSourcedT() const {return getConstRefTodFieldSourcedField()(1,0);}
    virtual double &getRefTodVoltageSourcedT() {return  getRefTodFieldSourcedField()(1,0);}

    virtual const double getConstRefTodVoltageSourcedV() const {return getConstRefTodFieldSourcedField()(1,1);}
    virtual double &getRefTodVoltageSourcedV() {return  getRefTodFieldSourcedField()(1,1);}

    virtual const  STensor3 &getConstRefTodVoltageSourcedF() const {return getConstRefTodFieldSourcedF()[1];}
    virtual  STensor3 &getRefTodVoltageSourcedF() {return getRefTodFieldSourcedF()[1];}


    // fT
    virtual const double getConstRefTofT() const {return  getConstRefToEnergyConjugatedField(0);}
    virtual double &getRefTofT() {return getRefToEnergyConjugatedField(0);}

    // GradfT
    virtual const SVector3 &getConstRefToGradfT() const {return getConstRefToGradEnergyConjugatedField()[0];}
    virtual SVector3 &getRefToGradfT() {return getRefToGradEnergyConjugatedField()[0];}

    // fV
    virtual const double getConstRefTofV() const {return  getConstRefToEnergyConjugatedField(1);}
    virtual double &getRefTofV() {return  getRefToEnergyConjugatedField(1);}

    // GradfV
    virtual const SVector3 &getConstRefToGradfV() const {return getConstRefToGradEnergyConjugatedField()[1];}
    virtual SVector3 &getRefToGradfV() {return getRefToGradEnergyConjugatedField()[1];}

    // dGradVdGradfV
    virtual const STensor3 &getConstRefTodgradVdgradfV() const {return getConstRefTodGradFielddGradEnergyConjugatedField()[1][1];}
    virtual STensor3 &getRefTodgradVdgradfV() {return  getRefTodGradFielddGradEnergyConjugatedField()[1][1];}

    // dGradVdGradfT
    virtual const STensor3 &getConstRefTodgradVdgradfT() const {return getConstRefTodGradFielddGradEnergyConjugatedField()[1][0];}
    virtual STensor3 &getRefTodgradVdgradfT() {return  getRefTodGradFielddGradEnergyConjugatedField()[1][0];}

    // dGradTdGradfT
    virtual const STensor3 &getConstRefTodgradTdgradfT() const {return getConstRefTodGradFielddGradEnergyConjugatedField()[0][0];}
    virtual STensor3 &getRefTodgradTdgradfT() {return  getRefTodGradFielddGradEnergyConjugatedField()[0][0];}

    // dGradTdGradfV
    virtual const STensor3 &getConstRefTodgradTdgradfV() const {return getConstRefTodGradFielddGradEnergyConjugatedField()[0][1];}
    virtual STensor3 &getRefTodgradTdgradfV() {return  getRefTodGradFielddGradEnergyConjugatedField()[0][1];}

    // dVdfV
    virtual const double &getConstRefTodVdfV() const {return getConstRefTodFielddEnergyConjugatedField()[1][1];}
    virtual double &getRefTodVdfV() {return  getRefTodFielddEnergyConjugatedField()[1][1];}

    // dVdfT
    virtual const double &getConstRefTodVdfT() const {return getConstRefTodFielddEnergyConjugatedField()[1][0];}
    virtual double &getRefTodVdfT() {return  getRefTodFielddEnergyConjugatedField()[1][0];}

    // dTdfT
    virtual const double &getConstRefTodTdfT() const {return getConstRefTodFielddEnergyConjugatedField()[0][0];}
    virtual double &getRefTodTdfT() {return  getRefTodFielddEnergyConjugatedField()[0][0];}

    // dTdfV
    virtual const double &getConstRefTodTdfV() const {return getConstRefTodFielddEnergyConjugatedField()[0][1];}
    virtual double &getRefTodTdfV() {return  getRefTodFielddEnergyConjugatedField()[0][1];}

    // dGradVdfV
    virtual const SVector3  &getConstRefTodgradVdfV() const {return getConstRefTodGradFielddEnergyConjugatedField()[1][1];}
    virtual SVector3  &getRefTodgradVdfV() {return  getRefTodGradFielddEnergyConjugatedField()[1][1];}

    // dGradVdfT
    virtual const SVector3  &getConstRefTodgradVdfT() const {return getConstRefTodGradFielddEnergyConjugatedField()[1][0];}
    virtual SVector3  &getRefTodgradVdfT() {return  getRefTodGradFielddEnergyConjugatedField()[1][0];}

    // dGradTdfT
    virtual const SVector3  &getConstRefTodgradTdfT() const {return getConstRefTodGradFielddEnergyConjugatedField()[0][0];}
    virtual SVector3  &getRefTodgradTdfT() {return  getRefTodGradFielddEnergyConjugatedField()[0][0];}

    // dGradTdfV
    virtual const SVector3  &getConstRefTodgradTdfV() const {return getConstRefTodGradFielddEnergyConjugatedField()[0][1];}
    virtual SVector3  &getRefTodgradTdfV() {return  getRefTodGradFielddEnergyConjugatedField()[0][1];}

    // FluxEnergy
    virtual const SVector3  &getConstRefToFluxEnergy() const {return getConstRefToFlux()[0];}
    virtual SVector3 &getRefToFluxEnergy(){return  getRefToFlux()[0];}

    // Electric displacement
    virtual const SVector3 &getConstRefToElecDisplacement() const{return getConstRefToElectricDisplacement()[1];}
    virtual SVector3 &getRefToElecDisplacement(){return getRefToElectricDisplacement()[1];}

    // dFluxEnergydV
    virtual const SVector3  &getConstRefTodFluxEnergydV() const {return getConstRefTodFluxdField()[0][1];}
    virtual SVector3 &getRefTodFluxEnergydV(){return  getRefTodFluxdField()[0][1];}

    // dFluxEnergydGradV
    virtual const STensor3 &getConstRefTodFluxEnergydGradV() const {return getConstRefTodFluxdGradField()[0][1];}
    virtual STensor3 &getRefTodFluxEnergydGradV(){return  getRefTodFluxdGradField()[0][1];}

    // dFluxEnergydT
    virtual const SVector3  &getConstRefTodFluxEnergydT() const {return getConstRefTodFluxdField()[0][0];}
    virtual SVector3 &getRefTodFluxEnergydT(){return  getRefTodFluxdField()[0][0];}

    // dFluxEnergydGradT
    virtual const STensor3 &getConstRefTodFluxEnergydGradT() const {return getConstRefTodFluxdGradField()[0][0];}
    virtual STensor3 &getRefTodFluxEnergydGradT(){return  getRefTodFluxdGradField()[0][0];}

    // dFluxEnergydF
    virtual const STensor33 &getConstRefTodFluxEnergydF() const {return getConstRefTodFluxdF()[0];}
    virtual STensor33 &getRefTodFluxEnergydF(){return  getRefTodFluxdF()[0];}

    // dFluxEnergydA
    virtual const STensor3 & getConstRefTodFluxEnergydMagneticVectorPotential() const {return getConstRefTodFluxdVectorPotential()[0][0];}
    virtual STensor3 & getRefTodFluxEnergydMagneticVectorPotential() {return getRefTodFluxdVectorPotential()[0][0];}

    // dFluxEnergydB
    virtual const STensor3 & getConstRefTodFluxEnergydMagneticVectorCurl() const {return getConstRefTodFluxdVectorCurl()[0][0];}
    virtual STensor3 & getRefTodFluxEnergydMagneticVectorCurl() {return getRefTodFluxdVectorCurl()[0][0];}

    // dFluxEnergydGradVdT
    virtual const STensor3 &getConstRefTodFluxEnergydGradVdT() const {return getConstRefTodFluxdGradFielddField()[0][1][0];}
    virtual STensor3 &getRefTodFluxEnergydGradVdT(){return  getRefTodFluxdGradFielddField()[0][1][0];}

    // dFluxEnergydGradVdV
    virtual const STensor3 &getConstRefTodFluxEnergydGradVdV() const {return getConstRefTodFluxdGradFielddField()[0][1][1];}
    virtual STensor3 &getRefTodFluxEnergydGradVdV(){return getRefTodFluxdGradFielddField()[0][1][1];}

    // dFluxEnergydGradTdT
    virtual const STensor3 &getConstRefTodFluxEnergydGradTdT() const {return getConstRefTodFluxdGradFielddField()[0][0][0];}
    virtual STensor3 &getRefTodFluxEnergydGradTdT(){return  getRefTodFluxdGradFielddField()[0][0][0];}

    // dFluxEnergydGradTdV
    virtual const STensor3 &getConstRefTodFluxEnergydGradTdV() const {return getConstRefTodFluxdGradFielddField()[0][0][1];}
    virtual STensor3 &getRefTodFluxEnergydGradTdV(){return getRefTodFluxdGradFielddField()[0][0][1];}

    // dFluxEnergydGradTdF
    virtual const STensor43 &getConstRefTodFluxEnergydGradTdF() const {return getConstRefTodFluxdGradFielddF()[0][0];}
    virtual STensor43 &getRefTodFluxEnergydGradTdF(){return getRefTodFluxdGradFielddF()[0][0];}

    // dFluxEnergydGradVdF
    virtual const STensor43 &getConstRefTodFluxEnergydGradVdF() const {return getConstRefTodFluxdGradFielddF()[0][1];}
    virtual STensor43 &getRefTodFluxEnergydGradVdF(){return getRefTodFluxdGradFielddF()[0][1];}

    // VectorPotential eg: Magnetic vector potential A
    virtual const SVector3 & getConstRefToMagneticVectorPotential() const
    {
        return getConstRefToVectorPotential(0);
    }
    virtual SVector3 & getRefToMagneticVectorPotential()
    {
        return getRefToVectorPotential(0);
    }

    // VectorCurl eg: Magnetic induction/ flux density B = CurlA
    virtual const SVector3 & getConstRefToMagneticInduction() const
    {
        return getConstRefToVectorCurl(0);
    }
    virtual SVector3 & getRefToMagneticInduction()
    {
        return getRefToVectorCurl(0);
    }

    // VectorField eg: Magnetic field H = mu * B
    virtual const SVector3 & getConstRefToMagneticField() const
    {
        return getConstRefToVectorField(0);
    }
    virtual SVector3 & getRefToMagneticField()
    {
        return getRefToVectorField(0);
    }

    // js0
    virtual const SVector3 & getConstRefToinductorSourceVectorField() const
    {
        return getConstRefToInductorSourceVectorField(0);
    }
    virtual SVector3 & getRefToinductorSourceVectorField()
    {
        return getRefToInductorSourceVectorField(0);
    }

    // coupling curlData with extraDof
    // dPdMagneticVectorCurl
    virtual const STensor33 & getConstRefTodPdMagneticVectorCurl() const
    {
        return getConstRefTodPdVectorCurl()[0];
    }
    virtual STensor33 & getRefTodPdMagneticVectorCurl()
    {
        return getRefTodPdVectorCurl()[0];
    }

    // dPdMagneticVectorPotential
    virtual const STensor33 & getConstRefTodPdMagneticVectorPotential() const
    {
        return getConstRefTodPdVectorPotential()[0];
    }
    virtual STensor33 & getRefTodPdMagneticVectorPotential()
    {
        return getRefTodPdVectorPotential()[0];
    }

    // dMagneticFielddMagneticVectorPotential
    virtual const STensor3 & getConstRefTodMagneticFielddMagneticVectorPotential() const
    {
        return getConstRefTodVectorFielddVectorPotential()[0][0];
    }
    virtual STensor3 & getRefTodMagneticFielddMagneticVectorPotential()
    {
        return getRefTodVectorFielddVectorPotential()[0][0];
    }

    // dMagneticFielddMagneticVectorCurl
    virtual const STensor3 & getConstRefTodMagneticFielddMagneticVectorCurl() const
    {
        return getConstRefTodVectorFielddVectorCurl()[0][0];
    }
    virtual STensor3 & getRefTodMagneticFielddMagneticVectorCurl()
    {
        return getRefTodVectorFielddVectorCurl()[0][0];
    }

    // dElecDisplacementdMagneticVectorPotential
    virtual const STensor3 & getConstRefTodElecDisplacementdMagneticVectorPotential() const
    {
        return getConstRefTodElecDisplacementdVectorPotential()[1][0];
    }
    virtual STensor3 & getRefTodElecDisplacementdMagneticVectorPotential()
    {
        return getRefTodElecDisplacementdVectorPotential()[1][0];
    }

    // dElecDisplacementdMagneticVectorCurl
    virtual const STensor3 & getConstRefTodElecDisplacementdMagneticVectorCurl() const
    {
        return  getConstRefTodElecDisplacementdVectorCurl()[1][0];
    }
    virtual STensor3 & getRefTodElecDisplacementdMagneticVectorCurl()
    {
        return  getRefTodElecDisplacementdVectorCurl()[1][0];
    }

    // dMagneticFielddF
    virtual const STensor33 & getConstRefTodMagneticFielddF() const
    {
        return getConstRefTodVectorFielddF()[0];
    }
    virtual STensor33 & getRefTodMagneticFielddF()
    {
        return getRefTodVectorFielddF()[0];
    }

    // dMagneticFielddT
    virtual const SVector3 & getConstRefTodMagneticFielddT() const
    {
        return getConstRefTodVectorFielddExtraDofField()[0][0];
    }
    virtual SVector3 & getRefTodMagneticFielddT()
    {
        return getRefTodVectorFielddExtraDofField()[0][0];
    }

    // dMagneticFielddGradT
    virtual const STensor3 & getConstRefTodMagneticFielddGradT() const
    {
        return getConstRefTodVectorFielddGradExtraDofField()[0][0];
    }
    virtual STensor3 & getRefTodMagneticFielddGradT()
    {
        return getRefTodVectorFielddGradExtraDofField()[0][0];
    }

    // dMagneticFielddV
    virtual const SVector3 & getConstRefTodMagneticFielddV() const
    {
        return getConstRefTodVectorFielddExtraDofField()[0][1];
    }
    virtual SVector3 & getRefTodMagneticFielddV()
    {
        return getRefTodVectorFielddExtraDofField()[0][1];
    }

    // dMagneticFielddGradV
    virtual const STensor3 & getConstRefTodMagneticFielddGradV() const
    {
        return getConstRefTodVectorFielddGradExtraDofField()[0][1];
    }
    virtual STensor3 & getRefTodMagneticFielddGradV()
    {
        return getRefTodVectorFielddGradExtraDofField()[0][1];
    }

    // dFluxTdA
    virtual const STensor3 & getConstRefTodFluxTdMagneticVectorPotential() const
    {
        return getConstRefTodFluxdVectorPotential()[0][0];
    }
    virtual STensor3 & getRefTodFluxTdMagneticVectorPotential()
    {
        return getRefTodFluxdVectorPotential()[0][0];
    }

    // dFluxjedA
    virtual const STensor3 & getConstRefTodFluxjedMagneticVectorPotential() const
    {
        return getConstRefTodFluxdVectorPotential()[1][0];
    }
    virtual STensor3 & getRefTodFluxjedMagneticVectorPotential()
    {
        return getRefTodFluxdVectorPotential()[1][0];
    }

    // dFluxTdB
    virtual const STensor3 & getConstRefTodFluxTdMagneticVectorCurl() const
    {
        return getConstRefTodFluxdVectorCurl()[0][0];
    }
    virtual STensor3 & getRefTodFluxTdMagneticVectorCurl()
    {
        return getRefTodFluxdVectorCurl()[0][0];
    }

    // dFluxjedB
    virtual const STensor3 & getConstRefTodFluxjedMagneticVectorCurl() const
    {
        return getConstRefTodFluxdVectorCurl()[1][0];
    }
    virtual STensor3 & getRefTodFluxjedMagneticVectorCurl()
    {
        return getRefTodFluxdVectorCurl()[1][0];
    }

    virtual const SVector3 & getConstRefTosourceVectorField() const
    {
        return getConstRefToSourceVectorField(0);
    }
    virtual SVector3 & getRefTosourceVectorField()
    {
        return getRefToSourceVectorField(0);
    }
    // dsourceVectorFielddt
    virtual const SVector3 & getConstRefTodsourceVectorFielddt() const
    {
      return getConstRefTodSourceVectorFielddt(0);
    }
    virtual SVector3 & getRefTodsourceVectorFielddt()
    {
      return getRefTodSourceVectorFielddt(0);
    }

    // EMFieldSource : w_AV
    virtual const double getConstRefToThermalEMFieldSource() const
    {
        return getConstRefToEMFieldSource()(0); // indices (extradof)
    }
    virtual double & getRefToThermalEMFieldSource()
    {
        return getRefToEMFieldSource()(0);
    }

    // Voltage part of EMFieldSource
    virtual const double getConstRefToVoltageEMFieldSource() const
    {
        return getConstRefToEMFieldSource()(1); // indices (extradof)
    }
    virtual double & getRefToVoltageEMFieldSource()
    {
        return getRefToEMFieldSource()(1);
    }

    virtual const SVector3 & getConstRefTodThermalEMFieldSourcedMagneticVectorPotential() const
    {
        return getConstRefTodEMFieldSourcedVectorPotential()[0][0]; // indices (extradof, curl)
    }

    virtual SVector3 & getRefTodThermalEMFieldSourcedMagneticVectorPotential()
    {
        return getRefTodEMFieldSourcedVectorPotential()[0][0];
    }

    virtual const SVector3 & getConstRefTodThermalEMFieldSourcedMagneticVectorCurl() const
    {
        return getConstRefTodEMFieldSourcedVectorCurl()[0][0]; // indices (extradof, curl)
    }

    virtual SVector3 & getRefTodThermalEMFieldSourcedMagneticVectorCurl()
    {
        return getRefTodEMFieldSourcedVectorCurl()[0][0];
    }

    virtual const STensor3 & getConstRefTodThermalEMFieldSourcedF() const
    {
        return getConstRefTodEMFieldSourcedF()[0]; // indices (extradof)
    }

    virtual STensor3 & getRefTodThermalEMFieldSourcedF()
    {
        return getRefTodEMFieldSourcedF()[0];
    }

    virtual const double getConstRefTodThermalEMFieldSourcedT() const
    {
        return getConstRefTodEMFieldSourcedField()(0,0); // indices (extradof, extradof)
    }

    virtual double & getRefTodThermalEMFieldSourcedT()
    {
        return getRefTodEMFieldSourcedField()(0,0);
    }

    virtual const double getConstRefTodThermalEMFieldSourcedV() const
    {
        return getConstRefTodEMFieldSourcedField()(0,1); // indices (extradof, extradof)
    }

    virtual double & getRefTodThermalEMFieldSourcedV()
    {
        return getRefTodEMFieldSourcedField()(0,1);
    }

    virtual const SVector3 & getConstRefTodThermalEMFieldSourcedGradT() const
    {
        return getConstRefTodEMFieldSourcedGradField()[0][0]; // indices (extradof, extradof)
    }

    virtual SVector3 & getRefTodThermalEMFieldSourcedGradT()
    {
        return getRefTodEMFieldSourcedGradField()[0][0];
    }

    virtual const SVector3 & getConstRefTodThermalEMFieldSourcedGradV() const
    {
        return getConstRefTodEMFieldSourcedGradField()[0][1]; // indices (extradof, extradof)
    }

    virtual SVector3 & getRefTodThermalEMFieldSourcedGradV()
    {
        return getRefTodEMFieldSourcedGradField()[0][1];
    }

    virtual const SVector3 & getConstRefTodVoltageEMFieldSourcedMagneticVectorPotential() const
    {
        return getConstRefTodEMFieldSourcedVectorPotential()[1][0]; // indices (extradof, curl)
    }

    virtual SVector3 & getRefTodVoltageEMFieldSourcedMagneticVectorPotential()
    {
        return getRefTodEMFieldSourcedVectorPotential()[1][0];
    }

    virtual const SVector3 & getConstRefTodVoltageEMFieldSourcedMagneticVectorCurl() const
    {
        return getConstRefTodEMFieldSourcedVectorCurl()[1][0]; // indices (extradof, curl)
    }

    virtual SVector3 & getRefTodVoltageEMFieldSourcedMagneticVectorCurl()
    {
        return getRefTodEMFieldSourcedVectorCurl()[1][0];
    }

    virtual const STensor3 & getConstRefTodVoltageEMFieldSourcedF() const
    {
        return getConstRefTodEMFieldSourcedF()[1]; // indices (extradof)
    }

    virtual STensor3 & getRefTodVoltageEMFieldSourcedF()
    {
        return getRefTodEMFieldSourcedF()[1];
    }

    virtual const double getConstRefTodVoltageEMFieldSourcedT() const
    {
        return getConstRefTodEMFieldSourcedField()(1,0); // indices (extradof, extradof)
    }

    virtual double & getRefTodVoltageEMFieldSourcedT()
    {
        return getRefTodEMFieldSourcedField()(1,0);
    }

    virtual const double getConstRefTodVoltageEMFieldSourcedV() const
    {
        return getConstRefTodEMFieldSourcedField()(1,1); // indices (extradof, extradof)
    }

    virtual double & getRefTodVoltageEMFieldSourcedV()
    {
        return getRefTodEMFieldSourcedField()(1,1);
    }

    virtual const SVector3 & getConstRefTodVoltageEMFieldSourcedGradT() const
    {
        return getConstRefTodEMFieldSourcedGradField()[1][0]; // indices (extradof, extradof)
    }

    virtual SVector3 & getRefTodVoltageEMFieldSourcedGradT()
    {
        return getRefTodEMFieldSourcedGradField()[1][0];
    }

    virtual const SVector3 & getConstRefTodVoltageEMFieldSourcedGradV() const
    {
        return getConstRefTodEMFieldSourcedGradField()[1][1]; // indices (extradof, extradof)
    }

    virtual SVector3 & getRefTodVoltageEMFieldSourcedGradV()
    {
        return getRefTodEMFieldSourcedGradField()[1][1];
    }

    // dsourceVectorFielddA
    virtual const STensor3 & getConstRefTodSourceVectorFielddMagneticVectorPotential() const
    {
        return getConstRefTodSourceVectorFielddVectorPotential()[0][0];
    }
    virtual STensor3 & getRefTodSourceVectorFielddMagneticVectorPotential()
    {
        return getRefTodSourceVectorFielddVectorPotential()[0][0];
    }

    // dsourceVectorFielddB
    virtual const STensor3 & getConstRefTodSourceVectorFielddMagneticVectorCurl() const
    {
      return getConstRefTodSourceVectorFielddVectorCurl()[0][0];
    }
    virtual STensor3 & getRefTodSourceVectorFielddMagneticVectorCurl()
    {
      return getRefTodSourceVectorFielddVectorCurl()[0][0];
    }

    virtual const SVector3 & getConstRefTomechanicalFieldSource() const
    {
        return getConstRefToMechanicalFieldSource(0);
    }
    virtual SVector3 & getRefTomechanicalFieldSource()
    {
        return getRefToMechanicalFieldSource(0);
    }

    // dsourceVectorFielddF
    virtual const STensor33 & getConstRefTodsourceVectorFielddF() const
    {
        return getConstRefTodSourceVectorFielddF()[0];
    }
    virtual STensor33 & getRefTodsourceVectorFielddF()
    {
        return getRefTodSourceVectorFielddF()[0];
    }

    // dsourceVectorFielddT
    virtual const SVector3 & getConstRefTodSourceVectorFielddT() const
    {
        return getConstRefTodSourceVectorFielddExtraDofField()[0][0];
    }
    virtual SVector3 & getRefTodSourceVectorFielddT()
    {
        return getRefTodSourceVectorFielddExtraDofField()[0][0];
    }

    // dsourceVectorFielddGradT
    virtual const STensor3 & getConstRefTodSourceVectorFielddGradT() const
    {
        return getConstRefTodSourceVectorFielddGradExtraDofField()[0][0];
    }
    virtual STensor3 & getRefTodSourceVectorFielddGradT()
    {
        return getRefTodSourceVectorFielddGradExtraDofField()[0][0];
    }

    // dsourceVectorFielddV
    virtual const SVector3 & getConstRefTodSourceVectorFielddV() const
    {
        return getConstRefTodSourceVectorFielddExtraDofField()[0][1];
    }
    virtual SVector3 & getRefTodSourceVectorFielddV()
    {
        return getRefTodSourceVectorFielddExtraDofField()[0][1];
    }

    // dsourceVectorFielddGradV
    virtual const STensor3 & getConstRefTodSourceVectorFielddGradV() const
    {
        return getConstRefTodSourceVectorFielddGradExtraDofField()[0][1];
    }
    virtual STensor3 & getRefTodSourceVectorFielddGradV()
    {
        return getRefTodSourceVectorFielddGradExtraDofField()[0][1];
    }

    virtual const STensor33 & getConstRefTodmechanicalFieldSourcedF() const
    {
        return getConstRefTodMechanicalFieldSourcedF()[0];
    }
    virtual STensor33 & getRefTodmechanicalFieldSourcedF()
    {
        return getRefTodMechanicalFieldSourcedF()[0];
    }

    virtual const SVector3 & getConstRefTodMechanicalFieldSourcedT() const
    {
        return getConstRefTodMechanicalFieldSourcedExtraDofField()[0][0];
    }
    virtual SVector3 & getRefTodMechanicalFieldSourcedT()
    {
        return getRefTodMechanicalFieldSourcedExtraDofField()[0][0];
    }

    virtual const STensor3 & getConstRefTodMechanicalFieldSourcedGradT() const
    {
        return getConstRefTodMechanicalFieldSourcedGradExtraDofField()[0][0];
    }
    virtual STensor3 & getRefTodMechanicalFieldSourcedGradT()
    {
        return getRefTodMechanicalFieldSourcedGradExtraDofField()[0][0];
    }

    virtual const SVector3 & getConstRefTodMechanicalFieldSourcedV() const
    {
        return getConstRefTodMechanicalFieldSourcedExtraDofField()[0][1];
    }
    virtual SVector3 & getRefTodMechanicalFieldSourcedV()
    {
        return getRefTodMechanicalFieldSourcedExtraDofField()[0][1];
    }

    virtual const STensor3 & getConstRefTodMechanicalFieldSourcedGradV() const
    {
        return getConstRefTodMechanicalFieldSourcedGradExtraDofField()[0][1];
    }
    virtual STensor3 & getRefTodMechanicalFieldSourcedGradV()
    {
        return getRefTodMechanicalFieldSourcedGradExtraDofField()[0][1];
    }

    virtual const STensor3 & getConstRefTodMechanicalFieldSourcedMagneticVectorPotential() const
    {
        return getConstRefTodMechanicalFieldSourcedVectorPotential()[0][0];
    }
    virtual STensor3 & getRefTodMechanicalFieldSourcedMagneticVectorPotential()
    {
        return getRefTodMechanicalFieldSourcedVectorPotential()[0][0];
    }

    virtual const STensor3 & getConstRefTodMechanicalFieldSourcedMagneticVectorCurl() const
    {
        return getConstRefTodMechanicalFieldSourcedVectorCurl()[0][0];
    }
    virtual STensor3 & getRefTodMechanicalFieldSourcedMagneticVectorCurl()
    {
        return getRefTodMechanicalFieldSourcedVectorCurl()[0][0];
    }

    // dw_TdA
    virtual const SVector3 & getConstRefTodFieldSourcedMagneticVectorPotential() const
    {
        return getConstRefTodFieldSourcedVectorPotential()[0][0];
    }
    virtual SVector3 & getRefTodFieldSourcedMagneticVectorPotential()
    {
        return getRefTodFieldSourcedVectorPotential()[0][0];
    }

    // dw_TdB
    virtual const SVector3 & getConstRefTodFieldSourcedMagneticVectorCurl() const
    {
        return getConstRefTodFieldSourcedVectorCurl()[0][0];
    }
    virtual SVector3 & getRefTodFieldSourcedMagneticVectorCurl()
    {
        return getRefTodFieldSourcedVectorCurl()[0][0];
    }

    // dmechSourcedA
    virtual const SVector3 & getConstRefTodMechanicalSourcedMagneticVectorPotential() const
    {
        return getConstRefTodMechanicalSourcedVectorPotential()[0][0];
    }
    virtual SVector3 & getRefTodMechanicalSourcedMagneticVectorPotential()
    {
        return getRefTodMechanicalSourcedVectorPotential()[0][0];
    }

    // dmechSourcedB
    virtual const SVector3 & getConstRefTodMechanicalSourcedMagneticVectorCurl() const
    {
        return getConstRefTodMechanicalSourcedVectorCurl()[0][0];
    }
    virtual SVector3 & getRefTodMechanicalSourcedMagneticVectorCurl()
    {
        return getRefTodMechanicalSourcedVectorCurl()[0][0];
    }

    virtual IPVariable* clone() const =0;
    virtual void restart();
};

class LinearElecMagTherMechDG3DIPVariable : public ElecMagTherMechDG3DIPVariableBase
{

    protected:
    IPLinearElecMagTherMech _linearEMTMIP;

    public:
    LinearElecMagTherMechDG3DIPVariable(const bool createBodyForceHO, const bool oninter):ElecMagTherMechDG3DIPVariableBase(createBodyForceHO,oninter)
    {
        Msg::Error("LinearElecMagTherMechDG3DIPVariable: Cannot use the default constructor");
    }
    LinearElecMagTherMechDG3DIPVariable(double tinitial,double vinitial,
    const SVector3 & magvecpotinitial, const bool createBodyForceHO, const bool oninter);
    LinearElecMagTherMechDG3DIPVariable(const LinearElecMagTherMechDG3DIPVariable &source);
    LinearElecMagTherMechDG3DIPVariable& operator=(const IPVariable &source);

    virtual void setLocation(const IPVariable::LOCATION loc)
    {
        ElecMagTherMechDG3DIPVariableBase::setLocation(loc);
        _linearEMTMIP.setLocation(loc);
    }

    virtual IPVariable* getInternalData() {return &_linearEMTMIP;}
    virtual const IPVariable* getInternalData()  const {return &_linearEMTMIP;}

    IPLinearElecMagTherMech* getIPLinearElecMagTherMech(){return &_linearEMTMIP;}
    const IPLinearElecMagTherMech* getIPLinearElecMagTherMech() const{return &_linearEMTMIP;}
    virtual ~LinearElecMagTherMechDG3DIPVariable()
    {}
    virtual double get(const int i) const;
    virtual double & getRef(const int i);
    virtual double defoEnergy() const;
    virtual double plasticEnergy() const;
    virtual double damageEnergy() const {return _linearEMTMIP.damageEnergy();}
    virtual IPVariable* clone() const {return new LinearElecMagTherMechDG3DIPVariable(*this);}
    virtual void restart();
};

class LinearElecMagInductorDG3DIPVariable : public ElecMagTherMechDG3DIPVariableBase // update
{
protected:
  IPLinearElecMagInductor _linearEMInductor;

public:
  LinearElecMagInductorDG3DIPVariable(const SVector3 &GaussP, const SVector3 &Centroid,
  const SVector3 &CentralAxis, const bool createBodyForceHO, const bool oninter);
  LinearElecMagInductorDG3DIPVariable(const LinearElecMagInductorDG3DIPVariable &source);
  virtual LinearElecMagInductorDG3DIPVariable & operator=(const IPVariable &source);

  virtual void setLocation(const IPVariable::LOCATION loc)
  {
    dG3DIPVariable::setLocation(loc);
    _linearEMInductor.setLocation(loc);
  }

  virtual IPVariable* getInternalData() {return &_linearEMInductor;}
  virtual const IPVariable* getInternalData() const {return &_linearEMInductor;}

  IPLinearElecMagInductor* getIPLinearElecMagInductor(){return &_linearEMInductor;}
  const IPLinearElecMagInductor* getIPLinearElecMagInductor() const {return &_linearEMInductor;}
  virtual ~LinearElecMagInductorDG3DIPVariable(){}
  virtual double get(const int i) const;
  virtual double & getRef(const int i);
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const;
  virtual double damageEnergy() const {return _linearEMInductor.damageEnergy();};
  virtual IPVariable* clone() const {return new LinearElecMagInductorDG3DIPVariable(*this);};
  virtual void restart();
};

class AnIsotropicElecTherMechDG3DIPVariable : public ElecTherMechDG3DIPVariableBase
{

 protected:
  IPAnIsotropicElecTherMech *_ETMIP;


public:
 AnIsotropicElecTherMechDG3DIPVariable(const mlawAnIsotropicElecTherMech &_lawETM, const bool createBodyForceHO, const bool oninter):ElecTherMechDG3DIPVariableBase(createBodyForceHO,oninter){Msg::Error("LinearElecTherMechDG3DIPVariable: Cannot use the default constructor");}
 AnIsotropicElecTherMechDG3DIPVariable(double tinitial,double Vinitial,const mlawAnIsotropicElecTherMech &_lawETM, const bool createBodyForceHO, const bool oninter);
 AnIsotropicElecTherMechDG3DIPVariable(const AnIsotropicElecTherMechDG3DIPVariable &source);
 AnIsotropicElecTherMechDG3DIPVariable& operator=(const IPVariable &source);

  virtual void setLocation(const IPVariable::LOCATION loc) {
    ElecTherMechDG3DIPVariableBase::setLocation(loc);
    if (_ETMIP)
      _ETMIP->setLocation(loc);
  };

  virtual IPVariable* getInternalData() {return _ETMIP;};
  virtual const IPVariable* getInternalData()  const {return _ETMIP;};

  IPAnIsotropicElecTherMech* getIPAnIsotropicElecTherMech(){return _ETMIP;}
  const IPAnIsotropicElecTherMech* getIPAnIsotropicElecTherMech() const{return _ETMIP;}
  virtual ~AnIsotropicElecTherMechDG3DIPVariable()
  {
    if(_ETMIP != NULL)
    {
       delete _ETMIP;
       _ETMIP=NULL;
    }
  }
  virtual double get(const int i) const;
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const;
  virtual double damageEnergy() const {return _ETMIP->damageEnergy();};
  virtual IPVariable* clone() const {return new AnIsotropicElecTherMechDG3DIPVariable(*this);};
  virtual void restart();
};

class ElecSMPDG3DIPVariable : public ElecTherMechDG3DIPVariableBase
{

 protected:
  IPElecSMP *_ESMPIP;

public:
 ElecSMPDG3DIPVariable(const mlawElecSMP &_lawESMP, const bool createBodyForceHO, const bool oninter):ElecTherMechDG3DIPVariableBase(createBodyForceHO,oninter){Msg::Error("ElecSMPDG3DIPVariable: Cannot use the default constructor");}
 ElecSMPDG3DIPVariable(double tinitial,double vinitial,const mlawElecSMP &_lawESMP, const bool createBodyForceHO, const bool oninter);
 ElecSMPDG3DIPVariable(const ElecSMPDG3DIPVariable &source);
 ElecSMPDG3DIPVariable& operator=(const IPVariable &source);

  virtual void setLocation(const IPVariable::LOCATION loc) {
    ElecTherMechDG3DIPVariableBase::setLocation(loc);
    if (_ESMPIP)
      _ESMPIP->setLocation(loc);
  };


  virtual IPVariable* getInternalData() {return _ESMPIP;};
  virtual const IPVariable* getInternalData()  const {return _ESMPIP;};

  IPElecSMP* getIPElecSMP(){return _ESMPIP;}
  const IPElecSMP* getIPElecSMP() const{return _ESMPIP;}
  virtual ~ElecSMPDG3DIPVariable()
  {
    if(_ESMPIP != NULL)
    {
       delete _ESMPIP;
       _ESMPIP = NULL;
    }
  }
  virtual double get(const int i) const;
  virtual double defoEnergy() const;
  virtual double getRefToEquivalentPlasticDefo1() const {return _ESMPIP->getConstRefToEquivalentPlasticDefo1();};
  virtual double plasticEnergy() const;
  virtual double damageEnergy() const {return _ESMPIP->damageEnergy();};
  virtual IPVariable* clone() const {return new ElecSMPDG3DIPVariable(*this);};
  virtual void restart();
};

class GenericThermoMechanicsDG3DIPVariable : public ThermoMechanicsDG3DIPVariableBase
{
protected:
    IPGenericTM *_genericTMIP;
    GenericThermoMechanicsDG3DIPVariable(const bool createBodyForceHO, const bool oninter):ThermoMechanicsDG3DIPVariableBase(createBodyForceHO,oninter) {Msg::Error("GenericThermoMechanicsDG3DIPVariable: Cannot use the default constructor");}
public:
    GenericThermoMechanicsDG3DIPVariable(IPGenericTM *ipc, const double tinitial, const bool createBodyForceHO, const bool oninter);
    GenericThermoMechanicsDG3DIPVariable(const GenericThermoMechanicsDG3DIPVariable &source);
    GenericThermoMechanicsDG3DIPVariable& operator=(const IPVariable &source);

    virtual void setLocation(const IPVariable::LOCATION loc) {
        dG3DIPVariable::setLocation(loc);
        _genericTMIP->setLocation(loc);
    };

    virtual IPVariable* getInternalData() {return _genericTMIP;};
    virtual const IPVariable* getInternalData()  const {return _genericTMIP;};

    /* specific function */
    IPGenericTM* getIPGenericThermoMechanics(){return  _genericTMIP;}
    const IPGenericTM* getIPGenericThermoMechanics() const{return _genericTMIP;}
    virtual ~GenericThermoMechanicsDG3DIPVariable()
    {
        if (_genericTMIP) delete _genericTMIP;
        _genericTMIP = NULL;
    }
    virtual double get(const int i) const;
    virtual double defoEnergy() const;
    virtual double damageEnergy() const;
    virtual double getInternalEnergyExtraDofDiffusion() const {return _genericTMIP->getThermalEnergy();};
    virtual double plasticEnergy() const;
    virtual IPVariable* clone() const {return new GenericThermoMechanicsDG3DIPVariable(*this);};
    virtual void restart();

    const IPVariableMechanics& getConstRefToIpMeca() const;
    IPVariableMechanics& getRefToIpMeca();
    void setIpMeca(IPVariable& ipMeca);
    const STensor3 & getConstRefToReferenceF() const;
    STensor3 & getRefToReferenceF();
};

//copy class GenericResidualMechanicsDG3DIPVariable : public dG3DIPVariableBase idem in Material

class GenericResidualMechanicsDG3DIPVariable : public dG3DIPVariable
{
protected:
    IPGenericRM *_genericRMIP;
    GenericResidualMechanicsDG3DIPVariable(const bool createBodyForceHO, const bool oninter):dG3DIPVariable(createBodyForceHO,oninter) {Msg::Error("GenericResidualMechanicsDG3DIPVariable: Cannot use the default constructor");}
public:
    GenericResidualMechanicsDG3DIPVariable(IPGenericRM *ipc, const bool createBodyForceHO, const bool oninter);
    GenericResidualMechanicsDG3DIPVariable(const GenericResidualMechanicsDG3DIPVariable &source);
    GenericResidualMechanicsDG3DIPVariable& operator=(const IPVariable &source);

    virtual void setLocation(const IPVariable::LOCATION loc) {
        dG3DIPVariable::setLocation(loc);
        _genericRMIP->setLocation(loc);
    };

    virtual IPVariable* getInternalData() {return _genericRMIP;};
    virtual const IPVariable* getInternalData()  const {return _genericRMIP;};

    /* specific function */
    IPGenericRM* getIPGenericResidualMechanics(){return  _genericRMIP;}
    const IPGenericRM* getIPGenericResidualMechanics() const{return _genericRMIP;}
    virtual ~GenericResidualMechanicsDG3DIPVariable()
    {
        if (_genericRMIP) delete _genericRMIP;
        _genericRMIP = NULL;
    }
    virtual double get(const int i) const;
    virtual double defoEnergy() const;
    virtual double damageEnergy() const;
    virtual double plasticEnergy() const;
    virtual IPVariable* clone() const {return new GenericResidualMechanicsDG3DIPVariable(*this);};
    virtual void restart();

    const IPVariableMechanics& getConstRefToIpMeca() const;
    IPVariableMechanics& getRefToIpMeca();
    void setIpMeca(IPVariable& ipMeca);
    //const STensor3 & getConstRefToReferenceF() const;
    //STensor3 & getRefToReferenceF();
};


class ElecMagGenericThermoMechanicsDG3DIPVariable : public ElecMagTherMechDG3DIPVariableBase
{
protected:
    IPElecMagGenericThermoMech *_elecMagGenericThermoMechIP;
    ElecMagGenericThermoMechanicsDG3DIPVariable(const bool createBodyForceHO, const bool oninter):
    ElecMagTherMechDG3DIPVariableBase(createBodyForceHO,oninter)
    {
        Msg::Error("ElecMagGenericThermoMechanicsDG3DIPVariable: Cannot use the default constructor");
    }
public:
    ElecMagGenericThermoMechanicsDG3DIPVariable(IPElecMagGenericThermoMech *ipc,const double tinitial,const double vinitial,
                                                const SVector3 & magvecpotinitial, const bool createBodyForceHO, const bool oninter);
    ElecMagGenericThermoMechanicsDG3DIPVariable(const ElecMagGenericThermoMechanicsDG3DIPVariable &source);
    ElecMagGenericThermoMechanicsDG3DIPVariable& operator=(const IPVariable &source);

    virtual void setLocation(const IPVariable::LOCATION loc) {
        ElecMagTherMechDG3DIPVariableBase::setLocation(loc);
        _elecMagGenericThermoMechIP->setLocation(loc);
    };

    virtual IPVariable* getInternalData() {return _elecMagGenericThermoMechIP;};
    virtual const IPVariable* getInternalData()  const {return _elecMagGenericThermoMechIP;};

    /* specific function */
    IPElecMagGenericThermoMech* getIPElecMagGenericThermoMechanics(){return  _elecMagGenericThermoMechIP;}
    const IPElecMagGenericThermoMech* getIPElecMagGenericThermoMechanics() const{return _elecMagGenericThermoMechIP;}
    virtual ~ElecMagGenericThermoMechanicsDG3DIPVariable()
    {
        if (_elecMagGenericThermoMechIP) delete _elecMagGenericThermoMechIP;
        _elecMagGenericThermoMechIP = NULL;
    }
    virtual double get(const int i) const;
    virtual double & getRef(const int i);
    virtual double defoEnergy() const;
    virtual double getInternalEnergyExtraDofDiffusion() const {return _elecMagGenericThermoMechIP->getThermalEnergy();};
    virtual double plasticEnergy() const;
    virtual double damageEnergy() const;
    virtual IPVariable* clone() const {return new ElecMagGenericThermoMechanicsDG3DIPVariable(*this);};
    virtual void restart();

    const IPCoupledThermoMechanics& getConstRefToIpThermoMech() const;
    IPCoupledThermoMechanics& getRefToIpThermoMech();
    void setIpThermoMech(IPVariable& ipThermoMech);
};

class ElecMagInductorDG3DIPVariable : public ElecMagTherMechDG3DIPVariableBase // update
{
protected:
    IPElecMagInductor *_EMInductorIP;

public:
    ElecMagInductorDG3DIPVariable(IPElecMagInductor *ipc,
                                  const double tinitial,
                                  const double vinitial,
                                  const SVector3 & magvecpotinitial,
                                  const bool createBodyForceHO, const bool oninter);
    ElecMagInductorDG3DIPVariable(const ElecMagInductorDG3DIPVariable &source);
    virtual ElecMagInductorDG3DIPVariable & operator=(const IPVariable &source);

    virtual void setLocation(const IPVariable::LOCATION loc)
    {
        ElecMagTherMechDG3DIPVariableBase::setLocation(loc);
        _EMInductorIP->setLocation(loc);
    }

    virtual IPVariable* getInternalData() {return _EMInductorIP;}
    virtual const IPVariable* getInternalData() const {return _EMInductorIP;}

    IPElecMagInductor* getIPElecMagInductor(){return _EMInductorIP;}
    const IPElecMagInductor* getIPElecMagInductor() const {return _EMInductorIP;}
    virtual ~ElecMagInductorDG3DIPVariable()
    {
        if(_EMInductorIP) delete _EMInductorIP;
        _EMInductorIP = NULL;
    }
    virtual double get(const int i) const;
    virtual double & getRef(const int i);
    virtual double defoEnergy() const;
    virtual double plasticEnergy() const;
    virtual double damageEnergy() const;
    virtual IPVariable* clone() const {return new ElecMagInductorDG3DIPVariable(*this);};
    virtual void restart();

    const IPCoupledThermoMechanics& getConstRefToIpThermoMech() const;
    IPCoupledThermoMechanics& getRefToIpThermoMech();
    void setIpThermoMech(IPVariable& ipThermoMech);
};


// NonLinearTVM Law Interface =================================================================== BEGIN

// Take Care here, nonlocal variables cannot be directly defined with the ThermoMechanicsDG3DIPVariable Base Class (This is a pure virtual base class).

// class NonLinearTVMDG3DIPVariable : public dG3DIPVariable{
class NonLinearTVMDG3DIPVariable : public ThermoMechanicsDG3DIPVariableBase{   // Changed

  protected:
    IPNonLinearTVM * _ipViscoElastic;
    NonLinearTVMDG3DIPVariable(const bool createBodyForceHO, const bool oninter):ThermoMechanicsDG3DIPVariableBase(createBodyForceHO, oninter) {Msg::Error("NonLinearTVMDG3DIPVariable: Cannot use the default constructor of ThermoMechanicsDG3DIPVariableBase");}

  public:
// Constructor Changed
  //  NonLinearTVMDG3DIPVariable(const mlawNonLinearTVM& viscoLaw, const bool oninter=false, int nbExtraDof=0);  // ?? - WHAT to add/change?
    NonLinearTVMDG3DIPVariable(const mlawNonLinearTVM& viscoLaw, double Tinitial, const bool createBodyForceHO, const bool oninter);  // Changed
    NonLinearTVMDG3DIPVariable(const NonLinearTVMDG3DIPVariable& src);
    virtual NonLinearTVMDG3DIPVariable& operator = (const IPVariable& src);
    virtual ~NonLinearTVMDG3DIPVariable();

    virtual void setLocation(const IPVariable::LOCATION loc) {
      dG3DIPVariable::setLocation(loc);
      if (_ipViscoElastic)
        _ipViscoElastic->setLocation(loc);
    };

    virtual IPVariable* getInternalData() {return _ipViscoElastic;};
    virtual const IPVariable* getInternalData()  const {return _ipViscoElastic;};

 // Specific functions - getIP
    IPNonLinearTVM* getIPNonLinearTVM() {return _ipViscoElastic;};
    const IPNonLinearTVM* getIPNonLinearTVM() const {return _ipViscoElastic;};

    virtual double get(const int i) const;
    virtual double defoEnergy() const;

// Added from LinearThermoMechanicsDG3DIPVariable
    virtual double getInternalEnergyExtraDofDiffusion() const {return _ipViscoElastic->getThermalEnergy();};           // Added
    virtual double plasticEnergy() const;                                                                             // Added
    virtual double damageEnergy() const {return _ipViscoElastic->damageEnergy();};                                     // Added

// for Path-Following based on irreversible energy
    virtual double irreversibleEnergy() const {return _ipViscoElastic->irreversibleEnergy();};
    virtual double& getRefToIrreversibleEnergy() {return _ipViscoElastic->getRefToIrreversibleEnergy();};

    virtual const STensor3& getConstRefToDIrreversibleEnergyDDeformationGradient() const {return _ipViscoElastic->getConstRefToDIrreversibleEnergyDF();};
    virtual STensor3& getRefToDIrreversibleEnergyDDeformationGradient() {return _ipViscoElastic->getRefToDIrreversibleEnergyDF();};

    virtual IPVariable* clone() const {return new NonLinearTVMDG3DIPVariable(*this);};
    virtual void restart();
};

// NonLinearTVM Law Interface =================================================================== END

// NonLinearTVP Law Interface =================================================================== BEGIN

class NonLinearTVPDG3DIPVariable : public ThermoMechanicsDG3DIPVariableBase{   // Changed

  protected:
    IPNonLinearTVP * _ipViscoElastoPlastic;
    NonLinearTVPDG3DIPVariable(const bool createBodyForceHO, const bool oninter):ThermoMechanicsDG3DIPVariableBase(createBodyForceHO, oninter) {
        Msg::Error("NonLinearTVPDG3DIPVariable: Cannot use the default constructor of ThermoMechanicsDG3DIPVariableBase");
    }

  public:
    NonLinearTVPDG3DIPVariable(const mlawNonLinearTVP& viscoEPLaw, double Tinitial, const bool createBodyForceHO, const bool oninter);
    NonLinearTVPDG3DIPVariable(const NonLinearTVPDG3DIPVariable& src);
    virtual NonLinearTVPDG3DIPVariable& operator = (const IPVariable& src);
    virtual ~NonLinearTVPDG3DIPVariable();

    virtual void setLocation(const IPVariable::LOCATION loc) {
      dG3DIPVariable::setLocation(loc);
      if (_ipViscoElastoPlastic)
        _ipViscoElastoPlastic->setLocation(loc);
    };

    virtual IPVariable* getInternalData() {return _ipViscoElastoPlastic;};
    virtual const IPVariable* getInternalData()  const {return _ipViscoElastoPlastic;};

    IPNonLinearTVP* getIPNonLinearTVP() {return _ipViscoElastoPlastic;};
    const IPNonLinearTVP* getIPNonLinearTVP() const {return _ipViscoElastoPlastic;};

    // CHECK
    /*
    virtual void setLocation(const IPVariable::LOCATION loc) {
      dG3DIPVariable::setLocation(loc);
      if (_ipViscoElastoPlastic)
        _ipViscoElastoPlastic->setLocation(loc);
    };*/

    virtual double get(const int i) const;

    virtual bool dissipationIsActive() const {return _ipViscoElastoPlastic->dissipationIsActive();};
    virtual void blockDissipation(const bool fl){   _ipViscoElastoPlastic->blockDissipation(fl);}
    virtual bool dissipationIsBlocked() const { return _ipViscoElastoPlastic->dissipationIsBlocked();}

// for Path-Following based on irreversible energy
    virtual double irreversibleEnergy() const {return _ipViscoElastoPlastic->irreversibleEnergy();};
    virtual double& getRefToIrreversibleEnergy() {return _ipViscoElastoPlastic->getRefToIrreversibleEnergy();};

    virtual const STensor3& getConstRefToDIrreversibleEnergyDDeformationGradient() const {return _ipViscoElastoPlastic->getConstRefToDIrreversibleEnergyDF();};
    virtual STensor3& getRefToDIrreversibleEnergyDDeformationGradient() {return _ipViscoElastoPlastic->getRefToDIrreversibleEnergyDF();};

    virtual double defoEnergy() const;
    virtual double plasticEnergy() const;
    virtual double damageEnergy() const {return _ipViscoElastoPlastic->damageEnergy();};
    virtual double getInternalEnergyExtraDofDiffusion() const {return _ipViscoElastoPlastic->getThermalEnergy();};

    virtual IPVariable* clone() const {return new NonLinearTVPDG3DIPVariable(*this);};
    virtual void restart();
};

// NonLinearTVP Law Interface =================================================================== END

#endif //DG3DIPVARIABLE
