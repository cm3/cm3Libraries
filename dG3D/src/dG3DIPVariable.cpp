//
// C++ Interface: ipvariable
//
// Description: Class with definition of ipvarible for dG3D
//
//
// Author:  <L. Noels>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "dG3DIPVariable.h"
#include "ipstate.h"
#include "ipField.h"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <string.h>
#include "restartManager.h"
#include "highOrderTensor.h"
#include "DeepMaterialNetworks.h"

dG3DIPVariableBase::dG3DIPVariableBase(const dG3DIPVariableBase &source) : ipFiniteStrain(source)
{}

dG3DIPVariableBase & dG3DIPVariableBase::operator = (const IPVariable &source)
{
  ipFiniteStrain::operator=(source);
  return *this;
}

nonlocalData::nonlocalData(const int num, const int numExtraDof, const int numCurlVar)
: numNonLocalVariable(num), numConstitutiveExtraDofDiffusionVariable(numExtraDof), numConstitutiveCurlVariable(numCurlVar)
{
  if (num > 0){
    dLocalVariableDStrain.resize(num,STensor3(0.));
    dStressDNonLocalVariable.resize(num,STensor3(0.));
    gradNonLocalVariable.resize(num,SVector3(0.));
    dLocalVariableDNonLocalVariable.resize(num,num);
    dLocalVariableDNonLocalVariable.setAll(0.);
    nonLocalJump.resize(num,true);
    nonLocalVariable.resize(num,true);
    if(numExtraDof>0)
    {
      dLocalVariableDExtraDofDiffusionField.resize(num,numExtraDof); dLocalVariableDExtraDofDiffusionField.setAll(0.);
    }
    if (numCurlVar > 0)
    {
        std::vector<SVector3>        StdSVect3Tmp;
        StdSVect3Tmp.resize(numCurlVar,SVector3(0.));
        dLocalVariabledVectorPotential.resize(num,StdSVect3Tmp);
        dLocalVariabledVectorCurl.resize(num,StdSVect3Tmp);
    }
  }
};

nonlocalData::nonlocalData(const nonlocalData& source):
  numNonLocalVariable(source.numNonLocalVariable),
  numConstitutiveExtraDofDiffusionVariable(source.numConstitutiveExtraDofDiffusionVariable),
  numConstitutiveCurlVariable(source.numConstitutiveCurlVariable),
  dLocalVariableDStrain(source.dLocalVariableDStrain),
  dStressDNonLocalVariable(source.dStressDNonLocalVariable),
  dLocalVariableDNonLocalVariable(source.dLocalVariableDNonLocalVariable),
  nonLocalJump(source.nonLocalJump),
  nonLocalVariable(source.nonLocalVariable),
  gradNonLocalVariable(source.gradNonLocalVariable),
  dLocalVariableDExtraDofDiffusionField(source.dLocalVariableDExtraDofDiffusionField),
  dLocalVariabledVectorPotential(source.dLocalVariabledVectorPotential),
  dLocalVariabledVectorCurl(source.dLocalVariabledVectorCurl)
{
};
nonlocalData& nonlocalData::operator = (const nonlocalData& source){
  numNonLocalVariable             = source.numNonLocalVariable;
  numConstitutiveExtraDofDiffusionVariable = source.numConstitutiveExtraDofDiffusionVariable;
  numConstitutiveCurlVariable     = source.numConstitutiveCurlVariable;
  dLocalVariableDStrain           = source.dLocalVariableDStrain;
  dStressDNonLocalVariable        = source.dStressDNonLocalVariable;
  dLocalVariableDNonLocalVariable = source.dLocalVariableDNonLocalVariable;
  nonLocalJump                    = source.nonLocalJump;
  nonLocalVariable                = source.nonLocalVariable;
  gradNonLocalVariable            = source.gradNonLocalVariable;
  dLocalVariableDExtraDofDiffusionField= source.dLocalVariableDExtraDofDiffusionField;
  dLocalVariabledVectorPotential = source.dLocalVariabledVectorPotential;
  dLocalVariabledVectorCurl = source.dLocalVariabledVectorCurl;
  return *this;
};

void nonlocalData::restart(){
  restartManager::restart(numNonLocalVariable);
  restartManager::restart(numConstitutiveExtraDofDiffusionVariable);
  restartManager::restart(numConstitutiveCurlVariable);
  restartManager::restart(dLocalVariableDStrain);
  restartManager::restart(dStressDNonLocalVariable);
  restartManager::restart(dLocalVariableDNonLocalVariable.getDataPtr(),numNonLocalVariable*numNonLocalVariable);
  restartManager::restart(nonLocalJump.getDataPtr(),numNonLocalVariable);
  restartManager::restart(nonLocalVariable.getDataPtr(),numNonLocalVariable);
  restartManager::restart(gradNonLocalVariable);
  restartManager::restart(dLocalVariableDExtraDofDiffusionField.getDataPtr(),
                          numNonLocalVariable*numConstitutiveExtraDofDiffusionVariable);
   if (numConstitutiveCurlVariable > 0)
   {
     for(int i=0; i< numNonLocalVariable; i++)
     {
       restartManager::restart(dLocalVariabledVectorPotential[i]);
       restartManager::restart(dLocalVariabledVectorCurl[i]);
     }
  }

};
//
bodyForceForHO::bodyForceForHO()
{
  STensorOperation::unity(dFmdFM);
};
bodyForceForHO::bodyForceForHO(const bodyForceForHO& src):  GM(src.GM), dFmdFM(src.dFmdFM),  dFmdGM(src.dFmdGM), Bm(src.Bm), dBmdFm(src.dBmdFm), dBmdGM(src.dBmdGM), dCmdFm(src.dCmdFm), dPmdFM(src.dPmdFM)
{

};
bodyForceForHO& bodyForceForHO::operator = (const bodyForceForHO& src)
{
    GM=src.GM;
    dFmdFM=src.dFmdFM;
    dFmdGM=src.dFmdGM;
    Bm = src.Bm;
    dBmdFm=src.dBmdFm;
    dBmdGM=src.dBmdGM;
    dCmdFm=src.dCmdFm;
    dPmdFM =src.dPmdFM;
    return *this;
};
void bodyForceForHO::restart()
{
  restartManager::restart(GM);
  restartManager::restart(dFmdFM);
  restartManager::restart(dFmdGM);
  restartManager::restart(Bm);
  restartManager::restart(dBmdFm);
  restartManager::restart(dBmdGM);
  restartManager::restart(dCmdFm);
  restartManager::restart(dPmdFM);
};
//
constitutiveExtraDofDiffusionData::constitutiveExtraDofDiffusionData(const int num, const int numNonLocal,
const int numCurlVar, bool _useOfEnergy, bool _useOfEnergyConjugatedField)
: numConstitutiveExtraDofDiffusionVariable(num), numNonLocalVariable(numNonLocal), numConstitutiveCurlVariable(numCurlVar),
  useOfEnergy(_useOfEnergy), useOfEnergyConjugatedField(_useOfEnergyConjugatedField)
{
  if (num > 0){
    std::vector<STensor3>        StdSTens3Tmp; StdSTens3Tmp.resize(num,STensor3(0.));
    std::vector<std::vector<STensor3>>        StdSTens3Tmp2; StdSTens3Tmp2.resize(num,StdSTens3Tmp);
    std::vector<SVector3>        StdSVect3Tmp; StdSVect3Tmp.resize(num,SVector3(0.));
    STensor43		         StdSTens43Tmp; STensorOperation::zero(StdSTens43Tmp);
    std::vector<STensor43>	 StdSTens43Tmp2;StdSTens43Tmp2.resize(num,StdSTens43Tmp);
    std::vector<const STensor3*> StdSTens3PtTmp; StdSTens3PtTmp.resize(num,NULL);
    std::vector<double>          StdDoubleTmp; StdDoubleTmp.resize(num,0.);

    field.resize(num); field.setAll(0.);

    gradField.resize(num,SVector3(0.));
    flux.resize(num,SVector3(0.));
    electricDisplacement.resize(num, SVector3(0.0));
    dPdField.resize(num,STensor3(0.));
    dPdGradField.resize(num,STensor33(0.));
    dFluxdGradField.resize(num,StdSTens3Tmp);
    dFluxdField.resize(num,StdSVect3Tmp);
    dFluxdF.resize(num,STensor33(0.));
    dElecDisplacementdField.resize(num, StdSVect3Tmp);
    dElecDisplacementdGradField.resize(num, StdSTens3Tmp);
    dElecDisplacementdF.resize(num, STensor33(0.0));

    linearK.resize(num,StdSTens3PtTmp);
    //if(useOfEnergy)
    {
     /* EnergyK.resize(num,StdSTens3PtTmp);
      dEnergyKdField.resize(num,StdSTens3Tmp2);
      dEnergyKdF.resize(num,StdSTens43Tmp2);*/
      dlinearKdField.resize(num,StdSTens3Tmp2);
      dlinearKdF.resize(num,StdSTens43Tmp2);
      dFluxdGradFielddF.resize(num,StdSTens43Tmp2);
      dFluxdGradFielddField.resize(num,StdSTens3Tmp2);
    }
    fieldSource.resize(num); fieldSource.setAll(0.);
    dFieldSourcedField.resize(num,num); dFieldSourcedField.setAll(0.);
    dFieldSourcedGradField.resize(num,StdSVect3Tmp);
    dFieldSourcedF.resize(num,STensor3(0.));

    mechanicalSource.resize(num); mechanicalSource.setAll(0.);
    dMechanicalSourcedField.resize(num,num); dMechanicalSourcedField.setAll(0.);
    dMechanicalSourcedGradField.resize(num,StdSVect3Tmp);
    dMechanicalSourcedF.resize(num,STensor3(0.));

    fieldCapacityPerUnitField.resize(num); fieldCapacityPerUnitField.setAll(0.);

    emFieldSource.resize(num); emFieldSource.setAll(0.0);
    demFieldSourcedField.resize(num, num); demFieldSourcedField.setAll(0.0);
    demFieldSourcedGradField.resize(num, StdSVect3Tmp);

    fieldJump.resize(num); fieldJump.setAll(0.);

    //if(useOfEnergyConjugatedField)
    {
      energyConjugatedField.resize(num); energyConjugatedField.setAll(0.);
      gradEnergyConjugatedField.resize(num,SVector3(0.));
      energyConjugatedFieldJump.resize(num); energyConjugatedFieldJump.setAll(0.);
      dFielddEnergyConjugatedField.resize(num,StdDoubleTmp);
      dGradFielddEnergyConjugatedField.resize(num,StdSVect3Tmp);
      dGradFielddGradEnergyConjugatedField.resize(num,StdSTens3Tmp);
      dEnergyConjugatedFieldJumpdFieldm.resize(num,StdDoubleTmp);
      dEnergyConjugatedFieldJumpdFieldp.resize(num,StdDoubleTmp);
    }

    oneOverFieldJump.resize(num); oneOverFieldJump.setAll(0.);
    dOneOverFieldJumpdFieldm.resize(num,num); dOneOverFieldJumpdFieldm.setAll(0.);
    dOneOverFieldJumpdFieldp.resize(num,num); dOneOverFieldJumpdFieldp.setAll(0.);
    interfaceFlux.resize(num,SVector3(0.));
    linearSymmetrizationCoupling.resize(num,NULL);
    if(numNonLocalVariable > 0) {
      std::vector<SVector3>        StdSVect3CouplingTmp; StdSVect3CouplingTmp.resize(numNonLocalVariable,SVector3(0.));
      dFluxdNonLocalVariable.resize(num,StdSVect3CouplingTmp);
      dFieldSourcedNonLocalVariable.resize(num,numNonLocalVariable); dFieldSourcedNonLocalVariable.setAll(0.);
      dMechanicalSourcedNonLocalVariable.resize(num,numNonLocalVariable); dMechanicalSourcedNonLocalVariable.setAll(0.);
      dEMFieldSourcedNonLocalVariable.resize(num,numNonLocalVariable); dEMFieldSourcedNonLocalVariable.setAll(0.0);
      dElecDisplacementdNonLocalVariable.resize(num, StdSVect3CouplingTmp);

    }

    if (numConstitutiveCurlVariable > 0)
    {
        std::vector< SVector3 > StdSVec3CouplingTmp;
        StdSVec3CouplingTmp.resize(numConstitutiveCurlVariable, SVector3(0.0));
        std::vector< STensor3 > StdSTen3CouplingTmp;
        StdSTen3CouplingTmp.resize(numConstitutiveCurlVariable, STensor3(0.0));

        dFluxdVectorPotential.resize(numConstitutiveExtraDofDiffusionVariable, StdSTen3CouplingTmp);
        dFluxdVectorCurl.resize(numConstitutiveExtraDofDiffusionVariable, StdSTen3CouplingTmp);
        dFieldSourcedVectorPotential.resize(numConstitutiveExtraDofDiffusionVariable, StdSVec3CouplingTmp);
        dMechanicalSourcedVectorPotential.resize(numConstitutiveExtraDofDiffusionVariable, StdSVec3CouplingTmp);
        dFieldSourcedVectorCurl.resize(numConstitutiveExtraDofDiffusionVariable, StdSVec3CouplingTmp);
        dMechanicalSourcedVectorCurl.resize(numConstitutiveExtraDofDiffusionVariable, StdSVec3CouplingTmp);
        demFieldSourcedVectorPotential.resize(numConstitutiveExtraDofDiffusionVariable, StdSVec3CouplingTmp);
        demFieldSourcedVectorCurl.resize(numConstitutiveExtraDofDiffusionVariable, StdSVec3CouplingTmp);
        dElecDisplacementdVectorPotential.resize(numConstitutiveExtraDofDiffusionVariable, StdSTen3CouplingTmp);
        dElecDisplacementdVectorCurl.resize(numConstitutiveExtraDofDiffusionVariable, StdSTen3CouplingTmp);
    }
    demFieldSourcedF.resize(num, STensor3(0.0));
  }
};
constitutiveExtraDofDiffusionData::constitutiveExtraDofDiffusionData(const constitutiveExtraDofDiffusionData& source): numConstitutiveExtraDofDiffusionVariable(source.numConstitutiveExtraDofDiffusionVariable), numNonLocalVariable(source.numNonLocalVariable),
                                                                                   numConstitutiveCurlVariable(source.numConstitutiveCurlVariable),
                                                                                   field(source.field), gradField(source.gradField), flux(source.flux), electricDisplacement(source.electricDisplacement),
                                                                                   dPdField(source.dPdField), dPdGradField(source.dPdGradField), dFluxdGradField(source.dFluxdGradField),
                                                                                   dFluxdField(source.dFluxdField),
                                                                                   dFluxdF(source.dFluxdF),
                                                                                   dElecDisplacementdField(source.dElecDisplacementdField),
                                                                                   dElecDisplacementdGradField(source.dElecDisplacementdGradField),
                                                                                   dElecDisplacementdF(source.dElecDisplacementdF),
                                                                                   linearK(source.linearK),dlinearKdField(source.dlinearKdField),
                                                                                   dlinearKdF(source.dlinearKdF),fieldSource(source.fieldSource),
                                                                                   dFieldSourcedField(source.dFieldSourcedField), dFieldSourcedGradField(source.dFieldSourcedGradField),
                                                                                   dFieldSourcedF(source.dFieldSourcedF),
	                                                                           mechanicalSource(source.mechanicalSource), dMechanicalSourcedField(source.dMechanicalSourcedField),
                                                                                   dMechanicalSourcedGradField(source.dMechanicalSourcedGradField), dMechanicalSourcedF(source.dMechanicalSourcedF),
                                                                                   fieldCapacityPerUnitField(source.fieldCapacityPerUnitField),
                                                                                   emFieldSource(source.emFieldSource),
                                                                                   demFieldSourcedField(source.demFieldSourcedField),
                                                                                   demFieldSourcedGradField(source.demFieldSourcedGradField),
                                                                                   fieldJump(source.fieldJump),
                                                                                   oneOverFieldJump(source.oneOverFieldJump),
                                                                                   dOneOverFieldJumpdFieldm(source.dOneOverFieldJumpdFieldm),
                                                                                   dOneOverFieldJumpdFieldp(source.dOneOverFieldJumpdFieldp), interfaceFlux(source.interfaceFlux),
                                                                                   linearSymmetrizationCoupling(source.linearSymmetrizationCoupling),
                                                                                   dFluxdNonLocalVariable(source.dFluxdNonLocalVariable),
                                                                                   dFieldSourcedNonLocalVariable(source.dFieldSourcedNonLocalVariable),
                                                                                   dMechanicalSourcedNonLocalVariable(source.dMechanicalSourcedNonLocalVariable),
                                                                                   dEMFieldSourcedNonLocalVariable(source.dEMFieldSourcedNonLocalVariable),
                                                                                   dElecDisplacementdNonLocalVariable(source.dElecDisplacementdNonLocalVariable),
                                                                                   dFluxdVectorPotential(source.dFluxdVectorPotential),
                                                                                   dFluxdVectorCurl(source.dFluxdVectorCurl),
                                                                                   dFieldSourcedVectorPotential(source.dFieldSourcedVectorPotential),
                                                                                   dMechanicalSourcedVectorPotential(source.dMechanicalSourcedVectorPotential),
                                                                                   dFieldSourcedVectorCurl(source.dFieldSourcedVectorCurl),
                                                                                   dMechanicalSourcedVectorCurl(source.dMechanicalSourcedVectorCurl),
                                                                                   demFieldSourcedVectorPotential(source.demFieldSourcedVectorPotential),
                                                                                   demFieldSourcedVectorCurl(source.demFieldSourcedVectorCurl),
                                                                                   dElecDisplacementdVectorPotential(source.dElecDisplacementdVectorPotential),
                                                                                   dElecDisplacementdVectorCurl(source.dElecDisplacementdVectorCurl),
                                                                                   demFieldSourcedF(source.demFieldSourcedF),
                                                                                   useOfEnergyConjugatedField(source.useOfEnergyConjugatedField),useOfEnergy(source.useOfEnergy),
										   energyConjugatedField(source.energyConjugatedField), gradEnergyConjugatedField(source.gradEnergyConjugatedField),
										   dFielddEnergyConjugatedField(source.dFielddEnergyConjugatedField),
									           dGradFielddEnergyConjugatedField(source.dGradFielddEnergyConjugatedField),
										   dGradFielddGradEnergyConjugatedField(source.dGradFielddGradEnergyConjugatedField),
										   energyConjugatedFieldJump(source.energyConjugatedFieldJump),
										   dEnergyConjugatedFieldJumpdFieldm(source.dEnergyConjugatedFieldJumpdFieldm),
										   dEnergyConjugatedFieldJumpdFieldp(source.dEnergyConjugatedFieldJumpdFieldp), dFluxdGradFielddF(source.dFluxdGradFielddF),
										   dFluxdGradFielddField(source.dFluxdGradFielddField)
{

};
constitutiveExtraDofDiffusionData& constitutiveExtraDofDiffusionData::operator = (const constitutiveExtraDofDiffusionData& source){
  numConstitutiveExtraDofDiffusionVariable=source.numConstitutiveExtraDofDiffusionVariable;
  numNonLocalVariable=source.numNonLocalVariable;
  numConstitutiveCurlVariable = source.numConstitutiveCurlVariable;
  field=source.field;
  gradField=source.gradField;
  flux=source.flux;
  electricDisplacement=source.electricDisplacement;

  dPdField=source.dPdField;
  dPdGradField=source.dPdGradField;
  dFluxdGradField=source.dFluxdGradField;

  dFluxdField=source.dFluxdField;
  dFluxdF=source.dFluxdF;
  dElecDisplacementdField=source.dElecDisplacementdField;
  dElecDisplacementdGradField=source.dElecDisplacementdGradField;
  dElecDisplacementdF=source.dElecDisplacementdF;
  linearK=source.linearK;
  dlinearKdF=source.dlinearKdF;
  dlinearKdField=source.dlinearKdField;
 /* EnergyK=source.EnergyK;
  dEnergyKdF=source.dEnergyKdF;
  dEnergyKdField=source.dEnergyKdField;*/

  dFluxdGradFielddF=source.dFluxdGradFielddF;
  dFluxdGradFielddField=source.dFluxdGradFielddField;

  fieldSource=source.fieldSource;
  dFieldSourcedField=source.dFieldSourcedField;
  dFieldSourcedGradField=source.dFieldSourcedGradField;
  dFieldSourcedF=source.dFieldSourcedF;
  mechanicalSource=source.mechanicalSource;
  dMechanicalSourcedField=source.dMechanicalSourcedField;
  dMechanicalSourcedGradField=source.dMechanicalSourcedGradField;
  dMechanicalSourcedF=source.dMechanicalSourcedF;

  dFluxdVectorPotential = source.dFluxdVectorPotential;
  dFluxdVectorCurl = source.dFluxdVectorCurl;
  dFieldSourcedVectorPotential = source.dFieldSourcedVectorPotential;
  dMechanicalSourcedVectorPotential = source.dMechanicalSourcedVectorPotential;
  dFieldSourcedVectorCurl = source.dFieldSourcedVectorCurl;
  dMechanicalSourcedVectorCurl = source.dMechanicalSourcedVectorCurl;

  fieldCapacityPerUnitField=source.fieldCapacityPerUnitField;
  emFieldSource=source.emFieldSource;
  demFieldSourcedField=source.demFieldSourcedField;
  demFieldSourcedGradField=source.demFieldSourcedGradField;
  fieldJump=source.fieldJump;
  oneOverFieldJump=source.oneOverFieldJump;
  dOneOverFieldJumpdFieldm=source.dOneOverFieldJumpdFieldm;
  dOneOverFieldJumpdFieldp=source.dOneOverFieldJumpdFieldp;
  interfaceFlux=source.interfaceFlux;
  linearSymmetrizationCoupling=source.linearSymmetrizationCoupling;
  dFluxdNonLocalVariable=source.dFluxdNonLocalVariable;
  dFieldSourcedNonLocalVariable=source.dFieldSourcedNonLocalVariable;
  dMechanicalSourcedNonLocalVariable=source.dMechanicalSourcedNonLocalVariable;
  dEMFieldSourcedNonLocalVariable=source.dEMFieldSourcedNonLocalVariable;
  dElecDisplacementdNonLocalVariable=source.dElecDisplacementdNonLocalVariable;

  demFieldSourcedVectorPotential=source.demFieldSourcedVectorPotential;
  demFieldSourcedVectorCurl=source.demFieldSourcedVectorCurl;
  dElecDisplacementdVectorPotential=source.dElecDisplacementdVectorPotential;
  dElecDisplacementdVectorCurl=source.dElecDisplacementdVectorCurl;
  demFieldSourcedF=source.demFieldSourcedF;

  energyConjugatedField=source.energyConjugatedField;
  gradEnergyConjugatedField=source.gradEnergyConjugatedField;
  dFielddEnergyConjugatedField=source.dFielddEnergyConjugatedField;
  dGradFielddEnergyConjugatedField=source.dGradFielddEnergyConjugatedField;
  dGradFielddGradEnergyConjugatedField=source.dGradFielddGradEnergyConjugatedField;
  energyConjugatedFieldJump=source.energyConjugatedFieldJump;
  dEnergyConjugatedFieldJumpdFieldm=source.dEnergyConjugatedFieldJumpdFieldm;
  dEnergyConjugatedFieldJumpdFieldp=source.dEnergyConjugatedFieldJumpdFieldp;

  useOfEnergy=source.useOfEnergy;
  useOfEnergyConjugatedField=source.useOfEnergyConjugatedField;

  return *this;
};

void constitutiveExtraDofDiffusionData::restart(){

    restartManager::restart(numConstitutiveExtraDofDiffusionVariable);
    restartManager::restart(numNonLocalVariable);
    restartManager::restart(numConstitutiveCurlVariable);

    restartManager::restart(field.getDataPtr(),numConstitutiveExtraDofDiffusionVariable);

    restartManager::restart(gradField);
    restartManager::restart(flux);
    restartManager::restart(electricDisplacement);
    restartManager::restart(dPdField);
    restartManager::restart(dPdGradField);
    restartManager::restart(dFluxdF);
    restartManager::restart(dElecDisplacementdF);
    for(int i=0; i<numConstitutiveExtraDofDiffusionVariable; i++)
    {
      restartManager::restart(dFluxdGradField[i]);
      restartManager::restart(dFluxdField[i]);
      restartManager::restart(dElecDisplacementdField[i]);
      restartManager::restart(dElecDisplacementdGradField[i]);
      for(int j=0; j<numConstitutiveExtraDofDiffusionVariable; j++)
      {
        restartManager::restart(dlinearKdField[i][j]);
        restartManager::restart(dFluxdGradFielddField[i][j]);
      }
      restartManager::restart(dlinearKdF[i]);
      restartManager::restart(dFluxdGradFielddF[i]);
      restartManager::restart(dFieldSourcedGradField[i]);
      restartManager::restart(dMechanicalSourcedGradField[i]);
      restartManager::restart(dFluxdVectorPotential[i]);
      restartManager::restart(dFluxdVectorCurl[i]);
      restartManager::restart(dFieldSourcedVectorPotential[i]);
      restartManager::restart(dMechanicalSourcedVectorPotential[i]);
      restartManager::restart(dFieldSourcedVectorCurl[i]);
      restartManager::restart(dMechanicalSourcedVectorCurl[i]);
      restartManager::restart(demFieldSourcedGradField[i]);
      restartManager::restart(demFieldSourcedVectorPotential[i]);
      restartManager::restart(demFieldSourcedVectorCurl[i]);
      restartManager::restart(dElecDisplacementdVectorPotential[i]);
      restartManager::restart(dElecDisplacementdVectorCurl[i]);
      restartManager::restart(dFielddEnergyConjugatedField[i]);
      restartManager::restart(dGradFielddEnergyConjugatedField[i]);
      restartManager::restart(dGradFielddGradEnergyConjugatedField[i]);
      restartManager::restart(dEnergyConjugatedFieldJumpdFieldm[i]);
      restartManager::restart(dEnergyConjugatedFieldJumpdFieldp[i]);
    }
    restartManager::restart(useOfEnergy);
    restartManager::restart(useOfEnergyConjugatedField);

    //restartManager::restart(linearK.resize(num,StdSTens3PtTmp);
    restartManager::restart(fieldSource.getDataPtr(),numConstitutiveExtraDofDiffusionVariable);
    restartManager::restart(dFieldSourcedField.getDataPtr(),
                            numConstitutiveExtraDofDiffusionVariable*numConstitutiveExtraDofDiffusionVariable);
    restartManager::restart(dFieldSourcedF);

    restartManager::restart(mechanicalSource.getDataPtr(),numConstitutiveExtraDofDiffusionVariable);
    restartManager::restart(dMechanicalSourcedField.getDataPtr(),
                            numConstitutiveExtraDofDiffusionVariable*numConstitutiveExtraDofDiffusionVariable);
    restartManager::restart(dMechanicalSourcedF);


    restartManager::restart(fieldCapacityPerUnitField.getDataPtr(),numConstitutiveExtraDofDiffusionVariable);
    restartManager::restart(emFieldSource.getDataPtr(),numConstitutiveExtraDofDiffusionVariable);
    restartManager::restart(demFieldSourcedField.getDataPtr(),
                            numConstitutiveExtraDofDiffusionVariable*numConstitutiveExtraDofDiffusionVariable);
    restartManager::restart(fieldJump.getDataPtr(),numConstitutiveExtraDofDiffusionVariable);
    restartManager::restart(oneOverFieldJump.getDataPtr(),numConstitutiveExtraDofDiffusionVariable);
    restartManager::restart(dOneOverFieldJumpdFieldm.getDataPtr(),numConstitutiveExtraDofDiffusionVariable*numConstitutiveExtraDofDiffusionVariable);
    restartManager::restart(dOneOverFieldJumpdFieldp.getDataPtr(),numConstitutiveExtraDofDiffusionVariable*numConstitutiveExtraDofDiffusionVariable);
    restartManager::restart(interfaceFlux);
    //restartManager::restart(linearSymmetrizationCoupling.resize(num,StdSTens3PtTmp);
    restartManager::restart(dFluxdNonLocalVariable);
    restartManager::restart(dFieldSourcedNonLocalVariable.getDataPtr(),numConstitutiveExtraDofDiffusionVariable*numNonLocalVariable);
    restartManager::restart(dMechanicalSourcedNonLocalVariable.getDataPtr(),numConstitutiveExtraDofDiffusionVariable*numNonLocalVariable);
    restartManager::restart(dEMFieldSourcedNonLocalVariable.getDataPtr(), numConstitutiveExtraDofDiffusionVariable*numNonLocalVariable);
    restartManager::restart(dElecDisplacementdNonLocalVariable);
    restartManager::restart(demFieldSourcedF);

    restartManager::restart(energyConjugatedField.getDataPtr(),numConstitutiveExtraDofDiffusionVariable);
    restartManager::restart(gradEnergyConjugatedField);
    restartManager::restart(energyConjugatedFieldJump.getDataPtr(),numConstitutiveExtraDofDiffusionVariable);

};

constitutiveCurlData::constitutiveCurlData(const int numCurlVar, const int numNonLocal, const int numExtraDofDiffusion)
: numConstitutiveCurlVariable(numCurlVar), numConstitutiveExtraDofDiffusionVariable(numExtraDofDiffusion),
  numNonLocalVariable(numNonLocal)
{
    if(numCurlVar > 0)
    {
        vectorPotential.resize(numCurlVar, SVector3(0.0));
        vectorCurl.resize(numCurlVar, SVector3(0.0));
        vectorField.resize(numCurlVar, SVector3(0.0));
        inductorSourceVectorField.resize(numCurlVar, SVector3(0.0));
        sourceVectorField.resize(numCurlVar, SVector3(0.0));
        dSourceVectorFielddt.resize(numCurlVar, SVector3(0.0));

        std::vector< STensor3 > StdSTen3Tmp;
        StdSTen3Tmp.resize(numCurlVar, STensor3(0.0));
        dSourceVectorFielddVectorPotential.resize(numCurlVar, StdSTen3Tmp);

        std::vector< SVector3 > StdSVec3TmpCouplingExtraDof;
        StdSVec3TmpCouplingExtraDof.resize(numExtraDofDiffusion, SVector3(0.0));
        std::vector< STensor3 > StdSTen3TmpCouplingExtraDof;
        StdSTen3TmpCouplingExtraDof.resize(numExtraDofDiffusion, STensor3(0.0));

        std::vector< SVector3 > StdSVec3TmpCouplingNonLocal;
        StdSVec3TmpCouplingNonLocal.resize(numNonLocal, SVector3(0.0));


        dVectorFielddVectorPotential.resize(numCurlVar, StdSTen3Tmp);
        dVectorFielddVectorCurl.resize(numCurlVar, StdSTen3Tmp);
        dPdVectorPotential.resize(numCurlVar, STensor33(0.0));
        dPdVectorCurl.resize(numCurlVar, STensor33(0.0));
        dVectorFielddF.resize(numCurlVar, STensor33(0.0));
        dSourceVectorFielddVectorCurl.resize(numCurlVar, StdSTen3Tmp);
        dSourceVectorFielddF.resize(numCurlVar, STensor33(0.0));
        mechanicalFieldSource.resize(numCurlVar, SVector3(0.0));
        dMechanicalFieldSourcedVectorPotential.resize(numCurlVar, StdSTen3Tmp);
        dMechanicalFieldSourcedVectorCurl.resize(numCurlVar, StdSTen3Tmp);
        dMechanicalFieldSourcedF.resize(numCurlVar, STensor33(0.0));
        dVectorFielddExtraDofField.resize(numCurlVar, StdSVec3TmpCouplingExtraDof);
        dVectorFielddGradExtraDofField.resize(numCurlVar, StdSTen3TmpCouplingExtraDof);
        dSourceVectorFielddExtraDofField.resize(numCurlVar, StdSVec3TmpCouplingExtraDof);
        dSourceVectorFielddGradExtraDofField.resize(numCurlVar, StdSTen3TmpCouplingExtraDof);
        dMechanicalFieldSourcedExtraDofField.resize(numCurlVar, StdSVec3TmpCouplingExtraDof);
        dMechanicalFieldSourcedGradExtraDofField.resize(numCurlVar, StdSTen3TmpCouplingExtraDof);
        dVectorFielddNonLocalVariable.resize(numCurlVar, StdSVec3TmpCouplingNonLocal);
        dSourceVectorFielddNonLocalVariable.resize(numCurlVar, StdSVec3TmpCouplingNonLocal);
        dMechanicalFieldSourcedNonLocalVariable.resize(numCurlVar, StdSVec3TmpCouplingNonLocal);
    }
}

constitutiveCurlData::constitutiveCurlData(const constitutiveCurlData& src)
: numConstitutiveCurlVariable(src.numConstitutiveCurlVariable), numConstitutiveExtraDofDiffusionVariable(src.numConstitutiveExtraDofDiffusionVariable),
  numNonLocalVariable(src.numNonLocalVariable), vectorPotential(src.vectorPotential), vectorCurl(src.vectorCurl),
  vectorField(src.vectorField),
  inductorSourceVectorField(src.inductorSourceVectorField),
  sourceVectorField(src.sourceVectorField),
  dSourceVectorFielddt(src.dSourceVectorFielddt),
  dSourceVectorFielddVectorCurl(src.dSourceVectorFielddVectorCurl),
  dSourceVectorFielddVectorPotential(src.dSourceVectorFielddVectorPotential),
  dVectorFielddVectorPotential(src.dVectorFielddVectorPotential),
  dVectorFielddVectorCurl(src.dVectorFielddVectorCurl),
  dPdVectorPotential(src.dPdVectorPotential),
  dPdVectorCurl(src.dPdVectorCurl),
  dVectorFielddF(src.dVectorFielddF),
  dSourceVectorFielddF(src.dSourceVectorFielddF),
  mechanicalFieldSource(src.mechanicalFieldSource),
  dMechanicalFieldSourcedVectorPotential(src.dMechanicalFieldSourcedVectorPotential),
  dMechanicalFieldSourcedVectorCurl(src.dMechanicalFieldSourcedVectorCurl),
  dMechanicalFieldSourcedF(src.dMechanicalFieldSourcedF),
  dVectorFielddExtraDofField(src.dVectorFielddExtraDofField),
  dVectorFielddGradExtraDofField(src.dVectorFielddGradExtraDofField),
  dSourceVectorFielddExtraDofField(src.dSourceVectorFielddExtraDofField),
  dSourceVectorFielddGradExtraDofField(src.dSourceVectorFielddGradExtraDofField),
  dMechanicalFieldSourcedExtraDofField(src.dMechanicalFieldSourcedExtraDofField),
  dMechanicalFieldSourcedGradExtraDofField(src.dMechanicalFieldSourcedGradExtraDofField),
  dVectorFielddNonLocalVariable(src.dVectorFielddNonLocalVariable),
  dSourceVectorFielddNonLocalVariable(src.dSourceVectorFielddNonLocalVariable),
  dMechanicalFieldSourcedNonLocalVariable(src.dMechanicalFieldSourcedNonLocalVariable)
{
    ;
}

constitutiveCurlData & constitutiveCurlData::operator=(const constitutiveCurlData& src)
{
    numConstitutiveCurlVariable = src.numConstitutiveCurlVariable;
    numConstitutiveExtraDofDiffusionVariable = src.numConstitutiveExtraDofDiffusionVariable;
    numNonLocalVariable = src.numNonLocalVariable;
    vectorPotential = src.vectorPotential;
    vectorCurl = src.vectorCurl;
    vectorField = src.vectorField;
    inductorSourceVectorField = src.inductorSourceVectorField;
    sourceVectorField = src.sourceVectorField;
    dSourceVectorFielddt = src.dSourceVectorFielddt;
    dSourceVectorFielddVectorCurl = src.dSourceVectorFielddVectorCurl;
    dSourceVectorFielddVectorPotential = src.dSourceVectorFielddVectorPotential;
    dVectorFielddVectorPotential = src.dVectorFielddVectorPotential;
    dVectorFielddVectorCurl = src.dVectorFielddVectorCurl;
    dPdVectorPotential = src.dPdVectorPotential;
    dPdVectorCurl = src.dPdVectorCurl;
    dVectorFielddF = src.dVectorFielddF;
    dSourceVectorFielddF = src.dSourceVectorFielddF;
    mechanicalFieldSource = src.mechanicalFieldSource;
    dMechanicalFieldSourcedVectorPotential = src.dMechanicalFieldSourcedVectorPotential;
    dMechanicalFieldSourcedVectorCurl = src.dMechanicalFieldSourcedVectorCurl;
    dMechanicalFieldSourcedF = src.dMechanicalFieldSourcedF;
    dVectorFielddExtraDofField = src.dVectorFielddExtraDofField;
    dVectorFielddGradExtraDofField = src.dVectorFielddGradExtraDofField;
    dSourceVectorFielddExtraDofField = src.dSourceVectorFielddExtraDofField;
    dSourceVectorFielddGradExtraDofField = src.dSourceVectorFielddGradExtraDofField;
    dMechanicalFieldSourcedExtraDofField = src.dMechanicalFieldSourcedExtraDofField;
    dMechanicalFieldSourcedGradExtraDofField = src.dMechanicalFieldSourcedGradExtraDofField;
    dVectorFielddNonLocalVariable = src.dVectorFielddNonLocalVariable;
    dSourceVectorFielddNonLocalVariable = src.dSourceVectorFielddNonLocalVariable;
    dMechanicalFieldSourcedNonLocalVariable = src.dMechanicalFieldSourcedNonLocalVariable;

    return *this;
}

void constitutiveCurlData::restart()
{
    restartManager::restart(numConstitutiveCurlVariable);
    restartManager::restart(numConstitutiveExtraDofDiffusionVariable);
    restartManager::restart(numNonLocalVariable);
    restartManager::restart(vectorPotential);
    restartManager::restart(vectorCurl);
    restartManager::restart(vectorField);
    restartManager::restart(inductorSourceVectorField);
    restartManager::restart(sourceVectorField);
    restartManager::restart(dSourceVectorFielddt);
    restartManager::restart(dVectorFielddF);
    restartManager::restart(dPdVectorPotential);
    restartManager::restart(dPdVectorCurl);
    restartManager::restart(dSourceVectorFielddF);
    restartManager::restart(mechanicalFieldSource);
    restartManager::restart(dMechanicalFieldSourcedF);
    for(int i = 0; i < numConstitutiveCurlVariable; i++)
    {
        restartManager::restart(dSourceVectorFielddVectorCurl[i]);
        restartManager::restart(dSourceVectorFielddVectorPotential[i]);
        restartManager::restart(dVectorFielddVectorPotential[i]);
        restartManager::restart(dVectorFielddVectorCurl[i]);
        restartManager::restart(dMechanicalFieldSourcedVectorPotential[i]);
        restartManager::restart(dMechanicalFieldSourcedVectorCurl[i]);
        restartManager::restart(dVectorFielddExtraDofField[i]);
        restartManager::restart(dVectorFielddGradExtraDofField[i]);
        restartManager::restart(dSourceVectorFielddExtraDofField[i]);
        restartManager::restart(dSourceVectorFielddGradExtraDofField[i]);
        restartManager::restart(dMechanicalFieldSourcedExtraDofField[i]);
        restartManager::restart(dMechanicalFieldSourcedGradExtraDofField[i]);
        restartManager::restart(dVectorFielddNonLocalVariable[i]);
        restartManager::restart(dSourceVectorFielddNonLocalVariable[i]);
        restartManager::restart(dMechanicalFieldSourcedNonLocalVariable[i]);
    }
}

dG3DIPVariable::dG3DIPVariable(const bool createBodyForceHO, const bool oninter, int numNonLocal, int extraDofDiffusion,
int curlVar, bool useOfEnergy, bool energyConjugatedField)
: dG3DIPVariableBase(), _nonlocalData(NULL), _constitutiveExtraDofDiffusionData(NULL), _constitutiveCurlData(NULL), _bodyForceForHO(NULL)
{
  _oninter = oninter;
  deformationGradient(0,0) = 1.;
  deformationGradient(1,1) = 1.;
  deformationGradient(2,2) = 1.;
  barJ = 1.;
  localJ = 1.;
  if(numNonLocal>0)
    _nonlocalData=new nonlocalData(numNonLocal,extraDofDiffusion,curlVar);
  if(extraDofDiffusion>0)
    _constitutiveExtraDofDiffusionData = new constitutiveExtraDofDiffusionData(extraDofDiffusion,numNonLocal,curlVar,
    useOfEnergy, energyConjugatedField);
  if(curlVar > 0)
    _constitutiveCurlData = new constitutiveCurlData(curlVar, numNonLocal, extraDofDiffusion);
  if(createBodyForceHO)
    _bodyForceForHO = new bodyForceForHO();

}

dG3DIPVariable::dG3DIPVariable(const dG3DIPVariable &source) : dG3DIPVariableBase(source), _nonlocalData(NULL),
_constitutiveExtraDofDiffusionData(NULL), _constitutiveCurlData(NULL), _bodyForceForHO(NULL)
{
    _oninter                  = source._oninter;
    deformationGradient       = source.deformationGradient;
    barJ                      = source.barJ;
    localJ                    = source.localJ;
    firstPiolaKirchhoffStress = source.firstPiolaKirchhoffStress;
    tangentModuli             = source.tangentModuli;
    elasticTangentModuli      = source.elasticTangentModuli;
    DGElasticTangentModuli      = source.DGElasticTangentModuli;
    referenceOutwardNormal    = source.referenceOutwardNormal;
    currentOutwardNormal      = source.currentOutwardNormal;
    jump                      = source.jump;
    incompatibleJump               = source.incompatibleJump;
    firstPiolaKirchhoffStressb4AV = source.firstPiolaKirchhoffStressb4AV;
    if(source._nonlocalData!=NULL)
    {
      _nonlocalData = new nonlocalData(*(source._nonlocalData));
    }
    if(source._constitutiveExtraDofDiffusionData!=NULL)
    {
      _constitutiveExtraDofDiffusionData = new constitutiveExtraDofDiffusionData(*(source._constitutiveExtraDofDiffusionData));
    }
    if (source._constitutiveCurlData != NULL)
    {
        _constitutiveCurlData = new constitutiveCurlData(*(source._constitutiveCurlData));
    }
    if (source._bodyForceForHO != NULL)
    {
        _bodyForceForHO  = new bodyForceForHO(*(source._bodyForceForHO));
    }
}

dG3DIPVariable & dG3DIPVariable::operator = (const IPVariable &_source)
{
    dG3DIPVariableBase::operator=(_source);
    const dG3DIPVariable *source=dynamic_cast<const dG3DIPVariable *> (&_source);
    if(source!=NULL)
    {
      _oninter                  = source->_oninter;
      barJ                      = source->barJ;
      localJ                    = source->localJ;
      deformationGradient       = source->deformationGradient;
      firstPiolaKirchhoffStress = source->firstPiolaKirchhoffStress;
      tangentModuli             = source->tangentModuli;
      elasticTangentModuli      = source->elasticTangentModuli;
      DGElasticTangentModuli      = source->DGElasticTangentModuli;
      referenceOutwardNormal    = source->referenceOutwardNormal;
      currentOutwardNormal      = source->currentOutwardNormal;
      jump                      = source->jump;
      incompatibleJump          = source->incompatibleJump;
      firstPiolaKirchhoffStressb4AV = source->firstPiolaKirchhoffStressb4AV;
      if(source->_nonlocalData!=NULL)
      {
        if(_nonlocalData==NULL)
          _nonlocalData = new nonlocalData(*(source->_nonlocalData));
        else
          _nonlocalData->operator=(*(source->_nonlocalData));
      }
      if(source->_constitutiveExtraDofDiffusionData!=NULL)
      {
        if(_constitutiveExtraDofDiffusionData==NULL)
          _constitutiveExtraDofDiffusionData = new constitutiveExtraDofDiffusionData(*(source->_constitutiveExtraDofDiffusionData));
        else
         _constitutiveExtraDofDiffusionData->operator=(*(source->_constitutiveExtraDofDiffusionData));
      }
      if (source->_constitutiveCurlData != NULL)
      {
        if (_constitutiveCurlData == NULL)
            _constitutiveCurlData = new constitutiveCurlData(*(source->_constitutiveCurlData));
        else
            _constitutiveCurlData->operator=(*(source->_constitutiveCurlData));
      }
      if (source->_bodyForceForHO != NULL )
      {
        if (_bodyForceForHO == NULL)
        {
          _bodyForceForHO = new bodyForceForHO(*(source->_bodyForceForHO));
        }
        else
        {
          _bodyForceForHO->operator=(*(source->_bodyForceForHO));
        }
      }
    }

    return *this;

}

double dG3DIPVariable::getJ(int dim) const
{
  const STensor3& F = getConstRefToDeformationGradient();
  if(dim==3)
    return STensorOperation::determinantSTensor3(F);
  else if(dim==2)
    return F(0,0)*F(1,1)-F(0,1)*F(1,0);
  else
    return F(0,0);
}

void dG3DIPVariable::getCauchyStress(STensor3 &cauchy) const
{
  double detJ = getJ();
  for (int i = 0; i< 3; i ++){
    for (int j = 0; j< 3; j ++){
      cauchy(i,j) = 0.;
      for(int k =0; k <3; k++){
        cauchy(i,j) += getConstRefToFirstPiolaKirchhoffStress()(i,k)*getConstRefToDeformationGradient()(j,k)/detJ;
      }
    }
  }
}

void dG3DIPVariable::getBackLocalDeformationGradient(STensor3& localF) const
{
  localF=getConstRefToDeformationGradient();
  double fact=getLocalJ()/getBarJ();
  localF*=pow(fact,1./3.);
};


double dG3DIPVariable::get(const int comp) const
{
  static STensor3 cauchy;
  getCauchyStress(cauchy);
  const STensor3& F = this->getConstRefToDeformationGradient();
  const STensor3& P = this->getConstRefToFirstPiolaKirchhoffStress();

  static STensor3 invF, secondPK;
  STensorOperation::inverseSTensor3(F,invF);
  STensorOperation::multSTensor3(invF,P,secondPK);

  if(comp == IPField::SIG_XX)
    return cauchy(0,0);
  else if(comp == IPField::SIG_YY)
    return cauchy(1,1);
  else if(comp == IPField::SIG_ZZ)
    return cauchy(2,2);
  else if(comp == IPField::SIG_XY)
    return cauchy(0,1);
  else if(comp == IPField::SIG_YZ)
    return cauchy(1,2);
  else if(comp == IPField::SIG_XZ)
    return cauchy(0,2);
  else if(comp == IPField::K_XX)
    return cauchy(0,0)*getJ();
  else if(comp == IPField::K_YY)
    return cauchy(1,1)*getJ();
  else if(comp == IPField::K_ZZ)
    return cauchy(2,2)*getJ();
  else if(comp == IPField::K_XY)
    return cauchy(0,1)*getJ();
  else if(comp == IPField::K_YZ)
    return cauchy(1,2)*getJ();
  else if(comp == IPField::K_XZ)
    return cauchy(0,2)*getJ();
  else if(comp == IPField::SVM) // von Mises
  {
    return vonMises();
  }
  else if (comp == IPField::KVM)
  {
    return vonMises()*getJ();
  }

  else if(comp == IPField::STRAIN_XX)
    return getConstRefToDeformationGradient()(0,0)-1.;
  else if(comp == IPField::STRAIN_YY)
    return getConstRefToDeformationGradient()(1,1)-1.;
  else if(comp == IPField::STRAIN_ZZ)
    return getConstRefToDeformationGradient()(2,2)-1.;
  else if(comp == IPField::STRAIN_XY)
    return (getConstRefToDeformationGradient()(0,1)+getConstRefToDeformationGradient()(1,0))/2.;
  else if(comp == IPField::STRAIN_YZ)
    return (getConstRefToDeformationGradient()(1,2)+getConstRefToDeformationGradient()(2,1))/2.;
  else if(comp == IPField::STRAIN_XZ)
    return (getConstRefToDeformationGradient()(0,2)+getConstRefToDeformationGradient()(2,0))/2.;
  else if (comp == IPField::VOLUMETRIC_STRAIN)
  {
    return getConstRefToDeformationGradient()(0,0) + getConstRefToDeformationGradient()(1,1) + getConstRefToDeformationGradient()(2,2) - 2.;
  }
  else if (comp == IPField::F_XX){
    return F(0,0);
	}
	else if (comp == IPField::F_XY){
    return F(0,1);
	}
	else if (comp == IPField::F_XZ){
    return F(0,2);
	}
	else if (comp == IPField::F_YX){
    return F(1,0);
	}
	else if (comp == IPField::F_YY){
    return F(1,1);
	}
	else if (comp == IPField::F_YZ){
    return F(1,2);
	}
	else if (comp == IPField::F_ZX){
    return F(2,0);
	}
	else if (comp == IPField::F_ZY){
        return F(2,1);
	}
	else if (comp == IPField::F_ZZ){
    return F(2,2);
	}
	else if (comp == IPField::P_XX){
    return P(0,0);
	}
	else if (comp == IPField::P_XY){
    return P(0,1);
	}
	else if (comp == IPField::P_XZ){
    return P(0,2);
	}
	else if (comp == IPField::P_YX){
    return P(1,0);
	}
	else if (comp == IPField::P_YY){
    return P(1,1);
	}
	else if (comp == IPField::P_YZ){
    return P(1,2);
	}
	else if (comp == IPField::P_ZX){
    return P(2,0);
	}
	else if (comp == IPField::P_ZY){
    return P(2,1);
	}
	else if (comp == IPField::P_ZZ){
    return P(2,2);
	}
	// effective P
	else if (comp == IPField::P_XX_EFF){
	double Dam = get(IPField::DAMAGE);
    return P(0,0)/(1.0-Dam);
	}
	else if (comp == IPField::P_XY_EFF){
	double Dam = get(IPField::DAMAGE);
    return P(0,1)/(1.0-Dam);
	}
	else if (comp == IPField::P_XZ_EFF){
	double Dam = get(IPField::DAMAGE);
    return P(0,2)/(1.0-Dam);
	}
	else if (comp == IPField::P_YX_EFF){
	double Dam = get(IPField::DAMAGE);
    return P(1,0)/(1.0-Dam);
	}
	else if (comp == IPField::P_YY_EFF){
	double Dam = get(IPField::DAMAGE);
    return P(1,1)/(1.0-Dam);
	}
	else if (comp == IPField::P_YZ_EFF){
	double Dam = get(IPField::DAMAGE);
    return P(1,2)/(1.0-Dam);
	}
	else if (comp == IPField::P_ZX_EFF){
	double Dam = get(IPField::DAMAGE);
    return P(2,0)/(1.0-Dam);
	}
	else if (comp == IPField::P_ZY_EFF){
	double Dam = get(IPField::DAMAGE);
    return P(2,1)/(1.0-Dam);
	}
	else if (comp == IPField::P_ZZ_EFF){
	double Dam = get(IPField::DAMAGE);
    return P(2,2)/(1.0-Dam);
	}

	else if (comp == IPField::S_XX)
  {
    return secondPK(0,0);
  }
  else if (comp == IPField::S_XY)
  {
    return secondPK(0,1);
  }
  else if (comp == IPField::S_XZ)
  {
    return secondPK(0,2);
  }
  else if (comp == IPField::S_YY)
  {
    return secondPK(1,1);
  }
  else if (comp == IPField::S_YZ)
  {
    return secondPK(1,2);
  }
  else if (comp == IPField::S_ZZ)
  {
    return secondPK(2,2);
  }
  else if (comp == IPField::JACOBIAN){
    return getJ();
  }
  else if (comp == IPField::PRESSION){
    return (cauchy(0,0)+cauchy(1,1)+cauchy(2,2))/3.;
  }
  else if (comp == IPField::FIRST_PRINCIPAL_STRESS){
    static fullMatrix<double> eigVec(3,3);
    static fullVector<double> eigVal(3);
    cauchy.eig(eigVec,eigVal,true);
    return eigVal(0);
  }
  else if (comp == IPField::SECOND_PRINCIPAL_STRESS){
    static fullMatrix<double> eigVec(3,3);
    static fullVector<double> eigVal(3);
    cauchy.eig(eigVec,eigVal,true);
    return eigVal(1);
  }
  else if (comp == IPField::THIRD_PRINCIPAL_STRESS){
    static fullMatrix<double> eigVec(3,3);
    static fullVector<double> eigVal(3);
    cauchy.eig(eigVec,eigVal,true);
    return eigVal(2);
  }
  else if (comp == IPField::LODE_PARAMETER){
    static fullMatrix<double> eigVec(3,3);
    static fullVector<double> eigVal(3);
    cauchy.eig(eigVec,eigVal,true);
    return (2*eigVal(1) - eigVal(0) - eigVal(2))/(eigVal(2)-eigVal(0));
  }
  else if (comp == IPField::OMEGA_PARAMETER)
  {
    double trSig;
    static STensor3 devSig;
    STensorOperation::decomposeDevTr(cauchy,devSig,trSig);
    double J1,J2,J3;
    STensorOperation::getThreeInvariantsSTensor3(devSig,J1,J2,J3);
    double sigEq = sqrt(3.*J2);
    return 27.*J3/(2.*sigEq*sigEq*sigEq);
  }
  else if (comp == IPField::LODE_ANGLE){
    double trSig;
    static STensor3 devSig;
    STensorOperation::decomposeDevTr(cauchy,devSig,trSig);
    double J1,J2,J3;
    STensorOperation::getThreeInvariantsSTensor3(devSig,J1,J2,J3);
    double sigEq = sqrt(3.*J2);
    double xi = 27.*J3/(2.*sigEq*sigEq*sigEq);
    const double pi = 3.14159265358979323846;
    return (acos(xi))/3.*(180./pi);
  }
  else if (comp == IPField::DISP_JUMP_X){
    return getConstRefToJump()(0);
  }
  else if (comp == IPField::DISP_JUMP_Y){
    return getConstRefToJump()(1);
  }
  else if (comp == IPField::DISP_JUMP_Z){
    return getConstRefToJump()(2);
  }
  else if (comp == IPField::STRESS_TRIAXIALITY){
    double p = (cauchy(0,0)+cauchy(1,1)+cauchy(2,2))/3.;
    double svm = vonMises();
    if (p ==0.) return 0.;
    else return p/svm;
  }
  else if (comp == IPField::INCOMPATIBLE_STRAIN_X){
    const SVector3& istrain = this->getConstRefToIncompatibleJump();
    return istrain(0);
  }
  else if (comp == IPField::INCOMPATIBLE_STRAIN_Y){
    const SVector3& istrain = this->getConstRefToIncompatibleJump();
    return istrain(1);
  }
  else if (comp == IPField::INCOMPATIBLE_STRAIN_Z){
    const SVector3& istrain = this->getConstRefToIncompatibleJump();
    return istrain(2);
  }
  else if (comp == IPField::DAMAGE_IS_BLOCKED){
    return dissipationIsBlocked();
  }
  else if (comp == IPField::ACTIVE_DISSIPATION){
    return this->dissipationIsActive();
  }
  else if (comp == IPField::DEFO_ENERGY){
    return this->defoEnergy();
  }
  else if (comp == IPField::PLASTIC_ENERGY){
    return this->plasticEnergy();
  }
  else if (comp == IPField::DAMAGE_ENERGY){
    return this->damageEnergy();
  }
  else if (comp == IPField::IRREVERSIBLE_ENERGY){
    return this->irreversibleEnergy();
  }
  else if (comp == IPField::DELETED){
    if (this->isDeleted()) return 1.;
    else return 0.;
  }
  else if (comp == IPField::GL_EQUIVALENT_STRAIN){
    static STensor3 E, devE;
    static double trE;
    STensorOperation::diag(E,-1.);
    STensorOperation::multSTensor3FirstTransposeAdd(F,F,E);
    E *= (0.5);
    STensorOperation::decomposeDevTr(E,devE,trE);
    return sqrt(2.*devE.dotprod()/3.);
  }
  else if (comp == IPField::GL_XX){
    return 0.5*(F(0,0)*F(0,0)+F(1,0)*F(1,0)+F(2,0)*F(2,0) - 1.);
  }
  else if (comp == IPField::GL_XY){
    return 0.5*(F(0,0)*F(0,1)+F(1,0)*F(1,1)+F(2,0)*F(2,1));
  }
  else if (comp == IPField::GL_XZ){
    return 0.5*(F(0,0)*F(0,2)+F(1,0)*F(1,2)+F(2,0)*F(2,2));
  }
  else if (comp == IPField::GL_YY){
    return 0.5*(F(0,1)*F(0,1)+F(1,1)*F(1,1)+F(2,1)*F(2,1) - 1.);
  }
  else if (comp == IPField::GL_YZ){
    return 0.5*(F(0,1)*F(0,2)+F(1,1)*F(1,2)+F(2,1)*F(2,2));
  }
  else if (comp == IPField::GL_ZZ){
    return 0.5*(F(0,2)*F(0,2)+F(1,2)*F(1,2)+F(2,2)*F(2,2) - 1.);
  }
  else if (comp == IPField::GL_NORM)
  {
    static STensor3 E;
    STensorOperation::diag(E,-1.);
    STensorOperation::multSTensor3FirstTransposeAdd(F,F,E);
    E *= (0.5);
    return sqrt(STensorOperation::doubledot(E,E));
  }
  else if (comp == IPField::U_NORM)
  {
    // def norm(U-I)
    static STensor3 U, R;
    STensorOperation::RUDecomposition(F,U,R);
    U(0,0) -= 1.;
    U(1,1) -= 1.;
    U(2,2) -= 1.;
    return sqrt(STensorOperation::doubledot(U,U));
  }
  else if (comp == IPField::U_XX)
  {
    static STensor3 U, R;
    STensorOperation::RUDecomposition(F,U,R);
    return U(0,0);
  }
  else if (comp == IPField::U_XY)
  {
    static STensor3 U, R;
    STensorOperation::RUDecomposition(F,U,R);
    return U(0,1);
  }
  else if (comp == IPField::U_XZ)
  {
    static STensor3 U, R;
    STensorOperation::RUDecomposition(F,U,R);
    return U(0,2);
  }
  else if (comp == IPField::U_YY)
  {
    static STensor3 U, R;
    STensorOperation::RUDecomposition(F,U,R);
    return U(1,1);
  }
  else if (comp == IPField::U_YZ)
  {
    static STensor3 U, R;
    STensorOperation::RUDecomposition(F,U,R);
    return U(1,2);
  }
  else if (comp == IPField::U_ZZ)
  {
    static STensor3 U, R;
    STensorOperation::RUDecomposition(F,U,R);
    return U(2,2);
  }
  else if (comp == IPField::LOCAL_0){
    if (getNumberNonLocalVariable()>0){
      return getConstRefToLocalVariable(0);
    }
    else return 0.;
  }
  else if (comp == IPField::LOCAL_1){
    if (getNumberNonLocalVariable()>1){
      return getConstRefToLocalVariable(1);
    }
    else return 0.;
  }
  else if (comp == IPField::LOCAL_2){
    if (getNumberNonLocalVariable() > 2){
      return getConstRefToLocalVariable(2);
    }
    else return 0.;
  }
  else if (comp == IPField::NONLOCAL_0){
    if (getNumberNonLocalVariable() > 0){
      return getConstRefToNonLocalVariable(0);
    }
    else return 0.;
  }
  else if (comp == IPField::NONLOCAL_1){
    if (getNumberNonLocalVariable()> 1){
      return getConstRefToNonLocalVariable(1);
    }
    else return 0.;
  }
  else if (comp == IPField::NONLOCAL_2){
    if (getNumberNonLocalVariable() > 2)
      return getConstRefToNonLocalVariable(2);
    else return 0.;
  }
  else if (comp == IPField::CL_XX){
    if (getNumberNonLocalVariable() > 0){
      const STensor3& CL = getConstRefToCharacteristicLengthMatrix(0);
      return CL(0,0);
    }
  }
  else if (comp == IPField::CL_YY){
    if (getNumberNonLocalVariable() > 0){
      const STensor3& CL = getConstRefToCharacteristicLengthMatrix(0);
      return CL(1,1);
    }
  }
  else if (comp == IPField::CL_ZZ){
    if (getNumberNonLocalVariable() > 0){
      const STensor3& CL = getConstRefToCharacteristicLengthMatrix(0);
      return CL(2,2);
    }
  }
  else if (comp == IPField::BODYFORCE_X){
    if(hasBodyForceForHO())
      return _bodyForceForHO->Bm(0);
    else  return 0.0;
  }
  else if (comp == IPField::BODYFORCE_Y){
    if(hasBodyForceForHO())
      return _bodyForceForHO->Bm(1);
    else  return 0.0;
  }
  else if (comp == IPField::BODYFORCE_Z){
    if(hasBodyForceForHO())
      return _bodyForceForHO->Bm(2);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_XXXX){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(0,0,0,0);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_XXXY){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(0,0,0,1);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_XXXZ){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(0,0,0,2);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_XXYX){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(0,0,1,0);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_XXYY){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(0,0,1,1);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_XXYZ){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(0,0,1,2);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_XXZX){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(0,0,2,0);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_XXZY){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(0,0,2,1);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_XXZZ){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(0,0,2,2);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_XYXX){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(0,1,0,0);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_XYXY){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(0,1,0,1);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_XYXZ){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(0,1,0,2);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_XYYX){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(0,1,1,0);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_XYYY){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(0,1,1,1);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_XYYZ){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(0,1,1,2);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_XYZX){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(0,1,2,0);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_XYZY){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(0,1,2,1);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_XYZZ){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(0,1,2,2);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_XZXX){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(0,2,0,0);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_XZXY){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(0,2,0,1);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_XZXZ){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(0,2,0,2);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_XZYX){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(0,2,1,0);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_XZYY){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(0,2,1,1);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_XZYZ){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(0,2,1,2);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_XZZX){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(0,2,2,0);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_XZZY){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(0,2,2,1);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_XZZZ){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(0,2,2,2);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_YXXX){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(1,0,0,0);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_YXXY){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(1,0,0,1);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_YXXZ){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(1,0,0,2);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_YXYX){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(1,0,1,0);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_YXYY){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(1,0,1,1);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_YXYZ){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(1,0,1,2);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_YXZX){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(1,0,2,0);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_YXZY){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(1,0,2,1);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_YXZZ){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(1,0,2,2);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_YYXX){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(1,1,0,0);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_YYXY){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(1,1,0,1);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_YYXZ){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(1,1,0,2);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_YYYX){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(1,1,1,0);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_YYYY){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(1,1,1,1);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_YYYZ){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(1,1,1,2);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_YYZX){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(1,1,2,0);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_YYZY){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(1,1,2,1);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_YYZZ){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(1,1,2,2);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_YZXX){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(1,2,0,0);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_YZXY){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(1,2,0,1);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_YZXZ){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(1,2,0,2);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_YZYX){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(1,2,1,0);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_YZYY){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(1,2,1,1);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_YZYZ){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(1,2,1,2);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_YZZX){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(1,2,2,0);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_YZZY){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(1,2,2,1);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_YZZZ){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(1,2,2,2);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_ZXXX){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(2,0,0,0);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_ZXXY){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(2,0,0,1);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_ZXXZ){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(2,0,0,2);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_ZXYX){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(2,0,1,0);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_ZXYY){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(2,0,1,1);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_ZXYZ){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(2,0,1,2);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_ZXZX){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(2,0,2,0);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_ZXZY){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(2,0,2,1);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_ZXZZ){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(2,0,2,2);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_ZYXX){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(2,1,0,0);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_ZYXY){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(2,1,0,1);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_ZYXZ){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(2,1,0,2);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_ZYYX){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(2,1,1,0);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_ZYYY){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(2,1,1,1);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_ZYYZ){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(2,1,1,2);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_ZYZX){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(2,1,2,0);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_ZYZY){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(2,1,2,1);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_ZYZZ){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(2,1,2,2);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_ZZXX){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(2,2,0,0);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_ZZXY){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(2,2,0,1);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_ZZXZ){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(2,2,0,2);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_ZZYX){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(2,2,1,0);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_ZZYY){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(2,2,1,1);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_ZZYZ){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(2,2,1,2);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_ZZZX){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(2,2,2,0);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_ZZZY){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(2,2,2,1);
    else  return 0.0;
  }
  else if (comp == IPField::dFmdFM_ZZZZ){
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM(2,2,2,2);
    else  return 0.0;
  }
  else if (comp == IPField::ONE)
  {
    return 1.;
  }
  else
     return 0.;
  return 0.;
}
double & dG3DIPVariable::getRef(const int comp)
{
    static STensor3 cauchy;
    getCauchyStress(cauchy);
    STensor3& F = this->getRefToDeformationGradient();
    STensor3& P = this->getRefToFirstPiolaKirchhoffStress();

    static STensor3 invF, secondPK;
    STensorOperation::inverseSTensor3(F,invF);
    STensorOperation::multSTensor3(invF,P,secondPK);

    if(comp == IPField::SIG_XX)
        return cauchy(0,0);
    else if(comp == IPField::SIG_YY)
        return cauchy(1,1);
    else if(comp == IPField::SIG_ZZ)
        return cauchy(2,2);
    else if(comp == IPField::SIG_XY)
        return cauchy(0,1);
    else if(comp == IPField::SIG_YZ)
        return cauchy(1,2);
    else if(comp == IPField::SIG_XZ)
        return cauchy(0,2);
    else if (comp == IPField::F_XX){
        return F(0,0);
    }
    else if (comp == IPField::F_XY){
        return F(0,1);
    }
    else if (comp == IPField::F_XZ){
        return F(0,2);
    }
    else if (comp == IPField::F_YX){
        return F(1,0);
    }
    else if (comp == IPField::F_YY){
        return F(1,1);
    }
    else if (comp == IPField::F_YZ){
        return F(1,2);
    }
    else if (comp == IPField::F_ZX){
        return F(2,0);
    }
    else if (comp == IPField::F_ZY){
        return F(2,1);
    }
    else if (comp == IPField::F_ZZ){
        return F(2,2);
    }
    else if (comp == IPField::P_XX){
        return P(0,0);
    }
    else if (comp == IPField::P_XY){
        return P(0,1);
    }
    else if (comp == IPField::P_XZ){
        return P(0,2);
    }
    else if (comp == IPField::P_YX){
        return P(1,0);
    }
    else if (comp == IPField::P_YY){
        return P(1,1);
    }
    else if (comp == IPField::P_YZ){
        return P(1,2);
    }
    else if (comp == IPField::P_ZX){
        return P(2,0);
    }
    else if (comp == IPField::P_ZY){
        return P(2,1);
    }
    else if (comp == IPField::P_ZZ){
        return P(2,2);
    }
    else if (comp == IPField::S_XX)
    {
        return secondPK(0,0);
    }
    else if (comp == IPField::S_XY)
    {
        return secondPK(0,1);
    }
    else if (comp == IPField::S_XZ)
    {
        return secondPK(0,2);
    }
    else if (comp == IPField::S_YY)
    {
        return secondPK(1,1);
    }
    else if (comp == IPField::S_YZ)
    {
        return secondPK(1,2);
    }
    else if (comp == IPField::S_ZZ)
    {
        return secondPK(2,2);
    }
    else if (comp == IPField::FIRST_PRINCIPAL_STRESS){
        static fullMatrix<double> eigVec(3,3);
        static fullVector<double> eigVal(3);
        cauchy.eig(eigVec,eigVal,true);
        return eigVal(0);
    }
    else if (comp == IPField::SECOND_PRINCIPAL_STRESS){
        static fullMatrix<double> eigVec(3,3);
        static fullVector<double> eigVal(3);
        cauchy.eig(eigVec,eigVal,true);
        return eigVal(1);
    }
    else if (comp == IPField::THIRD_PRINCIPAL_STRESS){
        static fullMatrix<double> eigVec(3,3);
        static fullVector<double> eigVal(3);
        cauchy.eig(eigVec,eigVal,true);
        return eigVal(2);
    }
    else
        Msg::Error("dG3DIPVariable::getRef: Comp %d cannot be parsed as reference", comp);
}
double dG3DIPVariable::defoEnergy() const
{
  double ene=0;
  double detJ = getJ();
  if (detJ < 1.e-12) Msg::Error("Negative Jacobian");
  // default implementation for small deformations
  static STensor3 cauchy;
  getCauchyStress(cauchy);
  static STensor3 E;
  for (int i = 0; i< 3; i ++){
    for (int j = 0; j< 3; j ++){
     E(i,j)  = getConstRefToDeformationGradient()(i,j)/2.+getConstRefToDeformationGradient()(j,i)/2.;
     if(i == j) E(i,j)-=1.;
    }
  }
  for (int i = 0; i< 3; i ++)
  {
    for (int j = 0; j< 3; j ++)
    {
       ene += cauchy(i,j)*E(i,j)/2.;
    }
  }
  return ene;
}
double dG3DIPVariable::plasticEnergy() const
{
  return 0.;
}
double dG3DIPVariable::vonMises() const
{
  static STensor3 cauchy;
  getCauchyStress(cauchy);
  double svm= (cauchy(0,0)-cauchy(1,1))*(cauchy(0,0)-cauchy(1,1))+(cauchy(0,0)-cauchy(2,2))*(cauchy(0,0)-cauchy(2,2))+
               (cauchy(2,2)-cauchy(1,1))*(cauchy(2,2)-cauchy(1,1));
     svm += 6.*(cauchy(0,1)*cauchy(1,0)+cauchy(0,2)*cauchy(2,0)+cauchy(2,1)*cauchy(1,2));
     svm /= 2.;
     svm = sqrt(svm);
     return svm;
}


void dG3DIPVariable::computeBodyForce(const STensor33& G, SVector3& B) const{
 static STensor33 tmp1;
 static STensor33 tmp2;
 static const STensor3 I(1.);

 STensorOperation::multSTensor43STensor33(_bodyForceForHO->dPmdFM, G, tmp1);
 STensorOperation::multSTensor43STensor33RightTranspose(_bodyForceForHO->dPmdFM, G, tmp2);
 STensorOperation::zero(B);
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          B(i) -= 0.5*(tmp1(i,j,k)+tmp2(i,j,k))*I(k,j);
        }
      }
    }
}


void dG3DIPVariable::setValueForBodyForce(const STensor43& K, const int _ModuliType){
 partDomain::BodyForceModuliType ModuliType=(partDomain::BodyForceModuliType)_ModuliType;
 if(hasBodyForceForHO()){
   STensorOperation::zero(_bodyForceForHO->dPmdFM);
   STensorOperation::zero(_bodyForceForHO->Bm);
   STensorOperation::zero(_bodyForceForHO->dBmdGM);
   STensorOperation::zero(_bodyForceForHO->dBmdFm);
   static const STensor3 I(1.);
   static STensor33 tmp1;
   static STensor33 tmp2;

   if(ModuliType == partDomain::Homogeneous){
     STensorOperation::multSTensor43STensor33(K, _bodyForceForHO->GM, tmp1);
     STensorOperation::multSTensor43STensor33RightTranspose(K, _bodyForceForHO->GM, tmp2);
     for (int i=0; i<3; i++){
       for (int j=0; j<3; j++){
         for (int k=0; k<3; k++){
           _bodyForceForHO->Bm(i) -= 0.5*(tmp1(i,j,k)+tmp2(i,j,k))*I(k,j);
         }
       }
     }
     for (int i=0; i<3; i++){
       for (int j=0; j<3; j++){
         for (int k=0; k<3; k++){
           for (int l=0; l<3; l++){
             for (int p=0; p<3; p++){
              _bodyForceForHO->dBmdGM(i,j,k,l) -= 0.5*(K(i,p,j,k)*I(l,p) + K(i,p,j,l)*I(k,p));
            }
          }
        }
      }
     }
   }
   else{
     STensorOperation::multSTensor43(K, _bodyForceForHO->dFmdFM, _bodyForceForHO->dPmdFM);
     STensorOperation::multSTensor43STensor33(_bodyForceForHO->dPmdFM, _bodyForceForHO->GM, tmp1);
     STensorOperation::multSTensor43STensor33RightTranspose(_bodyForceForHO->dPmdFM, _bodyForceForHO->GM, tmp2);

     for (int i=0; i<3; i++){
       for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          _bodyForceForHO->Bm(i) -= 0.5*(tmp1(i,j,k)+tmp2(i,j,k))*I(k,j);
        }
      }
     }

     for (int i=0; i<3; i++){
       for (int j=0; j<3; j++){
         for (int k=0; k<3; k++){
           for (int l=0; l<3; l++){
             for (int p=0; p<3; p++){
              _bodyForceForHO->dBmdGM(i,j,k,l) -= 0.5*(_bodyForceForHO->dPmdFM(i,p,j,k)*I(l,p)+_bodyForceForHO->dPmdFM(i,p,j,l)*I(k,p));
            }
          }
        }
      }
     }
     if(ModuliType == partDomain::Present){
       STensorOperation::multSTensor43STensor33(_bodyForceForHO->dFmdFM, _bodyForceForHO->GM, tmp1);
       STensorOperation::multSTensor43STensor33RightTranspose(_bodyForceForHO->dFmdFM, _bodyForceForHO->GM, tmp2);
       for (int i=0; i<3; i++){
         for (int j=0; j<3; j++){
           for (int k=0; k<3; k++){
             for (int l=0; l<3; l++){
                for (int p=0; p<3; p++){
                  for (int q=0; q<3; q++){
                      _bodyForceForHO->dBmdFm(i,p,q) -= _bodyForceForHO->dCmdFm(i,j,k,l,p,q)*(tmp1(k,l,j)+tmp2(k,l,j))/2.0;
                  }
                }
             }
           }
         }
       }
     }
   }
  }
  else{
  Msg::Error("Body Force is not defined");
  }
 }

void dG3DIPVariable::restart()
{
  dG3DIPVariableBase::restart();
  restartManager::restart(localJ);
  restartManager::restart(barJ);
  restartManager::restart(deformationGradient);
  restartManager::restart(firstPiolaKirchhoffStress);
  restartManager::restart(tangentModuli);
  restartManager::restart(elasticTangentModuli);
  /// Ignore this pointer???const STensor43                   *elasticTangentModuli;
  restartManager::restart(_oninter);
  restartManager::restart(referenceOutwardNormal);
  restartManager::restart(currentOutwardNormal);
  restartManager::restart(jump);
  restartManager::restart(incompatibleJump);
  if(_nonlocalData!=NULL)
    _nonlocalData->restart();
  if(_constitutiveExtraDofDiffusionData!=NULL)
    _constitutiveExtraDofDiffusionData->restart();
  if(_constitutiveCurlData != NULL)
    _constitutiveCurlData->restart();
  if(_bodyForceForHO!=NULL)
    _bodyForceForHO->restart();
  return;
};

DMNBasedDG3DIPVariable::DMNBasedDG3DIPVariable(DeepMaterialNetwork* d, const bool createBodyForceHO, const bool oninter): dG3DIPVariable(createBodyForceHO, oninter),_DMN(d){}
DMNBasedDG3DIPVariable::DMNBasedDG3DIPVariable(const DMNBasedDG3DIPVariable& src):dG3DIPVariable(src),_DMN(src._DMN){}
DMNBasedDG3DIPVariable& DMNBasedDG3DIPVariable::operator =(const IPVariable& src)
{
  dG3DIPVariable::operator=(src);
  const DMNBasedDG3DIPVariable* psrc = dynamic_cast<const DMNBasedDG3DIPVariable*>(&src);
  if (psrc!=NULL)
  {
    _DMN = psrc->_DMN;
  }
  return *this;
}
DMNBasedDG3DIPVariable::~DMNBasedDG3DIPVariable(){};

double DMNBasedDG3DIPVariable::get(const int comp) const
{
  double val =  dG3DIPVariable::get(comp);
  if (fabs(val) != 0.)
  {
    return val;
  }
  return _DMN->get(comp);
}
void DMNBasedDG3DIPVariable::restart()
{
  dG3DIPVariable::restart();
  _DMN->restart();
};
DeepMaterialNetwork* DMNBasedDG3DIPVariable::getDeepMaterialNetwork() {return _DMN;};
const DeepMaterialNetwork* DMNBasedDG3DIPVariable::getDeepMaterialNetwork() const {return _DMN;};

dG3DIPVariableMultiple::dG3DIPVariableMultiple(int numIPs, const bool createBodyForceHO, const bool oninter):
    dG3DIPVariable(createBodyForceHO, oninter),
    _numIP(numIPs),
    _IPWeights(numIPs,0),
    _IPVector(numIPs,NULL)
{
}

dG3DIPVariableMultiple::dG3DIPVariableMultiple(const dG3DIPVariableMultiple& src):
  dG3DIPVariable(src), _numIP(src._numIP), _IPWeights(src._IPWeights),
  _IPVector(src._numIP,NULL)
{
  for (int j=0; j< src._numIP; j++)
  {
    if (src._IPVector[j] != NULL)
    {
      _IPVector[j] = src._IPVector[j]->clone();
    }
  }
};
dG3DIPVariableMultiple& dG3DIPVariableMultiple::operator =(const IPVariable& src)
{
  dG3DIPVariable::operator=(src);
  const dG3DIPVariableMultiple* psrc = dynamic_cast<const dG3DIPVariableMultiple*>(&src);
  if (psrc!=NULL)
  {
    _numIP = psrc->_numIP;
    _IPWeights = psrc->_IPWeights;
    for (int j=0; j< _numIP; j++)
    {
      if ((_IPVector[j]!=NULL) and (psrc->_IPVector[j] != NULL))
      {
        _IPVector[j]->operator =(*(psrc->_IPVector[j]));
      }
      else
      {
        Msg::Error("cannot perform equal operator in dG3DIPVariableMultiple::operator =");
      }
    }
  }
  return *this;
}
dG3DIPVariableMultiple::~dG3DIPVariableMultiple()
{
  for (int j=0; j< _numIP; j++)
  {
    if (_IPVector[j] != NULL)
    {
      delete _IPVector[j];
    }
  }
}
void dG3DIPVariableMultiple::addIPv(int loc, IPVariable* ipv, double w)
{
  _IPVector[loc] = ipv;
  _IPWeights[loc] = w;
}

double dG3DIPVariableMultiple::get(const int comp) const
{
  if (comp == IPField::DEFO_ENERGY ||
      comp == IPField::PLASTIC_ENERGY ||
      comp == IPField::DAMAGE_ENERGY ||
      comp == IPField::DAMAGE )
  {
    double v = 0;
    for (int j=0; j< _numIP; j++)
    {
      v += _IPVector[j]->get(comp);
    }
    return v;
  }
  // Appended to enable use with torchbasedmaterial law @Mohib
  // USER0 and USER1 stores extra variable assigned to the torchbasedmaterial law
  else if (comp == IPField::USER0 || comp == IPField::USER1)
  {
    double v = 0;
    for (int j=0; j< _numIP; j++)
    {
      v += _IPVector[j]->get(comp);
    }
    return v;
  }
  else
  {
    return dG3DIPVariable::get(comp);
  }
};

void dG3DIPVariableMultiple::restart()
{
  dG3DIPVariable::restart();
  restartManager::restart(_numIP);
  restartManager::restart(_IPWeights);
  restartManager::restart(_IPVector);
  for (int j=0; j< _numIP; j++)
  {
    _IPVector[j]->restart();
  };
};

bool dG3DIPVariableMultiple::withDeepMN() const
{
  for (int j=0; j< _numIP; j++)
  {
    if (_IPVector[j]->withDeepMN())
    {
      return true;
    }
  }
  return false;
}
void dG3DIPVariableMultiple::getAllDeepMNs(std::vector<DeepMaterialNetwork*>& allDMN)
{
  allDMN.clear();
  for (int j=0; j< _numIP; j++)
  {
    std::vector<DeepMaterialNetwork*> allDMN_part;
    _IPVector[j]->getAllDeepMNs(allDMN_part);
    for (int k=0; k< allDMN_part.size(); k++)
    {
      allDMN.push_back(allDMN_part[k]);
    }
  }
};

double dG3DIPVariableMultiple::defoEnergy() const
{
  double v = 0;
  for (int j=0; j< _numIP; j++)
  {
    v += dynamic_cast<const dG3DIPVariableBase*>(_IPVector[j])->defoEnergy();
  }
  return v;
}
double dG3DIPVariableMultiple::plasticEnergy() const
{
  double v = 0;
  for (int j=0; j< _numIP; j++)
  {
    v += dynamic_cast<const dG3DIPVariableBase*>(_IPVector[j])->plasticEnergy();
  }
  return v;
}
double dG3DIPVariableMultiple::damageEnergy() const
{
  double v = 0;
  for (int j=0; j< _numIP; j++)
  {
    v += dynamic_cast<const dG3DIPVariableBase*>(_IPVector[j])->damageEnergy();
  }
  return v;
}

ANNBasedDG3DIPVariable::ANNBasedDG3DIPVariable(const int n, const bool createBodyForceHO, const bool oninter): dG3DIPVariable(createBodyForceHO, oninter),_internalVars(1,n),_kinematicVariables(1,6), _stressVars(1,6)
{

};
ANNBasedDG3DIPVariable::ANNBasedDG3DIPVariable(const ANNBasedDG3DIPVariable &source):dG3DIPVariable(source),
  _internalVars(source._internalVars), _kinematicVariables(source._kinematicVariables),_stressVars(source._stressVars){};
ANNBasedDG3DIPVariable& ANNBasedDG3DIPVariable::operator =(const IPVariable& src)
{
  dG3DIPVariable::operator=(src);
  const ANNBasedDG3DIPVariable* psrc = dynamic_cast<const ANNBasedDG3DIPVariable*>(&src);
  if (psrc!=NULL)
  {
    _internalVars = psrc->_internalVars;
    _kinematicVariables = psrc->_kinematicVariables;
    _stressVars = psrc->_stressVars;
  }
  return *this;
}
ANNBasedDG3DIPVariable::~ANNBasedDG3DIPVariable(){};

void ANNBasedDG3DIPVariable::restart()
{
  dG3DIPVariable::restart();
  restartManager::restart(_internalVars.getDataPtr(),_internalVars.size1()*_internalVars.size2());
  restartManager::restart(_kinematicVariables.getDataPtr(),_kinematicVariables.size1()*_kinematicVariables.size2());
  restartManager::restart(_stressVars.getDataPtr(),_stressVars.size1()*_stressVars.size2());
};

double ANNBasedDG3DIPVariable::get(const int comp) const
{
  if (comp == IPField::USER0)
  {
    if (_internalVars.size2() >= 1) return _internalVars(0,0);
    else return 0.;
  }
  else if (comp == IPField::USER1)
  {
    if (_internalVars.size2() >= 2) return _internalVars(0,1);
    else return 0.;
  }
  else if (comp == IPField::USER2)
  {
    if (_internalVars.size2() >= 3) return _internalVars(0,2);
    else return 0.;
  }
  else if (comp == IPField::USER3)
  {
    if (_internalVars.size2() >= 4) return _internalVars(0,3);
    else return 0.;
  }
  else if (comp == IPField::USER4)
  {
    if (_internalVars.size2() >= 5) return _internalVars(0,4);
    else return 0.;
  }
  else if (comp == IPField::USER5)
  {
    if (_internalVars.size2() >= 6) return _internalVars(0,5);
    else return 0.;
  }
  else if (comp == IPField::USER6)
  {
    if (_internalVars.size2() >= 7) return _internalVars(0,6);
    else return 0.;
  }
  else if (comp == IPField::USER7)
  {
    if (_internalVars.size2() >= 8) return _internalVars(0,7);
    else return 0.;
  }
  else if (comp == IPField::USER8)
  {
    if (_internalVars.size2() >= 9) return _internalVars(0,8);
    else return 0.;
  }
  else if (comp == IPField::USER9)
  {
    if (_internalVars.size2() >= 10) return _internalVars(0,9);
    else return 0.;
  }
  else if (comp == IPField::USER10)
  {
    if (_internalVars.size2() >= 11) return _internalVars(0,10);
    else return 0.;
  }
  else
  {
    return dG3DIPVariable::get(comp);
  }
}


StochDMNDG3DIPVariable::StochDMNDG3DIPVariable(const int NRoot,const int NInterface, const bool createBodyForceHO, const bool oninter):
    dG3DIPVariable(createBodyForceHO, oninter), _NRoot(NRoot), _NInterface(NInterface), _IPVector(NRoot,NULL){
    bool resizeFlag;
    int entryFP = 9*_NRoot;
    int entry_a = 3*_NInterface;
    resizeFlag = Fvec.resize(entryFP, true);
    resizeFlag = Pvec.resize(entryFP, true);
    resizeFlag = a.resize(entry_a, true);
 }

StochDMNDG3DIPVariable::StochDMNDG3DIPVariable(const StochDMNDG3DIPVariable& src):
  dG3DIPVariable(src), _NRoot(src._NRoot), Fvec(src.Fvec), Pvec(src.Pvec), a(src.a),_IPVector(src._NRoot,NULL){
    for (int j=0; j< src._NRoot; j++){
      if (src._IPVector[j] != NULL){
        _IPVector[j] = src._IPVector[j]->clone();
      }
    }
  }


StochDMNDG3DIPVariable& StochDMNDG3DIPVariable::operator =(const IPVariable& src)
{
  dG3DIPVariable::operator=(src);
  const StochDMNDG3DIPVariable* psrc = dynamic_cast<const StochDMNDG3DIPVariable*>(&src);
  if (psrc!=NULL)
  {
    _NRoot = psrc->_NRoot;
    Fvec = psrc->Fvec;
    Pvec = psrc->Pvec;
    a = psrc->a;
    for (int j=0; j< _NRoot; j++)
    {
      if ((_IPVector[j]!=NULL) and (psrc->_IPVector[j] != NULL))
      {
        _IPVector[j]->operator =(*(psrc->_IPVector[j]));
      }
      else
      {
        Msg::Error("cannot perform equal operator in StochDMNDG3DIPVariable::operator =");
      }
    }
  }
  return *this;
}

StochDMNDG3DIPVariable::~StochDMNDG3DIPVariable()
{
  for (int j=0; j< _NRoot; j++)
  {
    if (_IPVector[j] != NULL)
    {
      delete _IPVector[j];
    }
  }
}

void StochDMNDG3DIPVariable::addIPv(int loc, IPVariable* ipv)
{
  _IPVector[loc] = ipv;
}

double StochDMNDG3DIPVariable::get(const int comp) const
{
  if (comp == IPField::DEFO_ENERGY ||
      comp == IPField::PLASTIC_ENERGY ||
      comp == IPField::DAMAGE_ENERGY ||
      comp == IPField::DAMAGE )
  {
    double v = 0;
    for (int j=0; j< _NRoot; j++)
    {
      v += _IPVector[j]->get(comp);
    }
    return v;
  }
  else
  {
    return dG3DIPVariable::get(comp);
  }
};


void StochDMNDG3DIPVariable::restart()
{
  dG3DIPVariable::restart();
  restartManager::restart(_NRoot);
  restartManager::restart(Fvec.getDataPtr(),9*_NRoot);
  restartManager::restart(Pvec.getDataPtr(),9*_NRoot);
  restartManager::restart(a.getDataPtr(),3*_NInterface);
  restartManager::restart(_IPVector);
  for (int j=0; j< _NRoot; j++){
    _IPVector[j]->restart();
  }
};



double StochDMNDG3DIPVariable::defoEnergy() const
{
  double v = 0;
  for (int j=0; j< _NRoot; j++)
  {
    v += dynamic_cast<const dG3DIPVariableBase*>(_IPVector[j])->defoEnergy();
  }
  return v;
}
double StochDMNDG3DIPVariable::plasticEnergy() const
{
  double v = 0;
  for (int j=0; j< _NRoot; j++)
  {
    v += dynamic_cast<const dG3DIPVariableBase*>(_IPVector[j])->plasticEnergy();
  }
  return v;
}
double StochDMNDG3DIPVariable::damageEnergy() const
{
  double v = 0;
  for (int j=0; j< _NRoot; j++)
  {
    v += dynamic_cast<const dG3DIPVariableBase*>(_IPVector[j])->damageEnergy();
  }
  return v;
}



//torchANNBasedDG3DIPVariable::torchANNBasedDG3DIPVariable(const int n, const double initial_h, const bool createBodyForceHO, const bool oninter):
//                 dG3DIPVariable(createBodyForceHO, oninter), _n(n), _initial_h(initial_h), restart_internalVars(1,n), _kinematicVariables(1,6)
//{
//#if defined(HAVE_TORCH)
//   _internalVars = _initial_h*torch::ones({1, 1, _n});
//#endif
//}

// Overloading for handling additional params at GP @Mohib
torchANNBasedDG3DIPVariable::torchANNBasedDG3DIPVariable(const int n, const double initial_h, const bool createBodyForceHO, const bool oninter, const int extraInput, const std::vector<double> initialValue_Extra):
        dG3DIPVariable(createBodyForceHO, oninter), _n(n), _initial_h(initial_h), restart_internalVars(1,n), _kinematicVariables(1,6), _extraVariables(1, extraInput)
{
#if defined(HAVE_TORCH)
    _internalVars = _initial_h*torch::ones({1, 1, _n});
#endif
    for(int i = 0; i < extraInput; i++){
        _extraVariables(0, i) = initialValue_Extra[i];
    }
}


torchANNBasedDG3DIPVariable::torchANNBasedDG3DIPVariable(const torchANNBasedDG3DIPVariable &source):dG3DIPVariable(source), _n(source._n),
     _initial_h(source._initial_h), _internalVars(source._internalVars), _kinematicVariables(source._kinematicVariables), _extraVariables(source._extraVariables){};

torchANNBasedDG3DIPVariable& torchANNBasedDG3DIPVariable::operator =(const IPVariable& src)
{
  dG3DIPVariable::operator=(src);
#if defined(HAVE_TORCH)
  const torchANNBasedDG3DIPVariable* psrc = dynamic_cast<const torchANNBasedDG3DIPVariable*>(&src);
  if (psrc!=NULL)
  {
    _internalVars = psrc->_internalVars;
    _kinematicVariables = psrc->_kinematicVariables;
    _extraVariables = psrc->_extraVariables; // Include extra parameters at GP when using overloaded(=) @Mohib
  }
#else
  Msg::Error("NOT COMPILED WITH TORCH");
#endif
  return *this;
}
torchANNBasedDG3DIPVariable::~torchANNBasedDG3DIPVariable(){};

void torchANNBasedDG3DIPVariable::restart()
{
#if defined(HAVE_TORCH)
  auto Vars_a = _internalVars.accessor<float,3>();
  
  for(int i=0;i<_n;i++) restart_internalVars(0,i) = Vars_a[0][0][i];
  
  dG3DIPVariable::restart();
  restartManager::restart(restart_internalVars.getDataPtr(),restart_internalVars.size1()*restart_internalVars.size2());
  restartManager::restart(_kinematicVariables.getDataPtr(),_kinematicVariables.size1()*_kinematicVariables.size2());
  for(int i=0;i<_n;i++) _internalVars[0][0][i] = restart_internalVars(0,i);
#else
  Msg::Error("NOT COMPILED WITH TORCH");
#endif
}

double torchANNBasedDG3DIPVariable::get(const int comp) const
{
#if defined(HAVE_TORCH)
   auto Vars_a = _internalVars.accessor<float,3>();
  if (comp == IPField::USER0)
  {
    if (_extraVariables.size2() > 0) return _extraVariables(0,0);
    else return 0.;
  }
  else if (comp == IPField::USER1)
  {
    if (_extraVariables.size2() > 1) return _extraVariables(0,1);
    else return 0.;
  }
  else if (comp == IPField::USER2)
  {
    if (_extraVariables.size2() > 2) return _extraVariables(0,2);
    else return 0.;
  }
  else  if (comp == IPField::USER3)
  {
    if (_n > 1) return Vars_a[0][0][0];
    else return 0.;
  }
  else if (comp == IPField::USER4)
  {
    if (_n > 2) return Vars_a[0][0][1];
    else return 0.;
  }
  else if (comp == IPField::USER5)
  {
    if (_n > 3) return Vars_a[0][0][2];
    else return 0.;
  }
  else if (comp == IPField::USER6)
  {
    if (_n > 4) return Vars_a[0][0][3];
    else return 0.;
  }
  else if (comp == IPField::USER7)
  {
    if (_n > 5) return Vars_a[0][0][4];
    else return 0.;
  }
  else if (comp == IPField::USER8)
  {
    if (_n > 6) return Vars_a[0][0][5];
    else return 0.;
  }
  else if (comp == IPField::USER9)
  {
    if (_n > 7) return Vars_a[0][0][6];
    else return 0.;
  }
  else if (comp == IPField::USER10)
  {
    if (_n > 8) return Vars_a[0][0][7];
    else return 0.;
  }
  /*else if (comp == IPField::USER8)
  {
    if (_n > 9) return Vars_a[0][0][8];
    else return 0.;
  }
  else if (comp == IPField::USER9)
  {
    if (_n > 10) return Vars_a[0][0][9];
    else return 0.;
  }
  else if (comp == IPField::USER10)
  {
    if (_n > 11) return Vars_a[0][0][10];
    else return 0.;
  }*/
  else
  {
    return dG3DIPVariable::get(comp);
  }
#else
  Msg::Error("NOT COMPILED WITH TORCH");
  return 0;
#endif
}



J2SmallStrainDG3DIPVariable::J2SmallStrainDG3DIPVariable(const mlawJ2VMSmallStrain&_j2law, const bool createBodyForceHO, const bool oninter) : dG3DIPVariable(createBodyForceHO, oninter)
{
  _j2ipv = new   IPJ2linear(_j2law.getJ2IsotropicHardening());
}
J2SmallStrainDG3DIPVariable::J2SmallStrainDG3DIPVariable(const J2SmallStrainDG3DIPVariable &source) : dG3DIPVariable(source)
{
 _j2ipv = NULL;
  if(source._j2ipv != NULL){
    _j2ipv = dynamic_cast<IPJ2linear*>(source._j2ipv->clone());
  }

}
J2SmallStrainDG3DIPVariable& J2SmallStrainDG3DIPVariable::operator=(const IPVariable &source)
{
  dG3DIPVariable::operator=(source);
  const J2SmallStrainDG3DIPVariable* src = dynamic_cast<const J2SmallStrainDG3DIPVariable*>(&source);
  if(src != NULL){
    if (src->_j2ipv != NULL){
      if (_j2ipv != NULL) {
        _j2ipv->operator=(*dynamic_cast<IPVariable*>(src->_j2ipv));
      }
      else{
        _j2ipv = dynamic_cast<IPJ2linear*>(src->_j2ipv->clone());
      }
    }
    else{
      if (_j2ipv) {
        delete _j2ipv;
       _j2ipv = NULL;
      }
    }

  }
  return *this;
}
double J2SmallStrainDG3DIPVariable::get(const int comp) const
{
  if(comp == IPField::PLASTICSTRAIN)
  {
    return _j2ipv->_j2lepspbarre;
  }
  else if(comp == IPField::FP_XX)
  {
    return _j2ipv->_j2lepsp(0,0);
  }
  else if(comp == IPField::FP_XY)
  {
    return _j2ipv->_j2lepsp(0,1);
  }
  else if(comp == IPField::FP_XZ)
  {
    return _j2ipv->_j2lepsp(0,2);
  }
  else if(comp == IPField::FP_YX)
  {
    return _j2ipv->_j2lepsp(1,0);
  }
  else if(comp == IPField::FP_YY)
  {
    return _j2ipv->_j2lepsp(1,1);
  }
  else if(comp == IPField::FP_YZ)
  {
    return _j2ipv->_j2lepsp(1,2);
  }
  else if(comp == IPField::FP_ZX)
  {
    return _j2ipv->_j2lepsp(2,0);
  }
  else if(comp == IPField::FP_ZY)
  {
    return _j2ipv->_j2lepsp(2,1);
  }
  else if(comp == IPField::FP_ZZ)
  {
    return _j2ipv->_j2lepsp(2,2);
  }
  else if (comp == IPField::ISO_YIELD or
           comp == IPField::ISO_YIELD_TENSILE or
           comp == IPField::ISO_YIELD_COMPRESSION)
  {
    return _j2ipv->getConstRefToIPJ2IsotropicHardening().getR();
  }
  else if (comp == IPField::ISO_YIELD_SHEAR)
  {
    return (_j2ipv->getConstRefToIPJ2IsotropicHardening().getR())/sqrt(3.);
  }
  else
  {
    //return dG3DIPVariable::get(comp);
    double val = _j2ipv->get(comp);
    if (val == 0.)
      val = dG3DIPVariable::get(comp);
    return val;
  }
}

double  J2SmallStrainDG3DIPVariable::defoEnergy() const
{
  return getIPJ2linear()->defoEnergy();
}
double  J2SmallStrainDG3DIPVariable::plasticEnergy() const
{
  return getIPJ2linear()->plasticEnergy();
}

void J2SmallStrainDG3DIPVariable::restart()
{
  dG3DIPVariable::restart();
  restartManager::restart(_j2ipv);
  return;
}

AnisotropicPlasticityDG3DIPVariable::AnisotropicPlasticityDG3DIPVariable(const mlawAnisotropicPlasticity &anisoLaw, const bool createBodyForceHO, const bool oninter): dG3DIPVariable(createBodyForceHO, oninter)
{
  _ipv = new  IPAnisotropicPlasticity(anisoLaw.getJ2IsotropicHardening());
}
AnisotropicPlasticityDG3DIPVariable::AnisotropicPlasticityDG3DIPVariable(const AnisotropicPlasticityDG3DIPVariable &source) : dG3DIPVariable(source)
{
  _ipv = NULL;
  if(source._ipv != NULL){
    _ipv = dynamic_cast<IPAnisotropicPlasticity*>(source._ipv->clone());
  }
}
AnisotropicPlasticityDG3DIPVariable& AnisotropicPlasticityDG3DIPVariable::operator=(const IPVariable &source)
{
  dG3DIPVariable::operator=(source);
  const AnisotropicPlasticityDG3DIPVariable* src = dynamic_cast<const AnisotropicPlasticityDG3DIPVariable*>(&source);
  if(src != NULL){
    if (src->_ipv != NULL){
      if (_ipv != NULL) {
        _ipv->operator=(*dynamic_cast<const IPVariable*>(src->_ipv));
      }
      else{
        _ipv = dynamic_cast<IPAnisotropicPlasticity*>(src->_ipv->clone());
      }
    }
    else{
      if (_ipv) {
        delete _ipv;
       _ipv = NULL;
      }
    }

  }
  return *this;
}
double AnisotropicPlasticityDG3DIPVariable::get(const int comp) const
{
  if(comp == IPField::PLASTICSTRAIN)
  {
    return _ipv->_j2lepspbarre;
  }
  else if(comp == IPField::FP_XX)
  {
    return _ipv->_j2lepsp(0,0);
  }
  else if(comp == IPField::FP_XY)
  {
    return _ipv->_j2lepsp(0,1);
  }
  else if(comp == IPField::FP_XZ)
  {
    return _ipv->_j2lepsp(0,2);
  }
  else if(comp == IPField::FP_YX)
  {
    return _ipv->_j2lepsp(1,0);
  }
  else if(comp == IPField::FP_YY)
  {
    return _ipv->_j2lepsp(1,1);
  }
  else if(comp == IPField::FP_YZ)
  {
    return _ipv->_j2lepsp(1,2);
  }
  else if(comp == IPField::FP_ZX)
  {
    return _ipv->_j2lepsp(2,0);
  }
  else if(comp == IPField::FP_ZY)
  {
    return _ipv->_j2lepsp(2,1);
  }
  else if(comp == IPField::FP_ZZ)
  {
    return _ipv->_j2lepsp(2,2);
  }
  else if (comp == IPField::ISO_YIELD or
           comp == IPField::ISO_YIELD_TENSILE or
           comp == IPField::ISO_YIELD_COMPRESSION)
  {
    return _ipv->getConstRefToIPJ2IsotropicHardening().getR();
  }
  else if (comp == IPField::ISO_YIELD_SHEAR)
  {
    return (_ipv->getConstRefToIPJ2IsotropicHardening().getR())/sqrt(3.);
  }
  else if (comp == IPField::PRESSION_EFF)
  {
    return _ipv->getConstRefToEffectivePressure();
  }
  else if (comp == IPField::SVM_EFF) 
  {
    return _ipv->getConstRefToEffectiveSVM();
  }
  else if (comp == IPField::STRESS_TRIAXIALITY_EFF) 
  {
    return _ipv->getConstRefToEffectivePressure()/_ipv->getConstRefToEffectiveSVM();
  }
  else
  {
    //return dG3DIPVariable::get(comp);
    double val = _ipv->get(comp);
    if (val == 0.)
      val = dG3DIPVariable::get(comp);
    return val;
  }
}
double  AnisotropicPlasticityDG3DIPVariable::defoEnergy() const
{
  return getIPAnisotropicPlasticity()->defoEnergy();
}
double  AnisotropicPlasticityDG3DIPVariable::plasticEnergy() const
{
  return getIPAnisotropicPlasticity()->plasticEnergy();
}

void AnisotropicPlasticityDG3DIPVariable::restart()
{
  dG3DIPVariable::restart();
  restartManager::restart(_ipv);
  return;
}


J2LinearDG3DIPVariable::J2LinearDG3DIPVariable(const mlawJ2linear &_j2law, const bool createBodyForceHO, const bool oninter): dG3DIPVariable(createBodyForceHO, oninter)
{
  _j2ipv = new   IPJ2linear(_j2law.getJ2IsotropicHardening());
}
J2LinearDG3DIPVariable::J2LinearDG3DIPVariable(const J2LinearDG3DIPVariable &source) : dG3DIPVariable(source)
{
  _j2ipv = NULL;
  if(source._j2ipv != NULL){
    _j2ipv = dynamic_cast<IPJ2linear*>(source._j2ipv->clone());
  }
}
J2LinearDG3DIPVariable& J2LinearDG3DIPVariable::operator=(const IPVariable &source)
{
  dG3DIPVariable::operator=(source);
  const J2LinearDG3DIPVariable* src = dynamic_cast<const J2LinearDG3DIPVariable*>(&source);
  if(src != NULL){
    if (src->_j2ipv != NULL){
      if (_j2ipv != NULL) {
        _j2ipv->operator=(*dynamic_cast<const IPVariable*>(src->_j2ipv));
      }
      else{
        _j2ipv = dynamic_cast<IPJ2linear*>(src->_j2ipv->clone());
      }
    }
    else{
      if (_j2ipv) {
        delete _j2ipv;
       _j2ipv = NULL;
      }
    }

  }
  return *this;
}
double J2LinearDG3DIPVariable::get(const int comp) const
{
  if(comp == IPField::PLASTICSTRAIN)
  {
    return _j2ipv->_j2lepspbarre;
  }
  else if(comp == IPField::FP_XX)
  {
    return _j2ipv->_j2lepsp(0,0);
  }
  else if(comp == IPField::FP_XY)
  {
    return _j2ipv->_j2lepsp(0,1);
  }
  else if(comp == IPField::FP_XZ)
  {
    return _j2ipv->_j2lepsp(0,2);
  }
  else if(comp == IPField::FP_YX)
  {
    return _j2ipv->_j2lepsp(1,0);
  }
  else if(comp == IPField::FP_YY)
  {
    return _j2ipv->_j2lepsp(1,1);
  }
  else if(comp == IPField::FP_YZ)
  {
    return _j2ipv->_j2lepsp(1,2);
  }
  else if(comp == IPField::FP_ZX)
  {
    return _j2ipv->_j2lepsp(2,0);
  }
  else if(comp == IPField::FP_ZY)
  {
    return _j2ipv->_j2lepsp(2,1);
  }
  else if(comp == IPField::FP_ZZ)
  {
    return _j2ipv->_j2lepsp(2,2);
  }
  else if (comp == IPField::ISO_YIELD or
           comp == IPField::ISO_YIELD_TENSILE or
           comp == IPField::ISO_YIELD_COMPRESSION)
  {
    return _j2ipv->getConstRefToIPJ2IsotropicHardening().getR();
  }
  else if (comp == IPField::ISO_YIELD_SHEAR)
  {
    return (_j2ipv->getConstRefToIPJ2IsotropicHardening().getR())/sqrt(3.);
  }
  else
  {
    //return dG3DIPVariable::get(comp);
    double val = _j2ipv->get(comp);
    if (val == 0.)
      val = dG3DIPVariable::get(comp);
    return val;
  }
}
double  J2LinearDG3DIPVariable::defoEnergy() const
{
  return getIPJ2linear()->defoEnergy();
}
double  J2LinearDG3DIPVariable::plasticEnergy() const
{
  return getIPJ2linear()->plasticEnergy();
}

void J2LinearDG3DIPVariable::restart()
{
  dG3DIPVariable::restart();
  restartManager::restart(_j2ipv);
  return;
}

HyperViscoElasticdG3DIPVariable::HyperViscoElasticdG3DIPVariable(const mlawHyperViscoElastic& viscoLaw, const bool createBodyForceHO, const bool oninter): dG3DIPVariable(createBodyForceHO, oninter){
  _ipViscoElastic = new IPHyperViscoElastic(viscoLaw.getViscoElasticNumberOfElement());
};
HyperViscoElasticdG3DIPVariable::HyperViscoElasticdG3DIPVariable(const HyperViscoElasticdG3DIPVariable& src): dG3DIPVariable(src){
  _ipViscoElastic = NULL;
  if (src._ipViscoElastic != NULL){
    _ipViscoElastic = dynamic_cast<IPHyperViscoElastic*>(src._ipViscoElastic->clone());
  }
};
HyperViscoElasticdG3DIPVariable& HyperViscoElasticdG3DIPVariable::operator = (const IPVariable& src){
  dG3DIPVariable::operator =(src);
  const HyperViscoElasticdG3DIPVariable* psrc = dynamic_cast<const HyperViscoElasticdG3DIPVariable*>(&src);
  if (psrc != NULL){
    if (psrc->_ipViscoElastic != NULL){
      if (_ipViscoElastic!= NULL){
        _ipViscoElastic->operator=(*dynamic_cast<const IPVariable*>(psrc->_ipViscoElastic));
      }
      else{
        _ipViscoElastic = dynamic_cast<IPHyperViscoElastic*>(psrc->_ipViscoElastic->clone());
      }
    }
  }
  return *this;
};
HyperViscoElasticdG3DIPVariable::~HyperViscoElasticdG3DIPVariable(){
  if (_ipViscoElastic){delete _ipViscoElastic; _ipViscoElastic=NULL;};
};

double HyperViscoElasticdG3DIPVariable::get(const int i) const
{
  if (i == IPField::Ee_XX)
  {
    return _ipViscoElastic->_Ee(0,0);
  }
  else if (i == IPField::Ee_XY)
  {
    return _ipViscoElastic->_Ee(0,1);
  }
  else if (i == IPField::Ee_XZ)
  {
    return _ipViscoElastic->_Ee(0,2);
  }
  else if (i == IPField::Ee_YY)
  {
    return _ipViscoElastic->_Ee(1,1);
  }
  else if (i == IPField::Ee_YZ)
  {
    return _ipViscoElastic->_Ee(1,2);
  }
  else if (i == IPField::Ee_ZZ)
  {
    return _ipViscoElastic->_Ee(2,2);
  }
  else if (i == IPField::hyperElastic_BulkScalar){return _ipViscoElastic->getConstRefToElasticBulkPropertyScaleFactor();}
  else if (i == IPField::hyperElastic_ShearScalar){return _ipViscoElastic->getConstRefToElasticShearPropertyScaleFactor();}
  else if (i == IPField::Eve1_XX)
  {
    if (_ipViscoElastic->_A.size() > 0)
      return _ipViscoElastic->_A[0](0,0) + _ipViscoElastic->_B[0]/3.;
    else
      return 0.;
  }
  else if (i == IPField::Eve1_XY)
  {
    if (_ipViscoElastic->_A.size() > 0)
      return _ipViscoElastic->_A[0](0,1);
    else return 0.;
  }
  else if (i == IPField::Eve1_XZ)
  {
    if (_ipViscoElastic->_A.size() > 0)
      return _ipViscoElastic->_A[0](0,2);
    return 0.;
  }
  else if (i == IPField::Eve1_YY)
  {
    if (_ipViscoElastic->_A.size() > 0)
      return _ipViscoElastic->_A[0](1,1) + _ipViscoElastic->_B[0]/3.;
    else return 0.;
  }
  else if (i == IPField::Eve1_YZ)
  {
    if (_ipViscoElastic->_A.size() > 0)
      return _ipViscoElastic->_A[0](1,2);
    else return 0.;
  }
  else if (i == IPField::Eve1_ZZ)
  {
    if (_ipViscoElastic->_A.size() > 0)
      return _ipViscoElastic->_A[0](2,2) + _ipViscoElastic->_B[0]/3.;
    else return 0.;
  }
  else if (i == IPField::Eve2_XX)
  {
    if (_ipViscoElastic->_A.size() > 1)
      return _ipViscoElastic->_A[1](0,0) + _ipViscoElastic->_B[1]/3.;
    else return 0.;
  }
  else if (i == IPField::Eve2_XY)
  {
    if (_ipViscoElastic->_A.size() > 1)
      return _ipViscoElastic->_A[1](0,1);
    else return 0.;
  }
  else if (i == IPField::Eve2_XZ)
  {
    if (_ipViscoElastic->_A.size() > 1)
      return _ipViscoElastic->_A[1](0,2);
    else return 0.;
  }
  else if (i == IPField::Eve2_YY)
  {
    if (_ipViscoElastic->_A.size() > 1)
      return _ipViscoElastic->_A[1](1,1) + _ipViscoElastic->_B[1]/3.;
    else return 0.;
  }
  else if (i == IPField::Eve2_YZ)
  {
    if (_ipViscoElastic->_A.size() > 1)
      return _ipViscoElastic->_A[1](1,2);
    else return 0.;
  }
  else if (i == IPField::Eve2_ZZ)
  {
    if (_ipViscoElastic->_A.size() > 1)
      return _ipViscoElastic->_A[1](2,2) + _ipViscoElastic->_B[1]/3.;
    else return 0.;
  }
  else if (i == IPField::corKir_XX)
  {
    return _ipViscoElastic->_kirchhoff(0,0);
  }
  else if (i == IPField::corKir_XY)
  {
    return _ipViscoElastic->_kirchhoff(0,1);
  }
  else if (i == IPField::corKir_XZ)
  {
    return _ipViscoElastic->_kirchhoff(0,2);
  }
  else if (i == IPField::corKir_YY)
  {
    return _ipViscoElastic->_kirchhoff(1,1);
  }
  else if (i == IPField::corKir_YZ)
  {
    return _ipViscoElastic->_kirchhoff(1,2);
  }
  else if (i == IPField::corKir_ZZ)
  {
    return _ipViscoElastic->_kirchhoff(2,2);
  }
  else
  {
    return dG3DIPVariable::get(i);
  }
};

double  HyperViscoElasticdG3DIPVariable::defoEnergy() const
{
  return _ipViscoElastic->defoEnergy();
}

void HyperViscoElasticdG3DIPVariable::restart()
{
  dG3DIPVariable::restart();
  restartManager::restart(_ipViscoElastic);
  return;
}

HyperViscoElastoPlasticdG3DIPVariable::HyperViscoElastoPlasticdG3DIPVariable(const mlawPowerYieldHyper& viscoEPLaw, const bool createBodyForceHO, const bool oninter):
      dG3DIPVariable(createBodyForceHO, oninter){
  _ipViscoElastoPlastic = new IPHyperViscoElastoPlastic(viscoEPLaw.getConstRefToCompressionHardening(),
                                            viscoEPLaw.getConstRefToTractionHardening(),
                                            viscoEPLaw.getConstRefToKinematicHardening(),
                                            viscoEPLaw.getViscoElasticNumberOfElement());
};
HyperViscoElastoPlasticdG3DIPVariable::HyperViscoElastoPlasticdG3DIPVariable(const HyperViscoElastoPlasticdG3DIPVariable& src):dG3DIPVariable(src){
  _ipViscoElastoPlastic = NULL;
  if (src._ipViscoElastoPlastic != NULL){
    _ipViscoElastoPlastic = dynamic_cast<IPHyperViscoElastoPlastic*>(src._ipViscoElastoPlastic->clone());
  }
};
HyperViscoElastoPlasticdG3DIPVariable& HyperViscoElastoPlasticdG3DIPVariable::operator = (const IPVariable& src){
  dG3DIPVariable::operator =(src);
  const HyperViscoElastoPlasticdG3DIPVariable* psrc = dynamic_cast<const HyperViscoElastoPlasticdG3DIPVariable*>(&src);
  if (psrc != NULL){
    if (psrc->_ipViscoElastoPlastic != NULL){
      if (_ipViscoElastoPlastic!= NULL){
        _ipViscoElastoPlastic->operator=(*dynamic_cast<const IPVariable*>(psrc->_ipViscoElastoPlastic));
      }
      else{
        _ipViscoElastoPlastic = dynamic_cast<IPHyperViscoElastoPlastic*>(psrc->_ipViscoElastoPlastic->clone());
      }
    }
  }
  return *this;
};
HyperViscoElastoPlasticdG3DIPVariable::~HyperViscoElastoPlasticdG3DIPVariable(){
  if (_ipViscoElastoPlastic){delete _ipViscoElastoPlastic; _ipViscoElastoPlastic = NULL;}
};

double HyperViscoElastoPlasticdG3DIPVariable::get(const int comp) const
{
  if (comp == IPField::Ee_XX)
  {
    return _ipViscoElastoPlastic->_Ee(0,0);
  }
  else if (comp == IPField::Ee_XY)
  {
    return _ipViscoElastoPlastic->_Ee(0,1);
  }
  else if (comp == IPField::Ee_XZ)
  {
    return _ipViscoElastoPlastic->_Ee(0,2);
  }
  else if (comp == IPField::Ee_YY)
  {
    return _ipViscoElastoPlastic->_Ee(1,1);
  }
  else if (comp == IPField::Ee_YZ)
  {
    return _ipViscoElastoPlastic->_Ee(1,2);
  }
  else if (comp == IPField::Ee_ZZ)
  {
    return _ipViscoElastoPlastic->_Ee(2,2);
  }
  else if (comp == IPField::hyperElastic_BulkScalar){return _ipViscoElastoPlastic->getConstRefToElasticBulkPropertyScaleFactor();}
  else if (comp == IPField::hyperElastic_ShearScalar){return _ipViscoElastoPlastic->getConstRefToElasticShearPropertyScaleFactor();}
  else if (comp == IPField::Eve1_XX)
  {
    if (_ipViscoElastoPlastic->_A.size() >0)
      return _ipViscoElastoPlastic->_A[0](0,0) + _ipViscoElastoPlastic->_B[0]/3.;
    else return 0.;
  }
  else if (comp == IPField::Eve1_XY)
  {
    if (_ipViscoElastoPlastic->_A.size() >0)
      return _ipViscoElastoPlastic->_A[0](0,1);
    else return 0.;
  }
  else if (comp == IPField::Eve1_XZ)
  {
    if (_ipViscoElastoPlastic->_A.size() >0)
      return _ipViscoElastoPlastic->_A[0](0,2);
    else return 0.;
  }
  else if (comp == IPField::Eve1_YY)
  {
    if (_ipViscoElastoPlastic->_A.size() >0)
      return _ipViscoElastoPlastic->_A[0](1,1) + _ipViscoElastoPlastic->_B[0]/3.;
    else return 0.;
  }
  else if (comp == IPField::Eve1_YZ)
  {
    if (_ipViscoElastoPlastic->_A.size() >0)
      return _ipViscoElastoPlastic->_A[0](1,2);
    else return 0.;
  }
  else if (comp == IPField::Eve1_ZZ)
  {
    if (_ipViscoElastoPlastic->_A.size() >0)
      return _ipViscoElastoPlastic->_A[0](2,2) + _ipViscoElastoPlastic->_B[0]/3.;
    else return 0.;
  }
  else if (comp == IPField::Eve2_XX)
  {
    if (_ipViscoElastoPlastic->_A.size() >1)
      return _ipViscoElastoPlastic->_A[1](0,0) + _ipViscoElastoPlastic->_B[1]/3.;
    else return 0.;
  }
  else if (comp == IPField::Eve2_XY)
  {
    if (_ipViscoElastoPlastic->_A.size() >1)
      return _ipViscoElastoPlastic->_A[1](0,1);
    else return 0.;
  }
  else if (comp == IPField::Eve2_XZ)
  {
    if (_ipViscoElastoPlastic->_A.size() >1)
      return _ipViscoElastoPlastic->_A[1](0,2);
    else return 0.;
  }
  else if (comp == IPField::Eve2_YY)
  {
    if (_ipViscoElastoPlastic->_A.size() >1)
      return _ipViscoElastoPlastic->_A[1](1,1) + _ipViscoElastoPlastic->_B[1]/3.;
    else return 0.;
  }
  else if (comp == IPField::Eve2_YZ)
  {
    if (_ipViscoElastoPlastic->_A.size() >1)
      return _ipViscoElastoPlastic->_A[1](1,2);
    else return 0.;
  }
  else if (comp == IPField::Eve2_ZZ)
  {
    if (_ipViscoElastoPlastic->_A.size() >1)
      return _ipViscoElastoPlastic->_A[1](2,2) + _ipViscoElastoPlastic->_B[1]/3.;
    else return 0.;
  }
  else if (comp == IPField::corKir_XX)
  {
    return _ipViscoElastoPlastic->_kirchhoff(0,0);
  }
  else if (comp == IPField::corKir_XY)
  {
    return _ipViscoElastoPlastic->_kirchhoff(0,1);
  }
  else if (comp == IPField::corKir_XZ)
  {
    return _ipViscoElastoPlastic->_kirchhoff(0,2);
  }
  else if (comp == IPField::corKir_YY)
  {
    return _ipViscoElastoPlastic->_kirchhoff(1,1);
  }
  else if (comp == IPField::corKir_YZ)
  {
    return _ipViscoElastoPlastic->_kirchhoff(1,2);
  }
  else if (comp == IPField::corKir_ZZ)
  {
    return _ipViscoElastoPlastic->_kirchhoff(2,2);
  }
  else if(comp == IPField::PLASTICSTRAIN)
  {
    return _ipViscoElastoPlastic->getConstRefToEqPlasticStrain();
  }
   else if(comp == IPField::FP_XX)
  {
    return _ipViscoElastoPlastic->getConstRefToFp()(0,0);
  }
  else if(comp == IPField::FP_XY)
  {
    return _ipViscoElastoPlastic->getConstRefToFp()(0,1);
  }
  else if(comp == IPField::FP_XZ)
  {
    return _ipViscoElastoPlastic->getConstRefToFp()(0,2);
  }
  else if(comp == IPField::FP_YX)
  {
    return _ipViscoElastoPlastic->getConstRefToFp()(1,0);
  }
  else if(comp == IPField::FP_YY)
  {
    return _ipViscoElastoPlastic->getConstRefToFp()(1,1);
  }
  else if(comp == IPField::FP_YZ)
  {
    return _ipViscoElastoPlastic->getConstRefToFp()(1,2);
  }
  else if(comp == IPField::FP_ZX)
  {
    return _ipViscoElastoPlastic->getConstRefToFp()(2,0);
  }
  else if(comp == IPField::FP_ZY)
  {
    return _ipViscoElastoPlastic->getConstRefToFp()(2,1);
  }
  else if(comp == IPField::FP_ZZ)
  {
    return _ipViscoElastoPlastic->getConstRefToFp()(2,2);
  }
  else if (comp == IPField::PLASTIC_POISSON_RATIO){
    return _ipViscoElastoPlastic->getConstRefToPlasticPoissonRatio();
  }
  else if (comp == IPField::ISO_YIELD_COMPRESSION)
  {
    return _ipViscoElastoPlastic->getConstRefToIPCompressionHardening().getR();
  }
  else if (comp == IPField::ISO_YIELD_TENSILE)
  {
    return _ipViscoElastoPlastic->getConstRefToIPTractionHardening().getR();
  }
  else if (comp == IPField::KIN_YIELD)
  {
    return _ipViscoElastoPlastic->getConstRefToKinematicHardening().getR();
  }
  else
  {
    return dG3DIPVariable::get(comp);
  }
}

void HyperViscoElastoPlasticdG3DIPVariable::restart()
{
  dG3DIPVariable::restart();
  restartManager::restart(_ipViscoElastoPlastic);
  return;
}


localDamageJ2HyperDG3DIPVariable::localDamageJ2HyperDG3DIPVariable(const mlawLocalDamageJ2Hyper &_j2law, const bool createBodyForceHO,  const bool oninter) :
                                   dG3DIPVariable(createBodyForceHO, oninter)
{
  _nldJ2Hyperipv = new IPLocalDamageJ2Hyper(_j2law.getJ2IsotropicHardening(),_j2law.getDamageLaw());
}

localDamageJ2HyperDG3DIPVariable::localDamageJ2HyperDG3DIPVariable(const mlawLocalDamageJ2SmallStrain &_j2law, const bool createBodyForceHO, const bool oninter) :dG3DIPVariable(createBodyForceHO, oninter)
{
  _nldJ2Hyperipv = new IPLocalDamageJ2Hyper(_j2law.getJ2IsotropicHardening(),_j2law.getDamageLaw());
}

localDamageJ2HyperDG3DIPVariable::localDamageJ2HyperDG3DIPVariable(const localDamageJ2HyperDG3DIPVariable &source) : dG3DIPVariable(source)
{
  _nldJ2Hyperipv = NULL;
  if (source.getIPLocalDamageJ2Hyper() != NULL){
    _nldJ2Hyperipv = dynamic_cast<IPLocalDamageJ2Hyper*>(source.getIPLocalDamageJ2Hyper()->clone());
  }
}

localDamageJ2HyperDG3DIPVariable& localDamageJ2HyperDG3DIPVariable::operator=(const IPVariable &source)
{
  dG3DIPVariable::operator=(source);
  const localDamageJ2HyperDG3DIPVariable* src = dynamic_cast<const localDamageJ2HyperDG3DIPVariable*>(&source);
  if(src != NULL)
  {
    if (src->getIPLocalDamageJ2Hyper() != NULL){
      if (_nldJ2Hyperipv != NULL){
        _nldJ2Hyperipv->operator=(*(dynamic_cast<const IPVariable*>(src->getIPLocalDamageJ2Hyper())));
      }
      else
        _nldJ2Hyperipv = dynamic_cast<IPLocalDamageJ2Hyper*>(src->getIPLocalDamageJ2Hyper()->clone());
    }
    else{
      if (_nldJ2Hyperipv != NULL){
        delete _nldJ2Hyperipv; _nldJ2Hyperipv = NULL;
      }
    }

  }
  return *this;
}
double localDamageJ2HyperDG3DIPVariable::get(const int i) const
{
  if(i == IPField::PLASTICSTRAIN)
  {
    return getIPLocalDamageJ2Hyper()->getConstRefToEquivalentPlasticStrain();
  }
  else if(i == IPField::DAMAGE)
  {
    return getIPLocalDamageJ2Hyper()->getDamage();
  }
  else
  {
    return dG3DIPVariable::get(i);
  }
}
double localDamageJ2HyperDG3DIPVariable::defoEnergy() const
{
  return getIPLocalDamageJ2Hyper()->defoEnergy();
}
double localDamageJ2HyperDG3DIPVariable::plasticEnergy() const
{
  return getIPLocalDamageJ2Hyper()->plasticEnergy();
}

void localDamageJ2HyperDG3DIPVariable::restart()
{
  dG3DIPVariable::restart();
  if (_nldJ2Hyperipv != NULL)
    restartManager::restart(_nldJ2Hyperipv);
  return;
}


localDamageIsotropicElasticityDG3DIPVariable::localDamageIsotropicElasticityDG3DIPVariable(const mlawLocalDamageHyperelastic & elasticLaw, const bool createBodyForceHO, const bool oninter) : dG3DIPVariable(createBodyForceHO, oninter)
{
  _localIPV = new IPLocalDamageIsotropicElasticity(elasticLaw.getDamageLaw());
}
localDamageIsotropicElasticityDG3DIPVariable::localDamageIsotropicElasticityDG3DIPVariable(const mlawLocalDamageIsotropicElasticity & elasticLaw, const bool createBodyForceHO, const bool oninter) : dG3DIPVariable(createBodyForceHO, oninter)
{
  _localIPV = new IPLocalDamageIsotropicElasticity(elasticLaw.getDamageLaw());
}

localDamageIsotropicElasticityDG3DIPVariable::localDamageIsotropicElasticityDG3DIPVariable(const localDamageIsotropicElasticityDG3DIPVariable &source) : dG3DIPVariable(source)
{
  _localIPV = NULL;
  if (source.getIPLocalDamageIsotropicElasticity() != NULL){
    _localIPV = dynamic_cast<IPLocalDamageIsotropicElasticity*>(source.getIPLocalDamageIsotropicElasticity()->clone());
  }
}

localDamageIsotropicElasticityDG3DIPVariable& localDamageIsotropicElasticityDG3DIPVariable::operator=(const IPVariable &source)
{
  dG3DIPVariable::operator=(source);
  const localDamageIsotropicElasticityDG3DIPVariable* src = dynamic_cast<const localDamageIsotropicElasticityDG3DIPVariable*>(&source);
  if(src != NULL)
  {
    if (src->getIPLocalDamageIsotropicElasticity() != NULL){
      if (_localIPV != NULL){
        _localIPV->operator=(*(dynamic_cast<const IPVariable*>(src->getIPLocalDamageIsotropicElasticity())));
      }
      else
        _localIPV = dynamic_cast<IPLocalDamageIsotropicElasticity*>(src->getIPLocalDamageIsotropicElasticity()->clone());
    }
    else{
      if (_localIPV != NULL){
        delete _localIPV; _localIPV = NULL;
      }
    }

  }
  return *this;
}
double localDamageIsotropicElasticityDG3DIPVariable::get(const int i) const
{
  if(i == IPField::DAMAGE)
  {
    return getIPLocalDamageIsotropicElasticity()->getDamage();
  }
  else
  {
    return dG3DIPVariable::get(i);
  }
}

void localDamageIsotropicElasticityDG3DIPVariable::restart()
{
  dG3DIPVariable::restart();
  if (_localIPV!= NULL)
    restartManager::restart(_localIPV);
  return;
}

//====================================================VEVPMFHDG3DIPVariable==========================================================//
VEVPMFHDG3DIPVariable::VEVPMFHDG3DIPVariable(bool createBodyForceHO, const bool oninter) : dG3DIPVariable(createBodyForceHO, oninter)
{
  _VEVPipv = new IPVEVPMFH();
}



//VEVPMFHDG3DIPVariable::VEVPMFHDG3DIPVariable(const VEVPMFHDG3DIPVariable &source) : dG3DIPVariable(source), _tiipv(source._tiipv)
VEVPMFHDG3DIPVariable::VEVPMFHDG3DIPVariable(const VEVPMFHDG3DIPVariable &source) : dG3DIPVariable(source)
{
  _VEVPipv = NULL;
  if (source._VEVPipv){
    _VEVPipv = new IPVEVPMFH();

  }
}



VEVPMFHDG3DIPVariable& VEVPMFHDG3DIPVariable::operator=(const IPVariable &source)
{
  dG3DIPVariable::operator=(source);
  const VEVPMFHDG3DIPVariable* src = dynamic_cast<const VEVPMFHDG3DIPVariable*>(&source);
  /*if(src != NULL)
    _tiipv.operator=(*dynamic_cast<const IPVariable*>(&( src->_tiipv)));
  return *this;*/

  if(src != NULL){
    if (src->_VEVPipv){
      if (_VEVPipv){
        _VEVPipv->operator=(*dynamic_cast<const IPVariable*>(src->_VEVPipv));
      }
      else{
        _VEVPipv = dynamic_cast<IPVEVPMFH*>(src->_VEVPipv->clone());
      }

    }
    else{
      if (_VEVPipv) {
        delete _VEVPipv; _VEVPipv = NULL;
      }
    }
  }
  return *this;
}



double VEVPMFHDG3DIPVariable::get(const int comp) const
{
  double val=_VEVPipv->get(comp);
  if(val==0.)
  {
    return dG3DIPVariable::get(comp);
  }
  return val;
}




double VEVPMFHDG3DIPVariable::defoEnergy() const
{
  return getIPVEVPMFH()->defoEnergy();
}



double  VEVPMFHDG3DIPVariable::plasticEnergy() const
{
  return getIPVEVPMFH()->plasticEnergy();
}



void VEVPMFHDG3DIPVariable::restart()
{
  dG3DIPVariable::restart();
  restartManager::restart(_VEVPipv);
  return;
}
//====================================================ViscoelasticDG3DIPVariable==========================================================//

ViscoelasticDG3DIPVariable::ViscoelasticDG3DIPVariable(const mlawViscoelastic &_Vislaw, const bool createBodyForceHO, const bool oninter) : dG3DIPVariable(createBodyForceHO, oninter)
{
  _Visipv = new   IPViscoelastic(_Vislaw.numInternalVariable());
}
ViscoelasticDG3DIPVariable::ViscoelasticDG3DIPVariable(const ViscoelasticDG3DIPVariable &source) : dG3DIPVariable(source)
{
  _Visipv = NULL;
  if (source._Visipv){
    _Visipv = new   IPViscoelastic(*source._Visipv);

  }
}
ViscoelasticDG3DIPVariable& ViscoelasticDG3DIPVariable::operator=(const IPVariable &source)
{
  dG3DIPVariable::operator=(source);
  const ViscoelasticDG3DIPVariable* src = dynamic_cast<const ViscoelasticDG3DIPVariable*>(&source);
  if(src != NULL){
    if (src->_Visipv){
      if (_Visipv){
        _Visipv->operator=(*dynamic_cast<const IPVariable*>(src->_Visipv));
      }
      else{
        _Visipv = dynamic_cast<IPViscoelastic*>(src->_Visipv->clone());
      }

    }
    else{
      if (_Visipv) {
        delete _Visipv; _Visipv = NULL;
      }
    }
  }
  return *this;
}
double ViscoelasticDG3DIPVariable::get(const int comp) const
{

    return dG3DIPVariable::get(comp);
}

void ViscoelasticDG3DIPVariable::restart()
{
  dG3DIPVariable::restart();
  restartManager::restart(_Visipv);
  return;
}

//=========================================================EOSDG3DIPVariable======================================================================//

EOSDG3DIPVariable::EOSDG3DIPVariable(double elementSize, const bool createBodyForceHO, const bool oninter) : dG3DIPVariable(createBodyForceHO, oninter)
{
  _eosipv = new   IPEOS(elementSize);
}
EOSDG3DIPVariable::EOSDG3DIPVariable(double elementSize,IPVariable *IPv, const bool createBodyForceHO, const bool oninter): dG3DIPVariable(createBodyForceHO, oninter)
{
    STensor3 ZEROS(0.);
    _PK1_vis = ZEROS;
    _eosipv = new   IPEOS(elementSize, IPv);
}

EOSDG3DIPVariable::EOSDG3DIPVariable(const EOSDG3DIPVariable &source) : dG3DIPVariable(source)
{
  _eosipv = NULL;
  if (source._eosipv!=NULL){
    _eosipv = new IPEOS(*source._eosipv);
  }
}
EOSDG3DIPVariable& EOSDG3DIPVariable::operator=(const IPVariable &source)
{
  dG3DIPVariable::operator=(source);
  const EOSDG3DIPVariable* src = dynamic_cast<const EOSDG3DIPVariable*>(&source);
  if(src != NULL){
    if (src->_eosipv!=NULL){
      if (_eosipv!=NULL){
         _eosipv->operator=(*dynamic_cast<const IPVariable*>(src->_eosipv));
      }
      else{
        _eosipv = dynamic_cast<IPEOS*>(src->_eosipv->clone());
      }

    }
    else{
      if (_eosipv != NULL){
        delete _eosipv;
        _eosipv = NULL;
      }
    }
  }
  return *this;
}
double EOSDG3DIPVariable::get(const int comp) const
{
    return dG3DIPVariable::get(comp);
}

void EOSDG3DIPVariable::getCauchyStress(STensor3 &cauchy) const
{
  const STensor3& PK1_vis = _PK1_vis;
  cauchy=0.;
  double detJ = getJ();
  if (detJ < 1.e-12) Msg::Error("Negative Jacobian");
  for (int i = 0; i< 3; i ++)
  {
    for (int j = 0; j< 3; j ++)
    {
       for(int k =0; k <3; k++)
       {
           cauchy(i,j) += (getConstRefToFirstPiolaKirchhoffStress()(i,k) + (-1.*PK1_vis(i,k)))*getConstRefToDeformationGradient()(j,k)/detJ;
       }
    }
  }
}

void EOSDG3DIPVariable::restart()
{
  dG3DIPVariable::restart();
  restartManager::restart(_eosipv);
  restartManager::restart(_PK1_vis);
  return;
}


VUMATinterfaceDG3DIPVariable::VUMATinterfaceDG3DIPVariable(int _nsdv, double _size, const bool createBodyForceHO, const bool oninter) : dG3DIPVariable(createBodyForceHO, oninter)
{
  _vumatipv = new IPVUMATinterface(_nsdv,_size);
}
VUMATinterfaceDG3DIPVariable::VUMATinterfaceDG3DIPVariable(const VUMATinterfaceDG3DIPVariable &source) : dG3DIPVariable(source)
{
    _vumatipv = NULL;
  if(source._vumatipv != NULL){
    _vumatipv = new IPVUMATinterface(*source._vumatipv);
  }
}
VUMATinterfaceDG3DIPVariable& VUMATinterfaceDG3DIPVariable::operator=(const IPVariable &source)
{
  dG3DIPVariable::operator=(source);
  const VUMATinterfaceDG3DIPVariable* src = dynamic_cast<const VUMATinterfaceDG3DIPVariable*>(&source);
  if(src != NULL){
    if (src->_vumatipv != NULL){
      if (_vumatipv!= NULL){
        _vumatipv->operator= (*(dynamic_cast<const IPVariable*>(src->getIPVUMATinterface())));
      }
      else{
        _vumatipv = dynamic_cast<IPVUMATinterface*>(src->_vumatipv->clone());
      }

    }
    else{
      if (_vumatipv != NULL){
        delete _vumatipv;
        _vumatipv = NULL;
      }
    }

  }
  return *this;
}
double VUMATinterfaceDG3DIPVariable::get(const int comp) const
{
  if(comp >= 100) // Internal variables
  {
    return getIPVUMATinterface()->get(comp);
  }
  else
  {
    return dG3DIPVariable::get(comp);
  }
}

double  VUMATinterfaceDG3DIPVariable::defoEnergy() const
{
  return getIPVUMATinterface()->defoEnergy();
}
double  VUMATinterfaceDG3DIPVariable::plasticEnergy() const
{
  return getIPVUMATinterface()->plasticEnergy();
}

void VUMATinterfaceDG3DIPVariable::restart()
{
  dG3DIPVariable::restart();
  restartManager::restart(_vumatipv);
  return;
}

//
TransverseIsotropicDG3DIPVariable::TransverseIsotropicDG3DIPVariable(bool createBodyForceHO, const bool oninter) : dG3DIPVariable(createBodyForceHO, oninter)
{

}
TransverseIsotropicDG3DIPVariable::TransverseIsotropicDG3DIPVariable(const TransverseIsotropicDG3DIPVariable &source) : dG3DIPVariable(source),
_tiipv(source._tiipv)
{

}
TransverseIsotropicDG3DIPVariable& TransverseIsotropicDG3DIPVariable::operator=(const IPVariable &source)
{
  dG3DIPVariable::operator=(source);
  const TransverseIsotropicDG3DIPVariable* src = dynamic_cast<const TransverseIsotropicDG3DIPVariable*>(&source);
  if(src != NULL)
    _tiipv.operator=(*dynamic_cast<const IPVariable*>(&(src->_tiipv)));
  return *this;
}
double TransverseIsotropicDG3DIPVariable::get(const int comp) const
{
  if(comp == IPField::PLASTICSTRAIN)
  {
    return 0.;
  }
  else
  {
    return dG3DIPVariable::get(comp);
  }
}
double  TransverseIsotropicDG3DIPVariable::defoEnergy() const
{
  return getIPTransverseIsotropic()->defoEnergy();
}
double  TransverseIsotropicDG3DIPVariable::plasticEnergy() const
{
  return getIPTransverseIsotropic()->plasticEnergy();
}

void TransverseIsotropicDG3DIPVariable::restart()
{
  dG3DIPVariable::restart();
  restartManager::restart(&_tiipv);
  return;
}

TransverseIsoUserDirectionDG3DIPVariable::TransverseIsoUserDirectionDG3DIPVariable(const SVector3 &dir, const bool createBodyForceHO, const bool oninter) :
                                                                           dG3DIPVariable(createBodyForceHO, oninter), _tiipv(dir)
{

}
TransverseIsoUserDirectionDG3DIPVariable::TransverseIsoUserDirectionDG3DIPVariable(const TransverseIsoUserDirectionDG3DIPVariable &source) :
    dG3DIPVariable(source),  _tiipv(source._tiipv)
{

}
TransverseIsoUserDirectionDG3DIPVariable& TransverseIsoUserDirectionDG3DIPVariable::operator=(const IPVariable &source)
{
  dG3DIPVariable::operator=(source);
  const TransverseIsoUserDirectionDG3DIPVariable* src = dynamic_cast<const TransverseIsoUserDirectionDG3DIPVariable*>(&source);
  if(src != NULL)
    _tiipv.operator=(*dynamic_cast<const IPVariable*>(&( src->_tiipv)));
  return *this;
}
double TransverseIsoUserDirectionDG3DIPVariable::get(const int comp) const
{
  if(comp == IPField::PLASTICSTRAIN)
  {
    return 0.;
  }
  else
  {
    return dG3DIPVariable::get(comp);
  }
}
double  TransverseIsoUserDirectionDG3DIPVariable::defoEnergy() const
{
  return getIPTransverseIsoUserDirection()->defoEnergy();
}
double  TransverseIsoUserDirectionDG3DIPVariable::plasticEnergy() const
{
  return getIPTransverseIsoUserDirection()->plasticEnergy();
}

void TransverseIsoUserDirectionDG3DIPVariable::restart()
{
  dG3DIPVariable::restart();
  restartManager::restart(&_tiipv);
  return;
}



TransverseIsoCurvatureDG3DIPVariable::TransverseIsoCurvatureDG3DIPVariable(const SVector3 &GaussP, const SVector3 &Centroid,
                                          const SVector3 &PointStrart1, const SVector3 &PointStrart2, const double init_Angle,
                                          bool createBodyForceHO, const bool oninter) :dG3DIPVariable(createBodyForceHO, oninter),
                                          _tiipv(GaussP, Centroid, PointStrart1, PointStrart2, init_Angle)
{

}
TransverseIsoCurvatureDG3DIPVariable::TransverseIsoCurvatureDG3DIPVariable(const TransverseIsoCurvatureDG3DIPVariable &source) : dG3DIPVariable(source),  _tiipv(source._tiipv)
{

}
TransverseIsoCurvatureDG3DIPVariable& TransverseIsoCurvatureDG3DIPVariable::operator=(const IPVariable &source)
{
  dG3DIPVariable::operator=(source);
  const TransverseIsoCurvatureDG3DIPVariable* src = dynamic_cast<const TransverseIsoCurvatureDG3DIPVariable*>(&source);
  if(src != NULL)
    _tiipv.operator=(*dynamic_cast<const IPVariable*>(&( src->_tiipv)));
  return *this;
}
double TransverseIsoCurvatureDG3DIPVariable::get(const int comp) const
{
  if(comp == IPField::PLASTICSTRAIN)
  {
    return 0.;
  }
  else
  {
    return dG3DIPVariable::get(comp);
  }
}
double  TransverseIsoCurvatureDG3DIPVariable::defoEnergy() const
{
  return getIPTransverseIsoCurvature()->defoEnergy();
}
double  TransverseIsoCurvatureDG3DIPVariable::plasticEnergy() const
{
  return getIPTransverseIsoCurvature()->plasticEnergy();
}

void TransverseIsoCurvatureDG3DIPVariable::restart()
{
  dG3DIPVariable::restart();
  restartManager::restart(&_tiipv);
  return;
}


TransverseIsoYarnBDG3DIPVariable::TransverseIsoYarnBDG3DIPVariable(const SVector3 &GaussP, const Yarn YD, const bool createBodyForceHO, const bool oninter) :
                                                                   dG3DIPVariable(createBodyForceHO, oninter), _tiipv(GaussP, YD)
{

}

TransverseIsoYarnBDG3DIPVariable::TransverseIsoYarnBDG3DIPVariable(const SVector3 &GaussP, const bool createBodyForceHO, const bool oninter):
                                                                   dG3DIPVariable(createBodyForceHO, oninter), _tiipv(GaussP)
{

}


TransverseIsoYarnBDG3DIPVariable::TransverseIsoYarnBDG3DIPVariable(const SVector3 &GaussP, const vector<double> Dir, const bool createBodyForceHO, const bool oninter) :
                                                                   dG3DIPVariable(createBodyForceHO, oninter), _tiipv(GaussP, Dir)
{

}

TransverseIsoYarnBDG3DIPVariable::TransverseIsoYarnBDG3DIPVariable(const TransverseIsoYarnBDG3DIPVariable &source) : dG3DIPVariable(source),  _tiipv(source._tiipv)
{

}

TransverseIsoYarnBDG3DIPVariable& TransverseIsoYarnBDG3DIPVariable::operator=(const IPVariable &source){
	dG3DIPVariable::operator =(source);
	const TransverseIsoYarnBDG3DIPVariable* psrc = dynamic_cast<const TransverseIsoYarnBDG3DIPVariable*>(&source);
	if (psrc != NULL){
		_tiipv.operator=(*dynamic_cast<const IPVariable*>(&psrc->_tiipv));
	}
	return *this;
};




/*
double TransverseIsoYarnBDG3DIPVariable::get(const int comp) const
{
  if(comp == IPField::PLASTICSTRAIN)
  {
    return 0.;
  }
  else
  {
    return dG3DIPVariable::get(comp);
  }
}
*/

/*
double  TransverseIsoYarnBDG3DIPVariable::defoEnergy()
{
  return getIPTransverseIsoYarnB()->defoEnergy();
}
*/

/*
double  TransverseIsoYarnBDG3DIPVariable::plasticEnergy()
{
  return getIPTransverseIsoYarnB()->plasticEnergy();
}
*/

void TransverseIsoYarnBDG3DIPVariable::restart()
{
  dG3DIPVariable::restart();
  restartManager::restart(&_tiipv);
  return;
}




AnisotropicDG3DIPVariable::AnisotropicDG3DIPVariable(bool createBodyForceHO, const bool oninter) : dG3DIPVariable(createBodyForceHO, oninter)
{

}
AnisotropicDG3DIPVariable::AnisotropicDG3DIPVariable(const AnisotropicDG3DIPVariable &source) : dG3DIPVariable(source),
_tiipv(source._tiipv)
{

}
AnisotropicDG3DIPVariable& AnisotropicDG3DIPVariable::operator=(const IPVariable &source)
{
  dG3DIPVariable::operator=(source);
  const AnisotropicDG3DIPVariable* src = dynamic_cast<const AnisotropicDG3DIPVariable*>(&source);
  if(src != NULL)
    _tiipv.operator=(*dynamic_cast<const IPVariable*>(&( src->_tiipv)));
  return *this;
}
double AnisotropicDG3DIPVariable::get(const int comp) const
{
  if(comp == IPField::PLASTICSTRAIN)
  {
    return 0.;
  }
  else
  {
    return dG3DIPVariable::get(comp);
  }
}
double  AnisotropicDG3DIPVariable::defoEnergy() const
{
  return getIPAnisotropic()->defoEnergy();
}
double  AnisotropicDG3DIPVariable::plasticEnergy() const
{
  return getIPAnisotropic()->plasticEnergy();
}

void AnisotropicDG3DIPVariable::restart()
{
  dG3DIPVariable::restart();
  restartManager::restart(&_tiipv);
  return;
}



AnisotropicStochDG3DIPVariable::AnisotropicStochDG3DIPVariable(const SVector3 &GaussP, const fullMatrix<double> &ExMat,
                                 const fullMatrix<double> &EyMat, const fullMatrix<double> &EzMat, const fullMatrix<double> &VxyMat,
                                 const fullMatrix<double> &VxzMat, const fullMatrix<double> &VyzMat, const fullMatrix<double> &MUxyMat,
                                 const fullMatrix<double> &MUxzMat, const fullMatrix<double> &MUyzMat, const double dx, const double dy,
                                 const double OrigX, const double OrigY, const int intpl, const bool createBodyForceHO, const bool oninter) :
                                 dG3DIPVariable(createBodyForceHO, oninter), _tiipv(GaussP, ExMat,
                                 EyMat, EzMat, VxyMat, VxzMat, VyzMat, MUxyMat, MUxzMat, MUyzMat, dx, dy, OrigX, OrigY, intpl) {}

AnisotropicStochDG3DIPVariable::AnisotropicStochDG3DIPVariable(const AnisotropicStochDG3DIPVariable &source) : dG3DIPVariable(source),
_tiipv(source._tiipv) {}

AnisotropicStochDG3DIPVariable& AnisotropicStochDG3DIPVariable::operator=(const IPVariable &source)
{
  dG3DIPVariable::operator=(source);
  const AnisotropicStochDG3DIPVariable* src = dynamic_cast<const AnisotropicStochDG3DIPVariable*>(&source);
  if(src != NULL)
    _tiipv.operator=(*dynamic_cast<const IPVariable*>(&( src->_tiipv)));
  return *this;
}
double AnisotropicStochDG3DIPVariable::get(const int comp) const
{
  if(comp == IPField::PLASTICSTRAIN)
  {
    return 0.;
  }
  else
  {
    return dG3DIPVariable::get(comp);
  }
}
double  AnisotropicStochDG3DIPVariable::defoEnergy() const
{
  return getIPAnisotropicStoch()->defoEnergy();
}
double  AnisotropicStochDG3DIPVariable::plasticEnergy() const
{
  return getIPAnisotropicStoch()->plasticEnergy();
}

void AnisotropicStochDG3DIPVariable::restart()
{
  dG3DIPVariable::restart();
  restartManager::restart(&_tiipv);
  return;
}


//
/*TFADG3DIPVariable::TFADG3DIPVariable(IPTFA* ipc, const bool createBodyForceHO, const bool oninter) : dG3DIPVariable(createBodyForceHO, oninter)
{
  _tfaipv=ipc;
}

TFADG3DIPVariable::TFADG3DIPVariable(const TFADG3DIPVariable &source) : dG3DIPVariable(source)
{
  _tfaipv = NULL;
  if (source.getIPTFA() != NULL){
    _tfaipv = new IPTFA(*(source.getIPTFA()));
  }
}

TFADG3DIPVariable& TFADG3DIPVariable::operator=(const IPVariable &source)
{
  dG3DIPVariable::operator=(source);
  const TFADG3DIPVariable* src = dynamic_cast<const TFADG3DIPVariable*>(&source);

  if(src != NULL)
    _tfaipv->operator=(*(dynamic_cast<const IPVariable*>( src->_tfaipv )));
  return *this;
}


double TFADG3DIPVariable::get(const int i) const
{
  double val = _tfaipv->get(i);
  if (val == 0.)
    val = dG3DIPVariable::get(i);
  return val;
}



double TFADG3DIPVariable::defoEnergy() const
{
  return getIPTFA()->defoEnergy();
}
double  TFADG3DIPVariable::plasticEnergy() const
{
  return getIPTFA()->plasticEnergy();
}

void TFADG3DIPVariable::restart()
{
  dG3DIPVariable::restart();
  _tfaipv->restart();
  return;
}*/

GenericTFADG3DIPVariable::GenericTFADG3DIPVariable(GenericIPTFA* ipc, const bool createBodyForceHO, const bool oninter) : dG3DIPVariable(createBodyForceHO, oninter)
{
  _generictfaipv=ipc;
}

GenericTFADG3DIPVariable::GenericTFADG3DIPVariable(const GenericTFADG3DIPVariable &source) : dG3DIPVariable(source)
{
  _generictfaipv = NULL;
  if (source.getIPTFA() != NULL){
    _generictfaipv = new GenericIPTFA(*(source.getIPTFA()));
  }
}

GenericTFADG3DIPVariable& GenericTFADG3DIPVariable::operator=(const IPVariable &source)
{
  dG3DIPVariable::operator=(source);
  const GenericTFADG3DIPVariable* src = dynamic_cast<const GenericTFADG3DIPVariable*>(&source);

  if(src != NULL)
    _generictfaipv->operator=(*(dynamic_cast<const IPVariable*>( src->_generictfaipv )));
  return *this;
}

GenericTFADG3DIPVariable::~GenericTFADG3DIPVariable()
{
  //if (_generictfaipv) delete _generictfaipv;
  //_generictfaipv = NULL;
}


double GenericTFADG3DIPVariable::get(const int i) const
{
  double val = _generictfaipv->get(i);
  if (val == 0.)
    val = dG3DIPVariable::get(i);
  return val;
}



double GenericTFADG3DIPVariable::defoEnergy() const
{
  return getIPTFA()->defoEnergy();
}
double  GenericTFADG3DIPVariable::plasticEnergy() const
{
  return getIPTFA()->plasticEnergy();
}

void GenericTFADG3DIPVariable::restart()
{
  dG3DIPVariable::restart();
  _generictfaipv->restart();
  return;
}


TFADG3DIPVariable::TFADG3DIPVariable(IPTFA* ipc, const bool createBodyForceHO, const bool oninter) : GenericTFADG3DIPVariable(ipc,createBodyForceHO, oninter)
{
  _tfaipv=ipc;
}
TFADG3DIPVariable::TFADG3DIPVariable(const TFADG3DIPVariable &source) : GenericTFADG3DIPVariable(source)
{
  _tfaipv = NULL;
  if (source.getIPTFA() != NULL){
    _tfaipv = new IPTFA(*(source.getIPTFA()));
  }
}
TFADG3DIPVariable& TFADG3DIPVariable::operator=(const IPVariable &source)
{
  dG3DIPVariable::operator=(source);
  const TFADG3DIPVariable* src = dynamic_cast<const TFADG3DIPVariable*>(&source);

  if(src != NULL){
      _tfaipv->operator=(*(dynamic_cast<const IPVariable*>( src->_tfaipv )));
  }
  return *this;
}

TFADG3DIPVariable::~TFADG3DIPVariable()
{
  if (_tfaipv) delete _tfaipv;
  _tfaipv = NULL;
}

double TFADG3DIPVariable::get(const int i) const
{
  double val = _tfaipv->get(i);
  if (val == 0.)
    val = dG3DIPVariable::get(i);
  return val;
}
void TFADG3DIPVariable::restart()
{
  dG3DIPVariable::restart();
  _tfaipv->restart();
  return;
}

TFA2LevelsDG3DIPVariable::TFA2LevelsDG3DIPVariable(IPTFA2Levels* ipc, const bool createBodyForceHO, const bool oninter) : GenericTFADG3DIPVariable(ipc,createBodyForceHO, oninter)
{
  _tfa2levelsipv=ipc;
}
TFA2LevelsDG3DIPVariable::TFA2LevelsDG3DIPVariable(const TFA2LevelsDG3DIPVariable &source) : GenericTFADG3DIPVariable(source)
{
  _tfa2levelsipv = NULL;
  if (source.getIPTFA() != NULL){
    _tfa2levelsipv = new IPTFA2Levels(*(source.getIPTFA()));
  }
}
TFA2LevelsDG3DIPVariable& TFA2LevelsDG3DIPVariable::operator=(const IPVariable &source)
{
  dG3DIPVariable::operator=(source);
  const TFA2LevelsDG3DIPVariable* src = dynamic_cast<const TFA2LevelsDG3DIPVariable*>(&source);

  if(src != NULL)
    _tfa2levelsipv->operator=(*(dynamic_cast<const IPVariable*>( src->_tfa2levelsipv )));
  return *this;
}

TFA2LevelsDG3DIPVariable::~TFA2LevelsDG3DIPVariable()
{
  if (_tfa2levelsipv) delete _tfa2levelsipv;
  _tfa2levelsipv = NULL;
}

double TFA2LevelsDG3DIPVariable::get(const int i) const
{
  double val = _tfa2levelsipv->get(i);
  if (val == 0.)
    val = dG3DIPVariable::get(i);
  return val;
}
void TFA2LevelsDG3DIPVariable::restart()
{
  dG3DIPVariable::restart();
  _tfa2levelsipv->restart();
  return;
}


/*
ExtraDofDiffusionDG3DIPVariableBase::ExtraDofDiffusionDG3DIPVariableBase( const bool oninter) : dG3DIPVariable(oninter),
{

}

ExtraDofDiffusionDG3DIPVariableBase::ExtraDofDiffusionDG3DIPVariableBase(const ExtraDofDiffusionDG3DIPVariableBase &src) :
                                                                        dG3DIPVariable(src)
{

}

ExtraDofDiffusionDG3DIPVariableBase& ExtraDofDiffusionDG3DIPVariableBase::operator=(const IPVariable &_src)
{
  dG3DIPVariable::operator=(_src);
  const ExtraDofDiffusionDG3DIPVariableBase* src = dynamic_cast<const ExtraDofDiffusionDG3DIPVariableBase*>(&_src);
  if(src != NULL)
  {

  }
  return *this;
}

void ExtraDofDiffusionDG3DIPVariableBase::restart()
{
  dG3DIPVariable::restart();
  return;
}*/

ThermoMechanicsDG3DIPVariableBase::ThermoMechanicsDG3DIPVariableBase(bool createBodyForceHO, const bool oninter) : dG3DIPVariable(createBodyForceHO, oninter,0,1,0), extraDofIPVariableBase()

{

}

ThermoMechanicsDG3DIPVariableBase::ThermoMechanicsDG3DIPVariableBase(const ThermoMechanicsDG3DIPVariableBase &source) :
                                                                        dG3DIPVariable(source), extraDofIPVariableBase()
{

}

ThermoMechanicsDG3DIPVariableBase& ThermoMechanicsDG3DIPVariableBase::operator=(const IPVariable &source)
{
  dG3DIPVariable::operator=(source);
  const ThermoMechanicsDG3DIPVariableBase* src = dynamic_cast<const ThermoMechanicsDG3DIPVariableBase*>(&source);
  if(src != NULL)
  {
  }
  return *this;
}

double ThermoMechanicsDG3DIPVariableBase::get(const int i) const
{
  if(i == IPField::TEMPERATURE)
  {
    return getConstRefToField(0);
  }
  else if(i == IPField::THERMALFLUX_X)
  {
    return getConstRefToFlux()[0](0);
  }
  else if(i == IPField::THERMALFLUX_Y)
  {
    return getConstRefToFlux()[0](1);
  }
  else if(i == IPField::THERMALFLUX_Z)
  {
    return getConstRefToFlux()[0](2);
  }
  else if (i == IPField::FIELD_SOURCE){
    return getConstRefToFieldSource()(0);
  }
  else if (i == IPField::MECHANICAL_SOURCE){
    return getConstRefToMechanicalSource()(0);
  }
  else
  {
    return dG3DIPVariable::get(i);
  }
}

void ThermoMechanicsDG3DIPVariableBase::restart()
{
  dG3DIPVariableBase::restart();
  return;
}

LinearThermoMechanicsDG3DIPVariable::LinearThermoMechanicsDG3DIPVariable(double tinitial, const bool createBodyForceHO, const bool oninter) :
                                   ThermoMechanicsDG3DIPVariableBase(createBodyForceHO, oninter)
{
  this->getRefToTemperature()=tinitial;

}


LinearThermoMechanicsDG3DIPVariable::LinearThermoMechanicsDG3DIPVariable(const LinearThermoMechanicsDG3DIPVariable &source) :
                                                        ThermoMechanicsDG3DIPVariableBase(source),
																												_linearTMIP(source._linearTMIP)
{


}

LinearThermoMechanicsDG3DIPVariable& LinearThermoMechanicsDG3DIPVariable::operator=(const IPVariable &source)
{
  ThermoMechanicsDG3DIPVariableBase::operator=(source);
  const LinearThermoMechanicsDG3DIPVariable* src = dynamic_cast<const LinearThermoMechanicsDG3DIPVariable*>(&source);
  if(src != NULL)
  {
    _linearTMIP.operator=(*(dynamic_cast<const IPVariable*>(&src->_linearTMIP)));
  }
  return *this;
}
double LinearThermoMechanicsDG3DIPVariable::get(const int i) const
{
  return ThermoMechanicsDG3DIPVariableBase::get(i);
}
double LinearThermoMechanicsDG3DIPVariable::defoEnergy() const
{
  return getIPLinearThermoMechanics()->defoEnergy();
}
double LinearThermoMechanicsDG3DIPVariable::plasticEnergy() const
{
  return getIPLinearThermoMechanics()->plasticEnergy();
}

void LinearThermoMechanicsDG3DIPVariable::restart()
{
  ThermoMechanicsDG3DIPVariableBase::restart();
  restartManager::restart(&_linearTMIP);
  return;
}



AnIsotropicTherMechDG3DIPVariable::AnIsotropicTherMechDG3DIPVariable(double tinitial, const bool createBodyForceHO, const bool oninter) :
                                   ThermoMechanicsDG3DIPVariableBase(createBodyForceHO, oninter)
{
  this->getRefToTemperature()=tinitial;

}


AnIsotropicTherMechDG3DIPVariable::AnIsotropicTherMechDG3DIPVariable(const AnIsotropicTherMechDG3DIPVariable &source) :
                                                        ThermoMechanicsDG3DIPVariableBase(source),
																												_AnIsotropicTMIP(source._AnIsotropicTMIP)
{


}
AnIsotropicTherMechDG3DIPVariable& AnIsotropicTherMechDG3DIPVariable::operator=(const IPVariable &source)
{
  ThermoMechanicsDG3DIPVariableBase::operator=(source);
  const AnIsotropicTherMechDG3DIPVariable* src = dynamic_cast<const AnIsotropicTherMechDG3DIPVariable*>(&source);
  if(src != NULL)
  {
    _AnIsotropicTMIP.operator=(*(dynamic_cast<const IPVariable*>(&src->_AnIsotropicTMIP)));
  }
  return *this;
}
double AnIsotropicTherMechDG3DIPVariable::get(const int i) const
{
  return ThermoMechanicsDG3DIPVariableBase::get(i);
}
double AnIsotropicTherMechDG3DIPVariable::defoEnergy() const
{
  return getIPAnIsotropicTherMech()->defoEnergy();
}
double AnIsotropicTherMechDG3DIPVariable::plasticEnergy() const
{
  return getIPAnIsotropicTherMech()->plasticEnergy();
}

void AnIsotropicTherMechDG3DIPVariable::restart()
{
  ThermoMechanicsDG3DIPVariableBase::restart();
  restartManager::restart(&_AnIsotropicTMIP);
  return;
}


J2ThermoMechanicsDG3DIPVariable::J2ThermoMechanicsDG3DIPVariable(double tinitial ,const mlawJ2ThermoMechanics &_j2law, const bool createBodyForceHO, const bool oninter) : ThermoMechanicsDG3DIPVariableBase(createBodyForceHO, oninter)
{
  _j2ipv = new   IPJ2ThermoMechanics(_j2law.getJ2IsotropicHardening());
    this->getRefToTemperature()=tinitial;
}
J2ThermoMechanicsDG3DIPVariable::J2ThermoMechanicsDG3DIPVariable(const J2ThermoMechanicsDG3DIPVariable &source) : ThermoMechanicsDG3DIPVariableBase(source)
{
  _j2ipv = NULL;
  if (source._j2ipv != NULL){
    _j2ipv = dynamic_cast<IPJ2ThermoMechanics*>(source._j2ipv->clone());
  }
}
J2ThermoMechanicsDG3DIPVariable& J2ThermoMechanicsDG3DIPVariable::operator=(const IPVariable &source)
{
  ThermoMechanicsDG3DIPVariableBase::operator=(source);
  const J2ThermoMechanicsDG3DIPVariable* src = dynamic_cast<const J2ThermoMechanicsDG3DIPVariable*>(&source);
  if(src != NULL){
    if (src->_j2ipv != NULL){
			if (_j2ipv!= NULL){
				_j2ipv->operator=(*dynamic_cast<const IPVariable*>(src->_j2ipv->clone()));
			}
			else
				_j2ipv = dynamic_cast<IPJ2ThermoMechanics*>(src->_j2ipv->clone());
    }
		else{
			if (_j2ipv!=NULL){
				delete _j2ipv;
				_j2ipv=NULL;
			}
		}
  }

  return *this;
}
double J2ThermoMechanicsDG3DIPVariable::get(const int i) const
{
  if(i== IPField::PLASTICSTRAIN)
  {
    return _j2ipv->_j2lepspbarre;
  }
  else
  {
    return ThermoMechanicsDG3DIPVariableBase::get(i);
  }
}
double  J2ThermoMechanicsDG3DIPVariable::defoEnergy() const
{
  return getIPJ2ThermoMechanics()->defoEnergy();
}
double  J2ThermoMechanicsDG3DIPVariable::plasticEnergy() const
{
  return getIPJ2ThermoMechanics()->plasticEnergy();
}

void J2ThermoMechanicsDG3DIPVariable::restart()
{
  ThermoMechanicsDG3DIPVariableBase::restart();
  if (_j2ipv != NULL)
    restartManager::restart(_j2ipv);
  return;
}

J2FullThermoMechanicsDG3DIPVariable::J2FullThermoMechanicsDG3DIPVariable(const J2IsotropicHardening* j2harden, const bool createBodyForceHO,
                       const bool oninter):ThermoMechanicsDG3DIPVariableBase(createBodyForceHO, oninter),_j2Thermo(j2harden){};
J2FullThermoMechanicsDG3DIPVariable::J2FullThermoMechanicsDG3DIPVariable(const J2FullThermoMechanicsDG3DIPVariable& src):
			ThermoMechanicsDG3DIPVariableBase(src),_j2Thermo(src._j2Thermo){};
J2FullThermoMechanicsDG3DIPVariable& J2FullThermoMechanicsDG3DIPVariable::operator =(const IPVariable& src){
	ThermoMechanicsDG3DIPVariableBase::operator =(src);
	const J2FullThermoMechanicsDG3DIPVariable *psrc = dynamic_cast<const J2FullThermoMechanicsDG3DIPVariable*>(&src);
	if (psrc != NULL){
		_j2Thermo.operator=(*(dynamic_cast<const IPVariable*>(&(psrc->_j2Thermo))));
	}
	return *this;
};;

double J2FullThermoMechanicsDG3DIPVariable::get(const int i) const{
  if ( i== IPField::PLASTICSTRAIN){
    return _j2Thermo.getConstRefToEquivalentPlasticStrain();
  }
  else
  {
    return ThermoMechanicsDG3DIPVariableBase::get(i);
  }
};

void J2FullThermoMechanicsDG3DIPVariable::restart(){
	ThermoMechanicsDG3DIPVariableBase::restart();
	_j2Thermo.restart();
};


SMPDG3DIPVariable::SMPDG3DIPVariable( double tinitial,const mlawSMP &_lawSMP,bool createBodyForceHO,  const bool oninter) :
                                   ThermoMechanicsDG3DIPVariableBase(createBodyForceHO, oninter)
{
  _TMIPSMP = new IPSMP(_lawSMP.getSa0(),_lawSMP.getPhia0(),_lawSMP.getSastar0());
  this->getRefToTemperature()=tinitial;
}

SMPDG3DIPVariable::SMPDG3DIPVariable(const SMPDG3DIPVariable &source) :
                                                        ThermoMechanicsDG3DIPVariableBase(source)
{
  _TMIPSMP = NULL;
  if (source._TMIPSMP != NULL){
    _TMIPSMP = dynamic_cast<IPSMP*>(source._TMIPSMP->clone());
  }

}

SMPDG3DIPVariable& SMPDG3DIPVariable::operator=(const IPVariable &source)
{
  ThermoMechanicsDG3DIPVariableBase::operator=(source);
  const SMPDG3DIPVariable* src = dynamic_cast<const SMPDG3DIPVariable*>(&source);
  if(src != NULL)
  {
    if (src->_TMIPSMP != NULL) {
			if (_TMIPSMP!=NULL){
				_TMIPSMP->operator=(*dynamic_cast<const IPVariable*>(src->_TMIPSMP));
			}
			else
				_TMIPSMP = dynamic_cast<IPSMP*>(src->_TMIPSMP->clone());
    }
		else{
			if (_TMIPSMP!=NULL){
				delete _TMIPSMP;
				_TMIPSMP = NULL;
			}
		}
  }
  return *this;
}
double SMPDG3DIPVariable::get(const int i) const
{
  if(i == IPField::PLASTICSTRAIN)
  {
   // return getInternalEnergyExtraDofDiffusion();
    return getRefToEquivalentPlasticDefo1();
  }
 /*   if(i == IPField::Plasric_Defo_mec1)
  {
    return getConstRefToEquivalentPlasticDefo1();
  }*/
  else
  {
    return ThermoMechanicsDG3DIPVariableBase::get(i);
  }
}
 // return ThermoMechanicsDG3DIPVariableBase::get(i);
//}
double SMPDG3DIPVariable::defoEnergy() const
{
  return getIPSMP()->defoEnergy();
}
double SMPDG3DIPVariable::plasticEnergy() const
{
  return getIPSMP()->plasticEnergy();
}

void SMPDG3DIPVariable::restart()
{
  ThermoMechanicsDG3DIPVariableBase::restart();
  if (_TMIPSMP!= NULL)
    restartManager::restart(_TMIPSMP);
  return;
}

//===========/start/Phenomenological===================================================================================
PhenomenologicalSMPDG3DIPVariable::PhenomenologicalSMPDG3DIPVariable(double tinitial, const mlawPhenomenologicalSMP &_lawSMP, const bool createBodyForceHO,
                                  const bool oninter):ThermoMechanicsDG3DIPVariableBase(createBodyForceHO, oninter)
{   _TMIPSMP = new IPPhenomenologicalSMP(tinitial, _lawSMP.getCrystallizationTemperature(), _lawSMP.getMeltingTemperature(),
       _lawSMP.getCrystallizationParameter(), _lawSMP.getMeltingParameter(),
      _lawSMP.getJ2IsotropicHardeningCompressionG(), _lawSMP.getJ2IsotropicHardeningTractionG(), _lawSMP.getKinematicHardeningG(), _lawSMP.getViscoElasticNumberOfElementG(),
      _lawSMP.getJ2IsotropicHardeningCompressionR(), _lawSMP.getJ2IsotropicHardeningTractionR(), _lawSMP.getKinematicHardeningR(), _lawSMP.getViscoElasticNumberOfElementR(),
      _lawSMP.getJ2IsotropicHardeningCompressionAM(), _lawSMP.getJ2IsotropicHardeningTractionAM(), _lawSMP.getKinematicHardeningAM(), _lawSMP.getViscoElasticNumberOfElementAM()  );
    this->getRefToTemperature()=tinitial;
}

PhenomenologicalSMPDG3DIPVariable::PhenomenologicalSMPDG3DIPVariable(const PhenomenologicalSMPDG3DIPVariable &source)
                                  :ThermoMechanicsDG3DIPVariableBase(source)
{_TMIPSMP = NULL;
    if (source._TMIPSMP != NULL)
    {_TMIPSMP = dynamic_cast<IPPhenomenologicalSMP*>(source._TMIPSMP->clone());
    }
}

PhenomenologicalSMPDG3DIPVariable& PhenomenologicalSMPDG3DIPVariable::operator=(const IPVariable &source)
{
    ThermoMechanicsDG3DIPVariableBase::operator=(source);
    const PhenomenologicalSMPDG3DIPVariable* src = dynamic_cast<const PhenomenologicalSMPDG3DIPVariable*>(&source);
    if(src != NULL)
    {
        if (src->_TMIPSMP != NULL)
        {
            if (_TMIPSMP!=NULL)
            {
            _TMIPSMP->operator=(*dynamic_cast<const IPVariable*>(src->_TMIPSMP));}
            else
            _TMIPSMP = dynamic_cast<IPPhenomenologicalSMP*>(src->_TMIPSMP->clone());
        }
        else
        {
            if (_TMIPSMP!=NULL)
            {
                delete _TMIPSMP;
                _TMIPSMP = NULL;
            }
        }
    }
    return *this;
}

double PhenomenologicalSMPDG3DIPVariable::get(const int i) const
{
    double val = ThermoMechanicsDG3DIPVariableBase::get(i);
    if (val == 0.)
        val = _TMIPSMP->get(i);
    return  val;
}

double PhenomenologicalSMPDG3DIPVariable::defoEnergy() const
{return getIPPhenomenologicalSMP()->defoEnergy();
}

double PhenomenologicalSMPDG3DIPVariable::plasticEnergy() const
{return getIPPhenomenologicalSMP()->plasticEnergy();
}

void PhenomenologicalSMPDG3DIPVariable::restart()
{   ThermoMechanicsDG3DIPVariableBase::restart();
    if (_TMIPSMP!= NULL)
        restartManager::restart(_TMIPSMP);
    return;
}
//===========/end/Phenomenological===================================================================================



//
crystalPlasticityDG3DIPVariable::crystalPlasticityDG3DIPVariable(int _nsdv, double size, const bool createBodyForceHO, const bool oninter) : dG3DIPVariable(createBodyForceHO, oninter,0)
{
  _ipv = new IPCrystalPlasticity(_nsdv,size);
}

crystalPlasticityDG3DIPVariable::crystalPlasticityDG3DIPVariable(const crystalPlasticityDG3DIPVariable &source) :
                                                                   dG3DIPVariable(source)
{
  _ipv = NULL;
  if (source.getIPCrystalPlasticity() != NULL){
    _ipv = new IPCrystalPlasticity((int)source.getIPCrystalPlasticity()->getNsdv(),(double)source.getIPCrystalPlasticity()->elementSize());
    _ipv->operator= (*dynamic_cast<const IPVariable*>(source.getIPCrystalPlasticity()));
  }

}

crystalPlasticityDG3DIPVariable& crystalPlasticityDG3DIPVariable::operator=(const IPVariable &source)
{
  dG3DIPVariable::operator=(source);
  const crystalPlasticityDG3DIPVariable* src = dynamic_cast<const crystalPlasticityDG3DIPVariable*>(&source);
  if(src != NULL)
  {
    if (src->getIPCrystalPlasticity() != NULL){
      if (_ipv != NULL){
        _ipv->operator=(*dynamic_cast<const IPVariable*>(src->getIPCrystalPlasticity()));
      }
      else{
        _ipv = dynamic_cast<IPCrystalPlasticity*>(src->getIPCrystalPlasticity()->clone());
      }
    }
    else{
      if (_ipv != NULL){
        delete _ipv;
        _ipv = NULL;
      }
    }
  }
  return *this;
}
double crystalPlasticityDG3DIPVariable::get(const int i) const
{
  double val = dG3DIPVariable::get(i);
  if (val == 0)
    val = getIPCrystalPlasticity()->get(i);
  return val;
}
double crystalPlasticityDG3DIPVariable::defoEnergy() const
{
  return getIPCrystalPlasticity()->defoEnergy();
}
double crystalPlasticityDG3DIPVariable::plasticEnergy() const
{
  return getIPCrystalPlasticity()->plasticEnergy();
}


void crystalPlasticityDG3DIPVariable::restart()
{
  dG3DIPVariable::restart();
  if (_ipv != NULL)
    restartManager::restart(_ipv);
  return;
}

//

//
VEVPUMATDG3DIPVariable::VEVPUMATDG3DIPVariable(int _nsdv, double size, const bool createBodyForceHO, const bool oninter):
                                                                           dG3DIPVariable(createBodyForceHO, oninter,0)
{
  _ipv = new IPVEVPUMAT(_nsdv,size);
}

VEVPUMATDG3DIPVariable::VEVPUMATDG3DIPVariable(const VEVPUMATDG3DIPVariable &source) :
                                                                   dG3DIPVariable(source)
{
  _ipv = NULL;
  if (source.getIPVEVPUMAT() != NULL){
    _ipv = new IPVEVPUMAT((int)source.getIPVEVPUMAT()->getNsdv(),(double)source.getIPVEVPUMAT()->elementSize());
    _ipv->operator= (*dynamic_cast<const IPVariable*>(source.getIPVEVPUMAT()));
  }

}

VEVPUMATDG3DIPVariable& VEVPUMATDG3DIPVariable::operator=(const IPVariable &source)
{
  dG3DIPVariable::operator=(source);
  const VEVPUMATDG3DIPVariable* src = dynamic_cast<const VEVPUMATDG3DIPVariable*>(&source);
  if(src != NULL)
  {
    if (src->getIPVEVPUMAT() != NULL){
      if (_ipv != NULL){
        _ipv->operator=(*dynamic_cast<const IPVariable*>(src->getIPVEVPUMAT()));
      }
      else{
        _ipv = dynamic_cast<IPVEVPUMAT*>(src->getIPVEVPUMAT()->clone());
      }
    }
    else{
      if (_ipv != NULL){
        delete _ipv;
        _ipv = NULL;
      }
    }
  }
  return *this;
}
double VEVPUMATDG3DIPVariable::get(const int i) const
{
  double val = dG3DIPVariable::get(i);
  if (val == 0)
    val = getIPVEVPUMAT()->get(i);
  return val;
}
double VEVPUMATDG3DIPVariable::defoEnergy() const
{
  return getIPVEVPUMAT()->defoEnergy();
}
double VEVPUMATDG3DIPVariable::plasticEnergy() const
{
  return getIPVEVPUMAT()->plasticEnergy();
}


void VEVPUMATDG3DIPVariable::restart()
{
  dG3DIPVariable::restart();
  if (_ipv != NULL)
    restartManager::restart(_ipv);
  return;
}

//
IMDEACPUMATDG3DIPVariable::IMDEACPUMATDG3DIPVariable(int _nsdv, double size, const bool createBodyForceHO, const bool oninter):
                                                                           dG3DIPVariable(createBodyForceHO, oninter,0)
{
  _ipv = new IPIMDEACPUMAT(_nsdv,size);
}

IMDEACPUMATDG3DIPVariable::IMDEACPUMATDG3DIPVariable(const IMDEACPUMATDG3DIPVariable &source) :
                                                                   dG3DIPVariable(source)
{
  _ipv = NULL;
  if (source.getIPIMDEACPUMAT() != NULL){
    _ipv = new IPIMDEACPUMAT((int)source.getIPIMDEACPUMAT()->getNsdv(),(double)source.getIPIMDEACPUMAT()->elementSize());
    _ipv->operator= (*dynamic_cast<const IPVariable*>(source.getIPIMDEACPUMAT()));
  }

}

IMDEACPUMATDG3DIPVariable& IMDEACPUMATDG3DIPVariable::operator=(const IPVariable &source)
{
  dG3DIPVariable::operator=(source);
  const IMDEACPUMATDG3DIPVariable* src = dynamic_cast<const IMDEACPUMATDG3DIPVariable*>(&source);
  if(src != NULL)
  {
    if (src->getIPIMDEACPUMAT() != NULL){
      if (_ipv != NULL){
        _ipv->operator=(*dynamic_cast<const IPVariable*>(src->getIPIMDEACPUMAT()));
      }
      else{
        _ipv = dynamic_cast<IPIMDEACPUMAT*>(src->getIPIMDEACPUMAT()->clone());
      }
    }
    else{
      if (_ipv != NULL){
        delete _ipv;
        _ipv = NULL;
      }
    }
  }
  return *this;
}
double IMDEACPUMATDG3DIPVariable::get(const int i) const
{
  double val = dG3DIPVariable::get(i);
  if (val == 0)
    val = getIPIMDEACPUMAT()->get(i);
  return val;
}
double IMDEACPUMATDG3DIPVariable::defoEnergy() const
{
  return getIPIMDEACPUMAT()->defoEnergy();
}
double IMDEACPUMATDG3DIPVariable::plasticEnergy() const
{
  return getIPIMDEACPUMAT()->plasticEnergy();
}


void IMDEACPUMATDG3DIPVariable::restart()
{
  dG3DIPVariable::restart();
  if (_ipv != NULL)
    restartManager::restart(_ipv);
  return;
}

//
gursonUMATDG3DIPVariable::gursonUMATDG3DIPVariable(int _nsdv, double size, const bool createBodyForceHO, const bool oninter):
                                                                           dG3DIPVariable(createBodyForceHO, oninter,0)
{
  _ipv = new IPGursonUMAT(_nsdv,size);
}

gursonUMATDG3DIPVariable::gursonUMATDG3DIPVariable(const gursonUMATDG3DIPVariable &source) :
                                                                   dG3DIPVariable(source)
{
  _ipv = NULL;
  if (source.getIPGursonUMAT() != NULL){
    _ipv = new IPGursonUMAT((int)source.getIPGursonUMAT()->getNsdv(),(double)source.getIPGursonUMAT()->elementSize());
    _ipv->operator= (*dynamic_cast<const IPVariable*>(source.getIPGursonUMAT()));
  }

}

gursonUMATDG3DIPVariable& gursonUMATDG3DIPVariable::operator=(const IPVariable &source)
{
  dG3DIPVariable::operator=(source);
  const gursonUMATDG3DIPVariable* src = dynamic_cast<const gursonUMATDG3DIPVariable*>(&source);
  if(src != NULL)
  {
    if (src->getIPGursonUMAT() != NULL){
      if (_ipv != NULL){
        _ipv->operator=(*dynamic_cast<const IPVariable*>(src->getIPGursonUMAT()));
      }
      else{
        _ipv = dynamic_cast<IPGursonUMAT*>(src->getIPGursonUMAT()->clone());
      }
    }
    else{
      if (_ipv != NULL){
        delete _ipv;
        _ipv = NULL;
      }
    }
  }
  return *this;
}
double gursonUMATDG3DIPVariable::get(const int i) const
{
  double val = dG3DIPVariable::get(i);
  if (val == 0)
    val = getIPGursonUMAT()->get(i);
  return val;
}
double gursonUMATDG3DIPVariable::defoEnergy() const
{
  return getIPGursonUMAT()->defoEnergy();
}
double gursonUMATDG3DIPVariable::plasticEnergy() const
{
  return getIPGursonUMAT()->plasticEnergy();
}


void gursonUMATDG3DIPVariable::restart()
{
  dG3DIPVariable::restart();
  if (_ipv != NULL)
    restartManager::restart(_ipv);
  return;
}

//
ElecTherMechDG3DIPVariableBase::ElecTherMechDG3DIPVariableBase(bool createBodyForceHO,  const bool oninter) :
                                                    dG3DIPVariable(createBodyForceHO, oninter,0,2,0,true,true)

{

}




ElecTherMechDG3DIPVariableBase::ElecTherMechDG3DIPVariableBase(const ElecTherMechDG3DIPVariableBase &source) :
                                                                        dG3DIPVariable(source)
{
/*
  //Temperature    = source.Temperature;
  //gradT          = source.gradT;
  //fluxT          = source.fluxT;
  //dPdT           = source.dPdT;
  //dFluxTdGradT   = source.dFluxTdGradT ;
  //dFluxTdT       = source.dFluxTdT;
  //dFluxTdGradV   = source.dFluxTdGradV ;
  //dFluxTdV       = source.dFluxTdV;
  //dFluxTdF       = source.dFluxTdF;
  //TemperatureJump= source.TemperatureJump;
  //interfaceFluxT = source.interfaceFluxT;

  //Voltage        = source.Voltage;
  //gradV          = source.gradV;
  //fluxV          = source.fluxV;
  //dPdV           = source.dPdV;
  //dFluxVdGradV   = source.dFluxVdGradV ;
  //dFluxVdV       = source.dFluxVdV;
  //dFluxVdGradT   = source.dFluxVdGradT ;
  //dFluxVdT       = source.dFluxVdT;
  //dFluxVdF       = source.dFluxVdF;
  //VoltageJump    = source.VoltageJump;
  //interfaceFluxV = source.interfaceFluxV;
 //thermalSource  = source.thermalSource;
  //dThermalSourcedF = source.dThermalSourcedF;
  //dThermalSourcedT = source.dThermalSourcedT;


  //l10		= source.l10;
  //l20		= source.l20;
  //k10		= source.k10;
  //k20		= source.k20;

  //dl10dT	= source.dl10dT;
  //dl20dT	= source.dl20dT;
  //dk10dT	= source.dk10dT;
  //dk20dT	= source.dk20dT;

  //dl20dv	= source.dl20dv;
  //dk10dv	= source.dk10dv;

  //dl10dF	= source.dl10dF;
  //dl20dF	= source.dl20dF;
  //dk10dF	= source.dk10dF;
  //dk20dF	= source.dk20dF;

  //fvJump	= source.fvJump;
 // fTJump	= source.fTJump;
 // dfvjumpdvm	= source.dfvjumpdvm;
  //dfvjumpdvp	= source.dfvjumpdvp;
  //dfvjumpdTm	= source.dfvjumpdTm;
  //dfvjumpdTp	= source.dfvjumpdTp;
  //dfTjumpdTm	= source.dfTjumpdTm;
 // dfTjumpdTp	= source.dfTjumpdTp;


  //fT             = source.fT;
  //gradfT         = source.gradfT;
  //fV             = source.fV;
  //gradfV         = source.gradfV;

  //dgradVdgradfV	= source.dgradVdgradfV;
  //dgradVdgradfT	= source.dgradVdgradfT;
  //dgradTdgradfT	= source.dgradTdgradfT;
  //dgradTdgradfV	= source.dgradTdgradfV;
  //dVdfV		= source. dVdfV;
  //dVdfT		= source. dVdfT;
  //dTdfT		= source. dTdfT;
  //dTdfV		= source. dTdfV;
  //dgradVdfV	= source.dgradVdfV;
  //dgradVdfT	= source.dgradVdfT;
  //dgradTdfT	= source.dgradTdfT;
  //dgradTdfV	= source.dgradTdfV;


  //fluxjy	= source.fluxjy;
  //djydV 	= source.djydV;
  //djydgradV	= source.djydgradV;
  //djydT		= source.djydT;
  //djydgradT	= source.djydgradT;
  //djydF		= source.djydF;
  //jy1		= source.jy1;
  //djy1dT	= source.djy1dT;
  //djy1dV	= source.djy1dV;
  //djy1dF	= source.djy1dF;
*/


}
ElecTherMechDG3DIPVariableBase& ElecTherMechDG3DIPVariableBase::operator=(const IPVariable &source)
{
  dG3DIPVariable::operator=(source);
  const ElecTherMechDG3DIPVariableBase* src = dynamic_cast<const ElecTherMechDG3DIPVariableBase*>(&source);
  if(src != NULL)
  {
  /*  lineark10      = src->lineark10;
    lineark20      = src->lineark20;*/
/*    //Temperature    = src->Temperature;
    //gradT          = src->gradT;
    //fluxT          = src->fluxT;
    //dPdT           = src-> dPdT;
    //dFluxTdGradT   = src->dFluxTdGradT;
    //dFluxTdT       = src->dFluxTdT;
    //dFluxTdGradV   = src->dFluxTdGradV;
    //dFluxTdV       = src->dFluxTdV;
    //dFluxTdF       = src->dFluxTdF;
    //TemperatureJump= src->TemperatureJump;
    //interfaceFluxT = src-> interfaceFluxT;
*/
   /* linearl10      = src->linearl10;
    linearl20      = src->linearl20;*/
/*    //Voltage        = src->Voltage;
    //gradV          = src->gradV;
    //fluxV          = src->fluxV;
    //dPdV           = src->dPdV;
    //dFluxVdGradV   = src->dFluxVdGradV;
    //dFluxVdV       = src->dFluxVdV;
    //dFluxVdGradT   = src->dFluxVdGradT;
    //dFluxVdT       = src->dFluxVdT;
    //dFluxVdF       = src->dFluxVdF;
    //VoltageJump    = src->VoltageJump;
    //interfaceFluxV = src-> interfaceFluxV;
    //thermalSource  = src-> thermalSource;
    //dThermalSourcedF = src-> dThermalSourcedF;
    //dThermalSourcedT = src-> dThermalSourcedT;

    //l10 	= src->l10;
    //l20 	= src->l20;
    //k10 	= src->k10;
    //k20 	= src->k20;

    //dl10dT 	= src->dl10dT;
    //dl20dT 	= src->dl20dT;
    //dk10dT 	= src->dk10dT;
    //dk20dT 	= src->dk20dT;

    //dl20dv 	= src->dl20dv;
    //dk10dv 	= src->dk10dv;

    //dl10dF 	= src->dl10dF;
    //dl20dF 	= src->dl20dF;
    //dk10dF 	= src->dk10dF;
    //dk20dF 	= src->dk20dF;

    //fvJump	= src->fvJump;
    //fTJump	= src->fTJump;
    //dfvjumpdvm	= src->dfvjumpdvm;
    //dfvjumpdvp	= src->dfvjumpdvp;
    //dfvjumpdTm	= src->dfvjumpdTm;
    //dfvjumpdTp	= src->dfvjumpdTp;
    //dfTjumpdTm	= src->dfTjumpdTm;
    //dfTjumpdTp	= src->dfTjumpdTp;


    //fT             = src->fT;
    //gradfT         = src->gradfT;
    //fV             = src->fV;
    //gradfV         = src->gradfV;

    //dgradVdgradfV= src->dgradVdgradfV;
    //dgradVdgradfT= src->dgradVdgradfT;
    //dgradTdgradfT= src->dgradTdgradfT;
    //dgradTdgradfV= src->dgradTdgradfV;
    //dVdfV	= src-> dVdfV;
    //dVdfT	= src-> dVdfT;
    //dTdfT	= src-> dTdfT;
    //dTdfV	= src-> dTdfV;
    //dgradVdfV	= src->dgradVdfV;
    //dgradVdfT	= src->dgradVdfT;
    //dgradTdfT	= src->dgradTdfT;
    //dgradTdfV	= src->dgradTdfV;

    //fluxjy	= src->fluxjy;
    //djydV	=src->djydV;
    //djydgradV	= src->djydgradV;
    //djydT	= src->djydT;
    //djydgradT	= src->djydgradT;
    //djydF	= src->djydF;
    //jy1		= src->jy1;
    //djy1dT	=src->djy1dT;
    //djy1dV	=src->djy1dV;
    //djy1dF	=src->djy1dF;
*/

  }
  return *this;

}
double ElecTherMechDG3DIPVariableBase::get(const int i) const
{
  if(i == IPField::TEMPERATURE)
  {
    return getConstRefToTemperature();
  }
  else if(i == IPField::THERMALFLUX_X)
  {
    return getConstRefToFlux()[0](0);
  }
  else if(i == IPField::THERMALFLUX_Y)
  {
    return getConstRefToFlux()[0](1);
  }
  else if(i == IPField::THERMALFLUX_Z)
  {
    return getConstRefToFlux()[0](2);
  }
  else if(i == IPField::VOLTAGE)
  {
    return getConstRefToVoltage();
  }
  else if(i == IPField::ELECTRICALFLUX_X)
  {
    return getConstRefToFlux()[1](0);
  }
  else if(i == IPField::ELECTRICALFLUX_Y)
  {
    return getConstRefToFlux()[1](1);
  }
  else if(i == IPField::ELECTRICALFLUX_Z)
  {
    return getConstRefToFlux()[1](2);
  }
  else
  {
    return dG3DIPVariable::get(i);
  }
}


void ElecTherMechDG3DIPVariableBase::restart()
{
  dG3DIPVariable::restart();
  /*
  //restartManager::restart(Temperature);
  //restartManager::restart(gradT);
  //restartManager::restart(fluxT);
  //restartManager::restart(dPdT);
  //restartManager::restart(dFluxTdGradT);
  //restartManager::restart(dFluxTdT);
  //restartManager::restart(dFluxTdGradV);
  //restartManager::restart(dFluxTdV);
  //restartManager::restart(dFluxTdF);
  ///const STensor3               *lineark10; // Nor save nor load
  ///const STensor3               *lineark20; // nor save nor load
  //restartManager::restart(TemperatureJump);
  //restartManager::restart(interfaceFluxT);

  //restartManager::restart(Voltage);
  //restartManager::restart(gradV);
  //restartManager::restart(fluxV);
  //restartManager::restart(dPdV);
  //restartManager::restart(dFluxVdGradV);
  //restartManager::restart(dFluxVdV);
  //restartManager::restart(dFluxVdGradV);
  //restartManager::restart(dFluxVdV);
  //restartManager::restart(dFluxVdGradT);
  //restartManager::restart(dFluxVdT);
  //restartManager::restart(dFluxVdF);
  ///const STensor3               *linearl10;  Nor save nor load
  ///const STensor3               *linearl20;  Nor save nor load
  //restartManager::restart(VoltageJump);
  //restartManager::restart(interfaceFluxV);
  //restartManager::restart(thermalSource);
  //restartManager::restart(dThermalSourcedT);
  //restartManager::restart(dThermalSourcedF);

  //modified
  //restartManager::restart(l10);
  //restartManager::restart(l20);
  //restartManager::restart(k10);
  //restartManager::restart(k20);
  //restartManager::restart(dl10dT);
  //restartManager::restart(dl20dT);
  //restartManager::restart(dk10dT);
  //restartManager::restart(dk20dT);
  //restartManager::restart(dk10dv);
  //restartManager::restart(dl20dv);

  //restartManager::restart(dl10dF);
  //restartManager::restart(dl20dF);
  //restartManager::restart(dk10dF);
  //restartManager::restart(dk20dF);

  //restartManager::restart(fvJump);
 //restartManager::restart(fTJump);
  //restartManager::restart(dfvjumpdvm);
  //restartManager::restart(dfvjumpdvp);
  //restartManager::restart(dfvjumpdTm);
  //restartManager::restart(dfvjumpdTp);
  //restartManager::restart(dfTjumpdTm);
  //restartManager::restart(dfTjumpdTp);

  //restartManager::restart(fT);
  //restartManager::restart(gradfT);
  //restartManager::restart(fV);
  //restartManager::restart(gradfV);

  //restartManager::restart(dgradVdgradfV);
  //restartManager::restart(dgradVdgradfT);
  //restartManager::restart(dgradTdgradfT);
  //restartManager::restart(dgradTdgradfV);
  //restartManager::restart(dVdfV);
  //restartManager::restart(dVdfT);
  //restartManager::restart(dTdfT);
  //restartManager::restart(dTdfV);
  //restartManager::restart(dgradVdfV);
  //restartManager::restart(dgradVdfT);
  //restartManager::restart(dgradTdfT);
  //restartManager::restart(dgradTdfV);

  //restartManager::restart(fluxjy);
  //restartManager::restart(djydV);
  //restartManager::restart(djydgradV);
  //restartManager::restart(djydT);
  //restartManager::restart(djydgradT);
  //restartManager::restart(djydF);
  //restartManager::restart(jy1);
  //restartManager::restart(djy1dT);
  //restartManager::restart(djy1dV);
  //restartManager::restart(djy1dF);
*/

  return;
}

LinearElecTherMechDG3DIPVariable::LinearElecTherMechDG3DIPVariable( double tinitial,double vinitial,bool createBodyForceHO, const bool oninter) :
                                   ElecTherMechDG3DIPVariableBase(createBodyForceHO, oninter)
{

this->getRefToTemperature()=tinitial;
this->getRefToVoltage()=vinitial;
this->getRefTofT()         =1./tinitial;
this->getRefTofV()         =-vinitial/tinitial;
}


LinearElecTherMechDG3DIPVariable::LinearElecTherMechDG3DIPVariable(const LinearElecTherMechDG3DIPVariable &source) :
                                                        ElecTherMechDG3DIPVariableBase(source),
																												_linearETMIP(source._linearETMIP)
{

}

LinearElecTherMechDG3DIPVariable& LinearElecTherMechDG3DIPVariable::operator=(const IPVariable &source)
{
  ElecTherMechDG3DIPVariableBase::operator=(source);
  const LinearElecTherMechDG3DIPVariable* src = dynamic_cast<const LinearElecTherMechDG3DIPVariable*>(&source);
  if(src != NULL)
  {
    const IPVariable* tmp= dynamic_cast<const IPVariable*>(&src->_linearETMIP);
    _linearETMIP.operator=(*tmp);
  }
  return *this;
}
double LinearElecTherMechDG3DIPVariable::get(const int i) const
{

  return ElecTherMechDG3DIPVariableBase::get(i);
}

//should i add to ipField Voltage and  Vflux_X ,Y ,Z ????


double LinearElecTherMechDG3DIPVariable::defoEnergy() const
{
  return getIPLinearElecTherMech()->defoEnergy();
}
double LinearElecTherMechDG3DIPVariable::plasticEnergy() const
{
  return getIPLinearElecTherMech()->plasticEnergy();
}

void LinearElecTherMechDG3DIPVariable::restart()
{
  ElecTherMechDG3DIPVariableBase::restart();
  restartManager::restart(&_linearETMIP);
  return;
}


ElecMagTherMechDG3DIPVariableBase::ElecMagTherMechDG3DIPVariableBase(bool createBodyForceHO, const bool oninter)
: dG3DIPVariable(createBodyForceHO, oninter,0,2,1,true,true)
{}

ElecMagTherMechDG3DIPVariableBase::ElecMagTherMechDG3DIPVariableBase(const ElecMagTherMechDG3DIPVariableBase & source)
: dG3DIPVariable(source)
{
}

ElecMagTherMechDG3DIPVariableBase& ElecMagTherMechDG3DIPVariableBase::operator=(const IPVariable &source)
{
  dG3DIPVariable::operator=(source);
  const ElecMagTherMechDG3DIPVariableBase* src = dynamic_cast<const ElecMagTherMechDG3DIPVariableBase*>(&source);
//  if(src != NULL)
//  {
//
//  }

  return *this;
}

double ElecMagTherMechDG3DIPVariableBase::get(const int i) const
{
  if(i == IPField::TEMPERATURE)
  {
    return getConstRefToTemperature();
  }
  else if(i == IPField::THERMALFLUX_X)
  {
    return getConstRefToFlux()[0](0);
  }
  else if(i == IPField::THERMALFLUX_Y)
  {
    return getConstRefToFlux()[0](1);
  }
  else if(i == IPField::THERMALFLUX_Z)
  {
    return getConstRefToFlux()[0](2);
  }
  else if(i == IPField::THERMALSOURCE)
  {
      return getConstRefToThermalSource();
  }
  else if(i == IPField::ELECTRICALSOURCE)
  {
      return getConstRefToVoltageSource();
  }
  else if(i == IPField::VOLTAGE)
  {
    return getConstRefToVoltage();
  }
  else if(i == IPField::ELECTRICALFLUX_X)
  {
    return getConstRefToFlux()[1](0);
  }
  else if(i == IPField::ELECTRICALFLUX_Y)
  {
    return getConstRefToFlux()[1](1);
  }
  else if(i == IPField::ELECTRICALFLUX_Z)
  {
    return getConstRefToFlux()[1](2);
  }
  else if(i == IPField::MAGNETICVECTORPOTENTIAL_X)
  {
    return getConstRefToMagneticVectorPotential()[0];
  }
    else if(i == IPField::MAGNETICVECTORPOTENTIAL_Y)
  {
    return getConstRefToMagneticVectorPotential()[1];
  }
  else if(i == IPField::MAGNETICVECTORPOTENTIAL_Z)
  {
    return getConstRefToMagneticVectorPotential()[2];
  }
  else if(i == IPField::MAGNETICINDUCTION_X)
  {
    return getConstRefToMagneticInduction()[0];
  }
  else if(i == IPField::MAGNETICINDUCTION_Y)
  {
    return getConstRefToMagneticInduction()[1];
  }
  else if(i == IPField::MAGNETICINDUCTION_Z)
  {
    return getConstRefToMagneticInduction()[2];
  }
  else if(i == IPField::MAGNETICFIELD_X)
  {
      return getConstRefToMagneticField()[0];
  }
  else if(i == IPField::MAGNETICFIELD_Y)
  {
      return getConstRefToMagneticField()[1];
  }
  else if(i == IPField::MAGNETICFIELD_Z)
  {
      return getConstRefToMagneticField()[2];
  }
  else if(i == IPField::EMSOURCEVECTORFIELD_X)
  {
    return getConstRefTosourceVectorField()[0];
  }
  else if(i == IPField::EMSOURCEVECTORFIELD_Y)
  {
    return getConstRefTosourceVectorField()[1];
  }
  else if(i == IPField::EMSOURCEVECTORFIELD_Z)
  {
    return getConstRefTosourceVectorField()[2];
  }
  else if(i == IPField::INDUCTORSOURCEVECTORFIELD_X)
  {
      return getConstRefToinductorSourceVectorField()[0];
  }
  else if(i == IPField::INDUCTORSOURCEVECTORFIELD_Y)
  {
      return getConstRefToinductorSourceVectorField()[1];
  }
  else if(i == IPField::INDUCTORSOURCEVECTORFIELD_Z)
  {
      return getConstRefToinductorSourceVectorField()[2];
  }
  else if(i == IPField::EMFIELDSOURCE)
  {
      return getConstRefToThermalEMFieldSource();
  }
  else if(i == IPField::ELECDISPLACEMENT_X)
  {
      return getConstRefToElecDisplacement()[0];
  }
  else if(i == IPField::ELECDISPLACEMENT_Y)
  {
      return getConstRefToElecDisplacement()[1];
  }
  else if(i == IPField::ELECDISPLACEMENT_Z)
  {
      return getConstRefToElecDisplacement()[2];
  }
  else
  {
    return dG3DIPVariable::get(i);
  }
}

double & ElecMagTherMechDG3DIPVariableBase::getRef(const int i)
{
    if(i == IPField::TEMPERATURE)
    {
        return getRefToTemperature();
    }
    else if(i == IPField::THERMALFLUX_X)
    {
        return getRefToFlux()[0](0);
    }
    else if(i == IPField::THERMALFLUX_Y)
    {
        return getRefToFlux()[0](1);
    }
    else if(i == IPField::THERMALFLUX_Z)
    {
        return getRefToFlux()[0](2);
    }
    else if(i == IPField::THERMALSOURCE)
    {
        return getRefToThermalSource();
    }
    else if(i == IPField::ELECTRICALSOURCE)
    {
        return getRefToVoltageSource();
    }
    else if(i == IPField::VOLTAGE)
    {
        return getRefToVoltage();
    }
    else if(i == IPField::ELECTRICALFLUX_X)
    {
        return getRefToFlux()[1](0);
    }
    else if(i == IPField::ELECTRICALFLUX_Y)
    {
        return getRefToFlux()[1](1);
    }
    else if(i == IPField::ELECTRICALFLUX_Z)
    {
        return getRefToFlux()[1](2);
    }
    else if(i == IPField::MAGNETICVECTORPOTENTIAL_X)
    {
        return getRefToMagneticVectorPotential()[0];
    }
    else if(i == IPField::MAGNETICVECTORPOTENTIAL_Y)
    {
        return getRefToMagneticVectorPotential()[1];
    }
    else if(i == IPField::MAGNETICVECTORPOTENTIAL_Z)
    {
        return getRefToMagneticVectorPotential()[2];
    }
    else if(i == IPField::MAGNETICINDUCTION_X)
    {
        return getRefToMagneticInduction()[0];
    }
    else if(i == IPField::MAGNETICINDUCTION_Y)
    {
        return getRefToMagneticInduction()[1];
    }
    else if(i == IPField::MAGNETICINDUCTION_Z)
    {
        return getRefToMagneticInduction()[2];
    }
    else if(i == IPField::MAGNETICFIELD_X)
    {
        return getRefToMagneticField()[0];
    }
    else if(i == IPField::MAGNETICFIELD_Y)
    {
        return getRefToMagneticField()[1];
    }
    else if(i == IPField::MAGNETICFIELD_Z)
    {
        return getRefToMagneticField()[2];
    }
    else if(i == IPField::EMSOURCEVECTORFIELD_X)
    {
        return getRefTosourceVectorField()[0];
    }
    else if(i == IPField::EMSOURCEVECTORFIELD_Y)
    {
        return getRefTosourceVectorField()[1];
    }
    else if(i == IPField::EMSOURCEVECTORFIELD_Z)
    {
        return getRefTosourceVectorField()[2];
    }
    else if(i == IPField::INDUCTORSOURCEVECTORFIELD_X)
    {
        return getRefToinductorSourceVectorField()[0];
    }
    else if(i == IPField::INDUCTORSOURCEVECTORFIELD_Y)
    {
        return getRefToinductorSourceVectorField()[1];
    }
    else if(i == IPField::INDUCTORSOURCEVECTORFIELD_Z)
    {
        return getRefToinductorSourceVectorField()[2];
    }
    else if(i == IPField::EMFIELDSOURCE)
    {
        return getRefToThermalEMFieldSource();
    }
    else if(i == IPField::ELECDISPLACEMENT_X)
    {
        return getRefToElecDisplacement()[0];
    }
    else if(i == IPField::ELECDISPLACEMENT_Y)
    {
        return getRefToElecDisplacement()[1];
    }
    else if(i == IPField::ELECDISPLACEMENT_Z)
    {
        return getRefToElecDisplacement()[2];
    }
    else
    {
        return dG3DIPVariable::getRef(i);
    }
}

void ElecMagTherMechDG3DIPVariableBase::restart()
{
    dG3DIPVariable::restart();
}

LinearElecMagTherMechDG3DIPVariable::LinearElecMagTherMechDG3DIPVariable( double tinitial,double vinitial,
const SVector3 & magvecpotinitial, const bool createBodyForceHO, const bool oninter)
: ElecMagTherMechDG3DIPVariableBase(createBodyForceHO, oninter)
{
    this->getRefToTemperature() = tinitial;
    this->getRefToVoltage() = vinitial;
    this->getRefTofT() = 1./tinitial;
    this->getRefTofV() = -vinitial/tinitial;
    this->getRefToMagneticVectorPotential() = magvecpotinitial;
}

LinearElecMagTherMechDG3DIPVariable::LinearElecMagTherMechDG3DIPVariable(const LinearElecMagTherMechDG3DIPVariable &source)
: ElecMagTherMechDG3DIPVariableBase(source),_linearEMTMIP(source._linearEMTMIP)
{

}

LinearElecMagTherMechDG3DIPVariable& LinearElecMagTherMechDG3DIPVariable::operator=(const IPVariable &source)
{
    ElecMagTherMechDG3DIPVariableBase::operator=(source);

    const LinearElecMagTherMechDG3DIPVariable* src = dynamic_cast<const LinearElecMagTherMechDG3DIPVariable*>(&source);
    if(src != NULL)
    {
        //_linearEMTMIP.operator=(src->_linearEMTMIP);
        const IPVariable* tmp= dynamic_cast<const IPVariable*>(&(src->_linearEMTMIP));
        _linearEMTMIP.operator=(*tmp);
        //_linearEMTMIP.operator=(src->_linearEMTMIP);
    }
    return *this;
}

double LinearElecMagTherMechDG3DIPVariable::get(const int comp) const
{
    double val =ElecMagTherMechDG3DIPVariableBase::get(comp);
    if (val == 0)
        val = _linearEMTMIP.get(comp);
    return val;
}

double & LinearElecMagTherMechDG3DIPVariable::getRef(const int i)
{

    return ElecMagTherMechDG3DIPVariableBase::getRef(i);
}

double LinearElecMagTherMechDG3DIPVariable::defoEnergy() const
{
    return getIPLinearElecMagTherMech()->defoEnergy();
}

double LinearElecMagTherMechDG3DIPVariable::plasticEnergy() const
{
    return getIPLinearElecMagTherMech()->plasticEnergy();
}

void LinearElecMagTherMechDG3DIPVariable::restart()
{
    ElecMagTherMechDG3DIPVariableBase::restart();
    restartManager::restart(&_linearEMTMIP);
    return;
}

 LinearElecMagInductorDG3DIPVariable::LinearElecMagInductorDG3DIPVariable(const SVector3 &GaussP, const SVector3 &Centroid,
const SVector3 &CentralAxis, const bool createBodyForceHO, const bool oninter)
: ElecMagTherMechDG3DIPVariableBase(createBodyForceHO, oninter), _linearEMInductor(GaussP, Centroid, CentralAxis)
{}

LinearElecMagInductorDG3DIPVariable::LinearElecMagInductorDG3DIPVariable(const LinearElecMagInductorDG3DIPVariable &source)
: ElecMagTherMechDG3DIPVariableBase(source), _linearEMInductor(source._linearEMInductor)
{}

LinearElecMagInductorDG3DIPVariable & LinearElecMagInductorDG3DIPVariable::operator=(const IPVariable &source)
{
  ElecMagTherMechDG3DIPVariableBase::operator=(source);
  const LinearElecMagInductorDG3DIPVariable* src = dynamic_cast<const LinearElecMagInductorDG3DIPVariable*>(&source);
  if(src != NULL)
  {
    const IPVariable* tmp =dynamic_cast<const IPVariable*>(&( src->_linearEMInductor));
    _linearEMInductor.operator=(*tmp);
    //_linearEMInductor.operator=(src->_linearEMInductor);

  }
  return *this;
}

double LinearElecMagInductorDG3DIPVariable::get(const int comp) const
{
  double val =ElecMagTherMechDG3DIPVariableBase::get(comp);
  if (val == 0)
        val = _linearEMInductor.get(comp);
  return val;

}

double & LinearElecMagInductorDG3DIPVariable::getRef(const int comp)
{
    return ElecMagTherMechDG3DIPVariableBase::getRef(comp);
}

double LinearElecMagInductorDG3DIPVariable::defoEnergy() const
{
  return getIPLinearElecMagInductor()->defoEnergy();
}
double LinearElecMagInductorDG3DIPVariable::plasticEnergy() const
{
  return getIPLinearElecMagInductor()->plasticEnergy();
}

void LinearElecMagInductorDG3DIPVariable::restart()
{
  ElecMagTherMechDG3DIPVariableBase::restart();
  restartManager::restart(&_linearEMInductor);
  return;
}


AnIsotropicElecTherMechDG3DIPVariable::AnIsotropicElecTherMechDG3DIPVariable(double tinitial,double vinitial,const mlawAnIsotropicElecTherMech &_lawETM, const bool createBodyForceHO,  const bool oninter) :
                                  ElecTherMechDG3DIPVariableBase(createBodyForceHO, oninter)
{
  _ETMIP = new IPAnIsotropicElecTherMech();//_lawETM.getSa0(),_lawETM.getPhia0(),_lawETM.getSastar0());
  this->getRefToTemperature()=tinitial;
  this->getRefToVoltage()=vinitial;
  this->getRefTofT()         =1./tinitial;
  this->getRefTofV()         =-vinitial/tinitial;

}

AnIsotropicElecTherMechDG3DIPVariable::AnIsotropicElecTherMechDG3DIPVariable(const AnIsotropicElecTherMechDG3DIPVariable &source) :
                                                        ElecTherMechDG3DIPVariableBase(source)
{
  _ETMIP = NULL;
  if(source._ETMIP != NULL)
    _ETMIP = new IPAnIsotropicElecTherMech(*source._ETMIP);
}

AnIsotropicElecTherMechDG3DIPVariable& AnIsotropicElecTherMechDG3DIPVariable::operator=(const IPVariable &source)
{
  ElecTherMechDG3DIPVariableBase::operator=(source);
  const AnIsotropicElecTherMechDG3DIPVariable* src = dynamic_cast<const AnIsotropicElecTherMechDG3DIPVariable*>(&source);
  if(src != NULL)
  {
    if (src->_ETMIP != NULL){
      if (_ETMIP != NULL){
        _ETMIP->operator=(*(dynamic_cast<const IPVariable*>(src->_ETMIP)));
      }
      else{
        _ETMIP = dynamic_cast<IPAnIsotropicElecTherMech*>(src->_ETMIP->clone());
      }
    }
    else{
      if (_ETMIP!=NULL){
        delete _ETMIP; _ETMIP = NULL;
      }
    }
  }
  return *this;
}
double AnIsotropicElecTherMechDG3DIPVariable::get(const int i) const
{
  return ElecTherMechDG3DIPVariableBase::get(i);
}

double AnIsotropicElecTherMechDG3DIPVariable::defoEnergy() const
{
  return getIPAnIsotropicElecTherMech()->defoEnergy();
}
double AnIsotropicElecTherMechDG3DIPVariable::plasticEnergy() const
{
  return getIPAnIsotropicElecTherMech()->plasticEnergy();
}

void AnIsotropicElecTherMechDG3DIPVariable::restart()
{
  ElecTherMechDG3DIPVariableBase::restart();
  restartManager::restart(_ETMIP);
  return;
}


ElecSMPDG3DIPVariable::ElecSMPDG3DIPVariable(double tinitial,double vinitial,const mlawElecSMP &_lawESMP, const bool createBodyForceHO, const bool oninter) :
                                  ElecTherMechDG3DIPVariableBase(createBodyForceHO, oninter)
{
  _ESMPIP = new IPElecSMP(_lawESMP.getSa0(),_lawESMP.getPhia0(),_lawESMP.getSastar0());
  this->getRefToTemperature()=tinitial;
  this->getRefToVoltage()=vinitial;
  this->getRefTofT()=1./tinitial;
  this->getRefTofV()=-vinitial/tinitial;

}

ElecSMPDG3DIPVariable::ElecSMPDG3DIPVariable(const ElecSMPDG3DIPVariable &source) :
                                                        ElecTherMechDG3DIPVariableBase(source)
{
  _ESMPIP = NULL;
  if(source._ESMPIP != NULL){
    _ESMPIP = new IPElecSMP(*source._ESMPIP);
  }
}

ElecSMPDG3DIPVariable& ElecSMPDG3DIPVariable::operator=(const IPVariable &source)
{
  ElecTherMechDG3DIPVariableBase::operator=(source);
  const ElecSMPDG3DIPVariable* src = dynamic_cast<const ElecSMPDG3DIPVariable*>(&source);
  if(src != NULL)
  {

    if (src->_ESMPIP != NULL){
      if (_ESMPIP != NULL){
        _ESMPIP->operator=(*(dynamic_cast<const IPVariable*>(src->_ESMPIP)));
      }
      else{
        _ESMPIP = dynamic_cast<IPElecSMP*>(src->_ESMPIP->clone());
      }
    }
    else{
      if (_ESMPIP != NULL){
        delete _ESMPIP;
        _ESMPIP = NULL;
      }
    }

  }
  return *this;
}
double ElecSMPDG3DIPVariable::get(const int i) const
{
  if(i == IPField::PLASTICSTRAIN)
  {
   // return getInternalEnergyExtraDofDiffusion();
    return getRefToEquivalentPlasticDefo1();
  }
  return ElecTherMechDG3DIPVariableBase::get(i);
}

double ElecSMPDG3DIPVariable::defoEnergy() const
{
  return getIPElecSMP()->defoEnergy();
}
double ElecSMPDG3DIPVariable::plasticEnergy() const
{
  return getIPElecSMP()->plasticEnergy();
}

void ElecSMPDG3DIPVariable::restart()
{
  ElecTherMechDG3DIPVariableBase::restart();
  restartManager::restart(_ESMPIP);
  return;
}


GenericThermoMechanicsDG3DIPVariable::GenericThermoMechanicsDG3DIPVariable(IPGenericTM *ipc,
                                                                           const double tinitial,
                                                                           const bool createBodyForceHO, const bool oninter):
ThermoMechanicsDG3DIPVariableBase(createBodyForceHO, oninter)
{
    _genericTMIP = ipc;
    this->getRefToTemperature() = tinitial;
}

GenericThermoMechanicsDG3DIPVariable::GenericThermoMechanicsDG3DIPVariable(const GenericThermoMechanicsDG3DIPVariable &source) :
        ThermoMechanicsDG3DIPVariableBase(source)
{
    _genericTMIP = NULL;
    if (source._genericTMIP != NULL){
        _genericTMIP = new IPGenericTM(*(source.getIPGenericThermoMechanics()));
    }
}

GenericThermoMechanicsDG3DIPVariable& GenericThermoMechanicsDG3DIPVariable::operator=(const IPVariable &source)
{
    ThermoMechanicsDG3DIPVariableBase::operator=(source);
    const GenericThermoMechanicsDG3DIPVariable* src = dynamic_cast<const GenericThermoMechanicsDG3DIPVariable*>(&source);
    if(src != NULL)
    {
        _genericTMIP->operator=((dynamic_cast<const IPVariable&>(*src->_genericTMIP)));
    }
    return *this;
}

double GenericThermoMechanicsDG3DIPVariable::get(const int i) const
{
    double val = _genericTMIP->get(i);
    if (val == 0.)
        val = ThermoMechanicsDG3DIPVariableBase::get(i);
    return val;
}

double GenericThermoMechanicsDG3DIPVariable::defoEnergy() const
{
    return getConstRefToIpMeca().defoEnergy();
}

double GenericThermoMechanicsDG3DIPVariable::damageEnergy() const
{
    return getConstRefToIpMeca().damageEnergy();
}

double  GenericThermoMechanicsDG3DIPVariable::plasticEnergy() const
{
    return getConstRefToIpMeca().plasticEnergy();
}

void GenericThermoMechanicsDG3DIPVariable::restart()
{
    ThermoMechanicsDG3DIPVariableBase::restart();
    _genericTMIP->restart();
    return;
}

const IPVariableMechanics& GenericThermoMechanicsDG3DIPVariable::getConstRefToIpMeca() const
{
    return _genericTMIP->getConstRefToIpMeca();
}
IPVariableMechanics& GenericThermoMechanicsDG3DIPVariable::getRefToIpMeca()
{
    return _genericTMIP->getRefToIpMeca();
}
void GenericThermoMechanicsDG3DIPVariable::setIpMeca(IPVariable& ipMeca)
{
    _genericTMIP->setIpMeca(ipMeca);
}
const STensor3 & GenericThermoMechanicsDG3DIPVariable::getConstRefToReferenceF() const
{
    return _genericTMIP->getConstRefToReferenceF();
}
STensor3 & GenericThermoMechanicsDG3DIPVariable::getRefToReferenceF()
{
    return _genericTMIP->getRefToReferenceF();
}

GenericResidualMechanicsDG3DIPVariable::GenericResidualMechanicsDG3DIPVariable(IPGenericRM *ipc, const bool createBodyForceHO, const bool oninter): dG3DIPVariable(createBodyForceHO, oninter)
{
    _genericRMIP = ipc;
    //this->getRefToTemperature() = tinitial;
    
}

GenericResidualMechanicsDG3DIPVariable::GenericResidualMechanicsDG3DIPVariable(const GenericResidualMechanicsDG3DIPVariable &source) :
        dG3DIPVariable(source)
{
    _genericRMIP = NULL;
    if (source._genericRMIP != NULL){
        _genericRMIP = new IPGenericRM(*(source.getIPGenericResidualMechanics()));
    }
}

GenericResidualMechanicsDG3DIPVariable& GenericResidualMechanicsDG3DIPVariable::operator=(const IPVariable &source)
{
    dG3DIPVariable::operator=(source);
    const GenericResidualMechanicsDG3DIPVariable* src = dynamic_cast<const GenericResidualMechanicsDG3DIPVariable*>(&source);
    if(src != NULL)
    {
        _genericRMIP->operator=((dynamic_cast<const IPVariable&>(*src->_genericRMIP)));
    }
    return *this;
}

double GenericResidualMechanicsDG3DIPVariable::get(const int i) const
{
    double val = _genericRMIP->get(i);
    if (val == 0.)
        val = dG3DIPVariable::get(i);
    return val;
}

double GenericResidualMechanicsDG3DIPVariable::defoEnergy() const
{
    return getConstRefToIpMeca().defoEnergy();
}

double GenericResidualMechanicsDG3DIPVariable::damageEnergy() const
{
    return getConstRefToIpMeca().damageEnergy();
}

double  GenericResidualMechanicsDG3DIPVariable::plasticEnergy() const
{
    return getConstRefToIpMeca().plasticEnergy();
}

void GenericResidualMechanicsDG3DIPVariable::restart()
{
    dG3DIPVariable::restart();
    _genericRMIP->restart();
    return;
}

const IPVariableMechanics& GenericResidualMechanicsDG3DIPVariable::getConstRefToIpMeca() const
{
    return _genericRMIP->getConstRefToIpMeca();
}
IPVariableMechanics& GenericResidualMechanicsDG3DIPVariable::getRefToIpMeca()
{
    return _genericRMIP->getRefToIpMeca();
}
void GenericResidualMechanicsDG3DIPVariable::setIpMeca(IPVariable& ipMeca)
{
    _genericRMIP->setIpMeca(ipMeca);
}
/*const STensor3 & GenericResidualMechanicsDG3DIPVariable::getConstRefToReferenceF() const
{
    return _genericRMIP->getConstRefToReferenceF();
}
STensor3 & GenericResidualMechanicsDG3DIPVariable::getRefToReferenceF()
{
    return _genericRMIP->getRefToReferenceF();
}*/


ElecMagGenericThermoMechanicsDG3DIPVariable::ElecMagGenericThermoMechanicsDG3DIPVariable(IPElecMagGenericThermoMech *ipc,
                                                                                         const double tinitial,
                                                                                         const double vinitial,
                                                                                         const SVector3 & magvecpotinitial,
                                                                                         const bool createBodyForceHO,
                                                                                         const bool oninter):
                                                                                         ElecMagTherMechDG3DIPVariableBase(createBodyForceHO, oninter)
{
    _elecMagGenericThermoMechIP = ipc;
    this->getRefToTemperature() = tinitial;
    this->getRefToVoltage() = vinitial;
    this->getRefToMagneticVectorPotential() = magvecpotinitial;
}

ElecMagGenericThermoMechanicsDG3DIPVariable::ElecMagGenericThermoMechanicsDG3DIPVariable(const ElecMagGenericThermoMechanicsDG3DIPVariable &source) :
ElecMagTherMechDG3DIPVariableBase(source)
{
    _elecMagGenericThermoMechIP = NULL;
    if (source._elecMagGenericThermoMechIP != NULL){
        _elecMagGenericThermoMechIP = new IPElecMagGenericThermoMech(*(source.getIPElecMagGenericThermoMechanics()));
    }
}

ElecMagGenericThermoMechanicsDG3DIPVariable& ElecMagGenericThermoMechanicsDG3DIPVariable::operator=(const IPVariable &source)
{
    ElecMagTherMechDG3DIPVariableBase::operator=(source);
    const ElecMagGenericThermoMechanicsDG3DIPVariable* src = dynamic_cast<const ElecMagGenericThermoMechanicsDG3DIPVariable*>(&source);
    if(src != NULL)
    {
        _elecMagGenericThermoMechIP->operator=((dynamic_cast<const IPVariable&>(*src->_elecMagGenericThermoMechIP)));
    }
    return *this;
}

double ElecMagGenericThermoMechanicsDG3DIPVariable::get(const int i) const
{
    double val = _elecMagGenericThermoMechIP->get(i);
    if (val == 0.)
        val = ElecMagTherMechDG3DIPVariableBase::get(i);
    return val;
}

double & ElecMagGenericThermoMechanicsDG3DIPVariable::getRef(const int i)
{
    return ElecMagTherMechDG3DIPVariableBase::getRef(i);
}

double ElecMagGenericThermoMechanicsDG3DIPVariable::defoEnergy() const
{
    return getConstRefToIpThermoMech().defoEnergy();
}

double ElecMagGenericThermoMechanicsDG3DIPVariable::damageEnergy() const
{
    return getConstRefToIpThermoMech().damageEnergy();
}

double  ElecMagGenericThermoMechanicsDG3DIPVariable::plasticEnergy() const
{
    return getConstRefToIpThermoMech().plasticEnergy();
}

void ElecMagGenericThermoMechanicsDG3DIPVariable::restart()
{
    ElecMagTherMechDG3DIPVariableBase::restart();
    _elecMagGenericThermoMechIP->restart();
    return;
}

const IPCoupledThermoMechanics& ElecMagGenericThermoMechanicsDG3DIPVariable::getConstRefToIpThermoMech() const
{
    return _elecMagGenericThermoMechIP->getConstRefToIpThermoMech();
}
IPCoupledThermoMechanics& ElecMagGenericThermoMechanicsDG3DIPVariable::getRefToIpThermoMech()
{
    return _elecMagGenericThermoMechIP->getRefToIpThermoMech();
}
void ElecMagGenericThermoMechanicsDG3DIPVariable::setIpThermoMech(IPVariable& ipThermoMech)
{
    _elecMagGenericThermoMechIP->setIpThermoMech(ipThermoMech);
}

ElecMagInductorDG3DIPVariable::ElecMagInductorDG3DIPVariable(IPElecMagInductor *ipc,
                                                             const double tinitial,
                                                             const double vinitial,
                                                             const SVector3 & magvecpotinitial,
                                                             const bool createBodyForceHO,
                                                             const bool oninter):
                                                             ElecMagTherMechDG3DIPVariableBase(createBodyForceHO, oninter)
{
    _EMInductorIP = ipc;
    this->getRefToTemperature() = tinitial;
    this->getRefToVoltage() = vinitial;
    this->getRefToMagneticVectorPotential() = magvecpotinitial;
}

ElecMagInductorDG3DIPVariable::ElecMagInductorDG3DIPVariable(const ElecMagInductorDG3DIPVariable &source):
ElecMagTherMechDG3DIPVariableBase(source)
{
    _EMInductorIP = NULL;
    if (source._EMInductorIP != NULL){
        _EMInductorIP = new IPElecMagInductor(*(source.getIPElecMagInductor()));
    }
}

ElecMagInductorDG3DIPVariable & ElecMagInductorDG3DIPVariable::operator=(const IPVariable &source)
{
    ElecMagTherMechDG3DIPVariableBase::operator=(source);
    const ElecMagInductorDG3DIPVariable* src = dynamic_cast<const ElecMagInductorDG3DIPVariable*>(&source);
    if(src != NULL)
    {
        const IPVariable* tmp =dynamic_cast<const IPVariable*>( src->_EMInductorIP);
        _EMInductorIP->operator=(*tmp);
    }
    return *this;
}

double ElecMagInductorDG3DIPVariable::get(const int i) const
{
    return ElecMagTherMechDG3DIPVariableBase::get(i);
}

double & ElecMagInductorDG3DIPVariable::getRef(const int i)
{
    return ElecMagTherMechDG3DIPVariableBase::getRef(i);
}

void ElecMagInductorDG3DIPVariable::restart()
{
    ElecMagTherMechDG3DIPVariableBase::restart();
    _EMInductorIP->restart();
    return;
}

const IPCoupledThermoMechanics& ElecMagInductorDG3DIPVariable::getConstRefToIpThermoMech() const
{
    return _EMInductorIP->getConstRefToIpThermoMech();
}

IPCoupledThermoMechanics& ElecMagInductorDG3DIPVariable::getRefToIpThermoMech()
{
    return _EMInductorIP->getRefToIpThermoMech();
}

void ElecMagInductorDG3DIPVariable::setIpThermoMech(IPVariable& ipThermoMech)
{
    return _EMInductorIP->setIpThermoMech(ipThermoMech);
}

double ElecMagInductorDG3DIPVariable::defoEnergy() const
{
    return getConstRefToIpThermoMech().defoEnergy();
}

double ElecMagInductorDG3DIPVariable::plasticEnergy() const
{
    return getConstRefToIpThermoMech().plasticEnergy();
}

double ElecMagInductorDG3DIPVariable::damageEnergy() const
{
    return getConstRefToIpThermoMech().damageEnergy();
}


// NonLinearTVM Law Interface =================================================================== BEGIN

// Take Care here, nonlocal variables cannot be directly defined with the ThermoMechanicsDG3DIPVariable Base Class (This is a pure virtual base class).

NonLinearTVMDG3DIPVariable::NonLinearTVMDG3DIPVariable(const mlawNonLinearTVM& viscoLaw, double Tinitial, const bool createBodyForceHO, const bool oninter) :
              ThermoMechanicsDG3DIPVariableBase(createBodyForceHO, oninter){
  _ipViscoElastic = new IPNonLinearTVM(viscoLaw.getConstRefToCompressionHardening(),
                                       viscoLaw.getConstRefToTractionHardening(),
                                       viscoLaw.getConstRefToKinematicHardening(),
                                       viscoLaw.getViscoElasticNumberOfElement(),
                                       viscoLaw.getConstRefToMullinsEffect());
  this->getRefToTemperature()=Tinitial;
};

NonLinearTVMDG3DIPVariable::NonLinearTVMDG3DIPVariable(const NonLinearTVMDG3DIPVariable& src) :
              ThermoMechanicsDG3DIPVariableBase(src), _ipViscoElastic(src._ipViscoElastic){
  _ipViscoElastic = NULL;
  if (src._ipViscoElastic != NULL){
    _ipViscoElastic = dynamic_cast<IPNonLinearTVM*>(src._ipViscoElastic->clone());
  }
};


NonLinearTVMDG3DIPVariable& NonLinearTVMDG3DIPVariable::operator = (const IPVariable& src){
  ThermoMechanicsDG3DIPVariableBase::operator =(src);
  const NonLinearTVMDG3DIPVariable* psrc = dynamic_cast<const NonLinearTVMDG3DIPVariable*>(&src);
  if (psrc != NULL){
    if (psrc->_ipViscoElastic != NULL){
      if (_ipViscoElastic!= NULL){
        _ipViscoElastic->operator=(*dynamic_cast<const IPVariable*>(psrc->_ipViscoElastic));
      }
      else{
        _ipViscoElastic = dynamic_cast<IPNonLinearTVM*>(psrc->_ipViscoElastic->clone());
      }
    }
  }
  return *this;
};

NonLinearTVMDG3DIPVariable::~NonLinearTVMDG3DIPVariable(){
  if (_ipViscoElastic){delete _ipViscoElastic; _ipViscoElastic=NULL;};
};

// 5) Member Function - get(i) - Changed - Refer to HyperVisco...DG3DIP for the Original
double NonLinearTVMDG3DIPVariable::get(const int i) const{

  if      (i == IPField::Ee_XX){return _ipViscoElastic->_Ee(0,0);}
  else if (i == IPField::Ee_XY){return _ipViscoElastic->_Ee(0,1);}
  else if (i == IPField::Ee_XZ){return _ipViscoElastic->_Ee(0,2);}
  else if (i == IPField::Ee_YY){return _ipViscoElastic->_Ee(1,1);}
  else if (i == IPField::Ee_YZ){return _ipViscoElastic->_Ee(1,2);}
  else if (i == IPField::Ee_ZZ){return _ipViscoElastic->_Ee(2,2);}
  else if (i == IPField::corKir_XX){return _ipViscoElastic->_kirchhoff(0,0);}
  else if (i == IPField::corKir_XY){return _ipViscoElastic->_kirchhoff(0,1);}
  else if (i == IPField::corKir_XZ){return _ipViscoElastic->_kirchhoff(0,2);}
  else if (i == IPField::corKir_YY){return _ipViscoElastic->_kirchhoff(1,1);}
  else if (i == IPField::corKir_YZ){return _ipViscoElastic->_kirchhoff(1,2);}
  else if (i == IPField::corKir_ZZ){return _ipViscoElastic->_kirchhoff(2,2);}
  else if (i == IPField::mandel_XX){return _ipViscoElastic->_ModMandel(0,0);}
  else if (i == IPField::mandel_XY){return _ipViscoElastic->_ModMandel(0,1);}
  else if (i == IPField::mandel_YX){return _ipViscoElastic->_ModMandel(1,0);}
  else if (i == IPField::mandel_XZ){return _ipViscoElastic->_ModMandel(0,2);}
  else if (i == IPField::mandel_ZX){return _ipViscoElastic->_ModMandel(2,0);}
  else if (i == IPField::mandel_YY){return _ipViscoElastic->_ModMandel(1,1);}
  else if (i == IPField::mandel_YZ){return _ipViscoElastic->_ModMandel(1,2);}
  else if (i == IPField::mandel_ZY){return _ipViscoElastic->_ModMandel(2,1);}
  else if (i == IPField::mandel_ZZ){return _ipViscoElastic->_ModMandel(2,2);}
  else if (i == IPField::mandelCommuteChecker){return _ipViscoElastic->_mandelCommuteChecker;}
  else if (i == IPField::hyperElastic_BulkScalar){return _ipViscoElastic->getConstRefToElasticBulkPropertyScaleFactor();}
  else if (i == IPField::hyperElastic_ShearScalar){return _ipViscoElastic->getConstRefToElasticShearPropertyScaleFactor();}
  else if (i == IPField::corKirExtra_XX){return _ipViscoElastic->_corKirExtra(0,0);}
  else if (i == IPField::corKirExtra_XY){return _ipViscoElastic->_corKirExtra(0,1);}
  else if (i == IPField::corKirExtra_XZ){return _ipViscoElastic->_corKirExtra(0,2);}
  else if (i == IPField::corKirExtra_YY){return _ipViscoElastic->_corKirExtra(1,1);}
  else if (i == IPField::corKirExtra_YZ){return _ipViscoElastic->_corKirExtra(1,2);}
  else if (i == IPField::corKirExtra_ZZ){return _ipViscoElastic->_corKirExtra(2,2);}
  else if (i == IPField::Tr_corKir_Inf){return _ipViscoElastic->_trCorKirinf_TVE;}
  else if (i == IPField::Dev_corKir_Inf_XX){return _ipViscoElastic->_devCorKirinf_TVE(0,0);}
  else if (i == IPField::Dev_corKir_Inf_XY){return _ipViscoElastic->_devCorKirinf_TVE(0,1);}
  else if (i == IPField::Dev_corKir_Inf_XZ){return _ipViscoElastic->_devCorKirinf_TVE(0,2);}
  else if (i == IPField::Dev_corKir_Inf_YY){return _ipViscoElastic->_devCorKirinf_TVE(1,1);}
  else if (i == IPField::Dev_corKir_Inf_YZ){return _ipViscoElastic->_devCorKirinf_TVE(1,2);}
  else if (i == IPField::Dev_corKir_Inf_ZZ){return _ipViscoElastic->_devCorKirinf_TVE(2,2);}
  else if (i == IPField::PRESSURE){return _ipViscoElastic->_pressure;}
  else if (i == IPField::viscousDissipatedEnergy){return _ipViscoElastic->_viscousDissipatedEnergy;}
  else if (i == IPField::mullinsDissipatedEnergy){return _ipViscoElastic->_mullinsDissipatedEnergy;}
  else if (i == IPField::Eve1_XX){
    if (_ipViscoElastic->_A.size() > 0)
      return _ipViscoElastic->_A[0](0,0) + _ipViscoElastic->_B[0]/3.;
    else
      return 0.;
    }
  else if (i == IPField::Eve1_XY){
    if (_ipViscoElastic->_A.size() > 0)
      return _ipViscoElastic->_A[0](0,1);
    else return 0.;
  }
  else if (i == IPField::Eve1_XZ){
    if (_ipViscoElastic->_A.size() > 0)
      return _ipViscoElastic->_A[0](0,2);
    return 0.;
  }
  else if (i == IPField::Eve1_YY){
    if (_ipViscoElastic->_A.size() > 0)
      return _ipViscoElastic->_A[0](1,1) + _ipViscoElastic->_B[0]/3.;
    else return 0.;
  }
  else if (i == IPField::Eve1_YZ){
    if (_ipViscoElastic->_A.size() > 0)
      return _ipViscoElastic->_A[0](1,2);
    else return 0.;
  }
  else if (i == IPField::Eve1_ZZ){
    if (_ipViscoElastic->_A.size() > 0)
      return _ipViscoElastic->_A[0](2,2) + _ipViscoElastic->_B[0]/3.;
    else return 0.;
  }
  else if (i == IPField::Eve2_XX){                                              // Eve2 - WHAT is it?
    if (_ipViscoElastic->_A.size() > 1)
      return _ipViscoElastic->_A[1](0,0) + _ipViscoElastic->_B[1]/3.;
    else return 0.;
  }
  else if (i == IPField::Eve2_XY){
    if (_ipViscoElastic->_A.size() > 1)
      return _ipViscoElastic->_A[1](0,1);
    else return 0.;
  }
  else if (i == IPField::Eve2_XZ){
    if (_ipViscoElastic->_A.size() > 1)
      return _ipViscoElastic->_A[1](0,2);
    else return 0.;
  }
  else if (i == IPField::Eve2_YY){
    if (_ipViscoElastic->_A.size() > 1)
      return _ipViscoElastic->_A[1](1,1) + _ipViscoElastic->_B[1]/3.;
    else return 0.;
  }
  else if (i == IPField::Eve2_YZ){
    if (_ipViscoElastic->_A.size() > 1)
      return _ipViscoElastic->_A[1](1,2);
    else return 0.;
  }
  else if (i == IPField::Eve2_ZZ){
    if (_ipViscoElastic->_A.size() > 1)
      return _ipViscoElastic->_A[1](2,2) + _ipViscoElastic->_B[1]/3.;
    else return 0.;
  }

  // Additional IPFields from ThermoMechanicsDG3DIPVariableBase - Adopted here directly
  else if (i == IPField::TEMPERATURE){return getConstRefToField(0);}
  else if (i == IPField::THERMALFLUX_X){return getConstRefToFlux()[0](0);}
  else if (i == IPField::THERMALFLUX_Y){return getConstRefToFlux()[0](1);}
  else if (i == IPField::THERMALFLUX_Z){return getConstRefToFlux()[0](2);}
  else if (i == IPField::FIELD_SOURCE){return getConstRefToFieldSource()(0);}
  else if (i == IPField::MECHANICAL_SOURCE){return getConstRefToMechanicalSource()(0);}
  else if (i == IPField::DEFO_ENERGY)
  {
    return _ipViscoElastic->_elasticEnergy;
  }
  else if (i == IPField::MAX_DEFO_ENERGY)
  {
    return _ipViscoElastic->_psiMax;
  }
  else if (i == IPField::MULLINS_DAMAGE)
  {
    return _ipViscoElastic->_mullinsDamage;
  }

  else
  {
    return dG3DIPVariable::get(i);
  }
};

// 5) Member Function - defoEnergy()  - Unchanged
double  NonLinearTVMDG3DIPVariable::defoEnergy() const{
  return _ipViscoElastic->defoEnergy();
}

// 6) Member Function - plasticEnergy() - Added from LinearTherMech -------------- WHY and used HOW??
double NonLinearTVMDG3DIPVariable::plasticEnergy() const{
  return getIPNonLinearTVM()->plasticEnergy();
}

// 7) Other Member Functions - damageEnergy(), etc, may have to be added later.

// 8) Member Function - restart() - Changed only 2nd line
void NonLinearTVMDG3DIPVariable::restart(){
// dG3DIPVariable::restart();
  ThermoMechanicsDG3DIPVariableBase::restart();
  restartManager::restart(_ipViscoElastic);
  return;
}

// NonLinearTVM Law Interface =================================================================== END

// NonLinearTVP Law Interface =================================================================== BEGIN

NonLinearTVPDG3DIPVariable::NonLinearTVPDG3DIPVariable(const mlawNonLinearTVP& viscoEPLaw, double Tinitial, const bool createBodyForceHO, const bool oninter) :
              ThermoMechanicsDG3DIPVariableBase(createBodyForceHO, oninter){
  _ipViscoElastoPlastic = new IPNonLinearTVP(viscoEPLaw.getConstRefToCompressionHardening(),
                                              viscoEPLaw.getConstRefToTractionHardening(),
                                              viscoEPLaw.getConstRefToKinematicHardening(),
                                              viscoEPLaw.getViscoElasticNumberOfElement(),
                                              viscoEPLaw.getConstRefToMullinsEffect());
  this->getRefToTemperature()=Tinitial;
};

NonLinearTVPDG3DIPVariable::NonLinearTVPDG3DIPVariable(const NonLinearTVPDG3DIPVariable& src) :
              ThermoMechanicsDG3DIPVariableBase(src), _ipViscoElastoPlastic(src._ipViscoElastoPlastic){
  _ipViscoElastoPlastic = NULL;
  if (src._ipViscoElastoPlastic != NULL){
    _ipViscoElastoPlastic = dynamic_cast<IPNonLinearTVP*>(src._ipViscoElastoPlastic->clone());
  }
};

NonLinearTVPDG3DIPVariable& NonLinearTVPDG3DIPVariable::operator = (const IPVariable& src){
  ThermoMechanicsDG3DIPVariableBase::operator =(src);
  const NonLinearTVPDG3DIPVariable* psrc = dynamic_cast<const NonLinearTVPDG3DIPVariable*>(&src);
  if (psrc != NULL){
    if (psrc->_ipViscoElastoPlastic != NULL){
      if (_ipViscoElastoPlastic!= NULL){
        _ipViscoElastoPlastic->operator=(*dynamic_cast<const IPVariable*>(psrc->_ipViscoElastoPlastic));
      }
      else{
        _ipViscoElastoPlastic = dynamic_cast<IPNonLinearTVP*>(psrc->_ipViscoElastoPlastic->clone());
      }
    }
  }
  return *this;
};

NonLinearTVPDG3DIPVariable::~NonLinearTVPDG3DIPVariable(){
  if (_ipViscoElastoPlastic){delete _ipViscoElastoPlastic; _ipViscoElastoPlastic = NULL;};
};

double NonLinearTVPDG3DIPVariable::get(const int i) const{

  if      (i == IPField::Ee_XX){return _ipViscoElastoPlastic->_Ee(0,0);}
  else if (i == IPField::Ee_XY){return _ipViscoElastoPlastic->_Ee(0,1);}
  else if (i == IPField::Ee_XZ){return _ipViscoElastoPlastic->_Ee(0,2);}
  else if (i == IPField::Ee_YY){return _ipViscoElastoPlastic->_Ee(1,1);}
  else if (i == IPField::Ee_YZ){return _ipViscoElastoPlastic->_Ee(1,2);}
  else if (i == IPField::Ee_ZZ){return _ipViscoElastoPlastic->_Ee(2,2);}
  else if (i == IPField::detEe){return _ipViscoElastoPlastic->_detEe;}
  else if (i == IPField::corKir_XX){return _ipViscoElastoPlastic->_kirchhoff(0,0);}
  else if (i == IPField::corKir_XY){return _ipViscoElastoPlastic->_kirchhoff(0,1);}
  else if (i == IPField::corKir_XZ){return _ipViscoElastoPlastic->_kirchhoff(0,2);}
  else if (i == IPField::corKir_YY){return _ipViscoElastoPlastic->_kirchhoff(1,1);}
  else if (i == IPField::corKir_YZ){return _ipViscoElastoPlastic->_kirchhoff(1,2);}
  else if (i == IPField::corKir_ZZ){return _ipViscoElastoPlastic->_kirchhoff(2,2);}
  else if (i == IPField::mandel_XX){return _ipViscoElastoPlastic->_ModMandel(0,0);}
  else if (i == IPField::mandel_XY){return _ipViscoElastoPlastic->_ModMandel(0,1);}
  else if (i == IPField::mandel_YX){return _ipViscoElastoPlastic->_ModMandel(1,0);}
  else if (i == IPField::mandel_XZ){return _ipViscoElastoPlastic->_ModMandel(0,2);}
  else if (i == IPField::mandel_ZX){return _ipViscoElastoPlastic->_ModMandel(2,0);}
  else if (i == IPField::mandel_YY){return _ipViscoElastoPlastic->_ModMandel(1,1);}
  else if (i == IPField::mandel_YZ){return _ipViscoElastoPlastic->_ModMandel(1,2);}
  else if (i == IPField::mandel_ZY){return _ipViscoElastoPlastic->_ModMandel(2,1);}
  else if (i == IPField::mandel_ZZ){return _ipViscoElastoPlastic->_ModMandel(2,2);}
  else if (i == IPField::mandelCommuteChecker){return _ipViscoElastoPlastic->_mandelCommuteChecker;}
  else if (i == IPField::hyperElastic_BulkScalar){return _ipViscoElastoPlastic->getConstRefToElasticBulkPropertyScaleFactor();}
  else if (i == IPField::hyperElastic_ShearScalar){return _ipViscoElastoPlastic->getConstRefToElasticShearPropertyScaleFactor();}
  else if (i == IPField::corKirExtra_XX){return _ipViscoElastoPlastic->_corKirExtra(0,0);}
  else if (i == IPField::corKirExtra_XY){return _ipViscoElastoPlastic->_corKirExtra(0,1);}
  else if (i == IPField::corKirExtra_XZ){return _ipViscoElastoPlastic->_corKirExtra(0,2);}
  else if (i == IPField::corKirExtra_YY){return _ipViscoElastoPlastic->_corKirExtra(1,1);}
  else if (i == IPField::corKirExtra_YZ){return _ipViscoElastoPlastic->_corKirExtra(1,2);}
  else if (i == IPField::corKirExtra_ZZ){return _ipViscoElastoPlastic->_corKirExtra(2,2);}
  else if (i == IPField::Tr_corKir_Inf){return _ipViscoElastoPlastic->_trCorKirinf_TVE;}
  else if (i == IPField::Dev_corKir_Inf_XX){return _ipViscoElastoPlastic->_devCorKirinf_TVE(0,0);}
  else if (i == IPField::Dev_corKir_Inf_XY){return _ipViscoElastoPlastic->_devCorKirinf_TVE(0,1);}
  else if (i == IPField::Dev_corKir_Inf_XZ){return _ipViscoElastoPlastic->_devCorKirinf_TVE(0,2);}
  else if (i == IPField::Dev_corKir_Inf_YY){return _ipViscoElastoPlastic->_devCorKirinf_TVE(1,1);}
  else if (i == IPField::Dev_corKir_Inf_YZ){return _ipViscoElastoPlastic->_devCorKirinf_TVE(1,2);}
  else if (i == IPField::Dev_corKir_Inf_ZZ){return _ipViscoElastoPlastic->_devCorKirinf_TVE(2,2);}
  else if (i == IPField::viscousDissipatedEnergy){return _ipViscoElastoPlastic->_viscousDissipatedEnergy;}
  else if (i == IPField::mullinsDissipatedEnergy){return _ipViscoElastoPlastic->_mullinsDissipatedEnergy;}
  else if (i == IPField::Eve1_XX){
    if (_ipViscoElastoPlastic->_A.size() > 0)
      return _ipViscoElastoPlastic->_A[0](0,0) + _ipViscoElastoPlastic->_B[0]/3.;
    else
      return 0.;
    }
  else if (i == IPField::Eve1_XY){
    if (_ipViscoElastoPlastic->_A.size() > 0)
      return _ipViscoElastoPlastic->_A[0](0,1);
    else return 0.;
  }
  else if (i == IPField::Eve1_XZ){
    if (_ipViscoElastoPlastic->_A.size() > 0)
      return _ipViscoElastoPlastic->_A[0](0,2);
    return 0.;
  }
  else if (i == IPField::Eve1_YY){
    if (_ipViscoElastoPlastic->_A.size() > 0)
      return _ipViscoElastoPlastic->_A[0](1,1) + _ipViscoElastoPlastic->_B[0]/3.;
    else return 0.;
  }
  else if (i == IPField::Eve1_YZ){
    if (_ipViscoElastoPlastic->_A.size() > 0)
      return _ipViscoElastoPlastic->_A[0](1,2);
    else return 0.;
  }
  else if (i == IPField::Eve1_ZZ){
    if (_ipViscoElastoPlastic->_A.size() > 0)
      return _ipViscoElastoPlastic->_A[0](2,2) + _ipViscoElastoPlastic->_B[0]/3.;
    else return 0.;
  }
  else if (i == IPField::Eve2_XX){                                              // Eve2 - WHAT is it?
    if (_ipViscoElastoPlastic->_A.size() > 1)
      return _ipViscoElastoPlastic->_A[1](0,0) + _ipViscoElastoPlastic->_B[1]/3.;
    else return 0.;
  }
  else if (i == IPField::Eve2_XY){
    if (_ipViscoElastoPlastic->_A.size() > 1)
      return _ipViscoElastoPlastic->_A[1](0,1);
    else return 0.;
  }
  else if (i == IPField::Eve2_XZ){
    if (_ipViscoElastoPlastic->_A.size() > 1)
      return _ipViscoElastoPlastic->_A[1](0,2);
    else return 0.;
  }
  else if (i == IPField::Eve2_YY){
    if (_ipViscoElastoPlastic->_A.size() > 1)
      return _ipViscoElastoPlastic->_A[1](1,1) + _ipViscoElastoPlastic->_B[1]/3.;
    else return 0.;
  }
  else if (i == IPField::Eve2_YZ){
    if (_ipViscoElastoPlastic->_A.size() > 1)
      return _ipViscoElastoPlastic->_A[1](1,2);
    else return 0.;
  }
  else if (i == IPField::Eve2_ZZ){
    if (_ipViscoElastoPlastic->_A.size() > 1)
      return _ipViscoElastoPlastic->_A[1](2,2) + _ipViscoElastoPlastic->_B[1]/3.;
    else return 0.;
  }

  // Additional IPFields from ThermoMechanicsDG3DIPVariableBase - Adopted here directly
  else if (i == IPField::TEMPERATURE){return getConstRefToField(0);}
  else if (i == IPField::THERMALFLUX_X){return getConstRefToFlux()[0](0);}
  else if (i == IPField::THERMALFLUX_Y){return getConstRefToFlux()[0](1);}
  else if (i == IPField::THERMALFLUX_Z){return getConstRefToFlux()[0](2);}
  else if (i == IPField::FIELD_SOURCE){return getConstRefToFieldSource()(0);}
  else if (i == IPField::MECHANICAL_SOURCE){return getConstRefToMechanicalSource()(0);}

  else if(i == IPField::PLASTICSTRAIN)
  {
    return _ipViscoElastoPlastic->getConstRefToEqPlasticStrain();
  }
   else if(i == IPField::FP_XX)
  {
    return _ipViscoElastoPlastic->getConstRefToFp()(0,0);
  }
  else if(i == IPField::FP_XY)
  {
    return _ipViscoElastoPlastic->getConstRefToFp()(0,1);
  }
  else if(i == IPField::FP_XZ)
  {
    return _ipViscoElastoPlastic->getConstRefToFp()(0,2);
  }
  else if(i == IPField::FP_YX)
  {
    return _ipViscoElastoPlastic->getConstRefToFp()(1,0);
  }
  else if(i == IPField::FP_YY)
  {
    return _ipViscoElastoPlastic->getConstRefToFp()(1,1);
  }
  else if(i == IPField::FP_YZ)
  {
    return _ipViscoElastoPlastic->getConstRefToFp()(1,2);
  }
  else if(i == IPField::FP_ZX)
  {
    return _ipViscoElastoPlastic->getConstRefToFp()(2,0);
  }
  else if(i == IPField::FP_ZY)
  {
    return _ipViscoElastoPlastic->getConstRefToFp()(2,1);
  }
  else if(i == IPField::FP_ZZ)
  {
    return _ipViscoElastoPlastic->getConstRefToFp()(2,2);
  }
  else if (i == IPField::PLASTIC_POISSON_RATIO){
    return _ipViscoElastoPlastic->getConstRefToPlasticPoissonRatio();
  }
  else if (i == IPField::ISO_YIELD_COMPRESSION)
  {
    return _ipViscoElastoPlastic->getConstRefToIPCompressionHardening().getR();
  }
  else if (i == IPField::ISO_YIELD_TENSILE)
  {
    return _ipViscoElastoPlastic->getConstRefToIPTractionHardening().getR();
  }
  else if (i == IPField::KIN_YIELD)
  {
    return _ipViscoElastoPlastic->getConstRefToKinematicHardening().getR();
  }
  else if (i == IPField::DEFO_ENERGY)
  {
    return _ipViscoElastoPlastic->_elasticEnergy;
  }
  else if (i == IPField::MAX_DEFO_ENERGY)
  {
    return _ipViscoElastoPlastic->_psiMax;
  }
  else if (i == IPField::MULLINS_DAMAGE)
  {
    return _ipViscoElastoPlastic->_mullinsDamage;
  }
  else
  {
    return dG3DIPVariable::get(i);
  }
};

double  NonLinearTVPDG3DIPVariable::defoEnergy() const{
  return _ipViscoElastoPlastic->defoEnergy();
}

double NonLinearTVPDG3DIPVariable::plasticEnergy() const{
  return getIPNonLinearTVP()->plasticEnergy();
}

void NonLinearTVPDG3DIPVariable::restart(){
// dG3DIPVariable::restart();
  ThermoMechanicsDG3DIPVariableBase::restart();
  restartManager::restart(_ipViscoElastoPlastic);
  return;
}

// NonLinearTVP Law Interface =================================================================== END
