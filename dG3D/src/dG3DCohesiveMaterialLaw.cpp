//
//
// Description: Class of materials for non linear dg
//
//
// Author:  <L. Noels>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "dG3DCohesiveMaterialLaw.h"
#include "dG3DCohesiveIPVariable.h"
#include "FractureCohesiveDG3DIPVariable.h"
#include "ipField.h"
#include "nonLinearMechSolver.h"
#include "failureDetection.h"

Cohesive3DLaw::Cohesive3DLaw(const int num, const bool init) : materialLaw(num,init)
{

}
Cohesive3DLaw::Cohesive3DLaw(const Cohesive3DLaw &source) : materialLaw(source)
{

}

BaseCohesive3DLaw::BaseCohesive3DLaw(const int num, const double fractureStrengthFactorMin,
                                               const double fractureStrengthFactorMax, const double Kp) :
									Cohesive3DLaw(num,true), _cohesiveLaw(NULL),
									_fscmin(fractureStrengthFactorMin),
									_fscmax(fractureStrengthFactorMax),_Kp(Kp),_viscosity(0.)
{

}

BaseCohesive3DLaw::BaseCohesive3DLaw(const BaseCohesive3DLaw &source) : Cohesive3DLaw(source),
      _fscmin(source._fscmin), _fscmax(source._fscmax),
      _Kp(source._Kp), _viscosity(source._viscosity)
{
	_cohesiveLaw = NULL;
	if (source._cohesiveLaw != NULL)
		_cohesiveLaw = dynamic_cast<CohesiveLawBase*>(source._cohesiveLaw->clone());

}
BaseCohesive3DLaw::~BaseCohesive3DLaw(){
  if (_cohesiveLaw) {
    delete _cohesiveLaw;
    _cohesiveLaw = NULL;
  }
};

void BaseCohesive3DLaw::stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff, const bool checkfrac, const bool dTangent)
{
  FractureCohesive3DIPVariable *fipv = dynamic_cast < FractureCohesive3DIPVariable *> (ipv);
  const FractureCohesive3DIPVariable *fipvprev = dynamic_cast < const FractureCohesive3DIPVariable *> (ipvprev);
  if (fipv == NULL) Msg::Error("FractureCohesive3DIPVariable must be used in BaseCohesive3DLaw::stress");

  Cohesive3DIPVariable* cohIpv = dynamic_cast<Cohesive3DIPVariable*>(fipv->getIPvFrac());
  const Cohesive3DIPVariable* cohIpvprev = dynamic_cast<const Cohesive3DIPVariable*>(fipvprev->getIPvFrac());
  if (cohIpv == NULL) Msg::Error("Cohesive3DIPVariable must be used in BaseCohesive3DLaw::stress");

  // set kinematic data
  const SVector3& ujump = fipv->getConstRefToJump();

  // initial data for cohesive
  SVector3& jump0 = cohIpv->getRefToInitialCohesiveJump();
  SVector3& ujump0 = cohIpv->getRefToDGJump();
  // initial value take from previous cohesive IP
  jump0 = cohIpvprev->getConstRefToInitialCohesiveJump();
  ujump0 = cohIpvprev->getConstRefToDGJump();

  // previous current outward normal is used to avoid its derivatives
  SVector3& normDir = cohIpv->getRefToCohesiveNormal();
  normDir = fipvprev->getConstRefToCurrentOutwardNormal();
  normDir.normalize();

  // jump = jump0 + ujump-ujump0
  SVector3& jump = cohIpv->getRefToCohesiveJump();
  jump = jump0;
  jump += ujump;
  jump -= ujump0;
  
  
  //jump.print("jump");
  //normDir.print("normDir");

  if (_cohesiveLaw == NULL) Msg::Error("CohesiveLaw is not set BaseCohesive3DLaw::stress");
  // compute interface force and tangent
  SVector3& interfaceForce = fipv->getRefToInterfaceForce();
  STensor3& DinterfaceForceDjump = fipv->getRefToDInterfaceForceDjump();

  IPCohesive* q1 = cohIpv->getIPCohesive();
  const IPCohesive* q0 = cohIpvprev->getIPCohesive();

  _cohesiveLaw->constitutive(jump,normDir,q0,q1,interfaceForce,stiff,DinterfaceForceDjump);

  //jump.print("jump");
  //Msg::Info("curjump = %.6f, initial jump = %.6f critical jump = %.6f",q1->getConstRefToMaximalEffectiveJump(), q1->getEffectiveJumpDamageOnset(),q1->getCriticalEffectiveJump());

  double timeStep = this->getTimeStep();
  const SVector3 &jumpPrev= cohIpvprev->getConstRefToCohesiveJump();
  for(int i=0; i<3; i++)
  {
    if(timeStep>0.)
    {
      interfaceForce(i)+=_viscosity/timeStep*(jump(i)-jumpPrev(i));
      if (stiff)
         DinterfaceForceDjump(i,i)+=_viscosity/timeStep;
    }
  }


  // the surface change is assumed to be detemined from previous--> avoid derivatives
  double surfRatio = fipvprev->getConstRefToCurrentOutwardNormal().norm()/fipvprev->getConstRefToReferenceOutwardNormal().norm();
  interfaceForce *= surfRatio;
  if (stiff){
     DinterfaceForceDjump *= surfRatio;
  };

}

ElasticCohesive3DLaw::ElasticCohesive3DLaw(const int num, const cohesiveElasticPotential& poten,  const double beta, const double Kn, const double Kp) :
									BaseCohesive3DLaw(num,1, 1, Kp), _beta(beta), _Kn(Kn)
{
  ZeroCohesiveDamageLaw zeroLaw(num); 
  _cohesiveLaw = new generalElasticDamageCohesiveLaw(num,poten,zeroLaw);
}


ElasticCohesive3DLaw::ElasticCohesive3DLaw(const ElasticCohesive3DLaw &source) : BaseCohesive3DLaw(source),
      _beta(source._beta), _Kn(source._Kn)
{

}
ElasticCohesive3DLaw::~ElasticCohesive3DLaw(){

};
void ElasticCohesive3DLaw::createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_,
                                          const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  IPVariable *ipvi = new ElasticCohesive3DIPVariable(*_cohesiveLaw);
  IPVariable *ipv1 = new ElasticCohesive3DIPVariable(*_cohesiveLaw);
  IPVariable *ipv2 = new ElasticCohesive3DIPVariable(*_cohesiveLaw);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void ElasticCohesive3DLaw::createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  ipv = new ElasticCohesive3DIPVariable(*_cohesiveLaw);
}


LinearCohesive3DLaw::LinearCohesive3DLaw(const int num, const double Gc, const double sigmac,
                                               const double beta,const double mu,
                                               const double fractureStrengthFactorMin,
                                               const double fractureStrengthFactorMax, const double Kp) :
									BaseCohesive3DLaw(num,fractureStrengthFactorMin, fractureStrengthFactorMax, Kp),
									_Gc(Gc), _sigmac(sigmac),_beta(beta), _mu(mu)
{
  _cohesiveLaw = new BiLinearCohesiveLaw(num,true);
}

LinearCohesive3DLaw::LinearCohesive3DLaw(const int num, const cohesiveElasticPotential& poten, const cohesiveDamageLaw& dam, const double Gc, const double sigmac,
                                               const double beta,const double mu,
                                               const double fractureStrengthFactorMin,
                                               const double fractureStrengthFactorMax, const double Kp) :
									BaseCohesive3DLaw(num,fractureStrengthFactorMin, fractureStrengthFactorMax, Kp),
									_Gc(Gc), _sigmac(sigmac),_beta(beta), _mu(mu)
{
  _cohesiveLaw = new generalElasticDamageCohesiveLaw(num,poten,dam);
}


LinearCohesive3DLaw::LinearCohesive3DLaw(const LinearCohesive3DLaw &source) : BaseCohesive3DLaw(source),
      _Gc(source._Gc), _sigmac(source._sigmac),
      _beta(source._beta), _mu(source._mu)
{

}
LinearCohesive3DLaw::~LinearCohesive3DLaw(){

};
void LinearCohesive3DLaw::createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_,
                                          const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  IPVariable *ipvi = new LinearCohesive3DIPVariable(*_cohesiveLaw);
  IPVariable *ipv1 = new LinearCohesive3DIPVariable(*_cohesiveLaw);
  IPVariable *ipv2 = new LinearCohesive3DIPVariable(*_cohesiveLaw);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void LinearCohesive3DLaw::createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  ipv = new LinearCohesive3DIPVariable(*_cohesiveLaw);
}

void LinearCohesive3DLaw::checkCohesiveInsertion(IPStateBase* ipsm, IPStateBase* ipsp, const bool forcedInsert) const{
  // check failure onset from both negative and postive IPStates
  FractureCohesive3DIPVariable* fMinusCur = dynamic_cast<FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::current));
  FractureCohesive3DIPVariable* fMinusPrev = dynamic_cast<FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::previous));

  FractureCohesive3DIPVariable* fPlusCur = dynamic_cast<FractureCohesive3DIPVariable*>(ipsp->getState(IPStateBase::current));
  FractureCohesive3DIPVariable* fPlusPrev = dynamic_cast<FractureCohesive3DIPVariable*>(ipsp->getState(IPStateBase::previous));

  if (fMinusCur == NULL or fPlusCur == NULL or fMinusPrev==NULL or fPlusPrev==NULL){
    Msg::Error("In LinearCohesive3DLaw::initCohesiveLaw, FractureCohesive3DIPVariable is not used ");
    return;
  }

  if (fMinusPrev->isbroken() or fPlusPrev->isbroken()){
    Msg::Warning("Previous IPs are broken");
    fMinusCur->broken();
    fPlusCur->broken();
    return;
  }

  // this step is only checked to activate cohesive law in next time step
  // so the current normal should be used
  static SVector3 normDir;
  normDir =  fMinusCur->getConstRefToCurrentOutwardNormal();
  normDir.normalize();

  // at interface, the mean operator is used with parameter gamma
  // if value 1 is used --> check in negative part only
  // if value 0 is used --> check in positive part only
  // value 0.5 should be used
  double gamma = 0.5; // average factor

  /* value of sigma_c which depends on random factor */
  double facminus = fMinusCur->fractureStrengthFactor();
  double facplus =  fPlusCur->fractureStrengthFactor();
  double effsigc = (gamma*facminus+ (1.-gamma)*facplus) * _sigmac; // effective stress limit


  // estimate value of cauchy stress at interface
  // get Cauchy stress on negative IP
  static STensor3 cauchyMinus;
  STensorOperation::zero(cauchyMinus);
  fMinusCur->getCauchyStress(cauchyMinus);
  // get Cauchy stress on positive IP
  static STensor3 cauchyPplus;
  STensorOperation::zero(cauchyPplus);
  fPlusCur->getCauchyStress(cauchyPplus);

  // get mean value using gamma
  static STensor3 cauchyAverage;
  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      cauchyAverage(i,j) = gamma*cauchyMinus(i,j)+ (1.-gamma)*cauchyPplus(i,j);

  // interface force vector to
  static SVector3 force0;
  for (int i=0; i<3; i++){
    force0(i)=0.;
    for (int j=0; j<3; j++){
      force0(i) += cauchyAverage(i,j)*normDir(j);
    }
  }

  double tractionSq=0.; // square interface force
  double snor =0.; // normal part
  double tau =0.; // sliding part

  for (int i=0; i<3; i++){
    tractionSq += force0(i)*force0(i);
    snor += force0(i)*normDir(i);
  }
  tau = sqrt(fabs(tractionSq-snor*snor));
  double traction = sqrt(tractionSq);

  // compute effective stress
  bool ift = true;
  double smax = 0.; // normal part when failure
  double seff = 0.; // effective stress when failure

  // effective stress (Camacho and Ortiz)
  if(snor>=0.){
    // traction case
    seff = sqrt(snor*snor+tau*tau/(_beta*_beta));
    smax = snor;
    ift = true; // tension flag
  }
  else{
    // compression case
    double temp = fabs(tau)-_mu*fabs(snor); // friction force
    if (temp >0)
      seff = temp/_beta;
    else
      seff = 0.;
    //no fracture in compression if mu<0
    if(_mu< 0) seff =0.;
    smax = tau;
    ift=false; // tension flag
  }

  if((seff> effsigc) or forcedInsert){
    if(forcedInsert and seff ==0.) seff= sqrt(snor*snor+tau*tau/(_beta*_beta)); //if not we could insert a crack with seff=0
    // failure occurs
    // activate cohesive element
    fMinusCur->broken();
    fPlusCur->broken();

   if (seff > effsigc){
      fMinusCur->getOnsetCriterion() = seff - effsigc;
      fPlusCur->getOnsetCriterion() =  seff - effsigc;
    }
    else if (forcedInsert){
      fMinusCur->getOnsetCriterion() = 0;
      fPlusCur->getOnsetCriterion() = 0.;
    }


    if (this->getMacroSolver()->withFailureBasedOnPreviousState()){
      // initialize cohesive law in negative part
      Cohesive3DIPVariableBase* coMinus = fMinusCur->getIPvFrac();
      coMinus->initializeFracture(fMinusCur->getConstRefToJump(),getKp(),seff,snor,tau,getGc(),getBeta(),ift,cauchyAverage,getKp(),normDir,force0,
                                fMinusCur->getIPvBulk());

      // positive part take a same value as negative cohesive part
      Cohesive3DIPVariableBase* coPlus = fPlusCur->getIPvFrac();
      coPlus->initializeFracture(fPlusCur->getConstRefToJump(),getKp(),seff,snor,tau,getGc(),getBeta(),ift,cauchyAverage,getKp(),normDir,force0,
                                  fPlusCur->getIPvBulk());

    }
    else{
      double fact = effsigc/seff;

      // initialize cohesive law in negative part
      Cohesive3DIPVariableBase* coMinus = fMinusCur->getIPvFrac();
      coMinus->initializeFracture(fMinusCur->getConstRefToJump(),getKp(),effsigc,fact*snor,fact*tau,getGc(),getBeta(),ift,cauchyAverage,getKp(),normDir,force0,
                                fMinusCur->getIPvBulk());

      // positive part take a same value as negative cohesive part
      Cohesive3DIPVariableBase* coPlus = fPlusCur->getIPvFrac();
      coPlus->initializeFracture(fPlusCur->getConstRefToJump(),getKp(),effsigc,fact*snor,fact*tau,getGc(),getBeta(),ift,cauchyAverage,getKp(),normDir,force0,
                                  fPlusCur->getIPvBulk());


      fMinusPrev->getIPvFrac()->operator =(*dynamic_cast<const IPVariable*>(coMinus));
      fPlusPrev->getIPvFrac()->operator =(*dynamic_cast<const IPVariable*>(coPlus));
    }
  }
  else{
    // no brocken
    fMinusCur->nobroken();
    fPlusCur->nobroken();
  }

};

BaseTransverseIsotropicLinearCohesive3D::BaseTransverseIsotropicLinearCohesive3D(const int num, const double GcL, const double sigmacL, const double GcT, const double sigmacT,
                                               const double beta, const double mu,
                                               const double fractureStrengthFactorMin,
                                               const double fractureStrengthFactorMax, const double Kp) :
									BaseCohesive3DLaw(num,fractureStrengthFactorMin, fractureStrengthFactorMax, Kp),
									_GcL(GcL), _sigmacL(sigmacL),_GcT(GcT), _sigmacT(sigmacT),_beta(beta), _mu(mu)
{

}

BaseTransverseIsotropicLinearCohesive3D::BaseTransverseIsotropicLinearCohesive3D(const BaseTransverseIsotropicLinearCohesive3D &source) : BaseCohesive3DLaw(source),
      _GcL(source._GcL), _sigmacL(source._sigmacL),
      _GcT(source._GcT), _sigmacT(source._sigmacT),
      _beta(source._beta), _mu(source._mu)
{

}
BaseTransverseIsotropicLinearCohesive3D::~BaseTransverseIsotropicLinearCohesive3D(){

};

void BaseTransverseIsotropicLinearCohesive3D::checkCohesiveInsertion(IPStateBase* ipsm, IPStateBase* ipsp, const bool forcedInsert) const{
  // check failure onset from both negative and postive IPStates
  FractureCohesive3DIPVariable* fMinusCur = dynamic_cast<FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::current));
  FractureCohesive3DIPVariable* fMinusPrev = dynamic_cast<FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::previous));

  FractureCohesive3DIPVariable* fPlusCur = dynamic_cast<FractureCohesive3DIPVariable*>(ipsp->getState(IPStateBase::current));
  FractureCohesive3DIPVariable* fPlusPrev = dynamic_cast<FractureCohesive3DIPVariable*>(ipsp->getState(IPStateBase::previous));

  if (fMinusCur == NULL or fPlusCur == NULL or fMinusPrev==NULL or fPlusPrev==NULL){
    Msg::Error("In BaseTransverseIsotropicLinearCohesive3D, FractureCohesive3DIPVariable is not used ");
    return;
  }

  if (fMinusPrev->isbroken() or fPlusPrev->isbroken()){
    Msg::Warning("Previous IPs are broken");
    fMinusCur->broken();
    fPlusCur->broken();
    return;
  }

  const TransverseIsotropicCohesiveLaw *tic = dynamic_cast<const TransverseIsotropicCohesiveLaw*>(_cohesiveLaw);
  if(tic==NULL) Msg::Error("In BaseTransverseIsotropicLinearCohesive3D, not a  TransverseIsotropicCohesiveLaw");
  Cohesive3DIPVariable* cohIpvMinus = dynamic_cast<Cohesive3DIPVariable*>(fMinusCur->getIPvFrac());
  if(cohIpvMinus==NULL) Msg::Error("In BaseTransverseIsotropicLinearCohesive3D, not a BaseCohesive3DIPVariable");
  Cohesive3DIPVariable* cohIpvPlus = dynamic_cast<Cohesive3DIPVariable*>(fPlusCur->getIPvFrac());
  if(cohIpvPlus==NULL) Msg::Error("In BaseTransverseIsotropicLinearCohesive3D, not a BaseCohesive3DIPVariable");


  static SVector3 anisotropyDirection;
  STensorOperation::zero(anisotropyDirection);
  tic->getTransverseDirection(anisotropyDirection,cohIpvMinus->getIPCohesive());
  anisotropyDirection.normalize();


  // this step is only checked to activate cohesive law in next time step
  // so the current normal should be used
  static SVector3 normDir;
  normDir =  fMinusCur->getConstRefToCurrentOutwardNormal();
  normDir.normalize();

  static SVector3 refNormDir;
  refNormDir =  fMinusCur->getConstRefToCohesiveReferenceNormal();
  refNormDir.normalize();


  double cosTheta = dot(anisotropyDirection,refNormDir);
  double sinTheta = norm(crossprod(anisotropyDirection,refNormDir));

  double sigmac   = sqrt(sinTheta*sinTheta*_sigmacT*_sigmacT+cosTheta*cosTheta*_sigmacL*_sigmacL);
  double Gc       = sqrt(sinTheta*sinTheta*_GcT*_GcT+cosTheta*cosTheta*_GcL*_GcL);

  // at interface, the mean operator is used with parameter gamma
  // if value 1 is used --> check in negative part only
  // if value 0 is used --> check in positive part only
  // value 0.5 should be used
  double gamma = 0.5; // average factor

  /* value of sigma_c which depends on random factor */
  double facminus = fMinusCur->fractureStrengthFactor();
  double facplus =  fPlusCur->fractureStrengthFactor();

  //CHANGE FROM HERE

  double effsigc = (gamma*facminus+ (1.-gamma)*facplus) * sigmac; // effective stress limit


  // estimate value of cauchy stress at interface
  // get Cauchy stress on negative IP
  static STensor3 cauchyMinus;
  STensorOperation::zero(cauchyMinus);
  fMinusCur->getCauchyStress(cauchyMinus);
  // get Cauchy stress on positive IP
  static STensor3 cauchyPplus;
  STensorOperation::zero(cauchyPplus);
  fPlusCur->getCauchyStress(cauchyPplus);

  // get mean value using gamma
  static STensor3 cauchyAverage;
  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      cauchyAverage(i,j) = gamma*cauchyMinus(i,j)+ (1.-gamma)*cauchyPplus(i,j);

  // interface force vector to
  static SVector3 force0;
  for (int i=0; i<3; i++){
    force0(i) = 0.;
    for (int j=0; j<3; j++){
      force0(i) += cauchyAverage(i,j)*normDir(j);
    }
  }

  double tractionSq=0.; // square interface force
  double snor =0.; // normal part
  double tau =0.; // sliding part

  for (int i=0; i<3; i++){
    tractionSq += force0(i)*force0(i);
    snor += force0(i)*normDir(i);
  }
  tau = sqrt(fabs(tractionSq-snor*snor));
  double traction = sqrt(tractionSq);

  // compute effective stress
  bool ift = true;
  double smax = 0.; // normal part when failure
  double seff = 0.; // effective stress when failure

  // effective stress (Camacho and Ortiz)
  if(snor>=0.){
    // traction case
    seff = sqrt(snor*snor+tau*tau/(_beta*_beta));
    smax = snor;
    ift = true; // tension flag
  }
  else{
    // compression case
    double temp = fabs(tau)-_mu*fabs(snor); // friction force
    if (temp >0)
      seff = temp/_beta;
    else
      seff = 0.;
    //no fracture in compression if mu<0
    if(_mu< 0) seff =0.;
    smax = tau;
    ift=false; // tension flag
  }

  if((seff> effsigc) or forcedInsert){
    if(forcedInsert and seff ==0.) seff= sqrt(snor*snor+tau*tau/(_beta*_beta));
    // failure occurs
    // activate cohesive element
    fMinusCur->broken();
    fPlusCur->broken();

    // initialize cohesive law in negative part
    if (seff > effsigc){
      fMinusCur->getOnsetCriterion() = seff - effsigc;
      fPlusCur->getOnsetCriterion() =  seff - effsigc;
    }
    else if (forcedInsert){
      fMinusCur->getOnsetCriterion() = 0;
      fPlusCur->getOnsetCriterion() = 0.;
    }

    if (this->getMacroSolver()->withFailureBasedOnPreviousState()){
      // initialize cohesive law in negative part
      Cohesive3DIPVariableBase* coMinus = fMinusCur->getIPvFrac();
      coMinus->initializeFracture(fMinusCur->getConstRefToJump(),getKp(),seff,snor,tau,Gc,getBeta(),ift,cauchyAverage,getKp(),normDir,force0,
                                fMinusCur->getIPvBulk());

      // positive part take a same value as negative cohesive part
      Cohesive3DIPVariableBase* coPlus = fPlusCur->getIPvFrac();
      coPlus->initializeFracture(fPlusCur->getConstRefToJump(),getKp(),seff,snor,tau,Gc,getBeta(),ift,cauchyAverage,getKp(),normDir,force0,
                                  fPlusCur->getIPvBulk());
    }
    else{

      Cohesive3DIPVariableBase* coMinus = fMinusCur->getIPvFrac();
      coMinus->initializeFracture(fMinusCur->getConstRefToJump(),getKp(),effsigc,snor,tau,Gc,getBeta(),ift,cauchyAverage,getKp(),normDir,force0,
                                fMinusCur->getIPvBulk());

      // positive part take a same value as negative cohesive part
      Cohesive3DIPVariableBase* coPlus = fPlusCur->getIPvFrac();
      coPlus->initializeFracture(fPlusCur->getConstRefToJump(),getKp(),effsigc,snor,tau,Gc,getBeta(),ift,cauchyAverage,getKp(),normDir,force0,
                                  fPlusCur->getIPvBulk());

      fMinusPrev->getIPvFrac()->operator =(*dynamic_cast<const IPVariable*>(coMinus));
      fPlusPrev->getIPvFrac()->operator =(*dynamic_cast<const IPVariable*>(coPlus));
    }

  }
  else{
    // no brocken
    fMinusCur->nobroken();
    fPlusCur->nobroken();
  }

};

TransverseIsotropicLinearCohesive3D::TransverseIsotropicLinearCohesive3D(const int num, const double GcL, const double sigmacL, const double GcT, const double sigmacT,
                                               const double beta, const double mu,
                                               const double Ax, const double Ay, const double Az,
                                               const double fractureStrengthFactorMin,
                                               const double fractureStrengthFactorMax, const double Kp) :
									BaseTransverseIsotropicLinearCohesive3D(num, GcL, sigmacL, GcT, sigmacT, beta, mu,
                                                                                                                fractureStrengthFactorMin, fractureStrengthFactorMax, Kp)
{
   _cohesiveLaw = new TransverseIsotropicCohesiveLaw(Ax, Ay, Az, num, true);
}

TransverseIsotropicLinearCohesive3D::TransverseIsotropicLinearCohesive3D(const TransverseIsotropicLinearCohesive3D &source) : BaseTransverseIsotropicLinearCohesive3D(source)
{

}
TransverseIsotropicLinearCohesive3D::~TransverseIsotropicLinearCohesive3D(){

};


void TransverseIsotropicLinearCohesive3D::createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_,
                                          const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  IPVariable *ipvi = new TransverseIsotropicLinearCohesive3DIPVariable(*_cohesiveLaw);
  IPVariable *ipv1 = new TransverseIsotropicLinearCohesive3DIPVariable(*_cohesiveLaw);
  IPVariable *ipv2 = new TransverseIsotropicLinearCohesive3DIPVariable(*_cohesiveLaw);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void TransverseIsotropicLinearCohesive3D::createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  ipv = new TransverseIsotropicLinearCohesive3DIPVariable(*_cohesiveLaw);
}

TransverseIsoCurvatureLinearCohesive3D::TransverseIsoCurvatureLinearCohesive3D(const int num, const double GcL, const double sigmacL, const double GcT, const double sigmacT,
                                               const double beta, const double mu,
                                               const double Centroid_x, const double Centroid_y, const double Centroid_z,
                                               const double PointStart1_x, const double PointStart1_y, const double PointStart1_z,
                                               const double PointStart2_x,  const double PointStart2_y, const double PointStart2_z, const double init_Angle,
                                               const double fractureStrengthFactorMin,
                                               const double fractureStrengthFactorMax, const double Kp) :
									BaseTransverseIsotropicLinearCohesive3D(num, GcL, sigmacL, GcT, sigmacT, beta, mu,
                                                                                                                fractureStrengthFactorMin, fractureStrengthFactorMax, Kp)
{
   _cohesiveLaw = new TransverseIsoCurvatureCohesiveLaw(Centroid_x, Centroid_y, Centroid_z, PointStart1_x, PointStart1_y, PointStart1_z,
                                                PointStart2_x, PointStart2_y, PointStart2_z, init_Angle, num, true);
}

TransverseIsoCurvatureLinearCohesive3D::TransverseIsoCurvatureLinearCohesive3D(const TransverseIsoCurvatureLinearCohesive3D &source) : BaseTransverseIsotropicLinearCohesive3D(source)
{

}
TransverseIsoCurvatureLinearCohesive3D::~TransverseIsoCurvatureLinearCohesive3D(){

};


void TransverseIsoCurvatureLinearCohesive3D::createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_,
                                          const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
//  bool inter=true;
//  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
//  if(iele==NULL) inter=false;

  SPoint3 pt_Global;
  ele->pnt(GP->pt[0],GP->pt[1],GP->pt[2],pt_Global);
  SVector3 GaussP(pt_Global);


  const TransverseIsoCurvatureCohesiveLaw *_tilaw= dynamic_cast<const TransverseIsoCurvatureCohesiveLaw*>(_cohesiveLaw);
  if(_tilaw==NULL) Msg::Error("InTransverseIsoCurvatureLinearCohesive3D, not a TransverseIsoCurvatureCohesiveLaw");

  IPVariable *ipvi = new TransverseIsoCurvatureLinearCohesive3DIPVariable(*_cohesiveLaw, GaussP, _tilaw->getCentroid(), _tilaw->getPointStart1(), _tilaw->getPointStart2(), _tilaw->getInit_Angle());
  IPVariable *ipv1 = new TransverseIsoCurvatureLinearCohesive3DIPVariable(*_cohesiveLaw, GaussP, _tilaw->getCentroid(), _tilaw->getPointStart1(), _tilaw->getPointStart2(), _tilaw->getInit_Angle());
  IPVariable *ipv2 = new TransverseIsoCurvatureLinearCohesive3DIPVariable(*_cohesiveLaw, GaussP, _tilaw->getCentroid(), _tilaw->getPointStart1(), _tilaw->getPointStart2(), _tilaw->getInit_Angle());
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void TransverseIsoCurvatureLinearCohesive3D::createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  SPoint3 pt_Global;
  ele->pnt(GP->pt[0],GP->pt[1],GP->pt[2],pt_Global);
  SVector3 GaussP(pt_Global);


  const TransverseIsoCurvatureCohesiveLaw *_tilaw= dynamic_cast<const TransverseIsoCurvatureCohesiveLaw*>(_cohesiveLaw);
  if(_tilaw==NULL) Msg::Error("InTransverseIsoCurvatureLinearCohesive3D, not a TransverseIsoCurvatureCohesiveLaw");
  ipv = new TransverseIsoCurvatureLinearCohesive3DIPVariable(*_cohesiveLaw, GaussP, _tilaw->getCentroid(), _tilaw->getPointStart1(), _tilaw->getPointStart2(), _tilaw->getInit_Angle());
}

//
NonLocalDamageLinearCohesive3DLaw::NonLocalDamageLinearCohesive3DLaw(const int num, const double Gc, const double sigmac, const double Dc,
                                               const double beta,
                                               const double fractureStrengthFactorMin,
                                               const double fractureStrengthFactorMax, const double Kp,
																							 const bool damageCheck) :
								LinearCohesive3DLaw(num,Gc,sigmac,beta,0.,fractureStrengthFactorMin,fractureStrengthFactorMax,Kp),
                                                                                         _Dc(Dc),_damageBased(damageCheck)
{

}

NonLocalDamageLinearCohesive3DLaw::NonLocalDamageLinearCohesive3DLaw(const int num, const cohesiveElasticPotential& poten, const cohesiveDamageLaw& dam, const double Gc, const double sigmac, const double Dc,
                                               const double beta,
                                               const double fractureStrengthFactorMin,
                                               const double fractureStrengthFactorMax, const double Kp,
																							 const bool damageCheck) :
								LinearCohesive3DLaw(num,poten,dam,Gc,sigmac,beta,0.,fractureStrengthFactorMin,fractureStrengthFactorMax,Kp),
                                                                                         _Dc(Dc),_damageBased(damageCheck)
{

}

NonLocalDamageLinearCohesive3DLaw::NonLocalDamageLinearCohesive3DLaw(const NonLocalDamageLinearCohesive3DLaw &source) : LinearCohesive3DLaw(source),
                                                                                        _Dc(source._Dc), _damageBased(source._damageBased)
{

}
void NonLocalDamageLinearCohesive3DLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,
                                          const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  IPVariable *ipvi = new NonLocalDamageLinearCohesive3DIPVariable(*_cohesiveLaw);
  IPVariable *ipv1 = new NonLocalDamageLinearCohesive3DIPVariable(*_cohesiveLaw);
  IPVariable *ipv2 = new NonLocalDamageLinearCohesive3DIPVariable(*_cohesiveLaw);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void NonLocalDamageLinearCohesive3DLaw::createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  ipv = new NonLocalDamageLinearCohesive3DIPVariable(*_cohesiveLaw);
}


void NonLocalDamageLinearCohesive3DLaw::stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff, const bool checkfrac, const bool dTangent)
{
  LinearCohesive3DLaw::stress(ipv,ipvprev,stiff,checkfrac);
}

void NonLocalDamageLinearCohesive3DLaw::checkCohesiveInsertion(IPStateBase* ipsm, IPStateBase* ipsp, const bool forcedInsert) const{
  // check failure onset from both negative and postive IPStates
  FractureCohesive3DIPVariable* fMinusCur = dynamic_cast<FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::current));
  FractureCohesive3DIPVariable* fMinusPrev = dynamic_cast<FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::previous));

  FractureCohesive3DIPVariable* fPlusCur = dynamic_cast<FractureCohesive3DIPVariable*>(ipsp->getState(IPStateBase::current));
  FractureCohesive3DIPVariable* fPlusPrev = dynamic_cast<FractureCohesive3DIPVariable*>(ipsp->getState(IPStateBase::previous));

  if (fMinusCur == NULL or fPlusCur == NULL or fMinusPrev==NULL or fPlusPrev==NULL){
    Msg::Error("In LinearCohesive3DLaw::initCohesiveLaw, FractureCohesive3DIPVariable is not used ");
    return;
  }

  if (fMinusPrev->isbroken() or fPlusPrev->isbroken()){
    Msg::Warning("Previous IPs are broken");
    fMinusCur->broken();
    fPlusCur->broken();
    return;
  }


  // because this step is check to activate the cohesive law for the next time step
  // current outward normal is used
  static SVector3 normDir;
  normDir =  fMinusCur->getConstRefToCurrentOutwardNormal();
  normDir.normalize();

  // estimate value of cauchy stress at interface
  double gamma = 0.5; // average factor

  /* value of sigma_c which depends on random factor */
  double facminus = fMinusCur->fractureStrengthFactor();
  double facplus =  fPlusCur->fractureStrengthFactor();

  double remainingG = _Gc;
  // value of sigma_c which depends on random factor
  const double effDc = (gamma*facminus+ (1.-gamma)*facplus) * _Dc;
  const double effsigc = (gamma*facminus+ (1.-gamma)*facplus) * _sigmac;

  // get Cauchy stress on negative IP
  static STensor3 cauchyMinus;
  STensorOperation::zero(cauchyMinus);
  fMinusCur->getCauchyStress(cauchyMinus);
  // get Cauchy stress on positive IP
  static STensor3 cauchyPplus;
  STensorOperation::zero(cauchyPplus);
  fPlusCur->getCauchyStress(cauchyPplus);

  // get mean value using gamma
  static STensor3 cauchyAverage;
  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      cauchyAverage(i,j) = gamma*cauchyMinus(i,j)+ (1.-gamma)*cauchyPplus(i,j);

  // interface force vector n
  static SVector3 force0;
  for (int i=0; i<3; i++){
    force0(i)=0.;
    for (int j=0; j<3; j++){
      force0(i) += cauchyAverage(i,j)*normDir(j);
    }
  }

  double tractionSq=0.; // square interface force
  double snor =0.; // normal part
  double tau =0.; // sliding part

  for (int i=0; i<3; i++){
    tractionSq += force0(i)*force0(i);
    snor += force0(i)*normDir(i);
  }
  tau = sqrt(fabs(tractionSq-snor*snor));
  double traction = sqrt(tractionSq);

  // compute effective stress
  bool ift = true;
  double smax = 0.; // normal part when failure
  double seff = 0.; // effective stress when failure

  // sigma eff (Camacho and Ortiz)
  if(snor>=0.){
    // traction case
    seff = sqrt(snor*snor+tau*tau/(_beta*_beta)); // _betaexpm2 = 1/beta^2
    smax = snor;
    ift = true; // tension flag
  }
  else{
    // compression case
    double temp = fabs(tau)-_mu*fabs(snor); // friction force
    if (temp >0)
      seff = temp/_beta;
    else
      seff = 0.;
    //no fracture in compression if mu<0
    if(_mu< 0) seff =0.;
    smax = tau;
    ift=false; // tension flag
  }

  //get damage
  double damage = gamma*fMinusCur->get(IPField::DAMAGE) + (1.-gamma)*fPlusCur->get(IPField::DAMAGE);

  //broken check according to effective sttress (Ling Wu)

  bool broken = false;
  if (_damageBased){
     broken = damage > effDc;
     if (broken){
       fMinusCur->getOnsetCriterion() = damage- effDc;
       fPlusCur->getOnsetCriterion() = damage-effDc;
     }
   }
   else{
     broken = seff/(1.0-damage)> effsigc;
     if (broken){
       fMinusCur->getOnsetCriterion() = seff/(1.0-damage)- effsigc;
       fPlusCur->getOnsetCriterion() = seff/(1.0-damage)- effsigc;
       fMinusCur->setNonLocalToLocal(true);
       fPlusCur->setNonLocalToLocal(true);
     }
   }


   //if(damage > effDc && ift){

  //if(seff/(1.0-damage)> effsigc){
    //Msg::Info("broken with damage %e, smax %e, traction %d",damage, smax,ift);
    // failure occurs
    // activate cohesive element
  if (broken or forcedInsert){
    if(forcedInsert and seff ==0.) seff= sqrt(snor*snor+tau*tau/(_beta*_beta));

    fMinusCur->broken();
    fPlusCur->broken();
    if (forcedInsert){
      fMinusCur->getOnsetCriterion() = 0;
      fPlusCur->getOnsetCriterion() = 0.;
    }

    // set critical damage at insertation time
    // the damage variable in bulk IPs remains constant after inserting cohesive elements
    // the material softening continues with cohesive elements
    /*if(fPlusCur->getNumberNonLocalVariable()!=fMinusCur->getNumberNonLocalVariable())
      Msg::Error("NonLocalDamageLinearCohesive3DLaw::checkCohesiveInsertion: the two laws do not have the same number of non-local variables");
    for(int nlv=0; nlv<fMinusCur->getNumberNonLocalVariable(); nlv++)    
    {
      fMinusCur->setBulkCriticalDamage(fMinusCur->getDamageIndicator(nlv),nlv);
      fPlusCur->setBulkCriticalDamage(fPlusCur->getDamageIndicator(nlv),nlv);
    }*/
    // initialize cohesive law in negative part
    Cohesive3DIPVariableBase* coMinus = fMinusCur->getIPvFrac();
    coMinus->initializeFracture(fMinusCur->getConstRefToJump(),getKp(),seff,snor,tau,remainingG,getBeta(),ift,cauchyAverage,getKp(),normDir,force0,
                                fMinusCur->getIPvBulk());

    // positive part take a same value as negative cohesive part
    Cohesive3DIPVariableBase* coPlus = fPlusCur->getIPvFrac();
    coPlus->initializeFracture(fPlusCur->getConstRefToJump(),getKp(),seff,snor,tau,remainingG,getBeta(),ift,cauchyAverage,getKp(),normDir,force0,
                                fPlusCur->getIPvBulk());

    if (!this->getMacroSolver()->withFailureBasedOnPreviousState()){
      fMinusPrev->getIPvFrac()->operator =(*dynamic_cast<const IPVariable*>(coMinus));
      fPlusPrev->getIPvFrac()->operator =(*dynamic_cast<const IPVariable*>(coPlus));
    }
  }
  else{
    // no brocken
    fMinusCur->nobroken();
    fPlusCur->nobroken();
  }

};



//------added begin   ---------------WU--------------

NonLocalDamageTransverseIsotropicLinearCohesive3DLaw::NonLocalDamageTransverseIsotropicLinearCohesive3DLaw(const int num, const double GcL, const double sigmacL, const double DcL, const double GcT, 
                         const double sigmacT, const double DcT, const double beta, const double mu, const double Ax, const double Ay, const double Az, const double fractureStrengthFactorMin,
                         const double fractureStrengthFactorMax, const double Kp, const bool damageCheck):
                         BaseTransverseIsotropicLinearCohesive3D(num, GcL, sigmacL, GcT, sigmacT, beta, mu, fractureStrengthFactorMin, fractureStrengthFactorMax, Kp),
			 _DcL(DcL), _DcT(DcT), _damageBased(damageCheck)
{
   _cohesiveLaw = new TransverseIsotropicCohesiveLaw(Ax, Ay, Az, num, true);
}

NonLocalDamageTransverseIsotropicLinearCohesive3DLaw::NonLocalDamageTransverseIsotropicLinearCohesive3DLaw(const NonLocalDamageTransverseIsotropicLinearCohesive3DLaw &source) :          
                                                          BaseTransverseIsotropicLinearCohesive3D(source), _DcL(source._DcL), _DcT(source._DcT), _damageBased(source._damageBased)
{

}

void NonLocalDamageTransverseIsotropicLinearCohesive3DLaw::createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_,
                                          const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  IPVariable *ipvi = new NonLocalDamageTransverseIsotropicLinearCohesive3DIPVariable(*_cohesiveLaw);
  IPVariable *ipv1 = new NonLocalDamageTransverseIsotropicLinearCohesive3DIPVariable(*_cohesiveLaw);
  IPVariable *ipv2 = new NonLocalDamageTransverseIsotropicLinearCohesive3DIPVariable(*_cohesiveLaw);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void NonLocalDamageTransverseIsotropicLinearCohesive3DLaw::createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  ipv = new NonLocalDamageTransverseIsotropicLinearCohesive3DIPVariable(*_cohesiveLaw);
}


void NonLocalDamageTransverseIsotropicLinearCohesive3DLaw::stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff, const bool checkfrac)
{
  BaseTransverseIsotropicLinearCohesive3D::stress(ipv,ipvprev,stiff,checkfrac);
}


void NonLocalDamageTransverseIsotropicLinearCohesive3DLaw::checkCohesiveInsertion(IPStateBase* ipsm, IPStateBase* ipsp, const bool forcedInsert) const{
  // check failure onset from both negative and postive IPStates
  FractureCohesive3DIPVariable* fMinusCur = dynamic_cast<FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::current));
  FractureCohesive3DIPVariable* fMinusPrev = dynamic_cast<FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::previous));

  FractureCohesive3DIPVariable* fPlusCur = dynamic_cast<FractureCohesive3DIPVariable*>(ipsp->getState(IPStateBase::current));
  FractureCohesive3DIPVariable* fPlusPrev = dynamic_cast<FractureCohesive3DIPVariable*>(ipsp->getState(IPStateBase::previous));

  if (fMinusCur == NULL or fPlusCur == NULL or fMinusPrev==NULL or fPlusPrev==NULL){
    Msg::Error("In NonLocalDamageTransverseIsotropicLinearCohesive3DLaw, FractureCohesive3DIPVariable is not used ");
    return;
  }

  if (fMinusPrev->isbroken() or fPlusPrev->isbroken()){
    Msg::Warning("Previous IPs are broken");
    fMinusCur->broken();
    fPlusCur->broken();
    return;
  }

  const TransverseIsotropicCohesiveLaw *tic = dynamic_cast<const TransverseIsotropicCohesiveLaw*>(_cohesiveLaw);
  if(tic==NULL) Msg::Error("In NonLocalDamageTransverseIsotropicLinearCohesive3DLaw, not a  TransverseIsotropicCohesiveLaw");
  Cohesive3DIPVariable* cohIpvMinus = dynamic_cast<Cohesive3DIPVariable*>(fMinusCur->getIPvFrac());
  if(cohIpvMinus==NULL) Msg::Error("In NonLocalDamageTransverseIsotropicLinearCohesive3DLaw, not a BaseCohesive3DIPVariable");
  Cohesive3DIPVariable* cohIpvPlus = dynamic_cast<Cohesive3DIPVariable*>(fPlusCur->getIPvFrac());
  if(cohIpvPlus==NULL) Msg::Error("In NonLocalDamageTransverseIsotropicLinearCohesive3DLaw, not a BaseCohesive3DIPVariable");

  static SVector3 anisotropyDirection;
  STensorOperation::zero(anisotropyDirection);
  tic->getTransverseDirection(anisotropyDirection,cohIpvMinus->getIPCohesive());
  anisotropyDirection.normalize();


  // because this step is check to activate the cohesive law for the next time step
  // current outward normal is used
  static SVector3 normDir;
  normDir =  fMinusCur->getConstRefToCurrentOutwardNormal();
  normDir.normalize();

  static SVector3 refNormDir;
  refNormDir =  fMinusCur->getConstRefToCohesiveReferenceNormal();
  refNormDir.normalize();

  double cosTheta = dot(anisotropyDirection,refNormDir);
  double sinTheta = norm(crossprod(anisotropyDirection,refNormDir));

  double gamma = 0.5; // average factor
  // set the broken criterion with the effect of damage     2019 L.Wu
  double damage_L = gamma*fMinusCur->get(IPField::INC_DAMAGE) + (1.-gamma)*fPlusCur->get(IPField::INC_DAMAGE);
  double damage_T = gamma*fMinusCur->get(IPField::MTX_DAMAGE) + (1.-gamma)*fPlusCur->get(IPField::MTX_DAMAGE);

  double sigmac   = sqrt(sinTheta*sinTheta*_sigmacT*_sigmacT*(1.0-damage_T)*(1.0-damage_T)+cosTheta*cosTheta*_sigmacL*_sigmacL*(1.0-damage_L)*(1.0-damage_L));
  double remainingG   = sqrt(sinTheta*sinTheta*_GcT*_GcT+cosTheta*cosTheta*_GcL*_GcL);

  // estimate value of cauchy stress at interface

  /* value of sigma_c which depends on random factor */
  double facminus = fMinusCur->fractureStrengthFactor();
  double facplus =  fPlusCur->fractureStrengthFactor();


  const double effDcL = (gamma*facminus+ (1.-gamma)*facplus) * _DcL;
  const double effDcT = (gamma*facminus+ (1.-gamma)*facplus) * _DcT;
  const double effsigc = (gamma*facminus+ (1.-gamma)*facplus) * sigmac;

  // get Cauchy stress on negative IP
  static STensor3 cauchyMinus;
  STensorOperation::zero(cauchyMinus);
  fMinusCur->getCauchyStress(cauchyMinus);
  // get Cauchy stress on positive IP
  static STensor3 cauchyPplus;
  STensorOperation::zero(cauchyPplus);
  fPlusCur->getCauchyStress(cauchyPplus);

  // get mean value using gamma
  static STensor3 cauchyAverage;
  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      cauchyAverage(i,j) = gamma*cauchyMinus(i,j)+ (1.-gamma)*cauchyPplus(i,j);

  // interface force vector n
  static SVector3 force0;
  for (int i=0; i<3; i++){
    force0(i)=0.;
    for (int j=0; j<3; j++){
      force0(i) += cauchyAverage(i,j)*normDir(j);
    }
  }

  double tractionSq=0.; // square interface force
  double snor =0.; // normal part
  double tau =0.; // sliding part

  for (int i=0; i<3; i++){
    tractionSq += force0(i)*force0(i);
    snor += force0(i)*normDir(i);
  }
  tau = sqrt(fabs(tractionSq-snor*snor));
  double traction = sqrt(tractionSq);

  // compute effective stress
  bool ift = true;
  double smax = 0.; // normal part when failure
  double seff = 0.; // effective stress when failure

  // sigma eff (Camacho and Ortiz)
  if(snor>=0.){
    // traction case
    seff = sqrt(snor*snor+tau*tau/(_beta*_beta)); // _betaexpm2 = 1/beta^2
    smax = snor;
    ift = true; // tension flag
  }
  else{
    // compression case
    double temp = fabs(tau)-_mu*fabs(snor); // friction force
    if (temp >0)
      seff = temp/_beta;
    else
      seff = 0.;
    //no fracture in compression if mu<0
    if(_mu< 0) seff =0.;
    smax = tau;
    ift=false; // tension flag
  }

  //broken check according to damage reduced maximum stress sigma_c (Ling Wu)

	 bool broken = false;
	 if (_damageBased){
	   Msg::Error("In NonLocalDamageTransverseIsotropicLinearCohesive3DLaw, Damage based broken is not applicable.");
	 }
	 else{
	     broken = seff > effsigc;
	     if (broken){
                fMinusCur->getOnsetCriterion() = seff - effsigc;
                fPlusCur->getOnsetCriterion() = seff - effsigc;
             }
	 }

   //if(damage > effDc && ift){

  //if(seff/(1.0-damage)> effsigc){
    //Msg::Info("broken with damage %e, smax %e, traction %d",damage, smax,ift);
    // failure occurs
    // activate cohesive element

  if (broken or forcedInsert){
    if(forcedInsert and seff ==0.) seff= sqrt(snor*snor+tau*tau/(_beta*_beta));

    fMinusCur->broken();
    fPlusCur->broken();
    fMinusCur->setNonLocalToLocal(true);
    fPlusCur->setNonLocalToLocal(true);
    if (forcedInsert){
      fMinusCur->getOnsetCriterion() = 0;
      fPlusCur->getOnsetCriterion() = 0.;
    }

    // set critical damage at insertation time
    // the damage variable in bulk IPs remains constant after inserting cohesive elements
    // the material softening continues with cohesive elements

/*    if(fPlusCur->getNumberNonLocalVariable()!=fMinusCur->getNumberNonLocalVariable())
      Msg::Error("NonLocalDamageTransverseIsotropicLinearCohesive3DLaw::checkCohesiveInsertion: the two laws do not have the same number of non-local variables");
    for(int nlv=0; nlv<fMinusCur->getNumberNonLocalVariable(); nlv++)    
    {
      fMinusCur->setBulkCriticalDamage(fMinusCur->getDamageIndicator(nlv),nlv);
      fPlusCur->setBulkCriticalDamage(fPlusCur->getDamageIndicator(nlv),nlv);
    }*/

    // initialize cohesive law in negative part
    Cohesive3DIPVariableBase* coMinus = fMinusCur->getIPvFrac();
    coMinus->initializeFracture(fMinusCur->getConstRefToJump(),getKp(),seff,snor,tau,remainingG,getBeta(),ift,cauchyAverage,getKp(),normDir,force0,
                                fMinusCur->getIPvBulk());

    // positive part take a same value as negative cohesive part
    Cohesive3DIPVariableBase* coPlus = fPlusCur->getIPvFrac();
    coPlus->initializeFracture(fPlusCur->getConstRefToJump(),getKp(),seff,snor,tau,remainingG,getBeta(),ift,cauchyAverage,getKp(),normDir,force0,
                                fPlusCur->getIPvBulk());
     
    if (!this->getMacroSolver()->withFailureBasedOnPreviousState()){
        fMinusPrev->getIPvFrac()->operator =(*dynamic_cast<const IPVariable*>(coMinus));
        fPlusPrev->getIPvFrac()->operator =(*dynamic_cast<const IPVariable*>(coPlus));
     }
  }
  else{
    // no brocken
    fMinusCur->nobroken();
    fPlusCur->nobroken();
  }
};

//------added end-----------------WU ---------



DelaminationLinearCohesive3DLaw::DelaminationLinearCohesive3DLaw(const int num, const double GcI, const double GcII, const double sigmacI,const double sigmacII, const double _alphaGc,
              const double fractureStrengthFactorMin, const double fractureStrengthFactorMax,
              const double Kp) :
	                                          LinearCohesive3DLaw(num,GcI,sigmacI,0,-1.,fractureStrengthFactorMin,fractureStrengthFactorMax,Kp),
                                            xiI(1.),xiII(1.)
{
    if(sigmacI <= 0. or sigmacII <= 0. or GcI <= 0. or GcII <= 0. or _alphaGc <= 0.)
      Msg::Error("DelaminationLinearCohesive3DLaw: wrong inputs");
    mixedModeSigmaRatio=sigmacII/sigmacI;
    mixedModeGcRatio=GcII/GcI;
    alphaGc=_alphaGc;
}

DelaminationLinearCohesive3DLaw::DelaminationLinearCohesive3DLaw(const int num, const cohesiveElasticPotential& poten, const cohesiveDamageLaw& dam, const double GcI, const double GcII, const double sigmacI,const double sigmacII, const double _alphaGc,
              const double fractureStrengthFactorMin, const double fractureStrengthFactorMax,
              const double Kp) :
	                                          LinearCohesive3DLaw(num,poten,dam,GcI,sigmacI,0,-1.,fractureStrengthFactorMin,fractureStrengthFactorMax,Kp),
                                            xiI(1.),xiII(1.)
{
    if(sigmacI <= 0. or sigmacII <= 0. or GcI <= 0. or GcII <= 0. or _alphaGc <= 0.)
      Msg::Error("DelaminationLinearCohesive3DLaw: wrong inputs");
    mixedModeSigmaRatio=sigmacII/sigmacI;
    mixedModeGcRatio=GcII/GcI;
    alphaGc=_alphaGc;
    
    const ExponentialCohesiveDamageLaw* damlaw = dynamic_cast<const ExponentialCohesiveDamageLaw*>(&dam);
    if (damlaw != NULL)
    {
      xiI = damlaw->getPowerParameter();
      xiII = damlaw->getPowerParameter();
    };
}


DelaminationLinearCohesive3DLaw::DelaminationLinearCohesive3DLaw(const DelaminationLinearCohesive3DLaw &source) : LinearCohesive3DLaw(source)
{
    mixedModeSigmaRatio=source.mixedModeSigmaRatio;
    mixedModeGcRatio=source.mixedModeGcRatio;
    alphaGc=source.alphaGc;
    xiI = source.xiI;
    xiII = source.xiII;
}

void DelaminationLinearCohesive3DLaw::createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_,
                                          const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  IPVariable *ipvi = new DelaminationLinearCohesive3DIPVariable(*_cohesiveLaw);
  IPVariable *ipv1 = new DelaminationLinearCohesive3DIPVariable(*_cohesiveLaw);
  IPVariable *ipv2 = new DelaminationLinearCohesive3DIPVariable(*_cohesiveLaw);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void DelaminationLinearCohesive3DLaw::createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  ipv = new DelaminationLinearCohesive3DIPVariable(*_cohesiveLaw);
}

void DelaminationLinearCohesive3DLaw::stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff, const bool checkfrac, const bool dTangent){
  LinearCohesive3DLaw::stress(ipv,ipvprev,stiff,checkfrac);
};


void DelaminationLinearCohesive3DLaw::checkCohesiveInsertion(IPStateBase* ipsm, IPStateBase* ipsp, const bool forcedInsert) const{
  // check failure onset from both negative and postive IPStates
  FractureCohesive3DIPVariable* fMinusCur = dynamic_cast<FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::current));
  FractureCohesive3DIPVariable* fMinusPrev = dynamic_cast<FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::previous));

  FractureCohesive3DIPVariable* fPlusCur = dynamic_cast<FractureCohesive3DIPVariable*>(ipsp->getState(IPStateBase::current));
  FractureCohesive3DIPVariable* fPlusPrev = dynamic_cast<FractureCohesive3DIPVariable*>(ipsp->getState(IPStateBase::previous));

   if (fMinusCur == NULL or fPlusCur == NULL or fMinusPrev==NULL or fPlusPrev==NULL){
    Msg::Error("In LinearCohesive3DLaw::initCohesiveLaw, FractureCohesive3DIPVariable is not used ");
    return;
  }

  if (fMinusPrev->isbroken() or fPlusPrev->isbroken()){
    Msg::Warning("Previous IPs are broken");
    fMinusCur->broken();
    fPlusCur->broken();
    return;
  }



  // because this step is check to activate the cohesive law for the next time step
  // current outward normal is used
  static SVector3 normDir;
  normDir =  fMinusCur->getConstRefToCurrentOutwardNormal();// fMinusPrev->getConstRefToCurrentOutwardNormal(); //
  normDir.normalize();

  // estimate value of cauchy stress at interface
  double gamma = 0.5; // average factor

  // get Cauchy stress on negative IP
  static STensor3 cauchyMinus;
  STensorOperation::zero(cauchyMinus);
  fMinusCur->getCauchyStress(cauchyMinus);
  // get Cauchy stress on positive IP
  static STensor3 cauchyPplus;
  STensorOperation::zero(cauchyPplus);
  fPlusCur->getCauchyStress(cauchyPplus);

  // get mean value using gamma
  static STensor3 cauchyAverage;
  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      cauchyAverage(i,j) = gamma*cauchyMinus(i,j)+ (1.-gamma)*cauchyPplus(i,j);

  // interface force vector n
  static SVector3 force0;
  for (int i=0; i<3; i++){
    force0(i)=0.;
    for (int j=0; j<3; j++){
      force0(i) += cauchyAverage(i,j)*normDir(j);
    }
  }

  double tractionSq=0.; // square interface force
  double snor =0.; // normal part
  double tau =0.; // sliding part

  for (int i=0; i<3; i++){
    tractionSq += force0(i)*force0(i);
    snor += force0(i)*normDir(i);
  }
  tau = sqrt(fabs(tractionSq-snor*snor));
  double traction = sqrt(tractionSq);

  /* value of sigma_c which depends on random factor */
  double facminus = fMinusCur->fractureStrengthFactor();
  double facplus =  fPlusCur->fractureStrengthFactor();
  // get Damage
  double damage = gamma*fMinusCur->get(IPField::DAMAGE) + (1.-gamma)*fPlusCur->get(IPField::DAMAGE);
  if(damage>0.999999999) damage=0.999999999;

  const double effsigcI = (gamma*facminus+ (1.-gamma)*facplus)*(1.-damage) * getSigmac();
  const double effsigcII = (gamma*facminus+ (1.-gamma)*facplus)*(1.-damage) * getSigmacII();
  const double effGcI = getGc();
  const double effGcII = getGcII();


  bool ift;
  double smax;
  // sigma eff (Camacho and Ortiz)
  if(effsigcI <=0. or effsigcII <=0. or effGcI <=0. or effGcII <=0.)
     Msg::Error("DelaminationLinearCohesive3DLaw: wrong input");

  double mixedratio = 0;
  double seff=0.;
  double alpha=getAlphaGc();
  double effGc=effGcII;

  if(snor>1.e-8*tau){
    seff = snor*snor/effsigcI/effsigcI+tau*tau/effsigcII/effsigcII;
    mixedratio=tau/snor;
    
    smax = traction;
    //smax = effsigcI*effsigcII*sqrt(1+mixedratio*mixedratio)/sqrt(effsigcII*effsigcII +mixedratio*mixedratio*effsigcI*effsigcI);
    ift = true;
    
    if (_cohesiveLaw->getCohesiveDamageLaw()->getType() == cohesiveDamageLaw::Bilinear)
    {
      effGc=(1.+mixedratio*mixedratio)*
           pow(1./pow(effGcI,alpha)+pow(mixedratio,2.*alpha)/pow(effGcII,alpha),-1./alpha);
    }
    else if (_cohesiveLaw->getCohesiveDamageLaw()->getType() == cohesiveDamageLaw::Exponential)
    {
      // critial value for each mode
      double DeltaCI = 2.*effGcI/effsigcI;
      double DeltaCII = 2.*effGcII/effsigcII;
      double penality = getKp();
                  // initial calue for each mode
      double Delta0I = effsigcI/penality;
      double Delta0II = effsigcII/penality;
      //
      if (Delta0I > 0.005*DeltaCI || Delta0II > 0.005*DeltaCII){
          double newValue = std::max(effsigcI/(0.005*DeltaCI), effsigcII/(0.05*DeltaCII));
          Msg::Error("penalty too small increasing K = from %e to a value larger than %e (initial jump should be smaller than 0.005*critical jump)",penality, newValue);
          Msg::Exit(0);
      }
      // 
      double JI = (DeltaCI-Delta0I)/(2*Delta0I);
      double JII = (DeltaCII-Delta0II)/(2*Delta0II);
      //
      double factG = mixedratio*mixedratio*(1.+2.*JII)/((1.+2.*JI));
      
      double effDIc =  (2.*sqrt(1+mixedratio*mixedratio)/smax)*pow(1./pow(effGcI,alpha)+pow(factG,alpha)/pow(effGcII,alpha),-1./alpha);
      double effDIIc = effDIc*mixedratio*(1.+2.*JII)/((1.+2.*JI));
      double effD = sqrt(effDIc*effDIc + effDIIc*effDIIc);
      effGc=0.5*smax*effD;   
      double xieff = (xiI+ mixedratio*mixedratio*xiII)/(1.+mixedratio*mixedratio);
      
      Cohesive3DIPVariable* coMinus = dynamic_cast<Cohesive3DIPVariable*>(fMinusCur->getIPvFrac());
      Cohesive3DIPVariable* coPlus =  dynamic_cast<Cohesive3DIPVariable*>(fPlusCur->getIPvFrac()) ;
      
      IPCohesive* ipCohesiveMinus = coMinus->getIPCohesive();
      IPCohesive* ipCohesivePlus = coPlus->getIPCohesive(); 
      
      IPExponentialCohesiveDamage* ipDamMinus = dynamic_cast<IPExponentialCohesiveDamage*>(ipCohesiveMinus->getRefToIPCohesiveDamage());
      IPExponentialCohesiveDamage* ipDamPlus = dynamic_cast<IPExponentialCohesiveDamage*>(ipCohesivePlus->getRefToIPCohesiveDamage());
      
      ipDamMinus->getRefToPowerParameter() = xieff;
      ipDamPlus->getRefToPowerParameter() = xieff;
    }
    else
    {
      Msg::Error("Missing case in DelaminationLinearCohesive3DLaw::checkCohesiveInsertion");
      Msg::Exit(0);
    }
  }
  else
  {
    seff = tau*tau/effsigcII/effsigcII;
    smax = tau;
    ift=false;
  }
  


  if((seff > 1.) or forcedInsert){
    #ifdef _DEBUG
    Msg::Error("broken with snor %e, tau %e, traction %d, damage %e", snor, tau,ift, damage);
    #endif // _DEBUG

    // activate cohesive element
    fMinusCur->broken();
    fPlusCur->broken();

    // initialize cohesive law in negative part
    if (seff > 1.){
      fMinusCur->getOnsetCriterion() = seff - 1.;
      fPlusCur->getOnsetCriterion() =  seff - 1.;
    }
    else if (forcedInsert){
      fMinusCur->getOnsetCriterion() = 0.;
      fPlusCur->getOnsetCriterion() = 0.;
    }

    // initialize cohesive law in negative part
    Cohesive3DIPVariableBase* coMinus = fMinusCur->getIPvFrac();
    coMinus->initializeFracture(fMinusCur->getConstRefToJump(),getKp(),smax,snor,tau,effGc,1.,ift,cauchyAverage,getKp(),normDir,force0,
                                fMinusCur->getIPvBulk());

    // positive part take a same value as negative cohesive part
    Cohesive3DIPVariableBase* coPlus = fPlusCur->getIPvFrac();
    coPlus->initializeFracture(fPlusCur->getConstRefToJump(),getKp(),smax,snor,tau,effGc,1.,ift,cauchyAverage,getKp(),normDir,force0,
                                fPlusCur->getIPvBulk());

    if (!this->getMacroSolver()->withFailureBasedOnPreviousState()){
      fMinusPrev->getIPvFrac()->operator =(*dynamic_cast<const IPVariable*>(coMinus));
      fPlusPrev->getIPvFrac()->operator =(*dynamic_cast<const IPVariable*>(coPlus));
    }
  }
  else{
    // no brocken
    fMinusCur->nobroken();
    fPlusCur->nobroken();
  }

};

GeneralBulkFollwedCohesive3DLaw::GeneralBulkFollwedCohesive3DLaw(const int num, const bool init):Cohesive3DLaw(num,init),
  _L(0.),_lostSolutionUniquenssTolerance(0.),_Kp(0.){};
GeneralBulkFollwedCohesive3DLaw::GeneralBulkFollwedCohesive3DLaw(const GeneralBulkFollwedCohesive3DLaw& src):
  Cohesive3DLaw(src),_L(src._L), _Kp(src._Kp),
	_lostSolutionUniquenssTolerance(src._lostSolutionUniquenssTolerance){}

void GeneralBulkFollwedCohesive3DLaw::setLostSolutionUniquenssTolerance(const double tol){
	_lostSolutionUniquenssTolerance = tol;
};


BulkFollwedCohesive3DLaw::BulkFollwedCohesive3DLaw(const int num): GeneralBulkFollwedCohesive3DLaw(num){};
void BulkFollwedCohesive3DLaw::createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_,
                                          const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  IPVariable *ipvi = new BulkFollowedCohesive3DIPVariable();
  IPVariable *ipv1 = new BulkFollowedCohesive3DIPVariable();
  IPVariable *ipv2 = new BulkFollowedCohesive3DIPVariable();
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void BulkFollwedCohesive3DLaw::createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  ipv = new BulkFollowedCohesive3DIPVariable();
}

void BulkFollwedCohesive3DLaw::checkCohesiveInsertion(IPStateBase* ipsm, IPStateBase* ipsp, const bool forcedInsert) const{
// check failure onset from both negative and postive IPStates
	FractureCohesive3DIPVariable* fMinusCur = dynamic_cast<FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::current));
	FractureCohesive3DIPVariable* fMinusPrev = dynamic_cast<FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::previous));
	FractureCohesive3DIPVariable* fPlusCur = dynamic_cast<FractureCohesive3DIPVariable*>(ipsp->getState(IPStateBase::current));
	FractureCohesive3DIPVariable* fPlusPrev = dynamic_cast<FractureCohesive3DIPVariable*>(ipsp->getState(IPStateBase::previous));

	if (fMinusCur == NULL or fPlusCur == NULL or fMinusPrev==NULL or fPlusPrev==NULL){
		Msg::Error("In MultiscaleCohesive3DLaw::initCohesiveLaw, FractureCohesive3DIPVariable is not used ");
	}

  if (fMinusPrev->isbroken() or fPlusPrev->isbroken()){
    Msg::Warning("Previous IPs are broken");
    fMinusCur->broken();
    fPlusCur->broken();
    return;
  }

  bool solverIsBrokenMinus = false;
  bool solverIsBrokenPlus = false;

  static SVector3 refN;
  refN = fMinusCur->getConstRefToReferenceOutwardNormal();
  refN.normalize();

  const STensor43& Lm = fMinusCur->getConstRefToTangentModuli();
  const STensor43& Lp = fPlusCur->getConstRefToTangentModuli();
  double detQm, detQp;

  FailureDetection::getLostSolutionUniquenessCriterionFollowingDirection(getMacroSolver()->getDim(),Lm,refN,detQm);
  FailureDetection::getLostSolutionUniquenessCriterionFollowingDirection(getMacroSolver()->getDim(),Lp,refN,detQp);



  if ((detQm < 0.) or (detQp <0.) or forcedInsert){
		printf("rank %d: cohesive element is inserted detQ= %e, detQp = %e \n",Msg::GetCommRank(),detQm,detQp);
    fMinusCur->broken();
    fPlusCur->broken();
    // broken is checked via bulk terial law, see function stress
    // initialize cohesive law in negative part
    static SVector3 zeroVec(0.);
    static STensor3 zeroTen(0.);

    if ((detQm < 0.) or (detQp <0.)){
      fMinusCur->getOnsetCriterion() = std::max(-detQm,-detQp);
      fPlusCur->getOnsetCriterion() = std::max(-detQm,-detQp);
    }
    else if (forcedInsert){
      fMinusCur->getOnsetCriterion() = 0;
      fPlusCur->getOnsetCriterion() = 0.;
    }

    Cohesive3DIPVariableBase* coMinus = fMinusCur->getIPvFrac();
    coMinus->initializeFracture(fMinusCur->getConstRefToJump(),getKp(),0.,0.,0.,0.,0.,true,zeroTen,getKp(),refN,zeroVec,
                              fMinusCur->getIPvBulk());
      // positive part take a same value as negative cohesive part
    Cohesive3DIPVariableBase* coPlus = fPlusCur->getIPvFrac();
    coPlus->initializeFracture(fPlusCur->getConstRefToJump(),getKp(),0.,0.,0.,0.,0.,true,zeroTen,getKp(),refN,zeroVec,
                              fPlusCur->getIPvBulk());

    if (!this->getMacroSolver()->withFailureBasedOnPreviousState()){
      fMinusPrev->getIPvFrac()->operator =(*dynamic_cast<const IPVariable*>(coMinus));
      fPlusPrev->getIPvFrac()->operator =(*dynamic_cast<const IPVariable*>(coPlus));
    }
  }
  else{
    fMinusCur->nobroken();
    fPlusCur->nobroken();
  }
};

void BulkFollwedCohesive3DLaw::transferInterfaceDataToBulk(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff) const{
	FractureCohesive3DIPVariable *fipv = static_cast < FractureCohesive3DIPVariable *> (ipv);
  const FractureCohesive3DIPVariable *fipvprev = static_cast < const FractureCohesive3DIPVariable *> (ipvprev);

  if (fipv->isbroken()){
    dG3DIPVariableBase* ipvbulk = dynamic_cast<dG3DIPVariableBase*>(fipv->getIPvBulk());
    const dG3DIPVariableBase* ipvbulkprev = dynamic_cast<const dG3DIPVariableBase*>(fipvprev->getIPvBulk());

    BulkFollowedCohesive3DIPVariable* cohIpv = dynamic_cast<BulkFollowedCohesive3DIPVariable*>(fipv->getIPvFrac());
    const BulkFollowedCohesive3DIPVariable* cohIpvprev = dynamic_cast<const BulkFollowedCohesive3DIPVariable*>(fipvprev->getIPvFrac());

    static SVector3 refN;
    refN = fipv->getConstRefToReferenceOutwardNormal();
    refN.normalize();

    SVector3& ujumpDG = cohIpv->getRefToDGJump();
    ujumpDG = cohIpvprev->getConstRefToDGJump();

    // update bulk part
    const STensor3& Fb0 = fipvprev->getBulkDeformationGradient();
    const STensor3& Fb = fipv->getBulkDeformationGradient();

    const SVector3& ujump = fipv->getConstRefToJump();
    const SVector3& ujump0 = fipvprev->getConstRefToJump();

    STensor3& F = fipv->getRefToDeformationGradient();
    F = fipvprev->getConstRefToDeformationGradient();
    for (int i=0; i<3; i++){
			for (int j=0; j<3; j++){
        F(i,j) += Fb(i,j) - Fb0(i,j)+ (ujump(i) - ujump0(i))*refN(j)/_L;
			}
		}

    if (stiff){
      STensor43& dFinterfacedF = fipv->getRefToDInterfaceDeformationGradientDDeformationGradient();
      STensor33& dFinterfacedJump = fipv->getRefToDInterfaceDeformationGradientDJump();

      STensorOperation::zero(dFinterfacedF);
      STensorOperation::zero(dFinterfacedJump);
      static STensor3 I(1.);
			for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){
            dFinterfacedJump(i,j,k) += I(i,k)*refN(j)/_L;
            for (int l=0; l<3; l++){
              dFinterfacedF(i,j,k,l) += I(i,k)*I(j,l);
            }
          }

        }
      }
		}

  }
};

void BulkFollwedCohesive3DLaw::stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff, const bool checkfrac, const bool dTangent){
  FractureCohesive3DIPVariable *fipv = static_cast < FractureCohesive3DIPVariable *> (ipv);
  const FractureCohesive3DIPVariable *fipvprev = static_cast < const FractureCohesive3DIPVariable *> (ipvprev);

  if (fipv->isbroken()){

    dG3DIPVariableBase* mipv = static_cast<dG3DIPVariableBase*>(fipv->getIPvBulk());
    const dG3DIPVariableBase* mipvprev = static_cast<const dG3DIPVariableBase*>(fipvprev->getIPvBulk());


    BulkFollowedCohesive3DIPVariable* cohmipv = dynamic_cast<BulkFollowedCohesive3DIPVariable*>(fipv->getIPvFrac());
    const BulkFollowedCohesive3DIPVariable* cohmipvprev = dynamic_cast<const BulkFollowedCohesive3DIPVariable*>(fipvprev->getIPvFrac());


    static SVector3 refN;
    refN = fipv->getConstRefToReferenceOutwardNormal();
    refN.normalize();
    static SVector3 curN;
    curN = fipvprev->getConstRefToCurrentOutwardNormal();
    curN.normalize();

    const STensor3& P = fipv->getConstRefToFirstPiolaKirchhoffStress();
    SVector3& T = fipv->getRefToInterfaceForce();
    STensorOperation::zero(T);
    for (int i=0; i<3; i++) {
      for (int j=0; j<3; j++){
        T(i) += P(i,j)*refN(j);
      }
    }

    const SVector3& ujump = fipv->getConstRefToJump();
		const SVector3& ujumpDG = fipv->getConstRefToDGJump();

    static SVector3 realUJump;
    realUJump=ujump;
    realUJump -= ujumpDG;
    // add penalty to avoid penetration
    double realUJumpNormal = dot(realUJump,curN);
    if (realUJumpNormal < 0){
      for (int i=0; i<3; i++){
				T(i) += _Kp*realUJumpNormal*curN(i);
			}
    }

		double& irr = fipv->getRefToIrreversibleEnergy();
		irr = mipv->irreversibleEnergy()*_L;

    if (stiff){

			const STensor43& dFinterfacedF = fipv->getConstRefToDInterfaceDeformationGradientDDeformationGradient();
			const STensor33& dFinterfacedJump = fipv->getConstRefToDInterfaceDeformationGradientDJump();

      const STensor43& dPdFinf = fipv->getConstRefToTangentModuli();
      STensor33& dTdF = fipv->getRefToDInterfaceForceDDeformationGradient();
      STensor3& dTdjump = fipv->getRefToDInterfaceForceDjump();

      STensorOperation::zero(dTdF);
      STensorOperation::zero(dTdjump);


			for (int i=0; i<3; i++){
				for (int j=0; j<3; j++){
					for (int p=0; p<3; p++){
						for (int q=0; q<3; q++){
							for (int k=0; k<3; k++){
								dTdjump(i,k) += dPdFinf(i,j,p,q)*refN(j)*dFinterfacedJump(p,q,k);
								for (int l=0; l<3; l++){
									dTdF(i,k,l) += dPdFinf(i,j,p,q)*refN(j)*dFinterfacedF(p,q,k,l);
								}
							}
						}
					}
				}
			}

			if (realUJumpNormal < 0){
				for (int i=0; i<3; i++){
					for (int j=0; j<3; j++){
						dTdjump(i,j) += _Kp*curN(i)*curN(j);
					};
				}
			};


      STensor3& dirrEnergDF = fipv->getRefToDIrreversibleEnergyDDeformationGradient();
      SVector3& dirrEnergDJump = fipv->getRefToDIrreversibleEnergyDJump();

      STensorOperation::zero(dirrEnergDF);
      STensorOperation::zero(dirrEnergDJump);

      const STensor3& dirrEnergdFinf = mipv->getConstRefToDIrreversibleEnergyDDeformationGradient();
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){
            dirrEnergDJump(k) += _L*dirrEnergdFinf(i,j)*dFinterfacedJump(i,j,k);
            for (int l=0; l<3; l++){
              dirrEnergDF(k,l) += _L*dirrEnergdFinf(i,j)*dFinterfacedF(i,j,k,l);
            }
          }

        }
      }
    }
  }
};
