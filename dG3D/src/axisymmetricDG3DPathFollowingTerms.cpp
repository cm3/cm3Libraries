//
//
// Description: pathfollowing terms
//
// Author:  <Van Dung NGUYEN>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "axisymmetricDG3DPathFollowingTerms.h"
#include "highOrderTensor.h"

void axisymmetricDG3DDissipationPathFollowingBulkScalarTerm::get(MElement *ele, int npts, IntPt *GP, double &val) const{
	val = 0.;
	const AllIPState::ipstateElementContainer *vips = _ipf->getAips()->getIPstate(ele->getNum());
  SPoint3 point; 
	for (int i = 0; i < npts; i++){
    
		const dG3DIPVariableBase* ipv = dynamic_cast<const dG3DIPVariableBase*>((*vips)[i]->getState(IPStateBase::current));
		const dG3DIPVariableBase* ipvprev = dynamic_cast<const dG3DIPVariableBase*>((*vips)[i]->getState(IPStateBase::previous));
		
		double weight = GP[i].weight;
		double detJ = ipv->getJacobianDeterminant(ele,GP[i]);
    ele->pnt(GP[i].pt[0],GP[i].pt[1],GP[i].pt[2],point);
    
    double ratio = 2.*M_PI*point[0]*detJ*weight;
		double damageDisp = (ipv->irreversibleEnergy() - ipvprev->irreversibleEnergy());
		val += (damageDisp*ratio);
	};
};

void axisymmetricDG3DDissipationPathFollowingBulkLinearTerm::get(MElement *ele,int npts,IntPt *GP,fullVector<double> &m) const{
	const FunctionSpaceBase* sp = _dom->getFunctionSpace();
	
	int nbFF = ele->getNumShapeFunctions();
	int nbdof = sp->getNumKeys(ele);
	m.resize(nbdof);
	m.setAll(0.);
  
  fullMatrix<double> dFdq;
  dFdq.resize(9,3*nbFF);
  SPoint3 point; 
	
	const AllIPState::ipstateElementContainer *vips = _ipf->getAips()->getIPstate(ele->getNum());
	
	for (int i = 0; i < npts; i++)
	{
		const dG3DIPVariableBase* ipv = dynamic_cast<const dG3DIPVariableBase*>((*vips)[i]->getState(IPStateBase::current));
		
		double weight = GP[i].weight;
		const double & detJ = ipv->getJacobianDeterminant(ele,GP[i]);
    
    ele->pnt(GP[i].pt[0],GP[i].pt[1],GP[i].pt[2],point);
    double ratio = 2.*M_PI*point[0]*detJ*weight;
    
    std::vector<TensorialTraits<double>::ValType> &Vals = ipv->f(sp,ele,GP[i]);
    std::vector<TensorialTraits<double>::GradType> &Grads = ipv->gradf(sp,ele,GP[i]);
    
    dFdq.setAll(0.);
    
    for (int j=0; j< nbFF; j++){
      // comp rr
      int row= Tensor23::getIndex(0,0);
      dFdq(row,j) = Grads[j+0*nbFF](0);
      // comp rz
      row= Tensor23::getIndex(0,2);
      dFdq(row,j) = Grads[j+0*nbFF](2);
      // comp tt
      row =Tensor23::getIndex(1,1);
      dFdq(row,j) = Vals[j+0*nbFF]/point[0];

      // comp zr
      row =Tensor23::getIndex(2,0);
      dFdq(row,j+2*nbFF) = Grads[j+2*nbFF](0);
      
      // comp zz
      row =Tensor23::getIndex(2,2);
      dFdq(row,j+2*nbFF) = Grads[j+2*nbFF](2);
    }
      
		
		const STensor3& DdissDF = ipv->getConstRefToDIrreversibleEnergyDDeformationGradient();
    for (int p=0; p< 3*nbFF; p++){
      for (int q=0; q<9; q++){
        int r,s;
        Tensor23::getIntsFromIndex(q,r,s);
        m(p) += ratio*DdissDF(r,s)*dFdq(q,p);
      }
    }
    
    // plastic energy does not depend on non-local variable  
		int numNonLocalVar = ipv->getNumberNonLocalVariable();
		for (int inl =0; inl < numNonLocalVar; inl++){
			const double & DdamEnergyDNonLocalVar = ipv->getConstRefToDIrreversibleEnergyDNonLocalVariable(inl);
			for (int k=0; k< nbFF; k++){
				m(k+inl*nbFF+3*nbFF) += DdamEnergyDNonLocalVar*Vals[k+ inl*nbFF+3*nbFF]*ratio;
			}
		}
    
	}
};
