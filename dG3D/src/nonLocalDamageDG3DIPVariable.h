//
// C++ Interface: ipvariable
//
// Description: Class with definition of ipvarible for dG3D
//
//
// Author:  <L. Noels>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef NONLOCALDAMAGEDG3DIPVARIABLE_H_
#define NONLOCALDAMAGEDG3DIPVARIABLE_H_
#include "dG3DIPVariable.h"
#include "ipNonLocalDamageJ2Hyper.h"
#include "ipNonLocalDamageIsotropicElasticity.h"
#include "ipNonLocalPorosity.h"
#include "mlawNonLocalDamageJ2Hyper.h"
#include "mlawNonLocalDamageJ2SmallStrain.h"
#include "mlawNonLocalDamageIsotropicElasticity.h"
#include "mlawNonLocalPorous.h"
#include "mlawHyperelasticDamage.h"
#include "mlawGenericCrackPhaseField.h"
#include "mlawNonLocalDamageJ2FullyCoupledThermoMechanics.h"
#include "ipNonLocalDamage.h"
#include "ipNonLocalDamageJ2FullyCoupledThermoMechanics.h"
#include "mlawNonLocalPorousWithFailure.h"


class virtualNonLocalDamageDG3DIPVariable : public dG3DIPVariableBase{
  protected:
    nonlocalData _nonlocalData; // al zero
    dG3DIPVariableBase* _ipvBase;
    std::vector<double> localVar;
    std::vector<double> nonLocalVar;
    const std::vector<STensor3>& characteristicLength;
    std::vector<double> DirrEnergDNonlocalVar;
    constitutiveExtraDofDiffusionData *_constitutiveExtraDofDiffusionData;
    std::vector<double> constitutiveExtraDofVar;
    std::vector<double> DirrEnergDConstitutiveExtraDofDiffusion;
    constitutiveCurlData * _constitutiveCurlData;
    bodyForceForHO       *_bodyForceForHO;

  public:
    virtualNonLocalDamageDG3DIPVariable(const int num, const dG3DIPVariableBase* base, const std::vector<STensor3>& length, int numExtra=0, bool createBodyForceHO=false);
    virtualNonLocalDamageDG3DIPVariable(const virtualNonLocalDamageDG3DIPVariable& src);
    virtual virtualNonLocalDamageDG3DIPVariable& operator =(const IPVariable& src);
    virtual ~virtualNonLocalDamageDG3DIPVariable();

    dG3DIPVariableBase* getIPVBase() {return _ipvBase;}
    const dG3DIPVariableBase* getIPVBase() const {return _ipvBase;}

    virtual IPVariable* getInternalData() {return _ipvBase->getInternalData();};
    virtual const IPVariable* getInternalData()  const {return _ipvBase->getInternalData();};

    #if defined(HAVE_MPI)
    // by default in MPI there is no value that are communicate between rank.
    // but you can communicate values by filling these functions for your IPVariable
    virtual int numberValuesToCommunicateMPI()const{return _ipvBase->numberValuesToCommunicateMPI();}
    virtual void fillValuesToCommunicateMPI(double *arrayMPI)const{_ipvBase->fillValuesToCommunicateMPI(arrayMPI);};
    virtual void getValuesFromMPI(const double *arrayMPI){_ipvBase->getValuesFromMPI(arrayMPI);};
    #endif // HAVE_MPI

    virtual void Delete(const bool fl){
      dG3DIPVariableBase::Delete(fl);
      _ipvBase->Delete(fl);
    }
    virtual void setLocation(const IPVariable::LOCATION loc){
      dG3DIPVariableBase::setLocation(loc);
      _ipvBase->setLocation(loc);
    };
    virtual void setMicroSolver(nonLinearMechSolver* s) {
      dG3DIPVariableBase::setMicroSolver(s);
      _ipvBase->setMicroSolver(s);
    };

    virtual bool dissipationIsActive() const {return _ipvBase->dissipationIsActive();};

    virtual void blockDissipation(const bool bl){
      dG3DIPVariableBase::blockDissipation(bl);
      _ipvBase->blockDissipation(bl);
    }
    virtual bool dissipationIsBlocked() const {return _ipvBase->dissipationIsBlocked();}

    virtual int fractureEnergy(double* arrayEnergy) const{return _ipvBase->fractureEnergy(arrayEnergy);}

    virtual const bool isInterface() const {return _ipvBase->isInterface();};

    // Archiving data
    virtual double get(const int i) const;
    virtual double defoEnergy() const {return _ipvBase->defoEnergy();};
    virtual double plasticEnergy() const {return _ipvBase->plasticEnergy();};
    virtual double damageEnergy() const {return _ipvBase->damageEnergy();};
    virtual double vonMises() const {return _ipvBase->vonMises();};
    virtual void getCauchyStress(STensor3 &cauchy) const {_ipvBase->getCauchyStress(cauchy);};
    virtual double getJ(int dim=3) const {return _ipvBase->getJ(dim);};

    virtual double getBarJ() const {return _ipvBase->getBarJ();};
    virtual double &getRefToBarJ() {return _ipvBase->getRefToBarJ();};
    virtual double getLocalJ() const {return _ipvBase->getLocalJ();};
    virtual double &getRefToLocalJ() {return _ipvBase->getRefToLocalJ();};
    virtual void getBackLocalDeformationGradient(STensor3& localF) const 
                   {_ipvBase->getBackLocalDeformationGradient(localF);};

    virtual const STensor3 &getConstRefToDeformationGradient() const {return _ipvBase->getConstRefToDeformationGradient();};
    virtual STensor3 &getRefToDeformationGradient() {return _ipvBase->getRefToDeformationGradient();};

    virtual const STensor3 &getConstRefToFirstPiolaKirchhoffStress() const{return _ipvBase->getConstRefToFirstPiolaKirchhoffStress();};
    virtual STensor3 &getRefToFirstPiolaKirchhoffStress() {return _ipvBase->getRefToFirstPiolaKirchhoffStress();};

    virtual const STensor43 &getConstRefToTangentModuli() const {return _ipvBase->getConstRefToTangentModuli();};
    virtual STensor43 &getRefToTangentModuli() {return _ipvBase->getRefToTangentModuli();};

     virtual const STensor43 &getConstRefToElasticTangentModuli() const {return _ipvBase->getConstRefToElasticTangentModuli();};
    virtual STensor43 &getRefToElasticTangentModuli() {return _ipvBase->getRefToElasticTangentModuli();};

    virtual const STensor43 &getConstRefToDGElasticTangentModuli() const {return _ipvBase->getConstRefToDGElasticTangentModuli();};
    virtual void setRefToDGElasticTangentModuli(const STensor43 &eT) {_ipvBase->setRefToDGElasticTangentModuli(eT);};

    virtual const SVector3 &getConstRefToReferenceOutwardNormal() const {return _ipvBase->getConstRefToReferenceOutwardNormal();};
    virtual SVector3 &getRefToReferenceOutwardNormal() {return _ipvBase->getRefToReferenceOutwardNormal();};

    virtual const SVector3 &getConstRefToCurrentOutwardNormal() const {return _ipvBase->getConstRefToCurrentOutwardNormal();};
    virtual SVector3 &getRefToCurrentOutwardNormal() {return _ipvBase->getRefToCurrentOutwardNormal();};

    virtual const SVector3 &getConstRefToJump() const {return _ipvBase->getConstRefToJump();};
    virtual SVector3 &getRefToJump() {return _ipvBase->getRefToJump();};

    // non local
    virtual int getNumberNonLocalVariable() const {return _nonlocalData.numNonLocalVariable;};
    //
    virtual double getConstRefToLocalVariable(const int idex) const {return localVar[idex];};
    virtual double& getRefToLocalVariable(const int idex) {return localVar[idex];};;
    //
    virtual double getConstRefToNonLocalVariableInMaterialLaw(const int idex) const {return nonLocalVar[idex];};
    virtual double& getRefToNonLocalVariableInMaterialLaw(const int idex) {return nonLocalVar[idex];};
    //
    virtual double getConstRefToNonLocalVariable(const int idex) const {return _nonlocalData.nonLocalVariable(idex);};
    virtual double& getRefToNonLocalVariable(const int idex) {return _nonlocalData.nonLocalVariable(idex);};
    //
    virtual const std::vector<STensor3> &getConstRefToDLocalVariableDStrain() const {return _nonlocalData.dLocalVariableDStrain;}
    virtual std::vector<STensor3> &getRefToDLocalVariableDStrain() {return _nonlocalData.dLocalVariableDStrain;}


    virtual const std::vector<STensor3> &getConstRefToDStressDNonLocalVariable() const {return _nonlocalData.dStressDNonLocalVariable;}
    virtual std::vector<STensor3> &getRefToDStressDNonLocalVariable() {return _nonlocalData.dStressDNonLocalVariable;}

    virtual const fullMatrix<double> &getConstRefToDLocalVariableDNonLocalVariable() const {return _nonlocalData.dLocalVariableDNonLocalVariable;}
    virtual fullMatrix<double> &getRefToDLocalVariableDNonLocalVariable() {return _nonlocalData.dLocalVariableDNonLocalVariable;}

    virtual const fullVector<double> &getConstRefToNonLocalJump() const {return _nonlocalData.nonLocalJump;}
    virtual fullVector<double> &getRefToNonLocalJump() {return _nonlocalData.nonLocalJump;}

    virtual const std::vector<SVector3> &getConstRefToGradNonLocalVariable() const {return _nonlocalData.gradNonLocalVariable;}
    virtual std::vector<SVector3> &getRefToGradNonLocalVariable() {return _nonlocalData.gradNonLocalVariable;}
    //coupling non local with constitutiveExtraDof
    virtual const fullMatrix<double> & getConstRefTodLocalVariableDExtraDofDiffusionField() const {return _nonlocalData.dLocalVariableDExtraDofDiffusionField;}
    virtual fullMatrix<double> & getRefTodLocalVariableDExtraDofDiffusionField(){return _nonlocalData.dLocalVariableDExtraDofDiffusionField;}
  // coupling non local with curlData
  virtual const std::vector<std::vector< SVector3 > > & getConstRefTodLocalVariabledVectorPotential() const
  {
      return _nonlocalData.dLocalVariabledVectorPotential;
  }
  virtual std::vector<std::vector< SVector3 > > & getRefTodLocalVariabledVectorPotential()
  {
        return _nonlocalData.dLocalVariabledVectorPotential;
  }
  virtual const std::vector<std::vector< SVector3 > > & getConstRefTodLocalVariabledVectorCurl() const
  {
    return _nonlocalData.dLocalVariabledVectorCurl;
  }
  virtual std::vector<std::vector< SVector3 > > & getRefTodLocalVariabledVectorCurl()
  {
    return _nonlocalData.dLocalVariabledVectorCurl;
  }

    virtual const STensor3 &getConstRefToCharacteristicLengthMatrix(const int idx) const {return characteristicLength[idx];};

    //-----------------get 1st piola before AV ------------//

    virtual const STensor3 &getConstRefToFirstPiolaKirchhoffStressb4AV() const {return _ipvBase->getConstRefToFirstPiolaKirchhoffStressb4AV();};
    virtual STensor3 &getRefToFirstPiolaKirchhoffStressb4AV() {return _ipvBase->getRefToFirstPiolaKirchhoffStressb4AV();};

    virtual void setNonLocalToLocal(const bool fl){_ipvBase->setNonLocalToLocal(fl);};
    virtual bool getNonLocalToLocal() const {return _ipvBase->getNonLocalToLocal();};

    // for multiple field formulation
    // incompatible jump
    virtual const SVector3& getConstRefToIncompatibleJump() const {return _ipvBase->getConstRefToIncompatibleJump();};
    virtual SVector3& getRefToIncompatibleJump() {return _ipvBase->getRefToIncompatibleJump();};

    // for path following based on  irreversibe energt
    virtual double irreversibleEnergy() const {return _ipvBase->irreversibleEnergy();};
    virtual double& getRefToIrreversibleEnergy() {return _ipvBase->getRefToIrreversibleEnergy();};

    virtual const STensor3& getConstRefToDIrreversibleEnergyDDeformationGradient() const {return _ipvBase->getConstRefToDIrreversibleEnergyDDeformationGradient();};
    virtual STensor3& getRefToDIrreversibleEnergyDDeformationGradient() {return _ipvBase->getRefToDIrreversibleEnergyDDeformationGradient();};

    // irreversible ennergy depend on nonlocal variale
    virtual const double& getConstRefToDIrreversibleEnergyDNonLocalVariable(const int index) const {return DirrEnergDNonlocalVar[index];};
    virtual double& getRefToDIrreversibleEnergyDNonLocalVariable(const int index) {return DirrEnergDNonlocalVar[index];};

    // irreversible ennergy depend on extra dof
    virtual const double& getConstRefToDIrreversibleEnergyDConstitutiveExtraDofDiffusion(const int index) const {return DirrEnergDConstitutiveExtraDofDiffusion[index];};
    virtual double& getRefToDIrreversibleEnergyDConstitutiveExtraDofDiffusion(const int index) {return DirrEnergDConstitutiveExtraDofDiffusion[index];};

    virtual IPVariable* clone() const {return new virtualNonLocalDamageDG3DIPVariable(*this);};

    virtual void restart();
  // higher order body force
  virtual bool hasBodyForceForHO() const
  {
    if(_bodyForceForHO==NULL)
      return false;
    else
      return true; 
  }  
  virtual const STensor33 &getConstRefToGM() const
  {
    if(hasBodyForceForHO())
      return _bodyForceForHO->GM;
    else
    {
      Msg::Error("getConstRefToGM is not defined");
      static STensor33 VoidGM;
      return VoidGM;
    }
  }  
  virtual STensor33 &getRefToGM()
  {
    if(hasBodyForceForHO())
      return _bodyForceForHO->GM;
    else
    {
      Msg::Error("getRefToGM is not defined");
      static STensor33 VoidGM;
      return VoidGM;
    }
  }  
  
  virtual const STensor43 &getConstRefToDFmdFM() const
  {
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM;
    else
      Msg::Error("getConstRefToDFmdFM is not defined");
    static STensor43 VoidGM;
    return VoidGM;
  }  
  virtual STensor43 &getRefToDFmdFM()
  {
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdFM;
    else
    {
      Msg::Error("getRefToDFmdFM is not defined");
      static STensor43 VoidGM;
      return VoidGM;
    }
  }  
  
  virtual const STensor53 &getConstRefToDFmdGM() const
  {
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdGM;
    else
      Msg::Error("getConstRefToDFmdGM is not defined");
    static STensor53 VoiddFmdGM;
    return VoiddFmdGM;
  }  

  virtual STensor53 &getRefToDFmdGM()
  {
    if(hasBodyForceForHO())
      return _bodyForceForHO->dFmdGM;
    else
    {
      Msg::Error("getRefToDFmdGM is not defined");
      static STensor53 VoiddFmdGM;
      return VoiddFmdGM;
    }
  }  

  virtual const SVector3 &getConstRefToBm() const
  {
    if(hasBodyForceForHO())
      return _bodyForceForHO->Bm;
    else
    {
      Msg::Error("getConstRefToBm is not defined");
      static SVector3 VoidBm;
      return VoidBm;
    }
  }  
virtual SVector3 &getRefToBm()
  {
    if(hasBodyForceForHO())
      return _bodyForceForHO->Bm;
    else
    {
      Msg::Error("getRefToBm is not defined");
      static SVector3 VoidBm;
      return VoidBm;
    }
  }  

virtual const STensor43 &getConstRefTodBmdGM() const
  {
    if(hasBodyForceForHO())
      return _bodyForceForHO->dBmdGM;
    else{
      Msg::Error("getConstRefTodBmdGM is not defined");
    static STensor43 VoiddBmdGM;
    return VoiddBmdGM;
    }
  }  
  virtual STensor43 &getRefTodBmdGM()
  {
    if(hasBodyForceForHO())
      return _bodyForceForHO->dBmdGM;
    else{
      Msg::Error("getConstRefTodBmdGM is not defined");
      static STensor43 VoiddBmdGM;
      return VoiddBmdGM;
    }
  }    


virtual const STensor33 &getConstRefTodBmdFm() const
  {
    if(hasBodyForceForHO())
      return _bodyForceForHO->dBmdFm;
    else
    {
      Msg::Error("getConstRefTodBmdFm is not defined");
      static STensor33 VoiddBmdFm;
      return VoiddBmdFm;
    }
  }  
virtual STensor33 &getRefTodBmdFm()
  {
    if(hasBodyForceForHO())
      return _bodyForceForHO->dBmdFm;
    else
    {
      Msg::Error("getRefTodBmdFm is not defined");
      static STensor33 VoiddBmdFm;
      return VoiddBmdFm;
    }
  }  

  
virtual const STensor43 &getConstRefTodPmdFM() const
  {
    if(hasBodyForceForHO())
      return _bodyForceForHO->dPmdFM;
    else
    {
      Msg::Error("getConstRefTodPmdFM is not defined");
      static STensor43 VoiddPmdFM;
      return VoiddPmdFM;
    }
  }  
virtual STensor43 &getRefTodPmdFM()
  {
    if(hasBodyForceForHO())
      return _bodyForceForHO->dPmdFM;
    else
    {
      Msg::Error("getRefTodPmdFM is not defined");
      static STensor43 VoiddPmdFM;
      return VoiddPmdFM;
    }
  }
  
virtual const STensor63 &getConstRefTodCmdFm() const
  {
    if(hasBodyForceForHO())
      return _bodyForceForHO->dCmdFm;
    else
    {
      Msg::Error("getConstRefTodCmdFm is not defined");
      static STensor63 VoiddCmdFm;
      return VoiddCmdFm;
    }
  }  
virtual STensor63 &getRefTodCmdFm()
  {
    if(hasBodyForceForHO())
      return _bodyForceForHO->dCmdFm;
    else
    {
      Msg::Error("getRefTodCmdFm is not defined");
      static STensor63 VoiddCmdFm;
      return VoiddCmdFm;
    }
  }  
virtual STensor63 *getPtrTodCmdFm()
  {
    if(hasBodyForceForHO())
      return &(_bodyForceForHO->dCmdFm);
    else
      return NULL;
  }

  // extradof
  virtual const std::vector<std::vector<std::vector<STensor3> > >  &getConstRefTodLinearKdField() const {return _ipvBase->getConstRefTodLinearKdField();}; 
  virtual std::vector<std::vector<std::vector<STensor3> > >  &getRefTodLinearKdField() {return _ipvBase->getRefTodLinearKdField();}; 
  virtual const std::vector<std::vector<STensor43>>  &getConstRefTodLinearKdF() const {return _ipvBase->getConstRefTodLinearKdF();}; 
  virtual std::vector<std::vector<STensor43>>  &getRefTodLinearKdF() {return _ipvBase->getRefTodLinearKdF();}; 
/*  virtual const std::vector<std::vector<std::vector<STensor3> > >  &getConstRefTodEnergyKdField() const {return _ipvBase->getConstRefTodEnergyKdField();}; 
  virtual std::vector<std::vector<std::vector<STensor3> > >  &getRefTodEnergyKdField() {return _ipvBase->getRefTodEnergyKdField();}; 
  virtual const std::vector<std::vector<STensor43>>  &getConstRefTodEnergyKdF() const {return _ipvBase->getConstRefTodEnergyKdF();}; 
  virtual std::vector<std::vector<STensor43>>  &getRefTodEnergyKdF() {return _ipvBase->getRefTodEnergyKdF();}; */
  virtual const std::vector<std::vector<std::vector<STensor3> > >  &getConstRefTodFluxdGradFielddField() const {return _ipvBase->getConstRefTodFluxdGradFielddField();}; 
  virtual std::vector<std::vector<std::vector<STensor3> > >  &getRefTodFluxdGradFielddField() {return _ipvBase->getRefTodFluxdGradFielddField();}; 
  virtual const std::vector<std::vector<STensor43>>  &getConstRefTodFluxdGradFielddF() const {return _ipvBase->getConstRefTodFluxdGradFielddF();}; 
  virtual std::vector<std::vector<STensor43>>  &getRefTodFluxdGradFielddF() {return _ipvBase->getRefTodFluxdGradFielddF();};
  virtual int getNumConstitutiveExtraDofDiffusionVariable() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      return 0;
    else
      return _constitutiveExtraDofDiffusionData->getConstitutiveExtraDofDiffusionVariable();
  }
  virtual double getConstRefToField(const int idex) const {return constitutiveExtraDofVar[idex];};
  virtual double &getRefToField(const int idex) {return constitutiveExtraDofVar[idex];};
  virtual const std::vector<SVector3>  &getConstRefToGradField() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefToGradField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->gradField;
  }
  virtual std::vector<SVector3>  &getRefToGradField()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefToGradField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->gradField;
  }
  virtual const std::vector<SVector3>  &getConstRefToFlux() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefToFlux() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->flux;
  }
  virtual std::vector<SVector3>  &getRefToFlux()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefToFlux() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->flux;
  }
  virtual const std::vector<SVector3> &getConstRefToElectricDisplacement() const
  {
      if(_constitutiveExtraDofDiffusionData==NULL)
          Msg::Error("getConstRefToElectricDisplacement() : _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->electricDisplacement;
  }
  virtual std::vector<SVector3> &getRefToElectricDisplacement()
  {
      if(_constitutiveExtraDofDiffusionData==NULL)
          Msg::Error("getRefToElectricDisplacement() : _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->electricDisplacement;
  }
  virtual const std::vector<STensor3>  &getConstRefTodPdField() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodPdField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dPdField;
  }
  virtual std::vector<STensor3>  &getRefTodPdField()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodPdField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dPdField;
  }
  virtual const std::vector<STensor33>  &getConstRefTodPdGradField() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)

      Msg::Error("getConstRefTodPdGradField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dPdGradField;
  }
  virtual std::vector<STensor33>  &getRefTodPdGradField()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)

      Msg::Error("getRefTodPdGradField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dPdGradField;
  }
  virtual const std::vector< std::vector<STensor3> >  &getConstRefTodFluxdGradField() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodFluxdGradField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFluxdGradField;
  }
  virtual std::vector<std::vector<STensor3> >  &getRefTodFluxdGradField()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodFluxdGradField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFluxdGradField;
  }
  virtual const std::vector<std::vector<SVector3> >  &getConstRefTodFluxdField() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodFluxdField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFluxdField;
  }
  virtual std::vector<std::vector<SVector3> >  &getRefTodFluxdField()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodFluxdField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFluxdField;
  }
  virtual const std::vector<STensor33>   &getConstRefTodFluxdF() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodFluxdF() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFluxdF;
  }
  virtual std::vector<STensor33>   &getRefTodFluxdF()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodFluxdF() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFluxdF;
  }
  virtual const std::vector< std::vector<SVector3> > &getConstRefTodElecDisplacementdField() const
  {
      if(_constitutiveExtraDofDiffusionData==NULL)
          Msg::Error("getConstRefTodElecDisplacementdField() : _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->dElecDisplacementdField;
  }
  virtual std::vector< std::vector<SVector3> > &getRefTodElecDisplacementdField()
  {
      if(_constitutiveExtraDofDiffusionData==NULL)
          Msg::Error("getRefTodElecDisplacementdField() : _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->dElecDisplacementdField;
  }
  virtual const std::vector< std::vector<STensor3> > &getConstRefTodElecDisplacementdGradField() const
  {
      if(_constitutiveExtraDofDiffusionData==NULL)
          Msg::Error("getConstRefTodElecDisplacementdGradField() : _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->dElecDisplacementdGradField;
  }
  virtual std::vector< std::vector<STensor3> > &getRefTodElecDisplacementdGradField()
  {
      if(_constitutiveExtraDofDiffusionData==NULL)
          Msg::Error("getRefTodElecDisplacementdGradField() : _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->dElecDisplacementdGradField;
  }
  virtual const std::vector<STensor33> &getConstRefTodElecDisplacementdF() const
  {
      if(_constitutiveExtraDofDiffusionData==NULL)
          Msg::Error("getConstRefTodElecDisplacementdF() : _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->dElecDisplacementdF;
  }
  virtual std::vector<STensor33> &getRefTodElecDisplacementdF()
  {
      if(_constitutiveExtraDofDiffusionData==NULL)
          Msg::Error("getRefTodElecDisplacementdF() : _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->dElecDisplacementdF;
  }
  virtual const std::vector<std::vector<const STensor3*> > &getConstRefToLinearK() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTolinearK() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->linearK;
  }
  virtual void setConstRefToLinearK(const STensor3 &eT, int i, int j)
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTolinearK() : _constitutiveExtraDofDiffusionData not created");
    else
    {
      if(i < getNumConstitutiveExtraDofDiffusionVariable() && j< getNumConstitutiveExtraDofDiffusionVariable())
         _constitutiveExtraDofDiffusionData->linearK[i][j] = &eT;
      else
         Msg::Error("getConstRefTolinearK() : index out of range");
    }
  }
/*  virtual const std::vector<std::vector<const STensor3*> > &getConstRefToEnergyK() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefToEnergyK() : _constitutiveExtraDofDiffusionData not created");
    else
      return _constitutiveExtraDofDiffusionData->EnergyK;
  }
  virtual void setConstRefToEnergyK(const STensor3 &eT, int i, int j)
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefToEnergyK() : _constitutiveExtraDofDiffusionData not created");
    else
    {
      if(i < getNumConstitutiveExtraDofDiffusionVariable() && j< getNumConstitutiveExtraDofDiffusionVariable())
         _constitutiveExtraDofDiffusionData->linearK[i][j] = &eT;
      else
         Msg::Error("getConstRefToEnergyK() : index out of range");
    }
  }*/
  virtual const fullVector<double>  &getConstRefToFieldSource() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefToFieldSource() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->fieldSource;
  }
  virtual fullVector<double>  &getRefToFieldSource()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefToFieldSource() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->fieldSource;
  }
  virtual const fullMatrix<double>  &getConstRefTodFieldSourcedField() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodFieldSourcedField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFieldSourcedField;
  }
  virtual fullMatrix<double>  &getRefTodFieldSourcedField()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodFieldSourcedField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFieldSourcedField;
  }
  virtual const std::vector<std::vector<SVector3> >  &getConstRefTodFieldSourcedGradField() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodFluxdField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFieldSourcedGradField;
  }
  virtual std::vector<std::vector<SVector3> >  &getRefTodFieldSourcedGradField()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodFieldSourcedGradField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFieldSourcedGradField;
  }
  virtual const std::vector<STensor3>  &getConstRefTodFieldSourcedF() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodFieldSourcedF() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFieldSourcedF;
  }
  virtual std::vector<STensor3>  &getRefTodFieldSourcedF()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodFieldSourcedF() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFieldSourcedF;
  }
  virtual const fullVector<double>  &getConstRefToMechanicalSource() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefToMechanicalSource() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->mechanicalSource;
  }
  virtual fullVector<double>  &getRefToMechanicalSource()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefToMechanicalSource() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->mechanicalSource;
  }
  virtual const fullMatrix<double>  &getConstRefTodMechanicalSourcedField() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodMechanicalSourcedField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dMechanicalSourcedField;
  }
  virtual fullMatrix<double>  &getRefTodMechanicalSourcedField()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodMechanicalSourcedField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dMechanicalSourcedField;
  }
  virtual const std::vector<std::vector<SVector3> >  &getConstRefTodMechanicalSourcedGradField() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodMechanicalSourcedGradField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dMechanicalSourcedGradField;
  }
  virtual std::vector<std::vector<SVector3> >  &getRefTodMechanicalSourcedGradField()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodMechanicalSourcedGradField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dMechanicalSourcedGradField;
  }
  virtual const std::vector<STensor3>  &getConstRefTodMechanicalSourcedF() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodMechanicalSourcedF() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dMechanicalSourcedF;
  }
  virtual std::vector<STensor3>  &getRefTodMechanicalSourcedF()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodMechanicalSourcedF() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dMechanicalSourcedF;
  }
  virtual const fullVector<double>  &getConstRefToExtraDofFieldCapacityPerUnitField() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefToExtraDofFieldCapacityPerUnitField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->fieldCapacityPerUnitField;
  }
  virtual fullVector<double>  &getRefToExtraDofFieldCapacityPerUnitField()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefToExtraDofFieldCapacityPerUnitField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->fieldCapacityPerUnitField;
  }
  virtual const fullVector<double> & getConstRefToEMFieldSource() const
  {
      if (_constitutiveExtraDofDiffusionData == NULL)
          Msg::Error("getConstRefToEMFieldSource : _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->emFieldSource;
  }
  virtual fullVector<double> & getRefToEMFieldSource()
  {
      if (_constitutiveExtraDofDiffusionData == NULL)
          Msg::Error("getRefToEMFieldSource : _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->emFieldSource;
  }
  virtual const fullMatrix<double> & getConstRefTodEMFieldSourcedField() const
  {
      if (_constitutiveExtraDofDiffusionData == NULL)
          Msg::Error("getConstRefTodEMFieldSourcedField : _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->demFieldSourcedField;
  }
  virtual fullMatrix<double> & getRefTodEMFieldSourcedField()
  {
      if (_constitutiveExtraDofDiffusionData == NULL)
          Msg::Error("getRefTodEMFieldSourcedField : _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->demFieldSourcedField;
  }
  virtual const std::vector< std::vector< SVector3 > > & getConstRefTodEMFieldSourcedGradField() const
  {
      if (_constitutiveExtraDofDiffusionData == NULL)
          Msg::Error("getConstRefTodEMFieldSourcedGradField : _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->demFieldSourcedGradField;
  }
  virtual std::vector< std::vector< SVector3 > > & getRefTodEMFieldSourcedGradField()
  {
      if (_constitutiveExtraDofDiffusionData == NULL)
          Msg::Error("getConstRefTodEMFieldSourcedGradField : _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->demFieldSourcedGradField;
  }
  virtual const fullVector<double>  &getConstRefToFieldJump() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefToFieldJump() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->fieldJump;
  }
  virtual fullVector<double>  &getRefToFieldJump()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefToFieldJump() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->fieldJump;
  }
  virtual const fullVector<double>  &getConstRefToOneOverFieldJump() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefToOneOverFieldJump() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->oneOverFieldJump;
  }
  virtual fullVector<double>  &getRefToOneOverFieldJump()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefToOneOverFieldJump() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->oneOverFieldJump;
  }
  virtual const fullMatrix<double>  &getConstRefTodOneOverFieldJumpdFieldm() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodOneOverFieldJumpdFieldm : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dOneOverFieldJumpdFieldm;
  }
  virtual fullMatrix<double>  &getRefTodOneOverFieldJumpdFieldm()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodOneOverFieldJumpdFieldm() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dOneOverFieldJumpdFieldm;
  }
  virtual const fullMatrix<double>  &getConstRefTodOneOverFieldJumpdFieldp() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodOneOverFieldJumpdFieldp : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dOneOverFieldJumpdFieldp;
  }
  virtual fullMatrix<double>  &getRefTodOneOverFieldJumpdFieldp()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodOneOverFieldJumpdFieldp() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dOneOverFieldJumpdFieldp;
  }
  virtual const std::vector<SVector3>  &getConstRefToInterfaceFlux() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefToInterfaceFlux : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->interfaceFlux;
  }
  virtual std::vector<SVector3>  &getRefToInterfaceFlux()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefToInterfaceFlux() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->interfaceFlux;
  }
  virtual const std::vector< const STensor3*> &getConstRefToLinearSymmetrizationCoupling() const 
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefToLinearSymmetrizationCoupling() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->linearSymmetrizationCoupling;
  }
  virtual void setConstRefToLinearSymmetrizationCoupling(const STensor3 &eT, int i) 
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("setConstRefToLinearSymmetrizationCoupling() : _constitutiveExtraDofDiffusionData not created");
    else
    { 
      if(i < getNumConstitutiveExtraDofDiffusionVariable())
         _constitutiveExtraDofDiffusionData->linearSymmetrizationCoupling[i] = &eT;
      else
         Msg::Error("setConstRefToLinearSymmetrizationCoupling() : index out of range");
    }
  }
  // coupling constitutive extra dof with curl data
  virtual const std::vector< std::vector< STensor3 > > & getConstRefTodFluxdVectorPotential() const
  {
      if (_constitutiveExtraDofDiffusionData==NULL)
          Msg::Error("getConstRefTodFluxdVectorPotential() : _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->dFluxdVectorPotential;
  }
  virtual const std::vector< std::vector< STensor3 > > & getConstRefTodFluxdVectorCurl() const
  {
    if (_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodFluxdVectorCurl() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFluxdVectorCurl;
  }
    virtual const std::vector< std::vector< SVector3 > > & getConstRefTodFieldSourcedVectorPotential() const
    {
        if (_constitutiveExtraDofDiffusionData==NULL)
            Msg::Error("getConstRefTodFieldSourcedVectorPotential() : _constitutiveExtraDofDiffusionData not created");
        return _constitutiveExtraDofDiffusionData->dFieldSourcedVectorPotential;
    }
    virtual const std::vector< std::vector< SVector3 > > & getConstRefTodMechanicalSourcedVectorPotential() const
    {
        if (_constitutiveExtraDofDiffusionData==NULL)
            Msg::Error("getConstRefTodMechanicalSourcedVectorPotential() : _constitutiveExtraDofDiffusionData not created");
        return _constitutiveExtraDofDiffusionData->dMechanicalSourcedVectorPotential;
    }
  virtual const std::vector< std::vector< SVector3 > > & getConstRefTodFieldSourcedVectorCurl() const
  {
    if (_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodFieldSourcedVectorCurl() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFieldSourcedVectorCurl;    
  }
  virtual const std::vector< std::vector< SVector3 > > & getConstRefTodMechanicalSourcedVectorCurl() const
  {
    if (_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodMechanicalSourcedVectorCurl() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dMechanicalSourcedVectorCurl;    
  }

    virtual std::vector< std::vector< STensor3 > > & getRefTodFluxdVectorPotential()
    {
        if (_constitutiveExtraDofDiffusionData==NULL)
            Msg::Error("getRefTodFluxdVectorPotential() : _constitutiveExtraDofDiffusionData not created");
        return _constitutiveExtraDofDiffusionData->dFluxdVectorPotential;
    }
  virtual std::vector< std::vector< STensor3 > > & getRefTodFluxdVectorCurl()
  {
    if (_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodFluxdVectorCurl() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFluxdVectorCurl;
  }
    virtual std::vector< std::vector< SVector3 > > & getRefTodFieldSourcedVectorPotential()
    {
        if (_constitutiveExtraDofDiffusionData==NULL)
            Msg::Error("getRefTodFieldSourcedVectorPotential() : _constitutiveExtraDofDiffusionData not created");
        return _constitutiveExtraDofDiffusionData->dFieldSourcedVectorPotential;
    }
    virtual std::vector< std::vector< SVector3 > > & getRefTodMechanicalSourcedVectorPotential()
    {
        if (_constitutiveExtraDofDiffusionData==NULL)
            Msg::Error("getRefTodMechanicalSourcedVectorPotential() : _constitutiveExtraDofDiffusionData not created");
        return _constitutiveExtraDofDiffusionData->dMechanicalSourcedVectorPotential;
    }
  virtual std::vector< std::vector< SVector3 > > & getRefTodFieldSourcedVectorCurl()
  {
    if (_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodFieldSourcedVectorCurl() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFieldSourcedVectorCurl;    
  }
  virtual std::vector< std::vector< SVector3 > > & getRefTodMechanicalSourcedVectorCurl()
  {
    if (_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodMechanicalSourcedVectorCurl() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dMechanicalSourcedVectorCurl;    
  }
  virtual const std::vector< std::vector< SVector3 > > & getConstRefTodEMFieldSourcedVectorPotential() const
  {
      if (_constitutiveExtraDofDiffusionData == NULL)
          Msg::Error("getConstRefTodEMFieldSourcedVectorPotential : _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->demFieldSourcedVectorPotential;
  }
  virtual std::vector< std::vector< SVector3 > > & getRefTodEMFieldSourcedVectorPotential()
  {
      if (_constitutiveExtraDofDiffusionData == NULL)
          Msg::Error("getRefTodEMFieldSourcedVectorPotential : _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->demFieldSourcedVectorPotential;
  }
  virtual const std::vector< std::vector< SVector3 > > & getConstRefTodEMFieldSourcedVectorCurl() const
  {
      if (_constitutiveExtraDofDiffusionData == NULL)
          Msg::Error("getConstRefTodEMFieldSourcedVectorCurl : _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->demFieldSourcedVectorCurl;
  }
  virtual std::vector< std::vector< SVector3 > > & getRefTodEMFieldSourcedVectorCurl()
  {
      if (_constitutiveExtraDofDiffusionData == NULL)
          Msg::Error("getRefTodEMFieldSourcedVectorCurl : _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->demFieldSourcedVectorCurl;
  }
  virtual const std::vector<std::vector< STensor3 > > & getConstRefTodElecDisplacementdVectorPotential() const
  {
      if (_constitutiveExtraDofDiffusionData == NULL)
          Msg::Error("getConstRefTodElecDisplacementdVectorPotential : _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->dElecDisplacementdVectorPotential;
  }
  virtual const std::vector<std::vector< STensor3 > > & getConstRefTodElecDisplacementdVectorCurl() const
  {
      if (_constitutiveExtraDofDiffusionData == NULL)
          Msg::Error("getConstRefTodElecDisplacementdVectorCurl : _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->dElecDisplacementdVectorCurl;
  }
  virtual std::vector<std::vector< STensor3 > > & getRefTodElecDisplacementdVectorPotential()
  {
      if (_constitutiveExtraDofDiffusionData == NULL)
          Msg::Error("getRefTodElecDisplacementdVectorPotential : _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->dElecDisplacementdVectorPotential;
  }
  virtual std::vector<std::vector< STensor3 > > & getRefTodElecDisplacementdVectorCurl()
  {
      if (_constitutiveExtraDofDiffusionData == NULL)
          Msg::Error("getRefTodElecDisplacementdVectorCurl : _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->dElecDisplacementdVectorCurl;
  }
  virtual const std::vector< STensor3 > & getConstRefTodEMFieldSourcedF() const
  {
      if (_constitutiveExtraDofDiffusionData == NULL)
          Msg::Error("getConstRefTodEMFieldSourcedF : _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->demFieldSourcedF;
  }
  virtual std::vector< STensor3 > & getRefTodEMFieldSourcedF()
  {
      if (_constitutiveExtraDofDiffusionData == NULL)
          Msg::Error("getRefTodEMFieldSourcedF : _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->demFieldSourcedF;
  }
  
  //coupling constitutive extra dof with nonLocal
  virtual const std::vector<std::vector<SVector3> > & getConstRefTodFluxdNonLocalVariable() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodFluxdNonLocalVariable : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFluxdNonLocalVariable;
  }
  virtual std::vector<std::vector<SVector3> > & getRefTodFluxdNonLocalVariable()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodFluxdNonLocalVariable : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFluxdNonLocalVariable;
  }
  virtual const fullMatrix<double> &getConstRefTodFieldSourcedNonLocalVariable() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodFieldSourcedNonLocalVariable : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFieldSourcedNonLocalVariable;
  }
  virtual fullMatrix<double> &getRefTodFieldSourcedNonLocalVariable()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodFieldSourcedNonLocalVariable : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFieldSourcedNonLocalVariable;
  }
  virtual const fullMatrix<double> &getConstRefTodMechanicalSourcedNonLocalVariable() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodMechanicalSourcedNonLocalVariable : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dMechanicalSourcedNonLocalVariable;
  }
  virtual fullMatrix<double> &getRefTodMechanicalSourcedNonLocalVariable()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodMechanicalSourcedNonLocalVariable : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dMechanicalSourcedNonLocalVariable;
  }
  virtual const fullMatrix< double> & getConstRefTodEMFieldSourcedNonLocalVariable() const
  {
      if (_constitutiveExtraDofDiffusionData == NULL)
          Msg::Error("getConstRefTodEMFieldSourcedNonLocalVariable : _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->dEMFieldSourcedNonLocalVariable;
  }
  virtual fullMatrix<double> & getRefTodEMFieldSourcedNonLocalVariable()
  {
      if (_constitutiveExtraDofDiffusionData == NULL)
          Msg::Error("getRefTodEMFieldSourcedNonLocalVariable : _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->dEMFieldSourcedNonLocalVariable;
  }
  virtual const std::vector< std::vector<SVector3> > &getConstRefTodElecDisplacementdNonLocalVariable() const
  {
      if (_constitutiveExtraDofDiffusionData == NULL)
          Msg::Error("getConstRefTodElecDisplacementdNonLocalVariable : _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->dElecDisplacementdNonLocalVariable;
  }
  virtual std::vector< std::vector<SVector3> > &getRefTodElecDisplacementdNonLocalVariable()
  {
      if (_constitutiveExtraDofDiffusionData == NULL)
          Msg::Error("getRefTodElecDisplacementdNonLocalVariable : _constitutiveExtraDofDiffusionData not created");
      return _constitutiveExtraDofDiffusionData->dElecDisplacementdNonLocalVariable;
  }
  
  // curl data
  virtual int getNumConstitutiveCurlVariable() const
  {
    if (_constitutiveCurlData == NULL)
      Msg::Error("getNumConstitutiveCurlVariable : _constitutiveCurlData not created");
    return _constitutiveCurlData->getNumConstitutiveCurlVariable();
  }
  virtual const SVector3 & getConstRefToVectorPotential(const int idex) const
  {
    if (_constitutiveCurlData == NULL)
      Msg::Error("getConstRefToVectorPotential : _constitutiveCurlData not created");
    return _constitutiveCurlData->vectorPotential[idex];
  }
  virtual SVector3 & getRefToVectorPotential(const int idex)
  {
    if (_constitutiveCurlData == NULL)
      Msg::Error("getRefToVectorPotential : _constitutiveCurlData not created");
    return _constitutiveCurlData->vectorPotential[idex];
  }
  virtual const SVector3 & getConstRefToVectorCurl(const int idex) const
  {
    if (_constitutiveCurlData == NULL)
      Msg::Error("getConstRefToVectorCurl : _constitutiveCurlData not created");
    return _constitutiveCurlData->vectorCurl[idex];
  }
  virtual SVector3 & getRefToVectorCurl(const int idex)
  {
    if (_constitutiveCurlData == NULL)
      Msg::Error("getRefToVectorCurl : _constitutiveCurlData not created");
    return _constitutiveCurlData->vectorCurl[idex];
  }
  virtual const SVector3 & getConstRefToVectorField(const int idex) const
  {
    if (_constitutiveCurlData == NULL)
      Msg::Error("getConstRefToVectorField : _constitutiveCurlData not created");
    return _constitutiveCurlData->vectorField[idex];
  }
  virtual SVector3 & getRefToVectorField(const int idex)
  {
    if (_constitutiveCurlData == NULL)
      Msg::Error("getRefToVectorField : _constitutiveCurlData not created");
    return _constitutiveCurlData->vectorField[idex];
  }
virtual const SVector3 & getConstRefToInductorSourceVectorField(const int idex) const
{
    if (_constitutiveCurlData == NULL)
        Msg::Error("getConstRefToInductorSourceVectorField : _constitutiveCurlData not created");
    return _constitutiveCurlData->inductorSourceVectorField[idex];
}
virtual SVector3 & getRefToInductorSourceVectorField(const int idex)
{
    if (_constitutiveCurlData == NULL)
        Msg::Error("getRefToInductorSourceVectorField : _constitutiveCurlData not created");
    return _constitutiveCurlData->inductorSourceVectorField[idex];
}
  virtual const SVector3 & getConstRefToSourceVectorField(const int idex) const
  {
    if (_constitutiveCurlData == NULL)
      Msg::Error("getConstRefToSourceVectorField : _constitutiveCurlData not created");
    return _constitutiveCurlData->sourceVectorField[idex];
  }
  virtual SVector3 & getRefToSourceVectorField(const int idex)
  {
    if (_constitutiveCurlData == NULL)
      Msg::Error("getRefToSourceVectorField : _constitutiveCurlData not created");
    return _constitutiveCurlData->sourceVectorField[idex];
  }
  virtual const SVector3 & getConstRefToMechanicalFieldSource(const int idex) const
  {
    if (_constitutiveCurlData == NULL)
      Msg::Error("getConstRefToMechanicalFieldSource : _constitutiveCurlData not created");
    return _constitutiveCurlData->mechanicalFieldSource[idex];
  }
  virtual SVector3 & getRefToMechanicalFieldSource(const int idex)
  {
    if (_constitutiveCurlData == NULL)
      Msg::Error("getRefToMechanicalFieldSource : _constitutiveCurlData not created");
    return _constitutiveCurlData->mechanicalFieldSource[idex];
  }

    virtual const std::vector<std::vector< STensor3 > > & getConstRefTodVectorFielddVectorPotential() const
    {
        if (_constitutiveCurlData == NULL)
            Msg::Error("getConstRefTodVectorFielddVectorPotential : _constitutiveCurlData not created");
        return _constitutiveCurlData->dVectorFielddVectorPotential;
    }
  virtual const std::vector<std::vector< STensor3 > > & getConstRefTodVectorFielddVectorCurl() const
  {
    if (_constitutiveCurlData == NULL)
      Msg::Error("getConstRefTodVectorFielddVectorCurl : _constitutiveCurlData not created");
    return _constitutiveCurlData->dVectorFielddVectorCurl;
  }
  virtual const std::vector< STensor33 > & getConstRefTodVectorFielddF() const
  {
    if (_constitutiveCurlData == NULL)
      Msg::Error("getConstRefTodVectorFielddF : _constitutiveCurlData not created");
    return _constitutiveCurlData->dVectorFielddF;
  }
  virtual const std::vector<std::vector< SVector3 > > & getConstRefTodVectorFielddExtraDofField() const
  {
    if (_constitutiveCurlData == NULL)
      Msg::Error("getConstRefTodVectorFielddExtraDofField : _constitutiveCurlData not created");
    return _constitutiveCurlData->dVectorFielddExtraDofField;
  }
  virtual const std::vector<std::vector< STensor3 > > & getConstRefTodVectorFielddGradExtraDofField() const
  {
    if (_constitutiveCurlData == NULL)
      Msg::Error("getConstRefTodVectorFielddGradExtraDofField : _constitutiveCurlData not created");
    return _constitutiveCurlData->dVectorFielddGradExtraDofField;
  }
  virtual const std::vector< STensor33 > & getConstRefTodPdVectorCurl() const
  {
    if (_constitutiveCurlData == NULL)
      Msg::Error("getConstRefTodPdVectorCurl : _constitutiveCurlData not created");
    return _constitutiveCurlData->dPdVectorCurl;
  }
  virtual const std::vector< STensor33 > & getConstRefTodPdVectorPotential() const
  {
      if (_constitutiveCurlData == NULL)
          Msg::Error("getConstRefTodPdVectorPotential : _constitutiveCurlData not created");
      return _constitutiveCurlData->dPdVectorPotential;
  }

    virtual std::vector<std::vector< STensor3 > > & getRefTodVectorFielddVectorPotential()
    {
        if (_constitutiveCurlData == NULL)
            Msg::Error("getRefTodVectorFielddVectorPotential : _constitutiveCurlData not created");
        return _constitutiveCurlData->dVectorFielddVectorPotential;
    }
  virtual std::vector<std::vector< STensor3 > > & getRefTodVectorFielddVectorCurl()
  {
    if (_constitutiveCurlData == NULL)
      Msg::Error("getRefTodVectorFielddVectorCurl : _constitutiveCurlData not created");
    return _constitutiveCurlData->dVectorFielddVectorCurl;
  }
  virtual std::vector< STensor33 > & getRefTodVectorFielddF()
  {
    if (_constitutiveCurlData == NULL)
      Msg::Error("getRefTodVectorFielddF : _constitutiveCurlData not created");
    return _constitutiveCurlData->dVectorFielddF;
  }
  virtual std::vector<std::vector< SVector3 > > & getRefTodVectorFielddExtraDofField()
  {
    if (_constitutiveCurlData == NULL)
      Msg::Error("getRefTodVectorFielddExtraDofField : _constitutiveCurlData not created");
    return _constitutiveCurlData->dVectorFielddExtraDofField;
  }
  virtual std::vector<std::vector< STensor3 > > & getRefTodVectorFielddGradExtraDofField()
  {
    if (_constitutiveCurlData == NULL)
      Msg::Error("getRefTodVectorFielddGradExtraDofField : _constitutiveCurlData not created");
    return _constitutiveCurlData->dVectorFielddGradExtraDofField;
  }
  virtual std::vector< STensor33 > & getRefTodPdVectorCurl()
  {
    if (_constitutiveCurlData == NULL)
      Msg::Error("getRefTodPdVectorCurl : _constitutiveCurlData not created");
    return _constitutiveCurlData->dPdVectorCurl;
  }
  virtual std::vector< STensor33 > & getRefTodPdVectorPotential()
  {
      if (_constitutiveCurlData == NULL)
          Msg::Error("getRefTodPdVectorPotential : _constitutiveCurlData not created");
      return _constitutiveCurlData->dPdVectorPotential;
  }

  virtual const std::vector<std::vector< SVector3 > > & getConstRefTodVectorFielddNonLocalVariable() const
  {
    if (_constitutiveCurlData == NULL)
      Msg::Error("getConstRefTodVectorFielddNonLocalVariable : _constitutiveCurlData not created");
    return _constitutiveCurlData->dVectorFielddNonLocalVariable;
  }
  virtual std::vector<std::vector< SVector3 > > & getRefTodVectorFielddNonLocalVariable()
  {
    if (_constitutiveCurlData == NULL)
      Msg::Error("getRefTodVectorFielddNonLocalVariable : _constitutiveCurlData not created");
    return _constitutiveCurlData->dVectorFielddNonLocalVariable;
  }
  virtual const std::vector< std::vector< SVector3 > > & getConstRefTodSourceVectorFielddNonLocalVariable() const
  {
    if (_constitutiveCurlData == NULL)
      Msg::Error("getConstRefTodSourceVectorFielddNonLocalVariable : _constitutiveCurlData not created");
    return _constitutiveCurlData->dSourceVectorFielddNonLocalVariable;
  }
  virtual std::vector< std::vector< SVector3 > > & getRefTodSourceVectorFielddNonLocalVariable()
  {
    if (_constitutiveCurlData == NULL)
      Msg::Error("getRefTodSourceVectorFielddNonLocalVariable : _constitutiveCurlData not created");
    return _constitutiveCurlData->dSourceVectorFielddNonLocalVariable;
  }

  virtual const std::vector< std::vector< SVector3 > > & getConstRefTodSourceVectorFielddExtraDofField() const
  {
    if (_constitutiveCurlData == NULL)
      Msg::Error("getConstRefTodSourceVectorFielddExtraDofField : _constitutiveCurlData not created");
    return _constitutiveCurlData->dSourceVectorFielddExtraDofField;
  }
  virtual std::vector< std::vector< SVector3 > > & getRefTodSourceVectorFielddExtraDofField()
  {
    if (_constitutiveCurlData == NULL)
      Msg::Error("getRefTodSourceVectorFielddExtraDofField : _constitutiveCurlData not created");
    return _constitutiveCurlData->dSourceVectorFielddExtraDofField;
  }
  virtual const std::vector< std::vector< STensor3 > > & getConstRefTodSourceVectorFielddGradExtraDofField() const
  {
    if (_constitutiveCurlData == NULL)
      Msg::Error("getConstRefTodSourceVectorFielddGradExtraDofField : _constitutiveCurlData not created");
    return _constitutiveCurlData->dSourceVectorFielddGradExtraDofField;
  }
  virtual std::vector< std::vector< STensor3 > > & getRefTodSourceVectorFielddGradExtraDofField()
  {
    if (_constitutiveCurlData == NULL)
      Msg::Error("getRefTodSourceVectorFielddGradExtraDofField : _constitutiveCurlData not created");
    return _constitutiveCurlData->dSourceVectorFielddGradExtraDofField;
  }
  virtual const std::vector< std::vector< STensor3 > > & getConstRefTodSourceVectorFielddVectorCurl() const
  {
    if (_constitutiveCurlData == NULL)
        Msg::Error("getConstRefTodSourceVectorFielddVectorCurl() : _constitutiveCurlData not created");
    return _constitutiveCurlData->dSourceVectorFielddVectorCurl;
  }
  virtual std::vector< std::vector< STensor3 > > & getRefTodSourceVectorFielddVectorCurl()
  {
    if (_constitutiveCurlData == NULL)
        Msg::Error("getRefTodSourceVectorFielddVectorCurl() : _constitutiveCurlData not created");
    return _constitutiveCurlData->dSourceVectorFielddVectorCurl;
  }
    virtual const std::vector< std::vector< STensor3 > > & getConstRefTodSourceVectorFielddVectorPotential() const
    {
        if (_constitutiveCurlData == NULL)
            Msg::Error("getConstRefTodSourceVectorFielddVectorPotential() : _constitutiveCurlData not created");
        return _constitutiveCurlData->dSourceVectorFielddVectorPotential;
    }
    virtual std::vector< std::vector< STensor3 > > & getRefTodSourceVectorFielddVectorPotential()
    {
        if (_constitutiveCurlData == NULL)
            Msg::Error("getRefTodSourceVectorFielddVectorPotential() : _constitutiveCurlData not created");
        return _constitutiveCurlData->dSourceVectorFielddVectorPotential;
    }
    virtual const std::vector< std::vector< STensor3 > > & getConstRefTodMechanicalFieldSourcedVectorPotential() const
    {
        if (_constitutiveCurlData == NULL)
            Msg::Error("getConstRefTodMechanicalFieldSourcedVectorPotential : _constitutiveCurlData not created");
        return _constitutiveCurlData->dMechanicalFieldSourcedVectorPotential;
    }
    virtual std::vector< std::vector< STensor3 > > & getRefTodMechanicalFieldSourcedVectorPotential()
    {
        if (_constitutiveCurlData == NULL)
            Msg::Error("getRefTodMechanicalFieldSourcedVectorPotential : _constitutiveCurlData not created");
        return _constitutiveCurlData->dMechanicalFieldSourcedVectorPotential;
    }
  virtual const std::vector< std::vector< STensor3 > > & getConstRefTodMechanicalFieldSourcedVectorCurl() const
  {
    if (_constitutiveCurlData == NULL)
      Msg::Error("getConstRefTodMechanicalFieldSourcedVectorCurl : _constitutiveCurlData not created");
    return _constitutiveCurlData->dMechanicalFieldSourcedVectorCurl;
  }
  virtual std::vector< std::vector< STensor3 > > & getRefTodMechanicalFieldSourcedVectorCurl()
  {
    if (_constitutiveCurlData == NULL)
      Msg::Error("getRefTodMechanicalFieldSourcedVectorCurl : _constitutiveCurlData not created");
    return _constitutiveCurlData->dMechanicalFieldSourcedVectorCurl;
  }
  virtual const std::vector< STensor33 > & getConstRefTodSourceVectorFielddF() const
  {
    if (_constitutiveCurlData == NULL)
      Msg::Error("getConstRefTodSourceVectorFielddF : _constitutiveCurlData not created");
    return _constitutiveCurlData->dSourceVectorFielddF;
  }
  virtual std::vector< STensor33 > & getRefTodSourceVectorFielddF()
  {
    if (_constitutiveCurlData == NULL)
      Msg::Error("getRefTodSourceVectorFielddF : _constitutiveCurlData not created");
    return _constitutiveCurlData->dSourceVectorFielddF;
  }
  virtual const std::vector< STensor33 > & getConstRefTodMechanicalFieldSourcedF() const
  {
    if (_constitutiveCurlData == NULL)
      Msg::Error("getConstRefTodMechanicalFieldSourcedF : _constitutiveCurlData not created");
    return _constitutiveCurlData->dMechanicalFieldSourcedF;
  }
  virtual std::vector< STensor33 > & getRefTodMechanicalFieldSourcedF()
  {
    if (_constitutiveCurlData == NULL)
      Msg::Error("getRefTodMechanicalFieldSourcedF : _constitutiveCurlData not created");
    return _constitutiveCurlData->dMechanicalFieldSourcedF;
  }
  virtual const std::vector< std::vector< SVector3 > > & getConstRefTodMechanicalFieldSourcedExtraDofField() const
  {
    if (_constitutiveCurlData == NULL)
      Msg::Error("getConstRefTodMechanicalFieldSourcedExtraDofField : _constitutiveCurlData not created");
    return _constitutiveCurlData->dMechanicalFieldSourcedExtraDofField;
  }
  virtual std::vector< std::vector< SVector3 > > & getRefTodMechanicalFieldSourcedExtraDofField()
  {
    if (_constitutiveCurlData == NULL)
      Msg::Error("getRefTodMechanicalFieldSourcedExtraDofField : _constitutiveCurlData not created");
    return _constitutiveCurlData->dMechanicalFieldSourcedExtraDofField;
  }
  virtual const std::vector< std::vector< STensor3 > > & getConstRefTodMechanicalFieldSourcedGradExtraDofField() const
  {
    if (_constitutiveCurlData == NULL)
      Msg::Error("getConstRefTodMechanicalFieldSourcedGradExtraDofField : _constitutiveCurlData not created");
    return _constitutiveCurlData->dMechanicalFieldSourcedGradExtraDofField;
  }
  virtual std::vector< std::vector< STensor3 > > & getRefTodMechanicalFieldSourcedGradExtraDofField()
  {
    if (_constitutiveCurlData == NULL)
      Msg::Error("getRefTodMechanicalFieldSourcedGradExtraDofField : _constitutiveCurlData not created");
    return _constitutiveCurlData->dMechanicalFieldSourcedGradExtraDofField;
  }
  virtual const std::vector< std::vector< SVector3 > > & getConstRefTodMechanicalFieldSourcedNonLocalVariable() const
  {
    if (_constitutiveCurlData == NULL)
      Msg::Error("getConstRefTodMechanicalFieldSourcedNonLocalVariable : _constitutiveCurlData not created");
    return _constitutiveCurlData->dMechanicalFieldSourcedNonLocalVariable;
  }
  virtual std::vector< std::vector< SVector3 > > & getRefTodMechanicalFieldSourcedNonLocalVariable()
  {
    if (_constitutiveCurlData == NULL)
      Msg::Error("getRefTodMechanicalFieldSourcedNonLocalVariable : _constitutiveCurlData not created");
    return _constitutiveCurlData->dMechanicalFieldSourcedNonLocalVariable;
  }
  virtual const SVector3 & getConstRefTodSourceVectorFielddt(const int idex) const
  {
    if (_constitutiveCurlData == NULL)
      Msg::Error("getConstRefTodSourceVectorFielddt : _constitutiveCurlData not created");
    return _constitutiveCurlData->dSourceVectorFielddt[idex];
  }
  virtual SVector3 & getRefTodSourceVectorFielddt(const int idex)
  {
    if (_constitutiveCurlData == NULL)
      Msg::Error("getRefTodSourceVectorFielddt : _constitutiveCurlData not created");
    return _constitutiveCurlData->dSourceVectorFielddt[idex];
  }

  //Energy Conjugated Field (fT & fv)

  virtual double getConstRefToEnergyConjugatedField(const int idex) const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefToEnergyConjugatedField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->energyConjugatedField(idex);
  }
  virtual double  &getRefToEnergyConjugatedField(const int idex)
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefToEnergyConjugatedField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->energyConjugatedField(idex);
  }
  virtual const std::vector<SVector3>  &getConstRefToGradEnergyConjugatedField() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefToGradEnergyConjugatedField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->gradEnergyConjugatedField;
  }
  virtual std::vector<SVector3>  &getRefToGradEnergyConjugatedField()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefToGradEnergyConjugatedField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->gradEnergyConjugatedField;
  }
  virtual const  std::vector<std::vector<double>>  &getConstRefTodFielddEnergyConjugatedField() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodFielddEnergyConjugatedField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFielddEnergyConjugatedField;
  }
  virtual std::vector<std::vector<double>>  &getRefTodFielddEnergyConjugatedField()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodFielddEnergyConjugatedField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dFielddEnergyConjugatedField;
  }
  virtual const std::vector<std::vector<SVector3>>  &getConstRefTodGradFielddEnergyConjugatedField() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodGradFielddEnergyConjugatedField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dGradFielddEnergyConjugatedField;
  }
  virtual std::vector<std::vector<SVector3>>  &getRefTodGradFielddEnergyConjugatedField()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodGradFielddEnergyConjugatedField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dGradFielddEnergyConjugatedField;
  }
  virtual const std::vector<std::vector<STensor3>>  &getConstRefTodGradFielddGradEnergyConjugatedField() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodGradFielddGradEnergyConjugatedField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dGradFielddGradEnergyConjugatedField;
  }
  virtual std::vector<std::vector<STensor3>>  &getRefTodGradFielddGradEnergyConjugatedField()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodGradFielddGradEnergyConjugatedField() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dGradFielddGradEnergyConjugatedField;
  }
  virtual const fullVector<double>  &getConstRefToEnergyConjugatedFieldJump() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefToEnergyConjugatedFieldJump() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->energyConjugatedFieldJump;
  } 
  virtual fullVector<double>  &getRefToEnergyConjugatedFieldJump()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefToEnergyConjugatedFieldJump() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->energyConjugatedFieldJump;
  } 
  virtual const  std::vector<std::vector<double>>  &getConstRefTodEnergyConjugatedFieldJumpdFieldm() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodEnergyConjugatedFieldJumpdFieldm() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dEnergyConjugatedFieldJumpdFieldm;
  } 
  virtual std::vector<std::vector<double>>  &getRefTodEnergyConjugatedFieldJumpdFieldm()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodEnergyConjugatedFieldJumpdFieldm() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dEnergyConjugatedFieldJumpdFieldm;
  } 
  virtual const  std::vector<std::vector<double>>  &getConstRefTodEnergyConjugatedFieldJumpdFieldp() const
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getConstRefTodEnergyConjugatedFieldJumpdFieldp() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dEnergyConjugatedFieldJumpdFieldp;
  }
  virtual std::vector<std::vector<double>>  &getRefTodEnergyConjugatedFieldJumpdFieldp()
  {
    if(_constitutiveExtraDofDiffusionData==NULL)
      Msg::Error("getRefTodEnergyConjugatedFieldJumpdFieldp() : _constitutiveExtraDofDiffusionData not created");
    return _constitutiveExtraDofDiffusionData->dEnergyConjugatedFieldJumpdFieldp;
  }

};

class nonLocalDamageDG3DIPVariable : public dG3DIPVariable
{

 protected:
  IPNonLocalDamage *_nldipv;

 public:
  nonLocalDamageDG3DIPVariable(int _nsdv, int _nlVar, const bool createBodyForceHO, const bool oninter);
  nonLocalDamageDG3DIPVariable(const nonLocalDamageDG3DIPVariable &source);
  virtual nonLocalDamageDG3DIPVariable& operator=(const IPVariable &source);

 /* specific function */
  IPNonLocalDamage* getIPNonLocalDamage(){return _nldipv;}
  const IPNonLocalDamage* getIPNonLocalDamage() const{return _nldipv;}
  #if defined(HAVE_MPI)
  virtual int numberValuesToCommunicateMPI()const{return _nldipv->numberValuesToCommunicateMPI();}
  virtual void fillValuesToCommunicateMPI(double *arrayMPI)const{_nldipv->fillValuesToCommunicateMPI(arrayMPI);};
  virtual void getValuesFromMPI(const double *arrayMPI){_nldipv->getValuesFromMPI(arrayMPI);};
  #endif // HAVE_MPI

  virtual ~nonLocalDamageDG3DIPVariable()
  {
    if (_nldipv) delete _nldipv;
    _nldipv = NULL;
  }

  virtual void setLocation(const IPVariable::LOCATION loc) {
    dG3DIPVariable::setLocation(loc);
    if (_nldipv)
      _nldipv->setLocation(loc);
  };

  virtual IPVariable* getInternalData() {return _nldipv;};
  virtual const IPVariable* getInternalData()  const {return _nldipv;};

  virtual bool dissipationIsActive() const {return _nldipv->dissipationIsActive();};

  virtual void blockDissipation(const bool fl){if (_nldipv) _nldipv->blockDissipation(fl);};
  virtual bool dissipationIsBlocked() const {
    if (_nldipv) return _nldipv->dissipationIsBlocked();
    else return false;
  }

  virtual double getConstRefToLocalVariable(const int idex) const ;
  virtual double& getRefToLocalVariable(const int idex);

  virtual double getConstRefToNonLocalVariableInMaterialLaw(const int idex) const;
  virtual double& getRefToNonLocalVariableInMaterialLaw(const int idex);

  virtual double get(const int i) const;
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const;
  virtual double damageEnergy() const {return getIPNonLocalDamage()->damageEnergy();};
  virtual const STensor3 &getConstRefToCharacteristicLengthMatrix(const int idex) const
  {
    if (idex == 0)
      return getIPNonLocalDamage()->_nldCharacteristicLengthMatrix;
    else if (idex == 1)
      return getIPNonLocalDamage()->_nldCharacteristicLengthFiber;
    else
    {
      Msg::Error("the non-local variable %d is not defined",idex);
      return getIPNonLocalDamage()->_nldCharacteristicLengthMatrix;
    }
  }

 /* virtual void setBulkCriticalDamage(const double DT, int idex){
    _nldipv->setCriticalDamage(DT, idex);
  } // set critical damage value
  virtual double getBulkCriticalDamage(int idex) const {
    return _nldipv->getCriticalDamage(idex);
   };
  virtual double getDamageIndicator(int idex) const 
  {
    return _nldipv->getDamageIndicator(idex);
  };*/

  virtual IPVariable* clone() const {return new nonLocalDamageDG3DIPVariable(*this);};
  virtual void restart();
 private:
  nonLocalDamageDG3DIPVariable(int _nsdv, const bool createBodyForceHO, const bool oninter): dG3DIPVariable(createBodyForceHO, oninter) {Msg::Error("Cannot build nonLocalDamageDG3DIPVariable without giving the nb non local Var");};

  // for path following based on irreversibe energt
  virtual double irreversibleEnergy() const {return getIPNonLocalDamage()->irreversibleEnergy();};
  virtual double& getRefToIrreversibleEnergy() {return getIPNonLocalDamage()->getRefToIrreversibleEnergy();};

  virtual const STensor3& getConstRefToDIrreversibleEnergyDDeformationGradient() const
    {return getIPNonLocalDamage()->getConstRefToDIrreversibleEnergyDF();};
  virtual STensor3& getRefToDIrreversibleEnergyDDeformationGradient()
    {return getIPNonLocalDamage()->getRefToDIrreversibleEnergyDF();};

  virtual const double& getConstRefToDIrreversibleEnergyDNonLocalVariable(const int index) const
  {
    if (index == 0 || index == 1)
      {
      return getIPNonLocalDamage()->getDIrreversibleEnergyDNonLocalVariable(index);
      }
    else{
      Msg::Error("the non-local variable %d is not defined",index); 
      return getIPNonLocalDamage()->getDIrreversibleEnergyDNonLocalVariable(0);
    }
  };
  virtual double& getRefToDIrreversibleEnergyDNonLocalVariable(const int index)
  {
    if (index == 0 || index ==1)
    {
      return getIPNonLocalDamage()->getRefToDIrreversibleEnergyDNonLocalVariable(index);
    }
    else{
      Msg::Error("the non-local variable %d is not defined",index); 
      return getIPNonLocalDamage()->getRefToDIrreversibleEnergyDNonLocalVariable(0);
    }
  };

};




/* Beginning IPvariable for nonlocalDamageTorchANNDG3DIPVariable*/
class nonLocalDamageTorchANNDG3DIPVariableDG3DIPVariable : public dG3DIPVariable
{  protected:
    // use torch tensor with 1 row
    int _numNonlocal;   //the number of non local variables
    int _n_stress;    //number of internal variables for the RNN that predicts the stresses
    int _n_elTang;    //number of internal variables for the RNN that predicts the components of the elastic tangent with damage
    int _n_NonDamTang; //number of internal variables for the RNN that predicts the components of the elastic tangent without damage
    double _initial_h;
    fullMatrix<double> _nlD;  //not used finally
    fullMatrix<double> _D;    //not used finally

    fullMatrix<double> _nlD36comp;  //not used finally
    fullMatrix<double> _D36comp;    //not used finally
    
    //to have D STensor43 instead of a matrix
    STensor43 _nlDTensor43;
    STensor43 _DTensor43;
    
    //plastic strain
    STensor3 _Eplast;
    
    //plastic strain reconstructed
    STensor3 _Eplastrec;
    
    //elastic stiffness without damage
    STensor43 _Cel;
    
    //DEplastDE
    STensor43 _DEplastDE;
    
    //DStressreconstructedDStrain (maybe this variable can be removed because defined elsewhere)
    STensor43 _DS_nlDE;
    
    double _defoEnergy;
    double _plasticEnergy;        //added for energy-based path Following
    double _damageEnergy;
    double _irreversibleEnergy;   //added for energy-based path Following
    STensor3 _DirrEnergyDF;       //added for energy-based path Following
    double _DirrEnergyDVar_nl0;   //added for energy-based path Following
    double _DirrEnergyDVar_nl1;   //added for energy-based path Following
    
    
    std::vector<double> localVar;
    std::vector<IPCLength*> ipvCL;
    
    IPVariableMechanics * _ipMeca;  //for using another local material law
    
#if defined(HAVE_TORCH)
    torch::Tensor _internalVars_stress; // internal variable for the RNN that predicts the stresses
    torch::Tensor _internalVars_elTang; // internal variable for the RNN that predicts the elastic tangent components with damage
    torch::Tensor _internalVars_NonDamTang; // internal variable for the RNN that predicts the elastic tangent components without damage
#else
    double _internalVars_stress;
    double _internalVars_elTang;
    double _internalVars_NonDamTang;
#endif
    fullMatrix<double> _kinematicVariables; // kinematic variable
    fullMatrix<double> restart_internalVars_stress; // internal variable
    fullMatrix<double> restart_internalVars_elTang; // internal variable
    fullMatrix<double> restart_internalVars_NonDamTang; // internal variable

  public:
    nonLocalDamageTorchANNDG3DIPVariableDG3DIPVariable(const std::vector< CLengthLaw *> cll, const int n_stress, const int n_elTang, const int n_NonDamTang, const double initial_h, const int numNonlocal, const bool createBodyForceHO, const bool oninter);
    nonLocalDamageTorchANNDG3DIPVariableDG3DIPVariable(const nonLocalDamageTorchANNDG3DIPVariableDG3DIPVariable &source);
    virtual nonLocalDamageTorchANNDG3DIPVariableDG3DIPVariable& operator =(const IPVariable& src);
    virtual ~nonLocalDamageTorchANNDG3DIPVariableDG3DIPVariable();

#if defined(HAVE_TORCH)
    virtual const torch::Tensor& getConstRefToInternalVariables_stress() const {return _internalVars_stress;};
    virtual torch::Tensor& getRefToInternalVariables_stress() {return _internalVars_stress;};
    virtual const torch::Tensor& getConstRefToInternalVariables_elTang() const {return _internalVars_elTang;};
    virtual torch::Tensor& getRefToInternalVariables_elTang() {return _internalVars_elTang;};
    virtual const torch::Tensor& getConstRefToInternalVariables_NonDamTang() const {return _internalVars_NonDamTang;};
    virtual torch::Tensor& getRefToInternalVariables_NonDamTang() {return _internalVars_NonDamTang;};
#endif

    const IPVariableMechanics& getConstRefToIpMeca() const {return *_ipMeca;}
    IPVariableMechanics& getRefToIpMeca() {return *_ipMeca;}
    void setIpMeca(IPVariable& ipMeca)
    {
      if(_ipMeca!=NULL)
      {
        delete _ipMeca;
        _ipMeca=NULL;
      }
      _ipMeca= (dynamic_cast<IPVariableMechanics *>(ipMeca.clone()));
    }

    virtual const fullMatrix<double>& getConstRefToKinematicVariables() const {return _kinematicVariables;};
    virtual fullMatrix<double>& getRefToKinematicVariables() {return _kinematicVariables;};

    virtual IPVariable* getInternalData() {return NULL;};
    virtual const IPVariable* getInternalData()  const {return NULL;};
    
    virtual double get(const int comp) const;
    virtual void restart();

    virtual double defoEnergy() const {return _defoEnergy;};
    virtual double plasticEnergy() const {return _plasticEnergy;};
    virtual double damageEnergy() const{return _damageEnergy;};

   
    virtual double getConstRefToLocalVariable(const int idex) const {return localVar[idex];};
    virtual double& getRefToLocalVariable(const int idex) {return localVar[idex];};

    virtual double getConstRefToNonLocalVariable(const int idex) const {return _nonlocalData->nonLocalVariable(idex);};
    virtual double& getRefToNonLocalVariable(const int idex) {return _nonlocalData->nonLocalVariable(idex);};
    
    
    virtual double getConstRefToNonLocalVariableInMaterialLaw(const int idex) const {return _nonlocalData->nonLocalVariable(idex);};
    virtual double& getRefToNonLocalVariableInMaterialLaw(const int idex) {return _nonlocalData->nonLocalVariable(idex);};
    
    /*initial code
    //virtual const fullMatrix<double> &getConstRefToNonLocalDamage() const {return _nlD;}; 
    //virtual fullMatrix<double> &getRefToNonLocalDamage() {return _nlD;};*/    
                  
    virtual const fullMatrix<double> &getConstRefToNonLocalDamage() const {return _nlD36comp;};
    virtual fullMatrix<double> &getRefToNonLocalDamage() {return _nlD36comp;};
    
    //to have D Tensor43
    virtual const STensor43 &getConstRefToNonLocalDamageTensor43() const {return _nlDTensor43;};
    virtual STensor43 &getRefToNonLocalDamageTensor43() {return _nlDTensor43;};
    
    //virtual const fullMatrix<double> &getConstRefToLocalDamage() const {return _D;}; //initial code
    //virtual const fullMatrix<double> &getConstRefToLocalDamage() const {return _D36comp;};
    
    //to have D Tensor43
    virtual const STensor43 &getConstRefToLocalDamageTensor43() const {return _DTensor43;};
    virtual STensor43 &getRefToLocalDamageTensor43() {return _DTensor43;};
    //virtual fullMatrix<double> &getRefToLocalDamage() {return _D;};  //initial code
    /*virtual fullMatrix<double> &getRefToLocalDamage() {return _D36comp;};*/
    
    
    
    
    //plastic strain
    virtual const STensor3 &getConstRefToEplast() const {return _Eplast;};
    virtual STensor3 &getRefToEplast() {return _Eplast;}; 
    
    //plastic strain reconstructed
    virtual const STensor3 &getConstRefToEplastrec() const {return _Eplastrec;};
    virtual STensor3 &getRefToEplastrec() {return _Eplastrec;}; 
    
    //elastic stiffness without damage  
    virtual const STensor43 &getConstRefToCel() const {return _Cel;};
    virtual STensor43 &getRefToCel() {return _Cel;}; 
    
    //DStressreconstructedDStrain (maybe this function can be removed)
    virtual STensor43 &getRefToDSnlDE() {return _DS_nlDE;}; 
    

    // for path following based on  irreversibe energt
    virtual double irreversibleEnergy() const {return getConstRefToIrreversibleEnergy();} ;

    virtual const STensor3& getConstRefToDIrreversibleEnergyDDeformationGradient() const {return getConstRefToDIrreversibleEnergyDF();};
    virtual STensor3& getRefToDIrreversibleEnergyDDeformationGradient() {return getRefToDIrreversibleEnergyDF();};


    virtual const STensor43 &getConstRefToDEplastDE() const {return _DEplastDE;};
    virtual STensor43 &getRefToDEplastDE() {return _DEplastDE;}; 
    
    virtual const double & getConstRefToIrreversibleEnergy() const {return _irreversibleEnergy;};   //added for energy-based path Following
    virtual double & getRefToIrreversibleEnergy() {return _irreversibleEnergy;};   //added for energy-based path Following
    virtual const STensor3 & getConstRefToDIrreversibleEnergyDF() const {return _DirrEnergyDF;};   //added for energy-based path Following
    virtual STensor3 & getRefToDIrreversibleEnergyDF() {return _DirrEnergyDF;};   //added for energy-based path Following
    virtual const double & getConstRefToDIrreversibleEnergyDNonLocalVariable(const int i) const {if (i==0){return _DirrEnergyDVar_nl0;} else {return _DirrEnergyDVar_nl1;};}   //added for energy-based path Following
    virtual double & getRefToDIrreversibleEnergyDNonLocalVariable(const int i) {if (i==0){return _DirrEnergyDVar_nl0;} else {return _DirrEnergyDVar_nl1;};}   //added for energy-based path Following
    virtual double & getRefToPlasticEnergy() {return _plasticEnergy;};   //added for energy-based path Following
    virtual const double & getConstRefToPlasticEnergy() const {return _plasticEnergy;};   //added for energy-based path Following
    virtual double & getRefToDefoEnergy() {return _defoEnergy;};   //added for energy-based path Following
    virtual const double & getConstRefToDefoEnergy() const {return _defoEnergy;};   //added for energy-based path Following
    virtual double & getRefToDamageEnergy() {return _damageEnergy;};   //added for energy-based path Following
    virtual const double & getConstRefToDamageEnergy() const {return _damageEnergy;};   //added for energy-based path Following
 //////////////////////////////////////////////////////////////////////////////////
   
    virtual const std::vector<IPCLength*> &getConstRefToIPCLength() const
    {
      if(ipvCL.size()==0)
        Msg::Error("nonLocalDamageTorchANNDG3DIPVariableDG3DIPVariable: ipvCL not initialized");
      return ipvCL;
    }
    virtual std::vector<IPCLength*> &getRefToIPCLength()
    {
      if(ipvCL.size()==0)
        Msg::Error("nonLocalDamageTorchANNDG3DIPVariableDG3DIPVariable: ipvCL not initialized");
      return ipvCL;
    }
    virtual const STensor3 &getConstRefToCharacteristicLengthMatrix(const int idex) const
    {
      if(ipvCL.size()==0)
        Msg::Error("nonLocalDamageTorchANNDG3DIPVariableDG3DIPVariable: ipvCL not initialized");
     
      if (idex < ipvCL.size())
      {
        if(ipvCL[idex]==NULL)
        {
          Msg::Error("nonLocalDamageTorchANNDG3DIPVariableDG3DIPVariable: ipvCL not initialized");   
        }
        return ipvCL[idex]->getConstRefToCL();
      }
      else
      {
        Msg::Error("the non-local variable %d is not defined",idex);
        return ipvCL[0]->getConstRefToCL();
      }
    }
     
    virtual IPVariable* clone() const {return new nonLocalDamageTorchANNDG3DIPVariableDG3DIPVariable(*this);};    

};

/* End IPvariable for nonlocalDamageTorchANNDG3DIPVariable*/






class nonLocalDamageIsotropicElasticityDG3DIPVariable : public dG3DIPVariable
{

 protected:
  IPNonLocalDamageIsotropicElasticity *_nldIsotropicElasticityipv;

 public:
  nonLocalDamageIsotropicElasticityDG3DIPVariable(const mlawNonlocalDamageHyperelastic & elasticLaw, const bool createBodyForceHO, const bool oninter);
  nonLocalDamageIsotropicElasticityDG3DIPVariable(const mlawNonLocalDamageIsotropicElasticity & isoElastLaw, const bool createBodyForceHO, const bool oninter);
  nonLocalDamageIsotropicElasticityDG3DIPVariable(const nonLocalDamageIsotropicElasticityDG3DIPVariable &source);
  virtual nonLocalDamageIsotropicElasticityDG3DIPVariable& operator=(const IPVariable &source);

 /* specific function */
  IPNonLocalDamageIsotropicElasticity* getIPNonLocalDamageIsotropicElasticity(){return _nldIsotropicElasticityipv;}
  const IPNonLocalDamageIsotropicElasticity* getIPNonLocalDamageIsotropicElasticity() const{return _nldIsotropicElasticityipv;}
  virtual ~nonLocalDamageIsotropicElasticityDG3DIPVariable()
  {
    if (_nldIsotropicElasticityipv){
     delete _nldIsotropicElasticityipv;
     _nldIsotropicElasticityipv = NULL;
    }
  }

  virtual void setLocation(const IPVariable::LOCATION loc) {
    dG3DIPVariable::setLocation(loc);
    if (_nldIsotropicElasticityipv)
      _nldIsotropicElasticityipv->setLocation(loc);
  };

  virtual IPVariable* getInternalData() {return _nldIsotropicElasticityipv;};
  virtual const IPVariable* getInternalData()  const {return _nldIsotropicElasticityipv;};

  virtual bool dissipationIsActive() const {return _nldIsotropicElasticityipv->dissipationIsActive();};

  virtual void blockDissipation(const bool fl){
    if (_nldIsotropicElasticityipv) _nldIsotropicElasticityipv->blockDissipation(fl);
  }
  virtual bool dissipationIsBlocked() const {
    if (_nldIsotropicElasticityipv) return _nldIsotropicElasticityipv->dissipationIsBlocked();
    else return false;
  }

  virtual void setNonLocalToLocal(const bool fl){_nldIsotropicElasticityipv->setNonLocalToLocal(fl);};
  virtual bool getNonLocalToLocal() const {return _nldIsotropicElasticityipv->getNonLocalToLocal();};

  virtual double getConstRefToLocalVariable(const int idex) const ;
  virtual double& getRefToLocalVariable(const int idex);

  virtual double getConstRefToNonLocalVariableInMaterialLaw(const int idex) const;
  virtual double& getRefToNonLocalVariableInMaterialLaw(const int idex);

  virtual double get(const int i) const;
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const;
  virtual double damageEnergy() const {return _nldIsotropicElasticityipv->damageEnergy();};
  virtual const STensor3 &getConstRefToCharacteristicLengthMatrix(const int idex) const
  {
    if (idex == 0)
      return _nldIsotropicElasticityipv->getConstRefToCharacteristicLength();
    else
      Msg::Error("the non-local variable %d is not defined",idex);
    return _nldIsotropicElasticityipv->getConstRefToCharacteristicLength();
  }

  // for path following based on irreversibe energt
  virtual double irreversibleEnergy() const {return _nldIsotropicElasticityipv->irreversibleEnergy();};
  virtual double& getRefToIrreversibleEnergy() {return _nldIsotropicElasticityipv->getRefToIrreversibleEnergy();};

  virtual const STensor3& getConstRefToDIrreversibleEnergyDDeformationGradient() const
    {return _nldIsotropicElasticityipv->getConstRefToDIrreversibleEnergyDF();};
  virtual STensor3& getRefToDIrreversibleEnergyDDeformationGradient()
    {return _nldIsotropicElasticityipv->getRefToDIrreversibleEnergyDF();};

  virtual const double& getConstRefToDIrreversibleEnergyDNonLocalVariable(const int index) const
  {
    if (index == 0)
      {
      return _nldIsotropicElasticityipv->getDIrreversibleEnergyDNonLocalVariable();
      }
    else{
      Msg::Error("the non-local variable %d is not defined",index);
      return _nldIsotropicElasticityipv->getDIrreversibleEnergyDNonLocalVariable();
    }
  };
  virtual double& getRefToDIrreversibleEnergyDNonLocalVariable(const int index)
  {
    if (index == 0)
    {
      return _nldIsotropicElasticityipv->getRefToDIrreversibleEnergyDNonLocalVariable();
    }
    else{
      Msg::Error("the non-local variable %d is not defined",index);
      return _nldIsotropicElasticityipv->getRefToDIrreversibleEnergyDNonLocalVariable();
    }
  };

  virtual IPVariable* clone() const {return new nonLocalDamageIsotropicElasticityDG3DIPVariable(*this);};
  virtual void restart();
};

class nonLocalDamageJ2HyperDG3DIPVariable : public dG3DIPVariable
{

 protected:
  IPNonLocalDamageJ2Hyper *_nldJ2Hyperipv;

 public:
  nonLocalDamageJ2HyperDG3DIPVariable(const mlawNonLocalDamageJ2Hyper &_j2law, const bool createBodyForceHO, const bool oninter);
  nonLocalDamageJ2HyperDG3DIPVariable(const mlawNonLocalDamageJ2SmallStrain &_j2law, const bool createBodyForceHO, const bool oninter);
  nonLocalDamageJ2HyperDG3DIPVariable(const nonLocalDamageJ2HyperDG3DIPVariable &source);
  virtual nonLocalDamageJ2HyperDG3DIPVariable& operator=(const IPVariable &source);

 /* specific function */
  IPNonLocalDamageJ2Hyper* getIPNonLocalDamageJ2Hyper(){return _nldJ2Hyperipv;}
  const IPNonLocalDamageJ2Hyper* getIPNonLocalDamageJ2Hyper() const{return _nldJ2Hyperipv;}
  virtual ~nonLocalDamageJ2HyperDG3DIPVariable()
  {
    if (_nldJ2Hyperipv) delete _nldJ2Hyperipv;
    _nldJ2Hyperipv = NULL;
  }
  #if defined(HAVE_MPI)
  virtual int numberValuesToCommunicateMPI()const{return _nldJ2Hyperipv->numberValuesToCommunicateMPI();}
  virtual void fillValuesToCommunicateMPI(double *arrayMPI)const{_nldJ2Hyperipv->fillValuesToCommunicateMPI(arrayMPI);};
  virtual void getValuesFromMPI(const double *arrayMPI){_nldJ2Hyperipv->getValuesFromMPI(arrayMPI);};
  #endif // HAVE_MPI

  virtual void blockDissipation(const bool fl){
    if (_nldJ2Hyperipv) _nldJ2Hyperipv->blockDissipation(fl);
  }
  virtual bool dissipationIsBlocked() const {
    if (_nldJ2Hyperipv) return _nldJ2Hyperipv->dissipationIsBlocked();
    else return false;
  }

  virtual void setLocation(const IPVariable::LOCATION loc) {
    dG3DIPVariable::setLocation(loc);
    if (_nldJ2Hyperipv)
      _nldJ2Hyperipv->setLocation(loc);
  };
  virtual IPVariable* getInternalData() {return _nldJ2Hyperipv;};
  virtual const IPVariable* getInternalData()  const {return _nldJ2Hyperipv;};

  virtual bool dissipationIsActive() const {return _nldJ2Hyperipv->dissipationIsActive();};

  virtual void setNonLocalToLocal(const bool fl){_nldJ2Hyperipv->setNonLocalToLocal(fl);};
  virtual bool getNonLocalToLocal() const {return _nldJ2Hyperipv->getNonLocalToLocal();};

  virtual double getConstRefToLocalVariable(const int idex) const ;
  virtual double& getRefToLocalVariable(const int idex);

  virtual double getConstRefToNonLocalVariableInMaterialLaw(const int idex) const;
  virtual double& getRefToNonLocalVariableInMaterialLaw(const int idex);

  virtual double get(const int i) const;
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const;
  virtual double damageEnergy() const {return _nldJ2Hyperipv->damageEnergy();}
  virtual const STensor3 &getConstRefToCharacteristicLengthMatrix(const int idex) const
  {
    if (idex == 0)
      return _nldJ2Hyperipv->getConstRefToCharacteristicLength();
    else
      Msg::Error("the non-local variable %d is not defined",idex);
    return _nldJ2Hyperipv->getConstRefToCharacteristicLength();
  }

	// for path following based on  irreversibe energt
	virtual double irreversibleEnergy() const {return _nldJ2Hyperipv->irreversibleEnergy();};
	virtual double& getRefToIrreversibleEnergy() {return _nldJ2Hyperipv->getRefToIrreversibleEnergy();};

	virtual const STensor3& getConstRefToDIrreversibleEnergyDDeformationGradient() const {return _nldJ2Hyperipv->getConstRefToDIrreversibleEnergyDF();};
	virtual STensor3& getRefToDIrreversibleEnergyDDeformationGradient() {return _nldJ2Hyperipv->getRefToDIrreversibleEnergyDF();};

	virtual const double& getConstRefToDIrreversibleEnergyDNonLocalVariable(const int index) const {
		if (index == 0){
			return _nldJ2Hyperipv->getDIrreversibleEnergyDNonLocalVariable();
		}
		else{
			Msg::Error("the non-local variable %d is not defined",index);
			return _nldJ2Hyperipv->getDIrreversibleEnergyDNonLocalVariable();
		}
	};
	virtual double& getRefToDIrreversibleEnergyDNonLocalVariable(const int index) {
		if (index == 0){
			return _nldJ2Hyperipv->getRefToDIrreversibleEnergyDNonLocalVariable();
		}
		else{
			Msg::Error("the non-local variable %d is not defined",index);
			return _nldJ2Hyperipv->getRefToDIrreversibleEnergyDNonLocalVariable();
		}
	};

  virtual IPVariable* clone() const {return new nonLocalDamageJ2HyperDG3DIPVariable(*this);};
  virtual void restart();
};

class nonLocalPorosityDG3DIPVariable : public dG3DIPVariable
{
 protected:
  IPNonLocalPorosity *_nldPorousipv;
  mlawNonLocalPorosity::nonLocalMethod _nonlocalMethod;

 public:
  nonLocalPorosityDG3DIPVariable(const bool createBodyForceHO, const bool oninter, const int numLocalVar, const int numExtraDofDiffusion);
  nonLocalPorosityDG3DIPVariable(const mlawNonLocalPorosity &porousLaw,const double fvInitial, const bool createBodyForceHO, const bool oninter, const int numNonLocalVar = 1, const int numExtraDofDiffusion=0);
  nonLocalPorosityDG3DIPVariable(const nonLocalPorosityDG3DIPVariable &source);
  virtual nonLocalPorosityDG3DIPVariable& operator=(const IPVariable &source);

  virtual IPVariable* getInternalData()  {return _nldPorousipv;};
	virtual const IPVariable* getInternalData()  const {return _nldPorousipv;};
 #if defined(HAVE_MPI)
  virtual int numberValuesToCommunicateMPI()const{return _nldPorousipv->numberValuesToCommunicateMPI();}
  virtual void fillValuesToCommunicateMPI(double *arrayMPI)const{_nldPorousipv->fillValuesToCommunicateMPI(arrayMPI);};
  virtual void getValuesFromMPI(const double *arrayMPI){_nldPorousipv->getValuesFromMPI(arrayMPI);};
  #endif // HAVE_MPI
 /* specific function */
  IPNonLocalPorosity* getIPNonLocalPorosity(){return _nldPorousipv;}
  const IPNonLocalPorosity* getIPNonLocalPorosity() const {return _nldPorousipv;}
  virtual ~nonLocalPorosityDG3DIPVariable()
  {
    if (_nldPorousipv != NULL){
      delete _nldPorousipv; _nldPorousipv=NULL;
    }
  }

  virtual void setLocation(const IPVariable::LOCATION loc) {
    dG3DIPVariable::setLocation(loc);
    if (_nldPorousipv)
      _nldPorousipv->setLocation(loc);
  };

  virtual bool dissipationIsActive() const {return _nldPorousipv->dissipationIsActive();};

  virtual void blockDissipation(const bool fl){
    if (_nldPorousipv) _nldPorousipv->blockDissipation(fl);
  }
  virtual bool dissipationIsBlocked() const{
    if (_nldPorousipv) return _nldPorousipv->dissipationIsBlocked();
    else return false;
  }

  virtual void setNonLocalToLocal(const bool fl){ if (_nldPorousipv) _nldPorousipv->setNonLocalToLocal(fl);};
  virtual bool getNonLocalToLocal() const {
    if (_nldPorousipv) return _nldPorousipv->getNonLocalToLocal();
    else return false;
  };

  virtual double getConstRefToLocalVariable(const int idex) const ;
  virtual double& getRefToLocalVariable(const int idex);

  virtual double getConstRefToNonLocalVariableInMaterialLaw(const int idex) const;
  virtual double& getRefToNonLocalVariableInMaterialLaw(const int idex);


  virtual double get(const int i) const;
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const;
  virtual double damageEnergy() const {return _nldPorousipv->damageEnergy();};
  virtual const STensor3 &getConstRefToCharacteristicLengthMatrix(const int idex) const
  {
    return _nldPorousipv->getConstRefToCharacteristicLength(idex);
  }
  virtual IPVariable* clone() const {return new nonLocalPorosityDG3DIPVariable(*this);};

  // for path following based on  irreversibe energt
	virtual double irreversibleEnergy() const {return _nldPorousipv->irreversibleEnergy();};
	virtual double& getRefToIrreversibleEnergy() {return _nldPorousipv->getRefToIrreversibleEnergy();};

	virtual const STensor3& getConstRefToDIrreversibleEnergyDDeformationGradient() const {return _nldPorousipv->getConstRefToDIrreversibleEnergyDF();};
	virtual STensor3& getRefToDIrreversibleEnergyDDeformationGradient() {return _nldPorousipv->getRefToDIrreversibleEnergyDF();};
  virtual const double& getConstRefToDIrreversibleEnergyDNonLocalVariable(const int index) const {
    return _nldPorousipv->getConstRefToDIrreversibleEnergyDNonLocalVariable(index);
	};
	virtual double& getRefToDIrreversibleEnergyDNonLocalVariable(const int index) {
    return _nldPorousipv->getRefToDIrreversibleEnergyDNonLocalVariable(index);
	};

  virtual bool getConstitutiveSuccessFlag() const
  {
    return _nldPorousipv->getConstitutiveSuccessFlag();
  }
  virtual void restart();
  virtual void setRefToLinearConductivity(const STensor3 &eT) {setConstRefToLinearK(eT,0,0);}
  virtual void setRefToStiff_alphaDilatation(const STensor3 &eT) {setConstRefToLinearSymmetrizationCoupling(eT,0);}
protected:
  nonLocalPorosityDG3DIPVariable(const mlawNonLocalPorosity &pLaw, const bool createBodyForceHO, const bool oninter, const int numLocalVar = 1, const int numExtraDofDiffusion=0);

};

class nonLocalPorosityWithCleavageDG3DIPVariable : public nonLocalPorosityDG3DIPVariable{
  protected:
    IPNonLocalPorosityWithCleavage* _cleavageNonLocalIPv;
  public:
    nonLocalPorosityWithCleavageDG3DIPVariable(const mlawNonLocalPorousWithCleavageFailure &porousLaw,const double fvInitial, const bool createBodyForceHO, const bool oninter, const int numNonLocalVar = 1, const int numExtraDofDiffusion=0);
    nonLocalPorosityWithCleavageDG3DIPVariable(const nonLocalPorosityWithCleavageDG3DIPVariable &source);
    virtual nonLocalPorosityWithCleavageDG3DIPVariable& operator=(const IPVariable &source);
    virtual ~nonLocalPorosityWithCleavageDG3DIPVariable();

    virtual double get(const int i) const;

    IPNonLocalPorosityWithCleavage* getIPNonLocalPorosityWithCleavage(){return dynamic_cast<IPNonLocalPorosityWithCleavage*>(_nldPorousipv);}
    const IPNonLocalPorosityWithCleavage* getIPNonLocalPorosityWithCleavage() const {return dynamic_cast<const IPNonLocalPorosityWithCleavage*>(_nldPorousipv);}

    virtual double getConstRefToLocalVariable(const int idex) const ;
    virtual double& getRefToLocalVariable(const int idex);

    virtual double getConstRefToNonLocalVariableInMaterialLaw(const int idex) const;
    virtual double& getRefToNonLocalVariableInMaterialLaw(const int idex);

    virtual const STensor3 &getConstRefToCharacteristicLengthMatrix(const int idex) const;

    virtual IPVariable* clone() const {return new nonLocalPorosityWithCleavageDG3DIPVariable(*this);};
    virtual void restart();
};

class nonLocalDamageJ2FullyCoupledDG3DIPVariable : public dG3DIPVariable, public extraDofIPVariableBase
{

 protected:
  IPNonLocalDamageJ2FullyCoupledThermoMechanics* _nldJ2Hyperipv;

 public:
  nonLocalDamageJ2FullyCoupledDG3DIPVariable(const mlawNonLocalDamageJ2FullyCoupledThermoMechanics &_j2law, const bool createBodyForceHO, const bool oninter);
  nonLocalDamageJ2FullyCoupledDG3DIPVariable(const nonLocalDamageJ2FullyCoupledDG3DIPVariable &source);
  virtual nonLocalDamageJ2FullyCoupledDG3DIPVariable& operator=(const IPVariable &source);

 /* specific function */
  IPNonLocalDamageJ2FullyCoupledThermoMechanics* getIPNonLocalDamageJ2FullyCoupledThermoMechanics(){return _nldJ2Hyperipv;}
  const IPNonLocalDamageJ2FullyCoupledThermoMechanics* getIPNonLocalDamageJ2FullyCoupledThermoMechanics() const{return _nldJ2Hyperipv;}
  virtual ~nonLocalDamageJ2FullyCoupledDG3DIPVariable()
  {
    if (_nldJ2Hyperipv) delete _nldJ2Hyperipv;
    _nldJ2Hyperipv = NULL;
  }
  #if defined(HAVE_MPI)
  virtual int numberValuesToCommunicateMPI()const{return _nldJ2Hyperipv->numberValuesToCommunicateMPI();}
  virtual void fillValuesToCommunicateMPI(double *arrayMPI)const{_nldJ2Hyperipv->fillValuesToCommunicateMPI(arrayMPI);};
  virtual void getValuesFromMPI(const double *arrayMPI){_nldJ2Hyperipv->getValuesFromMPI(arrayMPI);};
  #endif // HAVE_MPI

  virtual void blockDissipation(const bool fl){
    if (_nldJ2Hyperipv) _nldJ2Hyperipv->blockDissipation(fl);
  }
  virtual bool dissipationIsBlocked() const {
    if (_nldJ2Hyperipv) return _nldJ2Hyperipv->dissipationIsBlocked();
    else return false;
  }

  virtual void setLocation(const IPVariable::LOCATION loc) {
    dG3DIPVariable::setLocation(loc);
    if (_nldJ2Hyperipv)
      _nldJ2Hyperipv->setLocation(loc);
  };
  virtual IPVariable* getInternalData() {return _nldJ2Hyperipv;};
  virtual const IPVariable* getInternalData()  const {return _nldJ2Hyperipv;};

  virtual bool dissipationIsActive() const {return _nldJ2Hyperipv->dissipationIsActive();};

  virtual void setNonLocalToLocal(const bool fl){_nldJ2Hyperipv->setNonLocalToLocal(fl);};
  virtual bool getNonLocalToLocal() const {return _nldJ2Hyperipv->getNonLocalToLocal();};

  virtual double getConstRefToLocalVariable(const int idex) const ;
  virtual double& getRefToLocalVariable(const int idex);

  virtual double getConstRefToNonLocalVariableInMaterialLaw(const int idex) const;
  virtual double& getRefToNonLocalVariableInMaterialLaw(const int idex);

  virtual double get(const int i) const;
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const;
  virtual double damageEnergy() const {return _nldJ2Hyperipv->damageEnergy();}
  virtual const STensor3 &getConstRefToCharacteristicLengthMatrix(const int idex) const
  {
    if (idex == 0)
      return _nldJ2Hyperipv->getConstRefToCharacteristicLength();
    else
      Msg::Error("the non-local variable %d is not defined",idex);
    return _nldJ2Hyperipv->getConstRefToCharacteristicLength();
  }

  virtual double getInternalEnergyExtraDofDiffusion() const {return _nldJ2Hyperipv->getThermalEnergy();};



  // for path following based on  irreversibe energt
  virtual double irreversibleEnergy() const {return _nldJ2Hyperipv->irreversibleEnergy();};
  virtual double& getRefToIrreversibleEnergy() {return _nldJ2Hyperipv->getRefToIrreversibleEnergy();};

  virtual const STensor3& getConstRefToDIrreversibleEnergyDDeformationGradient() const {return _nldJ2Hyperipv->getConstRefToDIrreversibleEnergyDF();};
  virtual STensor3& getRefToDIrreversibleEnergyDDeformationGradient() {return _nldJ2Hyperipv->getRefToDIrreversibleEnergyDF();};

  virtual const double& getConstRefToDIrreversibleEnergyDNonLocalVariable(const int index) const {
    if (index == 0){
      return _nldJ2Hyperipv->getDIrreversibleEnergyDNonLocalVariable();
    } 
    else{
      Msg::Error("the non-local variable %d is not defined",index);
      return _nldJ2Hyperipv->getDIrreversibleEnergyDNonLocalVariable();
    }
  };
  virtual double& getRefToDIrreversibleEnergyDNonLocalVariable(const int index) {
   if (index == 0){
     return _nldJ2Hyperipv->getRefToDIrreversibleEnergyDNonLocalVariable();
   }
   else{
     Msg::Error("the non-local variable %d is not defined",index);
     return _nldJ2Hyperipv->getRefToDIrreversibleEnergyDNonLocalVariable();
   }
  };

  virtual IPVariable* clone() const {return new nonLocalDamageJ2FullyCoupledDG3DIPVariable(*this);};
  virtual void restart();
  
  virtual void getConstitutiveExtraDofFlux(const int index, SVector3& q) const {
    if (index > 0) Msg::Error("constitutive field with index %d is not defined",index);
    q = getConstRefToFlux()[index];
  }
  // constitutive field internal energy
  virtual double getConstitutiveExtraDofInternalEnergy(const int index) const {
    if (index > 0) Msg::Error("constitutive field with index %d is not defined",index);
    return getInternalEnergyExtraDofDiffusion();
  }
  // constitutive field capacity per unit field
  virtual double getConstitutiveExtraDofFieldCapacityPerUnitField(const int index) const {
    if (index > 0) Msg::Error("constitutive field with index %d is not defined",index);
    return getConstRefToExtraDofFieldCapacityPerUnitField()(index);
  };
  // mechanical source converted to this constitutive field
  virtual double getConstitutiveExtraDofMechanicalSource(const int index) const {
    if (index > 0) Msg::Error("constitutive field with index %d is not defined",index);
    return getConstRefToMechanicalSource()(index);
  };
  // non-constitutive field value
  virtual void getNonConstitutiveExtraDofFlux(const int index,SVector3& flux) const{
    Msg::Error("ExtraDofDiffusionDG3DIPVariableBase::getNonConstitutiveExtraDofFlux is not defined");
  }

};

class genericCrackPhaseFieldDG3DIPVariable : public dG3DIPVariable
{

 protected:
  IPGenericCrackPhaseField *_pfipv;
  genericCrackPhaseFieldDG3DIPVariable(int _nlVar, const bool createBodyForceHO, const bool oninter): dG3DIPVariable(createBodyForceHO, oninter) {Msg::Error("Cannot build genericCrackPhaseFieldDG3DIPVariable without an internal state");};
  genericCrackPhaseFieldDG3DIPVariable(const bool createBodyForceHO, const bool oninter): dG3DIPVariable(createBodyForceHO, oninter) {Msg::Error("Cannot build genericCrackPhaseFieldDG3DIPVariable without giving the nb non local Var");};

 public:
  genericCrackPhaseFieldDG3DIPVariable(IPGenericCrackPhaseField *pfipv, int _nlVar, const bool createBodyForceHO, const bool oninter);
  genericCrackPhaseFieldDG3DIPVariable(const genericCrackPhaseFieldDG3DIPVariable &source);
  // virtual genericCrackPhaseFieldDG3DIPVariable& operator=(const IPVariable &source);
  genericCrackPhaseFieldDG3DIPVariable& operator=(const IPVariable &source);
  
  virtual void setLocation(const IPVariable::LOCATION loc) {
        dG3DIPVariable::setLocation(loc);
        _pfipv->setLocation(loc);
  };

  virtual IPVariable* getInternalData() {return _pfipv;};
  virtual const IPVariable* getInternalData()  const {return _pfipv;};

  /* specific function */
  IPGenericCrackPhaseField* getRefToIPGenericCrackPhaseField(){return _pfipv;}
  const IPGenericCrackPhaseField* getConstRefToIPGenericCrackPhaseField() const{return _pfipv;}
  virtual ~genericCrackPhaseFieldDG3DIPVariable()
  {
    if (_pfipv) delete _pfipv;
    _pfipv = NULL;
  }

  virtual double get(const int i) const;
  virtual double defoEnergy() const;
  virtual double damageEnergy() const {return getConstRefToIPGenericCrackPhaseField()->damageEnergy();};
  //virtual double damageEnergy() const {return getConstRefToIpMeca()->damageEnergy();};
  virtual double plasticEnergy() const;
  virtual IPVariable* clone() const {return new genericCrackPhaseFieldDG3DIPVariable(*this);};
  virtual void restart();
  
  const IPVariableMechanics& getConstRefToIpMeca() const;
  IPVariableMechanics& getRefToIpMeca();
  void setIpMeca(IPVariable& ipMeca);

  #if defined(HAVE_MPI)
  virtual int numberValuesToCommunicateMPI()const{return _pfipv->numberValuesToCommunicateMPI();}
  virtual void fillValuesToCommunicateMPI(double *arrayMPI)const{_pfipv->fillValuesToCommunicateMPI(arrayMPI);};
  virtual void getValuesFromMPI(const double *arrayMPI){_pfipv->getValuesFromMPI(arrayMPI);};
  #endif // HAVE_MPI

  virtual bool dissipationIsActive() const {return _pfipv->dissipationIsActive();};
  virtual void blockDissipation(const bool fl){if (_pfipv) _pfipv->blockDissipation(fl);};
  virtual bool dissipationIsBlocked() const {
    // if (_pfipv) return getRefToIPGenericCrackPhaseField()->dissipationIsBlocked();
    if (_pfipv) return _pfipv->dissipationIsBlocked();
    else return false;
  }
  // virtual void setLocation(const IPVariable::LOCATION loc) {
  //       dG3DIPVariable::setLocation(loc);
  //       getRefToIPGenericCrackPhaseField()->setLocation(loc);
  //   };
  virtual double getConstRefToLocalVariable(const int idex) const ;
  virtual double& getRefToLocalVariable(const int idex);

  virtual double getConstRefToNonLocalVariableInMaterialLaw(const int idex) const;
  virtual double& getRefToNonLocalVariableInMaterialLaw(const int idex);


  
  
  virtual const STensor3 &getConstRefToCharacteristicLengthMatrix(const int idex) const
  {
    if (idex == 0)
      return getConstRefToIPGenericCrackPhaseField()->getRefToCharacteristicLength();
    {
      Msg::Error("the non-local variable %d is not defined",idex);
      return getConstRefToIPGenericCrackPhaseField()->getConstRefToCharacteristicLength();
    }
  }

  
 private:
  //TODO @MOHIB
  // for path following based on irreversibe energt
  virtual double irreversibleEnergy() const {return getConstRefToIPGenericCrackPhaseField()->irreversibleEnergy();};
  virtual double& getRefToIrreversibleEnergy() {return getRefToIPGenericCrackPhaseField()->getRefToIrreversibleEnergy();};

  //TODO @MOHIB
  virtual const STensor3& getConstRefToDIrreversibleEnergyDDeformationGradient() const
    {return IPGenericCrackPhaseField().getConstRefToDIrreversibleEnergyDF();};
  virtual STensor3& getRefToDIrreversibleEnergyDDeformationGradient()
    {return IPGenericCrackPhaseField().getRefToDIrreversibleEnergyDF();};

  virtual const double& getConstRefToDIrreversibleEnergyDNonLocalVariable(const int index) const
  {
    if (index == 0)
      {
      return getConstRefToIPGenericCrackPhaseField()->getDIrreversibleEnergyDNonLocalVariable();
      }
    else{
      Msg::Error("the non-local variable %d is not defined",index); 
      return getConstRefToIPGenericCrackPhaseField()->getDIrreversibleEnergyDNonLocalVariable();
    }
  };
  virtual double& getRefToDIrreversibleEnergyDDNonLocalVariable(const int index)
  {
    if (index == 0)
    {
      return getRefToIPGenericCrackPhaseField()->getRefToDIrreversibleEnergyDNonLocalVariable();
    }
    else{
      Msg::Error("the non-local variable %d is not defined",index); 
      return getRefToIPGenericCrackPhaseField()->getRefToDIrreversibleEnergyDNonLocalVariable();
    }
  };
  
};



#endif //NONLOCALDAMAGEDG3DIPVARIABLE_H_
