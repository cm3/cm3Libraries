//
// C++ Interface: material law strain gradient
//
// Description: Class with definition of material law for finite strain problem
//
//
// Author: V.D. NGUYEN 2011
//
// Copyright: See COPYING file that comes with this distribution
//
#ifndef HODGMATERIALLAW_H_
#define HODGMATERIALLAW_H_

#include "dG3DMaterialLaw.h"
#include "STensor53.h"
#include "STensor63.h"
#include "numericalMaterial.h"
#include "elasticLaw.h"


class hoDGElasticMaterialLaw : public dG3DMaterialLaw{
  public:
    STensor63 elasticSecondOrderTangent;
    STensor53 elasticSecondFirstTangent;
    STensor53 elasticFirstSecondTangent;
  #ifndef SWIG
  protected:
    elasticLawBase* _elasticFirst,* _elasticSecond;
    elasticLawBase::ELASTIC_TYPE _typeFirst, _typeSecond;
  #endif

  public:
    hoDGElasticMaterialLaw(const int num, const double rho);
    hoDGElasticMaterialLaw(const int num, const int first, const int second,
                           const double K, const double mu, const double rho, const double tol = 1e-6);
    void setType(const int first, const int second = -1);
    void setParameter(const std::string key, const double val);

    #ifndef SWIG
    hoDGElasticMaterialLaw(const hoDGElasticMaterialLaw& src);
    virtual ~hoDGElasticMaterialLaw();


    virtual matname getType() const{return materialLaw::secondOrderElastic;};
    virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw);
    virtual double soundSpeed() const;
    virtual bool isHighOrder() const;
    virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent =false);
    virtual void setElasticStiffness(IPVariable* ipv) const;

    virtual void fillElasticStiffness(elasticLawBase* firstlaw, STensor43 &tangent) const;
    virtual void fillElasticStiffness(elasticLawBase* secondlaw,STensor63& secondsecond);
		virtual materialLaw* clone() const {return new hoDGElasticMaterialLaw(*this);}
		virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{}; // do nothing
    virtual bool withEnergyDissipation() const {return false;};
    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
      Msg::Error("getConstNonLinearSolverMaterialLaw in hoDGElasticMaterialLaw does not exist");
      return NULL;
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
      Msg::Error("getNonLinearSolverMaterialLaw in hoDGElasticMaterialLaw does not exist");
      return NULL;
    }
		#endif //SWIG
};

/**
 full law with w = 0.5*L_ijkl*E_ij*E_kl+ 0.5*J_ijkpqr*G_ijk*G_pqr+b_i*E_jk*G_ijk
**/

class fullHODGElasticMaterialLaw : public hoDGElasticMaterialLaw{
  protected:
    elasticLawBase* _elasticFirstSecond;

  public:
    fullHODGElasticMaterialLaw(const int num, const double rho);
    fullHODGElasticMaterialLaw(const int num, const int first, const int second,
                           const double K, const double mu, const double rho, const double tol = 1e-6);
    void setParameter(const std::string key, const double val);

    #ifndef SWIG
    fullHODGElasticMaterialLaw(const fullHODGElasticMaterialLaw& src);
    virtual ~fullHODGElasticMaterialLaw();


    virtual matname getType() const{return materialLaw::secondOrderElastic;};
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw);

    virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent =false);

    virtual void fillElasticStiffness(elasticLawBase* fs, STensor53& firstsecond, STensor53& secondfirst);
		virtual materialLaw* clone() const {return new fullHODGElasticMaterialLaw(*this);}
		virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{}; // do nothing
    virtual bool withEnergyDissipation() const {return false;};
    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
      Msg::Error("getConstNonLinearSolverMaterialLaw in fullHODGElasticMaterialLaw does not exist");
      return NULL;
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
      Msg::Error("getNonLinearSolverMaterialLaw in fullHODGElasticMaterialLaw does not exist");
      return NULL;
    }
    #endif //SWIG
};



class hoDGMultiscaleMaterialLaw : public dG3DMaterialLaw,
                                  public numericalMaterial{
  public:
    STensor63 elasticSecondOrderTangent;
    STensor53 elasticSecondFirstTangent;
    STensor53 elasticFirstSecondTangent;


  public:
    hoDGMultiscaleMaterialLaw(const int lnum, const int tag);
    #ifndef SWIG
    hoDGMultiscaleMaterialLaw(const hoDGMultiscaleMaterialLaw& src)
                            : dG3DMaterialLaw(src),numericalMaterial(src),elasticFirstSecondTangent(src.elasticFirstSecondTangent),
                            	elasticSecondFirstTangent(src.elasticSecondFirstTangent),elasticSecondOrderTangent(src.elasticSecondOrderTangent){};
    virtual ~hoDGMultiscaleMaterialLaw(){};
    virtual matname getType() const{return materialLaw::numeric;};
    virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;

    virtual void createIPState(const bool isSolve, IPStateBase* &ips,bool hasBodyForce, const bool* state =NULL,
                                 const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt=0) const;
    // To allow initialization of bulk ip in case of fracture
    virtual void createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
    virtual void calledBy2lawFracture(){}
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw);
    virtual double density() const {return _rho;};
    // As to be defined for explicit scheme. If law for implicit only
    // please define this function with return 0
    virtual double soundSpeed() const {return 0;};
    // If law enable fracture the bool checkfrac allow to detect fracture. This bool is unused otherwise
    // The bool is necessery for computation of matrix by perturbation in this case the check of fracture
    // has not to be performed.
    virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent =false);
    virtual bool isHighOrder() const ;
    void fillElasticStiffness(double & rho, STensor43& LF, STensor53& LG, STensor53& JF, STensor63& JG);
    virtual void setElasticStiffness(IPVariable* ipv) const;
    virtual bool isNumeric() const{return true;}
		virtual void initialIPVariable(IPVariable* ipv, bool stiff);
		virtual materialLaw* clone() const {return new hoDGMultiscaleMaterialLaw(*this);}
		virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{}; // do nothing
    virtual bool withEnergyDissipation() const;
    virtual void setMacroSolver(const nonLinearMechSolver* sv){
      dG3DMaterialLaw::setMacroSolver(sv);
      numericalMaterial::_macroSolver = sv;
    };
    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
      Msg::Error("getConstNonLinearSolverMaterialLaw in hoDGMultiscaleMaterialLaw does not exist");
      return NULL;
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
      Msg::Error("getNonLinearSolverMaterialLaw in hoDGMultiscaleMaterialLaw does not exist");
      return NULL;
    }
    #endif
};

#endif // HODGMATERIALLAW_H_
