//
//
// Description: pathfollowing terms
//
// Author:  <Van Dung NGUYEN>, (C) 2016
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "pathFollowingTerms.h"
#include "FractureCohesiveDG3DIPVariable.h"
#include "highOrderTensor.h"

void dG3DDissipationPathFollowingBulkScalarTerm::get(MElement *ele, int npts, IntPt *GP, double &val) const{
	val = 0.;
	const AllIPState::ipstateElementContainer *vips = _ipf->getAips()->getIPstate(ele->getNum());
	for (int i = 0; i < npts; i++){
		const dG3DIPVariableBase* ipv = dynamic_cast<const dG3DIPVariableBase*>((*vips)[i]->getState(IPStateBase::current));
		const dG3DIPVariableBase* ipvprev = dynamic_cast<const dG3DIPVariableBase*>((*vips)[i]->getState(IPStateBase::previous));
		
		double weight = GP[i].weight;
		double detJ = ipv->getJacobianDeterminant(ele,GP[i]);
		double damageDisp = (ipv->irreversibleEnergy() - ipvprev->irreversibleEnergy());
		val += (damageDisp*detJ*weight);
	};
};

void dG3DDissipationPathFollowingBulkLinearTerm::get(MElement *ele,int npts,IntPt *GP,fullVector<double> &m) const{
	const nlsFunctionSpace<double>* sp = dynamic_cast<const nlsFunctionSpace<double>*>(_dom->getFunctionSpace());
  int nbdof = sp->getNumKeys(ele);
	m.resize(nbdof);
	m.setAll(0.);
	
	const AllIPState::ipstateElementContainer *vips = _ipf->getAips()->getIPstate(ele->getNum());
	
	for (int i = 0; i < npts; i++)
	{
		const dG3DIPVariableBase* ipv = dynamic_cast<const dG3DIPVariableBase*>((*vips)[i]->getState(IPStateBase::current));
		
		double weight = GP[i].weight;
		const double & detJ = ipv->getJacobianDeterminant(ele,GP[i]);
		const std::vector<TensorialTraits<double>::GradType> &Grads = ipv->gradf(sp,ele,GP[i]);
		const std::vector<TensorialTraits<double>::ValType> &Vals = ipv->f(sp,ele,GP[i]);
		
		const STensor3& DdissDF = ipv->getConstRefToDIrreversibleEnergyDDeformationGradient();
		int nbFF =  sp->getNumShapeFunctions(ele,0); 
		for (int k=0; k< nbFF; k++){
			for (int l=0; l< 3; l++){
				for (int p=0; p< 3; p++){
					m(k+l*nbFF) += (DdissDF(l,p))*Grads[k+l*nbFF](p)*detJ*weight;
				}
			}
		}
		// plastic energy does not depend on non-local variable  
		int numNonLocalVar = ipv->getNumberNonLocalVariable();
		for (int inl =0; inl < numNonLocalVar; inl++)
    {
			const double & DdamEnergyDNonLocalVar = ipv->getConstRefToDIrreversibleEnergyDNonLocalVariable(inl);
			int nbFFnonlocal = sp->getNumShapeFunctions(ele,3+inl);
      int nbFFTotalLastnonlocal = sp->getShapeFunctionsIndex(ele,3+inl);
      for (int k=0; k< nbFFnonlocal; k++){
				m(k+nbFFTotalLastnonlocal) += DdamEnergyDNonLocalVar*Vals[k+ nbFFTotalLastnonlocal]*detJ*weight;
			}
		}
		
	}	
};

void dG3DDissipationPathFollowingBoundScalarTerm::get(MElement *ele, int npts, IntPt *GP, double &val) const{
	val = 0.;
	MInterfaceElement *ie = dynamic_cast<MInterfaceElement*>(ele);
	if (ie == NULL){
		Msg::Error("MInterfaceElement must be used");
	}
	if ((ie != NULL) and _dom->getFormulation()){
		// get all ipvs
		const AllIPState::ipstateElementContainer *vips = _ipf->getAips()->getIPstate(ele->getNum());
						
		for (int i = 0; i < npts; i++){
			const IPStateBase *ipsm        = (*vips)[i];
			const FractureCohesive3DIPVariable *ipvm     = dynamic_cast<const FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::current));
			const FractureCohesive3DIPVariable *ipvmprev = dynamic_cast<const FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::previous));
			
			if (ipvm != NULL){
				if (ipvm->isbroken()){
					const IPStateBase *ipsp        = (*vips)[i+npts];
					const FractureCohesive3DIPVariable *ipvp     = dynamic_cast<const FractureCohesive3DIPVariable*>(ipsp->getState(IPStateBase::current));
					const FractureCohesive3DIPVariable *ipvpprev = dynamic_cast<const FractureCohesive3DIPVariable*>(ipsp->getState(IPStateBase::previous));
					
					const double weight = GP[i].weight;
					double detJ = ipvm->getJacobianDeterminant(ie,GP[i]);
					
					const Cohesive3DIPVariableBase* cipvm = ipvm->getIPvFrac();
					const Cohesive3DIPVariableBase* cipvmprev = ipvmprev->getIPvFrac();
					const Cohesive3DIPVariableBase* cipvp = ipvp->getIPvFrac();
					const Cohesive3DIPVariableBase* cipvpprev = ipvpprev->getIPvFrac();
					
					double damageDisp = 0.5*(cipvm->irreversibleEnergy() - cipvmprev->irreversibleEnergy())+
															0.5*(cipvp->irreversibleEnergy() - cipvpprev->irreversibleEnergy());
					val += (damageDisp*detJ*weight);
					
					//Msg::Error("ie = %d val = %e",ie->getNum(), val);
				}
			}
		};
		
	}
};

void dG3DDissipationPathFollowingBoundLinearTerm::get(MElement *ele,int npts,IntPt *GP,fullVector<double> &m) const{
	if(_dom->getFormulation())
	{
		const nlsFunctionSpace<double>* spaceMinus = dynamic_cast<const nlsFunctionSpace<double>*>(_dom->getMinusDomain()->getFunctionSpace());
		const nlsFunctionSpace<double>* spacePlus = dynamic_cast<const nlsFunctionSpace<double>*>(_dom->getPlusDomain()->getFunctionSpace());
		
		MInterfaceElement *ie = dynamic_cast<MInterfaceElement*>(ele);
		const int nbdofm = spaceMinus->getNumKeys(ie->getElem(0));
		const int nbdofp = spacePlus->getNumKeys(ie->getElem(1));
		m.resize(nbdofm+nbdofp);
		m.scale(0.);
    
    const int nbFFm = spaceMinus->getNumShapeFunctions(ie->getElem(0),0);
    const int nbFFp = spacePlus->getNumShapeFunctions(ie->getElem(1),0);
		
		const AllIPState::ipstateElementContainer *vips = _ipf->getAips()->getIPstate(ele->getNum());
		// Values on minus and plus elements
		IntPt *GPm; IntPt *GPp;
		_dom->getInterfaceQuadrature()->getIntPoints(ie,GP,&GPm,&GPp);
		
		for(int i=0;i<npts; i++)
		{
			const IPStateBase *ipsm        = (*vips)[i];
			const FractureCohesive3DIPVariable *ipvm     = dynamic_cast<const FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::current));
			const FractureCohesive3DIPVariable *ipvmprev = dynamic_cast<const FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::previous));
			
			if (ipvm != NULL){
				if (ipvm->isbroken()){
					const IPStateBase *ipsp        = (*vips)[i+npts];
					const FractureCohesive3DIPVariable *ipvp     = dynamic_cast<const FractureCohesive3DIPVariable*>(ipsp->getState(IPStateBase::current));
					const FractureCohesive3DIPVariable *ipvpprev = dynamic_cast<const FractureCohesive3DIPVariable*>(ipsp->getState(IPStateBase::previous));
					
					const double weight = GP[i].weight;
					double & detJ = ipvm->getJacobianDeterminant(ie,GP[i]);
					double wJ = detJ*weight;
					
					const Cohesive3DIPVariableBase* cipvm = ipvm->getIPvFrac();
					const Cohesive3DIPVariableBase* cipvp = ipvp->getIPvFrac();
          
          const std::vector<TensorialTraits<double>::ValType> &Valsm = ipvm->f(spaceMinus,ie->getElem(0),GPm[i]);
          const std::vector<TensorialTraits<double>::ValType> &Valsp = ipvp->f(spacePlus,ie->getElem(1),GPp[i]);
					
					static SVector3 dirrEnergDJump;
          dirrEnergDJump = cipvm->getConstRefToDIrreversibleEnergyDJump();
					dirrEnergDJump += cipvp->getConstRefToDIrreversibleEnergyDJump();
					dirrEnergDJump*= 0.5;
          
          
          for(int j=0;j<nbFFm;j++){
						for(int k=0;k<3;k++){
							m(j+k*nbFFm) -= dirrEnergDJump[k]*(Valsm[j+k*nbFFm]*wJ);
						}
					}
					for(int j=0;j<nbFFp;j++){
						for(int k=0;k<3;k++){
							m(j+k*nbFFp+nbdofm) += dirrEnergDJump[k]*(Valsp[j+k*nbFFp]*wJ);
						}
					}
          
          if (cipvm->withDeformationGradient() and cipvp->withDeformationGradient()){
            const STensor3& dirrEnergDFm = cipvm->getConstRefToDIrreversibleEnergyDDeformationGradient();
            const STensor3& dirrEnergDFp = cipvp->getConstRefToDIrreversibleEnergyDDeformationGradient();
            
            const std::vector<TensorialTraits<double>::GradType> &Gradsm = ipvm->gradf(spaceMinus,ie->getElem(0),GPm[i]);
            const std::vector<TensorialTraits<double>::GradType> &Gradsp = ipvp->gradf(spacePlus,ie->getElem(1),GPp[i]);

            // Assembly consistency + stability (if not broken)
            for(int j=0;j<nbFFm;j++){
              for(int k=0;k<3;k++){
                for (int l=0; l<3; l++){
                  m(j+k*nbFFm) += 0.5*dirrEnergDFm(k,l)*Gradsm[j+k*nbFFm](l)*wJ;
                }
              }
            }
            for(int j=0;j<nbFFp;j++){
              for(int k=0;k<3;k++){
                for (int l=0; l<3; l++){
                  m(j+k*nbFFp+nbdofm) += 0.5*dirrEnergDFp(k,l)*Gradsp[j+k*nbFFp](l)*wJ;
                }
              }
            }
          }
          
          if (cipvm->withJumpGradient() and cipvp->withJumpGradient()){
            std::vector<TensorialTraits<double>::GradType>& gradValInt = ipvm->gradf(spaceMinus,ie,GP[i]);
            const STensor3& dirrEnergDGradjumpm = cipvm->getConstRefToDIrreversibleEnergyDJumpGradient();
            const STensor3& dirrEnergDGradjumpp = cipvp->getConstRefToDIrreversibleEnergyDJumpGradient();
            
            int nbvertexInter = ele->getNumVertices();
            std::vector<int> vm;
            vm.resize(nbvertexInter);
            ie->getLocalVertexNum(0,vm);
            std::vector<int> vp;
            vp.resize(nbvertexInter);
            ie->getLocalVertexNum(1,vp);
            
            for(int j=0;j<nbvertexInter;j++){
              for(int k=0;k<3;k++){
                for (int l=0; l<3; l++){
                  m(vm[j]+k*nbFFm) -= 0.5*(dirrEnergDGradjumpm(k,l)+dirrEnergDGradjumpp(k,l))*gradValInt[j+0*nbvertexInter](l)*wJ;
                }
              }
            }
            for(int j=0;j<nbvertexInter;j++){
              for(int k=0;k<3;k++){
                for (int l=0; l<3; l++){
                  m(vp[j]+k*nbFFp+nbdofm) += 0.5*(dirrEnergDGradjumpm(k,l)+dirrEnergDGradjumpp(k,l))*gradValInt[j+0*nbvertexInter](l)*wJ;
                }
              }
            } 
          }
				}
			}
		}
	}
};