//
// C++ Interface: NonLocalDamagePowerYieldHyperDG3DMaterialLaw
//
// Description:
//
//
// Author: V.D. NGUYEN 2014
//
// Copyright: See COPYING file that comes with this distribution
//
#include "NonLocalDamageHyperelasticDG3DMaterialLaw.h"
#include "nonLocalDamageHyperelasticDG3DIPVariable.h"
#include "MInterfaceElement.h"
#include "FractureCohesiveDG3DIPVariable.h"

//
class NonLocalDamagePowerYieldHyperDG3DMaterialLawWithNonLocalBasedFailureCriterion;
NonLocalDamagePowerYieldHyperDG3DMaterialLaw::NonLocalDamagePowerYieldHyperDG3DMaterialLaw(const int num,const double rho,
                   double E,const double nu,const J2IsotropicHardening &comp,
                   const J2IsotropicHardening &trac,
                   const CLengthLaw &_cLLaw,const DamageLaw &_damLaw,const double tol,
                   const bool matrixbyPerturbation , const double pert):
                                                              dG3DMaterialLaw(num,rho,true)
{
  _nldPowerYieldHyperlaw= new mlawNonLocalDamagePowerYieldHyper(num,E,nu,rho,tol,matrixbyPerturbation,pert);
  _nldPowerYieldHyperlaw->setCompressionHardening(comp);
  _nldPowerYieldHyperlaw->setTractionHardening(trac);
  _nldPowerYieldHyperlaw->setCLengthLaw(_cLLaw);
  _nldPowerYieldHyperlaw->setDamageLaw(_damLaw);

  fillElasticStiffness(E, nu, elasticStiffness);
};

//
NonLocalDamagePowerYieldHyperDG3DMaterialLaw::NonLocalDamagePowerYieldHyperDG3DMaterialLaw(const int num,const double rho,
                   double E,const double nu,const J2IsotropicHardening &comp,
                   const J2IsotropicHardening &trac, const kinematicHardening& kin,
                   const CLengthLaw &_cLLaw,const DamageLaw &_damLaw,const double tol,
                   const bool matrixbyPerturbation , const double pert):
                                                              dG3DMaterialLaw(num,rho,true)
{
  _nldPowerYieldHyperlaw= new mlawNonLocalDamagePowerYieldHyper(num,E,nu,rho,tol,matrixbyPerturbation,pert);
  _nldPowerYieldHyperlaw->setCompressionHardening(comp);
  _nldPowerYieldHyperlaw->setTractionHardening(trac);
  _nldPowerYieldHyperlaw->setKinematicHardening(kin);
  _nldPowerYieldHyperlaw->setCLengthLaw(_cLLaw);
  _nldPowerYieldHyperlaw->setDamageLaw(_damLaw);
  fillElasticStiffness(E, nu, elasticStiffness);
};

void NonLocalDamagePowerYieldHyperDG3DMaterialLaw::setYieldPowerFactor(const double n){
  _nldPowerYieldHyperlaw->setPowerFactor(n);
};

void NonLocalDamagePowerYieldHyperDG3DMaterialLaw::nonAssociatedFlowRuleFactor(const double b){
  _nldPowerYieldHyperlaw->nonAssociatedFlowRuleFactor(b);
};

void NonLocalDamagePowerYieldHyperDG3DMaterialLaw::setPlasticPoissonRatio(const double nup){
  _nldPowerYieldHyperlaw->setPlasticPoissonRatio(nup);
};

  void NonLocalDamagePowerYieldHyperDG3DMaterialLaw::setNonAssociatedFlow(const bool flag){
    _nldPowerYieldHyperlaw->setNonAssociatedFlow(flag);
  };
void NonLocalDamagePowerYieldHyperDG3DMaterialLaw::setStrainOrder(const int o){
  _nldPowerYieldHyperlaw->setStrainOrder(o);
};

void NonLocalDamagePowerYieldHyperDG3DMaterialLaw::setViscosityEffect(const viscosityLaw& v,  const double p){
  _nldPowerYieldHyperlaw->setViscosityEffect(v,p);
};

void NonLocalDamagePowerYieldHyperDG3DMaterialLaw::setViscoelasticMethod(const int method){
  _nldPowerYieldHyperlaw->setViscoelasticMethod(method);
};

void NonLocalDamagePowerYieldHyperDG3DMaterialLaw::setViscoElasticNumberOfElement(const int N){
  _nldPowerYieldHyperlaw->setViscoElasticNumberOfElement(N);
};
void NonLocalDamagePowerYieldHyperDG3DMaterialLaw::setViscoElasticData(const int i, const double Ei, const double taui){
  _nldPowerYieldHyperlaw->setViscoElasticData(i,Ei,taui);
};

void NonLocalDamagePowerYieldHyperDG3DMaterialLaw::setViscoElasticData_Bulk(const int i, const double Ki, const double ki){
  _nldPowerYieldHyperlaw->setViscoElasticData_Bulk(i,Ki,ki);
};

void NonLocalDamagePowerYieldHyperDG3DMaterialLaw::setViscoElasticData_Shear(const int i, const double Gi, const double gi){
  _nldPowerYieldHyperlaw->setViscoElasticData_Shear(i,Gi,gi);
};

void NonLocalDamagePowerYieldHyperDG3DMaterialLaw::setViscoElasticData(const std::string filename){
  _nldPowerYieldHyperlaw->setViscoElasticData(filename);
}
void NonLocalDamagePowerYieldHyperDG3DMaterialLaw::setVolumeCorrection(double _vc, double _xivc, double _zetavc, double _dc, double _thetadc, double _pidc){
  _nldPowerYieldHyperlaw->setVolumeCorrection(_vc, _xivc, _zetavc, _dc, _thetadc, _pidc);
};
NonLocalDamagePowerYieldHyperDG3DMaterialLaw::~NonLocalDamagePowerYieldHyperDG3DMaterialLaw(){
  if (_nldPowerYieldHyperlaw) delete _nldPowerYieldHyperlaw;
	_nldPowerYieldHyperlaw = NULL;
};

NonLocalDamagePowerYieldHyperDG3DMaterialLaw::NonLocalDamagePowerYieldHyperDG3DMaterialLaw(const NonLocalDamagePowerYieldHyperDG3DMaterialLaw &source):

    dG3DMaterialLaw(source)
{
	_nldPowerYieldHyperlaw = NULL;
	if (source._nldPowerYieldHyperlaw != NULL){
		_nldPowerYieldHyperlaw = new mlawNonLocalDamagePowerYieldHyper(*(source._nldPowerYieldHyperlaw));
	}
};

void NonLocalDamagePowerYieldHyperDG3DMaterialLaw::setTime(const double t,const double dtime){
  dG3DMaterialLaw::setTime(t,dtime);
  _nldPowerYieldHyperlaw->setTime(t,dtime);
}

materialLaw::matname NonLocalDamagePowerYieldHyperDG3DMaterialLaw::getType() const {return _nldPowerYieldHyperlaw->getType();}

void NonLocalDamagePowerYieldHyperDG3DMaterialLaw::createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);

  if(iele==NULL) inter=false;
  IPVariable* ipvi = new  nonLocalDamageHyperelasticDG3DIPVariable(*_nldPowerYieldHyperlaw,hasBodyForce, inter);
  IPVariable* ipv1 = new  nonLocalDamageHyperelasticDG3DIPVariable(*_nldPowerYieldHyperlaw,hasBodyForce,inter);
  IPVariable* ipv2 = new  nonLocalDamageHyperelasticDG3DIPVariable(*_nldPowerYieldHyperlaw,hasBodyForce,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
  _nldPowerYieldHyperlaw->createIPState((dynamic_cast <nonLocalDamageHyperelasticDG3DIPVariable*> (ipvi))->getIPHyperViscoElastoPlasticNonLocalDamage(),
                         (dynamic_cast <nonLocalDamageHyperelasticDG3DIPVariable*> (ipv1))->getIPHyperViscoElastoPlasticNonLocalDamage(),
             			 (dynamic_cast <nonLocalDamageHyperelasticDG3DIPVariable*> (ipv2))->getIPHyperViscoElastoPlasticNonLocalDamage());

}

void NonLocalDamagePowerYieldHyperDG3DMaterialLaw::createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }

  ipv = new  nonLocalDamageHyperelasticDG3DIPVariable(*_nldPowerYieldHyperlaw,hasBodyForce,inter);
  IPHyperViscoElastoPlasticNonLocalDamage * ipvnl = static_cast <nonLocalDamageHyperelasticDG3DIPVariable*>(ipv)->getIPHyperViscoElastoPlasticNonLocalDamage();
  _nldPowerYieldHyperlaw->createIPVariable(ipvnl, hasBodyForce, ele,nbFF_,GP,gpt);
}

double NonLocalDamagePowerYieldHyperDG3DMaterialLaw::soundSpeed() const{return _nldPowerYieldHyperlaw->soundSpeed();} // or change value ??

void NonLocalDamagePowerYieldHyperDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  /* get ipvariable */
  nonLocalDamageHyperelasticDG3DIPVariable* ipvcur; //= static_cast < nonLocalDamageDG3DIPVariable *> (ipv);
  const nonLocalDamageHyperelasticDG3DIPVariable* ipvprev; //= static_cast <const  nonLocalDamageDG3DIPVariable *> (ipvp);

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<nonLocalDamageHyperelasticDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const nonLocalDamageHyperelasticDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<nonLocalDamageHyperelasticDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const nonLocalDamageHyperelasticDG3DIPVariable*>(ipvp);
  }

  _nldPowerYieldHyperlaw->checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());
};

void NonLocalDamagePowerYieldHyperDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  /* get ipvariable */
  nonLocalDamageHyperelasticDG3DIPVariable* ipvcur; //= static_cast < nonLocalDamageDG3DIPVariable *> (ipv);
  const nonLocalDamageHyperelasticDG3DIPVariable* ipvprev; //= static_cast <const  nonLocalDamageDG3DIPVariable *> (ipvp);

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<nonLocalDamageHyperelasticDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const nonLocalDamageHyperelasticDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<nonLocalDamageHyperelasticDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const nonLocalDamageHyperelasticDG3DIPVariable*>(ipvp);
  }

  /* strain xyz */
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

  /* data for J2 law */
  IPHyperViscoElastoPlasticNonLocalDamage* q1 = ipvcur->getIPHyperViscoElastoPlasticNonLocalDamage();
  const IPHyperViscoElastoPlasticNonLocalDamage* q0 = ipvprev->getIPHyperViscoElastoPlasticNonLocalDamage();

  STensor43& elasticL = ipvcur->getRefToElasticTangentModuli();

  /* compute stress */
  _nldPowerYieldHyperlaw->constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),
                        ipvcur->getRefToDLocalVariableDStrain()[0],ipvcur->getRefToDStressDNonLocalVariable()[0],
                        ipvcur->getRefToDLocalVariableDNonLocalVariable()(0,0),stiff,&elasticL,dTangent,ipvcur->getPtrTodCmdFm());

  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);

}

void NonLocalDamagePowerYieldHyperDG3DMaterialLaw::initialIPVariable(IPVariable* ipv) const{
	dG3DIPVariableBase* dgIPV = dynamic_cast<dG3DIPVariableBase*>(ipv);
	if (dgIPV!=NULL){
		dgIPV->setRefToDGElasticTangentModuli(this->elasticStiffness);
	}
	else{
		Msg::Error("dG3DIPVariableBase must be used dG3DLinearElasticMaterialLaw::initialIPVariable");
	}
};

//
LocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::LocalDamagePowerYieldHyperDG3DMaterialLawWithFailure(const int num,const double rho,
                   double E,const double nu,const J2IsotropicHardening &comp,
                   const J2IsotropicHardening &trac,
                   const DamageLaw &_damLaw1, const DamageLaw &_damLaw2, const double tol,
                   const bool matrixbyPerturbation , const double pert):
                                                              dG3DMaterialLaw(num,rho,true)
{
  _localPowerYieldHyperlaw= new mlawLocalDamagePowerYieldHyperWithFailure(num,E,nu,rho,tol,matrixbyPerturbation,pert);
  _localPowerYieldHyperlaw->setCompressionHardening(comp);
  _localPowerYieldHyperlaw->setTractionHardening(trac);

  _localPowerYieldHyperlaw->clearAllDamageLaw();
  _localPowerYieldHyperlaw->setDamageLaw(_damLaw1);
  _localPowerYieldHyperlaw->setDamageLaw(_damLaw2);

  fillElasticStiffness(E, nu, elasticStiffness);
};

//
LocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::LocalDamagePowerYieldHyperDG3DMaterialLawWithFailure(const int num,const double rho,
                   double E,const double nu,const J2IsotropicHardening &comp,
                   const J2IsotropicHardening &trac, const kinematicHardening& kin,
                   const DamageLaw &_damLaw1, const DamageLaw &_damLaw2, const double tol,
                   const bool matrixbyPerturbation , const double pert):
                                                              dG3DMaterialLaw(num,rho,true)
{
  _localPowerYieldHyperlaw= new mlawLocalDamagePowerYieldHyperWithFailure(num,E,nu,rho,tol,matrixbyPerturbation,pert);
  _localPowerYieldHyperlaw->setCompressionHardening(comp);
  _localPowerYieldHyperlaw->setTractionHardening(trac);
  _localPowerYieldHyperlaw->setKinematicHardening(kin);

  _localPowerYieldHyperlaw->clearAllDamageLaw();
  _localPowerYieldHyperlaw->setDamageLaw(_damLaw1);
  _localPowerYieldHyperlaw->setDamageLaw(_damLaw2);


  fillElasticStiffness(E, nu, elasticStiffness);
};

void LocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::setYieldPowerFactor(const double n){
  _localPowerYieldHyperlaw->setPowerFactor(n);
};

void LocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::nonAssociatedFlowRuleFactor(const double b){
  _localPowerYieldHyperlaw->nonAssociatedFlowRuleFactor(b);
};

void LocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::setPlasticPoissonRatio(const double nup){
  _localPowerYieldHyperlaw->setPlasticPoissonRatio(nup);
};

void LocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::setNonAssociatedFlow(const bool flag){
  _localPowerYieldHyperlaw->setNonAssociatedFlow(flag);
};
void LocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::setStrainOrder(const int o){
  _localPowerYieldHyperlaw->setStrainOrder(o);
};

void LocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::setViscosityEffect(const viscosityLaw& v,  const double p){
  _localPowerYieldHyperlaw->setViscosityEffect(v,p);
};

void LocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::setViscoelasticMethod(const int method){
  _localPowerYieldHyperlaw->setViscoelasticMethod(method);
};

void LocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::setViscoElasticNumberOfElement(const int N){
  _localPowerYieldHyperlaw->setViscoElasticNumberOfElement(N);
};
void LocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::setViscoElasticData(const int i, const double Ei, const double taui){
  _localPowerYieldHyperlaw->setViscoElasticData(i,Ei,taui);
};

void LocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::setViscoElasticData_Bulk(const int i, const double Ki, const double ki){
  _localPowerYieldHyperlaw->setViscoElasticData_Bulk(i,Ki,ki);
};

void LocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::setViscoElasticData_Shear(const int i, const double Gi, const double gi){
  _localPowerYieldHyperlaw->setViscoElasticData_Shear(i,Gi,gi);
};

void LocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::setViscoElasticData(const std::string filename){
  _localPowerYieldHyperlaw->setViscoElasticData(filename);
}
void LocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::setVolumeCorrection(double _vc, double _xivc, double _zetavc, double _dc, double _thetadc, double _pidc){
  _localPowerYieldHyperlaw->setVolumeCorrection(_vc, _xivc, _zetavc, _dc, _thetadc, _pidc);
};
void LocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::setFailureCriterion(const mlawHyperelasticFailureCriterionBase& fCr){
  _localPowerYieldHyperlaw->setFailureCriterion(fCr);
};
LocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::~LocalDamagePowerYieldHyperDG3DMaterialLawWithFailure(){
  if (_localPowerYieldHyperlaw) delete _localPowerYieldHyperlaw;
	_localPowerYieldHyperlaw = NULL;
};

LocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::LocalDamagePowerYieldHyperDG3DMaterialLawWithFailure(const LocalDamagePowerYieldHyperDG3DMaterialLawWithFailure &source):

    dG3DMaterialLaw(source)
{
	_localPowerYieldHyperlaw = NULL;
	if (source._localPowerYieldHyperlaw){
		_localPowerYieldHyperlaw = new mlawLocalDamagePowerYieldHyperWithFailure(*(source._localPowerYieldHyperlaw));
	}
};

void LocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::setTime(const double t,const double dtime){
  dG3DMaterialLaw::setTime(t,dtime);
  _localPowerYieldHyperlaw->setTime(t,dtime);
}

materialLaw::matname LocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::getType() const {return _localPowerYieldHyperlaw->getType();}

void LocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);

  if(iele==NULL) inter=false;
  IPVariable* ipvi = new  localDamageHyperelasticDG3DIPVariableWithFailure(*_localPowerYieldHyperlaw,hasBodyForce,inter);
  IPVariable* ipv1 = new  localDamageHyperelasticDG3DIPVariableWithFailure(*_localPowerYieldHyperlaw,hasBodyForce,inter);
  IPVariable* ipv2 = new  localDamageHyperelasticDG3DIPVariableWithFailure(*_localPowerYieldHyperlaw,hasBodyForce,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void LocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  ipv = new  localDamageHyperelasticDG3DIPVariableWithFailure(*_localPowerYieldHyperlaw,hasBodyForce,inter);
}

double LocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::soundSpeed() const{return _localPowerYieldHyperlaw->soundSpeed();} // or change value ??

void LocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
   /* get ipvariable */
  localDamageHyperelasticDG3DIPVariableWithFailure* ipvcur = NULL;
  const localDamageHyperelasticDG3DIPVariableWithFailure* ipvprev = NULL;

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<localDamageHyperelasticDG3DIPVariableWithFailure*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const localDamageHyperelasticDG3DIPVariableWithFailure*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<localDamageHyperelasticDG3DIPVariableWithFailure*>(ipv);
    ipvprev = static_cast<const localDamageHyperelasticDG3DIPVariableWithFailure*>(ipvp);
  }

  _localPowerYieldHyperlaw->checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());
};

void LocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  /* get ipvariable */
  localDamageHyperelasticDG3DIPVariableWithFailure* ipvcur = NULL;
  const localDamageHyperelasticDG3DIPVariableWithFailure* ipvprev = NULL;

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<localDamageHyperelasticDG3DIPVariableWithFailure*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const localDamageHyperelasticDG3DIPVariableWithFailure*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<localDamageHyperelasticDG3DIPVariableWithFailure*>(ipv);
    ipvprev = static_cast<const localDamageHyperelasticDG3DIPVariableWithFailure*>(ipvp);
  }

  /* strain xyz */
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

  /* data for J2 law */
  IPHyperViscoElastoPlasticMultipleLocalDamage* q1 = ipvcur->getIPHyperViscoElastoPlasticMultipleLocalDamage();
  const IPHyperViscoElastoPlasticMultipleLocalDamage* q0 = ipvprev->getIPHyperViscoElastoPlasticMultipleLocalDamage();

  /* compute stress */
  _localPowerYieldHyperlaw->constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),stiff,dTangent,ipvcur->getPtrTodCmdFm());

  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);
}

void LocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::initialIPVariable(IPVariable* ipv) const{
	dG3DIPVariableBase* dgIPV = dynamic_cast<dG3DIPVariableBase*>(ipv);
	if (dgIPV!=NULL){
		dgIPV->setRefToDGElasticTangentModuli(this->elasticStiffness);
	}
	else{
		Msg::Error("dG3DIPVariableBase must be used dG3DLinearElasticMaterialLaw::initialIPVariable");
	}
};

//
NonLocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::NonLocalDamagePowerYieldHyperDG3DMaterialLawWithFailure(const int num,const double rho,
                   double E,const double nu,const J2IsotropicHardening &comp,
                   const J2IsotropicHardening &trac,
                   const CLengthLaw &_cLLaw1, const CLengthLaw &_cLLaw2,
                   const DamageLaw &_damLaw1, const DamageLaw &_damLaw2, const double tol,
                   const bool matrixbyPerturbation , const double pert):
                                                              dG3DMaterialLaw(num,rho,true)
{
  _nldPowerYieldHyperlaw= new mlawNonLocalDamagePowerYieldHyperWithFailure(num,E,nu,rho,tol,matrixbyPerturbation,pert);
  _nldPowerYieldHyperlaw->setCompressionHardening(comp);
  _nldPowerYieldHyperlaw->setTractionHardening(trac);

  _nldPowerYieldHyperlaw->setCLengthLaw(_cLLaw1);
  _nldPowerYieldHyperlaw->setCLengthLaw(_cLLaw2);

  _nldPowerYieldHyperlaw->setDamageLaw(_damLaw1);
  _nldPowerYieldHyperlaw->setDamageLaw(_damLaw2);

  fillElasticStiffness(E, nu, elasticStiffness);
};

//
NonLocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::NonLocalDamagePowerYieldHyperDG3DMaterialLawWithFailure(const int num,const double rho,
                   double E,const double nu,const J2IsotropicHardening &comp,
                   const J2IsotropicHardening &trac, const kinematicHardening& kin,
                   const CLengthLaw &_cLLaw1, const CLengthLaw &_cLLaw2,
                   const DamageLaw &_damLaw1, const DamageLaw &_damLaw2, const double tol,
                   const bool matrixbyPerturbation , const double pert):
                                                              dG3DMaterialLaw(num,rho,true)
{
  _nldPowerYieldHyperlaw= new mlawNonLocalDamagePowerYieldHyperWithFailure(num,E,nu,rho,tol,matrixbyPerturbation,pert);
  _nldPowerYieldHyperlaw->setCompressionHardening(comp);
  _nldPowerYieldHyperlaw->setTractionHardening(trac);
  _nldPowerYieldHyperlaw->setKinematicHardening(kin);

  _nldPowerYieldHyperlaw->setCLengthLaw(_cLLaw1);
  _nldPowerYieldHyperlaw->setCLengthLaw(_cLLaw2);

  _nldPowerYieldHyperlaw->setDamageLaw(_damLaw1);
  _nldPowerYieldHyperlaw->setDamageLaw(_damLaw2);


  fillElasticStiffness(E, nu, elasticStiffness);
};


void NonLocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::setYieldPowerFactor(const double n){
  _nldPowerYieldHyperlaw->setPowerFactor(n);
};

void NonLocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::nonAssociatedFlowRuleFactor(const double b){
  _nldPowerYieldHyperlaw->nonAssociatedFlowRuleFactor(b);
};

void NonLocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::setPlasticPoissonRatio(const double nup){
  _nldPowerYieldHyperlaw->setPlasticPoissonRatio(nup);
};

  void NonLocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::setNonAssociatedFlow(const bool flag){
    _nldPowerYieldHyperlaw->setNonAssociatedFlow(flag);
  };
void NonLocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::setStrainOrder(const int o){
  _nldPowerYieldHyperlaw->setStrainOrder(o);
};

void NonLocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::setViscosityEffect(const viscosityLaw& v,  const double p){
  _nldPowerYieldHyperlaw->setViscosityEffect(v,p);
};

void NonLocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::setViscoelasticMethod(const int method){
  _nldPowerYieldHyperlaw->setViscoelasticMethod(method);
};

void NonLocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::setViscoElasticNumberOfElement(const int N){
  _nldPowerYieldHyperlaw->setViscoElasticNumberOfElement(N);
};
void NonLocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::setViscoElasticData(const int i, const double Ei, const double taui){
  _nldPowerYieldHyperlaw->setViscoElasticData(i,Ei,taui);
};

void NonLocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::setViscoElasticData_Bulk(const int i, const double Ki, const double ki){
  _nldPowerYieldHyperlaw->setViscoElasticData_Bulk(i,Ki,ki);
};

void NonLocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::setViscoElasticData_Shear(const int i, const double Gi, const double gi){
  _nldPowerYieldHyperlaw->setViscoElasticData_Shear(i,Gi,gi);
};

void NonLocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::setViscoElasticData(const std::string filename){
  _nldPowerYieldHyperlaw->setViscoElasticData(filename);
}
void NonLocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::setVolumeCorrection(double _vc, double _xivc, double _zetavc, double _dc, double _thetadc, double _pidc){
  _nldPowerYieldHyperlaw->setVolumeCorrection(_vc, _xivc, _zetavc, _dc, _thetadc, _pidc);
};
void NonLocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::setFailureCriterion(const mlawHyperelasticFailureCriterionBase& fCr){
  _nldPowerYieldHyperlaw->setFailureCriterion(fCr);
};

void NonLocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::setCritialDamage(const double D){
  _nldPowerYieldHyperlaw->setCritialDamage(D);
};

NonLocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::~NonLocalDamagePowerYieldHyperDG3DMaterialLawWithFailure(){
  if (_nldPowerYieldHyperlaw) delete _nldPowerYieldHyperlaw;
	_nldPowerYieldHyperlaw = NULL;
};

NonLocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::NonLocalDamagePowerYieldHyperDG3DMaterialLawWithFailure(const NonLocalDamagePowerYieldHyperDG3DMaterialLawWithFailure &source):

    dG3DMaterialLaw(source)
{
	_nldPowerYieldHyperlaw = NULL;
	if (source._nldPowerYieldHyperlaw){
		_nldPowerYieldHyperlaw = new mlawNonLocalDamagePowerYieldHyperWithFailure(*(source._nldPowerYieldHyperlaw));
	}
};

void NonLocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::setTime(const double t,const double dtime){
  dG3DMaterialLaw::setTime(t,dtime);
  _nldPowerYieldHyperlaw->setTime(t,dtime);
}

materialLaw::matname NonLocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::getType() const {return _nldPowerYieldHyperlaw->getType();}

void NonLocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);

  if(iele==NULL) inter=false;
  IPVariable* ipvi = new  nonLocalDamageHyperelasticDG3DIPVariableWithFailure(*_nldPowerYieldHyperlaw,hasBodyForce,inter);
  IPVariable* ipv1 = new  nonLocalDamageHyperelasticDG3DIPVariableWithFailure(*_nldPowerYieldHyperlaw,hasBodyForce,inter);
  IPVariable* ipv2 = new  nonLocalDamageHyperelasticDG3DIPVariableWithFailure(*_nldPowerYieldHyperlaw,hasBodyForce,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
  _nldPowerYieldHyperlaw->createIPState((dynamic_cast <nonLocalDamageHyperelasticDG3DIPVariableWithFailure*> (ipvi))->getIPHyperViscoElastoPlasticMultipleNonLocalDamage(),
                         (dynamic_cast <nonLocalDamageHyperelasticDG3DIPVariableWithFailure*> (ipv1))->getIPHyperViscoElastoPlasticMultipleNonLocalDamage(),
             			 (dynamic_cast <nonLocalDamageHyperelasticDG3DIPVariableWithFailure*> (ipv2))->getIPHyperViscoElastoPlasticMultipleNonLocalDamage());

}

void NonLocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }

  ipv = new  nonLocalDamageHyperelasticDG3DIPVariableWithFailure(*_nldPowerYieldHyperlaw,hasBodyForce,inter);
  IPHyperViscoElastoPlasticMultipleNonLocalDamage * ipvnl = static_cast <nonLocalDamageHyperelasticDG3DIPVariableWithFailure*>(ipv)->getIPHyperViscoElastoPlasticMultipleNonLocalDamage();
  _nldPowerYieldHyperlaw->createIPVariable(ipvnl, hasBodyForce, ele,nbFF_,GP,gpt);
}

double NonLocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::soundSpeed() const{return _nldPowerYieldHyperlaw->soundSpeed();} // or change value ??

void NonLocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  /* get ipvariable */
  nonLocalDamageHyperelasticDG3DIPVariableWithFailure* ipvcur = NULL;
  const nonLocalDamageHyperelasticDG3DIPVariableWithFailure* ipvprev = NULL;

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<nonLocalDamageHyperelasticDG3DIPVariableWithFailure*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const nonLocalDamageHyperelasticDG3DIPVariableWithFailure*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<nonLocalDamageHyperelasticDG3DIPVariableWithFailure*>(ipv);
    ipvprev = static_cast<const nonLocalDamageHyperelasticDG3DIPVariableWithFailure*>(ipvp);
  }

  _nldPowerYieldHyperlaw->checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());
};

void NonLocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  /* get ipvariable */
  nonLocalDamageHyperelasticDG3DIPVariableWithFailure* ipvcur = NULL;
  const nonLocalDamageHyperelasticDG3DIPVariableWithFailure* ipvprev = NULL;

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<nonLocalDamageHyperelasticDG3DIPVariableWithFailure*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const nonLocalDamageHyperelasticDG3DIPVariableWithFailure*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<nonLocalDamageHyperelasticDG3DIPVariableWithFailure*>(ipv);
    ipvprev = static_cast<const nonLocalDamageHyperelasticDG3DIPVariableWithFailure*>(ipvp);
  }

  /* strain xyz */
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

  /* data for J2 law */
  IPHyperViscoElastoPlasticMultipleNonLocalDamage* q1 = ipvcur->getIPHyperViscoElastoPlasticMultipleNonLocalDamage();
  const IPHyperViscoElastoPlasticMultipleNonLocalDamage* q0 = ipvprev->getIPHyperViscoElastoPlasticMultipleNonLocalDamage();

  STensor43& elasticL = ipvcur->getRefToElasticTangentModuli();

  /* compute stress */
  _nldPowerYieldHyperlaw->constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),
                        ipvcur->getRefToDLocalVariableDStrain(),ipvcur->getRefToDStressDNonLocalVariable(),
                        ipvcur->getRefToDLocalVariableDNonLocalVariable(),stiff,&elasticL,dTangent,ipvcur->getPtrTodCmdFm());

  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);
}

void NonLocalDamagePowerYieldHyperDG3DMaterialLawWithFailure::initialIPVariable(IPVariable* ipv) const{
	dG3DIPVariableBase* dgIPV = dynamic_cast<dG3DIPVariableBase*>(ipv);
	if (dgIPV!=NULL){
		dgIPV->setRefToDGElasticTangentModuli(this->elasticStiffness);
	}
	else{
		Msg::Error("dG3DIPVariableBase must be used dG3DLinearElasticMaterialLaw::initialIPVariable");
	}
};

// ############################################################################## END CLASS - START TVEVP ########################################################################################

// Why and what is this?
class NonLocalDamageTVEVPDG3DMaterialLawWithNonLocalBasedFailureCriterion;

//
NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::NonLocalDamageNonLinearTVEVPDG3DMaterialLaw(const int num, const double E, const double nu, const double rho,
                              const double Tinitial, const double Alpha, const double KThCon, const double Cp,
                              const J2IsotropicHardening &comp, const J2IsotropicHardening &trac, const CLengthLaw &_cLLaw, const DamageLaw &_damLaw,
                              const double tol, const bool matrixbyPerturbation, const double pert,
                              const double stressIteratorTol, const bool thermalEstimationPreviousConfig):
                                                              dG3DMaterialLaw(num,rho,true)
{
  _nldNonLinearTVEVP= new mlawNonLocalDamagaNonLinearTVEVP(num,E,nu,rho,tol,Tinitial,Alpha,KThCon,Cp,matrixbyPerturbation,pert,stressIteratorTol,thermalEstimationPreviousConfig);
  _nldNonLinearTVEVP->setCompressionHardening(comp);
  _nldNonLinearTVEVP->setTractionHardening(trac);
  _nldNonLinearTVEVP->setCLengthLaw(_cLLaw);
  _nldNonLinearTVEVP->setDamageLaw(_damLaw);
  fillElasticStiffness(E, nu, elasticStiffness);
};

NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::NonLocalDamageNonLinearTVEVPDG3DMaterialLaw(const int num, const double E, const double nu, const double rho,
                              const double Tinitial, const double Alpha, const double KThCon, const double Cp,
                              const J2IsotropicHardening &comp, const J2IsotropicHardening &trac, const kinematicHardening& kin, const CLengthLaw &_cLLaw, const DamageLaw &_damLaw,
                              const double tol, const bool matrixbyPerturbation, const double pert,
                              const double stressIteratorTol, const bool thermalEstimationPreviousConfig):
                                                              dG3DMaterialLaw(num,rho,true)
{
  _nldNonLinearTVEVP= new mlawNonLocalDamagaNonLinearTVEVP(num,E,nu,rho,tol,Tinitial,Alpha,KThCon,Cp,matrixbyPerturbation,pert,stressIteratorTol,thermalEstimationPreviousConfig);
  _nldNonLinearTVEVP->setCompressionHardening(comp);
  _nldNonLinearTVEVP->setTractionHardening(trac);
  _nldNonLinearTVEVP->setKinematicHardening(kin);
  _nldNonLinearTVEVP->setCLengthLaw(_cLLaw);
  _nldNonLinearTVEVP->setDamageLaw(_damLaw);
  fillElasticStiffness(E, nu, elasticStiffness);
};

void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::setStrainOrder(const int order){
  _nldNonLinearTVEVP->setStrainOrder(order);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::setViscoelasticMethod(const int method){
  _nldNonLinearTVEVP->setViscoelasticMethod(method);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::setViscoElasticNumberOfElement(const int N){
  _nldNonLinearTVEVP->setViscoElasticNumberOfElement(N);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::setViscoElasticData(const int i, const double Ei, const double taui){
  _nldNonLinearTVEVP->setViscoElasticData(i,Ei,taui);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::setViscoElasticData_Bulk(const int i, const double Ki, const double ki){
  _nldNonLinearTVEVP->setViscoElasticData_Bulk(i,Ki,ki);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::setViscoElasticData_Shear(const int i, const double Gi, const double gi){
  _nldNonLinearTVEVP->setViscoElasticData_Shear(i,Gi,gi);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::setViscoElasticData(const std::string filename){
  _nldNonLinearTVEVP->setViscoElasticData(filename);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::setIsotropicHardeningCoefficients(const double HR1, const double HR2, const double HR3){
  _nldNonLinearTVEVP->setIsotropicHardeningCoefficients(HR1,HR2,HR3);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::setPolynomialOrderChabocheCoeffs(const int order){
  _nldNonLinearTVEVP->setPolynomialOrderChabocheCoeffs(order);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::setPolynomialCoeffsChabocheCoeffs(const int i, const double val){
  _nldNonLinearTVEVP->setPolynomialCoeffsChabocheCoeffs(i,val);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::setReferenceTemperature(const double Tref){
  _nldNonLinearTVEVP->setReferenceTemperature(Tref);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::setShiftFactorConstantsWLF(const double C1, const double C2){
  _nldNonLinearTVEVP->setShiftFactorConstantsWLF(C1,C2);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::setTemperatureFunction_BulkModulus(const scalarFunction& Tfunc){
  _nldNonLinearTVEVP->setTemperatureFunction_BulkModulus(Tfunc);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::setTemperatureFunction_ShearModulus(const scalarFunction& Tfunc){
  _nldNonLinearTVEVP->setTemperatureFunction_ShearModulus(Tfunc);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::setTemperatureFunction_ThermalDilationCoefficient(const scalarFunction& Tfunc){
  _nldNonLinearTVEVP->setTemperatureFunction_ThermalDilationCoefficient(Tfunc);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::setTemperatureFunction_ThermalConductivity(const scalarFunction& Tfunc){
  _nldNonLinearTVEVP->setTemperatureFunction_ThermalConductivity(Tfunc);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::setTemperatureFunction_SpecificHeat(const scalarFunction& Tfunc){
  _nldNonLinearTVEVP->setTemperatureFunction_SpecificHeat(Tfunc);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::setTemperatureFunction_ElasticCorrection_Bulk(const scalarFunction& Tfunc){
  _nldNonLinearTVEVP->setTemperatureFunction_ElasticCorrection_Bulk(Tfunc);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::setTemperatureFunction_ElasticCorrection_Shear(const scalarFunction& Tfunc){
  _nldNonLinearTVEVP->setTemperatureFunction_ElasticCorrection_Shear(Tfunc);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::setViscosityEffect(const viscosityLaw& v, const double p){
  _nldNonLinearTVEVP->setViscosityEffect(v,p);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::setMullinsEffect(const mullinsEffect& mullins){
  _nldNonLinearTVEVP->setMullinsEffect(mullins);
};

void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::setYieldPowerFactor(const double n){
  _nldNonLinearTVEVP->setPowerFactor(n);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::nonAssociatedFlowRuleFactor(const double b){
  _nldNonLinearTVEVP->nonAssociatedFlowRuleFactor(b);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::setPlasticPoissonRatio(const double nup){
  _nldNonLinearTVEVP->setPlasticPoissonRatio(nup);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::setNonAssociatedFlow(const bool flag){
  _nldNonLinearTVEVP->setNonAssociatedFlow(flag);
};

void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::setTaylorQuineyFactor(const double f, const bool flag){
  _nldNonLinearTVEVP->setTaylorQuineyFactor(f, flag);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::setTemperatureFunction_InitialYieldStress(const scalarFunction& Tfunc){
  _nldNonLinearTVEVP->setTemperatureFunction_InitialYieldStress(Tfunc);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::setTemperatureFunction_Hardening(const scalarFunction& Tfunc){
  _nldNonLinearTVEVP->setTemperatureFunction_Hardening(Tfunc);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::setTemperatureFunction_KinematicHardening(const scalarFunction& Tfunc){
  _nldNonLinearTVEVP->setTemperatureFunction_KinematicHardening(Tfunc);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::setTemperatureFunction_Viscosity(const scalarFunction& Tfunc){
  _nldNonLinearTVEVP->setTemperatureFunction_Viscosity(Tfunc);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::useExtraBranchBool(const bool useExtraBranch){
  _nldNonLinearTVEVP->useExtraBranchBool(useExtraBranch);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::useExtraBranchBool_TVE(const bool useExtraBranch_TVE, const int ExtraBranch_TVE_option){
  _nldNonLinearTVEVP->useExtraBranchBool_TVE(useExtraBranch_TVE, ExtraBranch_TVE_option);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::useRotationCorrectionBool(const bool useRotationCorrection,  const int rotationCorrectionScheme){
  _nldNonLinearTVEVP->useRotationCorrectionBool(useRotationCorrection,rotationCorrectionScheme);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::setVolumeCorrection(const double _vc, const double _xivc, const double _zetavc, const double _dc, const double _thetadc, const double _pidc){
  _nldNonLinearTVEVP->setVolumeCorrection(_vc,_xivc,_zetavc,_dc,_thetadc,_pidc);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::setAdditionalVolumeCorrections(const double V3, const double V4, const double V5, const double D3, const double D4, const double D5){
  _nldNonLinearTVEVP->setAdditionalVolumeCorrections(V3,V4,V5,D3,D4,D5);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::setExtraBranch_CompressionParameter(const double compCorrection, const double compCorrection_2, const double compCorrection_3){
  _nldNonLinearTVEVP->setExtraBranch_CompressionParameter(compCorrection,compCorrection_2,compCorrection_3);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::setExtraBranchNLType(const int method){
  _nldNonLinearTVEVP->setExtraBranchNLType(method);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::setTensionCompressionRegularisation(const double tensionCompressionRegularisation){
  _nldNonLinearTVEVP->setTensionCompressionRegularisation(tensionCompressionRegularisation);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::setCorrectionsAllBranchesTVE(const int i, const double V0,const double V1, const double V2, const double D0,const double D1, const double D2){
  _nldNonLinearTVEVP->setCorrectionsAllBranchesTVE(i,V0,V1,V2,D0,D1,D2);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::setAdditionalCorrectionsAllBranchesTVE(const int i, const double V3, const double V4, const double V5, const double D3, const double D4, const double D5){
  _nldNonLinearTVEVP->setAdditionalCorrectionsAllBranchesTVE(i,V3,V4,V5,D3,D4,D5);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::setCompressionCorrectionsAllBranchesTVE(const int i, const double Ci){
  _nldNonLinearTVEVP->setCompressionCorrectionsAllBranchesTVE(i,Ci);
};

NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::~NonLocalDamageNonLinearTVEVPDG3DMaterialLaw(){
  if (_nldNonLinearTVEVP) delete _nldNonLinearTVEVP;
	_nldNonLinearTVEVP = NULL;
};

NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::NonLocalDamageNonLinearTVEVPDG3DMaterialLaw(const NonLocalDamageNonLinearTVEVPDG3DMaterialLaw &source):
    dG3DMaterialLaw(source)
{
	_nldNonLinearTVEVP = NULL;
	if (source._nldNonLinearTVEVP != NULL){
		_nldNonLinearTVEVP = new mlawNonLocalDamagaNonLinearTVEVP(*(source._nldNonLinearTVEVP));
	}
};

void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::setTime(const double t,const double dtime){
  dG3DMaterialLaw::setTime(t,dtime);
  _nldNonLinearTVEVP->setTime(t,dtime);
}

materialLaw::matname NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::getType() const {return _nldNonLinearTVEVP->getType();}

void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);

  if(iele==NULL) inter=false;
  IPVariable* ipvi = new  nonLocalDamageNonLinearTVEVPDG3DIPVariable(*_nldNonLinearTVEVP,hasBodyForce,inter);
  IPVariable* ipv1 = new  nonLocalDamageNonLinearTVEVPDG3DIPVariable(*_nldNonLinearTVEVP,hasBodyForce,inter);
  IPVariable* ipv2 = new  nonLocalDamageNonLinearTVEVPDG3DIPVariable(*_nldNonLinearTVEVP,hasBodyForce,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
  _nldNonLinearTVEVP->createIPState((dynamic_cast <nonLocalDamageNonLinearTVEVPDG3DIPVariable*> (ipvi))->getIPNonLinearTVEVPNonLocalDamage(),
                         (dynamic_cast <nonLocalDamageNonLinearTVEVPDG3DIPVariable*> (ipv1))->getIPNonLinearTVEVPNonLocalDamage(),
             			 (dynamic_cast <nonLocalDamageNonLinearTVEVPDG3DIPVariable*> (ipv2))->getIPNonLinearTVEVPNonLocalDamage());

}

void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }

  ipv = new  nonLocalDamageNonLinearTVEVPDG3DIPVariable(*_nldNonLinearTVEVP,hasBodyForce,inter);
  IPNonLinearTVEVPNonLocalDamage * ipvnl = static_cast <nonLocalDamageNonLinearTVEVPDG3DIPVariable*>(ipv)->getIPNonLinearTVEVPNonLocalDamage();
  _nldNonLinearTVEVP->createIPVariable(ipvnl, hasBodyForce, ele,nbFF_,GP,gpt);
}

double NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::soundSpeed() const{return _nldNonLinearTVEVP->soundSpeed();} // or change value ??

void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  /* get ipvariable */
  nonLocalDamageNonLinearTVEVPDG3DIPVariable* ipvcur; //= static_cast < nonLocalDamageDG3DIPVariable *> (ipv);
  const nonLocalDamageNonLinearTVEVPDG3DIPVariable* ipvprev; //= static_cast <const  nonLocalDamageDG3DIPVariable *> (ipvp);

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<nonLocalDamageNonLinearTVEVPDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const nonLocalDamageNonLinearTVEVPDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<nonLocalDamageNonLinearTVEVPDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const nonLocalDamageNonLinearTVEVPDG3DIPVariable*>(ipvp);
  }

  _nldNonLinearTVEVP->checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());
};

void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  /* get ipvariable */
  nonLocalDamageNonLinearTVEVPDG3DIPVariable* ipvcur; //= static_cast < nonLocalDamageDG3DIPVariable *> (ipv);
  const nonLocalDamageNonLinearTVEVPDG3DIPVariable* ipvprev; //= static_cast <const  nonLocalDamageDG3DIPVariable *> (ipvp);

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<nonLocalDamageNonLinearTVEVPDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const nonLocalDamageNonLinearTVEVPDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<nonLocalDamageNonLinearTVEVPDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const nonLocalDamageNonLinearTVEVPDG3DIPVariable*>(ipvp);
  }

  /* strain xyz */
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

  /* data for J2 law */
  IPNonLinearTVEVPNonLocalDamage* q1 = ipvcur->getIPNonLinearTVEVPNonLocalDamage();
  const IPNonLinearTVEVPNonLocalDamage* q0 = ipvprev->getIPNonLinearTVEVPNonLocalDamage();

  double& Cp = ipvcur->getRefToExtraDofFieldCapacityPerUnitField()(0);
  STensor43& elasticL = ipvcur->getRefToElasticTangentModuli();

  /* compute stress */
  _nldNonLinearTVEVP->constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),

                        ipvcur->getRefToDLocalVariableDStrain()[0],ipvcur->getRefToDStressDNonLocalVariable()[0],
                        ipvcur->getRefToDLocalVariableDNonLocalVariable()(0,0),

                        ipvcur->getRefTodPdField()[0], ipvcur->getRefTodPdGradField()[0],  ipvcur->getRefTodLocalVariableDExtraDofDiffusionField()(0,0),
                        ipvprev->getConstRefToField(0),ipvcur->getConstRefToField(0),
                        ipvprev->getConstRefToGradField()[0],ipvcur->getConstRefToGradField()[0],

                        ipvcur->getRefToFlux()[0],
                        ipvcur->getRefTodFluxdF()[0], ipvcur->getRefTodFluxdNonLocalVariable()[0][0],
                        ipvcur->getRefTodFluxdField()[0][0], ipvcur->getRefTodFluxdGradField()[0][0],

                        ipvcur->getRefToFieldSource()(0),
                        ipvcur->getRefTodFieldSourcedF()[0], ipvcur->getRefTodFieldSourcedNonLocalVariable()(0,0),
                        ipvcur->getRefTodFieldSourcedField()(0,0), ipvcur->getRefTodFieldSourcedGradField()[0][0],

                        ipvcur->getRefToMechanicalSource()(0),
                        ipvcur->getRefTodMechanicalSourcedF()[0],ipvcur->getRefTodMechanicalSourcedNonLocalVariable()(0,0),
                        ipvcur->getRefTodMechanicalSourcedField()(0,0), ipvcur->getRefTodMechanicalSourcedGradField()[0][0],

                        stiff,&elasticL,dTangent,ipvcur->getPtrTodCmdFm());

  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);

  ipvcur->setConstRefToLinearK(_nldNonLinearTVEVP->getConductivityTensor(),0,0);
  ipvcur->setConstRefToLinearSymmetrizationCoupling(_nldNonLinearTVEVP->getStiff_alphaDilatation(),0);
  Cp = _nldNonLinearTVEVP->getExtraDofStoredEnergyPerUnitField(ipvcur->getConstRefToField(0));
}

void NonLocalDamageNonLinearTVEVPDG3DMaterialLaw::initialIPVariable(IPVariable* ipv) const{
	ThermoMechanicsDG3DIPVariableBase* dgIPV = dynamic_cast<ThermoMechanicsDG3DIPVariableBase*>(ipv);
	if (dgIPV!=NULL){
		dgIPV->setRefToDGElasticTangentModuli(this->elasticStiffness);
	}
	else{
		Msg::Error("dG3DIPVariableBase must be used dG3DLinearElasticMaterialLaw::initialIPVariable");
	}
};


// ############################################################################## END CLASS 1 ########################################################################################


LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure(const int num, const double E, const double nu, const double rho,
                              const double Tinitial, const double Alpha, const double KThCon, const double Cp,
                              const J2IsotropicHardening &comp, const J2IsotropicHardening &trac, const DamageLaw &_damLaw1, const DamageLaw &_damLaw2,
                              const double tol, const bool matrixbyPerturbation, const double pert,
                              const double stressIteratorTol, const bool thermalEstimationPreviousConfig):
                                                              dG3DMaterialLaw(num,rho,true)
{
  _localNonLinearTVEVP= new mlawLocalDamageNonLinearTVEVPWithFailure(num,E,nu,rho,tol,Tinitial,Alpha,KThCon,Cp,matrixbyPerturbation,pert,stressIteratorTol,thermalEstimationPreviousConfig);
  _localNonLinearTVEVP->setCompressionHardening(comp);
  _localNonLinearTVEVP->setTractionHardening(trac);

  _localNonLinearTVEVP->clearAllDamageLaw();
  _localNonLinearTVEVP->setDamageLaw(_damLaw1);
  _localNonLinearTVEVP->setDamageLaw(_damLaw2);

  fillElasticStiffness(E, nu, elasticStiffness);
};

//
LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure(const int num, const double E, const double nu, const double rho,
                              const double Tinitial, const double Alpha, const double KThCon, const double Cp,
                              const J2IsotropicHardening &comp, const J2IsotropicHardening &trac, const kinematicHardening& kin, const DamageLaw &_damLaw1, const DamageLaw &_damLaw2,
                              const double tol, const bool matrixbyPerturbation, const double pert,
                              const double stressIteratorTol, const bool thermalEstimationPreviousConfig):
                                                              dG3DMaterialLaw(num,rho,true)
{
  _localNonLinearTVEVP= new mlawLocalDamageNonLinearTVEVPWithFailure(num,E,nu,rho,tol,Tinitial,Alpha,KThCon,Cp,matrixbyPerturbation,pert,stressIteratorTol,thermalEstimationPreviousConfig);
  _localNonLinearTVEVP->setCompressionHardening(comp);
  _localNonLinearTVEVP->setTractionHardening(trac);
  _localNonLinearTVEVP->setKinematicHardening(kin);

  _localNonLinearTVEVP->clearAllDamageLaw();
  _localNonLinearTVEVP->setDamageLaw(_damLaw1);
  _localNonLinearTVEVP->setDamageLaw(_damLaw2);

  fillElasticStiffness(E, nu, elasticStiffness);
};

void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setStrainOrder(const int order){
  _localNonLinearTVEVP->setStrainOrder(order);
};
void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setViscoelasticMethod(const int method){
  _localNonLinearTVEVP->setViscoelasticMethod(method);
};
void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setViscoElasticNumberOfElement(const int N){
  _localNonLinearTVEVP->setViscoElasticNumberOfElement(N);
};
void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setViscoElasticData(const int i, const double Ei, const double taui){
  _localNonLinearTVEVP->setViscoElasticData(i,Ei,taui);
};
void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setViscoElasticData_Bulk(const int i, const double Ki, const double ki){
  _localNonLinearTVEVP->setViscoElasticData_Bulk(i,Ki,ki);
};
void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setViscoElasticData_Shear(const int i, const double Gi, const double gi){
  _localNonLinearTVEVP->setViscoElasticData_Shear(i,Gi,gi);
};
void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setViscoElasticData(const std::string filename){
  _localNonLinearTVEVP->setViscoElasticData(filename);
};
void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setIsotropicHardeningCoefficients(const double HR1, const double HR2, const double HR3){
  _localNonLinearTVEVP->setIsotropicHardeningCoefficients(HR1,HR2,HR3);
};
void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setPolynomialOrderChabocheCoeffs(const int order){
  _localNonLinearTVEVP->setPolynomialOrderChabocheCoeffs(order);
};
void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setPolynomialCoeffsChabocheCoeffs(const int i, const double val){
  _localNonLinearTVEVP->setPolynomialCoeffsChabocheCoeffs(i,val);
};
void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setReferenceTemperature(const double Tref){
  _localNonLinearTVEVP->setReferenceTemperature(Tref);
};
void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setShiftFactorConstantsWLF(const double C1, const double C2){
  _localNonLinearTVEVP->setShiftFactorConstantsWLF(C1,C2);
};
void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setTemperatureFunction_BulkModulus(const scalarFunction& Tfunc){
  _localNonLinearTVEVP->setTemperatureFunction_BulkModulus(Tfunc);
};
void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setTemperatureFunction_ShearModulus(const scalarFunction& Tfunc){
  _localNonLinearTVEVP->setTemperatureFunction_ShearModulus(Tfunc);
};
void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setTemperatureFunction_ThermalDilationCoefficient(const scalarFunction& Tfunc){
  _localNonLinearTVEVP->setTemperatureFunction_ThermalDilationCoefficient(Tfunc);
};
void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setTemperatureFunction_ThermalConductivity(const scalarFunction& Tfunc){
  _localNonLinearTVEVP->setTemperatureFunction_ThermalConductivity(Tfunc);
};
void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setTemperatureFunction_SpecificHeat(const scalarFunction& Tfunc){
  _localNonLinearTVEVP->setTemperatureFunction_SpecificHeat(Tfunc);
};
void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setTemperatureFunction_ElasticCorrection_Bulk(const scalarFunction& Tfunc){
  _localNonLinearTVEVP->setTemperatureFunction_ElasticCorrection_Bulk(Tfunc);
};
void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setTemperatureFunction_ElasticCorrection_Shear(const scalarFunction& Tfunc){
  _localNonLinearTVEVP->setTemperatureFunction_ElasticCorrection_Shear(Tfunc);
};
void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setViscosityEffect(const viscosityLaw& v, const double p){
  _localNonLinearTVEVP->setViscosityEffect(v,p);
};
void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setMullinsEffect(const mullinsEffect& mullins){
  _localNonLinearTVEVP->setMullinsEffect(mullins);
};

void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setYieldPowerFactor(const double n){
  _localNonLinearTVEVP->setPowerFactor(n);
};
void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::nonAssociatedFlowRuleFactor(const double b){
  _localNonLinearTVEVP->nonAssociatedFlowRuleFactor(b);
};
void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setPlasticPoissonRatio(const double nup){
  _localNonLinearTVEVP->setPlasticPoissonRatio(nup);
};
void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setNonAssociatedFlow(const bool flag){
  _localNonLinearTVEVP->setNonAssociatedFlow(flag);
};

void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setTaylorQuineyFactor(const double f, const bool flag){
  _localNonLinearTVEVP->setTaylorQuineyFactor(f, flag);
};
void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setTemperatureFunction_InitialYieldStress(const scalarFunction& Tfunc){
  _localNonLinearTVEVP->setTemperatureFunction_InitialYieldStress(Tfunc);
};
void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setTemperatureFunction_Hardening(const scalarFunction& Tfunc){
  _localNonLinearTVEVP->setTemperatureFunction_Hardening(Tfunc);
};
void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setTemperatureFunction_KinematicHardening(const scalarFunction& Tfunc){
  _localNonLinearTVEVP->setTemperatureFunction_KinematicHardening(Tfunc);
};
void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setTemperatureFunction_Viscosity(const scalarFunction& Tfunc){
  _localNonLinearTVEVP->setTemperatureFunction_Viscosity(Tfunc);
};
void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::useExtraBranchBool(const bool useExtraBranch){
  _localNonLinearTVEVP->useExtraBranchBool(useExtraBranch);
};
void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::useExtraBranchBool_TVE(const bool useExtraBranch_TVE, const int ExtraBranch_TVE_option){
  _localNonLinearTVEVP->useExtraBranchBool_TVE(useExtraBranch_TVE, ExtraBranch_TVE_option);
};
void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::useRotationCorrectionBool(const bool useRotationCorrection,  const int rotationCorrectionScheme){
  _localNonLinearTVEVP->useRotationCorrectionBool(useRotationCorrection,rotationCorrectionScheme);
};
void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setVolumeCorrection(const double _vc, const double _xivc, const double _zetavc, const double _dc, const double _thetadc, const double _pidc){
  _localNonLinearTVEVP->setVolumeCorrection(_vc,_xivc,_zetavc,_dc,_thetadc,_pidc);
};
void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setAdditionalVolumeCorrections(const double V3, const double V4, const double V5, const double D3, const double D4, const double D5){
  _localNonLinearTVEVP->setAdditionalVolumeCorrections(V3,V4,V5,D3,D4,D5);
};
void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setExtraBranch_CompressionParameter(const double compCorrection, const double compCorrection_2, const double compCorrection_3){
  _localNonLinearTVEVP->setExtraBranch_CompressionParameter(compCorrection,compCorrection_2,compCorrection_3);
};
void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setExtraBranchNLType(const int method){
  _localNonLinearTVEVP->setExtraBranchNLType(method);
};
void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setTensionCompressionRegularisation(const double tensionCompressionRegularisation){
  _localNonLinearTVEVP->setTensionCompressionRegularisation(tensionCompressionRegularisation);
};
void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setCorrectionsAllBranchesTVE(const int i, const double V0,const double V1, const double V2, const double D0,const double D1, const double D2){
  _localNonLinearTVEVP->setCorrectionsAllBranchesTVE(i,V0,V1,V2,D0,D1,D2);
};
void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setAdditionalCorrectionsAllBranchesTVE(const int i, const double V3, const double V4, const double V5, const double D3, const double D4, const double D5){
  _localNonLinearTVEVP->setAdditionalCorrectionsAllBranchesTVE(i,V3,V4,V5,D3,D4,D5);
};
void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setCompressionCorrectionsAllBranchesTVE(const int i, const double Ci){
  _localNonLinearTVEVP->setCompressionCorrectionsAllBranchesTVE(i,Ci);
};

void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setFailureCriterion(const mlawHyperelasticFailureCriterionBase& fCr){
  _localNonLinearTVEVP->setFailureCriterion(fCr);
};

LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::~LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure(){
  if (_localNonLinearTVEVP) delete _localNonLinearTVEVP;
	_localNonLinearTVEVP = NULL;
};

LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure(const LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure &source):

    dG3DMaterialLaw(source)
{
	_localNonLinearTVEVP = NULL;
	if (source._localNonLinearTVEVP){
		_localNonLinearTVEVP = new mlawLocalDamageNonLinearTVEVPWithFailure(*(source._localNonLinearTVEVP));
	}
};

void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setTime(const double t,const double dtime){
  dG3DMaterialLaw::setTime(t,dtime);
  _localNonLinearTVEVP->setTime(t,dtime);
}

materialLaw::matname LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::getType() const {return _localNonLinearTVEVP->getType();}

void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);

  if(iele==NULL) inter=false;
  IPVariable* ipvi = new  localDamagenonLinearTVEVPDG3DIPVariableWithFailure(*_localNonLinearTVEVP,hasBodyForce,inter);
  IPVariable* ipv1 = new  localDamagenonLinearTVEVPDG3DIPVariableWithFailure(*_localNonLinearTVEVP,hasBodyForce,inter);
  IPVariable* ipv2 = new  localDamagenonLinearTVEVPDG3DIPVariableWithFailure(*_localNonLinearTVEVP,hasBodyForce,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  ipv = new  localDamagenonLinearTVEVPDG3DIPVariableWithFailure(*_localNonLinearTVEVP,hasBodyForce,inter);
}

double LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::soundSpeed() const{return _localNonLinearTVEVP->soundSpeed();} // or change value ??

void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
   /* get ipvariable */
  localDamagenonLinearTVEVPDG3DIPVariableWithFailure* ipvcur = NULL;
  const localDamagenonLinearTVEVPDG3DIPVariableWithFailure* ipvprev = NULL;

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<localDamagenonLinearTVEVPDG3DIPVariableWithFailure*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const localDamagenonLinearTVEVPDG3DIPVariableWithFailure*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<localDamagenonLinearTVEVPDG3DIPVariableWithFailure*>(ipv);
    ipvprev = static_cast<const localDamagenonLinearTVEVPDG3DIPVariableWithFailure*>(ipvp);
  }

  _localNonLinearTVEVP->checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());
};

void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  /* get ipvariable */
  localDamagenonLinearTVEVPDG3DIPVariableWithFailure* ipvcur = NULL;
  const localDamagenonLinearTVEVPDG3DIPVariableWithFailure* ipvprev = NULL;

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<localDamagenonLinearTVEVPDG3DIPVariableWithFailure*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const localDamagenonLinearTVEVPDG3DIPVariableWithFailure*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<localDamagenonLinearTVEVPDG3DIPVariableWithFailure*>(ipv);
    ipvprev = static_cast<const localDamagenonLinearTVEVPDG3DIPVariableWithFailure*>(ipvp);
  }

  /* strain xyz */
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

  /* data for J2 law */
  IPNonLinearTVEVPMultipleLocalDamage* q1 = ipvcur->getIPNonLinearTVEVPMultipleLocalDamage();
  const IPNonLinearTVEVPMultipleLocalDamage* q0 = ipvprev->getIPNonLinearTVEVPMultipleLocalDamage();

  double& Cp = ipvcur->getRefToExtraDofFieldCapacityPerUnitField()(0);
  STensor43& elasticL = ipvcur->getRefToElasticTangentModuli();

  /* compute stress */
  _localNonLinearTVEVP->constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),
                        ipvcur->getRefTodPdField()[0], ipvcur->getRefTodPdGradField()[0],

                        ipvprev->getConstRefToField(0),ipvcur->getConstRefToField(0),
                        ipvprev->getConstRefToGradField()[0],ipvcur->getConstRefToGradField()[0],

                        ipvcur->getRefToFlux()[0],
                        ipvcur->getRefTodFluxdF()[0],
                        ipvcur->getRefTodFluxdField()[0][0], ipvcur->getRefTodFluxdGradField()[0][0],

                        ipvcur->getRefToFieldSource()(0),
                        ipvcur->getRefTodFieldSourcedF()[0],
                        ipvcur->getRefTodFieldSourcedField()(0,0), ipvcur->getRefTodFieldSourcedGradField()[0][0],

                        ipvcur->getRefToMechanicalSource()(0),
                        ipvcur->getRefTodMechanicalSourcedF()[0],
                        ipvcur->getRefTodMechanicalSourcedField()(0,0), ipvcur->getRefTodMechanicalSourcedGradField()[0][0],

                        stiff,&elasticL,dTangent,ipvcur->getPtrTodCmdFm());

  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);

  ipvcur->setConstRefToLinearK(_localNonLinearTVEVP->getConductivityTensor(),0,0);
  ipvcur->setConstRefToLinearSymmetrizationCoupling(_localNonLinearTVEVP->getStiff_alphaDilatation(),0);
  Cp = _localNonLinearTVEVP->getExtraDofStoredEnergyPerUnitField(ipvcur->getConstRefToField(0));
}

void LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::initialIPVariable(IPVariable* ipv) const{
	ThermoMechanicsDG3DIPVariableBase* dgIPV = dynamic_cast<ThermoMechanicsDG3DIPVariableBase*>(ipv);
	if (dgIPV!=NULL){
		dgIPV->setRefToDGElasticTangentModuli(this->elasticStiffness);
	}
	else{
		Msg::Error("dG3DIPVariableBase must be used dG3DLinearElasticMaterialLaw::initialIPVariable");
	}
};

// ############################################################################## END CLASS 2 ########################################################################################

NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure(const int num, const double E, const double nu, const double rho,
                              const double Tinitial, const double Alpha, const double KThCon, const double Cp,
                              const J2IsotropicHardening &comp, const J2IsotropicHardening &trac,
                              const CLengthLaw &_cLLaw1, const CLengthLaw &_cLLaw2,
                              const DamageLaw &_damLaw1, const DamageLaw &_damLaw2,
                              const double tol, const bool matrixbyPerturbation, const double pert,
                              const double stressIteratorTol, const bool thermalEstimationPreviousConfig):
                                                              dG3DMaterialLaw(num,rho,true)
{
  _nldNonLinearTVEVP= new mlawNonLocalDamageNonLinearTVEVPWithFailure(num,E,nu,rho,tol,Tinitial,Alpha,KThCon,Cp,matrixbyPerturbation,pert,stressIteratorTol,thermalEstimationPreviousConfig);
  _nldNonLinearTVEVP->setCompressionHardening(comp);
  _nldNonLinearTVEVP->setTractionHardening(trac);

  _nldNonLinearTVEVP->setCLengthLaw(_cLLaw1);
  _nldNonLinearTVEVP->setCLengthLaw(_cLLaw2);

  _nldNonLinearTVEVP->setDamageLaw(_damLaw1);
  _nldNonLinearTVEVP->setDamageLaw(_damLaw2);

  fillElasticStiffness(E, nu, elasticStiffness);
};

//
NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure(const int num, const double E, const double nu, const double rho,
                              const double Tinitial, const double Alpha, const double KThCon, const double Cp,
                              const J2IsotropicHardening &comp, const J2IsotropicHardening &trac, const kinematicHardening& kin,
                              const CLengthLaw &_cLLaw1, const CLengthLaw &_cLLaw2,
                              const DamageLaw &_damLaw1, const DamageLaw &_damLaw2,
                              const double tol, const bool matrixbyPerturbation, const double pert,
                              const double stressIteratorTol, const bool thermalEstimationPreviousConfig):
                                                              dG3DMaterialLaw(num,rho,true)
{
  _nldNonLinearTVEVP= new mlawNonLocalDamageNonLinearTVEVPWithFailure(num,E,nu,rho,tol,Tinitial,Alpha,KThCon,Cp,matrixbyPerturbation,pert,stressIteratorTol,thermalEstimationPreviousConfig);
  _nldNonLinearTVEVP->setCompressionHardening(comp);
  _nldNonLinearTVEVP->setTractionHardening(trac);
  _nldNonLinearTVEVP->setKinematicHardening(kin);

  _nldNonLinearTVEVP->setCLengthLaw(_cLLaw1);
  _nldNonLinearTVEVP->setCLengthLaw(_cLLaw2);

  _nldNonLinearTVEVP->setDamageLaw(_damLaw1);
  _nldNonLinearTVEVP->setDamageLaw(_damLaw2);


  fillElasticStiffness(E, nu, elasticStiffness);
};

void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setStrainOrder(const int order){
  _nldNonLinearTVEVP->setStrainOrder(order);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setViscoelasticMethod(const int method){
  _nldNonLinearTVEVP->setViscoelasticMethod(method);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setViscoElasticNumberOfElement(const int N){
  _nldNonLinearTVEVP->setViscoElasticNumberOfElement(N);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setViscoElasticData(const int i, const double Ei, const double taui){
  _nldNonLinearTVEVP->setViscoElasticData(i,Ei,taui);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setViscoElasticData_Bulk(const int i, const double Ki, const double ki){
  _nldNonLinearTVEVP->setViscoElasticData_Bulk(i,Ki,ki);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setViscoElasticData_Shear(const int i, const double Gi, const double gi){
  _nldNonLinearTVEVP->setViscoElasticData_Shear(i,Gi,gi);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setViscoElasticData(const std::string filename){
  _nldNonLinearTVEVP->setViscoElasticData(filename);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setIsotropicHardeningCoefficients(const double HR1, const double HR2, const double HR3){
  _nldNonLinearTVEVP->setIsotropicHardeningCoefficients(HR1,HR2,HR3);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setPolynomialOrderChabocheCoeffs(const int order){
  _nldNonLinearTVEVP->setPolynomialOrderChabocheCoeffs(order);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setPolynomialCoeffsChabocheCoeffs(const int i, const double val){
  _nldNonLinearTVEVP->setPolynomialCoeffsChabocheCoeffs(i,val);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setReferenceTemperature(const double Tref){
  _nldNonLinearTVEVP->setReferenceTemperature(Tref);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setShiftFactorConstantsWLF(const double C1, const double C2){
  _nldNonLinearTVEVP->setShiftFactorConstantsWLF(C1,C2);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setTemperatureFunction_BulkModulus(const scalarFunction& Tfunc){
  _nldNonLinearTVEVP->setTemperatureFunction_BulkModulus(Tfunc);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setTemperatureFunction_ShearModulus(const scalarFunction& Tfunc){
  _nldNonLinearTVEVP->setTemperatureFunction_ShearModulus(Tfunc);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setTemperatureFunction_ThermalDilationCoefficient(const scalarFunction& Tfunc){
  _nldNonLinearTVEVP->setTemperatureFunction_ThermalDilationCoefficient(Tfunc);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setTemperatureFunction_ThermalConductivity(const scalarFunction& Tfunc){
  _nldNonLinearTVEVP->setTemperatureFunction_ThermalConductivity(Tfunc);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setTemperatureFunction_SpecificHeat(const scalarFunction& Tfunc){
  _nldNonLinearTVEVP->setTemperatureFunction_SpecificHeat(Tfunc);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setTemperatureFunction_ElasticCorrection_Bulk(const scalarFunction& Tfunc){
  _nldNonLinearTVEVP->setTemperatureFunction_ElasticCorrection_Bulk(Tfunc);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setTemperatureFunction_ElasticCorrection_Shear(const scalarFunction& Tfunc){
  _nldNonLinearTVEVP->setTemperatureFunction_ElasticCorrection_Shear(Tfunc);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setViscosityEffect(const viscosityLaw& v, const double p){
  _nldNonLinearTVEVP->setViscosityEffect(v,p);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setMullinsEffect(const mullinsEffect& mullins){
  _nldNonLinearTVEVP->setMullinsEffect(mullins);
};

void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setYieldPowerFactor(const double n){
  _nldNonLinearTVEVP->setPowerFactor(n);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::nonAssociatedFlowRuleFactor(const double b){
  _nldNonLinearTVEVP->nonAssociatedFlowRuleFactor(b);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setPlasticPoissonRatio(const double nup){
  _nldNonLinearTVEVP->setPlasticPoissonRatio(nup);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setNonAssociatedFlow(const bool flag){
  _nldNonLinearTVEVP->setNonAssociatedFlow(flag);
};

void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setTaylorQuineyFactor(const double f, const bool flag){
  _nldNonLinearTVEVP->setTaylorQuineyFactor(f, flag);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setTemperatureFunction_InitialYieldStress(const scalarFunction& Tfunc){
  _nldNonLinearTVEVP->setTemperatureFunction_InitialYieldStress(Tfunc);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setTemperatureFunction_Hardening(const scalarFunction& Tfunc){
  _nldNonLinearTVEVP->setTemperatureFunction_Hardening(Tfunc);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setTemperatureFunction_KinematicHardening(const scalarFunction& Tfunc){
  _nldNonLinearTVEVP->setTemperatureFunction_KinematicHardening(Tfunc);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setTemperatureFunction_Viscosity(const scalarFunction& Tfunc){
  _nldNonLinearTVEVP->setTemperatureFunction_Viscosity(Tfunc);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::useExtraBranchBool(const bool useExtraBranch){
  _nldNonLinearTVEVP->useExtraBranchBool(useExtraBranch);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::useExtraBranchBool_TVE(const bool useExtraBranch_TVE, const int ExtraBranch_TVE_option){
  _nldNonLinearTVEVP->useExtraBranchBool_TVE(useExtraBranch_TVE, ExtraBranch_TVE_option);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::useRotationCorrectionBool(const bool useRotationCorrection,  const int rotationCorrectionScheme){
  _nldNonLinearTVEVP->useRotationCorrectionBool(useRotationCorrection,rotationCorrectionScheme);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setVolumeCorrection(const double _vc, const double _xivc, const double _zetavc, const double _dc, const double _thetadc, const double _pidc){
  _nldNonLinearTVEVP->setVolumeCorrection(_vc,_xivc,_zetavc,_dc,_thetadc,_pidc);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setAdditionalVolumeCorrections(const double V3, const double V4, const double V5, const double D3, const double D4, const double D5){
  _nldNonLinearTVEVP->setAdditionalVolumeCorrections(V3,V4,V5,D3,D4,D5);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setExtraBranch_CompressionParameter(const double compCorrection, const double compCorrection_2, const double compCorrection_3){
  _nldNonLinearTVEVP->setExtraBranch_CompressionParameter(compCorrection,compCorrection_2,compCorrection_3);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setExtraBranchNLType(const int method){
  _nldNonLinearTVEVP->setExtraBranchNLType(method);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setTensionCompressionRegularisation(const double tensionCompressionRegularisation){
  _nldNonLinearTVEVP->setTensionCompressionRegularisation(tensionCompressionRegularisation);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setCorrectionsAllBranchesTVE(const int i, const double V0,const double V1, const double V2, const double D0,const double D1, const double D2){
  _nldNonLinearTVEVP->setCorrectionsAllBranchesTVE(i,V0,V1,V2,D0,D1,D2);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setAdditionalCorrectionsAllBranchesTVE(const int i, const double V3, const double V4, const double V5, const double D3, const double D4, const double D5){
  _nldNonLinearTVEVP->setAdditionalCorrectionsAllBranchesTVE(i,V3,V4,V5,D3,D4,D5);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setCompressionCorrectionsAllBranchesTVE(const int i, const double Ci){
  _nldNonLinearTVEVP->setCompressionCorrectionsAllBranchesTVE(i,Ci);
};

void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setFailureCriterion(const mlawHyperelasticFailureCriterionBase& fCr){
  _nldNonLinearTVEVP->setFailureCriterion(fCr);
};
void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setCritialDamage(const double D){
  _nldNonLinearTVEVP->setCritialDamage(D);
};

NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::~NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure(){
  if (_nldNonLinearTVEVP) delete _nldNonLinearTVEVP;
	_nldNonLinearTVEVP = NULL;
};

NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure(const NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure &source):

    dG3DMaterialLaw(source)
{
	_nldNonLinearTVEVP = NULL;
	if (source._nldNonLinearTVEVP){
		_nldNonLinearTVEVP = new mlawNonLocalDamageNonLinearTVEVPWithFailure(*(source._nldNonLinearTVEVP));
	}
};

void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::setTime(const double t,const double dtime){
  dG3DMaterialLaw::setTime(t,dtime);
  _nldNonLinearTVEVP->setTime(t,dtime);
}

materialLaw::matname NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::getType() const {return _nldNonLinearTVEVP->getType();}

void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);

  if(iele==NULL) inter=false;
  IPVariable* ipvi = new  nonLocalDamageNonLinearTVEVPDG3DIPVariableWithFailure(*_nldNonLinearTVEVP,hasBodyForce,inter);
  IPVariable* ipv1 = new  nonLocalDamageNonLinearTVEVPDG3DIPVariableWithFailure(*_nldNonLinearTVEVP,hasBodyForce,inter);
  IPVariable* ipv2 = new  nonLocalDamageNonLinearTVEVPDG3DIPVariableWithFailure(*_nldNonLinearTVEVP,hasBodyForce,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
  _nldNonLinearTVEVP->createIPState((dynamic_cast <nonLocalDamageNonLinearTVEVPDG3DIPVariableWithFailure*> (ipvi))->getIPNonLinearTVEVPMultipleNonLocalDamage(),
                         (dynamic_cast <nonLocalDamageNonLinearTVEVPDG3DIPVariableWithFailure*> (ipv1))->getIPNonLinearTVEVPMultipleNonLocalDamage(),
             			 (dynamic_cast <nonLocalDamageNonLinearTVEVPDG3DIPVariableWithFailure*> (ipv2))->getIPNonLinearTVEVPMultipleNonLocalDamage());

}

void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }

  ipv = new  nonLocalDamageNonLinearTVEVPDG3DIPVariableWithFailure(*_nldNonLinearTVEVP,hasBodyForce,inter);
  IPNonLinearTVEVPMultipleNonLocalDamage * ipvnl = static_cast <nonLocalDamageNonLinearTVEVPDG3DIPVariableWithFailure*>(ipv)->getIPNonLinearTVEVPMultipleNonLocalDamage();
  _nldNonLinearTVEVP->createIPVariable(ipvnl, hasBodyForce, ele,nbFF_,GP,gpt);
}

double NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::soundSpeed() const{return _nldNonLinearTVEVP->soundSpeed();} // or change value ??

void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  /* get ipvariable */
  nonLocalDamageNonLinearTVEVPDG3DIPVariableWithFailure* ipvcur = NULL;
  const nonLocalDamageNonLinearTVEVPDG3DIPVariableWithFailure* ipvprev = NULL;

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<nonLocalDamageNonLinearTVEVPDG3DIPVariableWithFailure*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const nonLocalDamageNonLinearTVEVPDG3DIPVariableWithFailure*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<nonLocalDamageNonLinearTVEVPDG3DIPVariableWithFailure*>(ipv);
    ipvprev = static_cast<const nonLocalDamageNonLinearTVEVPDG3DIPVariableWithFailure*>(ipvp);
  }

  _nldNonLinearTVEVP->checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());
};

void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  /* get ipvariable */
  nonLocalDamageNonLinearTVEVPDG3DIPVariableWithFailure* ipvcur = NULL;
  const nonLocalDamageNonLinearTVEVPDG3DIPVariableWithFailure* ipvprev = NULL;

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<nonLocalDamageNonLinearTVEVPDG3DIPVariableWithFailure*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const nonLocalDamageNonLinearTVEVPDG3DIPVariableWithFailure*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<nonLocalDamageNonLinearTVEVPDG3DIPVariableWithFailure*>(ipv);
    ipvprev = static_cast<const nonLocalDamageNonLinearTVEVPDG3DIPVariableWithFailure*>(ipvp);
  }

  /* strain xyz */
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

  /* data for J2 law */
  IPNonLinearTVEVPMultipleNonLocalDamage* q1 = ipvcur->getIPNonLinearTVEVPMultipleNonLocalDamage();
  const IPNonLinearTVEVPMultipleNonLocalDamage* q0 = ipvprev->getIPNonLinearTVEVPMultipleNonLocalDamage();

  double& Cp = ipvcur->getRefToExtraDofFieldCapacityPerUnitField()(0);
  STensor43& elasticL = ipvcur->getRefToElasticTangentModuli();

  /* compute stress */
  _nldNonLinearTVEVP->constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),

                        ipvcur->getRefToDLocalVariableDStrain(),ipvcur->getRefToDStressDNonLocalVariable(),
                        ipvcur->getRefToDLocalVariableDNonLocalVariable(),

                        ipvcur->getRefTodPdField()[0], ipvcur->getRefTodPdGradField()[0],  ipvcur->getRefTodLocalVariableDExtraDofDiffusionField(),

                        ipvprev->getConstRefToField(0),ipvcur->getConstRefToField(0),
                        ipvprev->getConstRefToGradField()[0],ipvcur->getConstRefToGradField()[0],

                        ipvcur->getRefToFlux()[0],
                        ipvcur->getRefTodFluxdF()[0], ipvcur->getRefTodFluxdNonLocalVariable()[0],
                        ipvcur->getRefTodFluxdField()[0][0], ipvcur->getRefTodFluxdGradField()[0][0],

                        ipvcur->getRefToFieldSource()(0),
                        ipvcur->getRefTodFieldSourcedF()[0], ipvcur->getRefTodFieldSourcedNonLocalVariable(),
                        ipvcur->getRefTodFieldSourcedField()(0,0), ipvcur->getRefTodFieldSourcedGradField()[0][0],

                        ipvcur->getRefToMechanicalSource()(0),
                        ipvcur->getRefTodMechanicalSourcedF()[0],ipvcur->getRefTodMechanicalSourcedNonLocalVariable(),
                        ipvcur->getRefTodMechanicalSourcedField()(0,0), ipvcur->getRefTodMechanicalSourcedGradField()[0][0],

                        stiff,&elasticL,dTangent,ipvcur->getPtrTodCmdFm());

  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);

  ipvcur->setConstRefToLinearK(_nldNonLinearTVEVP->getConductivityTensor(),0,0);
  ipvcur->setConstRefToLinearSymmetrizationCoupling(_nldNonLinearTVEVP->getStiff_alphaDilatation(),0);
  Cp = _nldNonLinearTVEVP->getExtraDofStoredEnergyPerUnitField(ipvcur->getConstRefToField(0));
}

void NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure::initialIPVariable(IPVariable* ipv) const{
	ThermoMechanicsDG3DIPVariableBase* dgIPV = dynamic_cast<ThermoMechanicsDG3DIPVariableBase*>(ipv);
	if (dgIPV!=NULL){
		dgIPV->setRefToDGElasticTangentModuli(this->elasticStiffness);
	}
	else{
		Msg::Error("dG3DIPVariableBase must be used dG3DLinearElasticMaterialLaw::initialIPVariable");
	}
};

// ############################################################################## END CLASS 3 ########################################################################################
