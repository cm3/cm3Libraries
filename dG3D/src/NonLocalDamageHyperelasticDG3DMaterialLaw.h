//
// C++ Interface: NonLocalDamagePowerYieldHyperDG3DMaterialLaw
//
// Description:
//
//
// Author: V.D. NGUYEN 2014
//
// Copyright: See COPYING file that comes with this distribution
//
#ifndef NONLOCALDAMAGEHYPERELASTICDG3DMATERIALLAW_H_
#define NONLOCALDAMAGEHYPERELASTICDG3DMATERIALLAW_H_
#include "dG3DMaterialLaw.h"
#include "mlawNonLocalDamageHyperelastic.h"
#include "mlawNonLinearTVEVPwithFailure.h"

class NonLocalDamagePowerYieldHyperDG3DMaterialLaw : public dG3DMaterialLaw
{

 protected:
  mlawNonLocalDamagePowerYieldHyper *_nldPowerYieldHyperlaw; // pointer to allow to choose between LLN style or Gmsh Style. The choice is performed by the constructor (2 constructors with != arguments)

 public:
  NonLocalDamagePowerYieldHyperDG3DMaterialLaw(const int num,const double rho,
                   double E,const double nu,const J2IsotropicHardening &comp,
                   const J2IsotropicHardening &trac,
                   const CLengthLaw &_cLLaw,const DamageLaw &_damLaw,const double tol=1.e-6,
                   const bool matrixbyPerturbation = false, const double pert = 1e-8);
  NonLocalDamagePowerYieldHyperDG3DMaterialLaw(const int num,const double rho,
                   double E,const double nu,const J2IsotropicHardening &comp,
                   const J2IsotropicHardening &trac,
                   const kinematicHardening& kin,
                   const CLengthLaw &_cLLaw,const DamageLaw &_damLaw,const double tol=1.e-6,
                   const bool matrixbyPerturbation = false, const double pert = 1e-8);

  void setYieldPowerFactor(const double n);
  void nonAssociatedFlowRuleFactor(const double b);
  void setPlasticPoissonRatio(const double nup);
  void setNonAssociatedFlow(const bool flag);
  void setStrainOrder(const int o);
  void setViscosityEffect(const viscosityLaw& v, const double p);
  void setViscoelasticMethod(const int method);
  void setViscoElasticNumberOfElement(const int N);
  void setViscoElasticData(const int i, const double Ei, const double taui);
  void setViscoElasticData_Bulk(const int i, const double Ki, const double ki);
  void setViscoElasticData_Shear(const int i, const double Gi, const double gi);
  void setViscoElasticData(const std::string filename);
  void setVolumeCorrection(double _vc, double _xivc, double _zetavc, double _dc, double _thetadc, double _pidc);

#ifndef SWIG
  virtual ~NonLocalDamagePowerYieldHyperDG3DMaterialLaw();
  NonLocalDamagePowerYieldHyperDG3DMaterialLaw(const NonLocalDamagePowerYieldHyperDG3DMaterialLaw &source);
	virtual materialLaw* clone() const {return new NonLocalDamagePowerYieldHyperDG3DMaterialLaw(*this);}
   virtual bool withEnergyDissipation() const {return _nldPowerYieldHyperlaw->withEnergyDissipation();};
  // set the time of _nldlaw
  virtual void setTime(const double t,const double dtime);
  virtual materialLaw::matname getType() const;
  virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
  virtual double soundSpeed() const;
  virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const;
  virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent =false);
	virtual void initialIPVariable(IPVariable* ipv) const;

  virtual void setMacroSolver(const nonLinearMechSolver* sv){
		dG3DMaterialLaw::setMacroSolver(sv);
		_nldPowerYieldHyperlaw->setMacroSolver(sv);
	};

  virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
  {
    return _nldPowerYieldHyperlaw;
  }
  virtual materialLaw *getNonLinearSolverMaterialLaw()
  {
    return _nldPowerYieldHyperlaw;
  }

#endif
};

class LocalDamagePowerYieldHyperDG3DMaterialLawWithFailure: public dG3DMaterialLaw
{

 protected:
  mlawLocalDamagePowerYieldHyperWithFailure *_localPowerYieldHyperlaw; // pointer to allow to choose between LLN style or Gmsh Style. The choice is performed by the constructor (2 constructors with != arguments)

 public:
  LocalDamagePowerYieldHyperDG3DMaterialLawWithFailure(const int num,const double rho,
                   double E,const double nu,const J2IsotropicHardening &comp,
                   const J2IsotropicHardening &trac,
                   const DamageLaw &_damLaw1, const DamageLaw &_damLaw2,const double tol=1.e-6,
                   const bool matrixbyPerturbation = false, const double pert = 1e-8);
  LocalDamagePowerYieldHyperDG3DMaterialLawWithFailure(const int num,const double rho,
                   double E,const double nu,const J2IsotropicHardening &comp,
                   const J2IsotropicHardening &trac,
                   const kinematicHardening& kin,
                   const DamageLaw &_damLaw1, const DamageLaw &_damLaw2, const double tol=1.e-6,
                   const bool matrixbyPerturbation = false, const double pert = 1e-8);

  void setYieldPowerFactor(const double n);
  void nonAssociatedFlowRuleFactor(const double b);
  void setPlasticPoissonRatio(const double nup);
  void setNonAssociatedFlow(const bool flag);
  void setStrainOrder(const int o);
  void setViscosityEffect(const viscosityLaw& v, const double p);
  void setViscoelasticMethod(const int method);
  void setViscoElasticNumberOfElement(const int N);
  void setViscoElasticData(const int i, const double Ei, const double taui);
  void setViscoElasticData_Bulk(const int i, const double Ki, const double ki);
  void setViscoElasticData_Shear(const int i, const double Gi, const double gi);
  void setViscoElasticData(const std::string filename);
  void setVolumeCorrection(double _vc, double _xivc, double _zetavc, double _dc, double _thetadc, double _pidc);

  void setFailureCriterion(const mlawHyperelasticFailureCriterionBase& fCr);

#ifndef SWIG
  virtual ~LocalDamagePowerYieldHyperDG3DMaterialLawWithFailure();
  LocalDamagePowerYieldHyperDG3DMaterialLawWithFailure(const LocalDamagePowerYieldHyperDG3DMaterialLawWithFailure &source);
	virtual materialLaw* clone() const {return new LocalDamagePowerYieldHyperDG3DMaterialLawWithFailure(*this);}
  virtual bool withEnergyDissipation() const {return _localPowerYieldHyperlaw->withEnergyDissipation();};
  // set the time of _nldlaw
  virtual void setTime(const double t,const double dtime);
  virtual materialLaw::matname getType() const;
  virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
  virtual double soundSpeed() const;
  virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const;
  virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent =false);
	virtual void initialIPVariable(IPVariable* ipv) const;
  virtual void setMacroSolver(const nonLinearMechSolver* sv){
		dG3DMaterialLaw::setMacroSolver(sv);
		_localPowerYieldHyperlaw->setMacroSolver(sv);
	};
  virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
  {
    return _localPowerYieldHyperlaw;
  }
  virtual materialLaw *getNonLinearSolverMaterialLaw()
  {
    return _localPowerYieldHyperlaw;
  }
#endif
};


class NonLocalDamagePowerYieldHyperDG3DMaterialLawWithFailure: public dG3DMaterialLaw
{

 protected:
  mlawNonLocalDamagePowerYieldHyperWithFailure *_nldPowerYieldHyperlaw; // pointer to allow to choose between LLN style or Gmsh Style. The choice is performed by the constructor (2 constructors with != arguments)

 public:
  NonLocalDamagePowerYieldHyperDG3DMaterialLawWithFailure(const int num,const double rho,
                   double E,const double nu,const J2IsotropicHardening &comp,
                   const J2IsotropicHardening &trac,
                   const CLengthLaw &_cLLaw1, const CLengthLaw &_cLLaw2,
                   const DamageLaw &_damLaw1, const DamageLaw &_damLaw2,const double tol=1.e-6,
                   const bool matrixbyPerturbation = false, const double pert = 1e-8);
  NonLocalDamagePowerYieldHyperDG3DMaterialLawWithFailure(const int num,const double rho,
                   double E,const double nu,const J2IsotropicHardening &comp,
                   const J2IsotropicHardening &trac,
                   const kinematicHardening& kin,
                   const CLengthLaw &_cLLaw1, const CLengthLaw &_cLLaw2,
                   const DamageLaw &_damLaw1, const DamageLaw &_damLaw2, const double tol=1.e-6,
                   const bool matrixbyPerturbation = false, const double pert = 1e-8);

  void setYieldPowerFactor(const double n);
  void nonAssociatedFlowRuleFactor(const double b);
  void setPlasticPoissonRatio(const double nup);
  void setNonAssociatedFlow(const bool flag);
  void setStrainOrder(const int o);
  void setViscosityEffect(const viscosityLaw& v, const double p);
  void setViscoelasticMethod(const int method);
  void setViscoElasticNumberOfElement(const int N);
  void setViscoElasticData(const int i, const double Ei, const double taui);
  void setViscoElasticData_Bulk(const int i, const double Ki, const double ki);
  void setViscoElasticData_Shear(const int i, const double Gi, const double gi);
  void setViscoElasticData(const std::string filename);
  void setVolumeCorrection(double _vc, double _xivc, double _zetavc, double _dc, double _thetadc, double _pidc);

  void setFailureCriterion(const mlawHyperelasticFailureCriterionBase& fCr);
  void setCritialDamage(const double D);

#ifndef SWIG
  virtual ~NonLocalDamagePowerYieldHyperDG3DMaterialLawWithFailure();
  NonLocalDamagePowerYieldHyperDG3DMaterialLawWithFailure(const NonLocalDamagePowerYieldHyperDG3DMaterialLawWithFailure &source);
	virtual materialLaw* clone() const {return new NonLocalDamagePowerYieldHyperDG3DMaterialLawWithFailure(*this);}
  virtual bool withEnergyDissipation() const {return _nldPowerYieldHyperlaw->withEnergyDissipation();};
  // set the time of _nldlaw
  virtual void setTime(const double t,const double dtime);
  virtual materialLaw::matname getType() const;
  virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
  virtual double soundSpeed() const;
  virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const;
  virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent =false);
	virtual void initialIPVariable(IPVariable* ipv) const;
  virtual void setMacroSolver(const nonLinearMechSolver* sv){
		dG3DMaterialLaw::setMacroSolver(sv);
		_nldPowerYieldHyperlaw->setMacroSolver(sv);
	};
  virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
  {
    return _nldPowerYieldHyperlaw;
  }
  virtual materialLaw *getNonLinearSolverMaterialLaw()
  {
    return _nldPowerYieldHyperlaw;
  }
#endif
};

class NonLocalDamageNonLinearTVEVPDG3DMaterialLaw : public dG3DMaterialLaw
{

 protected:
  mlawNonLocalDamagaNonLinearTVEVP *_nldNonLinearTVEVP; // pointer to allow to choose between LLN style or Gmsh Style. The choice is performed by the constructor (2 constructors with != arguments)

 public:
  NonLocalDamageNonLinearTVEVPDG3DMaterialLaw(const int num, const double E, const double nu, const double rho,
                                const double Tinitial, const double Alpha, const double KThCon, const double Cp,
                                const J2IsotropicHardening &comp, const J2IsotropicHardening &trac, const CLengthLaw &_cLLaw, const DamageLaw &_damLaw,
                                const double tol = 1.e-6, const bool matrixbyPerturbation = false, const double pert = 1e-8,
                                const double stressIteratorTol = 1.e-9, const bool thermalEstimationPreviousConfig = true);
  NonLocalDamageNonLinearTVEVPDG3DMaterialLaw(const int num, const double E, const double nu, const double rho,
                                const double Tinitial, const double Alpha, const double KThCon, const double Cp,
                                const J2IsotropicHardening &comp, const J2IsotropicHardening &trac, const kinematicHardening& kin, const CLengthLaw &_cLLaw, const DamageLaw &_damLaw,
                                const double tol = 1.e-6, const bool matrixbyPerturbation = false, const double pert = 1e-8,
                                const double stressIteratorTol = 1.e-9, const bool thermalEstimationPreviousConfig = true);

  void setStrainOrder(const int order);
  void setViscoelasticMethod(const int method);
  void setViscoElasticNumberOfElement(const int N);
  void setViscoElasticData(const int i, const double Ei, const double taui);
  void setViscoElasticData_Bulk(const int i, const double Ki, const double ki);
  void setViscoElasticData_Shear(const int i, const double Gi, const double gi);
  void setViscoElasticData(const std::string filename);
  void setIsotropicHardeningCoefficients(const double HR1, const double HR2, const double HR3);
  void setPolynomialOrderChabocheCoeffs(const int order);
  void setPolynomialCoeffsChabocheCoeffs(const int i, const double val);

  void setReferenceTemperature(const double Tref);
  void setShiftFactorConstantsWLF(const double C1, const double C2);
  void setTemperatureFunction_BulkModulus(const scalarFunction& Tfunc);
  void setTemperatureFunction_ShearModulus(const scalarFunction& Tfunc);
  void setTemperatureFunction_ThermalDilationCoefficient(const scalarFunction& Tfunc);
  void setTemperatureFunction_ThermalConductivity(const scalarFunction& Tfunc);
  void setTemperatureFunction_SpecificHeat(const scalarFunction& Tfunc);
  void setTemperatureFunction_ElasticCorrection_Bulk(const scalarFunction& Tfunc);
  void setTemperatureFunction_ElasticCorrection_Shear(const scalarFunction& Tfunc);
  void useExtraBranchBool(const bool useExtraBranch);
  void useExtraBranchBool_TVE(const bool useExtraBranch_TVE, const int ExtraBranch_TVE_option);
  void useRotationCorrectionBool(const bool useRotationCorrection, const int rotationCorrectionScheme);
  void setVolumeCorrection(const double _vc, const double _xivc, const double _zetavc, const double _dc, const double _thetadc, const double _pidc);
  void setAdditionalVolumeCorrections(const double V3, const double V4, const double V5, const double D3, const double D4, const double D5);
  void setExtraBranch_CompressionParameter(const double compCorrection, const double compCorrection_2, const double compCorrection_3);
  void setExtraBranchNLType(const int type);
  void setTensionCompressionRegularisation(const double tensionCompressionRegularisation);
  void setCorrectionsAllBranchesTVE(const int i, const double V0,const double V1, const double V2, const double D0, const double D1, const double D2);
  void setAdditionalCorrectionsAllBranchesTVE(const int i, const double V3, const double V4, const double V5, const double D3, const double D4, const double D5);
  void setCompressionCorrectionsAllBranchesTVE(const int i, const double Ci);

  void setViscosityEffect(const viscosityLaw& v, const double p);
  void setMullinsEffect(const mullinsEffect& mullins);
  void setYieldPowerFactor(const double n);
  void nonAssociatedFlowRuleFactor(const double b);
  void setPlasticPoissonRatio(const double nup);
  void setNonAssociatedFlow(const bool flag);

  void setTaylorQuineyFactor(const double f, const bool flag = false);
  void setTemperatureFunction_InitialYieldStress(const scalarFunction& Tfunc);
  void setTemperatureFunction_Hardening(const scalarFunction& Tfunc);
  void setTemperatureFunction_KinematicHardening(const scalarFunction& Tfunc);
  void setTemperatureFunction_Viscosity(const scalarFunction& Tfunc);

#ifndef SWIG
  virtual ~NonLocalDamageNonLinearTVEVPDG3DMaterialLaw();
  NonLocalDamageNonLinearTVEVPDG3DMaterialLaw(const NonLocalDamageNonLinearTVEVPDG3DMaterialLaw &source);
	virtual materialLaw* clone() const {return new NonLocalDamageNonLinearTVEVPDG3DMaterialLaw(*this);}
  virtual bool withEnergyDissipation() const {return _nldNonLinearTVEVP->withEnergyDissipation();};
  // set the time of _nldlaw
  virtual void setTime(const double t,const double dtime);
  virtual materialLaw::matname getType() const;
  virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
  virtual double soundSpeed() const;
  virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const;
  virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent =false);
	virtual void initialIPVariable(IPVariable* ipv) const;

  virtual void setMacroSolver(const nonLinearMechSolver* sv){
		dG3DMaterialLaw::setMacroSolver(sv);
		_nldNonLinearTVEVP->setMacroSolver(sv);
	};

  virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
  {
    return _nldNonLinearTVEVP;
  }
  virtual materialLaw *getNonLinearSolverMaterialLaw()
  {
    return _nldNonLinearTVEVP;
  }

  virtual const double bulkModulus() const {return _nldNonLinearTVEVP->bulkModulus();}
  virtual double scaleFactor() const{return _nldNonLinearTVEVP->scaleFactor();}
  virtual int getNumberOfExtraDofsDiffusion() const {return _nldNonLinearTVEVP->getNumberOfExtraDofsDiffusion();}
  virtual double getExtraDofStoredEnergyPerUnitField(double T) const {return _nldNonLinearTVEVP->getExtraDofStoredEnergyPerUnitField(T);};
  virtual double getInitialExtraDofStoredEnergyPerUnitField() const {return _nldNonLinearTVEVP->getInitialExtraDofStoredEnergyPerUnitField();};

#endif
};

class LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure: public dG3DMaterialLaw
{

 protected:
  mlawLocalDamageNonLinearTVEVPWithFailure *_localNonLinearTVEVP; // pointer to allow to choose between LLN style or Gmsh Style. The choice is performed by the constructor (2 constructors with != arguments)

 public:
  LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure(const int num, const double E, const double nu, const double rho,
                                const double Tinitial, const double Alpha, const double KThCon, const double Cp,
                                const J2IsotropicHardening &comp, const J2IsotropicHardening &trac, const DamageLaw &_damLaw1, const DamageLaw &_damLaw2,
                                const double tol = 1.e-6, const bool matrixbyPerturbation = false, const double pert = 1e-8,
                                const double stressIteratorTol = 1.e-9, const bool thermalEstimationPreviousConfig = true);
  LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure(const int num, const double E, const double nu, const double rho,
                                const double Tinitial, const double Alpha, const double KThCon, const double Cp,
                                const J2IsotropicHardening &comp, const J2IsotropicHardening &trac, const kinematicHardening& kin, const DamageLaw &_damLaw1, const DamageLaw &_damLaw2,
                                const double tol = 1.e-6, const bool matrixbyPerturbation = false, const double pert = 1e-8,
                                const double stressIteratorTol = 1.e-9, const bool thermalEstimationPreviousConfig = true);

  void setStrainOrder(const int order);
  void setViscoelasticMethod(const int method);
  void setViscoElasticNumberOfElement(const int N);
  void setViscoElasticData(const int i, const double Ei, const double taui);
  void setViscoElasticData_Bulk(const int i, const double Ki, const double ki);
  void setViscoElasticData_Shear(const int i, const double Gi, const double gi);
  void setViscoElasticData(const std::string filename);
  void setIsotropicHardeningCoefficients(const double HR1, const double HR2, const double HR3);
  void setPolynomialOrderChabocheCoeffs(const int order);
  void setPolynomialCoeffsChabocheCoeffs(const int i, const double val);

  void setReferenceTemperature(const double Tref);
  void setShiftFactorConstantsWLF(const double C1, const double C2);
  void setTemperatureFunction_BulkModulus(const scalarFunction& Tfunc);
  void setTemperatureFunction_ShearModulus(const scalarFunction& Tfunc);
  void setTemperatureFunction_ThermalDilationCoefficient(const scalarFunction& Tfunc);
  void setTemperatureFunction_ThermalConductivity(const scalarFunction& Tfunc);
  void setTemperatureFunction_SpecificHeat(const scalarFunction& Tfunc);
  void setTemperatureFunction_ElasticCorrection_Bulk(const scalarFunction& Tfunc);
  void setTemperatureFunction_ElasticCorrection_Shear(const scalarFunction& Tfunc);
  void useExtraBranchBool(const bool useExtraBranch);
  void useExtraBranchBool_TVE(const bool useExtraBranch_TVE, const int ExtraBranch_TVE_option);
  void useRotationCorrectionBool(const bool useRotationCorrection, const int rotationCorrectionScheme);
  void setVolumeCorrection(const double _vc, const double _xivc, const double _zetavc, const double _dc, const double _thetadc, const double _pidc);
  void setAdditionalVolumeCorrections(const double V3, const double V4, const double V5, const double D3, const double D4, const double D5);
  void setExtraBranch_CompressionParameter(const double compCorrection, const double compCorrection_2, const double compCorrection_3);
  void setExtraBranchNLType(const int type);
  void setTensionCompressionRegularisation(const double tensionCompressionRegularisation);
  void setCorrectionsAllBranchesTVE(const int i, const double V0,const double V1, const double V2, const double D0, const double D1, const double D2);
  void setAdditionalCorrectionsAllBranchesTVE(const int i, const double V3, const double V4, const double V5, const double D3, const double D4, const double D5);
  void setCompressionCorrectionsAllBranchesTVE(const int i, const double Ci);

  void setViscosityEffect(const viscosityLaw& v, const double p);
  void setMullinsEffect(const mullinsEffect& mullins);
  void setYieldPowerFactor(const double n);
  void nonAssociatedFlowRuleFactor(const double b);
  void setPlasticPoissonRatio(const double nup);
  void setNonAssociatedFlow(const bool flag);

  void setTaylorQuineyFactor(const double f, const bool flag = false);
  void setTemperatureFunction_InitialYieldStress(const scalarFunction& Tfunc);
  void setTemperatureFunction_Hardening(const scalarFunction& Tfunc);
  void setTemperatureFunction_KinematicHardening(const scalarFunction& Tfunc);
  void setTemperatureFunction_Viscosity(const scalarFunction& Tfunc);

  void setFailureCriterion(const mlawHyperelasticFailureCriterionBase& fCr);

#ifndef SWIG
  virtual ~LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure();
  LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure(const LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure &source);
	virtual materialLaw* clone() const {return new LocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure(*this);}
  virtual bool withEnergyDissipation() const {return _localNonLinearTVEVP->withEnergyDissipation();};
  // set the time of _nldlaw
  virtual void setTime(const double t,const double dtime);
  virtual materialLaw::matname getType() const;
  virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
  virtual double soundSpeed() const;
  virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const;
  virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent =false);
	virtual void initialIPVariable(IPVariable* ipv) const;
  virtual void setMacroSolver(const nonLinearMechSolver* sv){
		dG3DMaterialLaw::setMacroSolver(sv);
		_localNonLinearTVEVP->setMacroSolver(sv);
	};
  virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
  {
    return _localNonLinearTVEVP;
  }
  virtual materialLaw *getNonLinearSolverMaterialLaw()
  {
    return _localNonLinearTVEVP;
  }

  virtual const double bulkModulus() const {return _localNonLinearTVEVP->bulkModulus();}
  virtual double scaleFactor() const{return _localNonLinearTVEVP->scaleFactor();}
  virtual int getNumberOfExtraDofsDiffusion() const {return _localNonLinearTVEVP->getNumberOfExtraDofsDiffusion();}
  virtual double getExtraDofStoredEnergyPerUnitField(double T) const {return _localNonLinearTVEVP->getExtraDofStoredEnergyPerUnitField(T);};
  virtual double getInitialExtraDofStoredEnergyPerUnitField() const {return _localNonLinearTVEVP->getInitialExtraDofStoredEnergyPerUnitField();};

#endif
};


class NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure: public dG3DMaterialLaw
{

 protected:
  mlawNonLocalDamageNonLinearTVEVPWithFailure *_nldNonLinearTVEVP; // pointer to allow to choose between LLN style or Gmsh Style. The choice is performed by the constructor (2 constructors with != arguments)

 public:
  NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure(const int num, const double E, const double nu, const double rho,
                                const double Tinitial, const double Alpha, const double KThCon, const double Cp,
                                const J2IsotropicHardening &comp, const J2IsotropicHardening &trac,
                                const CLengthLaw &_cLLaw1, const CLengthLaw &_cLLaw2,
                                const DamageLaw &_damLaw1, const DamageLaw &_damLaw2,
                                const double tol = 1.e-6, const bool matrixbyPerturbation = false, const double pert = 1e-8,
                                const double stressIteratorTol = 1.e-9, const bool thermalEstimationPreviousConfig = true);
  NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure(const int num, const double E, const double nu, const double rho,
                                const double Tinitial, const double Alpha, const double KThCon, const double Cp,
                                const J2IsotropicHardening &comp, const J2IsotropicHardening &trac, const kinematicHardening& kin,
                                const CLengthLaw &_cLLaw1, const CLengthLaw &_cLLaw2,
                                const DamageLaw &_damLaw1, const DamageLaw &_damLaw2,
                                const double tol = 1.e-6, const bool matrixbyPerturbation = false, const double pert = 1e-8,
                                const double stressIteratorTol = 1.e-9, const bool thermalEstimationPreviousConfig = true);

  void setStrainOrder(const int order);
  void setViscoelasticMethod(const int method);
  void setViscoElasticNumberOfElement(const int N);
  void setViscoElasticData(const int i, const double Ei, const double taui);
  void setViscoElasticData_Bulk(const int i, const double Ki, const double ki);
  void setViscoElasticData_Shear(const int i, const double Gi, const double gi);
  void setViscoElasticData(const std::string filename);
  void setIsotropicHardeningCoefficients(const double HR1, const double HR2, const double HR3);
  void setPolynomialOrderChabocheCoeffs(const int order);
  void setPolynomialCoeffsChabocheCoeffs(const int i, const double val);

  void setReferenceTemperature(const double Tref);
  void setShiftFactorConstantsWLF(const double C1, const double C2);
  void setTemperatureFunction_BulkModulus(const scalarFunction& Tfunc);
  void setTemperatureFunction_ShearModulus(const scalarFunction& Tfunc);
  void setTemperatureFunction_ThermalDilationCoefficient(const scalarFunction& Tfunc);
  void setTemperatureFunction_ThermalConductivity(const scalarFunction& Tfunc);
  void setTemperatureFunction_SpecificHeat(const scalarFunction& Tfunc);
  void setTemperatureFunction_ElasticCorrection_Bulk(const scalarFunction& Tfunc);
  void setTemperatureFunction_ElasticCorrection_Shear(const scalarFunction& Tfunc);
  void useExtraBranchBool(const bool useExtraBranch);
  void useExtraBranchBool_TVE(const bool useExtraBranch_TVE, const int ExtraBranch_TVE_option);
  void useRotationCorrectionBool(const bool useRotationCorrection, const int rotationCorrectionScheme);
  void setVolumeCorrection(const double _vc, const double _xivc, const double _zetavc, const double _dc, const double _thetadc, const double _pidc);
  void setAdditionalVolumeCorrections(const double V3, const double V4, const double V5, const double D3, const double D4, const double D5);
  void setExtraBranch_CompressionParameter(const double compCorrection, const double compCorrection_2, const double compCorrection_3);
  void setExtraBranchNLType(const int type);
  void setTensionCompressionRegularisation(const double tensionCompressionRegularisation);
  void setCorrectionsAllBranchesTVE(const int i, const double V0,const double V1, const double V2, const double D0, const double D1, const double D2);
  void setAdditionalCorrectionsAllBranchesTVE(const int i, const double V3, const double V4, const double V5, const double D3, const double D4, const double D5);
  void setCompressionCorrectionsAllBranchesTVE(const int i, const double Ci);

  void setViscosityEffect(const viscosityLaw& v, const double p);
  void setMullinsEffect(const mullinsEffect& mullins);
  void setYieldPowerFactor(const double n);
  void nonAssociatedFlowRuleFactor(const double b);
  void setPlasticPoissonRatio(const double nup);
  void setNonAssociatedFlow(const bool flag);

  void setTaylorQuineyFactor(const double f, const bool flag = false);
  void setTemperatureFunction_InitialYieldStress(const scalarFunction& Tfunc);
  void setTemperatureFunction_Hardening(const scalarFunction& Tfunc);
  void setTemperatureFunction_KinematicHardening(const scalarFunction& Tfunc);
  void setTemperatureFunction_Viscosity(const scalarFunction& Tfunc);

  void setFailureCriterion(const mlawHyperelasticFailureCriterionBase& fCr);
  void setCritialDamage(const double D);

#ifndef SWIG
  virtual ~NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure();
  NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure(const NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure &source);
	virtual materialLaw* clone() const {return new NonLocalDamageNonLinearTVEVPDG3DMaterialLawWithFailure(*this);}
  virtual bool withEnergyDissipation() const {return _nldNonLinearTVEVP->withEnergyDissipation();};
  // set the time of _nldlaw
  virtual void setTime(const double t,const double dtime);
  virtual materialLaw::matname getType() const;
  virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
  virtual double soundSpeed() const;
  virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const;
  virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent =false);
	virtual void initialIPVariable(IPVariable* ipv) const;
  virtual void setMacroSolver(const nonLinearMechSolver* sv){
		dG3DMaterialLaw::setMacroSolver(sv);
		_nldNonLinearTVEVP->setMacroSolver(sv);
	};
  virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
  {
    return _nldNonLinearTVEVP;
  }
  virtual materialLaw *getNonLinearSolverMaterialLaw()
  {
    return _nldNonLinearTVEVP;
  }

  virtual const double bulkModulus() const {return _nldNonLinearTVEVP->bulkModulus();}
  virtual double scaleFactor() const{return _nldNonLinearTVEVP->scaleFactor();}
  virtual int getNumberOfExtraDofsDiffusion() const {return _nldNonLinearTVEVP->getNumberOfExtraDofsDiffusion();}
  virtual double getExtraDofStoredEnergyPerUnitField(double T) const {return _nldNonLinearTVEVP->getExtraDofStoredEnergyPerUnitField(T);};
  virtual double getInitialExtraDofStoredEnergyPerUnitField() const {return _nldNonLinearTVEVP->getInitialExtraDofStoredEnergyPerUnitField();};

#endif
};

#endif // NONLOCALDAMAGEHYPERELASTICDG3DMATERIALLAW_H_
