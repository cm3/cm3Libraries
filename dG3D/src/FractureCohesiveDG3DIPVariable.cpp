//
// C++ Interface: fracture ipvariable
//
// Description: Class with definition of fracture cohesive ipvarible for dG3D
//
//
// Author:  <L. Noels>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "FractureCohesiveDG3DIPVariable.h"
#include "ipField.h"
#include "highOrderTensor.h"
#include "nonLinearMechSolver.h"

FractureCohesive3DIPVariable::FractureCohesive3DIPVariable(const double facSigmac) :
                                                                           IPVariable2ForFracture<dG3DIPVariableBase,Cohesive3DIPVariableBase>(),
                                                                           _facSigmac(facSigmac){}
FractureCohesive3DIPVariable::FractureCohesive3DIPVariable(const FractureCohesive3DIPVariable &source) :
                                                                  IPVariable2ForFracture<dG3DIPVariableBase,Cohesive3DIPVariableBase>(source),
                                                                  _facSigmac(source._facSigmac){}
FractureCohesive3DIPVariable& FractureCohesive3DIPVariable::operator=(const IPVariable &source)
{
  IPVariable2ForFracture<dG3DIPVariableBase,Cohesive3DIPVariableBase>::operator=(source);
  const FractureCohesive3DIPVariable *src = dynamic_cast<const FractureCohesive3DIPVariable*>(&source);
  if(src != NULL)
    _facSigmac = src->_facSigmac;
  return *this;
}

void FractureCohesive3DIPVariable::restart()
{
  IPVariable2ForFracture<dG3DIPVariableBase,Cohesive3DIPVariableBase>::restart();
  restartManager::restart(_facSigmac);
  return;
}

double FractureCohesive3DIPVariable::get(const int comp) const{
  if (comp == IPField::COHESIVE_JUMP_X){
    const SVector3& cjump = _ipvfrac->getConstRefToCohesiveJump();
    return cjump(0);
  }
  else if (comp == IPField::COHESIVE_JUMP_Y){
    const SVector3& cjump = _ipvfrac->getConstRefToCohesiveJump();
    return cjump(1);
  }
  else if (comp == IPField::COHESIVE_JUMP_Z){
    const SVector3& cjump = _ipvfrac->getConstRefToCohesiveJump();
    return cjump(2);
  }
  else if (comp == IPField::BROKEN)
  {
    if (isbroken()) return 1;
    else return 0;
  }
  else{
    return _ipvbulk->get(comp);
  }
}

void FractureCohesive3DIPVariable::getBackLocalDeformationGradient(STensor3& localF) const 
{
  //Msg::Info("Here");
  localF=getBulkDeformationGradient();
  double fact=getLocalJ()/getBarJ();
  localF*=pow(fact,1./3.);
};



void MultiscaleFractureCohesive3DIPVariable::broken(){
	FractureCohesive3DIPVariable::broken();
	FractureCohesive3DIPVariable::brokenSolver(true);
};
void MultiscaleFractureCohesive3DIPVariable::nobroken(){
	FractureCohesive3DIPVariable::nobroken();
	FractureCohesive3DIPVariable::brokenSolver(false);
};
