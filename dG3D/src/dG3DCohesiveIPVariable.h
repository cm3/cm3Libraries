//
// C++ Interface: cohesive ipvariable
//
// Description: Class with definition of cohesive ipvarible for dG3D
//
//
// Author:  <L. Noels>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef DG3DCOHESIVEIPVARIABLETRIAXIALITY_H_
#define DG3DCOHESIVEIPVARIABLETRIAXIALITY_H_
#include "ipvariable.h"
#include "STensor3.h"
#include "STensor43.h"
#include "mlawCohesive.h"
#include "ipCohesive.h"

// general interface of cohesive ip
class Cohesive3DIPVariableBase : public IPVariableMechanics{
  protected:
     STensor3 _Fb; // bulk deformationGradient used for cohesive band but also for bar F method

  public:
    Cohesive3DIPVariableBase(): IPVariableMechanics(),_Fb(1.){};
    Cohesive3DIPVariableBase(const Cohesive3DIPVariableBase& src): IPVariableMechanics(src),_Fb(src._Fb){};
    virtual Cohesive3DIPVariableBase& operator =(const IPVariable& src){
      IPVariableMechanics::operator =(src);
      const Cohesive3DIPVariableBase* psrc = dynamic_cast<const Cohesive3DIPVariableBase*>(&src); 
      if (psrc!=NULL){        
        _Fb = (psrc->_Fb);
      }
      return *this;
    };
    virtual ~Cohesive3DIPVariableBase(){};
    
    // a cohesive traction natrurally depends on displacement jump
    // other ingredients can be activetd by returning true
    //
    virtual bool withDeformationGradient() const = 0; // with deformation gradient
    virtual bool withJumpGradient() const = 0; // with jump gradient
    virtual bool withIncompatibleJump() const = 0;
    
    
		// disp jump due to DG formulation at insertion time
    virtual const SVector3& getConstRefToDGJump() const = 0;
		virtual SVector3& getRefToDGJump() =0;
    
		// cohesive jump at insertion time
		virtual const SVector3& getConstRefToInitialCohesiveJump() const = 0;
		virtual SVector3& getRefToInitialCohesiveJump() = 0;
    
		// cohesive jump = ujump - ujump0+ jump0 --> insert at peak stress for cohesive continuity
		virtual const SVector3& getConstRefToCohesiveJump() const = 0;
		virtual SVector3& getRefToCohesiveJump() = 0;
		
		// interface force
		virtual const SVector3 &getConstRefToInterfaceForce() const = 0;
		virtual SVector3 &getRefToInterfaceForce() = 0;
    
    //interface depends on jump
		virtual const STensor3 &getConstRefToDInterfaceForceDjump() const = 0;
		virtual STensor3 &getRefToDInterfaceForceDjump() = 0;
    
    virtual const STensor3& getConstRefToJumpGradient() const = 0;
    virtual STensor3& getRefToJumpGradient() = 0;
    
    //interface depends on jump gradient
		virtual const STensor33 &getConstRefToDInterfaceForceDjumpGradient() const  = 0;
		virtual STensor33 &getRefToDInterfaceForceDjumpGradient()  = 0;

    //interface depends on defo
		virtual const STensor33 &getConstRefToDInterfaceForceDDeformationGradient() const = 0;
		virtual STensor33 &getRefToDInterfaceForceDDeformationGradient()  = 0;
    
    // //interface depends on incompatible jump
		virtual const STensor3& getConstRefToDInterfaceForceDIncompatibleJump() const = 0;
		virtual STensor3& getRefToDInterfaceForceDIncompatibleJump() = 0;

    // deformation gradient at onset of failure--> transition from continuum to crack
    virtual const STensor3& getConstRefToJumpGradientAtFailureOnset() const = 0;
    virtual STensor3& getRefToJumpGradientAtFailureOnset() = 0;
    
    virtual const STensor3& getConstRefToBulkDeformationGradientAtFailureOnset() const = 0;
    virtual STensor3& getRefToBulkDeformationGradientAtFailureOnset() = 0;

    virtual const SVector3& getConstRefToIncompatibleJumpAtFailureOnset() const = 0;
    virtual SVector3& getRefToIncompatibleJumpAtFailureOnset() =0;

    // deformation gradient without otherparts relating to the displacement jump
    virtual const STensor3& getBulkDeformationGradient() const {return _Fb;};
    virtual void setBulkDeformationGradient(const STensor3& Fb) {_Fb = Fb;};

		/* for current local  at cohesive surface basis*/
		// cohesive normal
		virtual const SVector3& getConstRefToCohesiveNormal() const = 0;
		virtual SVector3& getRefToCohesiveNormal() = 0;
		// cohesive tangent
		virtual const SVector3& getConstRefToCohesiveTangent() const = 0;
		virtual SVector3& getRefToCohesiveTangent() = 0;

		virtual const SVector3& getConstRefToCohesiveBiTangent() const = 0;
		virtual SVector3& getRefToCohesiveBiTangent() = 0;

		/*for reference local basis at cohesive surface*/
    virtual const SVector3& getConstRefToCohesiveReferenceNormal() const = 0;
		virtual SVector3& getRefToCohesiveReferenceNormal() =0;
		// cohesive tangent
		virtual const SVector3& getConstRefToCohesiveReferenceTangent() const = 0;
		virtual SVector3& getRefToCohesiveReferenceTangent() = 0;

		virtual const SVector3& getConstRefToCohesiveReferenceBiTangent() const = 0;
		virtual SVector3& getRefToCohesiveReferenceBiTangent() = 0;

    // cohesive jump depend on other parameter
		virtual const STensor3& getConstRefToDCohesiveJumpDJump() const  = 0;
    virtual STensor3& getRefToDCohesiveJumpDJump() = 0;

    virtual const STensor33& getConstRefToDCohesiveJumpDDeformationGradient() const = 0;
    virtual STensor33& getRefToDCohesiveJumpDDeformationGradient() =0;

    virtual const STensor3& getConstRefToDCohesiveJumpDIncompatibleJump() const = 0;
    virtual STensor3& getRefToDCohesiveJumpDIncompatibleJump() = 0;
    
    virtual const double& getConstRefToOpeningOffset() const = 0;
    virtual double& getRefToOpeningOffset() = 0;

    // when working with triaxiality effective
    // the deformation gradient is defined at interface from bulk deformation
    // gradient and from displacement jump
    virtual const STensor33& getConstRefToDInterfaceDeformationGradientDJump() const = 0;
    virtual STensor33& getRefToDInterfaceDeformationGradientDJump() = 0;

    virtual const STensor43& getConstRefToDInterfaceDeformationGradientDDeformationGradient() const = 0;
    virtual STensor43& getRefToDInterfaceDeformationGradientDDeformationGradient() =0;

    virtual const STensor33& getConstRefToDInterfaceDeformationGradientDIncompatibleJump() const =0;
    virtual STensor33& getRefToDInterfaceDeformationGradientDIncompatibleJump() =0;
		
    virtual double defoEnergy() const = 0;
    virtual double plasticEnergy() const = 0;
    virtual double damageEnergy() const = 0;
		//
		virtual double irreversibleEnergy() const = 0;
		virtual double& getRefToIrreversibleEnergy() = 0;
		
		virtual SVector3& getRefToDIrreversibleEnergyDJump() =0;
		virtual const SVector3& getConstRefToDIrreversibleEnergyDJump() const =0;
    
    virtual const STensor3& getConstRefToDIrreversibleEnergyDJumpGradient() const = 0;
    virtual STensor3& getRefToDIrreversibleEnergyDJumpGradient() = 0;
		
		virtual const STensor3& getConstRefToDIrreversibleEnergyDDeformationGradient() const = 0;
		virtual STensor3& getRefToDIrreversibleEnergyDDeformationGradient() = 0;
		
		virtual const SVector3& getConstRefToDIrreversibleEnergyDIncompatibleJump() const =0;
    virtual SVector3& getRefToDIrreversibleEnergyDIncompatibleJump() =0;

    // true if local basis is computed from domain
		virtual bool useLocalBasis() const = 0;

		// for tension or compression state
		virtual bool ifTension() const = 0; //
		virtual void setTensionFlag(const bool tensionflag) = 0;

    // initial parameters of cohesive ip
    virtual void initializeFracture(const SVector3 &ujump_,
                            const double Kp_, // compressive penalty
                            const double seff, const double snor, const double tau, const double Gc,
                            const double beta_, const bool ift, const STensor3 & cauchy,
                            const double K, // penalty
                            const SVector3& normal,  // normal
                            const SVector3& f0,  // interface force at insertion time
                            const IPVariable* bulkIPv // bulk data at insertion tim
                            ) = 0;
		virtual IPVariable* clone() const = 0;
    virtual int fractureEnergy(double* arrayEnergy) const = 0;
    virtual void restart() {
      IPVariableMechanics::restart();
      restartManager::restart(_Fb);

    };
};

// general interface of cohesive ip for displacement jump-based cohesive law
class Cohesive3DIPVariable : public Cohesive3DIPVariableBase{
	protected:
    SVector3 _interfaceForce;
    STensor3 _DInterfaceForceDjump;		
    SVector3 _ujump0; // due to DG
    SVector3 _cohesiveJump0; // cohesive jump at insertion
    SVector3 _cohesiveJump; // cohesive jump = ujump0 - ujump0+  jump0
    SVector3 _cohesiveNormal; // normal of cohesive layer
    IPCohesive* _qCohesive;
    
  public:
    Cohesive3DIPVariable(const CohesiveLawBase& law);
    Cohesive3DIPVariable(const Cohesive3DIPVariable& src);
    virtual Cohesive3DIPVariable& operator =(const IPVariable& src);
    virtual ~Cohesive3DIPVariable();
    
    virtual IPCohesive* getIPCohesive() {return _qCohesive;};
    virtual const IPCohesive* getIPCohesive() const {return _qCohesive;};
    
    virtual void Delete(const bool fl) {
        Cohesive3DIPVariableBase::Delete(fl);
        if (_qCohesive != NULL)
        {
          _qCohesive->Delete(fl);
        }
    };
    virtual bool isDeleted() const
    {
      if (_qCohesive != NULL)
      {
        return _qCohesive->isDeleted();
      }
      return Cohesive3DIPVariableBase::isDeleted();
  }
    
    virtual bool withDeformationGradient() const {return false;}; // with deformation gradient
    virtual bool withJumpGradient() const {return false;}; // with jump gradient
    virtual bool withIncompatibleJump() const {return false;};
  
		// disp jump due to DG formulation at insertion time
    virtual const SVector3& getConstRefToDGJump() const {return _ujump0;};
		virtual SVector3& getRefToDGJump() {return _ujump0;};
		// cohesive jump at insertion time
		virtual const SVector3& getConstRefToInitialCohesiveJump() const {return _cohesiveJump0;};
		virtual SVector3& getRefToInitialCohesiveJump() {return _cohesiveJump0;};
		// cohesive jump = ujump - ujump0+ jump0 --> insert at peak stress for cohesive continuity
		virtual const SVector3& getConstRefToCohesiveJump() const {return _cohesiveJump;};
		virtual SVector3& getRefToCohesiveJump() {return _cohesiveJump;};
		
		// interface force
		virtual const SVector3 &getConstRefToInterfaceForce() const {return _interfaceForce;};
		virtual SVector3 &getRefToInterfaceForce() {return _interfaceForce;};

		virtual const STensor3 &getConstRefToDInterfaceForceDjump() const {return _DInterfaceForceDjump;};
		virtual STensor3 &getRefToDInterfaceForceDjump() {return _DInterfaceForceDjump;};
    
    virtual const STensor3& getConstRefToJumpGradient() const {Msg::Error("Cohesive3DIPVariable::getConstRefToJumpGradient is not defined"); static STensor3 a; return a;};;
    virtual STensor3& getRefToJumpGradient() {Msg::Error("Cohesive3DIPVariable::getRefToJumpGradient is not defined"); static STensor3 a; return a;};

		virtual const STensor33 &getConstRefToDInterfaceForceDjumpGradient() const {Msg::Error("Cohesive3DIPVariable::getConstRefToDInterfaceForceDjumpGradient is not defined");static STensor33 a; return a;};
		virtual STensor33 &getRefToDInterfaceForceDjumpGradient() { Msg::Error("Cohesive3DIPVariable::getRefToDInterfaceForceDjumpGradient is not defined"); static STensor33 a; return a;};

		virtual const STensor33 &getConstRefToDInterfaceForceDDeformationGradient() const {Msg::Error("Cohesive3DIPVariable::getConstRefToDInterfaceForceDDeformationGradient is not defined");static STensor33 a; return a;};
		virtual STensor33 &getRefToDInterfaceForceDDeformationGradient(){Msg::Error("Cohesive3DIPVariable::getRefToDInterfaceForceDDeformationGradient is not defined");static STensor33 a; return a;};

		virtual const STensor3& getConstRefToDInterfaceForceDIncompatibleJump() const {Msg::Error("Cohesive3DIPVariable::getConstRefToDInterfaceForceDIncompatibleJump is not defined");static STensor3 a; return a;};
		virtual STensor3& getRefToDInterfaceForceDIncompatibleJump()  {Msg::Error("Cohesive3DIPVariable::getRefToDInterfaceForceDIncompatibleJump is not defined");static STensor3 a; return a;};

    // deformation gradient at onset of failure--> transition from continuum to crack
    virtual const STensor3& getConstRefToJumpGradientAtFailureOnset() const {Msg::Error("Cohesive3DIPVariable::getConstRefToJumpGradientAtFailureOnset is not defined");static STensor3 a; return a;};
    virtual STensor3& getRefToJumpGradientAtFailureOnset() {Msg::Error("Cohesive3DIPVariable::getRefToJumpGradientAtFailureOnset is not defined");static STensor3 a; return a;};
    
    virtual const STensor3& getConstRefToBulkDeformationGradientAtFailureOnset() const {Msg::Error("Cohesive3DIPVariable::getConstRefToBulkDeformationGradientAtFailureOnset is not defined");static STensor3 a; return a;};
    virtual STensor3& getRefToBulkDeformationGradientAtFailureOnset() {Msg::Error("Cohesive3DIPVariable::getRefToBulkDeformationGradientAtFailureOnset is not defined");static STensor3 a; return a;};

    virtual const SVector3& getConstRefToIncompatibleJumpAtFailureOnset() const {Msg::Error("Cohesive3DIPVariable::getConstRefToIncompatibleJumpAtFailureOnset is not defined");static SVector3 a; return a;};
    virtual SVector3& getRefToIncompatibleJumpAtFailureOnset() {Msg::Error("Cohesive3DIPVariable::getRefToIncompatibleJumpAtFailureOnset is not defined");static SVector3 a; return a;};


		/* for current local  at cohesive surface basis*/
		// cohesive normal
		virtual const SVector3& getConstRefToCohesiveNormal() const {return _cohesiveNormal;};
		virtual SVector3& getRefToCohesiveNormal() {return _cohesiveNormal;};
		// cohesive tangent
		virtual const SVector3& getConstRefToCohesiveTangent() const {Msg::Error("Cohesive3DIPVariable::getConstRefToCohesiveTangent is not defined");static SVector3 a; return a;};
		virtual SVector3& getRefToCohesiveTangent() {Msg::Error("Cohesive3DIPVariable::getRefToCohesiveTangent is not defined");static SVector3 a; return a;};

		virtual const SVector3& getConstRefToCohesiveBiTangent() const {Msg::Error("Cohesive3DIPVariable::getConstRefToCohesiveBiTangent is not defined");static SVector3 a; return a;};
		virtual SVector3& getRefToCohesiveBiTangent() {Msg::Error("Cohesive3DIPVariable::getRefToCohesiveBiTangent is not defined");static SVector3 a; return a;};

		/*for reference local basis at cohesive surface*/
    virtual const SVector3& getConstRefToCohesiveReferenceNormal() const {Msg::Error("Cohesive3DIPVariable::getConstRefToCohesiveReferenceNormal is not defined");static SVector3 a; return a;};
		virtual SVector3& getRefToCohesiveReferenceNormal() {Msg::Error("Cohesive3DIPVariable::getRefToCohesiveReferenceNormal is not defined");static SVector3 a; return a;};
		// cohesive tangent
		virtual const SVector3& getConstRefToCohesiveReferenceTangent() const {Msg::Error("Cohesive3DIPVariable::getConstRefToCohesiveReferenceTangent is not defined");static SVector3 a; return a;};
		virtual SVector3& getRefToCohesiveReferenceTangent() {Msg::Error("Cohesive3DIPVariable::getRefToCohesiveReferenceTangent is not defined");static SVector3 a; return a;};

		virtual const SVector3& getConstRefToCohesiveReferenceBiTangent() const {Msg::Error("Cohesive3DIPVariable::getConstRefToCohesiveReferenceBiTangent is not defined");static SVector3 a; return a;};
		virtual SVector3& getRefToCohesiveReferenceBiTangent() {Msg::Error("Cohesive3DIPVariable::getRefToCohesiveReferenceBiTangent is not defined");static SVector3 a; return a;};

		virtual const STensor3& getConstRefToDCohesiveJumpDJump() const  {Msg::Error("Cohesive3DIPVariable::getConstRefToDCohesiveJumpDJump is not defined");static STensor3 a; return a;};
    virtual STensor3& getRefToDCohesiveJumpDJump() {Msg::Error("Cohesive3DIPVariable::getRefToDCohesiveJumpDJump is not defined");static STensor3 a; return a;};

    virtual const STensor33& getConstRefToDCohesiveJumpDDeformationGradient() const {Msg::Error("Cohesive3DIPVariable::getConstRefToDCohesiveJumpDDeformationGradient is not defined");static STensor33 a; return a;};
    virtual STensor33& getRefToDCohesiveJumpDDeformationGradient() {Msg::Error("Cohesive3DIPVariable::getRefToDCohesiveJumpDDeformationGradient is not defined");static STensor33 a; return a;};

    virtual const STensor3& getConstRefToDCohesiveJumpDIncompatibleJump() const {Msg::Error("Cohesive3DIPVariable::getConstRefToDCohesiveJumpDIncompatibleJump is not defined");static STensor3 a; return a;};
    virtual STensor3& getRefToDCohesiveJumpDIncompatibleJump() {Msg::Error("Cohesive3DIPVariable::getRefToDCohesiveJumpDIncompatibleJump is not defined");static STensor3 a; return a;};
    
    virtual const double& getConstRefToOpeningOffset() const {Msg::Error("Cohesive3DIPVariable::getConstRefToOpeningOffset is not defined");static double a(0.); return a;};
    virtual double& getRefToOpeningOffset() {Msg::Error("Cohesive3DIPVariable::getRefToOpeningOffset is not defined");static double a(0.); return a;};

    // when working with triaxiality effective
    // the deformation gradient is defined at interface from bulk deformation
    // gradient and from displacement jump
    virtual const STensor33& getConstRefToDInterfaceDeformationGradientDJump() const {Msg::Error("Cohesive3DIPVariable::getConstRefToDInterfaceDeformationGradientDJump is not defined");static STensor33 a; return a;};
    virtual STensor33& getRefToDInterfaceDeformationGradientDJump() {Msg::Error("Cohesive3DIPVariable::getRefToDInterfaceDeformationGradientDJump is not defined");static STensor33 a; return a;};

    virtual const STensor43& getConstRefToDInterfaceDeformationGradientDDeformationGradient() const {Msg::Error("Cohesive3DIPVariable::getConstRefToDInterfaceDeformationGradientDDeformationGradient is not defined");static STensor43 a; return a;};
    virtual STensor43& getRefToDInterfaceDeformationGradientDDeformationGradient() {Msg::Error("Cohesive3DIPVariable::getRefToDInterfaceDeformationGradientDDeformationGradient is not defined");static STensor43 a; return a;};

    virtual const STensor33& getConstRefToDInterfaceDeformationGradientDIncompatibleJump() const {Msg::Error("Cohesive3DIPVariable::getConstRefToDInterfaceDeformationGradientDIncompatibleJump is not defined");static STensor33 a; return a;};
    virtual STensor33& getRefToDInterfaceDeformationGradientDIncompatibleJump() {Msg::Error("Cohesive3DIPVariable::getRefToDInterfaceDeformationGradientDIncompatibleJump is not defined");static STensor33 a; return a;};
		
    virtual double defoEnergy() const {return _qCohesive->defoEnergy();};
    virtual double plasticEnergy() const {return _qCohesive->plasticEnergy();};
    virtual double damageEnergy() const {return _qCohesive->damageEnergy();};
		//
		virtual double irreversibleEnergy() const {return _qCohesive->irreversibleEnergy();};
    virtual double& getRefToIrreversibleEnergy() {return _qCohesive->getRefToIrreversibleEnergy();};
		
		virtual SVector3& getRefToDIrreversibleEnergyDJump() {return _qCohesive->getRefToDIrreversibleEnergyDJump();};
    virtual const SVector3& getConstRefToDIrreversibleEnergyDJump() const {return _qCohesive->getConstRefToDIrreversibleEnergyDJump();};
    
    virtual const STensor3& getConstRefToDIrreversibleEnergyDJumpGradient() const {Msg::Error("Cohesive3DIPVariable::getConstRefToDIrreversibleEnergyDJumpGradient is not defined");static STensor3 a; return a;};
    virtual STensor3& getRefToDIrreversibleEnergyDJumpGradient() {Msg::Error("Cohesive3DIPVariable::getRefToDIrreversibleEnergyDJumpGradient is not defined");static STensor3 a; return a;};
		
		virtual const STensor3& getConstRefToDIrreversibleEnergyDDeformationGradient() const {Msg::Error("Cohesive3DIPVariable::getConstRefToDIrreversibleEnergyDDeformationGradient is not defined");static STensor3 a; return a;};
		virtual STensor3& getRefToDIrreversibleEnergyDDeformationGradient() {Msg::Error("Cohesive3DIPVariable::getRefToDIrreversibleEnergyDDeformationGradient is not defined");static STensor3 a; return a;};
		
		virtual const SVector3& getConstRefToDIrreversibleEnergyDIncompatibleJump() const {Msg::Error("Cohesive3DIPVariable::getConstRefToDIrreversibleEnergyDIncompatibleJump is not defined");static SVector3 a; return a;};
    virtual SVector3& getRefToDIrreversibleEnergyDIncompatibleJump() {Msg::Error("Cohesive3DIPVariable::getConstRefToDIrreversibleEnergyDIncompatibleJump is not defined");static SVector3 a; return a;};

		virtual bool useLocalBasis() const {return false;};

		// for tension or compression state
		virtual bool ifTension() const {return _qCohesive->ifTension();}; //
		virtual void setTensionFlag(const bool tensionflag) {_qCohesive->getRefToTensionFlag() = tensionflag;};

    // initial parameters of cohesive ip
    virtual void initializeFracture(const SVector3 &ujump_, 
                            const double Kp_, // compressive penalty
                            const double seff, const double snor, const double tau, const double Gc,
                            const double beta_, const bool ift, const STensor3 & cauchy,
                            const double K, // penalty
                            const SVector3& normal,  // normal
                            const SVector3& f0,  // interface force at insertion time
                            const IPVariable* bulkIPv // bulk data at insertion tim
                            );
		virtual IPVariable* clone() const = 0;
    virtual int fractureEnergy(double* arrayEnergy) const{arrayEnergy[0]=_qCohesive->getConstRefToFractureEnergy(); return 1;}
    virtual void restart();
};

class ElasticCohesive3DIPVariable : public Cohesive3DIPVariable{
 public :
  ElasticCohesive3DIPVariable(const CohesiveLawBase& law);
  ElasticCohesive3DIPVariable(const ElasticCohesive3DIPVariable &source);
  virtual ElasticCohesive3DIPVariable &operator = (const IPVariable &source);
  virtual ~ElasticCohesive3DIPVariable();
  virtual IPVariable* clone() const {return new ElasticCohesive3DIPVariable(*this);};
  virtual void initializeFracture(const SVector3 &ujump_, 
                            const double Kp_, // compressive penalty
                            const double seff, const double snor, const double tau, const double Gc,
                            const double beta_, const bool ift, const STensor3 & cauchy,
                            const double K, // penalty
                            const SVector3& normal,  // normal
                            const SVector3& f0,  // interface force at insertion time
                            const IPVariable* bulkIPv // bulk data at insertion tim
                            );
};


class LinearCohesive3DIPVariable : public Cohesive3DIPVariable{
 public :
  LinearCohesive3DIPVariable(const CohesiveLawBase& law);
  LinearCohesive3DIPVariable(const LinearCohesive3DIPVariable &source);
  virtual LinearCohesive3DIPVariable &operator = (const IPVariable &source);
  virtual ~LinearCohesive3DIPVariable();
  virtual IPVariable* clone() const {return new LinearCohesive3DIPVariable(*this);};
};

class BaseTransverseIsotropicLinearCohesive3DIPVariable : public Cohesive3DIPVariable{
 protected :
  // - Basis in current config
  SVector3 _cohesiveTangent;      // cohesive tangent
  SVector3 _cohesiveBiTangent;    // cohesive bitangent
  // - Basis in reference config
  SVector3 _cohesiveRefNormal;    // cohesive normal in reference config
  SVector3 _cohesiveRefTangent;   // cohesive tangent
  SVector3 _cohesiveRefBiTangent; // cohesive bitangent

 public :
  // no initial fracture (for initial fracture use ipfield)
  BaseTransverseIsotropicLinearCohesive3DIPVariable(const CohesiveLawBase& law);
  virtual ~BaseTransverseIsotropicLinearCohesive3DIPVariable();
  BaseTransverseIsotropicLinearCohesive3DIPVariable(const BaseTransverseIsotropicLinearCohesive3DIPVariable &source);
  virtual BaseTransverseIsotropicLinearCohesive3DIPVariable &operator = (const IPVariable &source);

  // cohesive basis - in current config
  virtual const SVector3& getConstRefToCohesiveTangent() const {return _cohesiveTangent;};
  virtual SVector3& getRefToCohesiveTangent() {return _cohesiveTangent;};
  virtual const SVector3& getConstRefToCohesiveBiTangent() const {return _cohesiveBiTangent;};
  virtual SVector3& getRefToCohesiveBiTangent() {return _cohesiveBiTangent;};
  // cohesive normal - in reference config
  virtual const SVector3& getConstRefToCohesiveReferenceNormal() const {return _cohesiveRefNormal;};
  virtual SVector3& getRefToCohesiveReferenceNormal() {return _cohesiveRefNormal;};
  virtual const SVector3& getConstRefToCohesiveReferenceTangent() const {return _cohesiveRefTangent;};
  virtual SVector3& getRefToCohesiveReferenceTangent() {return _cohesiveRefTangent;};
  virtual const SVector3& getConstRefToCohesiveReferenceBiTangent() const {return _cohesiveRefBiTangent;};
  virtual SVector3& getRefToCohesiveReferenceBiTangent() {return _cohesiveRefBiTangent;};

  virtual bool useLocalBasis() const {return true;};
  virtual IPVariable* clone() const {return new BaseTransverseIsotropicLinearCohesive3DIPVariable(*this);};
  virtual void restart();

};

class TransverseIsotropicLinearCohesive3DIPVariable : public BaseTransverseIsotropicLinearCohesive3DIPVariable{
 protected :

 public :
  // no initial fracture (for initial fracture use ipfield)
  TransverseIsotropicLinearCohesive3DIPVariable(const CohesiveLawBase& law);
  virtual ~TransverseIsotropicLinearCohesive3DIPVariable();
  TransverseIsotropicLinearCohesive3DIPVariable(const TransverseIsotropicLinearCohesive3DIPVariable &source);
  virtual TransverseIsotropicLinearCohesive3DIPVariable &operator = (const IPVariable &source);


  virtual IPVariable* clone() const {return new TransverseIsotropicLinearCohesive3DIPVariable(*this);};
  virtual void restart();

};

class TransverseIsoCurvatureLinearCohesive3DIPVariable : public BaseTransverseIsotropicLinearCohesive3DIPVariable{
 protected :

 public :
  // no initial fracture (for initial fracture use ipfield)
  TransverseIsoCurvatureLinearCohesive3DIPVariable(const CohesiveLawBase& law, const SVector3 &GaussP, const SVector3 &Centroid, const SVector3 &PointStart1, const SVector3 &PointStart2, const double init_Angle);
  virtual ~TransverseIsoCurvatureLinearCohesive3DIPVariable();
  TransverseIsoCurvatureLinearCohesive3DIPVariable(const TransverseIsoCurvatureLinearCohesive3DIPVariable &source);
  virtual TransverseIsoCurvatureLinearCohesive3DIPVariable &operator = (const IPVariable &source);


  virtual IPVariable* clone() const {return new TransverseIsoCurvatureLinearCohesive3DIPVariable(*this);};
  virtual void restart();

 protected:
  TransverseIsoCurvatureLinearCohesive3DIPVariable(const CohesiveLawBase& law) : BaseTransverseIsotropicLinearCohesive3DIPVariable(law)
  {
    Msg::Error("Cannot construct default TransverseIsoCurvatureLinearCohesive3DIPVariable");
  }

};

class NonLocalDamageLinearCohesive3DIPVariable : public LinearCohesive3DIPVariable{
 protected :

 public :
  NonLocalDamageLinearCohesive3DIPVariable(const CohesiveLawBase& law);// no initial fracture (for initial fracture use ipfield)
  virtual ~NonLocalDamageLinearCohesive3DIPVariable(){}
  NonLocalDamageLinearCohesive3DIPVariable(const NonLocalDamageLinearCohesive3DIPVariable &source);
  virtual NonLocalDamageLinearCohesive3DIPVariable &operator = (const IPVariable &source);

  virtual void initializeFracture(const SVector3 &ujump_, const double Kp_,
                          const double smax, const double snor, const double tau, const double Gc,
                          const double beta_, const bool ift, const STensor3 &, const double Kp,
                          const SVector3& normDir, const SVector3& f0,
                          const IPVariable* bulkIPv);
  virtual IPVariable* clone() const {return new NonLocalDamageLinearCohesive3DIPVariable(*this);};
  virtual void restart();
};


class NonLocalDamageTransverseIsotropicLinearCohesive3DIPVariable : public BaseTransverseIsotropicLinearCohesive3DIPVariable{
 protected :

 public :
  NonLocalDamageTransverseIsotropicLinearCohesive3DIPVariable(const CohesiveLawBase& law);// no initial fracture (for initial fracture use ipfield)
  virtual ~NonLocalDamageTransverseIsotropicLinearCohesive3DIPVariable(){};
  NonLocalDamageTransverseIsotropicLinearCohesive3DIPVariable(const NonLocalDamageTransverseIsotropicLinearCohesive3DIPVariable &source);
  virtual NonLocalDamageTransverseIsotropicLinearCohesive3DIPVariable &operator = (const IPVariable &source);

  virtual void initializeFracture(const SVector3 &ujump_,  const double Kp_,
                          const double smax, const double snor, const double tau, const double Gc,
                          const double beta_, const bool ift, const STensor3 &, const double Kp,
                          const SVector3& normDir, const SVector3& f0,
                          const IPVariable* bulkIPv);
  virtual IPVariable* clone() const {return new NonLocalDamageTransverseIsotropicLinearCohesive3DIPVariable(*this);};
  virtual void restart();
};



class DelaminationLinearCohesive3DIPVariable : public LinearCohesive3DIPVariable{
 protected :
  // variables use by cohesive law
 public :

  DelaminationLinearCohesive3DIPVariable(const CohesiveLawBase& law);// no initial fracture (for initial fracture use ipfield)
  virtual ~DelaminationLinearCohesive3DIPVariable(){}
  DelaminationLinearCohesive3DIPVariable(const DelaminationLinearCohesive3DIPVariable &source);
  virtual DelaminationLinearCohesive3DIPVariable &operator = (const IPVariable &source);

  virtual void initializeFracture(const SVector3 &ujump_,  const double Kp_,
                          const double smax, const double snor, const double tau, const double Gc,
                          const double beta_, const bool ift,  const STensor3 &cauchy, const double K,
                          const SVector3& norm, const SVector3& f0,
                          const IPVariable* bulkIPv);
  virtual IPVariable* clone() const {return new DelaminationLinearCohesive3DIPVariable(*this);};
  virtual void restart();
};

// general cohsive Ipv for cohesive data extrated from a bulk law
class BulkFollowedCohesive3DIPVariable : public Cohesive3DIPVariableBase{
  protected:
    bool _isTension;
    // interface force
    SVector3 _interfaceForce;
    STensor3 _DInterfaceForceDjump;		
    STensor33 _DInterfaceForceDF;		
    
    SVector3 _cohesiveJump; // cohesive jump = ujump0 - ujump0
		
    SVector3 _ujump0; // displacement jump at insertion time
    STensor3 _Fb0; // bulk deformationGradient at insertion time
    
    //defined interface strain
    STensor43 _dFinterfacedF;
    STensor33 _dFinterfacedJump;
    
    double _irreversibleEnergy;
    SVector3 _DirreversibleEnergyDjump;
    STensor3 _DirreversibleEnergyDF;

  public:
    BulkFollowedCohesive3DIPVariable(): Cohesive3DIPVariableBase(), 
    _isTension(true),_interfaceForce(0.),
    _DInterfaceForceDjump(0.),_DInterfaceForceDF(0.),
    _cohesiveJump(0.),_ujump0(0.),_Fb0(1.),
    _dFinterfacedF(0.), _dFinterfacedJump(0.),
    _irreversibleEnergy(0.), _DirreversibleEnergyDjump(0.),
    _DirreversibleEnergyDF(0.){};
    
    BulkFollowedCohesive3DIPVariable(const BulkFollowedCohesive3DIPVariable& src): Cohesive3DIPVariableBase(src), 
    _isTension(src._isTension),_interfaceForce(src._interfaceForce),
    _DInterfaceForceDjump(src._DInterfaceForceDjump),_DInterfaceForceDF(src._DInterfaceForceDF),
    _cohesiveJump(src._cohesiveJump),_ujump0(src._ujump0),_Fb0(src._Fb0),
    _dFinterfacedF(src._dFinterfacedF), _dFinterfacedJump(src._dFinterfacedJump),
    _irreversibleEnergy(src._irreversibleEnergy), _DirreversibleEnergyDjump(src._DirreversibleEnergyDjump),
    _DirreversibleEnergyDF(src._DirreversibleEnergyDF){};
    
    virtual BulkFollowedCohesive3DIPVariable& operator =(const IPVariable& src){
      Cohesive3DIPVariableBase::operator =(src);
      const BulkFollowedCohesive3DIPVariable* psrc = dynamic_cast<const BulkFollowedCohesive3DIPVariable*>(&src);
      if (psrc != NULL){
        _isTension = (psrc->_isTension);
        _interfaceForce = (psrc->_interfaceForce);
        _DInterfaceForceDjump = (psrc->_DInterfaceForceDjump);
        _DInterfaceForceDF = (psrc->_DInterfaceForceDF);
        _cohesiveJump = (psrc->_cohesiveJump);
        _ujump0 = (psrc->_ujump0);
        _Fb0 = (psrc->_Fb0);
        _dFinterfacedF = (psrc->_dFinterfacedF);
        _dFinterfacedJump = (psrc->_dFinterfacedJump);
        _irreversibleEnergy = (psrc->_irreversibleEnergy);
        _DirreversibleEnergyDjump = (psrc->_DirreversibleEnergyDjump);
        _DirreversibleEnergyDF = (psrc->_DirreversibleEnergyDF);
      }
      return *this;
    };
    virtual ~BulkFollowedCohesive3DIPVariable(){};
    
    // a cohesive traction natrurally depends on displacement jump
    // other ingredients can be activetd by returning true
    //
    virtual bool withDeformationGradient() const {return true;}; // with deformation gradient
    virtual bool withJumpGradient() const {return false;}; // with jump gradient
    virtual bool withIncompatibleJump() const {return false;};
    
    
		// disp jump due to DG formulation at insertion time
    virtual const SVector3& getConstRefToDGJump() const {return _ujump0;};
		virtual SVector3& getRefToDGJump() {return _ujump0;};
    
		// cohesive jump at insertion time
		virtual const SVector3& getConstRefToInitialCohesiveJump() const {Msg::Error("BulkFollowedCohesive3DIPVariable::getConstRefToInitialCohesiveJump is not defined");static SVector3 a; return a;};
		virtual SVector3& getRefToInitialCohesiveJump() {Msg::Error("BulkFollowedCohesive3DIPVariable::getRefToInitialCohesiveJump is not defined");static SVector3 a; return a;};
    
		// cohesive jump = ujump - ujump0+ jump0 --> insert at peak stress for cohesive continuity
		virtual const SVector3& getConstRefToCohesiveJump() const {return _cohesiveJump;}
		virtual SVector3& getRefToCohesiveJump() {return _cohesiveJump;};
		
		// interface force
		virtual const SVector3 &getConstRefToInterfaceForce() const {return _interfaceForce;};
		virtual SVector3 &getRefToInterfaceForce() {return _interfaceForce;};
    
    //interface depends on jump
		virtual const STensor3 &getConstRefToDInterfaceForceDjump() const {return _DInterfaceForceDjump;};
		virtual STensor3 &getRefToDInterfaceForceDjump() {return _DInterfaceForceDjump;};
    
    virtual const STensor3& getConstRefToJumpGradient() const {Msg::Error("Cohesive3DIPVariable::getConstRefToJumpGradient is not defined");static STensor3 a; return a;};;
    virtual STensor3& getRefToJumpGradient() {Msg::Error("Cohesive3DIPVariable::getRefToJumpGradient is not defined");static STensor3 a; return a;};
    
    //interface depends on jump gradient
		virtual const STensor33 &getConstRefToDInterfaceForceDjumpGradient() const {Msg::Error("BulkFollowedCohesive3DIPVariable::getConstRefToDInterfaceForceDjumpGradient is not defined");static STensor33 a; return a;};
		virtual STensor33 &getRefToDInterfaceForceDjumpGradient()  {Msg::Error("BulkFollowedCohesive3DIPVariable::getRefToDInterfaceForceDjumpGradient is not defined");static STensor33 a; return a;};

    //interface depends on defo
		virtual const STensor33 &getConstRefToDInterfaceForceDDeformationGradient() const {return _DInterfaceForceDF;};
		virtual STensor33 &getRefToDInterfaceForceDDeformationGradient()  {return _DInterfaceForceDF;};
    
    // //interface depends on incompatible jump
		virtual const STensor3& getConstRefToDInterfaceForceDIncompatibleJump() const {Msg::Error("BulkFollowedCohesive3DIPVariable::getConstRefToDInterfaceForceDIncompatibleJump is not defined");static STensor3 a; return a;};
		virtual STensor3& getRefToDInterfaceForceDIncompatibleJump() {Msg::Error("BulkFollowedCohesive3DIPVariable::getRefToDInterfaceForceDIncompatibleJump is not defined");static STensor3 a; return a;};

    // deformation gradient at onset of failure--> transition from continuum to crack
    virtual const STensor3& getConstRefToJumpGradientAtFailureOnset() const {Msg::Error("Cohesive3DIPVariable::getConstRefToJumpGradientAtFailureOnset is not defined");static STensor3 a; return a;};
    virtual STensor3& getRefToJumpGradientAtFailureOnset() {Msg::Error("Cohesive3DIPVariable::getRefToJumpGradientAtFailureOnset is not defined");static STensor3 a; return a;};
    
    virtual const STensor3& getConstRefToBulkDeformationGradientAtFailureOnset() const {return _Fb0;};
    virtual STensor3& getRefToBulkDeformationGradientAtFailureOnset() {return _Fb0;};

    virtual const SVector3& getConstRefToIncompatibleJumpAtFailureOnset() const {Msg::Error("BulkFollowedCohesive3DIPVariable::getConstRefToIncompatibleJumpAtFailureOnset is not defined");static SVector3 a; return a;};
    virtual SVector3& getRefToIncompatibleJumpAtFailureOnset() {Msg::Error("BulkFollowedCohesive3DIPVariable::getRefToIncompatibleJumpAtFailureOnset is not defined");static SVector3 a; return a;};


		/* for current local  at cohesive surface basis*/
		// cohesive normal
		virtual const SVector3& getConstRefToCohesiveNormal() const {Msg::Error("BulkFollowedCohesive3DIPVariable::getConstRefToCohesiveNormal is not defined");static SVector3 a; return a;};
		virtual SVector3& getRefToCohesiveNormal() {Msg::Error("BulkFollowedCohesive3DIPVariable::getRefToCohesiveNormal is not defined");static SVector3 a; return a;};
		// cohesive tangent
		virtual const SVector3& getConstRefToCohesiveTangent() const  {Msg::Error("BulkFollowedCohesive3DIPVariable::getConstRefToCohesiveTangent is not defined");static SVector3 a; return a;};
		virtual SVector3& getRefToCohesiveTangent()  {Msg::Error("BulkFollowedCohesive3DIPVariable::getRefToCohesiveTangent is not defined");static SVector3 a; return a;};

		virtual const SVector3& getConstRefToCohesiveBiTangent() const  {Msg::Error("BulkFollowedCohesive3DIPVariable::getConstRefToCohesiveBiTangent is not defined");static SVector3 a; return a;};
		virtual SVector3& getRefToCohesiveBiTangent()  {Msg::Error("BulkFollowedCohesive3DIPVariable::getRefToCohesiveBiTangent is not defined");static SVector3 a; return a;};

		/*for reference local basis at cohesive surface*/
    virtual const SVector3& getConstRefToCohesiveReferenceNormal() const {Msg::Error("BulkFollowedCohesive3DIPVariable::getConstRefToCohesiveReferenceNormal is not defined");static SVector3 a; return a;};
		virtual SVector3& getRefToCohesiveReferenceNormal() {Msg::Error("BulkFollowedCohesive3DIPVariable::getRefToCohesiveReferenceNormal is not defined");static SVector3 a; return a;};
		// cohesive tangent
		virtual const SVector3& getConstRefToCohesiveReferenceTangent() const {Msg::Error("BulkFollowedCohesive3DIPVariable::getConstRefToCohesiveReferenceTangent is not defined");static SVector3 a; return a;};
		virtual SVector3& getRefToCohesiveReferenceTangent() {Msg::Error("BulkFollowedCohesive3DIPVariable::getRefToCohesiveReferenceTangent is not defined");static SVector3 a; return a;};

		virtual const SVector3& getConstRefToCohesiveReferenceBiTangent() const {Msg::Error("BulkFollowedCohesive3DIPVariable::getConstRefToCohesiveReferenceBiTangent is not defined");static SVector3 a; return a;};
		virtual SVector3& getRefToCohesiveReferenceBiTangent() {Msg::Error("BulkFollowedCohesive3DIPVariable::getRefToCohesiveReferenceBiTangent is not defined");static SVector3 a; return a;};

    // cohesive jump depend on other parameter
		virtual const STensor3& getConstRefToDCohesiveJumpDJump() const {Msg::Error("BulkFollowedCohesive3DIPVariable::getConstRefToDCohesiveJumpDJump is not defined");static STensor3 a; return a;};
    virtual STensor3& getRefToDCohesiveJumpDJump() {Msg::Error("BulkFollowedCohesive3DIPVariable::getRefToDCohesiveJumpDJump is not defined");static STensor3 a; return a;};

    virtual const STensor33& getConstRefToDCohesiveJumpDDeformationGradient() const {Msg::Error("BulkFollowedCohesive3DIPVariable::getConstRefToDCohesiveJumpDDeformationGradient is not defined");static STensor33 a; return a;};
    virtual STensor33& getRefToDCohesiveJumpDDeformationGradient() {Msg::Error("BulkFollowedCohesive3DIPVariable::getRefToDCohesiveJumpDDeformationGradient is not defined");static STensor33 a; return a;};

    virtual const STensor3& getConstRefToDCohesiveJumpDIncompatibleJump() const {Msg::Error("BulkFollowedCohesive3DIPVariable::getConstRefToDCohesiveJumpDIncompatibleJump is not defined");static STensor3 a; return a;};
    virtual STensor3& getRefToDCohesiveJumpDIncompatibleJump() {Msg::Error("BulkFollowedCohesive3DIPVariable::getRefToDCohesiveJumpDIncompatibleJump is not defined");static STensor3 a; return a;};
    
    virtual const double& getConstRefToOpeningOffset() const {Msg::Error("BulkFollowedCohesive3DIPVariable::getConstRefToOpeningOffset is not defined");static double a(0.); return a;};
    virtual double& getRefToOpeningOffset() {Msg::Error("BulkFollowedCohesive3DIPVariable::getRefToOpeningOffset is not defined"); static double a(0.); return a;};

    // when working with triaxiality effective
    // the deformation gradient is defined at interface from bulk deformation
    // gradient and from displacement jump
    virtual const STensor33& getConstRefToDInterfaceDeformationGradientDJump() const {return _dFinterfacedJump;};
    virtual STensor33& getRefToDInterfaceDeformationGradientDJump() {return _dFinterfacedJump;};

    virtual const STensor43& getConstRefToDInterfaceDeformationGradientDDeformationGradient() const {return _dFinterfacedF;};
    virtual STensor43& getRefToDInterfaceDeformationGradientDDeformationGradient() {return _dFinterfacedF;};

    virtual const STensor33& getConstRefToDInterfaceDeformationGradientDIncompatibleJump() const {Msg::Error("BulkFollowedCohesive3DIPVariable::getConstRefToDInterfaceDeformationGradientDIncompatibleJump is not defined");static STensor33 a; return a;};
    virtual STensor33& getRefToDInterfaceDeformationGradientDIncompatibleJump() {Msg::Error("BulkFollowedCohesive3DIPVariable::getRefToDInterfaceDeformationGradientDIncompatibleJump is not defined");static STensor33 a; return a;};
		
    virtual double defoEnergy() const {return 0.;};
    virtual double plasticEnergy() const {return 0.;};
    virtual double damageEnergy() const {return 0.;};
		//
		virtual double irreversibleEnergy() const {return _irreversibleEnergy;};
		virtual double& getRefToIrreversibleEnergy()  {return _irreversibleEnergy;};
		
		virtual SVector3& getRefToDIrreversibleEnergyDJump() {return _DirreversibleEnergyDjump;};
		virtual const SVector3& getConstRefToDIrreversibleEnergyDJump() const {return _DirreversibleEnergyDjump;};
    
    virtual const STensor3& getConstRefToDIrreversibleEnergyDJumpGradient() const {Msg::Error("Cohesive3DIPVariable::getConstRefToDIrreversibleEnergyDJumpGradient is not defined");static STensor3 a; return a;};
    virtual STensor3& getRefToDIrreversibleEnergyDJumpGradient() {Msg::Error("Cohesive3DIPVariable::getRefToDIrreversibleEnergyDJumpGradient is not defined");static STensor3 a; return a;};
		
		virtual const STensor3& getConstRefToDIrreversibleEnergyDDeformationGradient() const {return _DirreversibleEnergyDF;};
		virtual STensor3& getRefToDIrreversibleEnergyDDeformationGradient() {return _DirreversibleEnergyDF;};
		
		virtual const SVector3& getConstRefToDIrreversibleEnergyDIncompatibleJump() const {Msg::Error("BulkFollowedCohesive3DIPVariable::getConstRefToDIrreversibleEnergyDIncompatibleJump is not defined");static SVector3 a; return a;};
    virtual SVector3& getRefToDIrreversibleEnergyDIncompatibleJump()  {Msg::Error("BulkFollowedCohesive3DIPVariable::getRefToDIrreversibleEnergyDIncompatibleJump is not defined");static SVector3 a; return a;};

    // true if local basis is computed from domain
		virtual bool useLocalBasis() const {return false;};

		// for tension or compression state
		virtual bool ifTension() const {return _isTension;};
		virtual void setTensionFlag(const bool tensionflag) {_isTension = tensionflag;};

    virtual int fractureEnergy(double* arrayEnergy) const {return 0;};
   
	    // initial parameters of cohesive ip
    virtual void initializeFracture(const SVector3 &ujump_,  const double Kp_,
                            const double seff, const double snor, const double tau, const double Gc,
                            const double beta_, const bool ift, const STensor3 & cauchy,
                            const double K, // penalty
                            const SVector3& normal,  // normal
                            const SVector3& f0,  // interface force at insertion time
                            const IPVariable* bulkIPv // bulk data at insertion tim
                            ) ;

    virtual IPVariable* clone() const {return new BulkFollowedCohesive3DIPVariable(*this);};
    virtual void restart();


    #if defined(HAVE_MPI)
    virtual int getMacroNumberElementDataSendToMicroProblem() const;
    // get number of values obtained by microscopic analysis to send to macroscopic analysis
    virtual int getMicroNumberElementDataSendToMacroProblem() const;
    // get macroscopic kinematic data to send to microscopic problem
    virtual void getMacroDataSendToMicroProblem(double* val) const;
    // get computed data obtaind by microscopic analysis to send to macroscopic analysis
    virtual void getMicroDataToMacroProblem(double* val) const;
    // set the received data from microscopic analysis to microscopic analysis
    virtual void setReceivedMacroDataToMicroProblem(const double* val);
    // set the received data from microscopic analysis to macroscopic analysis
    virtual void setReceivedMicroDataToMacroProblem(const double* val);
    #endif

};

class TwoFieldBulkFollowedCohesive3DIPVariable : public BulkFollowedCohesive3DIPVariable{
protected:
    STensor3 _DInterfaceForceDInJump;
    STensor33 _DcohesiveJumpDF;
    STensor3 _DcohesiveJumpDJump;
    STensor3 _DcohesiveJumpDInJump;

    SVector3 _incompatibleJump0;
		SVector3 _DirreversibleEnergyDInJump;
		
		STensor33 _dFinterfacedInJump;
		
  public:
    TwoFieldBulkFollowedCohesive3DIPVariable(): BulkFollowedCohesive3DIPVariable(),
    _DInterfaceForceDInJump(0.),
		_DcohesiveJumpDF(0.),_DcohesiveJumpDJump(0.),_DcohesiveJumpDInJump(0.),
		_incompatibleJump0(0.), _DirreversibleEnergyDInJump(0.),
		_dFinterfacedInJump(0.){};
		
    TwoFieldBulkFollowedCohesive3DIPVariable(const TwoFieldBulkFollowedCohesive3DIPVariable& src): BulkFollowedCohesive3DIPVariable(src),
    _DInterfaceForceDInJump(src._DInterfaceForceDInJump),
		_DcohesiveJumpDF(src._DcohesiveJumpDF),_DcohesiveJumpDJump(src._DcohesiveJumpDJump),
		_DcohesiveJumpDInJump(src._DcohesiveJumpDInJump),
		_incompatibleJump0(src._incompatibleJump0), 
		_DirreversibleEnergyDInJump(src._DirreversibleEnergyDInJump),
		_dFinterfacedInJump(src._dFinterfacedInJump){};
    virtual TwoFieldBulkFollowedCohesive3DIPVariable& operator =(const IPVariable& src){
      BulkFollowedCohesive3DIPVariable::operator=(src);
      const TwoFieldBulkFollowedCohesive3DIPVariable* psrc = dynamic_cast<const TwoFieldBulkFollowedCohesive3DIPVariable*>(&src);
      if (psrc){
        _DInterfaceForceDInJump = psrc->_DInterfaceForceDInJump;
        _DcohesiveJumpDF = psrc->_DcohesiveJumpDF;
				_DcohesiveJumpDJump = psrc->_DcohesiveJumpDJump;
				_DcohesiveJumpDInJump = psrc->_DcohesiveJumpDInJump;
				_incompatibleJump0 = psrc->_incompatibleJump0;
				_DirreversibleEnergyDInJump  = psrc->_DirreversibleEnergyDInJump;
				_dFinterfacedInJump = psrc->_dFinterfacedInJump;
      }
      return *this;
    };
    virtual ~TwoFieldBulkFollowedCohesive3DIPVariable(){}
    
    virtual bool withIncompatibleJump() const {return true;};
    
    // //interface depends on incompatible jump
		virtual const STensor3& getConstRefToDInterfaceForceDIncompatibleJump() const {return _DInterfaceForceDInJump;};
		virtual STensor3& getRefToDInterfaceForceDIncompatibleJump() {return _DInterfaceForceDInJump;};

    virtual const STensor33& getConstRefToDCohesiveJumpDDeformationGradient() const {return _DcohesiveJumpDF;};
    virtual STensor33& getRefToDCohesiveJumpDDeformationGradient() {return _DcohesiveJumpDF;};

    virtual const STensor3& getConstRefToDCohesiveJumpDJump() const  {return _DcohesiveJumpDJump;};
    virtual STensor3& getRefToDCohesiveJumpDJump() {return _DcohesiveJumpDJump;};

    virtual const STensor3& getConstRefToDCohesiveJumpDIncompatibleJump() const {return _DcohesiveJumpDInJump;};
    virtual STensor3& getRefToDCohesiveJumpDIncompatibleJump() {return _DcohesiveJumpDInJump;};

		
		virtual const SVector3& getConstRefToIncompatibleJumpAtFailureOnset() const {return _incompatibleJump0;};
    virtual SVector3& getRefToIncompatibleJumpAtFailureOnset() {return _incompatibleJump0;};


		virtual const STensor33& getConstRefToDInterfaceDeformationGradientDIncompatibleJump() const {return _dFinterfacedInJump;};;
    virtual STensor33& getRefToDInterfaceDeformationGradientDIncompatibleJump() {return _dFinterfacedInJump;};

		virtual const SVector3& getConstRefToDIrreversibleEnergyDIncompatibleJump() const {return _DirreversibleEnergyDInJump;};
		virtual SVector3& getRefToDIrreversibleEnergyDIncompatibleJump() {return _DirreversibleEnergyDInJump;};

	    // initial parameters of cohesive ip
    virtual void initializeFracture(const SVector3 &ujump_,  const double Kp_,
                            const double seff, const double snor, const double tau, const double Gc,
                            const double beta_, const bool ift, const STensor3 & cauchy,
                            const double K, // penalty
                            const SVector3& normal,  // normal
                            const SVector3& f0,  // interface force at insertion time
                            const IPVariable* bulkIPv // bulk data at insertion tim
                            ) ;

    virtual IPVariable* clone() const {return new TwoFieldBulkFollowedCohesive3DIPVariable(*this);};
    virtual void restart();


    #if defined(HAVE_MPI)
    virtual int getMacroNumberElementDataSendToMicroProblem() const;
    // get number of values obtained by microscopic analysis to send to macroscopic analysis
    virtual int getMicroNumberElementDataSendToMacroProblem() const;
    // get macroscopic kinematic data to send to microscopic problem
    virtual void getMacroDataSendToMicroProblem(double* val) const;
    // get computed data obtaind by microscopic analysis to send to macroscopic analysis
    virtual void getMicroDataToMacroProblem(double* val) const;
    // set the received data from microscopic analysis to microscopic analysis
    virtual void setReceivedMacroDataToMicroProblem(const double* val);
    // set the received data from microscopic analysis to macroscopic analysis
    virtual void setReceivedMicroDataToMacroProblem(const double* val);
    #endif

};



#endif //DG3DCOHESIVEIPVARIABLETRIAXIALITY_H_


