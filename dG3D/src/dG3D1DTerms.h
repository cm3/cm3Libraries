//
// C++ Interface: 1D terms
//
// Description: Class of terms for dg
//
//
// Author:  <V.D Nguyen>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef DG3D1DTERMS_H
#define DG3D1DTERMS_H
#include "nlTerms.h"
#include "ipField.h"
#include "partDomain.h"
#include "dG3DTerms.h"
#include "scalarFunction.h"


class dG3D1DForceBulk : public nonLinearTermBase<double>{
  protected:
    const partDomain* _dom;
    const IPField* _ipf;
    double _nonLocalEqRatio;
    int _numNonLocalVariable; //we will need when adding extradof

    const scalarFunction* _crossSection;
    mutable const dG3DIPVariableBase* vipv[256]; // max 256 Gauss' point ??
    
		
  public:
    virtual void setNonLocalEqRatio(double eq) {_nonLocalEqRatio=eq;}
    virtual double getNonLocalEqRatio() const {return _nonLocalEqRatio;}
    virtual void setNumNonLocalVariable(double num) {_numNonLocalVariable=num;}
    virtual double getNumNonLocalVariable() const {return _numNonLocalVariable;}

    dG3D1DForceBulk(const partDomain* d, const IPField *ip, const scalarFunction* fct) : _dom(d),_ipf(ip),_crossSection(fct), 
                       _nonLocalEqRatio(1.), _numNonLocalVariable(0) {}
    virtual ~dG3D1DForceBulk(){}
    virtual void get(MElement *ele,int npts,IntPt *GP,fullVector<double> &m) const;
    virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullVector<double> > &mv) const
    {
      Msg::Error("Define me get by integration point dG3D1DForceBulk");
    }
    virtual void set(const fullVector<double> *datafield) {};
    virtual const bool isData() const {return false;};
		
    virtual LinearTermBase<double>* clone () const
    {
      dG3D1DForceBulk*DG=new dG3D1DForceBulk(_dom,_ipf,_crossSection);
      DG->setNonLocalEqRatio(this->getNonLocalEqRatio());
      DG->setNumNonLocalVariable(this->getNumNonLocalVariable());
      return DG;
    }
};

class dG3D1DStiffnessBulk : public BiNonLinearTermBase {
  protected:
    const partDomain* _dom;
    const IPField* _ipf;
    const scalarFunction* _crossSection;
    double _nonLocalEqRatio;
    int _numNonLocalVariable; //we will need when adding extradof
    
    mutable const dG3DIPVariableBase* vipv[256]; // max 256 Gauss' point ??
		
  public:
    dG3D1DStiffnessBulk(const partDomain* d, const IPField *ip, const scalarFunction* fct) : _dom(d),_ipf(ip),_crossSection(fct), 
                                 _nonLocalEqRatio(1.), _numNonLocalVariable(0) {}
    virtual void setNonLocalEqRatio(double eq) {_nonLocalEqRatio=eq;}
    virtual double getNonLocalEqRatio() const {return _nonLocalEqRatio;}
    virtual void setNumNonLocalVariable(double num) {_numNonLocalVariable=num;}
    virtual double getNumNonLocalVariable() const {return _numNonLocalVariable;}
    virtual ~dG3D1DStiffnessBulk(){}									
    virtual void get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const;
    
    virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const
    {
      Msg::Error("Define me get by gauss point dG3D1DStiffnessBulk");
    }
    virtual void set(const fullVector<double> *datafield) {};
    virtual const bool isData() const {return false;}; 
		
    virtual BilinearTermBase* clone () const
    {
      dG3D1DStiffnessBulk *DG= new dG3D1DStiffnessBulk(_dom,_ipf,_crossSection);
      DG->setNonLocalEqRatio(this->getNonLocalEqRatio());
      DG->setNumNonLocalVariable(this->getNumNonLocalVariable());
      return DG;
    }
};

#endif //DG3D1DTERMS_H
