//
// C++ Interface: loading with material law
//
//
//
// Author:  <Van Dung NGUYEN>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef COMPUTEWITHMATERIALLAW_H_
#define COMPUTEWITHMATERIALLAW_H_

#include "dG3DMaterialLaw.h"
class computeMaterialLaw
{ 
  public:
    enum StateType{Previous=0,Current=1};
    enum ValType{Val=0,Rate=1,Increment=2};
    
  protected:
    #ifndef SWIG
    dG3DMaterialLaw* _materialLaw;
    IPStateBase* _ips;
    int _step;
    #endif //SWIG
  public:
    computeMaterialLaw(dG3DMaterialLaw& mlaw);
    virtual ~computeMaterialLaw();
    void resetState(dG3DMaterialLaw& mlaw);
    void setDeformationGradient(int i, int, double val);
    void setTime(double t, double dt);
    void nextStep();
    void computeStressState(bool stiff=false);
    void computeStressState_uniaxialStress(bool stiff = false);
    void computeStressState_PlaneStrainUniaxialStress( char* NoZeroPlane, bool stiff = false);
    std::string getString(int comp, StateType state, ValType vtype) const;
    double getValue(int comp, StateType state, ValType vtype) const;
    double getTime() const;
    double getTimeStep() const;
    void getStress(std::vector<double>& Pstress);
    void getDeformationGradient(std::vector<double>& Fstrain);
    void getParameterDerivative(std::vector< fullMatrix<double> >& dPdPn, std::vector< fullMatrix<double> >&dPdPv) const;
    void reset_Parameter(dG3DMaterialLaw& mlaw, std::vector<std::vector<double>>& Para_Norm, std::vector<std::vector<double>>& Para_Wt, const double Vf=0.0);
    void reset_Parameter(dG3DMaterialLaw& mlaw, const char* Para);
};


#endif // COMPUTEWITHMATERIALLAW_H_
