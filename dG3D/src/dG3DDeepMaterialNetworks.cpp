//
// C++ Interface: deep material network
//
//
// Author:  <V.-D. Nguyen>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "dG3DDeepMaterialNetworks.h"
#include "dG3DMaterialLaw.h"
#include "nonLinearMechSolver.h"
#include "FractureCohesiveDG3DIPVariable.h"
#include "FractureCohesiveDG3DMaterialLaw.h"
#include "MInterfacePoint.h"

void dG3DVoidCohesiveIPState::reset() 
{
  for (int i=0; i< _allJumps.size(); i++)
  {
    STensorOperation::zero(_allJumps[i]);
  }
};
SVector3& dG3DVoidCohesiveIPState::getRefToJump(int index) {return _allJumps[index];};
const SVector3& dG3DVoidCohesiveIPState::getConstRefToJump(int index) const {return _allJumps[index];};
const SVector3& dG3DVoidCohesiveIPState::getConstRefToInterfaceForce(int index) const
{
  static  SVector3 force(0.);
  return force;
}
const STensor3& dG3DVoidCohesiveIPState::getConstRefToDInterfaceForceDJump(int index) const
{
  static STensor3 Q(0.);
  return Q;
};

void dG3DVoidCohesiveIPState::nextStep() 
{
  _allJumpsPrev=_allJumps;
  
};
void dG3DVoidCohesiveIPState::resetToPreviousStep() 
{
  _allJumps = _allJumpsPrev;
}
void dG3DVoidCohesiveIPState::copy(const IPStateBase::whichState ws1, const IPStateBase::whichState ws2)
{
  auto getState  = [&](const IPStateBase::whichState ws)
  {
    if (ws == IPStateBase::current)
    {
      return _allJumps;
    }
    else if (ws == IPStateBase::previous)
    {
      return _allJumpsPrev;
    }
    else
    {
      return _allJumpsInit;
    }
  };
  getState(ws2) = getState(ws1);
};

dG3DCohesiveIPState::dG3DCohesiveIPState(): cohesiveIPState(), _allState()
{

}
dG3DCohesiveIPState::dG3DCohesiveIPState(const dG3DCohesiveIPState& src): cohesiveIPState(src)
{
  _allState.clear();
  if (src._allState.size() > 0)
  {
    Msg::Error("cannot copy material node when ips is set MaterialNode::MaterialNode(MaterialNode)");
    Msg::Exit(0);
  }
}

dG3DCohesiveIPState::~dG3DCohesiveIPState()
{
  for (int i=0; i<_allState.size(); i++)
  {
    delete _allState[i];
  }
};
IPStateBase* dG3DCohesiveIPState::getStateWithIndex(int index)
{
  if (index > _allState.size()-1)
  {
    Msg::Error("index %d is exceeded, size %d!!",index,_allState.size());
    Msg::Exit(0);
  }
  return _allState[index];
}
const IPStateBase* dG3DCohesiveIPState::getStateWithIndex(int index) const
{
  if (index > _allState.size()-1)
  {
    Msg::Error("index %d is exceeded, size %d!!",index,_allState.size());
    Msg::Exit(0);
  }
  return _allState[index];
}
void dG3DCohesiveIPState::reset() 
{
  copy(IPStateBase::initial,IPStateBase::current);
  copy(IPStateBase::initial,IPStateBase::previous);
}
SVector3& dG3DCohesiveIPState::getRefToJump(int index)
{
  IPStateBase* ips = getStateWithIndex(index);
  IPVariable* ipv = ips->getState(IPStateBase::current);
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if (ipvtmp == NULL)
  {
    Msg::Error("FractureCohesive3DIPVariable must be used");
    Msg::Exit(0);
  }
  return ipvtmp->getRefToJump();
}
const SVector3& dG3DCohesiveIPState::getConstRefToJump(int index) const
{
  const IPStateBase* ips = getStateWithIndex(index);
  const IPVariable* ipv = ips->getState(IPStateBase::current);
  const FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<const FractureCohesive3DIPVariable*>(ipv);
  if (ipvtmp == NULL)
  {
    Msg::Error("FractureCohesive3DIPVariable must be used");
    Msg::Exit(0);
  }
  return ipvtmp->getConstRefToJump();
}
const SVector3& dG3DCohesiveIPState::getConstRefToInterfaceForce(int index) const
{
  const IPStateBase* ips = getStateWithIndex(index);
  const IPVariable* ipv = ips->getState(IPStateBase::current);
  const FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<const FractureCohesive3DIPVariable*>(ipv);
  if (ipvtmp == NULL)
  {
    Msg::Error("FractureCohesive3DIPVariable must be used");
    Msg::Exit(0);
  }
  return ipvtmp->getConstRefToInterfaceForce();
}
const STensor3& dG3DCohesiveIPState::getConstRefToDInterfaceForceDJump(int index) const
{
  const IPStateBase* ips = getStateWithIndex(index);
  const IPVariable* ipv = ips->getState(IPStateBase::current);
  const FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<const FractureCohesive3DIPVariable*>(ipv);
  if (ipvtmp == NULL)
  {
    Msg::Error("FractureCohesive3DIPVariable must be used");
    Msg::Exit(0);
  }
  return ipvtmp->getConstRefToDInterfaceForceDjump();
}

void dG3DCohesiveIPState::nextStep()
{
  copy(IPStateBase::current,IPStateBase::previous);
}
void dG3DCohesiveIPState::resetToPreviousStep()
{
  copy(IPStateBase::previous,IPStateBase::current);
}
void dG3DCohesiveIPState::copy(const IPStateBase::whichState ws1, const IPStateBase::whichState ws2)
{
  for (int i=0; i< _allState.size(); i++)
  {
    IPVariable* ipv1 = _allState[i]->getState(ws1);
    IPVariable* ipv2 = _allState[i]->getState(ws2);
    ipv2->operator =(*ipv1);
  };
};


dG3DDeepMaterialNetwork::dG3DDeepMaterialNetwork(): DeepMaterialNetwork(), _state(true){}

dG3DDeepMaterialNetwork::dG3DDeepMaterialNetwork(const Tree& T):DeepMaterialNetwork(T), _state(true){}
dG3DDeepMaterialNetwork::~dG3DDeepMaterialNetwork()
{
  
}

dG3DDeepMaterialNetwork::dG3DDeepMaterialNetwork(const std::string fname):DeepMaterialNetwork(fname), _state(true){}

void dG3DDeepMaterialNetwork::addLaw(int matIndex, materialLaw& l1)
{
  _mlaw[matIndex] = &l1;
};

void dG3DDeepMaterialNetwork::setTime(double tcur,double timeStep)
{
  for (std::map<int,materialLaw*>::iterator it = _mlaw.begin(); it != _mlaw.end(); it++)
  {
    it->second->setTime(tcur,timeStep);
  }
};

void dG3DDeepMaterialNetwork::setMacroSolver(const nonLinearMechSolver* sv)
{
  for (std::map<int,materialLaw*>::iterator it = _mlaw.begin(); it != _mlaw.end(); it++)
  {
    it->second->setMacroSolver(sv);
  }
};

void dG3DDeepMaterialNetwork::createVoidIPState(IPStateBase*& ips, const bool* state_) const
{
  bool inter=false;
  IPVariable* ipvi = new  dG3DIPVariableVoid(false,inter);
  IPVariable* ipv1 = new  dG3DIPVariableVoid(false,inter);
  IPVariable* ipv2 = new  dG3DIPVariableVoid(false,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
};


void dG3DDeepMaterialNetwork::createIPState(MaterialNode* node)
{
  bool hasBodyForce=false;
  int matIndex = node->matIndex;  
  // ipvariables at leaves only
  MVertex v(0.,0.,0.);
  MPoint pt(&v);
  // create ipvs and initialize
  if (_mlaw.find(matIndex) == _mlaw.end())
  {
    //Msg::Info("material noed with index %d is considered as void",matIndex);
    createVoidIPState(node->ips, &_state);
  }
  else
  {
    if (_mlaw[matIndex]->getMacroSolver() == NULL)
    {
      nonLinearMechSolver* s = new nonLinearMechSolver(1000);
      _mlaw[matIndex]->setMacroSolver(s);
    }
    // create ipvs and initialize
    
    _mlaw[matIndex]->createIPState(node->ips, hasBodyForce, &_state,&pt,1,NULL,0);
    std::vector<IPVariable*> allIPv;
    node->ips->getAllIPVariable(allIPv);
    dG3DMaterialLaw* bulkLaw  = dynamic_cast<dG3DMaterialLaw*>(_mlaw[matIndex]);
    for (int i=0; i<allIPv.size(); i++)
    {
      bulkLaw->initialIPVariable(allIPv[i],true);
    }
  
    if (node->withCohesive())
    {
      // incase of cohesive
      int cohesiveMatIndex = node->cohesiveMatIndex;
      if (_mlaw.find(cohesiveMatIndex) == _mlaw.end() )
      {
        Msg::Warning("cohesive material node with index %d is considered as void",cohesiveMatIndex);
        int nCoh = node->cohesiveCoeffVars.size();
        node->ipsCohesive = new dG3DVoidCohesiveIPState(nCoh);
      }
      else
      {
        FractureByCohesive3DLaw* fracLaw = NULL;
        std::map<std::pair<int,int>, materialLaw*>::iterator itFind = _fracLaw.find(std::pair<int,int>(matIndex,cohesiveMatIndex));
        if (itFind == _fracLaw.end())
        {
          fracLaw = new FractureByCohesive3DLaw(-matIndex,matIndex,cohesiveMatIndex);
          fracLaw->initLaws(_mlaw);
          _fracLaw[std::pair<int,int>(matIndex,cohesiveMatIndex)] = fracLaw;
        }
        else
        {
          fracLaw = dynamic_cast<FractureByCohesive3DLaw*>(itFind->second);
        }
        
        int nCoh = node->cohesiveCoeffVars.size();
        node->ipsCohesive = new dG3DCohesiveIPState();
        std::vector<IPStateBase*>& allState = dynamic_cast<dG3DCohesiveIPState*>(node->ipsCohesive)->getRefToAllState();
        allState.resize(nCoh,NULL);
        MInterfacePoint mpt(&v);
        std::vector<SVector3> allNormals;
        node->getNormal(allNormals);
        for (int i=0; i< nCoh; i++)
        {
          fracLaw->createIPState(allState[i],hasBodyForce,&_state,&mpt,1,NULL,0);
          allIPv.clear();
          allState[i]->getAllIPVariable(allIPv);
          for (int j=0; j<allIPv.size(); j++)
          {
            // init bulk
            fracLaw->initialIPVariable(allIPv[j],true);
            // init interface
            FractureCohesive3DIPVariable* ipvfrac= dynamic_cast<FractureCohesive3DIPVariable*>(allIPv[j]);
            ipvfrac->getRefToReferenceOutwardNormal() = allNormals[i];
            ipvfrac->getRefToCurrentOutwardNormal() = allNormals[i];
          }
          // start broken
          fracLaw->initialBroken(allState[i]);
        }
      }
    }
  }
};
void dG3DDeepMaterialNetwork::stressLocal(MaterialNode* node)
{
  int matIndex = node->matIndex;
  if (_mlaw.find(matIndex) == _mlaw.end())
  {
    //Msg::Warning("material noed with index %d is considered as void",matIndex);
  }
  else
  {
    IPVariable* ipv = node->ips->getState(IPStateBase::current);
    const ipFiniteStrain* ipf = dynamic_cast<const ipFiniteStrain*>(ipv);
    /*
    if (ipf->getJ() < 1e-8)
    {
      Msg::Error("negative Jacobian");
      const STensor3& F = ipf->getConstRefToDeformationGradient();
      F.print("deformation gradient");
    }
    */
    
    const IPVariable* ipvprev = node->ips->getState(IPStateBase::previous);
    dG3DMaterialLaw* bulkLaw = dynamic_cast<dG3DMaterialLaw*>(_mlaw[matIndex]);
    bulkLaw->stress(ipv,ipvprev,true);
    
    
    
    // evaluate cohesive behavior
    if (node->withCohesive())
    {
      int cohesiveMatIndex = node->cohesiveMatIndex;
      if (_mlaw.find(cohesiveMatIndex) == _mlaw.end() )
      {
        // do nothing
        
      }
      else
      {
        Cohesive3DLaw* cohLaw = dynamic_cast<Cohesive3DLaw*>(_mlaw[cohesiveMatIndex]);
        std::vector<IPStateBase*>& allState = dynamic_cast<dG3DCohesiveIPState*>(node->ipsCohesive)->getRefToAllState();
        std::vector<SVector3> allNormals;
        node->getNormal(allNormals);
        for (int i=0; i< allState.size(); i++)
        {
          IPVariable* ipvfrac = allState[i]->getState(IPStateBase::current);
          const IPVariable* ipvprevfrac = allState[i]->getState(IPStateBase::previous);
          // bulk is copy 
          FractureCohesive3DIPVariable* fipv = dynamic_cast<FractureCohesive3DIPVariable*>(ipvfrac);
          const FractureCohesive3DIPVariable* fipvprev = dynamic_cast<const FractureCohesive3DIPVariable*>(ipvprevfrac);
          IPVariable* ipvfracBulk = dynamic_cast<IPVariable*>(fipv->getIPvBulk());
          Cohesive3DIPVariable* ipcohesive = dynamic_cast<Cohesive3DIPVariable*>(fipv->getIPvFrac());
          const Cohesive3DIPVariable* ipcohesiveprev = dynamic_cast<const Cohesive3DIPVariable*>(fipvprev->getIPvFrac());
          // state is copied from bulk 
          // cohesive jump and normals must be kept
          /*
          static SVector3 jumpTemp;
          jumpTemp = fipv->getConstRefToJump();
          ipvfracBulk->operator =(*ipv);
          fipv->getRefToJump() = jumpTemp;
          fipv->getRefToReferenceOutwardNormal() = allNormals[i];
          fipv->getRefToCurrentOutwardNormal() = allNormals[i];
          */
          // interface
          if (0)
          {
            //Msg::Error("IPv is deleted, Damage = %e!!!",ipcohesiveprev->getIPCohesive()->getConstRefToCohesiveDamage());
            fipv->Delete(true);
            fipv->getRefToInterfaceForce() = fipvprev->getConstRefToInterfaceForce();
            const STensor3& Qref = fipvprev->getConstRefToDInterfaceForceDjump();
            const SVector3& jump = fipv->getConstRefToJump();
            //jump.print("jump");
            //Qref.print("Qref");
            //STensorOperation::zero(fipv->getRefToDInterfaceForceDjump());
            fipv->getRefToDInterfaceForceDjump() = Qref;
            STensorOperation::multSTensor3SVector3(Qref,jump,fipv->getRefToInterfaceForce());
          }
          else
          {
            double fractionNode = _networkInteraction->getWeight(node)/_networkInteraction->getTotalWeight();
            Msg::Error("nodeId = %d fraction=%.6f matIndex = %d!!!", node->getNodeId(), fractionNode, node->matIndex);
            cohLaw->stress(ipvfrac,ipvprevfrac,true);
            //fipv->getConstRefToReferenceOutwardNormal().print("normal");
            //fipv->getRefToInterfaceForce().print("interface force");
            
            /*if (!fipv->ifTension())
            {
              double deltaSq= STensorOperation::dot(fipv->getConstRefToJump(),fipv->getConstRefToJump());
              double deltn = STensorOperation::dot(fipv->getConstRefToJump(),fipv->getConstRefToReferenceOutwardNormal());
            //jump.print("jump");)
              Msg::Error("COMPRESSSION DETECTED, Damage = %e!!! deltn=%e deltat=%e deltac = %e Kp = %e",ipcohesive->getIPCohesive()->getConstRefToCohesiveDamage(),
                         deltn,deltaSq-deltn*deltn,
                         ipcohesive->getIPCohesive()->getCriticalEffectiveJump(), ipcohesive->getIPCohesive()->getCompressivePenality());
              //const STensor3& Qref = fipvprev->getConstRefToDInterfaceForceDjump();
              //Qref.print("Qref not deleted");
            }
            */
            if (ipcohesive->getIPCohesive()->getConstRefToCohesiveDamage()> 0)
            {
             
              
              Msg::Error("damage active nodeId = %d fraction=%.6f matIndex = %d, Damage = %e!!!", node->getNodeId(), fractionNode, node->matIndex, 
                              ipcohesive->getIPCohesive()->getConstRefToCohesiveDamage());
            }
          }
        }
      }
    }
  }
};

void dG3DDeepMaterialNetwork::getDIPCompDFLocal(const IPStateBase* ips, IPField::Output comp, double& val, STensor3& DvalDF)
{
  if (comp == IPField::JACOBIAN)
  {
    const ipFiniteStrain* ipv = dynamic_cast<const ipFiniteStrain*>(ips->getState(IPStateBase::current));
    const STensor3& F = ipv->getConstRefToDeformationGradient();
    val = STensorOperation::determinantSTensor3(F);
    STensorOperation::inverseAndTransposeSTensor3(F,DvalDF);
    DvalDF *= val;
  }
  else if (comp == IPField::VOLUMETRIC_STRAIN)
  {
    const ipFiniteStrain* ipv = dynamic_cast<const ipFiniteStrain*>(ips->getState(IPStateBase::current));
    const STensor3& F = ipv->getConstRefToDeformationGradient();
    val = ipv->get(IPField::VOLUMETRIC_STRAIN);
    STensorOperation::unity(DvalDF);
  }
  else if (comp == IPField::PLASTIC_ENERGY)
  {
    const ipFiniteStrain* ipv = dynamic_cast<const ipFiniteStrain*>(ips->getState(IPStateBase::current));
    val = ipv->plasticEnergy();
    const IPVariable* ipvData =  ipv->getInternalData();
    const IPJ2linear* ipj2 = dynamic_cast<const IPJ2linear*>(ipvData);
    if (ipj2 != NULL)
    {
      DvalDF = ipj2->getConstRefToDPlasticEnergyDF();
    }
    else
    {
      Msg::Error("IPJ2linear must be used");
      Msg::Exit(0);
    }
    
  }
  else
  {
    Msg::Error("dG3DDeepMaterialNetwork::getDIPCompDFLocal is not defined for %s",IPField::ToString(comp));
  }
}
