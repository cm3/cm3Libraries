//
// Author: Van Dung NGUYEN, (C) 2016
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "dG3DMultiscaleIPVariable.h"
#include "nonLinearMechSolver.h"

void dG3DMultiscaleIPVariable::brokenSolver(const bool bl){
  dG3DIPVariable::brokenSolver(bl);
  _solverBroken = bl;
  if (this->getMicroSolver()!= NULL){
    this->getMicroSolver()->brokenSolver(_solverBroken);
  }
};

void dG3DMultiscaleIPVariable::blockDissipation(const bool bl){
	dG3DIPVariable::blockDissipation(bl);
	_dissipationBlocked = bl;
	if (this->getMicroSolver()!= NULL) {
		this->getMicroSolver()->blockDissipation(IPStateBase::current,_dissipationBlocked);
	}
}

double dG3DMultiscaleIPVariable::get(const int comp) const{
  if (comp == IPField::LOST_ELLIPTICITY){
    return _lostEllipticity;
  }
  else if (comp == IPField::FAILED){
    return _solverBroken;
  }
  else
    return dG3DIPVariable::get(comp);
};

#if defined(HAVE_MPI)
int dG3DMultiscaleIPVariable::getMacroNumberElementDataSendToMicroProblem() const{
	// defo + displacement jump and
  // ref interface normal
  // cur interface normal
  // incompatible strain
  return 9+3+3+3+3;
};
  // get number of values obtained by microscopic analysis to send to macroscopic analysis
int dG3DMultiscaleIPVariable::getMicroNumberElementDataSendToMacroProblem() const{
	// we send P+ L +_lostEllipticity Cr +elastic energy + plastic energy+
	// irreversible energy+DirreversibleEnergyDF
  return 9+81+1+1+1+1+9;

};
  // get macroscopic kinematic data to send to microscopic problem
void dG3DMultiscaleIPVariable::getMacroDataSendToMicroProblem(double* val) const{
  const STensor3& F = this->getConstRefToDeformationGradient();
	const SVector3& ujump = this->getConstRefToJump();
  const SVector3& refNorm = this->getConstRefToReferenceOutwardNormal();
  const SVector3& curNorm = this->getConstRefToCurrentOutwardNormal();
  const SVector3& fjump = this->getConstRefToIncompatibleJump();

	int row = 0;
	for (int index=row; index < row+9; index ++){
		int i,j;
		Tensor23::getIntsFromIndex(index-row,i,j);
		val[index] = F(i,j);
	}
	row+= 9;

	for (int index=row; index < row+3; index++){
		int i;
		Tensor13::getIntsFromIndex(index-row,i);
		val[index] = ujump(i);
	}
	row+= 3;

	for (int index=row; index < row+3; index++){
		int i;
		Tensor13::getIntsFromIndex(index-row,i);
		val[index] = refNorm(i);
	}
	row+= 3;

	for (int index=row; index < row+3; index++){
		int i;
		Tensor13::getIntsFromIndex(index-row,i);
		val[index] = curNorm(i);
	}
	row+= 3;

	for (int index=row; index < row+3; index++){
		int i;
		Tensor13::getIntsFromIndex(index-row,i);
		val[index] = fjump(i);
	}
	row+= 3;

};
  // get computed data obtaind by microscopic analysis to send to macroscopic analysis
void dG3DMultiscaleIPVariable::getMicroDataToMacroProblem(double* val) const{
	const STensor3& P = this->getConstRefToFirstPiolaKirchhoffStress();
  const STensor43& L = this->getConstRefToTangentModuli();
	const STensor3& dirrEnergdF = this->getConstRefToDIrreversibleEnergyDDeformationGradient();

	int row = 0;
	for (int index=row; index< row+9; index++){
		int i,j;
		Tensor23::getIntsFromIndex(index-row,i,j);
		val[index] = P(i,j);
	}
	row+= 9;

	for (int index=row; index< row+81; index++){
		int i,j,k,l;
		Tensor43::getIntsFromIndex(index-row,i,j,k,l);
		val[index] = L(i,j,k,l);
	}
	row+= 81;

  val[row] = this->getConstRefToLostEllipticityCriterion();
  row+= 1;
	//
	val[row] = this->defoEnergy();
	row += 1;

	val[row] = this->plasticEnergy();
	row += 1;

	val[row] = this->irreversibleEnergy();
	row+=1;


	for (int index=row; index<row+9; index++){
		int i, j;
		Tensor23::getIntsFromIndex(index-row,i,j);
		val[index] = dirrEnergdF(i,j);
	}
	row+=9;

};
  // set the received data from microscopic analysis to microscopic analysis
void dG3DMultiscaleIPVariable::setReceivedMacroDataToMicroProblem(const double* val){
  STensor3& F = this->getRefToDeformationGradient();
	SVector3& ujump = this->getRefToJump();
  SVector3& refNorm = this->getRefToReferenceOutwardNormal();
  SVector3& curNorm = this->getRefToCurrentOutwardNormal();
  SVector3& fjump = this->getRefToIncompatibleJump();

	int row = 0;
	for (int index=row; index < row+9; index ++){
		int i,j;
		Tensor23::getIntsFromIndex(index-row,i,j);
		F(i,j) = val[index];
	}
	row+= 9;

	for (int index=row; index < row+3; index++){
		int i;
		Tensor13::getIntsFromIndex(index-row,i);
		ujump(i) = val[index];
	}
	row+= 3;

	for (int index=row; index < row+3; index++){
		int i;
		Tensor13::getIntsFromIndex(index-row,i);
		refNorm(i) = val[index];
	}
	row+= 3;

	for (int index=row; index < row+3; index++){
		int i;
		Tensor13::getIntsFromIndex(index-row,i);
		curNorm(i) = val[index];
	}
	row+= 3;

	for (int index=row; index < row+3; index++){
		int i;
		Tensor13::getIntsFromIndex(index-row,i);
		fjump(i) = val[index];
	}
	row+= 3;
};
  // set the received data from microscopic analysis to macroscopic analysis
void dG3DMultiscaleIPVariable::setReceivedMicroDataToMacroProblem(const double* val){
  STensor3& P = this->getRefToFirstPiolaKirchhoffStress();
  STensor43& L = this->getRefToTangentModuli();
	STensor3& dirrEnergdF = this->getRefToDIrreversibleEnergyDDeformationGradient();

	int row = 0;
	for (int index=row; index< row+9; index++){
		int i,j;
		Tensor23::getIntsFromIndex(index-row,i,j);
		P(i,j) = val[index];
	}
	row+= 9;

	for (int index=row; index< row+81; index++){
		int i,j,k,l;
		Tensor43::getIntsFromIndex(index-row,i,j,k,l);
		L(i,j,k,l) = val[index];
	}
	row+= 81;

  _lostEllipticity = val[row];
  row+= 1;

	_elasticEnergy = val[row];
	row += 1;

	_plasticEnergy = val[row];
	row += 1;

	_irreversibleEnergy = val[row];
	row += 1;

	for (int index=row; index<row+9; index++){
		int i,j;
		Tensor23::getIntsFromIndex(index-row,i,j);
		dirrEnergdF(i,j) = val[index];
	}
	row+=9;

};
#endif

double MultiscaleThermoMechanicsDG3DIPVariable::defoEnergy() const{
	double energ = 0.;
	if (getMicroSolver() != NULL){
		energ = getMicroSolver()->getHomogenizationState(IPStateBase::current)->getDeformationEnergy();
	}
	return energ;
};
double MultiscaleThermoMechanicsDG3DIPVariable::getInternalEnergyExtraDofDiffusion() const{
	double energ = 0.;
	if (getMicroSolver() != NULL){
		energ = getMicroSolver()->getHomogenizationState(IPStateBase::current)->getHomogenizedConstitutiveExtraDofInternalEnergy(0);
	}
	return energ;
};
double MultiscaleThermoMechanicsDG3DIPVariable::plasticEnergy() const{
	double energ = 0.;
	if (getMicroSolver() != NULL){
		energ = getMicroSolver()->getHomogenizationState(IPStateBase::current)->getPlasticEnergy();
	}
	return energ;
};

#if defined(HAVE_MPI)

// using in multiscale analysis with MPI
// pass deformationGrad + extraFieldGrad+extraFieldValue
int MultiscaleThermoMechanicsDG3DIPVariable::getMacroNumberElementDataSendToMicroProblem() const {
  return 9+3+1;
};
// pass stress(9), tangentF(81), tangentFFlux(27), tangantFExtraDof(9)
// extraDofFlux(3), tangentFuxF(27), tangentFluxGradExtraDof(9), tangenetFluxExtraDof(3)
// cp (1)
// fieldSource (1), DfieldSourDF(9), DfieldSourceDGradExtraDof(3), DfieldSourceDExtraDof(1)
// mechanicalSource(1), DmechanicalSourceDF(9), DmechanicalSourceDGradExtraDof(3), DmechanicalSourceDExtraDof(1)
int MultiscaleThermoMechanicsDG3DIPVariable::getMicroNumberElementDataSendToMacroProblem() const {
  return 9+81+27+9+ 3+27+9+3 +1+ 1+9+3+1+ 1+9+3+1;};
void MultiscaleThermoMechanicsDG3DIPVariable::getMacroDataSendToMicroProblem(double* val) const {
  const STensor3& F = this->getConstRefToDeformationGradient();
  int row = 0;
  for (int idex = 0; idex < 9; idex++){
    int i,j;
    Tensor23::getIntsFromIndex(idex,i,j);
    val[row] = F(i,j);
    row++;
  }
  const SVector3& gradT = this->getConstRefToGradField()[0];
  for (int idex = 0; idex < 3; idex++){
    int i;
    Tensor13::getIntsFromIndex(idex,i);
    val[row] = gradT(i);
    row++;
  }
  const double& T = this->getConstRefToField(0);
  for (int idex = 0; idex< 1; idex++){
    val[row] = T;
    row++;
  }
};
void MultiscaleThermoMechanicsDG3DIPVariable::getMicroDataToMacroProblem(double* val) const {
  // fill all mechanical part
  const STensor3& P = this->getConstRefToFirstPiolaKirchhoffStress();
  const STensor43& L = this->getConstRefToTangentModuli();
  const STensor33& L_FgradField = this->getConstRefTodPdGradField()[0];
  const STensor3& L_FField = this->getConstRefTodPdField()[0];

  int row = 0;
  for (int idex = 0; idex< 9; idex++){
    int i,j;
    Tensor23::getIntsFromIndex(idex,i,j);
    val[row] = P(i,j);
    row++;
  }
  for (int idex = 0; idex <81; idex++){
    int i,j,k,l;
    Tensor43::getIntsFromIndex(idex,i,j,k,l);
    val[row] = L(i,j,k,l);
    row++;
  };
  for (int idex = 0; idex < 27; idex++){
    int i,j,k;
    Tensor33::getIntsFromIndex(idex,i,j,k);
    val[row] = L_FgradField(i,j,k);
    row++;
  }
  for (int idex=0; idex<9; idex++){
    int i,j;
    Tensor23::getIntsFromIndex(idex,i,j);
    val[row] = L_FField(i,j);
    row++;
  }
  // fill all extraDofFLux part
  const SVector3& fluxT = this->getConstRefToFlux()[0];
  const STensor33& L_fluxF = this->getConstRefTodFluxdF()[0];
  const STensor3& L_fluxGradField = this->getConstRefTodFluxdGradField()[0][0];
  const SVector3& L_fluxField = this->getConstRefTodFluxdField()[0][0];
  for (int idex=0; idex< 3; idex++){
    int i;
    Tensor13::getIntsFromIndex(idex,i);
    val[row] = fluxT(i);
    row++;
  }
  for (int idex=0; idex< 27; idex++){
    int i,j,k;
    Tensor33::getIntsFromIndex(idex,i,j,k);
    val[row] = L_fluxF(i,j,k);
    row++;
  }
  for (int idex=0; idex<9; idex++){
    int i,j;
    Tensor23::getIntsFromIndex(idex,i,j);
    val[row] = L_fluxGradField(i,j);
    row++;
  }
  for (int idex=0; idex<3; idex++){
    int i;
    Tensor13::getIntsFromIndex(idex,i);
    val[row] = L_fluxField(i);
    row++;
  }

  // send Cp
  const double& cp = this->getConstRefToExtraDofFieldCapacityPerUnitField()(0);
  for (int idex=0; idex <1; idex++){
    val[row] = cp;
    row++;
  }
  // send field source
  // sen mechanical source
  const double& fieldsource = this->getConstRefToFieldSource()(0);
  const STensor3& dfieldsourceDF = this->getConstRefTodFieldSourcedF()[0];
  const SVector3& dfieldsourceDgradField = this->getConstRefTodFieldSourcedGradField()[0][0];
  const double& dfieldsourcedField = this->getConstRefTodFieldSourcedField()(0,0);
  for (int idex=0; idex<1; idex++){
    val[row] = fieldsource;
    row++;
  }
  for (int idex=0; idex <9; idex++){
    int i,j;
    Tensor23::getIntsFromIndex(idex,i,j);
    val[row] = dfieldsourceDF(i,j);
    row++;
  }
  for (int idex=0; idex<3; idex++){
    int i;
    Tensor13::getIntsFromIndex(idex,i);
    val[row] = dfieldsourceDgradField(i);
    row++;
  }
  for (int idex=0; idex<1; idex++){
    val[row] = dfieldsourcedField;
    row++;
  }


  // send mechanical source
  const double& msource = this->getConstRefToMechanicalSource()(0);
  const STensor3& DmsourceDF = this->getConstRefTodMechanicalSourcedF()[0];
  const SVector3& dmsourceDgradField = this->getConstRefTodMechanicalSourcedGradField()[0][0];
  const double& dmsourcedField = this->getConstRefTodMechanicalSourcedField()(0,0);
  for (int idex=0; idex<1; idex++){
    val[row] = msource;
    row++;
  }
  for (int idex=0; idex <9; idex++){
    int i,j;
    Tensor23::getIntsFromIndex(idex,i,j);
    val[row] = DmsourceDF(i,j);
    row++;
  }
  for (int idex=0; idex<3; idex++){
    int i;
    Tensor13::getIntsFromIndex(idex,i);
    val[row] = dmsourceDgradField(i);
    row++;
  }
  for (int idex=0; idex<1; idex++){
    val[row] = dmsourcedField;
    row++;
  }
};
void MultiscaleThermoMechanicsDG3DIPVariable::setReceivedMacroDataToMicroProblem(const double* val){
  STensor3& F = this->getRefToDeformationGradient();
  int row = 0;
  for (int idex = 0; idex < 9; idex++){
    int i,j;
    Tensor23::getIntsFromIndex(idex,i,j);
     F(i,j) = val[row];
     row++;
  }
  SVector3& gradT = this->getRefToGradField()[0];
  for (int idex = 0; idex < 3; idex++){
    int i;
    Tensor13::getIntsFromIndex(idex,i);
    gradT(i) = val[row];
    row++;
  }
  double& T = this->getRefToField(0);
  for (int idex = 0; idex< 1; idex++){
    T = val[row];
    row++;
  }

};
void MultiscaleThermoMechanicsDG3DIPVariable::setReceivedMicroDataToMacroProblem(const double* val){
  // fill all mechanical part
  STensor3& P = this->getRefToFirstPiolaKirchhoffStress();
  STensor43& L = this->getRefToTangentModuli();
  STensor33& L_FgradField = this->getRefTodPdGradField()[0];
  STensor3& L_FField = this->getRefTodPdField()[0];

  int row = 0;
  for (int idex = 0; idex< 9; idex++){
    int i,j;
    Tensor23::getIntsFromIndex(idex,i,j);
    P(i,j) = val[row];
    row++;
  }
  for (int idex = 0; idex <81; idex++){
    int i,j,k,l;
    Tensor43::getIntsFromIndex(idex,i,j,k,l);
    L(i,j,k,l) = val[row];
    row++;
  };
  for (int idex = 0; idex < 27; idex++){
    int i,j,k;
    Tensor33::getIntsFromIndex(idex,i,j,k);
    L_FgradField(i,j,k)= val[row];
    row++;
  }
  for (int idex=0; idex<9; idex++){
    int i,j;
    Tensor23::getIntsFromIndex(idex,i,j);
    L_FField(i,j)= val[row];
    row++;
  }
  // fill all extraDofFLux part
  SVector3& fluxT = this->getRefToFlux()[0];
  STensor33& L_fluxF = this->getRefTodFluxdF()[0];
  STensor3& L_fluxGradField = this->getRefTodFluxdGradField()[0][0];
  SVector3& L_fluxField = this->getRefTodFluxdField()[0][0];
  for (int idex=0; idex< 3; idex++){
    int i;
    Tensor13::getIntsFromIndex(idex,i);
    fluxT(i) = val[row];
    row++;
  }
  for (int idex=0; idex< 27; idex++){
    int i,j,k;
    Tensor33::getIntsFromIndex(idex,i,j,k);
     L_fluxF(i,j,k) = val[row];
    row++;
  }
  for (int idex=0; idex<9; idex++){
    int i,j;
    Tensor23::getIntsFromIndex(idex,i,j);
    L_fluxGradField(i,j) = val[row];
    row++;
  }
  for (int idex=0; idex<3; idex++){
    int i;
    Tensor13::getIntsFromIndex(idex,i);
    L_fluxField(i) = val[row];
    row++;
  }

  // receive Cp
  double& cp = this->getRefToExtraDofFieldCapacityPerUnitField()(0);
  for (int idex=0; idex <1; idex++){
    cp = val[row];
    row++;
  }

  // receive field source
  double& fieldSource = this->getRefToFieldSource()(0);
  STensor3& dfieldSourceDF = this->getRefTodFieldSourcedF()[0];
  SVector3& dfieldSourceDgradField = this->getRefTodFieldSourcedGradField()[0][0];
  double& dfieldSourcedField = this->getRefTodFieldSourcedField()(0,0);
  for (int idex=0; idex<1; idex++){
    fieldSource = val[row];
    row++;
  }
  for (int idex=0; idex <9; idex++){
    int i,j;
    Tensor23::getIntsFromIndex(idex,i,j);
    dfieldSourceDF(i,j) = val[row];
    row++;
  }
  for (int idex=0; idex<3; idex++){
    int i;
    Tensor13::getIntsFromIndex(idex,i);
    dfieldSourceDgradField(i) = val[row];
    row++;
  }
  for (int idex=0; idex<1; idex++){
    dfieldSourcedField = val[row];
    row++;
  }

  // receive mechanical source
  double& msource = this->getRefToMechanicalSource()(0);
  STensor3& DmsourceDF = this->getRefTodMechanicalSourcedF()[0];
  SVector3& dmsourceDgradField = this->getRefTodMechanicalSourcedGradField()[0][0];
  double& dmsourcedField = this->getRefTodMechanicalSourcedField()(0,0);
  for (int idex=0; idex<1; idex++){
    msource = val[row];
    row++;
  }
  for (int idex=0; idex <9; idex++){
    int i,j;
    Tensor23::getIntsFromIndex(idex,i,j);
    DmsourceDF(i,j) = val[row];
    row++;
  }
  for (int idex=0; idex<3; idex++){
    int i;
    Tensor13::getIntsFromIndex(idex,i);
    dmsourceDgradField(i) = val[row];
    row++;
  }
  for (int idex=0; idex<1; idex++){
    dmsourcedField = val[row];
    row++;
  }
};
#endif // MPI
