//
// C++ Interface: nonLocalDamageQuadYieldHyperDG3DIPVariable
//
// Description:
//
//
// Author: V.D. NGUYEN 2014
//
// Copyright: See COPYING file that comes with this distribution
//
#ifndef NONLOCALDAMAGEHYPERELASTICDG3DIPVARIABLE_H_
#define NONLOCALDAMAGEHYPERELASTICDG3DIPVARIABLE_H_

#include "nonLocalDamageDG3DIPVariable.h"
#include "mlawNonLocalDamageHyperelastic.h"
#include "mlawNonLinearTVEVPwithFailure.h"

class nonLocalDamageHyperelasticDG3DIPVariable: public dG3DIPVariable{
  protected:
    IPHyperViscoElastoPlasticNonLocalDamage *_nldHyperipv;

  public:
    nonLocalDamageHyperelasticDG3DIPVariable(const mlawNonLocalDamagePowerYieldHyper &mlaw, const bool createBodyForceHO, const bool oninter);
    nonLocalDamageHyperelasticDG3DIPVariable(const nonLocalDamageHyperelasticDG3DIPVariable &source);
    virtual nonLocalDamageHyperelasticDG3DIPVariable& operator=(const IPVariable &source);

   /* specific function */
    IPHyperViscoElastoPlasticNonLocalDamage* getIPHyperViscoElastoPlasticNonLocalDamage(){return _nldHyperipv;}
    const IPHyperViscoElastoPlasticNonLocalDamage* getIPHyperViscoElastoPlasticNonLocalDamage() const{return _nldHyperipv;}
    virtual ~nonLocalDamageHyperelasticDG3DIPVariable()
    {
      if (_nldHyperipv)
      delete _nldHyperipv;
      _nldHyperipv = NULL;
    }

    virtual void setLocation(const IPVariable::LOCATION loc) {
      dG3DIPVariable::setLocation(loc);
      if (_nldHyperipv)
        _nldHyperipv->setLocation(loc);
    };

    virtual IPVariable* getInternalData() {return _nldHyperipv;};
    virtual const IPVariable* getInternalData()  const {return _nldHyperipv;};

    virtual bool dissipationIsActive() const {return _nldHyperipv->dissipationIsActive();};
    virtual void blockDissipation(const bool fl){   _nldHyperipv->blockDissipation(fl);}
    virtual bool dissipationIsBlocked() const { return _nldHyperipv->dissipationIsBlocked();}

    virtual double getConstRefToLocalVariable(const int idex) const;
    virtual double& getRefToLocalVariable(const int idex);

    virtual double getConstRefToNonLocalVariableInMaterialLaw(const int idex) const;
    virtual double& getRefToNonLocalVariableInMaterialLaw(const int idex);


    virtual double get(const int i) const;
    virtual double defoEnergy() const;
    virtual double plasticEnergy() const;
    virtual double damageEnergy() const {return _nldHyperipv->damageEnergy();};
    virtual const STensor3 &getConstRefToCharacteristicLengthMatrix(const int idex) const
    {
      if (idex == 0)
        return _nldHyperipv->getConstRefToCharacteristicLength();
      else
      {
        Msg::Error("the non-local damge id = %d is not defined",idex);
        static STensor3 a;
        return a;
      }
    }

    // for path following based on irreversibe energt
    virtual double irreversibleEnergy() const {return _nldHyperipv->irreversibleEnergy();};
    virtual double& getRefToIrreversibleEnergy() {return _nldHyperipv->getRefToIrreversibleEnergy();};

    virtual const STensor3& getConstRefToDIrreversibleEnergyDDeformationGradient() const
      {return _nldHyperipv->getConstRefToDIrreversibleEnergyDF();};
    virtual STensor3& getRefToDIrreversibleEnergyDDeformationGradient()
      {return _nldHyperipv->getRefToDIrreversibleEnergyDF();};

    virtual const double& getConstRefToDIrreversibleEnergyDNonLocalVariable(const int index) const
    {
      if (index == 0)
        {
        return _nldHyperipv->getDIrreversibleEnergyDNonLocalVariable();
        }
      else{
        Msg::Error("the non-local variable %d is not defined",index);
        static double a(0.);
        return a;
      }
    };
    virtual double& getRefToDIrreversibleEnergyDNonLocalVariable(const int index)
    {
      if (index == 0)
      {
        return _nldHyperipv->getRefToDIrreversibleEnergyDNonLocalVariable();
      }
      else{
        Msg::Error("the non-local variable %d is not defined",index);
        static double a(0.);
        return a;
      }
    };

    virtual IPVariable* clone() const {return new nonLocalDamageHyperelasticDG3DIPVariable(*this);};
    virtual void restart();
};

class localDamageHyperelasticDG3DIPVariableWithFailure : public dG3DIPVariable{

 protected:
  IPHyperViscoElastoPlasticMultipleLocalDamage *_localIPv;

 public:
  localDamageHyperelasticDG3DIPVariableWithFailure(const mlawLocalDamagePowerYieldHyperWithFailure &mlaw, const bool createBodyForceHO, const bool oninter);
  localDamageHyperelasticDG3DIPVariableWithFailure(const localDamageHyperelasticDG3DIPVariableWithFailure &source);
  virtual localDamageHyperelasticDG3DIPVariableWithFailure& operator=(const IPVariable &source);

 /* specific function */
  IPHyperViscoElastoPlasticMultipleLocalDamage* getIPHyperViscoElastoPlasticMultipleLocalDamage(){return _localIPv;}
  const IPHyperViscoElastoPlasticMultipleLocalDamage* getIPHyperViscoElastoPlasticMultipleLocalDamage() const{return _localIPv;}
  virtual ~localDamageHyperelasticDG3DIPVariableWithFailure()
  {
    if (_localIPv) delete _localIPv;
    _localIPv = NULL;
  }

  virtual void setLocation(const IPVariable::LOCATION loc) {
    dG3DIPVariable::setLocation(loc);
    if (_localIPv)
      _localIPv->setLocation(loc);
  };

  virtual IPVariable* getInternalData() {return _localIPv;};
  virtual const IPVariable* getInternalData()  const {return _localIPv;};

  virtual bool dissipationIsActive() const {return _localIPv->dissipationIsActive();};
  virtual void blockDissipation(const bool fl){   _localIPv->blockDissipation(fl);}
  virtual bool dissipationIsBlocked() const { return _localIPv->dissipationIsBlocked();}

  virtual double get(const int i) const;
  virtual double defoEnergy() const {return _localIPv->defoEnergy();}
  virtual double plasticEnergy() const {return _localIPv->plasticEnergy();};
  virtual double damageEnergy() const {return _localIPv->damageEnergy();};

  // for path following based on  irreversibe energt
  virtual double irreversibleEnergy() const {return _localIPv->irreversibleEnergy();};
  virtual double& getRefToIrreversibleEnergy() {return _localIPv->getRefToIrreversibleEnergy();};

  virtual const STensor3& getConstRefToDIrreversibleEnergyDDeformationGradient() const {return _localIPv->getConstRefToDIrreversibleEnergyDF();};
  virtual STensor3& getRefToDIrreversibleEnergyDDeformationGradient() {return _localIPv->getRefToDIrreversibleEnergyDF();};

  virtual IPVariable* clone() const {return new localDamageHyperelasticDG3DIPVariableWithFailure(*this);};
  virtual void restart();
};


class nonLocalDamageHyperelasticDG3DIPVariableWithFailure: public dG3DIPVariable{
  protected:
    IPHyperViscoElastoPlasticMultipleNonLocalDamage *_nldHyperipv;

  public:
    nonLocalDamageHyperelasticDG3DIPVariableWithFailure(const mlawNonLocalDamagePowerYieldHyperWithFailure &mlaw, const bool createBodyForceHO, const bool oninter);
    nonLocalDamageHyperelasticDG3DIPVariableWithFailure(const nonLocalDamageHyperelasticDG3DIPVariableWithFailure &source);
    virtual nonLocalDamageHyperelasticDG3DIPVariableWithFailure& operator=(const IPVariable &source);

   /* specific function */
    IPHyperViscoElastoPlasticMultipleNonLocalDamage* getIPHyperViscoElastoPlasticMultipleNonLocalDamage(){return _nldHyperipv;}
    const IPHyperViscoElastoPlasticMultipleNonLocalDamage* getIPHyperViscoElastoPlasticMultipleNonLocalDamage() const{return _nldHyperipv;}
    virtual ~nonLocalDamageHyperelasticDG3DIPVariableWithFailure()
    {
      if (_nldHyperipv)
      delete _nldHyperipv;
      _nldHyperipv = NULL;
    }

    virtual void setLocation(const IPVariable::LOCATION loc) {
      dG3DIPVariable::setLocation(loc);
      if (_nldHyperipv)
        _nldHyperipv->setLocation(loc);
    };

    virtual IPVariable* getInternalData() {return _nldHyperipv;};
    virtual const IPVariable* getInternalData()  const {return _nldHyperipv;};

    virtual bool dissipationIsActive() const {return _nldHyperipv->dissipationIsActive();};
    virtual void blockDissipation(const bool fl){   _nldHyperipv->blockDissipation(fl);}
    virtual bool dissipationIsBlocked() const { return _nldHyperipv->dissipationIsBlocked();}

    virtual double getConstRefToLocalVariable(const int idex) const;
    virtual double& getRefToLocalVariable(const int idex);

    virtual double getConstRefToNonLocalVariableInMaterialLaw(const int idex) const;
    virtual double& getRefToNonLocalVariableInMaterialLaw(const int idex);


    virtual double get(const int i) const;
    virtual double defoEnergy() const;
    virtual double plasticEnergy() const;
    virtual double damageEnergy() const {return _nldHyperipv->damageEnergy();};

    virtual const STensor3 &getConstRefToCharacteristicLengthMatrix(const int idex) const
    {
      return _nldHyperipv->getConstRefToCharacteristicLength(idex);
    }

    // for path following based on irreversibe energt
    virtual double irreversibleEnergy() const {return _nldHyperipv->irreversibleEnergy();};
    virtual double& getRefToIrreversibleEnergy() {return _nldHyperipv->getRefToIrreversibleEnergy();};

    virtual const STensor3& getConstRefToDIrreversibleEnergyDDeformationGradient() const {
      return _nldHyperipv->getConstRefToDIrreversibleEnergyDF();
    };
    virtual STensor3& getRefToDIrreversibleEnergyDDeformationGradient(){
      return _nldHyperipv->getRefToDIrreversibleEnergyDF();
    };

    virtual const double& getConstRefToDIrreversibleEnergyDNonLocalVariable(const int index) const
    {
      return _nldHyperipv->getDIrreversibleEnergyDNonLocalVariable(index);
    };
    virtual double& getRefToDIrreversibleEnergyDNonLocalVariable(const int index)
    {
      return _nldHyperipv->getRefToDIrreversibleEnergyDNonLocalVariable(index);
    };

    virtual IPVariable* clone() const {return new nonLocalDamageHyperelasticDG3DIPVariableWithFailure(*this);};
    virtual void restart();
};

class nonLocalDamageNonLinearTVEVPDG3DIPVariable: public dG3DIPVariable, public extraDofIPVariableBase{
  protected:
    IPNonLinearTVEVPNonLocalDamage *_nldTVEVP;

  public:
    nonLocalDamageNonLinearTVEVPDG3DIPVariable(const mlawNonLocalDamagaNonLinearTVEVP &mlaw, const bool createBodyForceHO, const bool oninter);
    nonLocalDamageNonLinearTVEVPDG3DIPVariable(const nonLocalDamageNonLinearTVEVPDG3DIPVariable &source);
    virtual nonLocalDamageNonLinearTVEVPDG3DIPVariable& operator=(const IPVariable &source);

   /* specific function */
    IPNonLinearTVEVPNonLocalDamage* getIPNonLinearTVEVPNonLocalDamage(){return _nldTVEVP;}
    const IPNonLinearTVEVPNonLocalDamage* getIPNonLinearTVEVPNonLocalDamage() const{return _nldTVEVP;}
    virtual ~nonLocalDamageNonLinearTVEVPDG3DIPVariable()
    {
      if (_nldTVEVP)
      delete _nldTVEVP;
      _nldTVEVP = NULL;
    }

    virtual void setLocation(const IPVariable::LOCATION loc) {
      dG3DIPVariable::setLocation(loc);
      if (_nldTVEVP)
        _nldTVEVP->setLocation(loc);
    };

    virtual IPVariable* getInternalData() {return _nldTVEVP;};
    virtual const IPVariable* getInternalData()  const {return _nldTVEVP;};

    virtual bool dissipationIsActive() const {return _nldTVEVP->dissipationIsActive();};
    virtual void blockDissipation(const bool fl){   _nldTVEVP->blockDissipation(fl);}
    virtual bool dissipationIsBlocked() const { return _nldTVEVP->dissipationIsBlocked();}

    virtual double getConstRefToLocalVariable(const int idex) const;
    virtual double& getRefToLocalVariable(const int idex);

    virtual double getConstRefToNonLocalVariableInMaterialLaw(const int idex) const;
    virtual double& getRefToNonLocalVariableInMaterialLaw(const int idex);


    virtual double get(const int i) const;
    virtual double defoEnergy() const;
    virtual double plasticEnergy() const;
    virtual double damageEnergy() const {return _nldTVEVP->damageEnergy();};
    virtual const STensor3 &getConstRefToCharacteristicLengthMatrix(const int idex) const
    {
      if (idex == 0)
        return _nldTVEVP->getConstRefToCharacteristicLength();
      else
      {
        Msg::Error("the non-local damge id = %d is not defined",idex);
        static STensor3 a;
        return a;
      }
    }

    virtual double getInternalEnergyExtraDofDiffusion() const {return _nldTVEVP->getThermalEnergy();}; // CHECK IF EXISTS

    // for path following based on irreversible energy
    virtual double irreversibleEnergy() const {return _nldTVEVP->irreversibleEnergy();};
    virtual double& getRefToIrreversibleEnergy() {return _nldTVEVP->getRefToIrreversibleEnergy();};

    virtual const STensor3& getConstRefToDIrreversibleEnergyDDeformationGradient() const
      {return _nldTVEVP->getConstRefToDIrreversibleEnergyDF();};
    virtual STensor3& getRefToDIrreversibleEnergyDDeformationGradient()
      {return _nldTVEVP->getRefToDIrreversibleEnergyDF();};

    virtual const double& getConstRefToDIrreversibleEnergyDNonLocalVariable(const int index) const
    {
      if (index == 0)
        {
        return _nldTVEVP->getDIrreversibleEnergyDNonLocalVariable();
        }
      else{
        Msg::Error("the non-local variable %d is not defined",index);
        static double a(0.);
        return a;
      }
    };
    virtual double& getRefToDIrreversibleEnergyDNonLocalVariable(const int index)
    {
      if (index == 0)
      {
        return _nldTVEVP->getRefToDIrreversibleEnergyDNonLocalVariable();
      }
      else{
        Msg::Error("the non-local variable %d is not defined",index);
        static double a(0.);
        return a;
      }
    };

    virtual IPVariable* clone() const {return new nonLocalDamageNonLinearTVEVPDG3DIPVariable(*this);};
    virtual void restart();

    // CHECK WHAT IS ALL OF THE BELOW
    virtual void getConstitutiveExtraDofFlux(const int index, SVector3& q) const {
      if (index > 0) Msg::Error("constitutive field with index %d is not defined",index);
      q = getConstRefToFlux()[index];
    }
    // constitutive field internal energy
    virtual double getConstitutiveExtraDofInternalEnergy(const int index) const {
      if (index > 0) Msg::Error("constitutive field with index %d is not defined",index);
      return getInternalEnergyExtraDofDiffusion();
    }
    // constitutive field capacity per unit field
    virtual double getConstitutiveExtraDofFieldCapacityPerUnitField(const int index) const {
      if (index > 0) Msg::Error("constitutive field with index %d is not defined",index);
      return getConstRefToExtraDofFieldCapacityPerUnitField()(index);
    };
    // mechanical source converted to this constitutive field
    virtual double getConstitutiveExtraDofMechanicalSource(const int index) const {
      if (index > 0) Msg::Error("constitutive field with index %d is not defined",index);
      return getConstRefToMechanicalSource()(index);
    };
    // non-constitutive field value
    virtual void getNonConstitutiveExtraDofFlux(const int index,SVector3& flux) const{
      Msg::Error("ExtraDofDiffusionDG3DIPVariableBase::getNonConstitutiveExtraDofFlux is not defined");
    }

};


class localDamagenonLinearTVEVPDG3DIPVariableWithFailure : public dG3DIPVariable, public extraDofIPVariableBase{

 protected:
  IPNonLinearTVEVPMultipleLocalDamage *_localIPv;

 public:
  localDamagenonLinearTVEVPDG3DIPVariableWithFailure(const mlawLocalDamageNonLinearTVEVPWithFailure &mlaw, const bool createBodyForceHO, const bool oninter);
  localDamagenonLinearTVEVPDG3DIPVariableWithFailure(const localDamagenonLinearTVEVPDG3DIPVariableWithFailure &source);
  virtual localDamagenonLinearTVEVPDG3DIPVariableWithFailure& operator=(const IPVariable &source);

 /* specific function */
  IPNonLinearTVEVPMultipleLocalDamage* getIPNonLinearTVEVPMultipleLocalDamage(){return _localIPv;}
  const IPNonLinearTVEVPMultipleLocalDamage* getIPNonLinearTVEVPMultipleLocalDamage() const{return _localIPv;}
  virtual ~localDamagenonLinearTVEVPDG3DIPVariableWithFailure()
  {
    if (_localIPv) delete _localIPv;
    _localIPv = NULL;
  }

  virtual void setLocation(const IPVariable::LOCATION loc) {
    dG3DIPVariable::setLocation(loc);
    if (_localIPv)
      _localIPv->setLocation(loc);
  };

  virtual IPVariable* getInternalData() {return _localIPv;};
  virtual const IPVariable* getInternalData()  const {return _localIPv;};

  virtual bool dissipationIsActive() const {return _localIPv->dissipationIsActive();};
  virtual void blockDissipation(const bool fl){   _localIPv->blockDissipation(fl);}
  virtual bool dissipationIsBlocked() const { return _localIPv->dissipationIsBlocked();}

  virtual double get(const int i) const;
  virtual double defoEnergy() const {return _localIPv->defoEnergy();}
  virtual double plasticEnergy() const {return _localIPv->plasticEnergy();};
  virtual double damageEnergy() const {return _localIPv->damageEnergy();};

  virtual double getInternalEnergyExtraDofDiffusion() const {return _localIPv->getThermalEnergy();}; // CHECK IF EXISTS

  // for path following based on  irreversibe energt
  virtual double irreversibleEnergy() const {return _localIPv->irreversibleEnergy();};
  virtual double& getRefToIrreversibleEnergy() {return _localIPv->getRefToIrreversibleEnergy();};

  virtual const STensor3& getConstRefToDIrreversibleEnergyDDeformationGradient() const {return _localIPv->getConstRefToDIrreversibleEnergyDF();};
  virtual STensor3& getRefToDIrreversibleEnergyDDeformationGradient() {return _localIPv->getRefToDIrreversibleEnergyDF();};

  virtual IPVariable* clone() const {return new localDamagenonLinearTVEVPDG3DIPVariableWithFailure(*this);};
  virtual void restart();

  // CHECK WHAT IS ALL OF THE BELOW
  virtual void getConstitutiveExtraDofFlux(const int index, SVector3& q) const {
    if (index > 0) Msg::Error("constitutive field with index %d is not defined",index);
    q = getConstRefToFlux()[index];
  }
  // constitutive field internal energy
  virtual double getConstitutiveExtraDofInternalEnergy(const int index) const {
    if (index > 0) Msg::Error("constitutive field with index %d is not defined",index);
    return getInternalEnergyExtraDofDiffusion();
  }
  // constitutive field capacity per unit field
  virtual double getConstitutiveExtraDofFieldCapacityPerUnitField(const int index) const {
    if (index > 0) Msg::Error("constitutive field with index %d is not defined",index);
    return getConstRefToExtraDofFieldCapacityPerUnitField()(index);
  };
  // mechanical source converted to this constitutive field
  virtual double getConstitutiveExtraDofMechanicalSource(const int index) const {
    if (index > 0) Msg::Error("constitutive field with index %d is not defined",index);
    return getConstRefToMechanicalSource()(index);
  };
  // non-constitutive field value
  virtual void getNonConstitutiveExtraDofFlux(const int index,SVector3& flux) const{
    Msg::Error("ExtraDofDiffusionDG3DIPVariableBase::getNonConstitutiveExtraDofFlux is not defined");
  }
};


class nonLocalDamageNonLinearTVEVPDG3DIPVariableWithFailure: public dG3DIPVariable, public extraDofIPVariableBase{
  protected:
    IPNonLinearTVEVPMultipleNonLocalDamage *_nldTVEVPipv;

  public:
    nonLocalDamageNonLinearTVEVPDG3DIPVariableWithFailure(const mlawNonLocalDamageNonLinearTVEVPWithFailure &mlaw, const bool createBodyForceHO, const bool oninter);
    nonLocalDamageNonLinearTVEVPDG3DIPVariableWithFailure(const nonLocalDamageNonLinearTVEVPDG3DIPVariableWithFailure &source);
    virtual nonLocalDamageNonLinearTVEVPDG3DIPVariableWithFailure& operator=(const IPVariable &source);

   /* specific function */
    IPNonLinearTVEVPMultipleNonLocalDamage* getIPNonLinearTVEVPMultipleNonLocalDamage(){return _nldTVEVPipv;}
    const IPNonLinearTVEVPMultipleNonLocalDamage* getIPNonLinearTVEVPMultipleNonLocalDamage() const{return _nldTVEVPipv;}
    virtual ~nonLocalDamageNonLinearTVEVPDG3DIPVariableWithFailure()
    {
      if (_nldTVEVPipv)
      delete _nldTVEVPipv;
      _nldTVEVPipv = NULL;
    }

    virtual void setLocation(const IPVariable::LOCATION loc) {
      dG3DIPVariable::setLocation(loc);
      if (_nldTVEVPipv)
        _nldTVEVPipv->setLocation(loc);
    };

    virtual IPVariable* getInternalData() {return _nldTVEVPipv;};
    virtual const IPVariable* getInternalData()  const {return _nldTVEVPipv;};

    virtual bool dissipationIsActive() const {return _nldTVEVPipv->dissipationIsActive();};
    virtual void blockDissipation(const bool fl){   _nldTVEVPipv->blockDissipation(fl);}
    virtual bool dissipationIsBlocked() const { return _nldTVEVPipv->dissipationIsBlocked();}

    virtual double getConstRefToLocalVariable(const int idex) const;
    virtual double& getRefToLocalVariable(const int idex);

    virtual double getConstRefToNonLocalVariableInMaterialLaw(const int idex) const;
    virtual double& getRefToNonLocalVariableInMaterialLaw(const int idex);


    virtual double get(const int i) const;
    virtual double defoEnergy() const;
    virtual double plasticEnergy() const;
    virtual double damageEnergy() const {return _nldTVEVPipv->damageEnergy();};

    virtual const STensor3 &getConstRefToCharacteristicLengthMatrix(const int idex) const
    {
      return _nldTVEVPipv->getConstRefToCharacteristicLength(idex);
    }

    virtual double getInternalEnergyExtraDofDiffusion() const {return _nldTVEVPipv->getThermalEnergy();}; // CHECK IF EXISTS

    // for path following based on irreversibe energt
    virtual double irreversibleEnergy() const {return _nldTVEVPipv->irreversibleEnergy();};
    virtual double& getRefToIrreversibleEnergy() {return _nldTVEVPipv->getRefToIrreversibleEnergy();};

    virtual const STensor3& getConstRefToDIrreversibleEnergyDDeformationGradient() const {
      return _nldTVEVPipv->getConstRefToDIrreversibleEnergyDF();
    };
    virtual STensor3& getRefToDIrreversibleEnergyDDeformationGradient(){
      return _nldTVEVPipv->getRefToDIrreversibleEnergyDF();
    };

    virtual const double& getConstRefToDIrreversibleEnergyDNonLocalVariable(const int index) const
    {
      return _nldTVEVPipv->getDIrreversibleEnergyDNonLocalVariable(index);
    };
    virtual double& getRefToDIrreversibleEnergyDNonLocalVariable(const int index)
    {
      return _nldTVEVPipv->getRefToDIrreversibleEnergyDNonLocalVariable(index);
    };

    virtual IPVariable* clone() const {return new nonLocalDamageNonLinearTVEVPDG3DIPVariableWithFailure(*this);};
    virtual void restart();

    // CHECK WHAT IS ALL OF THE BELOW
    virtual void getConstitutiveExtraDofFlux(const int index, SVector3& q) const {
      if (index > 0) Msg::Error("constitutive field with index %d is not defined",index);
      q = getConstRefToFlux()[index];
    }
    // constitutive field internal energy
    virtual double getConstitutiveExtraDofInternalEnergy(const int index) const {
      if (index > 0) Msg::Error("constitutive field with index %d is not defined",index);
      return getInternalEnergyExtraDofDiffusion();
    }
    // constitutive field capacity per unit field
    virtual double getConstitutiveExtraDofFieldCapacityPerUnitField(const int index) const {
      if (index > 0) Msg::Error("constitutive field with index %d is not defined",index);
      return getConstRefToExtraDofFieldCapacityPerUnitField()(index);
    };
    // mechanical source converted to this constitutive field
    virtual double getConstitutiveExtraDofMechanicalSource(const int index) const {
      if (index > 0) Msg::Error("constitutive field with index %d is not defined",index);
      return getConstRefToMechanicalSource()(index);
    };
    // non-constitutive field value
    virtual void getNonConstitutiveExtraDofFlux(const int index,SVector3& flux) const{
      Msg::Error("ExtraDofDiffusionDG3DIPVariableBase::getNonConstitutiveExtraDofFlux is not defined");
    }
};

#endif // NONLOCALDAMAGEHYPERELASTICDG3DIPVARIABLE_H_
