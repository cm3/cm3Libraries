//
// C++ Interface: material law strain gradient
//
// Description: Class with definition of material law for finite strain problem
//
//
// Author: V.D. NGUYEN 2011
//
// Copyright: See COPYING file that comes with this distribution
//
#include "hoDGMaterialLaw.h"
#include "hoDGIPVariable.h"
#include "terms.h"
#include "MInterfaceElement.h"
#include "nonLinearMechSolver.h"

void hoDGElasticMaterialLaw::fillElasticStiffness(elasticLawBase* firstlaw, STensor43 &tangent) const{
  if (firstlaw == NULL or dynamic_cast<elasticLaw*>(firstlaw)==NULL)
    Msg::Error("elastic law must be used");

  double E = firstlaw->getParameter("YOUNG_MODULUS");
  double nu = firstlaw->getParameter("POISSON_RATIO");
  dG3DMaterialLaw::fillElasticStiffness(E,nu,tangent);
};
void hoDGElasticMaterialLaw::fillElasticStiffness(elasticLawBase* secondlaw, STensor63& secondsecond){
  if (secondlaw == NULL or dynamic_cast<elasticSecondOrderLaw*>(secondlaw) == NULL)
    Msg::Error("second elastic law must be used");

  elasticSecondOrderLaw* law = dynamic_cast<elasticSecondOrderLaw*>(secondlaw);
  STensor33 G(0), Q(0);
  law->constitutive(G,Q,secondsecond,true);
};


hoDGElasticMaterialLaw::hoDGElasticMaterialLaw(const int num, const double rho) : dG3DMaterialLaw(num,rho,false),
                        _elasticFirst(NULL),_elasticSecond(NULL),_typeFirst(elasticLawBase::LINEAR_ELASTIC),
                        _typeSecond(elasticLawBase::LINEAR_ELASTIC_SECOND_ORDER){}


hoDGElasticMaterialLaw::hoDGElasticMaterialLaw(const int num, const int first, const int second,
                           const double k, const double mu, const double rho,
                           const double tol)
                           :dG3DMaterialLaw(num,rho,false){

  _typeFirst = (elasticLawBase::ELASTIC_TYPE)first;
  if (_typeFirst == elasticLawBase::LINEAR_ELASTIC){
    _elasticFirst = new linearElasticLaw(k,mu);
		Msg::Info("Linear elastic material is used");
	}
  else if (_typeFirst == elasticLawBase::NEO_HOOKEAN){
    _elasticFirst = new NeoHookeanElasticLaw(k,mu,tol);
		Msg::Info("Neo Hookean elastic material is used");
	}
  else if (_typeFirst == elasticLawBase::BI_LOGARITHMIC){
    _elasticFirst = new biLogarithmicElasticLaw(k,mu);
		Msg::Info("Bilogarithmic elastic material is used");
	}
	else{
		Msg::Error("Elastic law is not used");
    _elasticFirst = NULL;
  }


  _typeSecond = (elasticLawBase::ELASTIC_TYPE)second;
  if (_typeSecond == elasticLawBase::LINEAR_ELASTIC_SECOND_ORDER)
    _elasticSecond = new linearElasticSecondOrderLaw();
  else if (_typeSecond == elasticLawBase::LINEAR_MINDLIN)
    _elasticSecond = new linearElasticMindlinLaw();
  else{
    Msg::Error("Elastic second order law is not used");
    _elasticSecond == NULL;
  }

};

hoDGElasticMaterialLaw::hoDGElasticMaterialLaw(const hoDGElasticMaterialLaw& src) :dG3DMaterialLaw(src),
	elasticSecondOrderTangent(src.elasticSecondOrderTangent),elasticFirstSecondTangent(src.elasticFirstSecondTangent),
	elasticSecondFirstTangent(src.elasticSecondFirstTangent),
  _typeFirst(src._typeFirst),_typeSecond(src._typeSecond){
  if (src._elasticFirst){
    if (_elasticFirst) delete _elasticFirst;
    _elasticFirst = src._elasticFirst->clone();
  }
  else
    _elasticFirst = NULL;

  if (src._elasticSecond){
    if (_elasticSecond) delete _elasticSecond;
    _elasticSecond = src._elasticSecond->clone();
  }
  else
    _elasticSecond = NULL;
};

hoDGElasticMaterialLaw::~hoDGElasticMaterialLaw(){
  if (_elasticFirst) delete _elasticFirst;
  if (_elasticSecond) delete _elasticSecond;
  _elasticFirst = NULL;
  _elasticSecond = NULL;
};

void hoDGElasticMaterialLaw::setType(const int first, const int second){
  _typeFirst = (elasticLawBase::ELASTIC_TYPE)first;
	if (_elasticFirst and _elasticFirst->getType()!= _typeFirst){
		delete _elasticFirst;
		_elasticFirst == NULL;
	}
	if (_elasticFirst ==NULL){
		 if (_typeFirst == elasticLawBase::LINEAR_ELASTIC){
    	_elasticFirst = new linearElasticLaw();
			Msg::Info("Linear elastic material is used");
		}
  	else if (_typeFirst == elasticLawBase::NEO_HOOKEAN){
    	_elasticFirst = new NeoHookeanElasticLaw();
			Msg::Info("Neo Hookean elastic material is used");
		}
  	else if (_typeFirst == elasticLawBase::BI_LOGARITHMIC){
    	_elasticFirst = new biLogarithmicElasticLaw();
			Msg::Info("Bilogarithmic elastic material is used");
		}
		else{
			Msg::Error("Elastic material law is not used");
			_elasticFirst = NULL;
    }
	}

	_typeSecond = (elasticLawBase::ELASTIC_TYPE)second;
	if (_elasticSecond and  _elasticSecond->getType()!=_typeSecond){
    delete _elasticSecond;
    _elasticSecond= NULL;
	}
	if (_elasticSecond == NULL){
    if (_typeSecond == elasticLawBase::LINEAR_ELASTIC_SECOND_ORDER)
      _elasticSecond = new linearElasticSecondOrderLaw();
    else if (_typeSecond == elasticLawBase::LINEAR_MINDLIN)
      _elasticSecond = new linearElasticMindlinLaw();
    else{
      Msg::Error("Elastic second order law is not used");
      _elasticSecond = NULL;
    }

	}
};
void hoDGElasticMaterialLaw::setParameter(const std::string key, const double val){
  if (_elasticFirst==NULL or _elasticSecond ==NULL)
    this->setType(_typeFirst,_typeSecond);
  if (_elasticFirst)
    _elasticFirst->setParameter(key,val);
	if (_elasticSecond)
    _elasticSecond->setParameter(key,val);
};

double hoDGElasticMaterialLaw::soundSpeed() const {
  if (_elasticFirst){
    double K = _elasticFirst->getParameter("BULK_MODULUS");
    double rho = this->density();
    return std::sqrt(K/rho);
  }
  else{
    Msg::Error("first order law is not used");
    return 0.;
  }

};

bool hoDGElasticMaterialLaw::isHighOrder() const {
  if (_elasticSecond)
    return true;
  else
    return false;
};

void hoDGElasticMaterialLaw::createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt ) const{
  bool inter = true;
  const MInterfaceElement* ie = dynamic_cast<const MInterfaceElement*>(ele);
  if (ie==NULL) inter = false;
  if (_typeFirst == elasticLawBase::LINEAR_ELASTIC){
    IPVariable *ipvi = new hoDGLinearElasticIPVariable(hasBodyForce,inter);
    IPVariable *ipv1 = new hoDGLinearElasticIPVariable(hasBodyForce,inter);
    IPVariable *ipv2 = new hoDGLinearElasticIPVariable(hasBodyForce,inter);
    if(ips != NULL) delete ips;
    ips = new IP3State(state_,ipvi,ipv1,ipv2);
  }
  else{
    IPVariable *ipvi = new hoDGIPVariable(hasBodyForce,inter);
    IPVariable *ipv1 = new hoDGIPVariable(hasBodyForce,inter);
    IPVariable *ipv2 = new hoDGIPVariable(hasBodyForce,inter);
    if(ips != NULL) delete ips;
    ips = new IP3State(state_,ipvi,ipv1,ipv2);
  }
};

void hoDGElasticMaterialLaw::createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const{
  bool inter = true;
  const MInterfaceElement* iele  = dynamic_cast<const MInterfaceElement*>(ele);
  if (iele ==NULL) inter = false;
  if (ipv) delete ipv;
  ipv = new hoDGIPVariable(hasBodyForce,inter);
};

void hoDGElasticMaterialLaw::initLaws(const std::map<int,materialLaw*> &maplaw){
  if (!_initialized){
    _initialized = true;
    if (_elasticFirst){
      fillElasticStiffness(_elasticFirst,elasticStiffness);
    }
    if (_elasticSecond){
      fillElasticStiffness(_elasticSecond,elasticSecondOrderTangent);
    }
  }
};

void hoDGElasticMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff, const bool checkfrac, const bool dTangent){

  hoDGIPVariable* hoipv = static_cast<hoDGIPVariable*>(ipv);
  const hoDGIPVariable* hoipvprev = static_cast<const hoDGIPVariable*>(ipvprev);
  const STensor3& F = hoipv->getConstRefToDeformationGradient();
  STensor3& P = hoipv->getRefToFirstPiolaKirchhoffStress();
  STensor43& L = hoipv->getRefToTangentModuli();

  if (_elasticFirst){
    elasticLaw* firstlaw = dynamic_cast<elasticLaw*>(_elasticFirst);
    firstlaw->constitutive(F,P,L,stiff,dTangent,hoipv->getPtrTodCmdFm());
  }

  if (isHighOrder()){
    const STensor33& G = hoipv->getConstRefToGradientOfDeformationGradient();
    STensor33& Q = hoipv->getRefToSecondOrderStress();

    STensor53& LG = hoipv->getRefToCrossTangentFirstSecond();
    STensor53& JF = hoipv->getRefToCrossTangentSecondFirst();
    STensor63& J = hoipv->getRefToSecondOrderTangent();

    if (_elasticSecond){
      elasticSecondOrderLaw* secondlaw = dynamic_cast<elasticSecondOrderLaw*>(_elasticSecond);
      secondlaw->constitutive(G,Q,J,stiff);
      STensorOperation::zero(LG);
      STensorOperation::zero(JF);
    }
  };
  hoDGElasticMaterialLaw::setElasticStiffness(ipv);
};

void hoDGElasticMaterialLaw::setElasticStiffness(IPVariable* ipv) const{
  hoDGIPVariable* hoipv = dynamic_cast<hoDGIPVariable*>(ipv);
  if (_elasticFirst)
    hoipv->setRefToDGElasticTangentModuli(elasticStiffness);
  if (_elasticSecond){
    hoipv->setRefToSecondOrderDGElasticTangentModuli(elasticSecondOrderTangent);
    hoipv->setRefToCrossFirstSecondDGElasticTangentModuli(elasticFirstSecondTangent);
    hoipv->setRefToCrossSecondFirstDGElasticTangentModuli(elasticSecondFirstTangent);
  }
};


fullHODGElasticMaterialLaw::fullHODGElasticMaterialLaw(const int num, const double rho)
                    :hoDGElasticMaterialLaw(num,rho),_elasticFirstSecond(NULL){};
fullHODGElasticMaterialLaw::fullHODGElasticMaterialLaw(const int num, const int first, const int second,
                       const double K, const double mu, const double rho, const double tol)
                      : hoDGElasticMaterialLaw(num,first,second,K,mu,rho,tol),_elasticFirstSecond(NULL){};

fullHODGElasticMaterialLaw::fullHODGElasticMaterialLaw(const fullHODGElasticMaterialLaw& src): hoDGElasticMaterialLaw(src){
  if (src._elasticFirstSecond){
    this->_elasticFirstSecond = src._elasticFirstSecond->clone();
  }
  else
    _elasticFirstSecond = NULL;
};

fullHODGElasticMaterialLaw::~fullHODGElasticMaterialLaw(){
  if (_elasticFirstSecond) delete _elasticFirstSecond;
  _elasticFirstSecond = NULL;
}

void fullHODGElasticMaterialLaw::setParameter(const std::string key, const double val){
  hoDGElasticMaterialLaw::setParameter(key,val);
  if (_elasticFirstSecond == NULL){
    _elasticFirstSecond = new linearElasticFirstSecondLaw();
  }
  _elasticFirstSecond->setParameter(key,val);
};

void fullHODGElasticMaterialLaw::fillElasticStiffness(elasticLawBase* fs, STensor53& firstsecond, STensor53& secondfirst){
  static STensor3 F(0.), P(0.);
  static STensor33 G(0.), Q(0.);
  elasticFirstSecondLaw* pfs = dynamic_cast<linearElasticFirstSecondLaw*>(fs);
  pfs->constitutive(F,G,P,Q,firstsecond,secondfirst,true);
};

void fullHODGElasticMaterialLaw::initLaws(const std::map<int,materialLaw*> &maplaw){
  if (!_initialized){
    _initialized = true;
    if (_elasticFirst){
      hoDGElasticMaterialLaw::fillElasticStiffness(_elasticFirst,elasticStiffness);
    }
    if (_elasticSecond){
      hoDGElasticMaterialLaw::fillElasticStiffness(_elasticSecond,elasticSecondOrderTangent);
    }
    if (_elasticFirstSecond){
      fillElasticStiffness(_elasticFirstSecond,elasticFirstSecondTangent,elasticSecondFirstTangent);
    }
  }

};

void fullHODGElasticMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff2, const bool checkfrac, const bool dTangent){
  bool stiff=true;

  hoDGIPVariable* hoipv = static_cast<hoDGIPVariable*>(ipv);
  const hoDGIPVariable* hoipvprev = static_cast<const hoDGIPVariable*>(ipvprev);
  const STensor3& F = hoipv->getConstRefToDeformationGradient();
  STensor3& P = hoipv->getRefToFirstPiolaKirchhoffStress();
  STensor43& L = hoipv->getRefToTangentModuli();

  if (_elasticFirst){
    elasticLaw* firstlaw = dynamic_cast<elasticLaw*>(_elasticFirst);
    firstlaw->constitutive(F,P,L,stiff,dTangent,hoipv->getPtrTodCmdFm());
  }

  if (isHighOrder()){
    const STensor33& G = hoipv->getConstRefToGradientOfDeformationGradient();
    STensor33& Q = hoipv->getRefToSecondOrderStress();

    STensor53& LG = hoipv->getRefToCrossTangentFirstSecond();
    STensor53& JF = hoipv->getRefToCrossTangentSecondFirst();
    STensor63& J = hoipv->getRefToSecondOrderTangent();

    if (_elasticSecond){
      elasticSecondOrderLaw* secondlaw = dynamic_cast<elasticSecondOrderLaw*>(_elasticSecond);
      secondlaw->constitutive(G,Q,J,stiff);
    }


    if (_elasticFirstSecond){
      static STensor3 P1;
      STensorOperation::zero(P1);
      
      static STensor33 Q1;
      STensorOperation::zero(Q1);
      
      elasticFirstSecondLaw* fs = dynamic_cast<elasticFirstSecondLaw*>(_elasticFirstSecond);
      fs->constitutive(F,G,P1,Q1,LG,JF,stiff);
      P+= P1;
      Q+= Q1;
    }
  };
  hoDGElasticMaterialLaw::setElasticStiffness(ipv);
};



hoDGMultiscaleMaterialLaw::hoDGMultiscaleMaterialLaw(const int lnum, const int tag)
                            : dG3DMaterialLaw(lnum,0,false),numericalMaterial(tag){};

void hoDGMultiscaleMaterialLaw::initLaws(const std::map<int,materialLaw*> &maplaw){
  if (!_initialized){
    this->fillElasticStiffness(_rho,elasticStiffness, elasticFirstSecondTangent,
                           elasticSecondFirstTangent,elasticSecondOrderTangent);
    _initialized = true;
  }
};

bool hoDGMultiscaleMaterialLaw::isHighOrder() const {
  if (_microSolver->getMicroBC()->getOrder() ==1){
    return false;
  }
  else if (_microSolver->getMicroBC()->getOrder()==2){
    return true;
  }
  else{
    Msg::Error("The homogenization order is not correctly defined");
    return false;
  }
};

void hoDGMultiscaleMaterialLaw::fillElasticStiffness(double & rho, STensor43& LF, STensor53& LG, STensor53& JF, STensor63& JG){
  printf("get elastic properties\n");
	
  this->assignMeshId(0,0);
  nonLinearMechSolver* solver = this->createMicroSolver(0,0);
	solver->tangentAveragingFlag(true);
	solver->initMicroSolver();
	
  LF = solver->getHomogenizationState(IPStateBase::current)->getHomogenizedTangentOperator_F_F(); 
  LG = solver->getHomogenizationState(IPStateBase::current)->getHomogenizedTangentOperator_F_G();
  JF = solver->getHomogenizationState(IPStateBase::current)->getHomogenizedTangentOperator_G_F();
  if (_microSolver->getMicroBC()->getOrder() == 2){ 	
     if(solver->computedUdF()){
       STensorOperation::zero(JG);
       const STensor3& _rveGeoInertia= solver->getRVEGeometricalInertia();
       double vRVE = solver->getRVEVolume();
       for (int i=0; i<3; i++)
         for (int j=0; j<3; j++)
            for (int k=0; k<3; k++)
               for (int l=0; l<3; l++)
                  for (int p=0; p<3; p++)  
                     for (int q=0; q<3; q++)
                        JG(i,j,k,l,p,q) = 0.25*(LF(i,j,l,p)*_rveGeoInertia(q,k)+LF(i,k,l,p)*_rveGeoInertia(q,j))/vRVE
                                                                                    +0.25*(LF(i,j,l,q)*_rveGeoInertia(p,k)+LF(i,k,l,q)*_rveGeoInertia(p,j))/vRVE;
                                                                                    
     }
     else{	
       JG = solver->getHomogenizationState(IPStateBase::current)->getHomogenizedTangentOperator_G_G();
     }  
  };
  rho = solver->getHomogenizedDensity();

  delete solver;
}

void hoDGMultiscaleMaterialLaw::createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const{
  
	this->createIPState(true,ips,hasBodyForce, state_,ele,nbFF_,GP,gpt);
};

void hoDGMultiscaleMaterialLaw::createIPState(const bool isSolve, IPStateBase* &ips,bool hasBodyForce, const bool* state,
                                 const MElement *ele, const int nbFF, const IntPt *GP, const int gpt) const{
  bool oninter = true;
	const MInterfaceElement* iele = dynamic_cast<const MInterfaceElement*>(ele);
	if (iele == NULL) oninter = false;
	IPVariable* ipvi = new hoDGMultiscaleIPVariable(hasBodyForce,oninter);
	IPVariable* ipv1 = new hoDGMultiscaleIPVariable(hasBodyForce,oninter);
	IPVariable* ipv2 = new hoDGMultiscaleIPVariable(hasBodyForce,oninter);
	if(ips != NULL) delete ips;
	ips = new IP3State(state,ipvi,ipv1,ipv2);
	if (isSolve){
		int el = ele->getNum();
		nonLinearMechSolver* solver = this->createMicroSolver(el,gpt);
		solver->initMicroSolver();
		ips->setMicroSolver(solver);
	}
};

void hoDGMultiscaleMaterialLaw::createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const{
  bool oninter = true;
  const MInterfaceElement* iele = dynamic_cast<const MInterfaceElement*>(ele);
  if (iele == NULL) oninter = false;
  ipv = new hoDGMultiscaleIPVariable(hasBodyForce,oninter);
};

void hoDGMultiscaleMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff, const bool checkfrac, const bool dTangent){
  hoDGMultiscaleIPVariable* mipv = static_cast<hoDGMultiscaleIPVariable*>(ipv);
  const hoDGMultiscaleIPVariable* mipvprev = static_cast<const hoDGMultiscaleIPVariable*>(ipvprev);
  nonLinearMechSolver* solver = mipv->getMicroSolver();
  // set kinematics input
  const STensor3& F = mipv->getConstRefToDeformationGradient();
  solver->getMicroBC()->setDeformationGradient(F);
  if (solver->getMicroBC()->getOrder() ==2){
    const STensor33& G = mipv->getConstRefToGradientOfDeformationGradient();
    solver->getMicroBC()->setGradientOfDeformationGradient(G);
  };
  // set tangent averaging flag
  solver->tangentAveragingFlag(stiff);
  // solve micro problem
  bool sucess = solver->microSolve();
  if (sucess)
  {
    // get homogenized properties form solved micro problem
    mipv->getRefToFirstPiolaKirchhoffStress() = solver->getHomogenizationState(IPStateBase::current)->getHomogenizedStress();
    if (stiff){
      mipv->getRefToTangentModuli()= solver->getHomogenizationState(IPStateBase::current)->getHomogenizedTangentOperator_F_F();
    }

    mipv->setRefToDGElasticTangentModuli(elasticStiffness);

    if (solver->getMicroBC()->getOrder() ==2){
      mipv->getRefToSecondOrderStress() = solver->getHomogenizationState(IPStateBase::current)->getHomogenizedSecondOrderStress();
   
      if (stiff){
        mipv->getRefToCrossTangentFirstSecond() = solver->getHomogenizationState(IPStateBase::current)->getHomogenizedTangentOperator_F_G();
        mipv->getRefToCrossTangentSecondFirst() = solver->getHomogenizationState(IPStateBase::current)->getHomogenizedTangentOperator_G_F();
        mipv->getRefToSecondOrderTangent() = solver->getHomogenizationState(IPStateBase::current)->getHomogenizedTangentOperator_G_G();
      }

      mipv->setRefToCrossFirstSecondDGElasticTangentModuli(elasticFirstSecondTangent);
      mipv->setRefToCrossSecondFirstDGElasticTangentModuli(elasticSecondFirstTangent);
      mipv->setRefToSecondOrderDGElasticTangentModuli(elasticSecondOrderTangent);
    }
  }
};

void hoDGMultiscaleMaterialLaw::setElasticStiffness(IPVariable* ipv) const{
  dG3DIPVariable* dGipv =dynamic_cast<dG3DIPVariable*>(ipv);
  if (dGipv)
    dGipv->setRefToDGElasticTangentModuli(elasticStiffness);

  hoDGIPVariable* hoipv = dynamic_cast<hoDGIPVariable*>(ipv);
  if (hoipv){
    hoipv->setRefToSecondOrderDGElasticTangentModuli(elasticSecondOrderTangent);
    hoipv->setRefToCrossFirstSecondDGElasticTangentModuli(elasticFirstSecondTangent);
    hoipv->setRefToCrossSecondFirstDGElasticTangentModuli(elasticSecondFirstTangent);
  }
};

void hoDGMultiscaleMaterialLaw::initialIPVariable(IPVariable* ipv, bool stiff){
		dG3DIPVariable* dGipv =dynamic_cast<dG3DIPVariable*>(ipv);
		if (dGipv){
			dGipv->setRefToDGElasticTangentModuli(elasticStiffness);
			dGipv->getRefToTangentModuli() = elasticStiffness;
		}
		
		hoDGIPVariable* hoipv = dynamic_cast<hoDGIPVariable*>(ipv);
		if (hoipv){
			hoipv->setRefToSecondOrderDGElasticTangentModuli(elasticSecondOrderTangent);
			hoipv->setRefToCrossFirstSecondDGElasticTangentModuli(elasticFirstSecondTangent);
			hoipv->setRefToCrossSecondFirstDGElasticTangentModuli(elasticSecondFirstTangent);
			
			hoipv->getRefToSecondOrderTangent() = elasticSecondOrderTangent;
			hoipv->getRefToCrossTangentFirstSecond() =elasticFirstSecondTangent;
			hoipv->getRefToCrossTangentSecondFirst() = elasticSecondFirstTangent;
		}
    
};

bool hoDGMultiscaleMaterialLaw::withEnergyDissipation() const  {
  return _microSolver->withEnergyDissipation();
}
