//
//
// Description: Class with definition of materialLaw for shell
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "dG3DMaterialLaw.h"
#include "ipstate.h"
#include "MInterfaceElement.h"
#include "ipField.h"
#include "mlawLinearThermoMechanics.h"
#include "mlawSMP.h"
#include "mlawPhenomenologicalSMP.h"
#include "mlaw.h"
#include "mlawLinearElecTherMech.h"
#include "mlawAnIsotropicElecTherMech.h"
#include "mlawElecSMP.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <string>
#include "dG3DCohesiveIPVariable.h"
#include "FractureCohesiveDG3DIPVariable.h"
#include "dG3DReduction.h"
#include "mlawLinearElecMagTherMech.h"
#include "nonLinearMechSolver.h"
#include "numericalMaterial.h"
#include "nonLocalDamageDG3DIPVariable.h"
#if defined(HAVE_TORCH)
#include <torch/torch.h>
#include <torch/script.h>
#endif


// 3D to 1D
void dG3DMaterialLaw::stress3DTo1D(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff, const bool checkfrac){
	reduction3DTo1DStress(this,ipv,ipvprev,stiff,checkfrac);
};

void dG3DMaterialLaw::stress3DTo1D_constantTriaxiality(double T, IPVariable* ipv, const IPVariable* ipvprev, const bool stiff, const bool checkfrac){
	reduction3DTo1DStress_constantTriaxiality(T,this,ipv,ipvprev,stiff,checkfrac);
};

// 3D to 2D
void dG3DMaterialLaw::stress3DTo2D(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff, const bool checkfrac, const bool dTangent){
	reduction3DTo2DStress(this,ipv,ipvprev,stiff,checkfrac,dTangent);
};

void dG3DMaterialLaw::stress3DTo2D_XZ(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff, const bool checkfrac, const bool dTangent){
	reduction3DTo2DStress_XZ(this,ipv,ipvprev,stiff,checkfrac,dTangent);
};


void dG3DMaterialLaw::fillElasticStiffness(double E, double nu, STensor43 &K_) const
{
  double mu = 0.5*E/(1.+nu);
  double lambda = (E*nu)/(1.+nu)/(1.-2.*nu);
  double twicemu = mu+mu;
  STensorOperation::zero(K_);
  K_(0,0,0,0) = lambda + twicemu;
  K_(1,1,0,0) = lambda;
  K_(2,2,0,0) = lambda;
  K_(0,0,1,1) = lambda;
  K_(1,1,1,1) = lambda + twicemu;
  K_(2,2,1,1) = lambda;
  K_(0,0,2,2) = lambda;
  K_(1,1,2,2) = lambda;
  K_(2,2,2,2) = lambda + twicemu;

  if(lambda>=1000.*mu)
  {
      mu = lambda + mu;
  }

  K_(1,0,1,0) = mu;
  K_(2,0,2,0) = mu;
  K_(0,1,0,1) = mu;
  K_(2,1,2,1) = mu;
  K_(0,2,0,2) = mu;
  K_(1,2,1,2) = mu;

  K_(0,1,1,0) = mu;
  K_(0,2,2,0) = mu;
  K_(1,0,0,1) = mu;
  K_(1,2,2,1) = mu;
  K_(2,0,0,2) = mu;
  K_(2,1,1,2) = mu;
}
void dG3DMaterialLaw::fillElasticStiffness(double Ex,double Ey,double Ez,double Vxy,double Vxz,double Vyz,
                                           double MUxy,double MUxz,double MUyz,double alpha,double beta,double gamma,
                                           STensor43 &K_) const
{
  const double Vyx= Vxy*Ey/Ex  ;
  const double Vzx= Vxz*Ez/Ex  ;
  const double Vzy= Vyz*Ez/Ey  ;
  const double D=( 1-Vxy*Vyx-Vzy*Vyz-Vxz*Vzx-2*Vxy*Vyz*Vzx ) / ( Ex*Ey*Ez );

  STensor43 ElasticityTensor(0.);
  ElasticityTensor(0,0,0,0)=( 1-Vyz*Vzy ) / (Ey*Ez*D );
  ElasticityTensor(1,1,1,1)=( 1-Vxz*Vzx ) / (Ex*Ez*D );
  ElasticityTensor(2,2,2,2)=( 1-Vyx*Vxy ) / (Ey*Ex*D );

  ElasticityTensor(0,0,1,1)=( Vyx+Vzx*Vyz ) / (Ey*Ez*D );
  ElasticityTensor(0,0,2,2)=( Vzx+Vyx*Vzy ) / (Ey*Ez*D );
  ElasticityTensor(1,1,2,2)=( Vzy+Vxy*Vzx ) / (Ex*Ez*D );

  ElasticityTensor(1,1,0,0)=( Vxy+Vzy*Vxz ) / (Ex*Ez*D );
  ElasticityTensor(2,2,0,0)=( Vxz+Vxy*Vyz ) / (Ey*Ex*D );
  ElasticityTensor(2,2,1,1)=( Vyz+Vxz*Vyx ) / (Ey*Ex*D );

  ElasticityTensor(1,2,1,2)=MUyz;ElasticityTensor(1,2,2,1)=MUyz;
  ElasticityTensor(2,1,2,1)=MUyz;ElasticityTensor(2,1,1,2)=MUyz;

  ElasticityTensor(0,1,0,1)=MUxy;ElasticityTensor(0,1,1,0)=MUxy;
  ElasticityTensor(1,0,1,0)=MUxy;ElasticityTensor(1,0,0,1)=MUxy;

  ElasticityTensor(0,2,0,2)=MUxz;ElasticityTensor(0,2,2,0)=MUxz;
  ElasticityTensor(2,0,2,0)=MUxz;ElasticityTensor(2,0,0,2)=MUxz;


  double c1,c2,c3,s1,s2,s3;
  double s1c2, c1c2;
  double pi(3.14159265359);
  double fpi = pi/180.;

  c1 = cos(alpha*fpi);
  s1 = sin(alpha*fpi);
  c2 = cos(beta*fpi);
  s2 = sin(beta*fpi);
  c3 = cos(gamma*fpi);
  s3 = sin(gamma*fpi);

  s1c2 = s1*c2;
  c1c2 = c1*c2;

  STensor3 R;		//3x3 rotation matrix
  R(0,0) = c3*c1 - s1c2*s3;
  R(0,1) = c3*s1 + c1c2*s3;
  R(0,2) = s2*s3;
  R(1,0) = -s3*c1 - s1c2*c3;
  R(1,1) = -s3*s1 + c1c2*c3;
  R(1,2) = s2*c3;
  R(2,0) = s1*s2;
  R(2,1) = -c1*s2;
  R(2,2) = c2;

  for(int i=0;i<3;i++)
  {
    for(int j=0;j<3;j++)
      {
      for(int k=0;k<3;k++)
      {
        for(int l=0;l<3;l++)
        {
          K_(i,j,k,l)=0.;
          for(int m=0;m<3;m++)
          {
            for(int n=0;n<3;n++)
            {
              for(int o=0;o<3;o++)
              {
                for(int p=0;p<3;p++)
                {
                  K_(i,j,k,l)+=R(m,i)*R(n,j)*R(o,k)*R(p,l)*ElasticityTensor(m,n,o,p);
                }
              }
            }
          }
        }
      }
    }
  }
}

void dG3DMaterialLaw::initialIPVariable(IPVariable* ipv, bool stiff){
	IPVariable* ipvTemp = ipv->clone();
	this->stress(ipv,ipvTemp,stiff,false);
	delete ipvTemp;
};

dG3DMaterialLawWithTangentByPerturbation::dG3DMaterialLawWithTangentByPerturbation(dG3DMaterialLaw& law, double pert):
  dG3DMaterialLaw(law.getNum(),law.density(),law.isInitialized()),_stressLaw(&law), _pertFactor(pert),ipvTemp(NULL){
  elasticStiffness = law.elasticStiffness;
  TotalelasticStiffness = law.TotalelasticStiffness;
}
dG3DMaterialLawWithTangentByPerturbation::dG3DMaterialLawWithTangentByPerturbation(const dG3DMaterialLawWithTangentByPerturbation& src):
  dG3DMaterialLaw(src),_stressLaw(src._stressLaw),_pertFactor(src._pertFactor), ipvTemp(NULL)
{
  elasticStiffness = _stressLaw->elasticStiffness;
  TotalelasticStiffness = _stressLaw->TotalelasticStiffness;
};

dG3DMaterialLawWithTangentByPerturbation::~dG3DMaterialLawWithTangentByPerturbation()
{
  if (ipvTemp != NULL)
  {
    delete ipvTemp; ipvTemp = NULL;
  }
}

void dG3DMaterialLawWithTangentByPerturbation::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  _stressLaw->createIPState(ips,hasBodyForce,state_,ele,nbFF_,GP,gpt);
}

void dG3DMaterialLawWithTangentByPerturbation::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  _stressLaw->createIPVariable(ipv,hasBodyForce, ele,nbFF_,GP,gpt);
}

void dG3DMaterialLawWithTangentByPerturbation::calledBy2lawFracture()
{
  _stressLaw->calledBy2lawFracture();
}

void dG3DMaterialLawWithTangentByPerturbation::initLaws(const std::map<int,materialLaw*> &maplaw)
{
  _stressLaw->initLaws(maplaw);
}

double dG3DMaterialLawWithTangentByPerturbation::density() const{return _stressLaw->density();}
double dG3DMaterialLawWithTangentByPerturbation::soundSpeed() const {return _stressLaw->soundSpeed();};

void dG3DMaterialLawWithTangentByPerturbation::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  dG3DIPVariableBase* ipvcur = NULL;
  const dG3DIPVariableBase* ipvprev = NULL;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {
    ipvcur = static_cast<dG3DIPVariableBase*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const dG3DIPVariableBase*>(ipvtmp2->getIPvBulk());
  }
  else
  {
    ipvcur = static_cast<dG3DIPVariableBase*>(ipv);
    ipvprev = static_cast<const dG3DIPVariableBase*>(ipvp);
  };
  int numNonLocalVars = ipvcur->getNumberNonLocalVariable();
  int numExtraDof = ipvcur->getNumConstitutiveExtraDofDiffusionVariable();

  int numCurlDof = ipvcur->getNumConstitutiveCurlVariable();

  _stressLaw->stress(ipvcur,ipvprev,false,checkfrac,dTangent);

#if 0
  _stressLaw->stress(ipvcur,ipvprev,true,checkfrac,dTangent);
  ipvcur->getConstRefToDeformationGradient().print("F Analytique"); // FLE
  ipvcur->getConstRefToFirstPiolaKirchhoffStress().print("P Analytique"); // FLE
  ipvcur->getConstRefToTangentModuli().print("dPdE Analytique");
  for (int k=0; k< numNonLocalVars; k++)
  {
    ipvcur->getConstRefToDLocalVariableDStrain()[k].print("dpdE Analyique");
  }
  /*if (this->getMacroSolver() != NULL)
  {
    if (this->getMacroSolver()!=NULL)
    {
      if (this->getMacroSolver()->withPathFollowing() and this->getMacroSolver()->getPathFollowingMethod() == pathFollowingManager::LOCAL_BASED){
        ipvcur->getConstRefToDIrreversibleEnergyDDeformationGradient().print("dEnedE Analytique");
      }
    }
  }*/
  for (int i=0; i< numNonLocalVars; i++)
  {
    for (int j=0; j< numNonLocalVars; j++)
    {
      Msg::Info("dpdp Analytique: %e",ipvcur->getConstRefToDLocalVariableDNonLocalVariable()(j,i));
    }
    ipvcur->getConstRefToDStressDNonLocalVariable()[i].print("dPdp Analytique");
    //Msg::Info("dEnedp Analytique: %e",ipvcur->getConstRefToDIrreversibleEnergyDNonLocalVariable(i));

    /*for (unsigned int j = 0; j < numCurlDof; ++j)
    {
        ipvcur->getConstRefTodVectorFielddNonLocalVariable()[j][i].print("dMagneticVectorField dp Analytique");
        ipvcur->getConstRefTodSourceVectorFielddNonLocalVariable()[j][i].print("dSourceVectorField dp Analytique");
        ipvcur->getConstRefTodMechanicalFieldSourcedNonLocalVariable()[j][i].print("dMechanicalFieldSource dp Analytique");
    }

    for (unsigned int j = 0; j < numExtraDof; ++j)
    {
        ipvcur->getRefTodFluxdNonLocalVariable()[j][i].print("dFlux dp Analytique");
        Msg::Info("d Field source dp Analytique: %e", ipvcur->getRefTodFieldSourcedNonLocalVariable()(j,i));
        Msg::Info("d Mechanical source dp Analytique: %e", ipvcur->getRefTodMechanicalSourcedNonLocalVariable()(j,i));
        Msg::Info("dEMFieldSource dp Analytique: %e",ipvcur->getConstRefTodEMFieldSourcedNonLocalVariable()(j,i));
        ipvcur->getConstRefTodElecDisplacementdNonLocalVariable()[j][i].print("dElecDisplacement dp Analytique");
    }*/
  }
  /*for (int i=0; i< numExtraDof; i++)
  {
    Msg::Info("Field Analytique: %e",ipvcur->getConstRefToField(i)); // FLE
    ipvcur->getConstRefToFlux()[i].print("Flux Analytique"); // FLE
    ipvcur->getConstRefToElectricDisplacement()[i].print("ElecDisplacement Analytique");
    Msg::Info("ThermSource Analytique: %e",ipvcur->getConstRefToFieldSource()(i)); // FLE
    Msg::Info("MechSource Analytique: %e",ipvcur->getConstRefToMechanicalSource()(i)); // FLE
    ipvcur->getConstRefTodFluxdF()[i].print("d Flux dF Analytique");
    ipvcur->getConstRefTodElecDisplacementdF()[i].print("dElecDisplacement dF Analytique");
    ipvcur->getConstRefTodFieldSourcedF()[i].print("d Field source dF Analytique");
    ipvcur->getConstRefTodMechanicalSourcedF()[i].print("d Mechanical source dF Analytique");
    ipvcur->getConstRefTodPdField()[i].print("dP d Field Analytique");
    ipvcur->getConstRefTodEMFieldSourcedF()[i].print("dEMFieldSource dF Analytique");


    for (int k=0; k< numExtraDof; k++)
    {
      ipvcur->getConstRefTodFluxdField()[k][i].print("dFludx dField Analytique");
      Msg::Info("d thermal source d field Analytique: %e",ipvcur->getConstRefTodFieldSourcedField()(k,i));
      Msg::Info("d mechanical source d field Analytique: %e",ipvcur->getRefTodMechanicalSourcedField()(k,i) );
      ipvcur->getConstRefTodFluxdGradField()[k][i].print("d Flux d GradField Analytique");
      ipvcur->getConstRefTodElecDisplacementdField()[k][i].print("dElecDisplacement dField Analytique");
      ipvcur->getConstRefTodElecDisplacementdGradField()[k][i].print("dElecDisplacement dGradField Analytique");
      Msg::Info("d thermal source d Grad field Analytique: %e",ipvcur->getConstRefTodFieldSourcedGradField()[k][i]);
      Msg::Info("d Mechanical source d Grad field Analytique: %e",ipvcur->getRefTodMechanicalSourcedGradField()[k][i]);
      Msg::Info("dEMFieldSource dField Analytique: %e",ipvcur->getConstRefTodEMFieldSourcedField()(k,i));
      ipvcur->getConstRefTodEMFieldSourcedGradField()[k][i].print("dEMFieldSource dGradField Analytique");
    }

    for (unsigned int k = 0; k < numCurlDof; ++k)
    {
        ipvcur->getConstRefTodFluxdVectorPotential()[i][k].print("dFlux dMagneticVectorPotential Analytique");
        ipvcur->getConstRefTodFluxdVectorCurl()[i][k].print("dFlux dMagneticVectorCurl Analytique");
        ipvcur->getConstRefTodFieldSourcedVectorPotential()[i][k].print("dFieldSource dMagneticVectorPotential Analytique");
        ipvcur->getConstRefTodFieldSourcedVectorCurl()[i][k].print("dFieldSource dMagneticVectorCurl Analytique");
        ipvcur->getConstRefTodMechanicalSourcedVectorPotential()[i][k].print("dMechanicalSource dMagneticVectorPotential Analytique");
        ipvcur->getConstRefTodMechanicalSourcedVectorCurl()[i][k].print("dMechanicalSource dMagneticVectorCurl Analytique");
        ipvcur->getRefTodEMFieldSourcedVectorPotential()[i][k].print("dEMFieldSource dMagneticVectorPotential Analytique");
        ipvcur->getRefTodEMFieldSourcedVectorCurl()[i][k].print("dEMFieldSource dMagneticVectorCurl Analytique");
        ipvcur->getConstRefTodElecDisplacementdVectorPotential()[i][k].print("dElecDisplacement dMagneticVectorPotential Analytique");
        ipvcur->getConstRefTodElecDisplacementdVectorCurl()[i][k].print("dElecDisplacement dMagneticVectorCurl Analytique");
    }
    for (unsigned int k = 0; k < numNonLocalVars; ++k)
    {
        Msg::Info("dp dExtraDofDiffusionField Analytique: %e",ipvcur->getRefTodLocalVariableDExtraDofDiffusionField()(k,i));
    }
  }
  for (unsigned int i = 0; i < numCurlDof; ++i)
  {
      ipvcur->getConstRefTodPdVectorPotential()[i].print("dPdMagneticVectorPotential Analytique");
      ipvcur->getConstRefTodPdVectorCurl()[i].print("dPdMagneticVectorCurl Analytique");
      ipvcur->getConstRefTodVectorFielddF()[i].print("dMagneticVectorField dF Analytique");
      ipvcur->getConstRefTodSourceVectorFielddF()[i].print("dSourceVectorField dF Analytique");
      ipvcur->getConstRefTodMechanicalFieldSourcedF()[i].print("dMechanicalFieldSource dF Analytique");

      for (unsigned int k = 0; k < numExtraDof; ++k)
      {
          ipvcur->getConstRefTodVectorFielddExtraDofField()[i][k].print("dMagneticVectorField dField Analytique");
          ipvcur->getConstRefTodVectorFielddGradExtraDofField()[i][k].print("dMagneticVectorField dGradField Analytique");
          ipvcur->getConstRefTodSourceVectorFielddExtraDofField()[i][k].print("dSourceVectorField dField Analytique");
          ipvcur->getConstRefTodSourceVectorFielddGradExtraDofField()[i][k].print("dSourceVectorField dGradField Analytique");
          ipvcur->getConstRefTodMechanicalFieldSourcedExtraDofField()[i][k].print("dMechanicalFieldSource dField Analytique");
          ipvcur->getConstRefTodMechanicalFieldSourcedGradExtraDofField()[i][k].print("dMechanicalFieldSource dGradField Analytique");
      }

      for (unsigned int k = 0; k < numCurlDof; ++k)
      {
          ipvcur->getConstRefTodVectorFielddVectorPotential()[k][i].print("dMagneticVectorField dMagneticVectorPotential Analytique");
          ipvcur->getConstRefTodSourceVectorFielddVectorPotential()[k][i].print("dSourceVectorField dMagneticVectorPotential Analytique");
          ipvcur->getConstRefTodMechanicalFieldSourcedVectorPotential()[k][i].print("dMechanicalFieldSource dMagneticVectorPotential Analytique");
          ipvcur->getConstRefTodVectorFielddVectorCurl()[k][i].print("dMagneticVectorField dMagneticVectorCurl Analytique");
          ipvcur->getConstRefTodSourceVectorFielddVectorCurl()[k][i].print("dSourceVectorField dMagneticVectorCurl Analytique");
          ipvcur->getConstRefTodMechanicalFieldSourcedVectorCurl()[k][i].print("dMechanicalFieldSource dMagneticVectorCurl Analytique");
      }

      for (unsigned int k = 0; k < numNonLocalVars; ++k)
      {
          ipvcur->getRefTodLocalVariabledVectorPotential()[k][i].print("dp dMagneticVectorPotential Analytique");
          ipvcur->getRefTodLocalVariabledVectorCurl()[k][i].print("dp dMagneticVectorCurl Analytique");
      }
  }*/

#endif
  if (stiff)
  {
    const STensor3& P =   ipvcur->getConstRefToFirstPiolaKirchhoffStress();
    // pert on deformation gradinet
    if (ipvTemp == NULL)
    {
      ipvTemp = static_cast<dG3DIPVariableBase*>(ipvcur->clone());
    }
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        ipvTemp->operator =(*static_cast<const IPVariable*>(ipvcur));
        ipvTemp->getRefToDeformationGradient()(i,j) += _pertFactor;
        _stressLaw->stress(ipvTemp,ipvprev,false,checkfrac,dTangent);
        const STensor3& Pplus = ipvTemp->getConstRefToFirstPiolaKirchhoffStress();
        // stress tangent
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            ipvcur->getRefToTangentModuli()(k,l,i,j) = (Pplus(k,l) - P(k,l))/(_pertFactor);
          }
        }
        // stress -nonlocal
        for (int k=0; k< numNonLocalVars; k++)
        {
          ipvcur->getRefToDLocalVariableDStrain()[k](i,j) = (ipvTemp->getConstRefToLocalVariable(k) - ipvcur->getConstRefToLocalVariable(k))/_pertFactor;
        }
        for (int k=0; k< numExtraDof; k++)
        {
          for (int l=0; l<3; l++){
            ipvcur->getRefTodFluxdF()[k](l,i,j) = (ipvTemp->getConstRefToFlux()[k](l) - ipvcur->getConstRefToFlux()[k](l))/_pertFactor;
          }
          ipvcur->getRefTodFieldSourcedF()[k](i,j) =(ipvTemp->getConstRefToFieldSource()(k)- ipvcur->getConstRefToFieldSource()(k))/_pertFactor;
          ipvcur->getRefTodMechanicalSourcedF()[k](i,j) =(ipvTemp->getConstRefToMechanicalSource()(k)- ipvcur->getConstRefToMechanicalSource()(k))/_pertFactor;
          ipvcur->getRefTodEMFieldSourcedF()[k](i,j) = (ipvTemp->getConstRefToEMFieldSource()(k) - ipvcur->getConstRefToEMFieldSource()(k))/_pertFactor;
        }
        for (unsigned int k = 0; k < numCurlDof; ++k)
        {
            for (int l=0; l<3; l++)
            {
                ipvcur->getRefTodVectorFielddF()[k](l,i,j) = (ipvTemp->getConstRefToVectorField(k)[l] - ipvcur->getConstRefToVectorField(k)[l])/_pertFactor;
                ipvcur->getRefTodSourceVectorFielddF()[k](l,i,j) = (ipvTemp->getConstRefToSourceVectorField(k)[l]- ipvcur->getConstRefToSourceVectorField(k)[l])/_pertFactor;
                ipvcur->getRefTodMechanicalFieldSourcedF()[k](l,i,j) = (ipvTemp->getConstRefToMechanicalFieldSource(k)[l]- ipvcur->getConstRefToMechanicalFieldSource(k)[l])/_pertFactor;
            }
        }
        // energy
        if (this->getMacroSolver() != NULL)
        {
          if (this->getMacroSolver()->withPathFollowing() and this->getMacroSolver()->getPathFollowingMethod() == pathFollowingManager::LOCAL_BASED)
          {
            ipvcur->getRefToDIrreversibleEnergyDDeformationGradient()(i,j) = (ipvTemp->irreversibleEnergy() - ipvcur->irreversibleEnergy())/_pertFactor;
          }
        }
      }
    }
    // pert on nonlocal Variables
    for (int i=0; i< numNonLocalVars; i++)
    {
      ipvTemp->operator =(*static_cast<const IPVariable*>(ipvcur));
      ipvTemp->getRefToNonLocalVariableInMaterialLaw(i) += _pertFactor;
      _stressLaw->stress(ipvTemp,ipvprev,false,checkfrac,dTangent);
      const STensor3& Pplus = ipvTemp->getConstRefToFirstPiolaKirchhoffStress();
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
          ipvcur->getRefToDStressDNonLocalVariable()[i](k,l) = (Pplus(k,l) - P(k,l))/(_pertFactor);
        }
      }
      for (int j=0; j< numNonLocalVars; j++)
      {
        ipvcur->getRefToDLocalVariableDNonLocalVariable()(j,i) = (ipvTemp->getConstRefToLocalVariable(j) - ipvcur->getConstRefToLocalVariable(j))/_pertFactor;
      }
      // energy
      if (this->getMacroSolver() != NULL)
      {
        if (this->getMacroSolver()->withPathFollowing() and this->getMacroSolver()->getPathFollowingMethod() == pathFollowingManager::LOCAL_BASED){
          ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(i) = (ipvTemp->irreversibleEnergy() - ipvcur->irreversibleEnergy())/_pertFactor;
        }
      }
      for (int k=0; k< numExtraDof; k++)
      {
        for (int l=0; l< 3; l++)
        {
          ipvcur->getRefTodFluxdNonLocalVariable()[k][i](l) = (ipvTemp->getConstRefToFlux()[k](l) - ipvcur->getConstRefToFlux()[k](l))/_pertFactor;
        }
        ipvcur->getRefTodFieldSourcedNonLocalVariable()(k,i) = (ipvTemp->getConstRefToFieldSource()(k)- ipvcur->getConstRefToFieldSource()(k))/_pertFactor;
        ipvcur->getRefTodMechanicalSourcedNonLocalVariable()(k,i) = (ipvTemp->getConstRefToMechanicalSource()(k)- ipvcur->getConstRefToMechanicalSource()(k))/_pertFactor;
        ipvcur->getRefTodEMFieldSourcedNonLocalVariable()(k,i) = (ipvTemp->getConstRefToEMFieldSource()(k) - ipvcur->getConstRefToEMFieldSource()(k))/_pertFactor;
      }

      for (unsigned int k = 0; k < numCurlDof; ++k)
      {
          for (unsigned int l = 0; l < 3; ++l)
          {
              ipvcur->getRefTodVectorFielddNonLocalVariable()[k][i](l) = (ipvTemp->getConstRefToVectorField(k)[l] - ipvcur->getConstRefToVectorField(k)[l])/_pertFactor;
              ipvcur->getRefTodSourceVectorFielddNonLocalVariable()[k][i](l) = (ipvTemp->getConstRefToSourceVectorField(k)[l] - ipvcur->getConstRefToSourceVectorField(k)[l])/_pertFactor;
              ipvcur->getRefTodMechanicalFieldSourcedNonLocalVariable()[k][i](l) = (ipvTemp->getConstRefToMechanicalFieldSource(k)[l] - ipvcur->getConstRefToMechanicalFieldSource(k)[l])/_pertFactor;
          }
      }
    }
    // pert on extraDof Variables
    for (int i=0; i< numExtraDof; i++)
    {
      ipvTemp->operator =(*static_cast<const IPVariable*>(ipvcur));
      ipvTemp->getRefToField(i) +=_pertFactor;
      _stressLaw->stress(ipvTemp,ipvprev,false,checkfrac,dTangent);
      const STensor3& Pplus = ipvTemp->getConstRefToFirstPiolaKirchhoffStress();
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
          ipvcur->getRefTodPdField()[i](k,l) = (Pplus(k,l) - P(k,l))/(_pertFactor);
        }
      }
      for (int k=0; k< numExtraDof; k++)
      {
        for (int l=0; l<3; l++){
          ipvcur->getRefTodFluxdField()[k][i](l) = (ipvTemp->getConstRefToFlux()[k](l) - ipvcur->getConstRefToFlux()[k](l))/_pertFactor;
        }
        ipvcur->getRefTodFieldSourcedField()(k,i) = (ipvTemp->getConstRefToFieldSource()(k)- ipvcur->getConstRefToFieldSource()(k))/_pertFactor;
        ipvcur->getRefTodMechanicalSourcedField()(k,i) = (ipvTemp->getConstRefToMechanicalSource()(k)- ipvcur->getConstRefToMechanicalSource()(k))/_pertFactor;
        ipvcur->getRefTodEMFieldSourcedField()(k,i) = (ipvTemp->getConstRefToEMFieldSource()(k) - ipvcur->getConstRefToEMFieldSource()(k))/_pertFactor;
      }
      for (int j=0; j< numNonLocalVars; j++)
      {
        ipvcur->getRefTodLocalVariableDExtraDofDiffusionField()(j,i) = (ipvTemp->getConstRefToLocalVariable(j) - ipvcur->getConstRefToLocalVariable(j))/_pertFactor;
      }
      for (unsigned int m = 0; m < numCurlDof; ++m)
      {
          for (unsigned int n = 0; n < 3; ++n)
          {
              ipvcur->getRefTodVectorFielddExtraDofField()[m][i](n) = (ipvTemp->getConstRefToVectorField(m)[n] - ipvcur->getConstRefToVectorField(m)[n])/_pertFactor;
              ipvcur->getRefTodSourceVectorFielddExtraDofField()[m][i](n) = (ipvTemp->getConstRefToSourceVectorField(m)[n] - ipvcur->getConstRefToSourceVectorField(m)[n])/_pertFactor;
              ipvcur->getRefTodMechanicalFieldSourcedExtraDofField()[m][i](n) = (ipvTemp->getConstRefToMechanicalFieldSource(m)[n] - ipvcur->getConstRefToMechanicalFieldSource(m)[n])/_pertFactor;
          }
      }
      for (int j=0; j<3; j++){
        ipvTemp->operator =(*static_cast<const IPVariable*>(ipvcur));
        ipvTemp->getRefToGradField()[i](j) +=_pertFactor;
        _stressLaw->stress(ipvTemp,ipvprev,false,checkfrac,dTangent);
        const STensor3& Pplus = ipvTemp->getConstRefToFirstPiolaKirchhoffStress();
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            ipvcur->getRefTodPdGradField()[i](k,l,j) = (Pplus(k,l) - P(k,l))/(_pertFactor);
          }
        }
        for (int k=0; k< numExtraDof; k++)
        {
          for (int l=0; l<3; l++){
            ipvcur->getRefTodFluxdGradField()[k][i](l,j) = (ipvTemp->getConstRefToFlux()[k](l) - ipvcur->getConstRefToFlux()[k](l))/_pertFactor;
          }
          ipvcur->getRefTodFieldSourcedGradField()[k][i](j) = (ipvTemp->getConstRefToFieldSource()(k)- ipvcur->getConstRefToFieldSource()(k))/_pertFactor;
          ipvcur->getRefTodMechanicalSourcedGradField()[k][i](j) = (ipvTemp->getConstRefToMechanicalSource()(k)- ipvcur->getConstRefToMechanicalSource()(k))/_pertFactor;
          ipvcur->getRefTodEMFieldSourcedGradField()[k][i](j) = (ipvTemp->getConstRefToEMFieldSource()(k) - ipvcur->getConstRefToEMFieldSource()(k))/_pertFactor;
        }
        for (unsigned int k = 0; k < numCurlDof; ++k)
        {
            for (unsigned int l = 0; l < 3; ++l)
            {
                ipvcur->getRefTodVectorFielddGradExtraDofField()[k][i](l,j) = (ipvTemp->getConstRefToVectorField(k)[l] - ipvcur->getConstRefToVectorField(k)[l])/_pertFactor;
                ipvcur->getRefTodSourceVectorFielddGradExtraDofField()[k][i](l,j) = (ipvTemp->getConstRefToSourceVectorField(k)[l] - ipvcur->getConstRefToSourceVectorField(k)[l])/_pertFactor;
                ipvcur->getRefTodMechanicalFieldSourcedGradExtraDofField()[k][i](l,j) = (ipvTemp->getConstRefToMechanicalFieldSource(k)[l] - ipvcur->getConstRefToMechanicalFieldSource(k)[l])/_pertFactor;
            }
        }
      }
      //here do we need getRefTodFluxdGradFielddF()?
    }
    // pert on CurlDof Variables
    for (unsigned int i = 0; i < numCurlDof; ++i)
    {
        for (int m = 0; m < 3; ++m)
        {
            ipvTemp->operator =(*static_cast<const IPVariable*>(ipvcur));
            ipvTemp->getRefToVectorPotential(i)[m] += _pertFactor;
            _stressLaw->stress(ipvTemp, ipvprev, false, checkfrac, dTangent);
            const STensor3 &Pplus = ipvTemp->getConstRefToFirstPiolaKirchhoffStress();
            for (int k = 0; k < 3; k++)
            {
                for (int l = 0; l < 3; l++)
                {
                    ipvcur->getRefTodPdVectorPotential()[i](k, l, m) = (Pplus(k, l) - P(k, l)) / (_pertFactor);
                }
            }

            for (unsigned int k = 0; k < numCurlDof; ++k)
            {
                for (unsigned int l = 0; l < 3; ++l)
                {
                    ipvcur->getRefTodVectorFielddVectorPotential()[k][i](l, m) = (ipvTemp->getConstRefToVectorField(k)[l] - ipvcur->getConstRefToVectorField(k)[l])/_pertFactor;
                    ipvcur->getRefTodSourceVectorFielddVectorPotential()[k][i](l, m) = (ipvTemp->getConstRefToSourceVectorField(k)[l] - ipvcur->getConstRefToSourceVectorField(k)[l])/_pertFactor;
                    ipvcur->getRefTodMechanicalFieldSourcedVectorPotential()[k][i](l,m) = (ipvTemp->getConstRefToMechanicalFieldSource(k)(l) - ipvcur->getConstRefToMechanicalFieldSource(k)(l))/ _pertFactor;
                }
            }

            for (unsigned int k = 0; k < numExtraDof; ++k)
            {
                for (unsigned int n = 0; n < 3; ++n)
                {
                    ipvcur->getRefTodFluxdVectorPotential()[k][i](n, m) = (ipvTemp->getConstRefToFlux()[k](n) - ipvcur->getConstRefToFlux()[k](n))/_pertFactor;
                }
                ipvcur->getRefTodFieldSourcedVectorPotential()[k][i](m) = (ipvTemp->getConstRefToFieldSource()(k) - ipvcur->getConstRefToFieldSource()(k))/_pertFactor;
                ipvcur->getRefTodMechanicalSourcedVectorPotential()[k][i](m) = (ipvTemp->getConstRefToMechanicalSource()(k) - ipvcur->getConstRefToMechanicalSource()(k))/_pertFactor;
                ipvcur->getRefTodEMFieldSourcedVectorPotential()[k][i](m) = (ipvTemp->getConstRefToEMFieldSource()(k) - ipvcur->getConstRefToEMFieldSource()(k))/_pertFactor;
            }

            for (unsigned int k = 0; k < numNonLocalVars; ++k)
            {
                ipvcur->getRefTodLocalVariabledVectorPotential()[k][i](m) = (ipvTemp->getConstRefToLocalVariable(k) - ipvcur->getConstRefToLocalVariable(k))/_pertFactor;
            }
        }

        // Perturb on Curl of Field
        for (int m = 0; m < 3; ++m)
        {
            ipvTemp->operator =(*static_cast<const IPVariable*>(ipvcur));
            ipvTemp->getRefToVectorCurl(i)[m] +=_pertFactor;
            _stressLaw->stress(ipvTemp,ipvprev,false,checkfrac,dTangent);
            const STensor3& Pplus = ipvTemp->getConstRefToFirstPiolaKirchhoffStress();
            for (int k=0; k<3; k++)
            {
                for (int l=0; l<3; l++)
                {
                        ipvcur->getRefTodPdVectorCurl()[i](k,l,m) = (Pplus(k,l) - P(k,l)) / (_pertFactor);
                }
            }

            for (unsigned int k = 0; k < numCurlDof; ++k)
            {
                for (unsigned int l = 0; l < 3; ++l)
                    {
                        ipvcur->getRefTodVectorFielddVectorCurl()[k][i](l,m) = (ipvTemp->getConstRefToVectorField(k)[l] - ipvcur->getConstRefToVectorField(k)[l])/ _pertFactor;
                        ipvcur->getRefTodSourceVectorFielddVectorCurl()[k][i](l,m) = (ipvTemp->getConstRefToSourceVectorField(k)[l] - ipvcur->getConstRefToSourceVectorField(k)[l])/ _pertFactor;
                        ipvcur->getRefTodMechanicalFieldSourcedVectorCurl()[k][i](l,m) = (ipvTemp->getConstRefToMechanicalFieldSource(k)(l) - ipvcur->getConstRefToMechanicalFieldSource(k)(l))/ _pertFactor;
                    }
            }

            for (unsigned int k = 0; k < numExtraDof; ++k)
            {
                for (unsigned int n = 0; n < 3; ++n)
                {
                        ipvcur->getRefTodFluxdVectorCurl()[k][i](n,m) = (ipvTemp->getConstRefToFlux()[k](n) - ipvcur->getConstRefToFlux()[k](n))/_pertFactor;
                }
                ipvcur->getRefTodFieldSourcedVectorCurl()[k][i](m) = (ipvTemp->getConstRefToFieldSource()(k) - ipvcur->getConstRefToFieldSource()(k))/_pertFactor;
                ipvcur->getRefTodMechanicalSourcedVectorCurl()[k][i](m) = (ipvTemp->getConstRefToMechanicalSource()(k) - ipvcur->getConstRefToMechanicalSource()(k))/_pertFactor;
                ipvcur->getRefTodEMFieldSourcedVectorCurl()[k][i](m) = (ipvTemp->getConstRefToEMFieldSource()(k) - ipvcur->getConstRefToEMFieldSource()(k))/_pertFactor;
            }

            for (unsigned int k = 0; k < numNonLocalVars; ++k)
            {
                ipvcur->getRefTodLocalVariabledVectorCurl()[k][i](m) = (ipvTemp->getConstRefToLocalVariable(k) - ipvcur->getConstRefToLocalVariable(k))/_pertFactor;
            }
        }

    }
#if 0
    ipvcur->getConstRefToDeformationGradient().print("F Numerique"); // FLE
    ipvcur->getConstRefToFirstPiolaKirchhoffStress().print("P Numerique"); // FLE
    ipvcur->getConstRefToTangentModuli().print("dPdE Numerique");
    for (int k=0; k< numNonLocalVars; k++)
    {
      ipvcur->getConstRefToDLocalVariableDStrain()[k].print("dpdE Numerique");
    }
    /*if (this->getMacroSolver() != NULL)
    {
      if (this->getMacroSolver()->withPathFollowing() and this->getMacroSolver()->getPathFollowingMethod() ==  pathFollowingManager::LOCAL_BASED){
        ipvcur->getConstRefToDIrreversibleEnergyDDeformationGradient().print("dEnedE Numerique");
      }
    }*/
    for (int i=0; i< numNonLocalVars; i++)
    {
      for (int j=0; j< numNonLocalVars; j++)
      {
        Msg::Info("dpdp Numerique: %e",ipvcur->getConstRefToDLocalVariableDNonLocalVariable()(j,i));
      }
      ipvcur->getConstRefToDStressDNonLocalVariable()[i].print("dPdp Numerique");
      //Msg::Info("dEnedp Numerique: %e",ipvcur->getConstRefToDIrreversibleEnergyDNonLocalVariable(i));

        /*const unsigned int extradof_T = 0; // Thermal field EM source
        for (unsigned int j = 0; j < numCurlDof; ++j)
        {
            ipvcur->getConstRefTodVectorFielddNonLocalVariable()[j][i].print("dMagneticVectorField dp Numerique");
            ipvcur->getConstRefTodSourceVectorFielddNonLocalVariable()[j][i].print("dSourceVectorField dp Numerique");
            ipvcur->getConstRefTodMechanicalFieldSourcedNonLocalVariable()[j][i].print("dMechanicalFieldSource dp Numerique");
        }

        for (unsigned int j = 0; j < numExtraDof; ++j)
        {
            ipvcur->getRefTodFluxdNonLocalVariable()[j][i].print("dFlux dp Numerique");
            Msg::Info("d Field source dp Numerique: %e", ipvcur->getRefTodFieldSourcedNonLocalVariable()(j,i));
            Msg::Info("d Mechanical source dp Numerique: %e", ipvcur->getRefTodMechanicalSourcedNonLocalVariable()(j,i));
            Msg::Info("dEMFieldSource dp Numerique: %e",ipvcur->getConstRefTodEMFieldSourcedNonLocalVariable()(j,i));
            ipvcur->getConstRefTodElecDisplacementdNonLocalVariable()[j][i].print("dElecDisplacement dp Numerique");
        }*/
    }
    /*for (int i=0; i< numExtraDof; i++)
    {
      Msg::Info("Field Numerique: %e",ipvcur->getConstRefToField(i)); // FLE
      ipvcur->getConstRefToFlux()[i].print("Flux Numerique"); // FLE
      ipvcur->getConstRefToElectricDisplacement()[i].print("ElecDisplacement Numerique");
      Msg::Info("ThermSource Numerique: %e",ipvcur->getConstRefToFieldSource()(i)); // FLE
      Msg::Info("MechSource Numerique: %e",ipvcur->getConstRefToMechanicalSource()(i)); // FLE
      ipvcur->getConstRefTodFluxdF()[i].print("d Flux dF Numerique");
      ipvcur->getConstRefTodElecDisplacementdF()[i].print("dElecDisplacement dF Numerique");
      ipvcur->getConstRefTodFieldSourcedF()[i].print("d Field source dF Numerique");
      ipvcur->getConstRefTodMechanicalSourcedF()[i].print("d Mechanical source dF Numerique");
      ipvcur->getConstRefTodPdField()[i].print("dP d Field Numerique");
      ipvcur->getConstRefTodEMFieldSourcedF()[i].print("dEMFieldSource dF Numerique");

      for (int k=0; k< numExtraDof; k++)
      {
        ipvcur->getConstRefTodFluxdField()[k][i].print("dFludx dField Numerique");
        Msg::Info("d thermal source d field Numerique: %e",ipvcur->getConstRefTodFieldSourcedField()(k,i));
        Msg::Info("d mechanical source d field Numerique: %e",ipvcur->getRefTodMechanicalSourcedField()(k,i) );
        ipvcur->getConstRefTodFluxdGradField()[k][i].print("d Flux d GradField Numerique");
        ipvcur->getConstRefTodElecDisplacementdField()[k][i].print("dElecDisplacement dField Numerique");
        ipvcur->getConstRefTodElecDisplacementdGradField()[k][i].print("dElecDisplacement dGradField Numerique");
        Msg::Info("d thermal source d Grad field Numerique: %e",ipvcur->getConstRefTodFieldSourcedGradField()[k][i]);
        Msg::Info("d Mechanical source d Grad field Numerique: %e",ipvcur->getRefTodMechanicalSourcedGradField()[k][i]);
        Msg::Info("dEMFieldSource dField Numerique: %e",ipvcur->getConstRefTodEMFieldSourcedField()(k,i));
        ipvcur->getConstRefTodEMFieldSourcedGradField()[k][i].print("dEMFieldSource dGradField Numerique");
      }

      for (unsigned int k = 0; k < numCurlDof; ++k)
      {
          ipvcur->getConstRefTodFluxdVectorPotential()[i][k].print("dFlux dMagneticVectorPotential Numerique");
          ipvcur->getConstRefTodFluxdVectorCurl()[i][k].print("dFlux dMagneticVectorCurl Numerique");
          ipvcur->getConstRefTodFieldSourcedVectorPotential()[i][k].print("dFieldSource dMagneticVectorPotential Numerique");
          ipvcur->getConstRefTodFieldSourcedVectorCurl()[i][k].print("dFieldSource dMagneticVectorCurl Numerique");
          ipvcur->getConstRefTodMechanicalSourcedVectorPotential()[i][k].print("dMechanicalSource dMagneticVectorPotential Numerique");
          ipvcur->getConstRefTodMechanicalSourcedVectorCurl()[i][k].print("dMechanicalSource dMagneticVectorCurl Numerique");
          ipvcur->getRefTodEMFieldSourcedVectorPotential()[i][k].print("dEMFieldSource dMagneticVectorPotential Numerique");
          ipvcur->getRefTodEMFieldSourcedVectorCurl()[i][k].print("dEMFieldSource dMagneticVectorCurl Numerique");
          ipvcur->getConstRefTodElecDisplacementdVectorPotential()[i][k].print("dElecDisplacement dMagneticVectorPotential Numerique");
          ipvcur->getConstRefTodElecDisplacementdVectorCurl()[i][k].print("dElecDisplacement dMagneticVectorCurl Numerique");
      }
        for (unsigned int k = 0; k < numNonLocalVars; ++k)
        {
            Msg::Info("dp dExtraDofDiffusionField Numerique: %e",ipvcur->getRefTodLocalVariableDExtraDofDiffusionField()(k,i));
        }
    }
    for (unsigned int i = 0; i < numCurlDof; ++i)
    {
      const unsigned int extradof_T = 0; // Thermal field EM source
      ipvcur->getConstRefTodPdVectorPotential()[i].print("dPdMagneticVectorPotential Numerique");
      ipvcur->getConstRefTodPdVectorCurl()[i].print("dPdMagneticVectorCurl Numerique");
      ipvcur->getConstRefTodVectorFielddF()[i].print("dMagneticVectorField dF Numerique");
      ipvcur->getConstRefTodSourceVectorFielddF()[i].print("dSourceVectorField dF Numerique");
      ipvcur->getConstRefTodMechanicalFieldSourcedF()[i].print("dMechanicalFieldSource dF Numerique");

      for (unsigned int k = 0; k < numExtraDof; ++k)
      {
          ipvcur->getConstRefTodVectorFielddExtraDofField()[i][k].print("dMagneticVectorField dField Numerique");
          ipvcur->getConstRefTodVectorFielddGradExtraDofField()[i][k].print("dMagneticVectorField dGradField Numerique");
          ipvcur->getConstRefTodSourceVectorFielddExtraDofField()[i][k].print("dSourceVectorField dField Numerique");
          ipvcur->getConstRefTodSourceVectorFielddGradExtraDofField()[i][k].print("dSourceVectorField dGradField Numerique");
          ipvcur->getConstRefTodMechanicalFieldSourcedExtraDofField()[i][k].print("dMechanicalFieldSource dField Numerique");
          ipvcur->getConstRefTodMechanicalFieldSourcedGradExtraDofField()[i][k].print("dMechanicalFieldSource dGradField Numerique");
      }

      for (unsigned int k = 0; k < numCurlDof; ++k)
      {
          ipvcur->getConstRefTodVectorFielddVectorPotential()[k][i].print("dMagneticVectorField dMagneticVectorPotential Numerique");
          ipvcur->getConstRefTodSourceVectorFielddVectorPotential()[k][i].print("dSourceVectorField dMagneticVectorPotential Numerique");
          ipvcur->getConstRefTodMechanicalFieldSourcedVectorPotential()[k][i].print("dMechanicalFieldSource dMagneticVectorPotential Numerique");
          ipvcur->getConstRefTodVectorFielddVectorCurl()[k][i].print("dMagneticVectorField dMagneticVectorCurl Numerique");
          ipvcur->getConstRefTodSourceVectorFielddVectorCurl()[k][i].print("dSourceVectorField dMagneticVectorCurl Numerique");
          ipvcur->getConstRefTodMechanicalFieldSourcedVectorCurl()[k][i].print("dMechanicalFieldSource dMagneticVectorCurl Numerique");
      }

        for (unsigned int k = 0; k < numNonLocalVars; ++k)
        {
            ipvcur->getRefTodLocalVariabledVectorPotential()[k][i].print("dp dMagneticVectorPotential Numerique");
            ipvcur->getRefTodLocalVariabledVectorCurl()[k][i].print("dp dMagneticVectorCurl Numerique");
        }
    }*/
#endif
  }
}

void dG3DMaterialLawWithTangentByPerturbation::fillElasticStiffness(double E, double nu, STensor43 &tangent) const
{
  _stressLaw->fillElasticStiffness(E,nu,tangent);
}

void dG3DMaterialLawWithTangentByPerturbation::setElasticStiffness(IPVariable* ipv) const
{
  _stressLaw->setElasticStiffness(ipv);
};

bool dG3DMaterialLawWithTangentByPerturbation::withEnergyDissipation() const
{
  return _stressLaw->withEnergyDissipation();
}
// function to define when one IP is broken for block dissipation
bool dG3DMaterialLawWithTangentByPerturbation::brokenCheck(const IPVariable* ipv) const
{
  return _stressLaw->brokenCheck(ipv);
}
void dG3DMaterialLawWithTangentByPerturbation::checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const
{
  _stressLaw->checkInternalState(ipv,ipvprev);
}

void dG3DMaterialLawWithTangentByPerturbation::setTime(const double ctime,const double dtime)
{
  dG3DMaterialLaw::setTime(ctime,dtime);
  _stressLaw->setTime(ctime,dtime);
}
materialLaw::matname dG3DMaterialLawWithTangentByPerturbation::getType() const
{
  return _stressLaw->getType();
}

double dG3DMaterialLawWithTangentByPerturbation::scaleFactor() const { return _stressLaw->scaleFactor();};
bool dG3DMaterialLawWithTangentByPerturbation::isHighOrder() const { return _stressLaw->isHighOrder();};
bool dG3DMaterialLawWithTangentByPerturbation::isNumeric() const {return _stressLaw->isNumeric();};
int dG3DMaterialLawWithTangentByPerturbation::getNumberOfExtraDofsDiffusion() const {return _stressLaw->getNumberOfExtraDofsDiffusion();}
double dG3DMaterialLawWithTangentByPerturbation::getInitialExtraDofStoredEnergyPerUnitField() const {return _stressLaw->getInitialExtraDofStoredEnergyPerUnitField();}
double dG3DMaterialLawWithTangentByPerturbation::getExtraDofStoredEnergyPerUnitField(double T) const {return _stressLaw->getExtraDofStoredEnergyPerUnitField(T);}
double dG3DMaterialLawWithTangentByPerturbation::getCharacteristicLength() const {return _stressLaw->getCharacteristicLength();};
void dG3DMaterialLawWithTangentByPerturbation::setMacroSolver(const nonLinearMechSolver* sv)
{
  dG3DMaterialLaw::setMacroSolver(sv);
  _stressLaw->setMacroSolver(sv);
};

void dG3DMaterialLawWithTangentByPerturbation::constitutive(const STensor3& F0,         // initial deformation gradient (input @ time n)
                              const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                              STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                          // contains the initial values on input
                              const IPVariable *q0,       // array of initial internal variable
                              IPVariable *q1,              // updated array of internal variable (in ipvcur on output),
                              STensor43 &Tangent,         // constitutive tangents (output)
                              const bool stiff,
                              STensor43* elasticTangent,
                              const bool dTangent ,
                              STensor63* dCalgdeps ) const
{
  _stressLaw->constitutive(F0,Fn,P,q0,q1,Tangent,stiff,elasticTangent,dTangent,dCalgdeps);
}

void dG3DMaterialLawWithTangentByPerturbation::setUseBarF(bool useBarF)
{
  dG3DMaterialLaw::setUseBarF(useBarF);
  _stressLaw->setUseBarF(useBarF);
}


const materialLaw *dG3DMaterialLawWithTangentByPerturbation::getConstNonLinearSolverMaterialLaw() const
{
  return _stressLaw->getConstNonLinearSolverMaterialLaw();
}
materialLaw *dG3DMaterialLawWithTangentByPerturbation::getNonLinearSolverMaterialLaw()
{
  return _stressLaw->getNonLinearSolverMaterialLaw();
};


dG3DLinearElasticMaterialLaw::dG3DLinearElasticMaterialLaw(const int num, const double rho, const double E, const double nu):
	dG3DMaterialLaw(num,rho,false), _elasticLaw(num,E,nu){
}
dG3DLinearElasticMaterialLaw::dG3DLinearElasticMaterialLaw(const dG3DLinearElasticMaterialLaw& src):dG3DMaterialLaw(src),
_elasticLaw(src._elasticLaw){
};


void dG3DLinearElasticMaterialLaw::initLaws(const std::map<int,materialLaw*> &maplaw){
	if (!_initialized){
		elasticStiffness = _elasticLaw.getElasticTensor();
		_initialized = true;
	};
};

void dG3DLinearElasticMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  IPVariable* ipvi = new  LinearElasticDG3DIPVariable(hasBodyForce,inter);
  IPVariable* ipv1 = new  LinearElasticDG3DIPVariable(hasBodyForce,inter);
  IPVariable* ipv2 = new  LinearElasticDG3DIPVariable(hasBodyForce,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void dG3DLinearElasticMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  ipv = new  LinearElasticDG3DIPVariable(hasBodyForce,inter);
}


void dG3DLinearElasticMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  /* get ipvariable */
  dG3DIPVariableBase* ipvcur = static_cast<dG3DIPVariableBase*>(ipv);;
  const dG3DIPVariableBase* ipvprev = static_cast<const dG3DIPVariableBase*>(ipvp);;
  /* strain xyz */
  const STensor3& F = ipvcur->getConstRefToDeformationGradient();
  STensor3& P = ipvcur->getRefToFirstPiolaKirchhoffStress();
	STensor43& H = ipvcur->getRefToTangentModuli();
	_elasticLaw.constitutive(F,P,stiff,&H);
  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);
}

double dG3DLinearElasticMaterialLaw::scaleFactor() const{
	return _elasticLaw.getYoungModulus();
};

double dG3DLinearElasticMaterialLaw::soundSpeed() const
{
	double E = _elasticLaw.getYoungModulus();
	double nu = _elasticLaw.getPoissonRatio();

  double factornu = (1.-nu)/((1.+nu)*(1.-2.*nu));
  return sqrt(E*factornu/_rho);
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
dG3DLinearElasticAnisotropicMaterialLaw::dG3DLinearElasticAnisotropicMaterialLaw(const int num, const double rho,
		                                      const double Ex, const double Ey, const double Ez,
                                                      const double Vxy, const double Vxz, const double Vyz,
                                                      const double MUxy, const double MUxz, const double MUyz,
                                                      const double alpha, const double beta, const double gamma):
	dG3DMaterialLaw(num,rho,false), _elasticLaw(num,Ex,Ey,Ez,Vxy,Vxz,Vyz,MUxy,MUxz,MUyz,alpha,beta,gamma){
}
dG3DLinearElasticAnisotropicMaterialLaw::dG3DLinearElasticAnisotropicMaterialLaw(const dG3DLinearElasticAnisotropicMaterialLaw& src):dG3DMaterialLaw(src),
_elasticLaw(src._elasticLaw){
};

void dG3DLinearElasticAnisotropicMaterialLaw::initLaws(const std::map<int,materialLaw*> &maplaw){
	if (!_initialized){
		elasticStiffness = _elasticLaw.getElasticTensor();
		_initialized = true;
	};
};

void dG3DLinearElasticAnisotropicMaterialLaw::createIPState(IPStateBase* &ips,bool hasBodyForce,const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  IPVariable* ipvi = new  LinearElasticDG3DIPVariable(hasBodyForce,inter);
  IPVariable* ipv1 = new  LinearElasticDG3DIPVariable(hasBodyForce,inter);
  IPVariable* ipv2 = new  LinearElasticDG3DIPVariable(hasBodyForce,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void dG3DLinearElasticAnisotropicMaterialLaw::createIPVariable(IPVariable* &ipv,bool hasBodyForce,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  ipv = new  LinearElasticDG3DIPVariable(hasBodyForce,inter);
}

void dG3DLinearElasticAnisotropicMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  /* get ipvariable */
  dG3DIPVariableBase* ipvcur = static_cast<dG3DIPVariableBase*>(ipv);;
  const dG3DIPVariableBase* ipvprev = static_cast<const dG3DIPVariableBase*>(ipvp);;
  /* strain xyz */
  const STensor3& F = ipvcur->getConstRefToDeformationGradient();
  STensor3& P = ipvcur->getRefToFirstPiolaKirchhoffStress();
	STensor43& H = ipvcur->getRefToTangentModuli();
	_elasticLaw.constitutive(F,P,stiff,&H);
  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);
}

double dG3DLinearElasticAnisotropicMaterialLaw::scaleFactor() const{
	return _elasticLaw.getYoungModulus();
};

double dG3DLinearElasticAnisotropicMaterialLaw::soundSpeed() const
{
	double E = _elasticLaw.getYoungModulus();
	double nu = _elasticLaw.getPoissonRatio();

  double factornu = (1.-nu)/((1.+nu)*(1.-2.*nu));
  return sqrt(E*factornu/_rho);
};


dG3DLinearElasticTriclinicMaterialLaw::dG3DLinearElasticTriclinicMaterialLaw(const int num, const double rho,
		                                      const double C00, const double C11, const double C22, const double C33, const double C44, const double C55,
		                                      const double C01, const double C02, const double C03, const double C04, const double C05,
		                                      const double C12, const double C13, const double C14, const double C15,
		                                      const double C23, const double C24, const double C25,
		                                      const double C34, const double C35,
		                                      const double C45):
	dG3DMaterialLaw(num,rho,false), _elasticLaw(num,C00,C11,C22,C33,C44,C55,C01,C02,C03,C04,C05,C12,C13,C14,C15,C23,C24,C25,C34,C35,C45){
}
dG3DLinearElasticTriclinicMaterialLaw::dG3DLinearElasticTriclinicMaterialLaw(const dG3DLinearElasticTriclinicMaterialLaw& src):dG3DMaterialLaw(src),
_elasticLaw(src._elasticLaw){
};

void dG3DLinearElasticTriclinicMaterialLaw::initLaws(const std::map<int,materialLaw*> &maplaw){
	if (!_initialized){
		elasticStiffness = _elasticLaw.getElasticTensor();
		_initialized = true;
	};
};

void dG3DLinearElasticTriclinicMaterialLaw::createIPState(IPStateBase* &ips,bool hasBodyForce,const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  IPVariable* ipvi = new  LinearElasticDG3DIPVariable(hasBodyForce,inter);
  IPVariable* ipv1 = new  LinearElasticDG3DIPVariable(hasBodyForce,inter);
  IPVariable* ipv2 = new  LinearElasticDG3DIPVariable(hasBodyForce,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void dG3DLinearElasticTriclinicMaterialLaw::createIPVariable(IPVariable* &ipv,bool hasBodyForce,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  ipv = new  LinearElasticDG3DIPVariable(hasBodyForce,inter);
}

void dG3DLinearElasticTriclinicMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  /* get ipvariable */
  dG3DIPVariableBase* ipvcur = static_cast<dG3DIPVariableBase*>(ipv);;
  const dG3DIPVariableBase* ipvprev = static_cast<const dG3DIPVariableBase*>(ipvp);;
  /* strain xyz */
  const STensor3& F = ipvcur->getConstRefToDeformationGradient();
  STensor3& P = ipvcur->getRefToFirstPiolaKirchhoffStress();
	STensor43& H = ipvcur->getRefToTangentModuli();
	_elasticLaw.constitutive(F,P,stiff,&H);
  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);
}

double dG3DLinearElasticTriclinicMaterialLaw::scaleFactor() const{
	return _elasticLaw.getYoungModulus();
};

double dG3DLinearElasticTriclinicMaterialLaw::soundSpeed() const
{
	double E = _elasticLaw.getYoungModulus();
	double nu = _elasticLaw.getPoissonRatio();

  double factornu = (1.-nu)/((1.+nu)*(1.-2.*nu));
  return sqrt(E*factornu/_rho);
};
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//
dG3DHyperelasticMaterialLaw::dG3DHyperelasticMaterialLaw(const int num, const double rho, const elasticPotential& potential):
	dG3DMaterialLaw(num,rho,false), _elasticLaw(num,rho, potential)
{

}
dG3DHyperelasticMaterialLaw::dG3DHyperelasticMaterialLaw(const dG3DHyperelasticMaterialLaw& src):dG3DMaterialLaw(src),
  _elasticLaw(src._elasticLaw)
{

};


void dG3DHyperelasticMaterialLaw::initLaws(const std::map<int,materialLaw*> &maplaw){
	if (!_initialized){
		elasticStiffness = _elasticLaw.getElasticPotential()->getElasticTensor();
		_initialized = true;
	};
};

void dG3DHyperelasticMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  IPVariable* ipvi = new  HyperElasticDG3DIPVariable(hasBodyForce,inter);
  IPVariable* ipv1 = new  HyperElasticDG3DIPVariable(hasBodyForce,inter);
  IPVariable* ipv2 = new  HyperElasticDG3DIPVariable(hasBodyForce,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void dG3DHyperelasticMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  ipv = new  HyperElasticDG3DIPVariable(hasBodyForce,inter);
}


void dG3DHyperelasticMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  /* get ipvariable */
  HyperElasticDG3DIPVariable* ipvcur = static_cast<HyperElasticDG3DIPVariable*>(ipv);;
  const HyperElasticDG3DIPVariable* ipvprev = static_cast<const HyperElasticDG3DIPVariable*>(ipvp);
  /* strain xyz */
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();
  const STensor3& F = ipvcur->getConstRefToDeformationGradient();
  STensor3& P = ipvcur->getRefToFirstPiolaKirchhoffStress();
	STensor43& H = ipvcur->getRefToTangentModuli();

  IPHyperelasticPotential* q1 = ipvcur->getIPHyperelasticPotential();
  const IPHyperelasticPotential* q0 = ipvprev->getIPHyperelasticPotential();

  _elasticLaw.constitutive(F0, F, P, q0, q1, H, stiff);
  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);
}

double dG3DHyperelasticMaterialLaw::scaleFactor() const{
	return _elasticLaw.scaleFactor();
};

double dG3DHyperelasticMaterialLaw::soundSpeed() const
{
	return _elasticLaw.soundSpeed();
};

//

DMNBasedDG3DMaterialLaw::DMNBasedDG3DMaterialLaw(const int num, const double rho, const Tree* T, bool withPer, double tol):
   dG3DMaterialLaw(num,rho,false),_withPerturbation(withPer),_pertTol(tol), _withSubstepping(true), _numStep(5)
{
  _networkInteraction = new NetworkInteraction();
  if (T!=NULL)
  {
    _networkInteraction->getInteractionFromTree(*T);
  }
  _DMNForSetting = new dG3DDeepMaterialNetwork();
}

DMNBasedDG3DMaterialLaw::DMNBasedDG3DMaterialLaw(const int num, const double rho, const std::string interactionFname, bool withPer, double tol):
   dG3DMaterialLaw(num,rho,false),_withPerturbation(withPer),_pertTol(tol), _withSubstepping(true), _numStep(5), _lineSearch(false)
{
  _networkInteraction = new NetworkInteraction();
  _networkInteraction->loadDataFromFile(interactionFname);
  _DMNForSetting = new dG3DDeepMaterialNetwork();
}


DMNBasedDG3DMaterialLaw::DMNBasedDG3DMaterialLaw(const DMNBasedDG3DMaterialLaw& src): dG3DMaterialLaw(src),
      _fixedComp(src._fixedComp),_mapLaw(src._mapLaw), _withSubstepping(src._withSubstepping), _numStep(src._numStep),
      _viewBulkIPs(src._viewBulkIPs), _viewInterfaceIPs(src._viewInterfaceIPs), _gpForViewAll(src._gpForViewAll),
      _lineSearch(false)
{
  _networkInteraction  = NULL;
  if (src._networkInteraction != NULL)
  {
    _networkInteraction = src._networkInteraction->clone();
  }
  _DMNForSetting = new dG3DDeepMaterialNetwork();
}


DMNBasedDG3DMaterialLaw::~DMNBasedDG3DMaterialLaw()
{
  delete _networkInteraction;;
  _networkInteraction = NULL;
  delete _DMNForSetting;
};

void DMNBasedDG3DMaterialLaw::setLineSearch(bool fl)
{
  _lineSearch = fl;
}

DeepMaterialNetwork& DMNBasedDG3DMaterialLaw::getDMNFromSetting()
{
  return *_DMNForSetting;
}

void DMNBasedDG3DMaterialLaw::addLaw(int matIndex, dG3DMaterialLaw* law)
{
  _mapLaw[matIndex] = law;
}
void DMNBasedDG3DMaterialLaw::fixDofsByComp(int comp)
{
  _fixedComp.push_back(comp);
};
void DMNBasedDG3DMaterialLaw::setSubstepping(bool fl, int numStep)
{
  _withSubstepping = fl;
  _numStep = numStep;
  if (_withSubstepping)
  {
    Msg::Info("supstteping is used");
  }
};
void DMNBasedDG3DMaterialLaw::viewInternalData(int i)
{
  _DMNForSetting->viewIP(i);
};
void DMNBasedDG3DMaterialLaw::viewIPAveragePerPhase(int iphase, int icomp)
{
  _DMNForSetting->viewIPAveragePerPhase(iphase,icomp);
}
void DMNBasedDG3DMaterialLaw::viewIPAverage(int icomp)
{
  _DMNForSetting->viewIPAverage(icomp);
}

void DMNBasedDG3DMaterialLaw::viewBulkIPs(int ele, int gp)
{
  _viewBulkIPs[ele].insert(gp);
}
void DMNBasedDG3DMaterialLaw::viewInterfaceIPs(int eleminus, int eleplus, int gp)
{
  _viewInterfaceIPs[std::pair<int,int>(eleminus,eleplus)].insert(gp);
}

void DMNBasedDG3DMaterialLaw::viewAllIPs(int gp)
{
  _gpForViewAll.insert(gp);
}

void DMNBasedDG3DMaterialLaw::initLaws(const std::map<int,materialLaw*> &maplaw){
	if (!_initialized){

    _initialized = true;
  }
};

DeepMaterialNetwork* DMNBasedDG3DMaterialLaw::createDMN(const MElement *ele, int gpt) const
{
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  //
  DeepMaterialNetwork* dmn = new dG3DDeepMaterialNetwork();
  NetworkInteraction* interaction = _networkInteraction->clone();
  dmn->addInteraction(*interaction);
  dmn->setLineSearch(_lineSearch);
  for (int j=0; j< _fixedComp.size(); j++)
  {
    dmn->fixDofsByComp(_fixedComp[j]);
  }
  bool viewIP = false;
  if (_gpForViewAll.find(gpt) != _gpForViewAll.end())
  {
    viewIP = true;
  }

  if (inter)
  {
    dmn->setMicroProblemIndentification(iele->getElem(0)->getNum(),iele->getElem(1)->getNum(),gpt);
    if (!viewIP)
    {
      std::map<std::pair<int,int>,std::set<int> >::const_iterator itF = _viewInterfaceIPs.find(std::pair<int,int>(iele->getElem(0)->getNum(),iele->getElem(1)->getNum()));
      if (itF != _viewInterfaceIPs.end())
      {
        const std::set<int>& allIP = itF->second;
        if (allIP.find(gpt) != allIP.end())
        {
          viewIP = true;
        }
      }
    }
  }
  else
  {
    dmn->setMicroProblemIndentification(ele->getNum(),gpt);
    if (!viewIP)
    {
      std::map<int,std::set<int> >::const_iterator itF = _viewBulkIPs.find(ele->getNum());
      if (itF != _viewBulkIPs.end())
      {
        const std::set<int>& allIP = itF->second;
        if (allIP.find(gpt) != allIP.end())
        {
          viewIP = true;
        }
      }
    }
  }
  if (viewIP)
  {
    dmn->copyViewSetting(*_DMNForSetting);
  }
  return dmn;
};

void DMNBasedDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  //
  DeepMaterialNetwork* dmn = createDMN(ele,gpt);
  //
  IPVariable* ipvi = new  DMNBasedDG3DIPVariable(dmn,hasBodyForce,inter);
  IPVariable* ipv1 = new  DMNBasedDG3DIPVariable(dmn,hasBodyForce,inter);
  IPVariable* ipv2 = new  DMNBasedDG3DIPVariable(dmn,hasBodyForce,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
};

void DMNBasedDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  //
  DeepMaterialNetwork* dmn = createDMN(ele,gpt);
  //
  ipv = new  DMNBasedDG3DIPVariable(dmn,hasBodyForce,inter);
};

void DMNBasedDG3DMaterialLaw::initialIPVariable(IPVariable* ipv, bool stiff)
{
  DMNBasedDG3DIPVariable* ipvcur = static_cast<DMNBasedDG3DIPVariable*>(ipv);
	dG3DDeepMaterialNetwork* deepMN = dynamic_cast<dG3DDeepMaterialNetwork*>(ipvcur->getDeepMaterialNetwork());
  if (!(deepMN->isInitialized()))
  {
    for (std::map<int,dG3DMaterialLaw*>::iterator it = _mapLaw.begin(); it != _mapLaw.end(); it++)
    {
      deepMN->addLaw(it->first,*(it->second));
    }
    deepMN->initSolve();
  };

  STensor43& L = ipvcur->getRefToTangentModuli();

  int maxIter = 15;
  double tol = 1e-6;
  double absTol = 1e-8;
  bool sucessful = false;
  if (_withPerturbation)
  {
    deepMN->copy(IPStateBase::current,IPStateBase::initial);// store current state
    static STensor3 Fplus, Pplus;
    for (int i=0; i< 3; i++)
    {
      for (int j=0; j<3; j++)
      {
        STensorOperation::diag(Fplus,1.);
        Fplus(i,j) += _pertTol;
        sucessful = deepMN->stress(Fplus,Pplus,false,L,maxIter, tol, absTol);
        if (!sucessful)
        {
          Msg::Warning("No convergence for in Deep MN for pert system %d %d",i,j);
          break;
        }
        for (int k=0; k<3; k++)
        {
          for (int l=0; l<3; l++)
          {
            L(k,l,i,j) = (Pplus(k,l))/_pertTol;
          }
        }
      }
      if (!sucessful)
      {
        break;
      }
    }
    // make  estimation
    deepMN->copy(IPStateBase::initial,IPStateBase::current);// put back current state
  }
  else
  {
    STensor3 F(1.);
    STensor3 P(0.);
    sucessful= deepMN->stress(F,P,true,L,maxIter, tol, absTol);
  }
  if (!sucessful)
  {
    Msg::Error("DMNBasedDG3DIPVariable is not correctly initialized!");
    Msg::Exit(0);
  }
  else
  {
    elasticStiffness = L;
  }
};

void DMNBasedDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  DMNBasedDG3DIPVariable* ipvcur = static_cast<DMNBasedDG3DIPVariable*>(ipv);
  const DMNBasedDG3DIPVariable* ipvprev = static_cast<const DMNBasedDG3DIPVariable*>(ipvp);

  dG3DDeepMaterialNetwork* deepMN = dynamic_cast<dG3DDeepMaterialNetwork*>(ipvcur->getDeepMaterialNetwork());
  if (!(deepMN->isInitialized()))
  {
    for (std::map<int,dG3DMaterialLaw*>::iterator it = _mapLaw.begin(); it != _mapLaw.end(); it++)
    {
      deepMN->addLaw(it->first,*(it->second));
    }
    deepMN->initSolve();
  };

  deepMN->resetToPreviousStep();

  const STensor3& F = ipvcur->getConstRefToDeformationGradient();
  const STensor3& Fprev = ipvprev->getConstRefToDeformationGradient();
  STensor3& P = ipvcur->getRefToFirstPiolaKirchhoffStress();
  STensor43& L = ipvcur->getRefToTangentModuli();

  if (STensorOperation::isnan(F))
  {
    Msg::Warning("F is nan");
    // to force reduce time step
    P(0,0) = P(1,1) = P(2,2) = sqrt(-1.);
    return;
  };

  static STensor3 diffF;
  diffF = F;
  diffF -= Fprev;
  if (diffF.norm0() < 1e-10)
  {
    // copy
    ipv->operator=(*ipvp);
  }
  else
  {
    int maxIter = 15;
    double tol = 1e-6;
    double absTol = 1e-8;

    bool sucessful = false;
    if (_withPerturbation && stiff)
    {
      sucessful = deepMN->stress(F,P,false,L,maxIter, tol, absTol);
      if (sucessful)
      {
        deepMN->copy(IPStateBase::current,IPStateBase::initial);// store current state
        static STensor3 Fplus, Pplus;
        for (int i=0; i< 3; i++)
        {
          for (int j=0; j<3; j++)
          {
            Fplus = F;
            Fplus(i,j) += _pertTol;
            sucessful = deepMN->stress(Fplus,Pplus,false,L,maxIter, tol, absTol);
            if (!sucessful)
            {
              Msg::Warning("No convergence for in Deep MN for pert system %d %d",i,j);
              break;
            }
            for (int k=0; k<3; k++)
            {
              for (int l=0; l<3; l++)
              {
                L(k,l,i,j) = (Pplus(k,l)-P(k,l))/_pertTol;
              }
            }
          }
          if (!sucessful)
          {
            break;
          }
        }
        // make  estimation
        deepMN->copy(IPStateBase::initial,IPStateBase::current);// put back current state
      }
    }
    else
    {
      sucessful= deepMN->stress(F,P,stiff,L,maxIter, tol, absTol);
      if (!sucessful)
      {
        //deepMN->printDeformationGradientNodes();
        if (_withSubstepping && (_numStep  >1))
        {
          //
          deepMN->resetToPreviousStep();
          Msg::Warning("supstepping: numstep = %d",_numStep);
          // store current previous
          deepMN->copy(IPStateBase::previous,IPStateBase::initial);          //
          static STensor3 dF, Fcur;
          dF = F;
          dF -= Fprev;
          dF *= (1./(double)_numStep);

          for (int i=0; i< _numStep; i++)
          {
            Fcur = Fprev;
            Fcur.daxpy(dF,(double)i+1.);
            if (i < _numStep -1)
            {
              sucessful=deepMN->stress(Fcur,P,false,L,maxIter, tol, absTol);
              if (sucessful)
              {
                deepMN->nextStep();
              }
            }
            else
            {
              sucessful = deepMN->stress(Fcur,P,stiff,L,maxIter, tol, absTol);
            }
            if (!sucessful)
            {
              Msg::Warning("No convergence for in Deep MN for supstepping %d",i);
              F.print("F");
              Fcur.print("Fcur");
              break;
            }
          }
          // put back previous
          deepMN->copy(IPStateBase::initial,IPStateBase::previous);
        }
      }
    }

    if (!sucessful)
    {
      Msg::Warning("No convergence for in Deep MN");
      // to force reduce time step
      P(0,0) = P(1,1) = P(2,2) = sqrt(-1.);
    }
  }
};

//

MultipleDG3DMaterialLaw::MultipleDG3DMaterialLaw(const int num, const double rho):
  dG3DMaterialLaw(num,rho,false), _allLaws(), _weightGenerator(NULL){}

MultipleDG3DMaterialLaw::MultipleDG3DMaterialLaw(const MultipleDG3DMaterialLaw& src):dG3DMaterialLaw(src),
  _allLaws(src._allLaws)
{
  _weightGenerator  = NULL;
  if (src._weightGenerator != NULL)
  {
    _weightGenerator = src._weightGenerator->clone();
  }
};

MultipleDG3DMaterialLaw::~MultipleDG3DMaterialLaw()
{
  if (_weightGenerator!= NULL)
  {
    delete _weightGenerator;
    _weightGenerator = NULL;
  };
}

void MultipleDG3DMaterialLaw::addLaw(dG3DMaterialLaw* law)
{
  _allLaws.push_back(law);
}
void MultipleDG3DMaterialLaw::setWeightGenerator(const generalMapping* wGen)
{
  _weightGenerator = wGen->clone();
}

void MultipleDG3DMaterialLaw::setTime(const double t,const double dtime)
{
  dG3DMaterialLaw::setTime(t,dtime);
  for(int i=0; i<_allLaws.size();i++){
    _allLaws[i]->setTime(t,dtime);
  }
};

void MultipleDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  //
  int numIps = _allLaws.size();
  IPVariable* ipvi = new  dG3DIPVariableMultiple(numIps,hasBodyForce,inter);
  IPVariable* ipv1 = new  dG3DIPVariableMultiple(numIps,hasBodyForce,inter);
  IPVariable* ipv2 = new  dG3DIPVariableMultiple(numIps,hasBodyForce,inter);

  dG3DIPVariableMultiple* ipvi_multiple = static_cast<dG3DIPVariableMultiple*>(ipvi);
  dG3DIPVariableMultiple* ipv1_multiple = static_cast<dG3DIPVariableMultiple*>(ipv1);
  dG3DIPVariableMultiple* ipv2_multiple = static_cast<dG3DIPVariableMultiple*>(ipv2);

  // generate weight
  SPoint3 pt_Global;
  ele->pnt(GP->pt[0],GP->pt[1],GP->pt[2],pt_Global);
  std::vector<double> xyz(3);
  xyz[0] = pt_Global[0];
  xyz[1] = pt_Global[1];
  xyz[2] = pt_Global[2];
  if (_weightGenerator == NULL)
  {
    Msg::Error("weight generator need to be provide !!!");
  }
  std::vector<double> weights = _weightGenerator->evaluate(xyz);
  if (weights.size() != _allLaws.size())
  {
    Msg::Error("num of laws must be equal to number of weights");
  }

  for (int i=0; i< _allLaws.size(); i++)
  {
    IPStateBase* ipsPart = NULL;
    _allLaws[i]->createIPState(ipsPart, hasBodyForce, state_, ele, nbFF_, GP, gpt);
    std::vector<IPVariable*> ip_allParts;
    ipsPart->getAllIPVariable(ip_allParts);
    ipvi_multiple->addIPv(i, ip_allParts[0], weights[i]);
    ipv1_multiple->addIPv(i, ip_allParts[1], weights[i]);
    ipv2_multiple->addIPv(i, ip_allParts[2], weights[i]);
  };
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
};

void MultipleDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  int numIps = _allLaws.size();
  ipv = new  dG3DIPVariableMultiple(numIps, hasBodyForce,inter);
  dG3DIPVariableMultiple* ipv_multiple = static_cast<dG3DIPVariableMultiple*>(ipv);
  // generate weight
  SPoint3 pt_Global;
  ele->pnt(GP->pt[0],GP->pt[1],GP->pt[2],pt_Global);
  std::vector<double> xyz(3);
  xyz[0] = pt_Global[0];
  xyz[1] = pt_Global[1];
  xyz[2] = pt_Global[2];
  if (_weightGenerator == NULL)
  {
    Msg::Error("weight generator need to be provide !!!");
  }
  std::vector<double> weights = _weightGenerator->evaluate(xyz);
  if (weights.size() != _allLaws.size())
  {
    Msg::Error("num of laws must be equal to number of weights");
  }

  for (int i=0; i< _allLaws.size(); i++)
  {
    IPVariable* ipvPart = NULL;
    _allLaws[i]->createIPVariable(ipvPart, hasBodyForce, ele, nbFF_, GP, gpt);
    ipv_multiple->addIPv(i, ipvPart, weights[i]);
  };
};

void MultipleDG3DMaterialLaw::initialIPVariable(IPVariable* ipv, bool stiff)
{
  dG3DIPVariableMultiple* ipv_multiple = dynamic_cast<dG3DIPVariableMultiple*>(ipv);
  if (ipv_multiple == NULL)
  {
    Msg::Error("dG3DIPVariableMultiple must be used in MultipleDG3DMaterialLaw::initialIPVariable");
  }
  for (int i=0; i< _allLaws.size(); i++)
  {
    IPVariable* ipv_i = ipv_multiple->getIPv(i);
    _allLaws[i]->initialIPVariable(ipv_i, stiff);
  };
};

void MultipleDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const
{
  dG3DIPVariableMultiple* ipv_multiple = dynamic_cast<dG3DIPVariableMultiple*>(ipv);
  const dG3DIPVariableMultiple* ipvprev_multiple = dynamic_cast<const dG3DIPVariableMultiple*>(ipvprev);
  if (ipv_multiple == NULL)
  {
    Msg::Error("dG3DIPVariableMultiple must be used in MultipleDG3DMaterialLaw::checkInternalState");
  }
  for (int i=0; i< _allLaws.size(); i++)
  {
    IPVariable* ipv_i = ipv_multiple->getIPv(i);
    const IPVariable* ipvprev_i = ipvprev_multiple->getIPv(i);
    _allLaws[i]->checkInternalState(ipv_i, ipvprev_i);
  };
};
bool MultipleDG3DMaterialLaw::withEnergyDissipation() const
{
  for (int i=0; i< _allLaws.size(); i++)
  {
    if (_allLaws[i]->withEnergyDissipation())
    {
      return true;
    }
  }
  return false;
};

void MultipleDG3DMaterialLaw::setMacroSolver(const nonLinearMechSolver* sv)
{
  dG3DMaterialLaw::setMacroSolver(sv);
  for (int i=0; i< _allLaws.size(); i++)
  {
    _allLaws[i]->setMacroSolver(sv);
  }
};

void MultipleDG3DMaterialLaw::initLaws(const std::map<int,materialLaw*> &maplaw){
	if (!_initialized){
    STensorOperation::zero(elasticStiffness);
    STensorOperation::zero(TotalelasticStiffness);
    for (int i=0; i< _allLaws.size(); i++)
    {
      _allLaws[i]->initLaws(maplaw);
      elasticStiffness.axpy(1.,_allLaws[i]->elasticStiffness);
      TotalelasticStiffness.axpy(1.,_allLaws[i]->TotalelasticStiffness);
    }
    elasticStiffness *= (1./double(_allLaws.size()));
    TotalelasticStiffness *= (1./double(_allLaws.size()));
    _initialized = true;
  }
};

void MultipleDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  dG3DIPVariableMultiple* ipvcur = static_cast<dG3DIPVariableMultiple*>(ipv);;
  const dG3DIPVariableMultiple* ipvprev = static_cast<const dG3DIPVariableMultiple*>(ipvp);

  int numIPs = ipvcur->getNumIPs();
  const STensor3& F = ipvcur->getConstRefToDeformationGradient();
  const STensor3& Fprev = ipvprev->getConstRefToDeformationGradient();
  STensor3& P = ipvcur->getRefToFirstPiolaKirchhoffStress();
  STensor43& L = ipvcur->getRefToTangentModuli();


  STensorOperation::zero(P);
  STensorOperation::zero(L);
  for (int i=0; i< numIPs; i++)
  {
    IPVariable* ipv_i = ipvcur->getIPv(i);
    const IPVariable* ipvprev_i = ipvprev->getIPv(i);
    double wi = ipvcur->getWeight(i);
    if (wi > 1e-10)
    {
      dG3DIPVariableBase* ipvdG3D_i = dynamic_cast<dG3DIPVariableBase*>(ipv_i);
      ipvdG3D_i->getRefToDeformationGradient() = F;
      _allLaws[i]->stress(ipv_i, ipvprev_i, stiff, checkfrac, dTangent);

      const STensor3& P_i = ipvdG3D_i->getConstRefToFirstPiolaKirchhoffStress();
      const STensor43& L_i = ipvdG3D_i->getConstRefToTangentModuli();

      P.daxpy(P_i,wi);
      L.axpy(wi,L_i);
    }
  }
  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);
};

//


ANNBasedDG3DMaterialLaw::ANNBasedDG3DMaterialLaw(const int num, const double rho, const int numInternalVars, const MIMOFunction& fct, bool pert, double tol):
  dG3DMaterialLaw(num,rho,false), _initialInternalValues(), _numberOfInternalVariables(numInternalVars),_tangentByPerturbation(pert),_pertTol(tol),
  _kinematicInput(EGL)
{
  _mimoFct = fct.clone();
};
ANNBasedDG3DMaterialLaw::ANNBasedDG3DMaterialLaw(const ANNBasedDG3DMaterialLaw& src):
      dG3DMaterialLaw(src),  _initialInternalValues(src._initialInternalValues),
      _numberOfInternalVariables(src._numberOfInternalVariables), _tangentByPerturbation(src._tangentByPerturbation),
      _pertTol(src._pertTol), _kinematicInput(src._kinematicInput)
{
  _mimoFct = NULL;
  if (src._mimoFct!=NULL)
  {
    _mimoFct = src._mimoFct->clone();
  }
}
ANNBasedDG3DMaterialLaw::~ANNBasedDG3DMaterialLaw()
{
  if (_mimoFct!=NULL)
  {
    delete _mimoFct;
    _mimoFct = NULL;
  }
};

void ANNBasedDG3DMaterialLaw::setInitialInternalComp(int comp, double val)
{
  _initialInternalValues[comp] = val;
}

void ANNBasedDG3DMaterialLaw::setKinematicInput(const int i)
{
  if (i == 1)
  {
    _kinematicInput = EGL;
    Msg::Info("using Green Lagrange strain as kinematic variable");
  }
  else if (i == 2)
  {
    _kinematicInput = U;
    Msg::Info("using Biot strain as kinematic variable");
  }
  else if (i == 3)
  {
    _kinematicInput = smallStrain;
    Msg::Info("using small strain (FT+F)/2-I as kinematic variable");
  }
  else
  {
    Msg::Error("input type %d has not been defined",i);
  }
}

void ANNBasedDG3DMaterialLaw::initLaws(const std::map<int,materialLaw*> &maplaw){
	if (!_initialized){
    fullMatrix<double> E1(1,6), E0(1,6), q0(1,_numberOfInternalVariables) ,q1(1,_numberOfInternalVariables), S0(1,6), S(1,6);
    setInitialInternalState(q0);
    setInitialInternalState(q1);
    fullMatrix<double> DSDE(6,6);
    if (!_mimoFct)
    {
      Msg::Error("embeded function does not exist in ANNBasedDG3DMaterialLaw ");
      Msg::Exit(0);
    }

    if (_tangentByPerturbation)
    {
      _mimoFct->predict(E1,E0,q0,S0,q1,S,false,NULL);
      static fullMatrix<double> E1_plus(1,6),q1_plus(1,_numberOfInternalVariables),S_plus(1,6);
      for (int i=0; i< 6; i++)
      {
        E1_plus = E1;
        E1_plus(0,i) += _pertTol;
        _mimoFct->predict(E1_plus,E0,q0,S0,q1_plus,S_plus,false,NULL);
        for (int j=0; j<6; j++)
        {
          DSDE(j,i) = (S_plus(0,j) - S(0,j))/_pertTol;
        }
      }
    }
    else
    {
      _mimoFct->predict(E1,E0,q0,S0,q1,S,true,&DSDE);
    };

    convertFullMatrixToSTensor43(DSDE,elasticStiffness);
		_initialized = true;
	};
};


void ANNBasedDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  IPVariable* ipvi = new  ANNBasedDG3DIPVariable(_numberOfInternalVariables,hasBodyForce,inter);
  ANNBasedDG3DIPVariable* ipann = static_cast<ANNBasedDG3DIPVariable*>(ipvi);
  setInitialInternalState(ipann->getRefToInternalVariables());
  IPVariable* ipv1 = new  ANNBasedDG3DIPVariable(_numberOfInternalVariables,hasBodyForce,inter);
  ipann = static_cast<ANNBasedDG3DIPVariable*>(ipv1);
  setInitialInternalState(ipann->getRefToInternalVariables());
  IPVariable* ipv2 = new  ANNBasedDG3DIPVariable(_numberOfInternalVariables,hasBodyForce,inter);
  ipann = static_cast<ANNBasedDG3DIPVariable*>(ipv2);
  setInitialInternalState(ipann->getRefToInternalVariables());
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void ANNBasedDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  ipv = new  ANNBasedDG3DIPVariable(_numberOfInternalVariables,hasBodyForce,inter);
  ANNBasedDG3DIPVariable* ipann = static_cast<ANNBasedDG3DIPVariable*>(ipv);
  setInitialInternalState(ipann->getRefToInternalVariables());
}


void ANNBasedDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  /* get ipvariable */
  ANNBasedDG3DIPVariable* ipvcur = static_cast<ANNBasedDG3DIPVariable*>(ipv);;
  const ANNBasedDG3DIPVariable* ipvprev = static_cast<const ANNBasedDG3DIPVariable*>(ipvp);

  fullMatrix<double>& E1 = ipvcur->getRefToKinematicVariables();
  // compute E1
  const STensor3& F = ipvcur->getConstRefToDeformationGradient();
  static STensor3 FTF, Ustretch, R;
  if (_kinematicInput == ANNBasedDG3DMaterialLaw::EGL)
  {
    STensorOperation::multSTensor3FirstTranspose(F,F,FTF);
    E1(0,0) = 0.5*(FTF(0,0)-1.);
    E1(0,1) = 0.5*(FTF(1,1)-1.);
    E1(0,2) = 0.5*(FTF(2,2)-1.);
    E1(0,3) = 0.5*FTF(0,1);
    E1(0,4) = 0.5*FTF(0,2);
    E1(0,5) = 0.5*FTF(1,2);
  }
  else if (_kinematicInput == ANNBasedDG3DMaterialLaw::U)
  {
    STensorOperation::RUDecomposition(F,Ustretch,R);
    E1(0,0) = Ustretch(0,0)-1.;
    E1(0,1) = Ustretch(1,1)-1.;
    E1(0,2) = Ustretch(2,2)-1.;
    E1(0,3) = Ustretch(0,1);
    E1(0,4) = Ustretch(0,2);
    E1(0,5) = Ustretch(1,2);
  }
  else if (_kinematicInput == ANNBasedDG3DMaterialLaw::smallStrain)
  {
    E1(0,0) = F(0,0)-1.;
    E1(0,1) = F(1,1)-1.;
    E1(0,2) = F(2,2)-1.;
    E1(0,3) = 0.5*(F(0,1)+F(1,0));
    E1(0,4) = 0.5*(F(0,2)+F(2,0));
    E1(0,5) = 0.5*(F(1,2)+F(2,1));
  }
  else
  {
    Msg::Error("kinematic type %d has not been defined",_kinematicInput);
  }
  const fullMatrix<double>& E0 = ipvprev->getConstRefToKinematicVariables();
  const fullMatrix<double>& q0 = ipvprev->getConstRefToInternalVariables();
  const fullMatrix<double>& S0 = ipvprev->getConstRefToStressVariables();

  fullMatrix<double>& q1 = ipvcur->getRefToInternalVariables();
  fullMatrix<double>& S1 = ipvcur->getRefToStressVariables();

  static fullMatrix<double> DSDE(6,6);
  if (stiff && _tangentByPerturbation)
  {
    _mimoFct->predict(E1,E0,q0,S0,q1,S1,false,NULL);
    static fullMatrix<double> E1_plus(1,6),q1_plus(1,_numberOfInternalVariables),S1_plus(1,6);
    for (int i=0; i< 6; i++)
    {
      E1_plus = E1;
      E1_plus(0,i) += _pertTol;
      _mimoFct->predict(E1_plus,E0,q0,S0,q1_plus,S1_plus,false,NULL);
      for (int j=0; j<6; j++)
      {
        DSDE(j,i) = (S1_plus(0,j) - S1(0,j))/_pertTol;
      }
    }
  }
  else
  {
    _mimoFct->predict(E1,E0,q0,S0,q1,S1,stiff,&DSDE);
  }

  if (_kinematicInput == smallStrain)
  {
    // small strain
    STensor3& P = ipvcur->getRefToFirstPiolaKirchhoffStress();
    P(0,0) = S1(0,0);
    P(1,1) = S1(0,1);
    P(2,2) = S1(0,2);
    P(0,1) = S1(0,3);
    P(1,0) = S1(0,3);
    P(0,2) = S1(0,4);
    P(2,0) = S1(0,4);
    P(1,2) = S1(0,5);
    P(2,1) = S1(0,5);
    if (stiff)
    {
      STensor43& L = ipvcur->getRefToTangentModuli();
      convertFullMatrixToSTensor43(DSDE,L);
    }
  }
  else
  {
    // get stress
    // output is S00, S11, S22, S01, S02, S12
    static STensor3 secondPK;
    secondPK(0,0) = S1(0,0);
    secondPK(1,1) = S1(0,1);
    secondPK(2,2) = S1(0,2);
    secondPK(0,1) = S1(0,3);
    secondPK(1,0) = S1(0,3);
    secondPK(0,2) = S1(0,4);
    secondPK(2,0) = S1(0,4);
    secondPK(1,2) = S1(0,5);
    secondPK(2,1) = S1(0,5);

    STensor3& P = ipvcur->getRefToFirstPiolaKirchhoffStress();
    STensorOperation::multSTensor3(F,secondPK,P);

    if (stiff)
    {
      static STensor43 DsecondPKDE;
      convertFullMatrixToSTensor43(DSDE,DsecondPKDE);

      STensor43& L = ipvcur->getRefToTangentModuli();
      STensorOperation::zero(L);

      static STensor43 DsecondPKDEGreenLagrange;
      if (_kinematicInput == ANNBasedDG3DMaterialLaw::EGL)
      {
        DsecondPKDEGreenLagrange = DsecondPKDE;
      }
      else if (_kinematicInput == ANNBasedDG3DMaterialLaw::U)
      {
        static STensor3 invU;
        STensorOperation::inverseSTensor3(Ustretch,invU);
        for (int i=0; i<3; i++)
        {
          for (int j=0; j<3; j++)
          {
            for (int k=0; k<3; k++)
            {
              for (int l=0; l<3; l++)
              {
                DsecondPKDEGreenLagrange(i,j,k,l) = 0.;
                for (int m=0; m< 3; m++)
                {
                  DsecondPKDEGreenLagrange(i,j,k,l) += invU(k,m)*DsecondPKDE(i,j,m,l);
                }
              }
            }
          }
        }
      }
      for (int i=0; i<3; i++)
      {
        for (int j=0; j<3; j++)
        {
          for (int k=0; k<3; k++)
          {
            L(i,j,i,k) += secondPK(k,j);
            for (int l=0; l<3; l++)
            {
              //
              for (int a=0; a<3; a++)
              {
                for (int b=0; b<3; b++)
                {
                  L(i,j,k,l) += F(i,a)*DsecondPKDEGreenLagrange(a,j,l,b)*F(k,b);
                }
              }
            }
          }
        }
      }
    };
  }
  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);
};

void ANNBasedDG3DMaterialLaw::setInitialInternalState(fullMatrix<double>& q) const
{
  for (int i=0; i< _numberOfInternalVariables; i++)
  {
    std::map<int,double>::const_iterator itFind = _initialInternalValues.find(i);
    if (itFind != _initialInternalValues.end())
    {
      q(0,i) = itFind->second;
    }
  }
}

void ANNBasedDG3DMaterialLaw::convertFullMatrixToSTensor43(const fullMatrix<double>& DSDE, STensor43& DsecondPKDE) const
{
  if (DSDE.size1() != 6 or DSDE.size2() != 6)
  {
    Msg::Error("data is not compatible ANNBasedDG3DMaterialLaw:convertFullMatrixToSTensor43");
    return;
  }
  DsecondPKDE(0,0,0,0) = DSDE(0,0);
  DsecondPKDE(0,0,1,1) = DSDE(0,1);
  DsecondPKDE(0,0,2,2) = DSDE(0,2);
  DsecondPKDE(0,0,0,1) = 0.5*DSDE(0,3);
  DsecondPKDE(0,0,1,0) = 0.5*DSDE(0,3);
  DsecondPKDE(0,0,0,2) = 0.5*DSDE(0,4);
  DsecondPKDE(0,0,2,0) = 0.5*DSDE(0,4);
  DsecondPKDE(0,0,1,2) = 0.5*DSDE(0,5);
  DsecondPKDE(0,0,2,1) = 0.5*DSDE(0,5);

  DsecondPKDE(1,1,0,0) = DSDE(1,0);
  DsecondPKDE(1,1,1,1) = DSDE(1,1);
  DsecondPKDE(1,1,2,2) = DSDE(1,2);
  DsecondPKDE(1,1,0,1) = 0.5*DSDE(1,3);
  DsecondPKDE(1,1,1,0) = 0.5*DSDE(1,3);
  DsecondPKDE(1,1,0,2) = 0.5*DSDE(1,4);
  DsecondPKDE(1,1,2,0) = 0.5*DSDE(1,4);
  DsecondPKDE(1,1,1,2) = 0.5*DSDE(1,5);
  DsecondPKDE(1,1,2,1) = 0.5*DSDE(1,5);

  DsecondPKDE(2,2,0,0) = DSDE(2,0);
  DsecondPKDE(2,2,1,1) = DSDE(2,1);
  DsecondPKDE(2,2,2,2) = DSDE(2,2);
  DsecondPKDE(2,2,0,1) = 0.5*DSDE(2,3);
  DsecondPKDE(2,2,1,0) = 0.5*DSDE(2,3);
  DsecondPKDE(2,2,0,2) = 0.5*DSDE(2,4);
  DsecondPKDE(2,2,2,0) = 0.5*DSDE(2,4);
  DsecondPKDE(2,2,1,2) = 0.5*DSDE(2,5);
  DsecondPKDE(2,2,2,1) = 0.5*DSDE(2,5);

  DsecondPKDE(0,1,0,0) = DSDE(3,0);
  DsecondPKDE(0,1,1,1) = DSDE(3,1);
  DsecondPKDE(0,1,2,2) = DSDE(3,2);
  DsecondPKDE(0,1,0,1) = 0.5*DSDE(3,3);
  DsecondPKDE(0,1,1,0) = 0.5*DSDE(3,3);
  DsecondPKDE(0,1,0,2) = 0.5*DSDE(3,4);
  DsecondPKDE(0,1,2,0) = 0.5*DSDE(3,4);
  DsecondPKDE(0,1,1,2) = 0.5*DSDE(3,5);
  DsecondPKDE(0,1,2,1) = 0.5*DSDE(3,5);

  DsecondPKDE(1,0,0,0) = DSDE(3,0);
  DsecondPKDE(1,0,1,1) = DSDE(3,1);
  DsecondPKDE(1,0,2,2) = DSDE(3,2);
  DsecondPKDE(1,0,0,1) = 0.5*DSDE(3,3);
  DsecondPKDE(1,0,1,0) = 0.5*DSDE(3,3);
  DsecondPKDE(1,0,0,2) = 0.5*DSDE(3,4);
  DsecondPKDE(1,0,2,0) = 0.5*DSDE(3,4);
  DsecondPKDE(1,0,1,2) = 0.5*DSDE(3,5);
  DsecondPKDE(1,0,2,1) = 0.5*DSDE(3,5);

  DsecondPKDE(0,2,0,0) = DSDE(4,0);
  DsecondPKDE(0,2,1,1) = DSDE(4,1);
  DsecondPKDE(0,2,2,2) = DSDE(4,2);
  DsecondPKDE(0,2,0,1) = 0.5*DSDE(4,3);
  DsecondPKDE(0,2,1,0) = 0.5*DSDE(4,3);
  DsecondPKDE(0,2,0,2) = 0.5*DSDE(4,4);
  DsecondPKDE(0,2,2,0) = 0.5*DSDE(4,4);
  DsecondPKDE(0,2,1,2) = 0.5*DSDE(4,5);
  DsecondPKDE(0,2,2,1) = 0.5*DSDE(4,5);

  DsecondPKDE(2,0,0,0) = DSDE(4,0);
  DsecondPKDE(2,0,1,1) = DSDE(4,1);
  DsecondPKDE(2,0,2,2) = DSDE(4,2);
  DsecondPKDE(2,0,0,1) = 0.5*DSDE(4,3);
  DsecondPKDE(2,0,1,0) = 0.5*DSDE(4,3);
  DsecondPKDE(2,0,0,2) = 0.5*DSDE(4,4);
  DsecondPKDE(2,0,2,0) = 0.5*DSDE(4,4);
  DsecondPKDE(2,0,1,2) = 0.5*DSDE(4,5);
  DsecondPKDE(2,0,2,1) = 0.5*DSDE(4,5);

  DsecondPKDE(1,2,0,0) = DSDE(5,0);
  DsecondPKDE(1,2,1,1) = DSDE(5,1);
  DsecondPKDE(1,2,2,2) = DSDE(5,2);
  DsecondPKDE(1,2,0,1) = 0.5*DSDE(5,3);
  DsecondPKDE(1,2,1,0) = 0.5*DSDE(5,3);
  DsecondPKDE(1,2,0,2) = 0.5*DSDE(5,4);
  DsecondPKDE(1,2,2,0) = 0.5*DSDE(5,4);
  DsecondPKDE(1,2,1,2) = 0.5*DSDE(5,5);
  DsecondPKDE(1,2,2,1) = 0.5*DSDE(5,5);

  DsecondPKDE(2,1,0,0) = DSDE(5,0);
  DsecondPKDE(2,1,1,1) = DSDE(5,1);
  DsecondPKDE(2,1,2,2) = DSDE(5,2);
  DsecondPKDE(2,1,0,1) = 0.5*DSDE(5,3);
  DsecondPKDE(2,1,1,0) = 0.5*DSDE(5,3);
  DsecondPKDE(2,1,0,2) = 0.5*DSDE(5,4);
  DsecondPKDE(2,1,2,0) = 0.5*DSDE(5,4);
  DsecondPKDE(2,1,1,2) = 0.5*DSDE(5,5);
  DsecondPKDE(2,1,2,1) = 0.5*DSDE(5,5);
};

double ANNBasedDG3DMaterialLaw::soundSpeed() const
{
  Msg::Error("ANNBasedDG3DMaterialLaw::soundSpeed is not defined !!!");
  return 0.;
};
//

void ExtraVariablesAtGaussPoints::fill(const std::string fname)
{
  Msg::Info("read %s",fname.c_str());
  filled=true;
  locations.clear();
  field.clear();

  std::ifstream inputFile(fname.c_str());
  std::vector<double> numbers;
  if (inputFile.is_open())
  {
    std::string line;
    while (std::getline(inputFile, line))
    {
      std::stringstream ss(line);
      std::string token;
      numbers.clear();
      while (std::getline(ss, token, ';')) {
        double num;
        std::istringstream(token) >> num;
        numbers.push_back(num);
      }
      int numberSize=numbers.size();
      //Msg::Info("Line of %d values",numberSize);
      if(numberSize<4)
      {
        Msg::Error("Not enough numbers per line in %s",fname.c_str());
        Msg::Exit(0);
      }
      SVector3 pt(numbers[0],numbers[1],numbers[2]);
      locations.push_back(pt);
      std::vector<double> localfield=std::vector<double>(numbers.begin()+3,numbers.end());
      field.push_back(localfield);
    }
  }
  else
  {
     Msg::Error("Unable to read %s",fname.c_str());
     Msg::Exit(0);
  }

  inputFile.close();
}

const std::vector<double> &ExtraVariablesAtGaussPoints::getFieldAtClosestLocation(const SVector3 &gp) const
{
  std::vector<SVector3>::const_iterator it1min, it1= locations.begin();
  std::vector< std::vector<double> >::const_iterator it2min, it2 = field.begin();
  if (locations.size() == field.size()) {
    double minimumdist = 1.e12;
    SVector3 dist;
    for (; it1 != locations.end() && it2 != field.end(); ++it1, ++it2) {
      dist=gp;
      dist-=*it1;
      double norm=dist.norm();
      if(norm <minimumdist)
      {
        it1min=it1;
        it2min=it2;
        minimumdist=norm;
      }
    }
    //Msg::Info("Values at Gauss point location: %f %f %f", (*it1min)[0],(*it1min)[1],(*it1min)[2]);
    //std::vector<double>::const_iterator it3;
    //for (it3=it2min->begin(); it3 != it2min->end(); ++it3) {
    //        Msg::Info("Element %f",*it3);
    //}
  }
  else
  {
    Msg::Error("ExtraVariablesAtGaussPoints locations and field not of the same size");
    Msg::Exit(0);
  }
  return *it2min;
}

void ExtraVariablesAtGaussPoints::print() const
{
  if (locations.size() == field.size()) {
    std::vector<SVector3>::const_iterator it1 = locations.begin();
    std::vector< std::vector<double> >::const_iterator it2 = field.begin();
    for (; it1 != locations.end() && it2 != field.end(); ++it1, ++it2) {
      Msg::Info("Values at Gauss point location: %f %f %f", (*it1)[0],(*it1)[1],(*it1)[2]);
      std::vector<double>::const_iterator it3;
      for (it3=it2->begin(); it3 != it2->end(); ++it3) {
            Msg::Info("Element %f",*it3);
      }
    }
  }
  else
  {
    Msg::Error("ExtraVariablesAtGaussPoints locations and field not of the same size");
    Msg::Exit(0);
  }
}


torchANNBasedDG3DMaterialLaw::torchANNBasedDG3DMaterialLaw(const int num, const double rho, const int numberOfInput,
                const int numInternalVars, const char* nameTorch, const double EXXmean, const double EXXstd, const double EXYmean,
                const double EXYstd, const double EYYmean, const double EYYstd, const double EYZmean, const double EYZstd, const double EZZmean,
                const double EZZstd, const double EZXmean, const double EZXstd, const double SXXmean, const double SXXstd, const double SXYmean,
                const double SXYstd, const double SYYmean, const double SYYstd, const double SYZmean, const double SYZstd, const double SZZmean,
                const double SZZstd, const double SZXmean, const double SZXstd, bool pert, double tol):
                dG3DMaterialLaw(num,rho,false), _numberOfInput(numberOfInput), _numberOfInternalVariables(numInternalVars), _initialValue_h(-1.),
                _EXXmean(EXXmean), _EXXstd(EXXstd), _EXYmean(EXYmean), _EXYstd(EXYstd), _EYYmean(EYYmean), _EYYstd(EYYstd), _EYZmean(EYZmean),
                _EYZstd(EYZstd), _EZZmean(EZZmean), _EZZstd(EZZstd), _EZXmean(EZXmean), _EZXstd(EZXstd), _SXXmean(SXXmean), _SXXstd(SXXstd), _SXYmean(SXYmean),
                _SXYstd(SXYstd), _SYYmean(SYYmean), _SYYstd(SYYstd), _SYZmean(SYZmean), _SYZstd(SYZstd), _SZZmean(SZZmean), _SZZstd(SZZstd),
                _SZXmean(SZXmean), _SZXstd(SZXstd), _tangentByPerturbation(pert), _pertTol(tol), _kinematicInput(EGL), _NeedExtraNorm(false),  _DoubleInput(false),
		_numberOfExtraInput(0),
                _fileExtraInputsGp(""), _extraVariablesAtGaussPoints(), _time_arg(-1), _ctime(0.0)

{
#if defined(HAVE_TORCH)
    try{
      module = torch::jit::load(nameTorch); //nameTorch);
    }
    catch (const c10::Error& e) {
      Msg::Error("error loading the model");
    }
    module.eval();
    if(_tangentByPerturbation){torch::NoGradGuard no_grad;}

    if(_numberOfInput == 3){ // for 2D case
        VXX = torch::zeros({1, 1, 4});
        VXY = torch::zeros({1, 1, 4});
        VYY = torch::zeros({1, 1, 4});
        VZZ = torch::zeros({1, 1, 4});

        VXX[0][0][0] = 1.0;
        VXY[0][0][1] = 1.0;
        VYY[0][0][2] = 1.0;
        VZZ[0][0][3] = 1.0;
     }
     else if(_numberOfInput == 6){ // for 3D case
        VXX = torch::zeros({1, 1, 6});
        VXY = torch::zeros({1, 1, 6});
        VYY = torch::zeros({1, 1, 6});
        VYZ = torch::zeros({1, 1, 6});
        VZZ = torch::zeros({1, 1, 6});
        VZX = torch::zeros({1, 1, 6});

        VXX[0][0][0] = 1.0;
        VYY[0][0][1] = 1.0;
        VZZ[0][0][2] = 1.0;
        VXY[0][0][3] = 1.0;
        VZX[0][0][4] = 1.0;
        VYZ[0][0][5] = 1.0;
     }

     else if(_numberOfInput == 9){ // for 3D case with Lattice Params @Mohib
        VXX = torch::zeros({1, 1, 6});
        VXY = torch::zeros({1, 1, 6});
        VYY = torch::zeros({1, 1, 6});
        VYZ = torch::zeros({1, 1, 6});
        VZZ = torch::zeros({1, 1, 6});
        VZX = torch::zeros({1, 1, 6});

        VXX[0][0][0] = 1.0;
        VYY[0][0][1] = 1.0;
        VZZ[0][0][2] = 1.0;
        VXY[0][0][3] = 1.0;
        VZX[0][0][4] = 1.0;
        VYZ[0][0][5] = 1.0;
     }
#else
  Msg::Error("NOT COMPILED WITH TORCH");
#endif

}

torchANNBasedDG3DMaterialLaw::torchANNBasedDG3DMaterialLaw(const torchANNBasedDG3DMaterialLaw& src):
      dG3DMaterialLaw(src), _numberOfInput(src._numberOfInput), _numberOfInternalVariables(src._numberOfInternalVariables), _initialValue_h(src._initialValue_h),
                _EXXmean(src._EXXmean), _EXXstd(src._EXXstd), _EXYmean(src._EXYmean), _EXYstd(src._EXYstd), _EYYmean(src._EYYmean), _EYYstd(src._EYYstd),
                _EYZmean(src._EYZmean), _EYZstd(src._EYZstd), _EZZmean(src._EZZmean), _EZZstd(src._EZZstd), _EZXmean(src._EZXmean), _EZXstd(src._EZXstd),
                _SXXmean(src._SXXmean), _SXXstd(src._SXXstd), _SXYmean(src._SXYmean), _SXYstd(src._SXYstd), _SYYmean(src._SYYmean), _SYYstd(src._SYYstd),
                _SYZmean(src._SYZmean), _SYZstd(src._SYZstd), _SZZmean(src._SZZmean), _SZZstd(src._SZZstd), _SZXmean(src._SZXmean), _SZXstd(src._SZXstd),
                _tangentByPerturbation(src._tangentByPerturbation), _pertTol(src._pertTol), _kinematicInput(src._kinematicInput), _NeedExtraNorm(src._NeedExtraNorm),_DoubleInput(src._DoubleInput),
                _numberOfExtraInput(src._numberOfExtraInput), _initialValue_ExtraInput(src._initialValue_ExtraInput),
                _normParams_ExtraInput(src._normParams_ExtraInput), _fileExtraInputsGp(src._fileExtraInputsGp),
                _extraVariablesAtGaussPoints(src._extraVariablesAtGaussPoints), _time_arg(src._time_arg), _ctime(src._ctime)
{
#if defined(HAVE_TORCH)
       module = src.module;
       if(_numberOfInput == 3){ // for 2D case
        VXX = src.VXX;
        VXY = src.VXY;
        VYY = src.VYY;
        VZZ = src.VZZ;
       }
       else if(_numberOfInput == 6){ // for 3D case
        VXX = src.VXX;
        VXY = src.VXY;
        VYY = src.VYY;
        VYZ = src.VYZ;
        VZZ = src.VZZ;
        VZX = src.VZX;
       }
       else if(_numberOfInput == 9){ // for 3D case with Lattice Params @Mohib
        VXX = src.VXX;
        VXY = src.VXY;
        VYY = src.VYY;
        VYZ = src.VYZ;
        VZZ = src.VZZ;
        VZX = src.VZX;
       }
#else
  Msg::Error("NOT COMPILED WITH TORCH");
#endif
}
torchANNBasedDG3DMaterialLaw::~torchANNBasedDG3DMaterialLaw()
{
}

void torchANNBasedDG3DMaterialLaw::setKinematicInput(const int i)
{
  if (i == 1)
  {
    _kinematicInput = EGL;
    Msg::Info("using Green Lagrange strain as kinematic variable");
  }
  else if (i == 2)
  {
    _kinematicInput = U;
    Msg::Info("using Biot strain as kinematic variable");
  }
  else if (i == 3)
  {
    _kinematicInput = smallStrain;
    Msg::Info("using small strain (FT+F)/2-I as kinematic variable");
  }
  else if (i == 4)
  {
    _kinematicInput = EGLInc;
    Msg::Info("using Green Lagrange strain increment as kinematic variable");
  }
  else if (i == 5)
  {
    _kinematicInput = UInc;
    Msg::Info("using Biot strain increment as kinematic variable");
  }

  else if (i == 6)
  {
    _kinematicInput = smallStrainInc;
    Msg::Info("using small strain (FT+F)/2-I increment as kinematic variable");
  }
  else
  {
    Msg::Error("input type %d has not been defined",i);
  }
}

// Set number of extra inputs @Mohib
void torchANNBasedDG3DMaterialLaw::setNumExtraInput(const int value){
    _numberOfExtraInput = value;
}

// Set the position of time in the extra input array @Mohib
void torchANNBasedDG3DMaterialLaw::setTimeArg(const int value){
    _time_arg = value;
}

// Set init values for extra inputs @Mohib
void torchANNBasedDG3DMaterialLaw::setInitialExtraInput(const double value){
    _initialValue_ExtraInput.push_back(value);
}

// Set normalization parameters for extra inputs @Mohib
void torchANNBasedDG3DMaterialLaw::setNormExtraInp(const double mean, const double std){
    _normParams_ExtraInput.push_back(mean);
    _normParams_ExtraInput.push_back(std);
}

// Update current time. and access it at GP using ctime (local scope). Necessary for surrogates that are time dependend such as MOAMMM lattices @Mohib
void torchANNBasedDG3DMaterialLaw::setTime(const double ctime, const double dtime){
    dG3DMaterialLaw::setTime(ctime,dtime);
    this->_ctime = ctime;
}

void torchANNBasedDG3DMaterialLaw::initLaws(const std::map<int,materialLaw*> &maplaw){
#if defined(HAVE_TORCH)
    if (!_initialized){
        // Added additional container Extra1 for additonal inputs @Mohib TODO: Remove hardcode
        fullMatrix<double> E1(1,6), S(1,6), Extra1(1, _numberOfExtraInput);
        
        torch::Tensor h_init = _initialValue_h*torch::ones({1, 1, _numberOfInternalVariables});
        
        auto  h_tmp = torch::zeros({1, 1, _numberOfInternalVariables}, torch::requires_grad(false));
        
        fullMatrix<double> DSDE(6,6);

        //Set initial values for extra inputs @Mohib
        for(int i=0; i<_numberOfExtraInput; i++){
            Extra1(0, i) = _initialValue_ExtraInput[i];
        }

        if (_tangentByPerturbation){
           //TODO: Refractor this for Lattice @Mohib
          static fullMatrix<double> E1_plus(1,6), S_plus(1,6);
          if(_numberOfInput==6 || _numberOfInput==3)
          {
              for (int i=0; i< 6; i++)
              {
                 E1_plus = E1;
                 E1_plus(0,i) += _pertTol;

                 RNNstress_stiff(E1, E1_plus, h_init, h_tmp, S_plus, false, DSDE);
                 for (int j=0; j<6; j++)
                 {
                   DSDE(j,i) = (S_plus(0,j) - S(0,j))/_pertTol;
                 }
              }
          }
          if(_numberOfInput>6)
          {
              for (int i=0; i< 6; i++)
              {
                 E1_plus = E1;
                 E1_plus(0,i) += _pertTol;
                 // Matched signature with updated Call that feature enables SCU @Mohib
                 RNNstressGeo_stiff(E1, E1_plus, Extra1, Extra1, h_init, h_tmp, S_plus, false, DSDE);
                 for (int j=0; j<6; j++)
                 {
                   DSDE(j,i) = (S_plus(0,j) - S(0,j))/_pertTol;
                 }
              }

          }

        }
        else
        {
            if(_numberOfInput==6 || _numberOfInput==3)
            {
                RNNstress_stiff(E1, E1, h_init, h_tmp, S, true, DSDE);
            }
            if(_numberOfInput>6)
            {
               // Matched signature with updated Call that feature enables SCU @Mohib
                RNNstressGeo_stiff(E1, E1, Extra1, Extra1, h_init, h_tmp, S, true, DSDE);
            }

        }

        convertFullMatrixToSTensor43(DSDE,elasticStiffness);
        _initialized = true;
    };
#else
  Msg::Error("NOT COMPILED WITH TORCH");
#endif
};


void torchANNBasedDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  std::vector<double> value_ExtraInput=_initialValue_ExtraInput;
  SPoint3 ptGlobal;
  ele->pnt(GP->pt[0],GP->pt[1],GP->pt[2],ptGlobal);
  SVector3 pt(ptGlobal);
  for(int i=0; i<value_ExtraInput.size();i++)
  {
    if(_extraVariablesAtGaussPoints.isStored(i))
    {
      value_ExtraInput[i]=_extraVariablesAtGaussPoints.returnFieldAtClosestLocation(i,pt);
    }
  }
  //Msg::Info("value_ExtraInput %f %f %f", value_ExtraInput[0], value_ExtraInput[1],value_ExtraInput[2]);

  if(iele==NULL) inter=false;
    IPVariable* ipvi = new  torchANNBasedDG3DIPVariable(_numberOfInternalVariables,_initialValue_h,hasBodyForce,inter, _numberOfExtraInput, value_ExtraInput);
    IPVariable* ipv1 = new  torchANNBasedDG3DIPVariable(_numberOfInternalVariables,_initialValue_h,hasBodyForce,inter, _numberOfExtraInput, value_ExtraInput);
    IPVariable* ipv2 = new  torchANNBasedDG3DIPVariable(_numberOfInternalVariables,_initialValue_h,hasBodyForce,inter, _numberOfExtraInput, value_ExtraInput);
    if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void torchANNBasedDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  std::vector<double> value_ExtraInput=_initialValue_ExtraInput;
  SPoint3 ptGlobal;
  ele->pnt(GP->pt[0],GP->pt[1],GP->pt[2],ptGlobal);
  SVector3 pt(ptGlobal);
  for(int i=0; i<value_ExtraInput.size();i++)
  {
    if(_extraVariablesAtGaussPoints.isStored(i))
    {
      value_ExtraInput[i]=_extraVariablesAtGaussPoints.returnFieldAtClosestLocation(i,pt);
    }
  }
  //Msg::Info("value_ExtraInput %f %f %f", value_ExtraInput[0], value_ExtraInput[1],value_ExtraInput[2]);
  ipv = new  torchANNBasedDG3DIPVariable(_numberOfInternalVariables,_initialValue_h,hasBodyForce,inter, _numberOfExtraInput, value_ExtraInput);
}

void torchANNBasedDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
#if defined(HAVE_TORCH)
  /* get ipvariable */
  torchANNBasedDG3DIPVariable* ipvcur = static_cast<torchANNBasedDG3DIPVariable*>(ipv);;
  const torchANNBasedDG3DIPVariable* ipvprev = static_cast<const torchANNBasedDG3DIPVariable*>(ipvp);

  fullMatrix<double>& E1 = ipvcur->getRefToKinematicVariables();
  static fullMatrix<double> E0(1,6);
  // compute E1
  const STensor3& F = ipvcur->getConstRefToDeformationGradient();
  static STensor3 FTF, Ustretch, R, FTF0, Ustretch0, R0;
  if (_kinematicInput == torchANNBasedDG3DMaterialLaw::EGL)
  {
    STensorOperation::multSTensor3FirstTranspose(F,F,FTF);
    E1(0,0) = 0.5*(FTF(0,0)-1.);
    E1(0,1) = 0.5*(FTF(1,1)-1.);
    E1(0,2) = 0.5*(FTF(2,2)-1.);
    E1(0,3) = 0.5*FTF(0,1);
    E1(0,4) = 0.5*FTF(0,2);
    E1(0,5) = 0.5*FTF(1,2);

    if(_NeedExtraNorm){
      const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();
      STensorOperation::multSTensor3FirstTranspose(F0,F0,FTF0);
      E0(0,0) = 0.5*(FTF0(0,0)-1.);
      E0(0,1) = 0.5*(FTF0(1,1)-1.);
      E0(0,2) = 0.5*(FTF0(2,2)-1.);
      E0(0,3) = 0.5*FTF0(0,1);
      E0(0,4) = 0.5*FTF0(0,2);
      E0(0,5) = 0.5*FTF0(1,2);
    }
  }
  else if (_kinematicInput == torchANNBasedDG3DMaterialLaw::U)
  {
    STensorOperation::RUDecomposition(F,Ustretch,R);
    E1(0,0) = Ustretch(0,0)-1.;
    E1(0,1) = Ustretch(1,1)-1.;
    E1(0,2) = Ustretch(2,2)-1.;
    E1(0,3) = Ustretch(0,1);
    E1(0,4) = Ustretch(0,2);
    E1(0,5) = Ustretch(1,2);
  }
  else if (_kinematicInput == torchANNBasedDG3DMaterialLaw::smallStrain)
  {
    E1(0,0) = F(0,0)-1.;
    E1(0,1) = F(1,1)-1.;
    E1(0,2) = F(2,2)-1.;
    E1(0,3) = 0.5*(F(0,1)+F(1,0));
    E1(0,4) = 0.5*(F(0,2)+F(2,0));
    E1(0,5) = 0.5*(F(1,2)+F(2,1));
  }
  else if (_kinematicInput == torchANNBasedDG3DMaterialLaw::EGLInc)
  {
    STensorOperation::multSTensor3FirstTranspose(F,F,FTF);
    const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();
    STensorOperation::multSTensor3FirstTranspose(F0,F0,FTF0);
    E1(0,0) = 0.5*(FTF(0,0)-FTF0(0,0));
    E1(0,1) = 0.5*(FTF(1,1)-FTF0(1,1));
    E1(0,2) = 0.5*(FTF(2,2)-FTF0(2,2));
    E1(0,3) = 0.5*(FTF(0,1)-FTF0(0,1));
    E1(0,4) = 0.5*(FTF(0,2)-FTF0(0,2));
    E1(0,5) = 0.5*(FTF(1,2)-FTF0(1,2));
  }
  else if (_kinematicInput == torchANNBasedDG3DMaterialLaw::UInc)
  {
    STensorOperation::RUDecomposition(F,Ustretch,R);
    const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();
    STensorOperation::RUDecomposition(F0,Ustretch0,R0);
    E1(0,0) = Ustretch(0,0)-Ustretch0(0,0);
    E1(0,1) = Ustretch(1,1)-Ustretch0(1,1);
    E1(0,2) = Ustretch(2,2)-Ustretch0(2,2);
    E1(0,3) = Ustretch(0,1)-Ustretch0(0,1);
    E1(0,4) = Ustretch(0,2)-Ustretch0(0,2);
    E1(0,5) = Ustretch(1,2)-Ustretch0(1,2);
  }
  else if (_kinematicInput == torchANNBasedDG3DMaterialLaw::smallStrainInc)
  {
    const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();
    E1(0,0) = F(0,0)-F0(0,0);
    E1(0,1) = F(1,1)-F0(1,1);
    E1(0,2) = F(2,2)-F0(2,2);
    E1(0,3) = 0.5*(F(0,1)+F(1,0)-F0(0,1)-F0(1,0));
    E1(0,4) = 0.5*(F(0,2)+F(2,0)-F0(0,2)-F0(2,0));
    E1(0,5) = 0.5*(F(1,2)+F(2,1)-F0(1,2)-F0(2,1));
  }
  else
  {
    Msg::Error("kinematic type %d has not been defined",_kinematicInput);
  }

  // Get extra inputs required at previous time step - Necessary to feature enables SCU @Mohib
  const fullMatrix<double>& Extra0 = ipvprev->getConstRefToExtraVariables();

  // Get extra inputs required for torch model @Mohib
  fullMatrix<double>& Extra1 = ipvcur->getRefToExtraVariables();

  if(_time_arg != -1){
   // Set the current time @Mohib
   Extra1(0, _time_arg) = _ctime;
  }
  //Msg::Info("value Extra1 %f %f %f", Extra1(0,0), Extra1(0,1),Extra1(0,2));

  static fullMatrix<double> S(1,6), DSDE(6,6);
  const torch::Tensor& h0 = ipvprev->getConstRefToInternalVariables();
  torch::Tensor& h1 = ipvcur->getRefToInternalVariables();

  if (stiff && _tangentByPerturbation)
  {

    if(_numberOfInput==6 || _numberOfInput==3)
      {
        RNNstress_stiff(E0, E1, h0, h1, S, false, DSDE);
        static fullMatrix<double> E1_plus(1,6),S_plus(1,6);
        auto  h_tmp = torch::zeros({1, 1, _numberOfInternalVariables}, torch::requires_grad(false));

        for (int i=0; i< 6; i++)
        {
          E1_plus = E1;
          E1_plus(0,i) += _pertTol;
          RNNstress_stiff(E0, E1_plus, h0, h_tmp, S_plus, false, DSDE);
          for (int j=0; j<6; j++)
          {
            DSDE(j,i) = (S_plus(0,j) - S(0,j))/_pertTol;
          }
        }
      }
      
      else if(_numberOfInput>6)
      {

        // Matched signature with updated Call that feature enables SCU @Mohib
        RNNstressGeo_stiff(E0, E1, Extra0, Extra1, h0, h1, S, false, DSDE);
        //S.print("original");
        static fullMatrix<double> E1_plus(1,6), S_plus(1,6);
        torch::Tensor h_tmp = torch::zeros({1, 1, _numberOfInternalVariables});
        for (int i=0; i< 6; i++)
        {
          E1_plus = E1;
          E1_plus(0,i) += _pertTol;

          // Matched signature with updated Call that feature enables SCU @Mohib
          RNNstressGeo_stiff(E0, E1_plus, Extra0, Extra1, h0, h_tmp, S_plus, false, DSDE);
          
          //S_plus.print("perturbated");
          for (int j=0; j<6; j++)
          {
            DSDE(j,i) = (S_plus(0,j) - S(0,j))/_pertTol;
          }
        }

      }
  }
  else
  {
      if(_numberOfInput==3 || _numberOfInput==6){
          RNNstress_stiff(E0, E1, h0, h1, S, true, DSDE);
      }
      else
      {
        // Matched signature with updated Call that feature enables SCU @Mohib
        RNNstressGeo_stiff(E0, E1, Extra0, Extra1, h0, h1, S, true, DSDE);
      }

  }

  if (_kinematicInput == smallStrain || _kinematicInput== smallStrainInc  )
  {
    // small strain
    STensor3& P = ipvcur->getRefToFirstPiolaKirchhoffStress();
    P(0,0) = S(0,0);
    P(1,1) = S(0,1);
    P(2,2) = S(0,2);
    P(0,1) = S(0,3);
    P(1,0) = S(0,3);
    P(0,2) = S(0,4);
    P(2,0) = S(0,4);
    P(1,2) = S(0,5);
    P(2,1) = S(0,5);
    if (stiff)
    {
      STensor43& L = ipvcur->getRefToTangentModuli();
      convertFullMatrixToSTensor43(DSDE,L);
    }
  }
  else
  {
    // get stress
    // output is S00, S11, S22, S01, S02, S12
    static STensor3 secondPK;
    secondPK(0,0) = S(0,0);
    secondPK(1,1) = S(0,1);
    secondPK(2,2) = S(0,2);
    secondPK(0,1) = S(0,3);
    secondPK(1,0) = S(0,3);
    secondPK(0,2) = S(0,4);
    secondPK(2,0) = S(0,4);
    secondPK(1,2) = S(0,5);
    secondPK(2,1) = S(0,5);

    STensor3& P = ipvcur->getRefToFirstPiolaKirchhoffStress();
    STensorOperation::multSTensor3(F,secondPK,P);

    if (stiff)
    {
      static STensor43 DsecondPKDE;
      convertFullMatrixToSTensor43(DSDE,DsecondPKDE);

      STensor43& L = ipvcur->getRefToTangentModuli();
      STensorOperation::zero(L);

      static STensor43 DsecondPKDEGreenLagrange;
      if (_kinematicInput == torchANNBasedDG3DMaterialLaw::EGL || _kinematicInput == torchANNBasedDG3DMaterialLaw::EGLInc)
      {
        DsecondPKDEGreenLagrange = DsecondPKDE;
      }
      else if (_kinematicInput == torchANNBasedDG3DMaterialLaw::U || _kinematicInput == torchANNBasedDG3DMaterialLaw::UInc)
      {
        static STensor3 invU;
        STensorOperation::inverseSTensor3(Ustretch,invU);
        for (int i=0; i<3; i++)
        {
          for (int j=0; j<3; j++)
          {
            for (int k=0; k<3; k++)
            {
              for (int l=0; l<3; l++)
              {
                DsecondPKDEGreenLagrange(i,j,k,l) = 0.;
                for (int m=0; m< 3; m++)
                {
                  DsecondPKDEGreenLagrange(i,j,k,l) += invU(k,m)*DsecondPKDE(i,j,m,l);
                }
              }
            }
          }
        }
      }
      for (int i=0; i<3; i++)
      {
        for (int j=0; j<3; j++)
        {
          for (int k=0; k<3; k++)
          {
            L(i,j,i,k) += secondPK(k,j);
            for (int l=0; l<3; l++)
            {
              //
              for (int a=0; a<3; a++)
              {
                for (int b=0; b<3; b++)
                {
                  L(i,j,k,l) += F(i,a)*DsecondPKDEGreenLagrange(a,j,l,b)*F(k,b);
                }
              }
            }
          }
        }
      }
    };
  }
  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);
#else
  Msg::Error("NOT COMPILED WITH TORCH");
#endif
};
#if defined(HAVE_TORCH)
void torchANNBasedDG3DMaterialLaw::RNNstress_stiff(const fullMatrix<double>& E0, const fullMatrix<double>& E1, const torch::Tensor& h0, torch::Tensor& h1,
                                      fullMatrix<double>& S, const bool stiff, fullMatrix<double>& DSDE)
{
   vector<float> E_vec(_numberOfInput), E0_vec(_numberOfInput);
   torch::Tensor E_norm, E0_norm, dE_norm;
   if(!stiff){torch::NoGradGuard no_grad;}
   // use RNN to predict stress-------------
   vector<torch::jit::IValue> inputs;
   Normalize_strain(E1, E_vec);
   if(_NeedExtraNorm){
      Normalize_strain(E0, E0_vec);
   }
   if(_NeedExtraNorm and _DoubleInput){
      if(stiff){
         E0_norm = torch::from_blob(E0_vec.data(), {1,1, _numberOfInput}, torch::requires_grad());
      }
      else{
         E0_norm = torch::from_blob(E0_vec.data(), {1,1, _numberOfInput}, torch::requires_grad(false));
      }
      inputs.push_back(E0_norm);
   }
   else{
      if(stiff){
         E_norm = torch::from_blob(E_vec.data(), {1,1, _numberOfInput}, torch::requires_grad());
      }
      else{
         E_norm = torch::from_blob(E_vec.data(), {1,1, _numberOfInput}, torch::requires_grad(false));
      }
      inputs.push_back(E_norm);
   }


   vector<float> norm_E1(1);
   vector<float> DE_vec(_numberOfInput);
   torch::Tensor norm;
   if(_kinematicInput == torchANNBasedDG3DMaterialLaw::EGLInc or _NeedExtraNorm){
       if(_kinematicInput == torchANNBasedDG3DMaterialLaw::EGL and _NeedExtraNorm){
          for (int i = 0; i <_numberOfInput; i++) {
            DE_vec[i] = E_vec[i]-E0_vec[i];
          }
          if(_DoubleInput){
            if(stiff){
               dE_norm = torch::from_blob(DE_vec.data(), {1,1, _numberOfInput}, torch::requires_grad());
            }
            else{
               dE_norm = torch::from_blob(DE_vec.data(), {1,1, _numberOfInput}, torch::requires_grad(false));
            }
          inputs.push_back(dE_norm);
          }
          norm_E1[0] = sqrt(std::inner_product(DE_vec.begin(), DE_vec.end(), DE_vec.begin(), 0.0));
       }
       else{
          norm_E1[0] = E1.norm();
       }

      if(stiff){
        norm = torch::from_blob(norm_E1.data(), {1,1}, torch::requires_grad());
      }
      else{
        norm = torch::from_blob(norm_E1.data(), {1,1}, torch::requires_grad(false));
      }
      inputs.push_back(norm);
   }
   inputs.push_back(h0);

   auto outputs= module.forward(inputs).toTuple();
   torch::Tensor S_norm = outputs->elements()[0].toTensor();
   h1 = outputs->elements()[1].toTensor();

   InverseNormalize_stress(S_norm, S);

   if(stiff){
     for (int i=0; i<6; i++){
        for (int j=0; j<6; j++){
           DSDE(i,j) = 0.0;
        }
     }

     if(_kinematicInput != torchANNBasedDG3DMaterialLaw::EGLInc and !_NeedExtraNorm){
       if(_numberOfInput == 3){
         S_norm.backward(VXX,true);           //  dS/dE
         auto EnormGrad_a = E_norm.grad().accessor<float,3>();
         DSDE(0,0) = EnormGrad_a[0][0][0];
         DSDE(0,1) = EnormGrad_a[0][0][2];
         DSDE(0,3) = EnormGrad_a[0][0][1];
         E_norm.grad().zero_();
         S_norm.backward(VYY,true);              //  dS/dE
         DSDE(1,0) = EnormGrad_a[0][0][0];
         DSDE(1,1) = EnormGrad_a[0][0][2];
         DSDE(1,3) = EnormGrad_a[0][0][1];
         E_norm.grad().zero_();
         S_norm.backward(VZZ,true);                   //  dS/dE
         DSDE(2,0) = EnormGrad_a[0][0][0];
         DSDE(2,1) = EnormGrad_a[0][0][2];
         DSDE(2,3) = EnormGrad_a[0][0][1];
         E_norm.grad().zero_();
         S_norm.backward(VXY,true);                    //  dS/dE
         DSDE(3,0) = EnormGrad_a[0][0][0];
         DSDE(3,1) = EnormGrad_a[0][0][2];
         DSDE(3,3) = EnormGrad_a[0][0][1];
         E_norm.grad().zero_();
       }

       else if(_numberOfInput == 6){
         S_norm.backward(VXX,true);                   //  dS/dE
         auto EnormGrad_a = E_norm.grad().accessor<float,3>();
         for (int i=0; i<6; i++){
             DSDE(0,i) = EnormGrad_a[0][0][i];
         }
         E_norm.grad().zero_();
         S_norm.backward(VYY,true);                   //  dS/dE
         for (int i=0; i<6; i++){
             DSDE(1,i) = EnormGrad_a[0][0][i];
         }
         E_norm.grad().zero_();
         S_norm.backward(VZZ,true);                   //  dS/dE
         for (int i=0; i<6; i++){
             DSDE(2,i) = EnormGrad_a[0][0][i];
         }
         E_norm.grad().zero_();
         S_norm.backward(VXY,true);                   //  dS/dE
         for (int i=0; i<6; i++){
             DSDE(3,i) = EnormGrad_a[0][0][i];
         }
         E_norm.grad().zero_();
         S_norm.backward(VZX,true);                   //  dS/dE
         for (int i=0; i<6; i++){
             DSDE(4,i) = EnormGrad_a[0][0][i];
         }
         E_norm.grad().zero_();
         S_norm.backward(VYZ,true);                   //  dS/dE
         for (int i=0; i<6; i++){
             DSDE(5,i) = EnormGrad_a[0][0][i];
         }
         E_norm.grad().zero_();
       }
     }
     else{
       double norm_0 = 1.0;
       static vector<float> E_n0(_numberOfInput, 1.0);
       if(norm_E1[0]>1.0e-15){
         if(!_NeedExtraNorm){
           norm_0 = norm_E1[0];
           E_n0 = E_vec;
         }
         else{
           for (int i = 0; i < E_n0.size(); ++i){
             E_n0[i] = E_vec[i]/norm_E1[0];
           }
         }
       }
       if(_numberOfInput == 3){
         S_norm.backward(VXX,true);           //  dS/dE
         auto EnormGrad_a = E_norm.grad().accessor<float,3>();
         auto normGrad = norm.grad().accessor<float,2>();
         DSDE(0,0) = EnormGrad_a[0][0][0]/norm_0 + normGrad[0][0]*E_n0[0];
         DSDE(0,1) = EnormGrad_a[0][0][2]/norm_0 + normGrad[0][0]*E_n0[2];
         DSDE(0,3) = EnormGrad_a[0][0][1]/norm_0 + normGrad[0][0]*E_n0[1];

         E_norm.grad().zero_();
         norm.grad().zero_();
         S_norm.backward(VYY,true);              //  dS/dE
         DSDE(1,0) = EnormGrad_a[0][0][0]/norm_0 + normGrad[0][0]*E_n0[0];
         DSDE(1,1) = EnormGrad_a[0][0][2]/norm_0 + normGrad[0][0]*E_n0[2];
         DSDE(1,3) = EnormGrad_a[0][0][1]/norm_0 + normGrad[0][0]*E_n0[1];

         E_norm.grad().zero_();
         norm.grad().zero_();
         S_norm.backward(VZZ,true);                   //  dS/dE
         DSDE(2,0) = EnormGrad_a[0][0][0]/norm_0 + normGrad[0][0]*E_n0[0];
         DSDE(2,1) = EnormGrad_a[0][0][2]/norm_0 + normGrad[0][0]*E_n0[2];
         DSDE(2,3) = EnormGrad_a[0][0][1]/norm_0 + normGrad[0][0]*E_n0[1];

         E_norm.grad().zero_();
         norm.grad().zero_();
         S_norm.backward(VXY,true);                    //  dS/dE
         DSDE(3,0) = EnormGrad_a[0][0][0]/norm_0 + normGrad[0][0]*E_n0[0];
         DSDE(3,1) = EnormGrad_a[0][0][2]/norm_0 + normGrad[0][0]*E_n0[2];
         DSDE(3,3) = EnormGrad_a[0][0][1]/norm_0 + normGrad[0][0]*E_n0[1];
         E_norm.grad().zero_();
         norm.grad().zero_();
       }

       else if(_numberOfInput == 6){
         S_norm.backward(VXX,true);                   //  dS/dE
         auto EnormGrad_a = E_norm.grad().accessor<float,3>();
         auto normGrad = norm.grad().accessor<float,2>();
         for (int i=0; i<6; i++){
             DSDE(0,i) = EnormGrad_a[0][0][i]/norm_0 + normGrad[0][0]*E_n0[i];
         }
         E_norm.grad().zero_();
         norm.grad().zero_();
         S_norm.backward(VYY,true);                   //  dS/dE
         for (int i=0; i<6; i++){
             DSDE(1,i) = EnormGrad_a[0][0][i]/norm_0 + normGrad[0][0]*E_n0[i];
         }
         E_norm.grad().zero_();
         norm.grad().zero_();
         S_norm.backward(VZZ,true);                   //  dS/dE
         for (int i=0; i<6; i++){
             DSDE(2,i) = EnormGrad_a[0][0][i]/norm_0 + normGrad[0][0]*E_n0[i];
         }
         E_norm.grad().zero_();
         norm.grad().zero_();
         S_norm.backward(VXY,true);                   //  dS/dE
         for (int i=0; i<6; i++){
             DSDE(3,i) = EnormGrad_a[0][0][i]/norm_0 + normGrad[0][0]*E_n0[i];
         }
         E_norm.grad().zero_();
         norm.grad().zero_();
         S_norm.backward(VZX,true);                   //  dS/dE
         for (int i=0; i<6; i++){
             DSDE(4,i) = EnormGrad_a[0][0][i]/norm_0 + normGrad[0][0]*E_n0[i];
         }
         E_norm.grad().zero_();
         norm.grad().zero_();
         S_norm.backward(VYZ,true);                   //  dS/dE
         for (int i=0; i<6; i++){
             DSDE(5,i) = EnormGrad_a[0][0][i]/norm_0 + normGrad[0][0]*E_n0[i];
         }
         E_norm.grad().zero_();
         norm.grad().zero_();
       }
     }


     for (int i=0; i<6; i++){
      DSDE(i,0) = DSDE(i,0)/_EXXstd;
      DSDE(i,1) = DSDE(i,1)/_EYYstd;
      DSDE(i,2) = DSDE(i,2)/_EZZstd;
      DSDE(i,3) = DSDE(i,3)/_EXYstd;
      DSDE(i,4) = DSDE(i,4)/_EZXstd;
      DSDE(i,5) = DSDE(i,5)/_EYZstd;
     }
     for (int j=0; j<6; j++){
      DSDE(0,j) = DSDE(0,j)*_SXXstd;
      DSDE(1,j) = DSDE(1,j)*_SYYstd;
      DSDE(2,j) = DSDE(2,j)*_SZZstd;
      DSDE(3,j) = DSDE(3,j)*_SXYstd;
      DSDE(4,j) = DSDE(4,j)*_SZXstd;
      DSDE(5,j) = DSDE(5,j)*_SYZstd;
     }


   /*    for( i=0;i<6; i++){
        cout << DSDE(i,0)<< " " << DSDE(i,1)<<" " << DSDE(i,2)<<" " << DSDE(i,3)<<" " << DSDE(i,4)<<" " << DSDE(i,5)<< endl;
       }
         cout << "--------------------------------- "<< endl;*/
   }
}
#endif



#if defined(HAVE_TORCH)
// subroutine for computing stress and stiffness using extra inputs (other than Strain and internal variables) @Mohib TODO: Rename
//appended arguments with E0 and Extra0 to feature enable SCU @Mohib
void torchANNBasedDG3DMaterialLaw::RNNstressGeo_stiff(const fullMatrix<double>& E0, const fullMatrix<double>& E1, const fullMatrix<double>& Extra0, const fullMatrix<double>& Extra1, const torch::Tensor& h0, torch::Tensor& h1,
                                      fullMatrix<double>& S, const bool stiff, fullMatrix<double>& DSDE)
{

   // Containers for Strain and extra inputs @ Mohib
   vector<float> E_vec(_numberOfInput - _numberOfExtraInput);
   // Appended E0 to feature enable SCU @Mohib
   vector<float> E0_vec(_numberOfInput - _numberOfExtraInput);
   vector<float> Extra_vec(_numberOfExtraInput);
   // Appended Extra0_vec to feature enable SCU @Mohib
   vector<float> Extra0_vec(_numberOfExtraInput);
   // Container for combined input entries(Extra + Input) @ Mohib
   // Dont pre initialize Combine_vec! It needs to be populated as it grows @Mohib
   vector<float> Combine_vec;
   // Container for  normalized combined input exteries(Extra + Input) @ Mohib
   torch::Tensor Combine_norm;
   
   // Appended Container for combined input0 entries(Extra + Input) to feature enable SCU @ Mohib
   // Dont pre initialize Combine0_vec! It needs to be populated as it grows @Mohib
   vector<float> Combine0_vec;
   // Container for  normalized combined input exteries(Extra + Input) @ Mohib
   torch::Tensor Combine0_norm;

   // @Mohib TODO: Remove b4 commit
   torch::Tensor E_norm;
   // Appended E0 and dE_norm to feature enable SCU
   torch::Tensor E0_norm, dE_norm;

   //static torch::Tensor Extra_norm;
   if(!stiff){torch::NoGradGuard no_grad;}
   
   // use RNN to predict stress-------------
   // Declare storage container for inputs to torch @Mohib
   vector<torch::jit::IValue> inputs;

   // Normalize strains E1 and store in E_vec @Mohib
   Normalize_strain(E1, E_vec);
   // Normalize extra inputs Extra1 and store in Extra_vec @Mohib
   Normalize_geo(Extra1, Extra_vec);
   
   if(_NeedExtraNorm){
      // Normalize strains at previous time step and store in E0_vec. Necessary for feature enabling SCU @Mohib
      Normalize_strain(E0, E0_vec);
      // Normalize extra inputs at previous time step and store in Extra0_vec. Necessary for feature enabling SCU @Mohib
      Normalize_geo(Extra0, Extra0_vec);

      // Populate the container "Combine_vec0" with the normalized quantities in the correct order.
      // Order is : Radius + E0 for SCU. Last entry in Extra0_vec contains time and isnt needed here @Mohib.
      Combine0_vec.insert(Combine0_vec.end(), Extra0_vec.begin(), Extra0_vec.end() - 1);
      Combine0_vec.insert(Combine0_vec.end(), E0_vec.begin(), E0_vec.end());

   }

   if(_NeedExtraNorm and _DoubleInput){

      // Populate the container "Combine_vec" with the normalized quantities in the correct order.
      // Order is : Radius + E for SCU . Last entry in Extra_vec contains time and isnt needed here @Mohib.
      Combine_vec.insert(Combine_vec.end(), Extra_vec.begin(), Extra_vec.end() - 1);
      Combine_vec.insert(Combine_vec.end(), E_vec.begin(), E_vec.end());

      if(stiff){
         E0_norm = torch::from_blob(E0_vec.data(), {1,1, _numberOfInput - _numberOfExtraInput}, torch::requires_grad());
         Combine0_norm = torch::from_blob(Combine0_vec.data(), {1,1, _numberOfInput - 1}, torch::requires_grad());
      }
      else{
         E0_norm = torch::from_blob(E0_vec.data(), {1,1, _numberOfInput - _numberOfExtraInput}, torch::requires_grad(false));
         Combine0_norm = torch::from_blob(Combine0_vec.data(), {1,1, _numberOfInput - 1}, torch::requires_grad(false));
      } 
      inputs.push_back(Combine0_norm);      
   }
   else{

      // Populate the container "Combine_vec" with the normalized quantities in the correct order.
      // Order is : Radius, Size, TSTEP + E for GRU @Mohib.
      Combine_vec.insert(Combine_vec.end(), Extra_vec.begin(), Extra_vec.end());
      Combine_vec.insert(Combine_vec.end(), E_vec.begin(), E_vec.end());

      if(stiff){
         E_norm = torch::from_blob(E_vec.data(), {1,1, _numberOfInput - _numberOfExtraInput}, torch::requires_grad());
         Combine_norm = torch::from_blob(Combine_vec.data(), {1,1, _numberOfInput}, torch::requires_grad());
      }
      else{
         E_norm = torch::from_blob(E_vec.data(), {1,1, _numberOfInput - _numberOfExtraInput}, torch::requires_grad(false));
         Combine_norm = torch::from_blob(Combine_vec.data(), {1,1, _numberOfInput}, torch::requires_grad(false));
      }
      inputs.push_back(Combine_norm);
   }

  //  // @Mohib TODO: fix 3 and 6 cases
  //  E_norm = torch::from_blob(E_vec.data(), {1,1, _numberOfInput - _numOfExtra}, torch::requires_grad());
   
   // added norm of E1 and delta E_vec to feature enable SCU @Mohib
   vector<float> norm_E1(1);
   // delta DE_vec doesnt include time so decrementing by 1 @Mohib.
   vector<float> DE_vec(_numberOfInput - 1);
   torch::Tensor norm;  
  
   if(_kinematicInput == torchANNBasedDG3DMaterialLaw::EGLInc or _NeedExtraNorm){     
       if(_kinematicInput == torchANNBasedDG3DMaterialLaw::EGL and _NeedExtraNorm){           
          for (int i = 0; i <_numberOfInput - 1; i++) {
            // DE_vec[i] = E_vec[i]-E0_vec[i]; #TODO: Remove b4 Commit @Mohib
            DE_vec[i] = Combine_vec[i]- Combine0_vec[i];
          }
          if(_DoubleInput){
            if(stiff){
               dE_norm = torch::from_blob(DE_vec.data(), {1,1, _numberOfInput - 1}, torch::requires_grad());
            }
            else{
               dE_norm = torch::from_blob(DE_vec.data(), {1,1, _numberOfInput - 1}, torch::requires_grad(false));
            }  
          inputs.push_back(dE_norm); 
          }   
          norm_E1[0] = sqrt(std::inner_product(DE_vec.begin(), DE_vec.end(), DE_vec.begin(), 0.0));            
       } 
       else{
          fullMatrix<double> CombineE1(1, _numberOfInput - 1);
          
          
          for (int i = 0; i < _numberOfExtraInput - 1; i++)
          {
            CombineE1(0, i) = Extra1(0, i);
          }

          for (int i = 0; i < _numberOfInput - _numberOfExtraInput; i++)
          {
            CombineE1(0, _numberOfExtraInput - 1 + i) = E1(0, i);
          }
          //norm_E1[0] = E1.norm(); #TODO: Remove b4 Commit @Mohib
          norm_E1[0] = CombineE1.norm();
       }       
            
      if(stiff){
        norm = torch::from_blob(norm_E1.data(), {1,1,1}, torch::requires_grad());
      }
      else{
        norm = torch::from_blob(norm_E1.data(), {1,1,1}, torch::requires_grad(false));
      }    
      inputs.push_back(norm);  
   }

   // added norm of dt and dt to feature enable SCU @Mohib
   vector<float> dt_vec(1);
   torch::Tensor dt_norm; 

   if(_kinematicInput == torchANNBasedDG3DMaterialLaw::EGLInc or _NeedExtraNorm){     
       if(_kinematicInput == torchANNBasedDG3DMaterialLaw::EGL and _NeedExtraNorm){   
              // For SCU dt needs to be seperately normalized. @Mohib
              Normalize_dt(Extra1, Extra_vec);
              Normalize_dt(Extra0, Extra0_vec);

              dt_vec[0] = Extra_vec[_numberOfExtraInput - 1] - Extra0_vec[_numberOfExtraInput - 1];


              if(stiff){
               dt_norm = torch::from_blob(dt_vec.data(), {1,1,1}, torch::requires_grad());
              }
              else{
               dt_norm = torch::from_blob(dt_vec.data(), {1,1,1}, torch::requires_grad(false));
            }  
          inputs.push_back(dt_norm); 
       }
   }

   inputs.push_back(h0);

   auto outputs= module.forward(inputs).toTuple();
   torch::Tensor S_norm = outputs->elements()[0].toTensor();
   h1 = outputs->elements()[1].toTensor();

   InverseNormalize_stress(S_norm, S);

   if(stiff){
     for (int i=0; i<6; i++){
        for (int j=0; j<6; j++){
           DSDE(i,j) = 0.0;
        }
     }
     if(_numberOfInput == 3){
         S_norm.backward(VXX,true);                   //  dS/dE
         auto EnormGrad_a = E_norm.grad().accessor<float,3>();
         DSDE(0,0) = EnormGrad_a[0][0][0];
         DSDE(0,1) = EnormGrad_a[0][0][2];
         DSDE(0,3) = EnormGrad_a[0][0][1];
         E_norm.grad().zero_();
         S_norm.backward(VYY,true);              //  dS/dE
         DSDE(1,0) = EnormGrad_a[0][0][0];
         DSDE(1,1) = EnormGrad_a[0][0][2];
         DSDE(1,3) = EnormGrad_a[0][0][1];
         E_norm.grad().zero_();
         S_norm.backward(VZZ,true);                   //  dS/dE
         DSDE(2,0) = EnormGrad_a[0][0][0];
         DSDE(2,1) = EnormGrad_a[0][0][2];
         DSDE(2,3) = EnormGrad_a[0][0][1];
         E_norm.grad().zero_();
         S_norm.backward(VXY,true);                    //  dS/dE
         DSDE(3,0) = EnormGrad_a[0][0][0];
         DSDE(3,1) = EnormGrad_a[0][0][2];
         DSDE(3,3) = EnormGrad_a[0][0][1];
         E_norm.grad().zero_();

       }
       else if(_numberOfInput >= 6){
         S_norm.backward(VXX,true);                   //  dS/dE
         auto EnormGrad_a = Combine_norm.grad().accessor<float,3>();
//         DSDE(0,0) = EnormGrad_a[0][0][12];
//         DSDE(0,1) = EnormGrad_a[0][0][14];
//         DSDE(0,2) = EnormGrad_a[0][0][20];
//         DSDE(0,3) = EnormGrad_a[0][0][24];
//         DSDE(0,4) = EnormGrad_a[0][0][30];
//         DSDE(0,5) = EnormGrad_a[0][0][32];
         for (int i=0; i<6; i++){
             DSDE(0,i) = EnormGrad_a[0][0][i];
//             printf("%3.5f \n", EnormGrad_a[0][0][0]);
//             printf("%3.5f \n", EnormGrad_a[0][0][1]);
//             printf("%3.5f \n", EnormGrad_a[0][0][2]);
//             printf("%3.5f \n", EnormGrad_a[0][0][3]);
//             printf("%3.5f \n", EnormGrad_a[0][0][4]);
//             printf("%3.5f \n", EnormGrad_a[0][0][5]);
//             printf("%3.5f \n", EnormGrad_a[0][0][6]);
//             printf("%3.5f \n", EnormGrad_a[0][0][7]);
//             printf("%3.5f \n", EnormGrad_a[0][0][8]);
//             printf("%3.5f \n", EnormGrad_a[0][0][9]);
//             printf("%3.5f \n", EnormGrad_a[0][0][10]);
//             printf("%3.5f \n", EnormGrad_a[0][0][11]);
//             printf("%3.5f \n", EnormGrad_a[0][0][12]);
//             printf("%3.5f \n", EnormGrad_a[0][0][13]);
//             printf("%3.5f \n", EnormGrad_a[0][0][14]);
//             printf("%3.5f \n", EnormGrad_a[0][0][15]);
//             printf("%3.5f \n", EnormGrad_a[0][0][16]);
//             printf("%3.5f \n", EnormGrad_a[0][0][17]);
//             printf("%3.5f \n", EnormGrad_a[0][0][18]);
//             printf("%3.5f \n", EnormGrad_a[0][0][19]);
//             printf("%3.5f \n", EnormGrad_a[0][0][20]);
//             printf("%3.5f \n", EnormGrad_a[0][0][21]);
//             printf("%3.5f \n", EnormGrad_a[0][0][22]);
//             printf("%3.5f \n", EnormGrad_a[0][0][23]);
//             printf("%3.5f \n", EnormGrad_a[0][0][24]);
//             printf("%3.5f \n", EnormGrad_a[0][0][25]);
//             printf("%3.5f \n", EnormGrad_a[0][0][26]);
//             printf("%3.5f \n", EnormGrad_a[0][0][27]);
//             printf("%3.5f \n", EnormGrad_a[0][0][28]);
//             printf("%3.5f \n", EnormGrad_a[0][0][29]);
//             printf("%3.5f \n", EnormGrad_a[0][0][30]);
//             printf("%3.5f \n", EnormGrad_a[0][0][31]);
//             printf("%3.5f \n", EnormGrad_a[0][0][32]);
//             printf("%3.5f \n", EnormGrad_a[0][0][33]);
//             printf("%3.5f \n", EnormGrad_a[0][0][34]);
//             printf("%3.5f \n", EnormGrad_a[0][0][35]);
//             printf("%3.5f \n", EnormGrad_a[0][0][36]);
//             printf("%3.5f \n", EnormGrad_a[0][0][37]);
//             printf("%3.5f \n", EnormGrad_a[0][0][38]);
//             printf("%3.5f \n", EnormGrad_a[0][0][39]);
//
//             printf("\n---------------\n");
         }
         Combine_norm.grad().zero_();
         S_norm.backward(VYY,true);                   //  dS/dE
         for (int i=0; i<6; i++){
             DSDE(1,i) = EnormGrad_a[0][0][i+12];
         }
         Combine_norm.grad().zero_();
         S_norm.backward(VZZ,true);                   //  dS/dE
         for (int i=0; i<6; i++){
             DSDE(2,i) = EnormGrad_a[0][0][i+12];
         }
         Combine_norm.grad().zero_();
         S_norm.backward(VXY,true);                   //  dS/dE
         for (int i=0; i<6; i++){
             DSDE(3,i) = EnormGrad_a[0][0][i+12];
         }

         Combine_norm.grad().zero_();
         S_norm.backward(VZX,true);                   //  dS/dE
         for (int i=0; i<6; i++){
             DSDE(4,i) = EnormGrad_a[0][0][i+12];
         }
         Combine_norm.grad().zero_();
         S_norm.backward(VYZ,true);                   //  dS/dE
         for (int i=0; i<6; i++){
             DSDE(5,i) = EnormGrad_a[0][0][i+1];
         }
         Combine_norm.grad().zero_();
      }


      for (int i=0; i<6; i++){
         DSDE(i,0) = DSDE(i,0)/_EXXstd;
         DSDE(i,1) = DSDE(i,1)/_EYYstd;
         DSDE(i,2) = DSDE(i,2)/_EZZstd;
         DSDE(i,3) = DSDE(i,3)/_EXYstd;
         DSDE(i,4) = DSDE(i,4)/_EZXstd;
         DSDE(i,5) = DSDE(i,5)/_EYZstd;
      }
      for (int j=0; j<6; j++){
         DSDE(0,j) = DSDE(0,j)*_SXXstd;
         DSDE(1,j) = DSDE(1,j)*_SYYstd;
         DSDE(2,j) = DSDE(2,j)*_SZZstd;
         DSDE(3,j) = DSDE(3,j)*_SXYstd;
         DSDE(4,j) = DSDE(4,j)*_SZXstd;
         DSDE(5,j) = DSDE(5,j)*_SYZstd;
      }


   /*    for( i=0;i<6; i++){
        cout << DSDE(i,0)<< " " << DSDE(i,1)<<" " << DSDE(i,2)<<" " << DSDE(i,3)<<" " << DSDE(i,4)<<" " << DSDE(i,5)<< endl;
       }
         cout << "--------------------------------- "<< endl;*/
   }
}
#endif

void torchANNBasedDG3DMaterialLaw::convertFullMatrixToSTensor43(const fullMatrix<double>& DSDE, STensor43& DsecondPKDE) const
{
  if (DSDE.size1() != 6 or DSDE.size2() != 6)
  {
    Msg::Error("data is not compatible ANNBasedDG3DMaterialLaw:convertFullMatrixToSTensor43");
    return;
  }
  DsecondPKDE(0,0,0,0) = DSDE(0,0);
  DsecondPKDE(0,0,1,1) = DSDE(0,1);
  DsecondPKDE(0,0,2,2) = DSDE(0,2);
  DsecondPKDE(0,0,0,1) = 0.5*DSDE(0,3);
  DsecondPKDE(0,0,1,0) = 0.5*DSDE(0,3);
  DsecondPKDE(0,0,0,2) = 0.5*DSDE(0,4);
  DsecondPKDE(0,0,2,0) = 0.5*DSDE(0,4);
  DsecondPKDE(0,0,1,2) = 0.5*DSDE(0,5);
  DsecondPKDE(0,0,2,1) = 0.5*DSDE(0,5);

  DsecondPKDE(1,1,0,0) = DSDE(1,0);
  DsecondPKDE(1,1,1,1) = DSDE(1,1);
  DsecondPKDE(1,1,2,2) = DSDE(1,2);
  DsecondPKDE(1,1,0,1) = 0.5*DSDE(1,3);
  DsecondPKDE(1,1,1,0) = 0.5*DSDE(1,3);
  DsecondPKDE(1,1,0,2) = 0.5*DSDE(1,4);
  DsecondPKDE(1,1,2,0) = 0.5*DSDE(1,4);
  DsecondPKDE(1,1,1,2) = 0.5*DSDE(1,5);
  DsecondPKDE(1,1,2,1) = 0.5*DSDE(1,5);

  DsecondPKDE(2,2,0,0) = DSDE(2,0);
  DsecondPKDE(2,2,1,1) = DSDE(2,1);
  DsecondPKDE(2,2,2,2) = DSDE(2,2);
  DsecondPKDE(2,2,0,1) = 0.5*DSDE(2,3);
  DsecondPKDE(2,2,1,0) = 0.5*DSDE(2,3);
  DsecondPKDE(2,2,0,2) = 0.5*DSDE(2,4);
  DsecondPKDE(2,2,2,0) = 0.5*DSDE(2,4);
  DsecondPKDE(2,2,1,2) = 0.5*DSDE(2,5);
  DsecondPKDE(2,2,2,1) = 0.5*DSDE(2,5);

  DsecondPKDE(0,1,0,0) = DSDE(3,0);
  DsecondPKDE(0,1,1,1) = DSDE(3,1);
  DsecondPKDE(0,1,2,2) = DSDE(3,2);
  DsecondPKDE(0,1,0,1) = 0.5*DSDE(3,3);
  DsecondPKDE(0,1,1,0) = 0.5*DSDE(3,3);
  DsecondPKDE(0,1,0,2) = 0.5*DSDE(3,4);
  DsecondPKDE(0,1,2,0) = 0.5*DSDE(3,4);
  DsecondPKDE(0,1,1,2) = 0.5*DSDE(3,5);
  DsecondPKDE(0,1,2,1) = 0.5*DSDE(3,5);

  DsecondPKDE(1,0,0,0) = DSDE(3,0);
  DsecondPKDE(1,0,1,1) = DSDE(3,1);
  DsecondPKDE(1,0,2,2) = DSDE(3,2);
  DsecondPKDE(1,0,0,1) = 0.5*DSDE(3,3);
  DsecondPKDE(1,0,1,0) = 0.5*DSDE(3,3);
  DsecondPKDE(1,0,0,2) = 0.5*DSDE(3,4);
  DsecondPKDE(1,0,2,0) = 0.5*DSDE(3,4);
  DsecondPKDE(1,0,1,2) = 0.5*DSDE(3,5);
  DsecondPKDE(1,0,2,1) = 0.5*DSDE(3,5);

  DsecondPKDE(0,2,0,0) = DSDE(4,0);
  DsecondPKDE(0,2,1,1) = DSDE(4,1);
  DsecondPKDE(0,2,2,2) = DSDE(4,2);
  DsecondPKDE(0,2,0,1) = 0.5*DSDE(4,3);
  DsecondPKDE(0,2,1,0) = 0.5*DSDE(4,3);
  DsecondPKDE(0,2,0,2) = 0.5*DSDE(4,4);
  DsecondPKDE(0,2,2,0) = 0.5*DSDE(4,4);
  DsecondPKDE(0,2,1,2) = 0.5*DSDE(4,5);
  DsecondPKDE(0,2,2,1) = 0.5*DSDE(4,5);

  DsecondPKDE(2,0,0,0) = DSDE(4,0);
  DsecondPKDE(2,0,1,1) = DSDE(4,1);
  DsecondPKDE(2,0,2,2) = DSDE(4,2);
  DsecondPKDE(2,0,0,1) = 0.5*DSDE(4,3);
  DsecondPKDE(2,0,1,0) = 0.5*DSDE(4,3);
  DsecondPKDE(2,0,0,2) = 0.5*DSDE(4,4);
  DsecondPKDE(2,0,2,0) = 0.5*DSDE(4,4);
  DsecondPKDE(2,0,1,2) = 0.5*DSDE(4,5);
  DsecondPKDE(2,0,2,1) = 0.5*DSDE(4,5);

  DsecondPKDE(1,2,0,0) = DSDE(5,0);
  DsecondPKDE(1,2,1,1) = DSDE(5,1);
  DsecondPKDE(1,2,2,2) = DSDE(5,2);
  DsecondPKDE(1,2,0,1) = 0.5*DSDE(5,3);
  DsecondPKDE(1,2,1,0) = 0.5*DSDE(5,3);
  DsecondPKDE(1,2,0,2) = 0.5*DSDE(5,4);
  DsecondPKDE(1,2,2,0) = 0.5*DSDE(5,4);
  DsecondPKDE(1,2,1,2) = 0.5*DSDE(5,5);
  DsecondPKDE(1,2,2,1) = 0.5*DSDE(5,5);

  DsecondPKDE(2,1,0,0) = DSDE(5,0);
  DsecondPKDE(2,1,1,1) = DSDE(5,1);
  DsecondPKDE(2,1,2,2) = DSDE(5,2);
  DsecondPKDE(2,1,0,1) = 0.5*DSDE(5,3);
  DsecondPKDE(2,1,1,0) = 0.5*DSDE(5,3);
  DsecondPKDE(2,1,0,2) = 0.5*DSDE(5,4);
  DsecondPKDE(2,1,2,0) = 0.5*DSDE(5,4);
  DsecondPKDE(2,1,1,2) = 0.5*DSDE(5,5);
  DsecondPKDE(2,1,2,1) = 0.5*DSDE(5,5);
};

void torchANNBasedDG3DMaterialLaw::Normalize_strain(const fullMatrix<double>& E1, vector<float>& E_norm) const
{
  if(_kinematicInput != torchANNBasedDG3DMaterialLaw::EGLInc){
    if(_numberOfInput == 3){
        E_norm[0] =  (E1(0,0)- _EXXmean)/_EXXstd;
        E_norm[1] =  (E1(0,3)- _EXYmean)/_EXYstd;
        E_norm[2] =  (E1(0,1)- _EYYmean)/_EYYstd;
    }
    else if(_numberOfInput >= 6){
        E_norm[0] =  (E1(0,0)- _EXXmean)/_EXXstd;
        E_norm[1] =  (E1(0,1)- _EYYmean)/_EYYstd;
        E_norm[2] =  (E1(0,2)- _EZZmean)/_EZZstd;
        E_norm[3] =  (E1(0,3)- _EXYmean)/_EXYstd;
        E_norm[4] =  (E1(0,4)- _EZXmean)/_EZXstd;
        E_norm[5] =  (E1(0,5)- _EYZmean)/_EYZstd;
    }
  }
  else{
    double norm_E = E1.norm();
    if(norm_E<1.0e-15){norm_E = 1.0;}
    if(_numberOfInput == 3){
        E_norm[0] =  E1(0,0)/norm_E;
        E_norm[1] =  E1(0,3)/norm_E;
        E_norm[2] =  E1(0,1)/norm_E;
    }
    else if(_numberOfInput == 6){
        E_norm[0] =  E1(0,0)/norm_E;
        E_norm[1] =  E1(0,1)/norm_E;
        E_norm[2] =  E1(0,2)/norm_E;
        E_norm[3] =  E1(0,3)/norm_E;
        E_norm[4] =  E1(0,4)/norm_E;
        E_norm[5] =  E1(0,5)/norm_E;
    }
  }
};


// Normalize extra input params @Mohib TODO: Refractor to Normalizeextra()
void torchANNBasedDG3DMaterialLaw::Normalize_geo(const fullMatrix<double>& Extra1, vector<float>& Geo_norm) const
{
    if(_NeedExtraNorm and _DoubleInput){
      int j = 0;
      // For SCU we only need to normalize radius dt will be done seperatly. @Mohib
      for(int i=0; i<_numberOfExtraInput - 1; i++){
        if(_normParams_ExtraInput[j + 1] != 0.0){
            Geo_norm[i] = (Extra1(0, i) - _normParams_ExtraInput[j]) / _normParams_ExtraInput[j + 1];
        }
        else{
            Geo_norm[i] = (Extra1(0, i) - _normParams_ExtraInput[j]);
        }
        j = j + 2;
      }  
    }
    else{
      int j = 0;
      // For GRU all extras can be normalized alike. @Mohib
      for(int i=0; i<_numberOfExtraInput; i++){
          if(_normParams_ExtraInput[j + 1] != 0.0){
              Geo_norm[i] = (Extra1(0, i) - _normParams_ExtraInput[j]) / _normParams_ExtraInput[j + 1];
          }
          else{
              Geo_norm[i] = (Extra1(0, i) - _normParams_ExtraInput[j]);
          }
          j = j + 2;
      }
    }
};


// Normalize dt. Necessary for feature enabling SCU @Mohib
void torchANNBasedDG3DMaterialLaw::Normalize_dt(const fullMatrix<double>& Extra1, vector<float>& Geo_norm) const
{
    if(_NeedExtraNorm and _DoubleInput){
      Geo_norm[_numberOfExtraInput - 1] =  Extra1(0, _numberOfExtraInput - 1) / _normParams_ExtraInput[_numberOfExtraInput * 2 - 1];       
    }
};




#if defined(HAVE_TORCH)
void torchANNBasedDG3DMaterialLaw::InverseNormalize_stress(const torch::Tensor& S_norm, fullMatrix<double>& S) const
{
    auto Snorm_a = S_norm.accessor<float,3>();
    if(_numberOfInput == 3){
        S(0,0) =  Snorm_a[0][0][0]*_SXXstd + _SXXmean;
        S(0,1) =  Snorm_a[0][0][2]*_SYYstd + _SYYmean;
        S(0,2) =  Snorm_a[0][0][3]*_SZZstd + _SZZmean;
        S(0,3) =  Snorm_a[0][0][1]*_SXYstd + _SXYmean;
        S(0,4) =  0.0*_SZXstd + _SZXmean;
        S(0,5) =  0.0*_SYZstd + _SYZmean;
    }
    else if(_numberOfInput >= 6){
        S(0,0) =  Snorm_a[0][0][0]*_SXXstd + _SXXmean;
        S(0,1) =  Snorm_a[0][0][1]*_SYYstd + _SYYmean;
        S(0,2) =  Snorm_a[0][0][2]*_SZZstd + _SZZmean;
        S(0,3) =  Snorm_a[0][0][3]*_SXYstd + _SXYmean;
        S(0,4) =  Snorm_a[0][0][4]*_SZXstd + _SZXmean;
        S(0,5) =  Snorm_a[0][0][5]*_SYZstd + _SYZmean;
    }

};
#endif
double torchANNBasedDG3DMaterialLaw::soundSpeed() const
{
  Msg::Error("torchANNBasedDG3DMaterialLaw::soundSpeed is not defined !!!");
  return 0.;
};



StochDMNDG3DMaterialLaw::StochDMNDG3DMaterialLaw(const int num, const double rho, const double E,const double nu, const char *ParaFile, const bool ReadTree, const bool porous,const double tol):
  dG3DMaterialLaw(num,rho,false), _ReadTree(ReadTree), _porous(porous),_tol(tol){
  if(_ReadTree){
    read_TreeData(ParaFile);
  }
  else{
    read_parameter(ParaFile);
  }  
  fill_Matrices();         
}

StochDMNDG3DMaterialLaw::StochDMNDG3DMaterialLaw(const StochDMNDG3DMaterialLaw &src): dG3DMaterialLaw(src), _ReadTree(src._ReadTree), _porous(src._porous), 
    _LevelNode(src._LevelNode), _NodeLevel(src._NodeLevel), _Mat_Id(src._Mat_Id), _Parent(src._Parent), _Child(src._Child), _TotalLevel(src._TotalLevel), _NLeaf(src._NLeaf),
       _NInterface(src._NInterface), _N_Node(src._N_Node), _NPara_Wt(src._NPara_Wt), _NPara_Norm(src._NPara_Norm), _Dim(src._Dim), _Vf(src._Vf), _tol(src._tol),
       _Nv(src._Nv), _NTV(src._NTV), _V(src._V), _Para_Wt(src._Para_Wt), _Para_Norm(src._Para_Norm), _VA(src._VA), _VI(src._VI), _mapLaw(src._mapLaw){ }

StochDMNDG3DMaterialLaw::~StochDMNDG3DMaterialLaw(){};


void StochDMNDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const{
      // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
    IPVariable* ipvi = new  StochDMNDG3DIPVariable(_NLeaf, _NInterface, hasBodyForce,inter);
    IPVariable* ipv1 = new  StochDMNDG3DIPVariable(_NLeaf, _NInterface, hasBodyForce,inter);
    IPVariable* ipv2 = new  StochDMNDG3DIPVariable(_NLeaf, _NInterface, hasBodyForce,inter);

    StochDMNDG3DIPVariable* ipvi_Leaf = static_cast<StochDMNDG3DIPVariable*>(ipvi);
    StochDMNDG3DIPVariable* ipv1_Leaf = static_cast<StochDMNDG3DIPVariable*>(ipv1);
    StochDMNDG3DIPVariable* ipv2_Leaf = static_cast<StochDMNDG3DIPVariable*>(ipv2);
    int mat_IP;
    for (int i=0; i< _NLeaf; i++){
      mat_IP = _Mat_Id[_NInterface + i];
      IPStateBase* ips_i = NULL;      
      _mapLaw[mat_IP]->createIPState(ips_i, hasBodyForce, state_, ele, nbFF_, GP, gpt);
      std::vector<IPVariable*> ip_all;
      ips_i->getAllIPVariable(ip_all);
      ipvi_Leaf->addIPv(i, ip_all[0]);
      ipv1_Leaf->addIPv(i, ip_all[1]);
      ipv2_Leaf->addIPv(i, ip_all[2]);
    }
    if(ips != NULL) delete ips;
      ips = new IP3State(state_,ipvi,ipv1,ipv2);
}


void StochDMNDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const {
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  ipv = new  StochDMNDG3DIPVariable(_NLeaf, _NInterface, hasBodyForce,inter);
  StochDMNDG3DIPVariable* ipv_Leaf = static_cast<StochDMNDG3DIPVariable*>(ipv);
  int mat_IP;
  for (int i=0; i< _NLeaf; i++){
    IPVariable* ipv_i = NULL;
    mat_IP = _Mat_Id[_NInterface + i];
    _mapLaw[mat_IP]->createIPVariable(ipv_i, hasBodyForce, ele, nbFF_, GP, gpt);
    ipv_Leaf->addIPv(i, ipv_i);
  }
};

void StochDMNDG3DMaterialLaw::initialIPVariable(IPVariable* ipv, bool stiff){
  StochDMNDG3DIPVariable* ipv_all = dynamic_cast<StochDMNDG3DIPVariable*>(ipv);
  ipv_all->InitializeFluctuationVector();
  if (ipv_all == NULL){
    Msg::Error("StochDMNDG3DIPVariable must be used in StochDMNDG3DMaterialLaw::initialIPVariable");
  }
  int mat_IP;
  for (int i=0; i< _NLeaf; i++){
    IPVariable* ipv_i = ipv_all->getIPv(i);
    mat_IP = _Mat_Id[_NInterface + i];
    _mapLaw[mat_IP]->initialIPVariable(ipv_i, stiff);
  }
}

void StochDMNDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{
  StochDMNDG3DIPVariable* ipv_all = dynamic_cast<StochDMNDG3DIPVariable*>(ipv);
  const StochDMNDG3DIPVariable* ipvprev_all = dynamic_cast<const StochDMNDG3DIPVariable*>(ipvprev);
  if (ipv_all == NULL){
    Msg::Error("StochDMNDG3DIPVariable must be used in StochDMNDG3DMaterialLaw::checkInternalState");
  }
  int mat_IP;
  for (int i=0; i< _NLeaf; i++){
    IPVariable* ipv_i = ipv_all->getIPv(i);
    const IPVariable* ipvprev_i = ipvprev_all->getIPv(i);
    mat_IP = _Mat_Id[_NInterface + i];
    _mapLaw[mat_IP]->checkInternalState(ipv_i, ipvprev_i);
  }
}


void StochDMNDG3DMaterialLaw::TwoPhasesInteract(const std::vector<STensor43>& C, fullMatrix<double> &mat, const int pos){
  double n0 = _Para_Norm[pos][0];
  double n1 = _Para_Norm[pos][1];
  double n2 = _Para_Norm[pos][2];
  double tmp1[6][6], tmp2[6][6], Norm[6][3], NormT[3][6];
  double S_ba = 1.0;
  int pos0;
  int pos1;
  int level = _NodeLevel[pos];
  if(level ==_TotalLevel-1 and _porous){
    S_ba = _Para_Wt[pos][1];
  }

  for(int i=0; i<3; i++){
    for(int j=0; j<3; j++){
      Norm[i][j] = 0.0;
      Norm[3+i][j] = 0.0;
      NormT[i][j] = 0.0;
      NormT[i][3+j] = 0.0;
    }
  }
  Norm[0][0] = n0;        // different N and NT are used in order to be consistent with function "fromSTensor43ToFullMatrix()"
  Norm[1][1] = n1;
  Norm[2][2] = n2;
  Norm[3][0] = n1/2.0;
  Norm[3][1] = n0/2.0;
  Norm[4][0] = n2/2.0;
  Norm[4][2] = n0/2.0;
  Norm[5][1] = n2/2.0;
  Norm[5][2] = n1/2.0;

  NormT[0][0] = n0;
  NormT[1][1] = n1;
  NormT[2][2] = n2;
  NormT[0][3] = n1;
  NormT[0][4] = n2;
  NormT[1][3] = n0;
  NormT[1][5] = n2;
  NormT[2][4] = n0;
  NormT[2][5] = n1;

  fullMatrix<double> mat0(6,6), mat1(6,6), K(3,3);
  pos0 = _Child[pos][0];
  pos1 = _Child[pos][1];
  double v_A = _VA[pos0];
  double v_B = _VA[pos1];
  int L_ch0 = _NodeLevel[pos0];
  int L_ch1 = _NodeLevel[pos1];

  if(L_ch0 == _TotalLevel){
    STensorOperation::fromSTensor43ToFullMatrix(C[_Mat_Id[pos0]], mat0);
  }  
  else{
    TwoPhasesInteract(C, mat0, pos0);
  }
  if(L_ch1 == _TotalLevel){
    STensorOperation::fromSTensor43ToFullMatrix(C[_Mat_Id[pos1]], mat1);
  }  
  else{    
    TwoPhasesInteract(C, mat1, pos1);
  }

  for(int i=0; i<6;i++){
    for(int j=0; j<6;j++){
      tmp1[i][j] = mat0(i,j)/v_A + S_ba*S_ba*mat1(i,j)/v_B;
      tmp2[i][j] = S_ba*mat1(i,j) - mat0(i,j);
    }
  }

  for(int m=0; m<3;m++){
    for(int n=0; n<3;n++) {
      for(int i=0; i<6;i++) {
        for(int j=0; j<6;j++) {
          K(m,n) += NormT[m][i]*tmp1[i][j]*Norm[j][n];
        }
      }
    }
  }
  K.invertInPlace();
  for(int m=0; m<6;m++){
    for(int n=0; n<6;n++) {
      tmp1[m][n] = 0.0;
      for(int i=0; i<3;i++) {
        for(int j=0; j<3;j++) {
          tmp1[m][n] += Norm[m][i]*K(i,j)*NormT[j][n];
        }
      }
    }
  }

  for(int m=0; m<6;m++){
    for(int n=0; n<6; n++) {
      mat(m,n) = mat0(m,n)*v_A + mat1(m,n)*v_B;
      for(int i=0; i<6;i++) {
        for(int j=0; j<6;j++) {
          mat(m,n) -= tmp2[m][i]*tmp1[i][j]*tmp2[j][n];
        }
      }
    }
  }
}
  // end of TwoPhasesInteract

void StochDMNDG3DMaterialLaw::initLaws(const std::map<int,materialLaw*> &maplaw){
  if (!_initialized){
    STensorOperation::zero(elasticStiffness);
    fullMatrix<double> mat(6,6);
    std::vector<STensor43> C;
    for (std::vector<dG3DMaterialLaw*>::iterator ite = _mapLaw.begin(); ite!= _mapLaw.end(); ite++){
      (*ite)->initLaws(maplaw);
      elasticStiffness = (*ite)->elasticStiffness;
      C.push_back(elasticStiffness);
    }
    if(_porous) C.push_back(elasticStiffness);
    TwoPhasesInteract(C,  mat, 0);

    STensorOperation::fromFullMatrixToSTensor43(mat, elasticStiffness);
    //elasticStiffnedd.print("elasticStiffness StochDMNDG3DMaterialLaw");
    _initialized = true;
    //mat.print("initialize material tensor");
  }
}

void StochDMNDG3DMaterialLaw::setMacroSolver(const nonLinearMechSolver* sv){
   dG3DMaterialLaw::setMacroSolver(sv);
    for (std::vector<dG3DMaterialLaw*>::iterator ite = _mapLaw.begin(); ite!= _mapLaw.end(); ite++){
      (*ite)->setMacroSolver(sv);
    }
}
  
void StochDMNDG3DMaterialLaw::getLeafOfInterface(const int pos, int &node_start, int &node_end) const{
  node_start = pos;
  node_end = pos;
  if(_NodeLevel[pos] < _TotalLevel){
    while(_NodeLevel[node_start]<_TotalLevel){
      node_start = _Child[node_start][0];
    }
    while(_NodeLevel[node_end]<_TotalLevel){
      node_end = _Child[node_end][1];
    }
  }  
}  
         
void StochDMNDG3DMaterialLaw::fill_NLocal(const int pos, double N[][3])const{
  double n0 = _Para_Norm[pos][0];
  double n1 = _Para_Norm[pos][1];
  double n2 = _Para_Norm[pos][2];

  for(int i=0; i<9; i++){
    for(int j=0; j<3; j++){
      N[i][j]=0.0;
    }
  }
  N[0][0]= n0;
  N[1][0]= n1;
  N[2][0]= n2;
  N[3][1]= n0;
  N[4][1]= n1;
  N[5][1]= n2;
  N[6][2]= n0;
  N[7][2]= n1;
  N[8][2]= n2;
}


void StochDMNDG3DMaterialLaw::fill_W_WI(const int pos, double W[][2]){
  double VI = _VI[pos];
  double Wtmp0 = _Para_Wt[pos][0];
  double Wtmp1 = _Para_Wt[pos][1];
  double WA = Wtmp0*(1.0-VI)+ Wtmp1*VI;

  for(int i=0; i<2; i++){
    for(int j=0; j<2; j++){
      W[i][j] = 0.0;
    }
  }
  W[0][0]= WA;
  W[0][1]= 1.0-WA;
  W[1][0]= (Wtmp1*VI)/WA;
  W[1][1]= ((1.0-Wtmp1)*VI)/(1.0-WA);
}


// fill the matrices which keep the topological information
void  StochDMNDG3DMaterialLaw::fill_Matrices(){    
  double Norm[9][3];
  double W_WI[2][2];
  int pos0, pos1;
  int L_ch0, L_ch1;
  int node_start0, node_end0, node_start1, node_end1;
  int Col_start, Col_start0,Col_start1, Row_start,Row_start0,  Row_start1;
  double tmp;
  int col = 9;

  _VA.resize(_N_Node,0.0);
  _VA[0] = 1.0;
  _VI.resize(_NInterface, 0.0);
  _VI[0] = _Vf;
  int l;
  for(int pos=0; pos <_NInterface; pos++){
    l = _NodeLevel[pos];
    fill_NLocal(pos,Norm);
    pos0 = _Child[pos][0];
    pos1 = _Child[pos][1];
    L_ch0 = _NodeLevel[pos0];
    L_ch1 = _NodeLevel[pos1];
    Row_start = pos*3;
    Col_start0 = (pos0-1)*col;
    Col_start1 = (pos1-1)*col;

    double S_ba = 1.0;
    if(l ==_TotalLevel-1 and _porous) S_ba = _Para_Wt[pos][1];
    // information of interface norm
    for(int nc=0; nc<col;nc++){
      for(int nr=0; nr<3;nr++){
        _NT.set(Row_start+nr, Col_start0+nc, Norm[nc][nr]);
        _NT.set(Row_start+nr, Col_start1+nc,-S_ba*Norm[nc][nr]);
      }
    }

    if(l ==_TotalLevel-1){
      if(_porous){
        _VA[pos0] = (1.0-_VI[pos])*_Para_Wt[pos][0];
        _VA[pos1] = (1.0-_VI[pos])*(1.0-_Para_Wt[pos][0]);
      }
      else{
        _VA[pos0] = 1.0-_VI[pos];
        _VA[pos1] = _VI[pos];
      }
    }
    else{
      fill_W_WI(pos,W_WI);
      _VA[pos0] = W_WI[0][0];
      _VA[pos1] = W_WI[0][1];
      if(L_ch0 < _TotalLevel) _VI[pos0]=W_WI[1][0];
      if(L_ch1 < _TotalLevel) _VI[pos1]=W_WI[1][1];
   //   Msg::Info("Node= %d,%d,%d, VI = %f,%f,%f",pos,pos0,pos1,_VI[pos],W_WI[1][0],W_WI[1][1]);
    }
    //information of homogenous strain to local strain
    Col_start = pos*3;      
    getLeafOfInterface(pos0, node_start0, node_end0);
    getLeafOfInterface(pos1, node_start1, node_end1);
    for(int r=node_start0; r<=node_end0; r++){
      Row_start0 = (r-_NInterface)*col;
      for(int nr=0; nr<col;nr++){
        for(int nc=0; nc<3;nc++){
          _Nv.set(Row_start0+nr, Col_start+nc,Norm[nr][nc]/_VA[pos0]);
        }
      }
    }  
    for(int r=node_start1; r<=node_end1; r++){
      Row_start1 = (r-_NInterface)*col;
      for(int nr=0; nr<col;nr++){
        for(int nc=0; nc<3;nc++){
          _Nv.set(Row_start1+nr, Col_start+nc,-S_ba*Norm[nr][nc]/_VA[pos1]);
        }
      }
    }         
  }

 // information of voulme fraction to stress on node
  Row_start = col*(_N_Node-1-_NLeaf);
  for(int nc=0; nc<col*_NLeaf; nc++){
    _V.set(Row_start+nc,nc,1.0);
  }
  
  for(int pos = _NInterface-1; pos > 0; pos--){
    pos0 = _Child[pos][0];
    pos1 = _Child[pos][1];
    Row_start = (pos-1)*col;
    Row_start0 = (pos0-1)*col;
    Row_start1 = (pos1-1)*col;
    for(int nr=0; nr<col;nr++){
      for(int nc=0; nc<col*_NLeaf ; nc++){
        tmp = _VA[pos0]*_V(Row_start0+nr,nc) + _VA[pos1]*_V(Row_start1+nr,nc);
        _V.set(Row_start+nr,nc, tmp);
      }
    }
  }
  _NT.mult(_V, _NTV);

  for(int i=0; i<col*_NLeaf;i++){
    _I(i, i%col) = 1.0;
  }
}
 // end fill_matrices

// read parameter from input file
void StochDMNDG3DMaterialLaw::read_parameter(const char *ParaFile){

  double sin0, cos0, sin1,cos1;
  FILE *Para = fopen(ParaFile, "r");
  if ( Para != NULL ){
    int okf = fscanf(Para, "%d %d\n", &_Dim, &_TotalLevel);
    bool resizeFlag;
    _NLeaf = int(pow(2,_TotalLevel));
    _NInterface = _NLeaf-1;
    _N_Node = _NLeaf + _NInterface;

    int row = 9*_NLeaf;
    int col = 3*_NInterface;
    resizeFlag = _Nv.resize(row, col, true);

    resizeFlag = _I.resize(row, 9, true);

    row = 9*(_N_Node-1);
    col = 9*_NLeaf;
    resizeFlag = _V.resize(row, col, true);
        
    row = 3*_NInterface;        
    resizeFlag = _NTV.resize(row, col, true);  
    
    resizeFlag = _NT.resize(row, 9*(_N_Node-1), true);            
        
    okf = fscanf(Para, "%lf\n", &_Vf);

    _NPara_Norm = _NInterface;
    
    resizeFlag = _C.resize(col,col,true);
    resizeFlag = Jacobian.resize(row, row,true); 
    
    resizeFlag = _ParaAngle.resize(_NInterface,2,true);
        
    double ParaN[2];
    double Pi(3.14159265359);        
    SPoint3 Para_Norm;
    SPoint2 Para_Wt;
    
    for(int i=0;i<_NInterface;i++){
      if(_Dim==2){
        okf = fscanf(Para, "%lf\n", &ParaN[0]);
        ParaN[1] = 0.0;        
      }  
      else if(_Dim==3){
        okf = fscanf(Para, "%lf %lf\n", &ParaN[0],  &ParaN[1]);
      }
      sin0 = sin(2.0*Pi*ParaN[0]);
      cos0 = cos(2.0*Pi*ParaN[0]);
      sin1 = sin(Pi*ParaN[1]);
      cos1 = cos(Pi*ParaN[1]);
      Para_Norm[0] = cos1*sin0;
      Para_Norm[1] = cos1*cos0;
      Para_Norm[2] = sin1;
      _Para_Norm.push_back(Para_Norm); 
      _ParaAngle(i,0) =  ParaN[0];
      _ParaAngle(i,1) =  ParaN[1];
    }
                 
    if(_porous){
      _NPara_Wt = _NInterface;
    }
    else{
      _NPara_Wt = int(pow(2,(_TotalLevel-1)))-1;
    }
    for(int i=0;i<_NPara_Wt;i++){
      okf = fscanf(Para, "%lf %lf\n", &Para_Wt[0],  &Para_Wt[1]);
      _Para_Wt.push_back(Para_Wt);
    }
  }
   // creat data as for tree data
  int n = 0;
  int m = -10;
  for(int l=0; l<=_TotalLevel; l++){
    std::vector<int> ln;
    _LevelNode.push_back(ln);
    while(n < pow(2,l+1)-1){
      _LevelNode[l].push_back(n);
      _NodeLevel.push_back(l);
      if(n==0){
        _Parent.push_back(0);
      }
      else{
        _Parent.push_back(int((n-1)/2));
      }
      if(l==_TotalLevel){
        _Mat_Id.push_back((n+1)%2);
      }
      else{
        std::vector<int> ch;
        ch.push_back(2*n+1);
        ch.push_back(2*n+2);
        _Child.push_back(ch);
        _Mat_Id.push_back(m);
      }      
      n +=1;
    }  
  }
   
  Msg::Info("Finish reading parameter filefor Complete Binary Tree,  level = %d, Dim = %d, NLeaf = %d", _TotalLevel, _Dim, _NLeaf);
}
   // end of read_parameter 
 
void StochDMNDG3DMaterialLaw::read_TreeData(const char *TreeFile){
  std::ifstream file(TreeFile);
  if(!file.is_open()) {
        Msg::Error( "Error: Could not open the tree file." );
        return ;
  }
  int level_value, node_value, parent_value;
  double sin0, cos0, sin1,cos1;
  double ParaN[2];
  int pn = 0;
  double Pi(3.14159265359);        
  SPoint3 Para_Norm;
  SPoint2 Para_Wt;
  bool resizeFlag;
  
  std::string line;
  int DataId = 0;
  while(std::getline(file, line)) {
    if(line.empty()) continue;
    std::istringstream stream(line);
    std::string value;
    std::vector<std::string> row;
    while (std::getline(stream, value, ',')) {
      row.push_back(value);  
    }
    if (std::isalpha(row[0][0])){
      DataId +=1;
      if(DataId == 4){      
        _NLeaf = static_cast<int>(_LevelNode[_TotalLevel].size());
        _N_Node = static_cast<int>(_Parent.size());
        _NInterface = _N_Node - _NLeaf;
        resizeFlag = _ParaAngle.resize(_NInterface,2,true); 
      } 
    }
    else{      
      if(DataId == 1){                            // read the tree structure------------------
        _TotalLevel = std::stoi(row[0]); 
        for(int i=0; i<=_TotalLevel; i++){
          std::vector<int> LN;
          _LevelNode.push_back(LN);
        }
      }  
      else if(DataId == 2){ 
        level_value = std::stoi(row[0]);
        node_value = std::stoi(row[1]);
        parent_value = std::stoi(row[2]);
        _Mat_Id.push_back(std::stoi(row[3]));
                
        _LevelNode[level_value].push_back(node_value);
        _NodeLevel.push_back(level_value);
        _Parent.push_back(parent_value);
                
        if(level_value < _TotalLevel){
          std::vector<int> ch;
          _Child.push_back(ch);
        }  
        if(parent_value != node_value) _Child[parent_value].push_back(node_value);
      }  
      else if(DataId == 3){               // read the DMN Parameter  -----------------------
        _Dim = std::stoi(row[0]);
        _Vf = std::stod(row[1]);        
      }    
      else if(DataId == 4){
        if(_Dim==2){
          ParaN[0] = std::stod(row[0]);           
          ParaN[1] = 0.0;
        }
        else if(_Dim==3){
          ParaN[0] = std::stod(row[0]);           
          ParaN[1] = std::stod(row[1]);         
        }          
        sin0 = sin(2.0*Pi*ParaN[0]);
        cos0 = cos(2.0*Pi*ParaN[0]);
        sin1 = sin(Pi*ParaN[1]);
        cos1 = cos(Pi*ParaN[1]);
        Para_Norm[0] = cos1*sin0;
        Para_Norm[1] = cos1*cos0;
        Para_Norm[2] = sin1;
        _Para_Norm.push_back(Para_Norm); 
        _ParaAngle(pn,0) =  ParaN[0];
        _ParaAngle(pn,1) =  ParaN[1]; 
        pn +=1;
      }  
      else if(DataId == 5){
        Para_Wt[0] = std::stod(row[0]);
        Para_Wt[1] = std::stod(row[1]);
        _Para_Wt.push_back(Para_Wt);
      }
    }
  }      
    
  int row = 9*_NLeaf;
  int col = 3*_NInterface;
  resizeFlag = _Nv.resize(row, col, true);
  resizeFlag = _I.resize(row, 9, true);

  row = 9*(_N_Node-1);
  col = 9*_NLeaf;
  resizeFlag = _V.resize(row, col, true);
        
  row = 3*_NInterface;  
  resizeFlag = _NT.resize(row, 9*(_N_Node-1), true);         
  resizeFlag = _NTV.resize(row, col, true);     
           
  resizeFlag = _C.resize(col, col,true);
  resizeFlag = Jacobian.resize(row, row,true); 
  _NPara_Norm = _NInterface; 
  _NPara_Wt = static_cast<int>(_Para_Wt.size()); 
  
  Msg::Info("Finish reading Tree Data and parameter file, level = %d, Dim = %d, NLeaf = %d", _TotalLevel, _Dim, _NLeaf);
}      
      
         
   
   
void StochDMNDG3DMaterialLaw::reset_Parameter(std::vector<std::vector<double>>& Para_Norm, std::vector<std::vector<double>>& Para_Wt, const double Vf){
    
  double sin0, cos0, sin1,cos1; 
  double Pi(3.14159265359);
  for(int i=0;i<_NInterface;i++){ 
    if(_Dim==2){
      sin1 = 0.0;
      cos1 = 1.0;
      _ParaAngle(i,0) =  Para_Norm[i][0];
    }
    else if(_Dim==3){
      sin1 = sin(Pi*Para_Norm[i][1]);
      cos1 = cos(Pi*Para_Norm[i][1]); 
      _ParaAngle(i,0) =  Para_Norm[i][0];
      _ParaAngle(i,1) =  Para_Norm[i][1]; 
    }  
    sin0 = sin(2.0*Pi*Para_Norm[i][0]);
    cos0 = cos(2.0*Pi*Para_Norm[i][0]);
    _Para_Norm[i][0] = cos1*sin0;
    _Para_Norm[i][1] = cos1*cos0;
    _Para_Norm[i][2] = sin1; 
  }      
        
  for(int i=0;i<_NPara_Wt;i++){          
    _Para_Wt[i][0] = Para_Wt[i][0];
    _Para_Wt[i][1] = Para_Wt[i][1];
    if(_NodeLevel[_Child[i][1]]==_TotalLevel) _Para_Wt[i][1] = 1.0; 
  }      
  _VA.clear();
  _VI.clear();
  _Nv.setAll(0.0);
  _NTV.setAll(0.0);
  _NT.setAll(0.0);
  _V.setAll(0.0);
  _I.setAll(0.0);
  _C.setAll(0.0);
  Jacobian.setAll(0.0);
  if(Vf!= 0.0){
    _Vf = Vf;
  }
  fill_Matrices();     
  //Msg::Info("Finish update parameters of DMN ");       
}        

void StochDMNDG3DMaterialLaw::reset_Parameter(const char* Para){
  _VA.clear();
  _VI.clear();
  _Para_Norm.clear();
  _Para_Wt.clear();
  _LevelNode.clear(); 
  _NodeLevel.clear();    
  _Mat_Id.clear();
  _Parent.clear();      
  _Child.clear();
  if(_ReadTree){
    read_TreeData(Para);
  }
  else{
    read_parameter(Para);
  } 
  fill_Matrices();
}
       
void StochDMNDG3DMaterialLaw::addLaw(dG3DMaterialLaw* law){
  _mapLaw.push_back(law);
};


void StochDMNDG3DMaterialLaw::stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff, const bool checkfrac, const bool dTangent){
   /* get ipvariable */
  StochDMNDG3DIPVariable* ipvcur = static_cast<StochDMNDG3DIPVariable*>(ipv);
  const StochDMNDG3DIPVariable* ipv_prev = static_cast<const StochDMNDG3DIPVariable*>(ipvprev);
  const STensor3& F = ipvcur->getConstRefToDeformationGradient();

  STensor3& P = ipvcur->getRefToFirstPiolaKirchhoffStress();
  STensor43& L = ipvcur->getRefToTangentModuli();
  STensorOperation::zero(P);
  STensorOperation::zero(L);

  static double C_hom[9][9];
  static double P_hom[9];
 // fullMatrix<double> mat(6,6);

  fullVector<double>& Fvec = ipvcur->getRefToDeformationGradientVect();
  fullVector<double>& Pvec = ipvcur->getRefToFirstPiolaKirchhoffStressVect();
  fullVector<double>& a_cur = ipvcur->getRefToInterfaceFluctuationVect();
  Pvec.setAll(0.0);
 //  fullVector<double> P_node(9*(_N_Node-1));
  static fullVector<double> Res_node(3*_NInterface);
  static fullVector<double> a_step(3*_NInterface);
  static fullMatrix<double> NTVC(3*_NInterface, 9*_NLeaf);
  static fullMatrix<double> Jacobian_inv(3*_NInterface, 3*_NInterface); 
  static fullMatrix<double> tmp(9*_NLeaf, 9); 
  static fullMatrix<double> dRdF(3*_NInterface, 9); 
  static fullMatrix<double> dadF(3*_NInterface, 9); 
    
  Res_node.setAll(0.0);
  a_step.setAll(0.0);
  _C.setAll(0.0);
  NTVC.setAll(0.0); 
  Jacobian.setAll(0.0);
  dRdF.setAll(0.0);
  dadF.setAll(0.0);
  tmp.setAll(0.0);
  int ite = 0;
  double r = 1.0; 
  int pos_start = _NInterface;
  int max_iter= 15;
  bool stiff_loc = true;


  while(r>_tol and ite < max_iter){
    ite +=1;
    _Nv.mult(a_cur, Fvec);
    for(int i=0; i<_NLeaf; i++){
      if (_VA[pos_start+i] > 1e-6){
        dG3DIPVariableBase* ipv_i = dynamic_cast<dG3DIPVariableBase*>(ipvcur->getIPv(i));
        const dG3DIPVariableBase* ipvprev_i = dynamic_cast<const dG3DIPVariableBase*>(ipv_prev->getIPv(i));

        STensor3& Floc = ipv_i->getRefToDeformationGradient();
        for(int m=0; m<3; m++){
          for(int n=0; n<3; n++){
            Floc(m,n) = F(m,n) + Fvec(i*9 + m*3+n);
          }
        }
        int mat_IP = _Mat_Id[_NInterface + i];
        _mapLaw[mat_IP]->stress(ipv_i, ipvprev_i, stiff_loc, checkfrac, dTangent);
        const STensor3& P_i = ipv_i->getConstRefToFirstPiolaKirchhoffStress();

        for(int m=0; m<3; m++){
            for(int n=0; n<3; n++){
              Pvec(i*9 + m*3+n) = P_i(m,n);
            }
        }

        if(stiff_loc){
          const STensor43& L_i = ipv_i->getConstRefToTangentModuli();
          for(int m=0; m<3; m++){
            for(int n=0; n<3; n++){
              for(int k=0; k<3; k++){
                for(int l=0; l<3; l++){  
                  _C(i*9+m*3+n, i*9+k*3+l) = L_i(m,n,k,l);
                }
              }
            }
          }                  
        }
      }
    }

    _NTV.mult(Pvec, Res_node);
    r = Res_node.norm()/_NInterface;
    
    _NTV.mult(_C, NTVC);
    NTVC.mult(_Nv, Jacobian);
    Jacobian.invertInPlace();
    
    Jacobian.mult(Res_node, a_step);
    a_step.scale(-1.0);
    a_cur.axpy(a_step, 1.0);
  }
  Jacobian.scale(-1.0);
  int pos0 = _Child[0][0];
  int pos1 = _Child[0][1];  
  if(r>_tol){
    Msg::Error("DMN residual (= %lf) did n't converge to tolenerce after %d iteration",r,ite);
    return;
  }
  else{  
    for(int m=0;m<9;m++){
      P_hom[m] = 0.0;
      for(int i=0;i<_NLeaf;i++){
        P_hom[m] += (_VA[pos0]*_V((pos0-1)*9+m, 9*i+m) + _VA[pos1]*_V((pos1-1)*9+m, 9*i+m)) *Pvec(9*i+m);
      }
    }
    for(int i=0;i<3;i++){
      for(int j=0;j<3;j++){
        P(i,j) = P_hom[3*i+j];
      }
    }

    if(stiff){
      NTVC.mult(_I, dRdF);
      Jacobian.mult(dRdF, dadF);
      _Nv.mult(dadF, tmp);
      tmp.add(_I);

      for(int m=0;m<9;m++){
        for(int n=0;n<9;n++){
          C_hom[m][n] = 0.0;
          for(int i=0;i<_NLeaf;i++){
            for(int j=0;j< 9*_NLeaf;j++){
              C_hom[m][n] += (_VA[pos0]*_V((pos0-1)*9+m, 9*i+m) + _VA[pos1]*_V((pos1-1)*9+m, 9*i+m))*_C(m+9*i,j)*tmp(j,n);
            }
          }
        }
      }

      for(int i=0;i<3;i++){
        for(int j=0;j<3;j++){
          for(int k=0;k<3;k++){
            for(int l=0;l<3;l++){
              L(i,j,k,l) = C_hom[i*3+j][k*3+l];
            }
          }
        }
      }
    }
  }
  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);
} 
// end of stress


// Compute derivatives repect to DMN parameters at trainning satge
void StochDMNDG3DMaterialLaw::getdVAdP(std::vector< std::vector <SPoint2> >& dVAdPv) const{
  int pos0, pos1;
  dVAdPv.resize(_N_Node, std::vector<SPoint2>(_NPara_Wt, SPoint2()));
  std::vector< std::vector <SPoint2> > dVIdPv(_NInterface,std::vector<SPoint2>(_NPara_Wt, SPoint2()));
  for(int pos=0; pos <_NInterface; pos++){  
    int l = _NodeLevel[pos];
    pos0 = _Child[pos][0];
    pos1 = _Child[pos][1];
    if(l==0){
      dVAdPv[pos0][pos][0] = 1.0-_Vf;
      dVAdPv[pos0][pos][1] = _Vf;
     
      dVAdPv[pos1][pos][0] = -dVAdPv[pos0][pos][0];
      dVAdPv[pos1][pos][1] = -dVAdPv[pos0][pos][1];
      
      if(_NodeLevel[pos0]>_TotalLevel){
        dVIdPv[pos0][pos][0] = -_VI[pos0]/_VA[pos0]* dVAdPv[pos0][pos][0];
        dVIdPv[pos0][pos][1] = -_VI[pos0]/_VA[pos0]* dVAdPv[pos0][pos][1]+_Vf/_VA[pos0];
      }
      if(_NodeLevel[pos1]>_TotalLevel){
        dVIdPv[pos1][pos][0] = -_VI[pos1]/_VA[pos1]* dVAdPv[pos1][pos][0];
        dVIdPv[pos1][pos][1] = -_VI[pos1]/_VA[pos1]* dVAdPv[pos1][pos][1]-_Vf/_VA[pos1];
      } 
    }
    else if(l ==_TotalLevel-1){
      if(_porous){
        for (int i = 0; i < pos; ++i) {
          dVAdPv[pos0][i][0]= -1.0*dVIdPv[pos][i][0]*_Para_Wt[pos][0];
          dVAdPv[pos0][i][1]= -1.0*dVIdPv[pos][i][1]*_Para_Wt[pos][0];
        } 
        dVAdPv[pos0][pos][0] += 1.0-_VI[pos];        
        
        for (int i = 0; i < pos; ++i) {
          dVAdPv[pos1][i][0]= -1.0*dVIdPv[pos][i][0]*(1.0-_Para_Wt[pos][0]);
          dVAdPv[pos1][i][1]= -1.0*dVIdPv[pos][i][1]*(1.0-_Para_Wt[pos][0]);
        }
        dVAdPv[pos1][pos][0] += _VI[pos]-1.0;  
      }
      else{
        for (int i = 0; i < pos; ++i) {
          dVAdPv[pos0][i][0] = -1.0*dVIdPv[pos][i][0];
          dVAdPv[pos0][i][1] = -1.0*dVIdPv[pos][i][1];
          dVAdPv[pos1][i][0] = -dVAdPv[pos0][i][0];
          dVAdPv[pos1][i][1] = -dVAdPv[pos0][i][1];
        } 
      }
    }  
    else{
      dVAdPv[pos0][pos][0] = 1.0-_VI[pos];
      dVAdPv[pos0][pos][1] = _VI[pos];    
      for (int i = 0; i < pos; ++i) {
        dVAdPv[pos0][i][0] = (_Para_Wt[pos][1]-_Para_Wt[pos][0])*dVIdPv[pos][i][0];
        dVAdPv[pos0][i][1] = (_Para_Wt[pos][1]-_Para_Wt[pos][0])*dVIdPv[pos][i][1];
      }
      for (int i = 0; i <= pos; ++i) {  
        dVAdPv[pos1][i][0] = -dVAdPv[pos0][i][0];
        dVAdPv[pos1][i][1] = -dVAdPv[pos0][i][1];        
      } 
      if(_NodeLevel[pos0]>_TotalLevel){
        for (int i = 0; i <= pos; ++i) {
          dVIdPv[pos0][i][0] = -_VI[pos0]/_VA[pos0]* dVAdPv[pos0][i][0]+ _Para_Wt[pos][1]/_VA[pos0]*dVIdPv[pos][i][0];
          dVIdPv[pos0][i][1] = -_VI[pos0]/_VA[pos0]* dVAdPv[pos0][i][1]+ _Para_Wt[pos][1]/_VA[pos0]*dVIdPv[pos][i][1];
        } 
        dVIdPv[pos0][pos][1] += _VI[pos]/_VA[pos0];
      }
      if(_NodeLevel[pos1]>_TotalLevel){
        for (int i = 0; i <= pos; ++i) {
          dVIdPv[pos1][i][0] = -_VI[pos1]/_VA[pos1]* dVAdPv[pos1][i][0]+ (1.0-_Para_Wt[pos][1])/_VA[pos1]*dVIdPv[pos][i][0];
          dVIdPv[pos1][i][1] = -_VI[pos1]/_VA[pos1]* dVAdPv[pos1][i][1]+ (1.0-_Para_Wt[pos][1])/_VA[pos1]*dVIdPv[pos][i][1];
        } 
        dVIdPv[pos1][pos][1] -= _VI[pos]/_VA[pos1];
      }
    }
  }
}    
      
      


void StochDMNDG3DMaterialLaw::getdNLocaldP(const int pos, double dNda[][3], double dNdb[][3]) const {
  double Pi(3.14159265359);
  
  double sin0 = sin(2.0*Pi*_ParaAngle(pos,0));
  double cos0 = cos(2.0*Pi*_ParaAngle(pos,0));
  double sin1 = sin(Pi*_ParaAngle(pos,1));
  double cos1 = cos(Pi*_ParaAngle(pos,1));
  
  double dn0 = 2.0*Pi*cos1*cos0;
  double dn1 = -2.0*Pi*cos1*sin0;
  double dn2 = 0.0;
  
  for(int i=0; i<9; i++){
    for(int j=0; j<3; j++){
      dNda[i][j]=0.0;
    }
  } 
  dNda[0][0]= dn0;
  dNda[1][0]= dn1;
  dNda[2][0]= dn2;
  dNda[3][1]= dn0;
  dNda[4][1]= dn1;
  dNda[5][1]= dn2;
  dNda[6][2]= dn0;
  dNda[7][2]= dn1;
  dNda[8][2]= dn2;
  
  for(int i=0; i<9; i++){
    for(int j=0; j<3; j++){
      dNdb[i][j]=0.0;
    }
  } 
  //--------------------
  if(_Dim==3){  
    dn0 = -Pi*sin1*sin0;
    dn1 = -Pi*sin1*cos0;
    dn2 = Pi*cos1;  

    dNdb[0][0]= dn0;
    dNdb[1][0]= dn1;
    dNdb[2][0]= dn2;
    dNdb[3][1]= dn0;
    dNdb[4][1]= dn1;
    dNdb[5][1]= dn2;
    dNdb[6][2]= dn0;
    dNdb[7][2]= dn1;
    dNdb[8][2]= dn2;
  }
}


void StochDMNDG3DMaterialLaw::get_matrixdVdPv(std::vector< std::vector <SPoint2> >& dVAdPv, std::vector<std::vector<std::vector<std::vector<double>>>>& d_VdPv) const{
  int col = 9;
  int Row_start, Row_start0, Row_start1, Col_start, Col_end; 
  int pos0, pos1, L_ch0, L_ch1;
  int node_start0, node_end0, node_start1, node_end1;
  
// information of voulme fraction to stress on node       
  for(int pos = _NInterface-1; pos > 0; pos--){  
    pos0 = _Child[pos][0];
    pos1 = _Child[pos][1];
    getLeafOfInterface(pos0, node_start0, node_end0);
    getLeafOfInterface(pos1, node_start1, node_end1);
    Col_start = (node_start0-_NInterface)*col;
    Col_end = (node_end1-_NInterface)*col;
    Row_start = (pos-1)*col;
    Row_start0 = (pos0-1)*col;
    Row_start1 = (pos1-1)*col;
    for(int nr=0; nr<col;nr++){
      for(int nc= Col_start; nc<Col_end ; nc++){ 
        for(int i=0; i<_NPara_Wt; i++){
          for(int j=0; j<2; j++){
            d_VdPv[Row_start+nr][nc][i][j] = dVAdPv[pos0][i][j]*_V(Row_start0+nr,nc)+dVAdPv[pos1][i][j]*_V(Row_start1+nr,nc)
                                    + _VA[pos0]* d_VdPv[Row_start0+nr][nc][i][j] + _VA[pos1]*d_VdPv[Row_start1+nr][nc][i][j];
          }
        }
      }
    }
  }
}                                   
    


void StochDMNDG3DMaterialLaw::get_matrixdNdP(std::vector< std::vector <SPoint2> >& dVAdPv, std::vector<std::vector<std::vector<std::vector<double>>>>& d_NvdPv, std::vector<std::vector<std::vector<std::vector<double>>>>& d_NvdPn, std::vector<std::vector<std::vector<std::vector<double>>>>& d_NTdPn)const {

  double dN_a[9][3];
  double dN_b[9][3];
  double Norm[9][3];
  int pos0, pos1;
  int Row_start, Row_start0, Row_start1;
  int Col_start, Col_start0, Col_start1;
  int node_start0, node_end0, node_start1, node_end1;
  int l = 0;
  int col=9;
  for(int pos=0; pos <_NInterface; pos++){  
    l = _NodeLevel[pos];
    getdNLocaldP(pos, dN_a, dN_b);
    pos0 = _Child[pos][0];
    pos1 = _Child[pos][1];
    Row_start = pos*3;
    Col_start0 = (pos0-1)*col;
    Col_start1 = (pos1-1)*col;
    
    double S_ba = 1.0;    
    if(l ==_TotalLevel-1 and _porous) S_ba = _Para_Wt[pos][1];  
    // information of interface norm
    for(int nc=0; nc<col;nc++){
      for(int nr=0; nr<3;nr++){
        d_NTdPn[Row_start+nr][Col_start0+nc][pos][0] = dN_a[nc][nr];
        d_NTdPn[Row_start+nr][Col_start1+nc][pos][0] = -S_ba*dN_a[nc][nr];
        if(_Dim ==3){
          d_NTdPn[Row_start+nr][Col_start0+nc][pos][1] = dN_b[nc][nr];
          d_NTdPn[Row_start+nr][Col_start1+nc][pos][1] = -S_ba*dN_b[nc][nr];     
        }      
      }
    } 
    
   //information of homogenous strain to local strain 
    Col_start = pos*3;
    getLeafOfInterface(pos0, node_start0, node_end0);
    getLeafOfInterface(pos1, node_start1, node_end1);  
    
    for(int r=node_start0; r<= node_end0; r++){
      Row_start0 = (r-_NInterface)*col;
      for(int nr=0; nr<col;nr++){
        for(int nc=0; nc<3;nc++){
          d_NvdPn[Row_start0+nr][Col_start+nc][pos][0] = dN_a[nr][nc]/_VA[pos0];
          if(_Dim ==3){
            d_NvdPn[Row_start0+nr][Col_start+nc][pos][1] = dN_b[nr][nc]/_VA[pos0];
          }            
        }
      }
    } 
    for(int r=node_start1; r<= node_end1; r++){ 
      Row_start1 = (r-_NInterface)*col;
      for(int nr=0; nr<col;nr++){
        for(int nc=0; nc<3;nc++){
          d_NvdPn[Row_start1+nr][Col_start+nc][pos][0] = -S_ba*dN_a[nr][nc]/_VA[pos1];
          if(_Dim ==3){
            d_NvdPn[Row_start1+nr][Col_start+nc][pos][1] = -S_ba*dN_b[nr][nc]/_VA[pos1];
          }            
        }
      }
    }   
    
    
    // respect to Pv  --------------------------
    fill_NLocal(pos,Norm); 
    for(int r=node_start0; r<= node_end0; r++){
      Row_start0 = (r-_NInterface)*col;
      for(int nr=0; nr<col;nr++){
        for(int nc=0; nc<3;nc++){
          int Np = std::min(pos+1, _NPara_Wt);
          for(int p = 0; p< Np; p++){  
            for(int q = 0; q<2; q++){ 
              d_NvdPv[Row_start0+nr][Col_start+nc][p][q] = -Norm[nr][nc]/_VA[pos0]/_VA[pos0] * dVAdPv[pos0][p][q];
            }        
          }
        }
      } 
    }  
    
    for(int r=node_start1; r<= node_end1; r++){
      Row_start1 = (r-_NInterface)*col;
      for(int nr=0; nr<col;nr++){
        for(int nc=0; nc<3;nc++){
          int Np = std::min(pos+1, _NPara_Wt);
          for(int p = 0; p< Np; p++){  
            for(int q = 0; q<2; q++){ 
              d_NvdPv[Row_start1+nr][Col_start+nc][p][q] = S_ba*Norm[nr][nc]/_VA[pos1]/_VA[pos1] * dVAdPv[pos1][p][q];
            }        
          }
          if(l ==_TotalLevel-1 and _porous){
            d_NvdPv[Row_start1+nr][Col_start+nc][pos][1] -= Norm[nr][nc]/_VA[pos1];   
          }      
        }
      } 
    }   
  }
}

// for porous material
void StochDMNDG3DMaterialLaw::get_matrixdNTdPv(std::vector<std::vector<std::vector<std::vector<double>>>>& d_NTdPv)const {

  double Norm[9][3];
  int pos0, pos1;
  int Row_start;
  int Col_start1;
  int col=9;  
  int pos_start = _LevelNode[_TotalLevel-1][0];
  for(int pos=pos_start; pos <_NInterface; pos++){  
    pos0 = _Child[pos][0];
    pos1 = _Child[pos][1];
    fill_NLocal(pos,Norm); 
    Row_start = (pos-pos_start)*3;    //We keep only lower part of d_NTdPv which is no zero . for porous material
    Col_start1 = (pos1-1)*col;    
    for(int nc=0; nc<col;nc++){
      for(int nr=0; nr<3;nr++){
        d_NTdPv[Row_start+nr][Col_start1+nc][pos][1] = -Norm[nc][nr];    
      }
    }     
  }
}



//
    
void StochDMNDG3DMaterialLaw::getParameterDerivative(const IPVariable*ipv, std::vector< fullMatrix<double> >& dPdPn, std::vector< fullMatrix<double> >& dPdPv) const 
{
  static std::vector< std::vector <SPoint2> > dVAdPv;
  getdVAdP(dVAdPv);
  // Create a 4D matrix using nested vectors
  int A = _V.size1();
  int B = _V.size2();
  int C = _NPara_Wt; 
  int D = 2;
  
  static std::vector<std::vector<std::vector<std::vector<double>>>> d_VdPv(A, std::vector<std::vector<std::vector<double>>>(B,
  std::vector<std::vector<double>>(C, std::vector<double>(D, 0.0)))); 
  get_matrixdVdPv(dVAdPv, d_VdPv);  
  
  A = _Nv.size1();
  B = _Nv.size2();
  static std::vector<std::vector<std::vector<std::vector<double>>>> d_NvdPv(A, std::vector<std::vector<std::vector<double>>>(B,
  std::vector<std::vector<double>>(C, std::vector<double>(D, 0.0)))); 
  C = _NPara_Norm; 
  static std::vector<std::vector<std::vector<std::vector<double>>>> d_NvdPn(A, std::vector<std::vector<std::vector<double>>>(B,
  std::vector<std::vector<double>>(C, std::vector<double>((_Dim-1), 0.0)))); 
  A = _NT.size1();
  B = _NT.size2();  
  static std::vector<std::vector<std::vector<std::vector<double>>>> d_NTdPn(A, std::vector<std::vector<std::vector<double>>>(B,
  std::vector<std::vector<double>>(C, std::vector<double>((_Dim-1), 0.0)))); 
  A = int(3*pow(2, _TotalLevel-1));
  static std::vector<std::vector<std::vector<std::vector<double>>>> d_NTdPv(A, std::vector<std::vector<std::vector<double>>>(B,
  std::vector<std::vector<double>>(C, std::vector<double>(2, 0.0))));

  if(_porous){ 
     get_matrixdNTdPv(d_NTdPv); 
  }
  get_matrixdNdP(dVAdPv, d_NvdPv,  d_NvdPn,  d_NTdPn);   
  ///---------------------------------------------------------------  
   /* get ipvariable */
  const StochDMNDG3DIPVariable* ipvcur = static_cast<const StochDMNDG3DIPVariable*>(ipv);
  const fullVector<double>& Pvec = ipvcur->getConstRefToFirstPiolaKirchhoffStressVect();
  const fullVector<double>& a_cur = ipvcur->getConstRefToInterfaceFluctuationVect();  
  
  static std::vector<std::vector<std::vector<double>>> dA_dPv(3*_NInterface, std::vector<std::vector<double>>(_NPara_Wt, std::vector<double>(2, 0.0)));   
  static std::vector<std::vector<std::vector<double>>> tmp_v(3*_NInterface, std::vector<std::vector<double>>(_NPara_Wt, std::vector<double>(2, 0.0))); 
 
  int pos_start = _LevelNode[_TotalLevel-1][0]; 
  int node_start0, node_end0, node_start1, node_end1; 
  int _NTp,node;
  int col = 9; 
  int pos0, pos1; 
  int K_start, K_end, J_start, J_end;
  for(int pos=0; pos<_NInterface; pos++){
    _NTp = pos - pos_start;
    pos0 = _Child[pos][0];
    pos1 = _Child[pos][1];
    getLeafOfInterface(pos0, node_start0, node_end0);
    getLeafOfInterface(pos1, node_start1, node_end1); 
    J_start = (pos0-1)*col;
    J_end = pos1*col;
    K_start = (node_start0-_NInterface)*col;
    K_end = (node_end1-_NInterface)*col;
    for(int d=0; d<3; d++){ 
      int I = 3*pos + d;
      for(int p=0; p<_NPara_Wt; p++){
        for(int q=0; q<2; q++){
          tmp_v[I][p][q] = 0.0;        
          for(int j = J_start; j< J_end; j++){ 
            for(int k = K_start; k< K_end; k++){ 
              tmp_v[I][p][q] += _NT(I,j)*d_VdPv[j][k][p][q]*Pvec(k);
              if(_porous and _NTp>=0){
                tmp_v[I][p][q] += d_NTdPv[_NTp*3+d][j][p][q]*_V(j,k)*Pvec(k);
              }           
            }
          }  
          for(int j =K_start; j< K_end; j++){ 
            for(int k =K_start; k< K_end; k++){ 
              for(int ll =0; ll< 3*_NInterface; ll++){          
                tmp_v[I][p][q] += _NTV(I,j)*_C(j,k)*d_NvdPv[k][ll][p][q]*a_cur(ll);
              }
            }
          }
        }
      } 
    }  
    for(int p=0; p<_NPara_Wt; p++){
      for(int q=0; q<2; q++){  
        for(int m=0; m<3*_NInterface; m++){ 
          dA_dPv[m][p][q] = 0.0;
          for(int i=0; i<3*_NInterface; i++){          
            dA_dPv[m][p][q] += Jacobian(m,i)*tmp_v[i][p][q];
          }
        }        
      }
    }   
  }    
    
  int NDim = 1;
  if(_Dim ==3) NDim = 2;
  static std::vector<std::vector<std::vector<double>>> dA_dPn(3*_NInterface, std::vector<std::vector<double>>(_NPara_Norm, std::vector<double>(NDim, 0.0)));     
  static std::vector<std::vector<std::vector<double>>> tmp_n(3*_NInterface, std::vector<std::vector<double>>(_NPara_Norm, std::vector<double>(NDim, 0.0)));    
  
  int pos;
  for(int i=0; i<3*_NInterface; i++){ 
    pos = int(i/3);
    pos0 = _Child[pos][0];
    pos1 = _Child[pos][1];
    
    getLeafOfInterface(pos0, node_start0, node_end0);
    getLeafOfInterface(pos1, node_start1, node_end1); 
    J_start = (pos0-1)*col;
    J_end = pos1*col;
    K_start = (node_start0-_NInterface)*col;
    K_end = (node_end1-_NInterface)*col;
    for(int p=pos; p<_NPara_Norm; p++){
      for(int q=0; q<NDim; q++){     
        tmp_n[i][p][q] = 0.0; 
        for(int j =K_start; j< K_end; j++){ 
          for(int k =K_start; k< K_end; k++){ 
            for(int ll =0; ll< 3*_NInterface; ll++){          
              tmp_n[i][p][q] += _NTV(i,j)*_C(j,k)*d_NvdPn[k][ll][p][q]*a_cur(ll);
            }
          }
        }
      }
    }    
    for(int p=pos; p<pos+1; p++){
      for(int q=0; q<NDim; q++){              
        for(int j =J_start; j< J_end; j++){
          for(int k =K_start; k< K_end; k++){ 
            tmp_n[i][p][q] += d_NTdPn[i][j][p][q]*_V(j,k)*Pvec(k);
          }
        }   
      }  
    }  
  }    
  
  for(int p=pos; p<_NPara_Norm; p++){
    for(int q=0; q<NDim; q++){  
      for(int m=0; m<3*_NInterface; m++){ 
        dA_dPn[m][p][q] = 0.0;  
        for(int i=0; i<3*_NInterface; i++){  
          dA_dPn[m][p][q] += Jacobian(m,i)*tmp_n[i][p][q];
        } 
      }  
    }
  }  

  static std::vector<std::vector<std::vector<double>>> dS_dPv(9*_NLeaf, std::vector<std::vector<double>>(_NPara_Wt, std::vector<double>(2, 0.0)));  
  static std::vector<std::vector<std::vector<double>>> dS_dPn(9*_NLeaf, std::vector<std::vector<double>>(_NPara_Norm, std::vector<double>(NDim, 0.0)));
  static std::vector<std::vector<std::vector<double>>> tmp_dPv(9*_NLeaf, std::vector<std::vector<double>>(_NPara_Wt, std::vector<double>(2, 0.0))); 
  static std::vector<std::vector<std::vector<double>>> tmp_dPn(9*_NLeaf, std::vector<std::vector<double>>(_NPara_Norm, std::vector<double>(NDim, 0.0))); 
  
  for(int p=0; p<_NPara_Wt; p++){
    pos0 = _Child[p][0];
    pos1 = _Child[p][1];
    getLeafOfInterface(pos0, node_start0, node_end0);
    getLeafOfInterface(pos1, node_start1, node_end1); 
    int i_start = (node_start0-_NInterface)*col;
    int i_end = (node_end1-_NInterface)*col;   
    for(int q=0; q<2; q++){   
      for(int j=0; j<9*_NLeaf; j++){ 
        tmp_dPv[j][p][q] = 0.0;
        for(int k =0; k< 3*_NInterface; k++){ 
          tmp_dPv[j][p][q] += _Nv(j,k)* dA_dPv[k][p][q];
        }
      }     
      for(int j=i_start; j<i_end; j++){ 
        for(int k =0; k< 3*_NInterface; k++){ 
          tmp_dPv[j][p][q] += d_NvdPv[j][k][p][q]*a_cur(k);
        }
      } 
      for(int i=0; i<_NLeaf*col;i++){ 
        dS_dPv[i][p][q] = 0.0;
        J_start = int(i/col)*col;
        for(int j=J_start; j<J_start+col; j++){                
          dS_dPv[i][p][q] += _C(i,j)*tmp_dPv[j][p][q];
        }  
      }
    }
  } 
    

  for(int p=0; p<_NPara_Norm; p++){  
    pos0 = _Child[p][0];
    pos1 = _Child[p][1];
    getLeafOfInterface(pos0, node_start0, node_end0);
    getLeafOfInterface(pos1, node_start1, node_end1); 
    int i_start = (node_start0-_NInterface)*col;
    int i_end = (node_end1-_NInterface)*col;   
    
    for(int q=0; q<NDim; q++){
      for(int j=0; j<9*_NLeaf; j++){ 
        tmp_dPn[j][p][q] = 0.0;
        for(int k =0; k< 3*_NInterface; k++){   
          tmp_dPn[j][p][q] += _Nv(j,k)* dA_dPn[k][p][q];
        }
      }
      for(int j=i_start; j<i_end; j++){ 
        for(int k =0; k< 3*_NInterface; k++){   
          tmp_dPn[j][p][q] += d_NvdPn[j][k][p][q]*a_cur(k);
        }
      }     
      for(int i=0; i<9*_NLeaf; i++){ 
        dS_dPn[i][p][q] = 0.0;  
        J_start = int(i/col)*col;
        for(int j=J_start; j<J_start+col; j++){           
          dS_dPn[i][p][q] += _C(i,j)* tmp_dPn[j][p][q];
        }
      }
    }
  }     
      
  
  static std::vector<std::vector<std::vector<std::vector<double>>>> tmp(col, std::vector<std::vector<std::vector<double>>>(_V.size2(),
  std::vector<std::vector<double>>(_NPara_Wt, std::vector<double>(2, 0.0)))); 
  pos = 0;
  pos0 = _Child[pos][0];
  pos1 = _Child[pos][1];
  int Row_start0 = (pos0-1)*col;
  int Row_start1 = (pos1-1)*col;
  for(int nr=0; nr<col;nr++){
    for(int nc=0; nc<col*_NLeaf ; nc++){ 
      for(int i=0; i<_NPara_Wt; i++){
        for(int j=0; j<2; j++){
          tmp[nr][nc][i][j] = dVAdPv[pos0][i][j]*_V(Row_start0+nr,nc)+dVAdPv[pos1][i][j]*_V(Row_start1+nr,nc)
                                    + _VA[pos0]* d_VdPv[Row_start0+nr][nc][i][j] + _VA[pos1]*d_VdPv[Row_start1+nr][nc][i][j];
        }
      }
    }
  }
  
  for(int i=0; i<col; i++){
    dPdPv[i].setAll(0.0);
    for(int p=0; p<_NPara_Wt; p++){
      for(int q=0; q<2; q++){
        for(int nc=0; nc<col*_NLeaf ; nc++){ 
          dPdPv[i](p,q) += tmp[i][nc][p][q]*Pvec(nc) + (_VA[pos0]*_V(Row_start0+i,nc)+ _VA[pos1]*_V(Row_start1+i,nc))*dS_dPv[nc][p][q];
        }
      }
    }
  } 
  
  for(int i=0; i<col; i++){
    dPdPn[i].setAll(0.0);
    for(int p=0; p<_NPara_Norm; p++){
      for(int q=0; q<NDim; q++){
        for(int nc=0; nc<col*_NLeaf ; nc++){ 
          dPdPn[i](p,q) += (_VA[pos0]*_V(Row_start0+i,nc)+ _VA[pos1]*_V(Row_start1+i,nc))*dS_dPn[nc][p][q];
        }
      }
    }
  } 
}       
void StochDMNDG3DMaterialLaw::setTime(const double t,const double dtime){
  dG3DMaterialLaw::setTime(t,dtime);
  for (std::vector<dG3DMaterialLaw*>::iterator it = _mapLaw.begin(); it != _mapLaw.end(); it++) {
    (*it)->setTime(t,dtime);
  }
}

//
J2SmallStrainDG3DMaterialLaw::J2SmallStrainDG3DMaterialLaw(const int num,const double rho,
                                   double E,const double nu,const double sy0,const double h,
                                   const double tol,const bool tangentByPert, const double pert) : dG3DMaterialLaw(num,rho,true),
                                                             _j2law(num,E,nu,rho,sy0,h,tol,tangentByPert,pert)
{
  fillElasticStiffness(E, nu, elasticStiffness);
}

J2SmallStrainDG3DMaterialLaw::J2SmallStrainDG3DMaterialLaw(const int num,const double rho,
                                   double E, const double nu, const J2IsotropicHardening &_j2IH,
                                   const double tol, const bool tangentByPert, const double pert) : dG3DMaterialLaw(num,rho,true),
                                   _j2law(num,E,nu,rho,_j2IH,tol,tangentByPert,pert)
{
  fillElasticStiffness(E, nu, elasticStiffness);
}

J2SmallStrainDG3DMaterialLaw::J2SmallStrainDG3DMaterialLaw(const J2SmallStrainDG3DMaterialLaw &source) : dG3DMaterialLaw(source), _j2law(source._j2law)
{

}

void J2SmallStrainDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  IPVariable* ipvi = new  J2SmallStrainDG3DIPVariable(_j2law,hasBodyForce,inter);
  IPVariable* ipv1 = new  J2SmallStrainDG3DIPVariable(_j2law,hasBodyForce,inter);
  IPVariable* ipv2 = new  J2SmallStrainDG3DIPVariable(_j2law,hasBodyForce,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void J2SmallStrainDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  ipv = new  J2SmallStrainDG3DIPVariable(_j2law,hasBodyForce,inter);
}

void J2SmallStrainDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  /* get ipvariable */
  J2SmallStrainDG3DIPVariable* ipvcur;
  const J2SmallStrainDG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {
    ipvcur = static_cast<J2SmallStrainDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const J2SmallStrainDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<J2SmallStrainDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const J2SmallStrainDG3DIPVariable*>(ipvp);
  };

  _j2law.checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());
};


void J2SmallStrainDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  /* get ipvariable */
  J2SmallStrainDG3DIPVariable* ipvcur;
  const J2SmallStrainDG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<J2SmallStrainDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const J2SmallStrainDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<J2SmallStrainDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const J2SmallStrainDG3DIPVariable*>(ipvp);
  }

  /* strain xyz */
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

  /* data for J2 law */
  IPJ2linear* q1 = ipvcur->getIPJ2linear();
  const IPJ2linear* q0 = ipvprev->getIPJ2linear();
  STensor43& elasticL = ipvcur->getRefToElasticTangentModuli();
  //static STensor63 dCalgdeps;

  /* compute stress */
  _j2law.constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),stiff,&elasticL,dTangent,ipvcur->getPtrTodCmdFm());

  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);
}

//
AnisotropicPlasticityDG3DMaterialLaw::AnisotropicPlasticityDG3DMaterialLaw(const int num, std::string lawType, const double rho,
                                   double E, const double nu, const J2IsotropicHardening &_j2IH,
                                   const double tol,const bool matrixBypert, const double tolPert) :
                                   dG3DMaterialLaw(num,rho,true)
{
  fillElasticStiffness(E, nu, elasticStiffness);
  if (lawType=="J2")
  {
    _anisoLaw = new mlawAnisotropicPlasticityJ2(num,E,nu,rho,_j2IH,tol,matrixBypert,tolPert);
  }
  else if (lawType == "Hill48")
  {
    _anisoLaw = new mlawAnisotropicPlasticityHill48(num,E,nu,rho,_j2IH,tol,matrixBypert,tolPert);
  }
  else
  {
    Msg::Error("law type %s has not been implemented !!!", lawType.c_str());
  }
}

void AnisotropicPlasticityDG3DMaterialLaw::setStrainOrder(const int order){
  _anisoLaw->setStrainOrder(order);
};

void AnisotropicPlasticityDG3DMaterialLaw::setStressFormulation(int s)
{
  _anisoLaw->setStressFormulation(s);
};

void AnisotropicPlasticityDG3DMaterialLaw::setYieldParameters(const std::vector<double>& vals)
{
  _anisoLaw->setYieldParameters(vals);
}

void AnisotropicPlasticityDG3DMaterialLaw::setYieldParameters(const fullVector<double>& vals)
{
  std::vector<double> vv(vals.size());
  for (int i=0; i< vals.size(); i++)
  {
    vv[i] = vals(i);
  }
  _anisoLaw->setYieldParameters(vv);
}

AnisotropicPlasticityDG3DMaterialLaw::AnisotropicPlasticityDG3DMaterialLaw(const AnisotropicPlasticityDG3DMaterialLaw &source) :
        dG3DMaterialLaw(source)
{
  _anisoLaw = NULL;
  if (source._anisoLaw != NULL)
  {
    _anisoLaw = dynamic_cast<mlawAnisotropicPlasticity*>(source._anisoLaw->clone());
  }
}
AnisotropicPlasticityDG3DMaterialLaw::~AnisotropicPlasticityDG3DMaterialLaw()
{
  if (_anisoLaw!=NULL)
  {
    delete _anisoLaw;
    _anisoLaw = NULL;
  }
}


void AnisotropicPlasticityDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  IPVariable* ipvi = new  AnisotropicPlasticityDG3DIPVariable(*_anisoLaw,hasBodyForce,inter);
  IPVariable* ipv1 = new  AnisotropicPlasticityDG3DIPVariable(*_anisoLaw,hasBodyForce,inter);
  IPVariable* ipv2 = new  AnisotropicPlasticityDG3DIPVariable(*_anisoLaw,hasBodyForce,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void AnisotropicPlasticityDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  ipv = new  AnisotropicPlasticityDG3DIPVariable(*_anisoLaw,hasBodyForce,inter);
}

void AnisotropicPlasticityDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
    /* get ipvariable */
  AnisotropicPlasticityDG3DIPVariable* ipvcur;
  const AnisotropicPlasticityDG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<AnisotropicPlasticityDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const AnisotropicPlasticityDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<AnisotropicPlasticityDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const AnisotropicPlasticityDG3DIPVariable*>(ipvp);
  }
  _anisoLaw->checkInternalState(ipvcur->getInternalData(), ipvprev->getInternalData());

};

void AnisotropicPlasticityDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  /* get ipvariable */
  AnisotropicPlasticityDG3DIPVariable* ipvcur;
  const AnisotropicPlasticityDG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<AnisotropicPlasticityDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const AnisotropicPlasticityDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<AnisotropicPlasticityDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const AnisotropicPlasticityDG3DIPVariable*>(ipvp);
  }

  /* strain xyz */
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

  /* data for J2 law */
  IPAnisotropicPlasticity* q1 = ipvcur->getIPAnisotropicPlasticity();
  const IPAnisotropicPlasticity* q0 = ipvprev->getIPAnisotropicPlasticity();

  STensor43& elasticL = ipvcur->getRefToElasticTangentModuli();
  //static STensor63 dCalgdeps;

  /* compute stress */
  _anisoLaw->constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),stiff,&elasticL,dTangent,ipvcur->getPtrTodCmdFm());

  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);
}
//



J2LinearDG3DMaterialLaw::J2LinearDG3DMaterialLaw(const int num,const double rho,
                                   double E,const double nu,const double sy0,const double h,
                                   const double tol, const bool matrixBypert, const double tolPert) : dG3DMaterialLaw(num,rho,true),
                                                             _j2law(num,E,nu,rho,sy0,h,tol,matrixBypert,tolPert)
{
  fillElasticStiffness(E, nu, elasticStiffness);
}

void J2LinearDG3DMaterialLaw::setStrainOrder(const int order){
  _j2law.setStrainOrder(order);
};

void J2LinearDG3DMaterialLaw::setViscosityEffect(const viscosityLaw& v, const double p){
  _j2law.setViscosityEffect(v,p);
};

void J2LinearDG3DMaterialLaw::setStressFormulation(int s)
{
  _j2law.setStressFormulation(s);
}

J2LinearDG3DMaterialLaw::J2LinearDG3DMaterialLaw(const int num,const double rho,
                                   double E, const double nu, const J2IsotropicHardening &_j2IH,
                                   const double tol,const bool matrixBypert, const double tolPert) :
                                   dG3DMaterialLaw(num,rho,true), _j2law(num,E,nu,rho,_j2IH,tol,matrixBypert,tolPert)
{
  fillElasticStiffness(E, nu, elasticStiffness);
}

J2LinearDG3DMaterialLaw::J2LinearDG3DMaterialLaw(const J2LinearDG3DMaterialLaw &source) : dG3DMaterialLaw(source), _j2law(source._j2law)
{

}

void J2LinearDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  IPVariable* ipvi = new  J2LinearDG3DIPVariable(_j2law,hasBodyForce,inter);
  IPVariable* ipv1 = new  J2LinearDG3DIPVariable(_j2law,hasBodyForce,inter);
  IPVariable* ipv2 = new  J2LinearDG3DIPVariable(_j2law,hasBodyForce,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void J2LinearDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  ipv = new  J2LinearDG3DIPVariable(_j2law,hasBodyForce,inter);
}

void J2LinearDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
    /* get ipvariable */
  J2LinearDG3DIPVariable* ipvcur;
  const J2LinearDG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<J2LinearDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const J2LinearDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<J2LinearDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const J2LinearDG3DIPVariable*>(ipvp);
  }
  _j2law.checkInternalState(ipvcur->getInternalData(), ipvprev->getInternalData());

};

void J2LinearDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  /* get ipvariable */
  J2LinearDG3DIPVariable* ipvcur;
  const J2LinearDG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<J2LinearDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const J2LinearDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<J2LinearDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const J2LinearDG3DIPVariable*>(ipvp);
  }

  /* strain xyz */
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

  /* data for J2 law */
  IPJ2linear* q1 = ipvcur->getIPJ2linear();
  const IPJ2linear* q0 = ipvprev->getIPJ2linear();

  STensor43& elasticL = ipvcur->getRefToElasticTangentModuli();
  //static STensor63 dCalgdeps;

  /* compute stress */
  _j2law.constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),stiff,&elasticL,dTangent,ipvcur->getPtrTodCmdFm());

  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);
}
//

//

TrescaDG3DMaterialLaw::TrescaDG3DMaterialLaw(const int num,const double rho,
                                   double E, const double nu, const J2IsotropicHardening &_j2IH,
                                   const double tol,const bool matrixBypert, const double tolPert) :
                                   dG3DMaterialLaw(num,rho,true)
{

  fillElasticStiffness(E, nu, elasticStiffness);
  double fVinitial = 0.;
  double lambda0 = 1.;
  ZeroCLengthLaw zeroL(num);
  _trescaLaw = new mlawNonLocalPorousThomasonLawWithMSS(num,E,nu,rho,fVinitial,lambda0,_j2IH,zeroL,tol,matrixBypert,tolPert);
  _trescaLaw->setNonLocalMethod(0);
  NoCoalescenceLaw coales1(num);
	_trescaLaw->setCoalescenceLaw(coales1);
	TrivialEvolutionLaw evol1(num);
	_trescaLaw->setVoidEvolutionLaw(evol1);
  _trescaLaw->setStressFormulation(1); // coro Kirchoff as default
};

TrescaDG3DMaterialLaw::~TrescaDG3DMaterialLaw()
{
  if (_trescaLaw != NULL) delete _trescaLaw;
}

void TrescaDG3DMaterialLaw::setStrainOrder(const int order){
  _trescaLaw->setOrderForLogExp(order);
};

void TrescaDG3DMaterialLaw::setStressFormulation(int s)
{
  _trescaLaw->setStressFormulation(s);
}

void TrescaDG3DMaterialLaw::setSmoothFactor(double factor)
{
  _trescaLaw->setSmoothFactor(factor);
}

TrescaDG3DMaterialLaw::TrescaDG3DMaterialLaw(const TrescaDG3DMaterialLaw &source) : dG3DMaterialLaw(source)
{
  _trescaLaw = NULL;
  if (source._trescaLaw != NULL)
  {
    _trescaLaw = dynamic_cast<mlawNonLocalPorousThomasonLawWithMSS*>(source._trescaLaw->clone());
  };
}

void TrescaDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
 // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  int numNL=_trescaLaw->getNumNonLocalVariables();
  int numExtraDof=0;
  if(_trescaLaw->isThermomechanicallyCoupled())
    numExtraDof=1;
  double fvInit = _trescaLaw->generateARandomInitialPorosityValue();
  IPVariable* ipvi = new  nonLocalPorosityDG3DIPVariable(*_trescaLaw,fvInit,hasBodyForce,inter,numNL,numExtraDof);
  IPVariable* ipv1 = new  nonLocalPorosityDG3DIPVariable(*_trescaLaw,fvInit,hasBodyForce,inter,numNL,numExtraDof);
  IPVariable* ipv2 = new  nonLocalPorosityDG3DIPVariable(*_trescaLaw,fvInit,hasBodyForce,inter,numNL,numExtraDof);

  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void TrescaDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  double fvInit = _trescaLaw->generateARandomInitialPorosityValue();
  int numNL=_trescaLaw->getNumNonLocalVariables();
  int numExtraDof=0;
  if(_trescaLaw->isThermomechanicallyCoupled())
    numExtraDof=1;

  if(ipv !=NULL) delete ipv;
  ipv = new  nonLocalPorosityDG3DIPVariable(*_trescaLaw,fvInit,hasBodyForce,inter,numNL,numExtraDof);
}

void TrescaDG3DMaterialLaw::initLaws(const std::map<int,materialLaw*> &maplaw)
{
  _trescaLaw->initLaws(maplaw);
}

void TrescaDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  /* get ipvariable */

  nonLocalPorosityDG3DIPVariable* ipvcur;
  const nonLocalPorosityDG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);

  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<nonLocalPorosityDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const nonLocalPorosityDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<nonLocalPorosityDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const nonLocalPorosityDG3DIPVariable*>(ipvp);
  }
  _trescaLaw->checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());
};

void TrescaDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  /* get ipvariable */

  nonLocalPorosityDG3DIPVariable* ipvcur;
  const nonLocalPorosityDG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);

  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<nonLocalPorosityDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const nonLocalPorosityDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<nonLocalPorosityDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const nonLocalPorosityDG3DIPVariable*>(ipvp);
  }

  /* strain xyz */
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

  /* data for J2 law */
  IPNonLocalPorosity* q1 = ipvcur->getIPNonLocalPorosity();
  const IPNonLocalPorosity* q0 = ipvprev->getIPNonLocalPorosity();

  // set additional kinematic variables
	q1->setRefToCurrentOutwardNormal(ipvcur->getConstRefToCurrentOutwardNormal());
	q1->setRefToReferenceOutwardNormal(ipvcur->getConstRefToReferenceOutwardNormal());
  q1->setRefToDeformationGradient(ipvcur->getConstRefToDeformationGradient());

  /* compute stress */
  if(!_trescaLaw->isThermomechanicallyCoupled())
  {
    _trescaLaw->I1J2J3_constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),stiff);
  }
  else
  {
    Msg::Error("local model for thermomechanics has not been implemented!!!");
    Msg::Exit(0);
  };
  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);
};

//

HyperViscoElasticDG3DMaterialLaw::HyperViscoElasticDG3DMaterialLaw(const int num, const double rho, const double E,const double nu,
                        const bool matrixbyPerturbation, const double pert): dG3DMaterialLaw(num,rho), _viscoLaw(num,E,nu,rho,matrixbyPerturbation,pert){
  fillElasticStiffness(E, nu, elasticStiffness);
};

HyperViscoElasticDG3DMaterialLaw::HyperViscoElasticDG3DMaterialLaw(const HyperViscoElasticDG3DMaterialLaw& src):  dG3DMaterialLaw(src),
  _viscoLaw(src._viscoLaw){};
void HyperViscoElasticDG3DMaterialLaw::setStrainOrder(const int order){
  _viscoLaw.setStrainOrder(order);
};
void HyperViscoElasticDG3DMaterialLaw::setViscoelasticMethod(const int method){
  _viscoLaw.setViscoelasticMethod(method);
};
void HyperViscoElasticDG3DMaterialLaw::setExtraBranchNLType(const int method){
  _viscoLaw.setExtraBranchNLType(method);
};


void HyperViscoElasticDG3DMaterialLaw::setViscoElasticNumberOfElement(const int N){
  _viscoLaw.setViscoElasticNumberOfElement(N);
};
void HyperViscoElasticDG3DMaterialLaw::setViscoElasticData(const int i, const double Ei, const double taui){
  _viscoLaw.setViscoElasticData(i,Ei,taui);
};
void HyperViscoElasticDG3DMaterialLaw::setViscoElasticData_Bulk(const int i, const double Ki, const double ki){
  _viscoLaw.setViscoElasticData_Bulk(i,Ki,ki);
};
void HyperViscoElasticDG3DMaterialLaw::setViscoElasticData_Shear(const int i, const double Gi, const double gi){
  _viscoLaw.setViscoElasticData_Shear(i,Gi,gi);
};
void HyperViscoElasticDG3DMaterialLaw::setViscoElasticData(const std::string filename){
  _viscoLaw.setViscoElasticData(filename);
};
void HyperViscoElasticDG3DMaterialLaw::setVolumeCorrection(double _vc, double _xivc, double _zetavc ,double _dc, double _thetadc, double _pidc){
  _viscoLaw.setVolumeCorrection(_vc, _xivc, _zetavc, _dc, _thetadc, _pidc);
};
void HyperViscoElasticDG3DMaterialLaw::setAdditionalVolumeCorrections(const double V3, const double V4, const double V5, const double D3, const double D4, const double D5){
  _viscoLaw.setAdditionalVolumeCorrections(V3,V4,V5,D3,D4,D5);
};
void HyperViscoElasticDG3DMaterialLaw::setExtraBranch_CompressionParameter(const double compCorrection, const double compCorrection_2, const double compCorrection_3){
  _viscoLaw.setExtraBranch_CompressionParameter(compCorrection,compCorrection_2,compCorrection_3);
};
void HyperViscoElasticDG3DMaterialLaw::setTensionCompressionRegularisation(double tensionCompressionRegularisation){
  _viscoLaw.setTensionCompressionRegularisation(tensionCompressionRegularisation);
};

void HyperViscoElasticDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  IPVariable* ipvi = new  HyperViscoElasticdG3DIPVariable(_viscoLaw,hasBodyForce,inter);
  IPVariable* ipv1 = new  HyperViscoElasticdG3DIPVariable(_viscoLaw,hasBodyForce,inter);
  IPVariable* ipv2 = new  HyperViscoElasticdG3DIPVariable(_viscoLaw,hasBodyForce,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void HyperViscoElasticDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  ipv = new  HyperViscoElasticdG3DIPVariable(_viscoLaw,hasBodyForce,inter);
}

void HyperViscoElasticDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  /* get ipvariable */
  HyperViscoElasticdG3DIPVariable* ipvcur;
  const HyperViscoElasticdG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<HyperViscoElasticdG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const HyperViscoElasticdG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<HyperViscoElasticdG3DIPVariable*>(ipv);
    ipvprev = static_cast<const HyperViscoElasticdG3DIPVariable*>(ipvp);
  }

  _viscoLaw.checkInternalState(ipvcur->getInternalData(), ipvprev->getInternalData());
};

void HyperViscoElasticDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  /* get ipvariable */
  HyperViscoElasticdG3DIPVariable* ipvcur;
  const HyperViscoElasticdG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<HyperViscoElasticdG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const HyperViscoElasticdG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<HyperViscoElasticdG3DIPVariable*>(ipv);
    ipvprev = static_cast<const HyperViscoElasticdG3DIPVariable*>(ipvp);
  }

  /* strain xyz */
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

  /* data for J2 law */
  IPHyperViscoElastic* q1 = ipvcur->getIPHyperViscoElastic();
  const IPHyperViscoElastic* q0 = ipvprev->getIPHyperViscoElastic();
  //static STensor63 dCalgdeps;
  //STensor43& elasticL = ipvcur->getRefToElasticTangentModuli();

  /* compute stress */
  _viscoLaw.constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),stiff,NULL,dTangent,ipvcur->getPtrTodCmdFm());

  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);
}

HyperViscoElastoPlasticPowerYieldDG3DMaterialLaw::HyperViscoElastoPlasticPowerYieldDG3DMaterialLaw(const int num, const double rho, const double E,const double nu, const double tol ,
                        const bool matrixbyPerturbation, const double pert):
                        dG3DMaterialLaw(num,rho), _viscoLaw(num,E,nu,rho,tol,matrixbyPerturbation,pert)
{
  fillElasticStiffness(E, nu, elasticStiffness);
};
HyperViscoElastoPlasticPowerYieldDG3DMaterialLaw::HyperViscoElastoPlasticPowerYieldDG3DMaterialLaw(const HyperViscoElastoPlasticPowerYieldDG3DMaterialLaw& src): dG3DMaterialLaw(src),
                      _viscoLaw(src._viscoLaw){};

void HyperViscoElastoPlasticPowerYieldDG3DMaterialLaw::setStrainOrder(const int order){
  _viscoLaw.setStrainOrder(order);
};
void HyperViscoElastoPlasticPowerYieldDG3DMaterialLaw::setViscoelasticMethod(const int method){
  _viscoLaw.setViscoelasticMethod(method);
};
void HyperViscoElastoPlasticPowerYieldDG3DMaterialLaw::setViscoElasticNumberOfElement(const int N){
  _viscoLaw.setViscoElasticNumberOfElement(N);
};
void HyperViscoElastoPlasticPowerYieldDG3DMaterialLaw::setViscoElasticData(const int i, const double Ei, const double taui){
  _viscoLaw.setViscoElasticData(i,Ei,taui);
};
void HyperViscoElastoPlasticPowerYieldDG3DMaterialLaw::setViscoElasticData_Bulk(const int i, const double Ki, const double ki){
  _viscoLaw.setViscoElasticData_Bulk(i,Ki,ki);
};
void HyperViscoElastoPlasticPowerYieldDG3DMaterialLaw::setViscoElasticData_Shear(const int i, const double Gi, const double gi){
  _viscoLaw.setViscoElasticData_Shear(i,Gi,gi);
};
void HyperViscoElastoPlasticPowerYieldDG3DMaterialLaw::setViscoElasticData(const std::string filename){
  _viscoLaw.setViscoElasticData(filename);
};
void HyperViscoElastoPlasticPowerYieldDG3DMaterialLaw::setVolumeCorrection(double _vc, double _xivc, double _zetavc ,double _dc, double _thetadc, double _pidc){
  _viscoLaw.setVolumeCorrection(_vc, _xivc, _zetavc, _dc, _thetadc, _pidc);
};
void HyperViscoElastoPlasticPowerYieldDG3DMaterialLaw::setAdditionalVolumeCorrections(const double V3, const double V4, const double V5, const double D3, const double D4, const double D5){
  _viscoLaw.setAdditionalVolumeCorrections(V3,V4,V5,D3,D4,D5);
};
void HyperViscoElastoPlasticPowerYieldDG3DMaterialLaw::setExtraBranch_CompressionParameter(const double compCorrection, const double compCorrection_2, const double compCorrection_3){
  _viscoLaw.setExtraBranch_CompressionParameter(compCorrection,compCorrection_2,compCorrection_3);
};
void HyperViscoElastoPlasticPowerYieldDG3DMaterialLaw::setExtraBranchNLType(const int method){
  _viscoLaw.setExtraBranchNLType(method);
};
void HyperViscoElastoPlasticPowerYieldDG3DMaterialLaw::setTensionCompressionRegularisation(double tensionCompressionRegularisation){
  _viscoLaw.setTensionCompressionRegularisation(tensionCompressionRegularisation);
};

void HyperViscoElastoPlasticPowerYieldDG3DMaterialLaw::setCompressionHardening(const J2IsotropicHardening& comp){
  _viscoLaw.setCompressionHardening(comp);
};
void HyperViscoElastoPlasticPowerYieldDG3DMaterialLaw::setTractionHardening(const J2IsotropicHardening& trac){
  _viscoLaw.setTractionHardening(trac);
};
void HyperViscoElastoPlasticPowerYieldDG3DMaterialLaw::setKinematicHardening(const kinematicHardening& kin){
  _viscoLaw.setKinematicHardening(kin);
};
void HyperViscoElastoPlasticPowerYieldDG3DMaterialLaw::setViscosityEffect(const viscosityLaw& v, const double p){
  _viscoLaw.setViscosityEffect(v,p);
};

void HyperViscoElastoPlasticPowerYieldDG3DMaterialLaw::setYieldPowerFactor(const double n){
  _viscoLaw.setPowerFactor(n);
};
void HyperViscoElastoPlasticPowerYieldDG3DMaterialLaw::nonAssociatedFlowRuleFactor(const double b){
  _viscoLaw.nonAssociatedFlowRuleFactor(b);
};
void HyperViscoElastoPlasticPowerYieldDG3DMaterialLaw::setPlasticPoissonRatio(const double nup){
  _viscoLaw.setPlasticPoissonRatio(nup);
};
void HyperViscoElastoPlasticPowerYieldDG3DMaterialLaw::setNonAssociatedFlow(const bool flag){
  _viscoLaw.setNonAssociatedFlow(flag);
};

void HyperViscoElastoPlasticPowerYieldDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  IPVariable* ipvi = new  HyperViscoElastoPlasticdG3DIPVariable(_viscoLaw,hasBodyForce,inter);
  IPVariable* ipv1 = new  HyperViscoElastoPlasticdG3DIPVariable(_viscoLaw,hasBodyForce,inter);
  IPVariable* ipv2 = new  HyperViscoElastoPlasticdG3DIPVariable(_viscoLaw,hasBodyForce,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void HyperViscoElastoPlasticPowerYieldDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  ipv = new  HyperViscoElastoPlasticdG3DIPVariable(_viscoLaw,hasBodyForce,inter);
}

void HyperViscoElastoPlasticPowerYieldDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  /* get ipvariable */
  HyperViscoElastoPlasticdG3DIPVariable* ipvcur;
  const HyperViscoElastoPlasticdG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<HyperViscoElastoPlasticdG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const HyperViscoElastoPlasticdG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<HyperViscoElastoPlasticdG3DIPVariable*>(ipv);
    ipvprev = static_cast<const HyperViscoElastoPlasticdG3DIPVariable*>(ipvp);
  }

  _viscoLaw.checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());
};

void HyperViscoElastoPlasticPowerYieldDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  /* get ipvariable */
  HyperViscoElastoPlasticdG3DIPVariable* ipvcur;
  const HyperViscoElastoPlasticdG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<HyperViscoElastoPlasticdG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const HyperViscoElastoPlasticdG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<HyperViscoElastoPlasticdG3DIPVariable*>(ipv);
    ipvprev = static_cast<const HyperViscoElastoPlasticdG3DIPVariable*>(ipvp);
  }

  /* strain xyz */
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

  /* data for J2 law */
  IPHyperViscoElastoPlastic* q1 = ipvcur->getIPHyperViscoElastoPlastic();
  const IPHyperViscoElastoPlastic* q0 = ipvprev->getIPHyperViscoElastoPlastic();
  //static STensor63 dCalgdeps;
  STensor43& elasticL = ipvcur->getRefToElasticTangentModuli();

  /* compute stress */
  _viscoLaw.constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),stiff,&elasticL,dTangent,ipvcur->getPtrTodCmdFm());

  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);
}




//=========================================================VEVPMFHDG3DMaterialLaw===============================================================//
VEVPMFHDG3DMaterialLaw::VEVPMFHDG3DMaterialLaw(const int num, const double rho, const char *propName) :
                               dG3DMaterialLaw(num,rho,true)
{
  _glaw = new mlawVEVPMFH(num,rho,propName);
  fillElasticStiffness(_glaw->YoungModulus(), _glaw->poissonRatio(), elasticStiffness);
}



VEVPMFHDG3DMaterialLaw::~VEVPMFHDG3DMaterialLaw()
{
  if (_glaw !=NULL) delete _glaw;
  _glaw = NULL;
};



VEVPMFHDG3DMaterialLaw::VEVPMFHDG3DMaterialLaw(const VEVPMFHDG3DMaterialLaw &source):dG3DMaterialLaw(source)
{
	_glaw = NULL;
	if (source._glaw != NULL){
		_glaw = dynamic_cast<mlawVEVPMFH*>(source._glaw->clone());
	}

}



void VEVPMFHDG3DMaterialLaw::setTime(const double t,const double dtime){
  dG3DMaterialLaw::setTime(t,dtime);
  _glaw->setTime(t,dtime);
  return;
}



void VEVPMFHDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;


  IPVariable* ipvi = new  VEVPMFHDG3DIPVariable(hasBodyForce,inter);
  IPVariable* ipv1 = new  VEVPMFHDG3DIPVariable(hasBodyForce,inter);
  IPVariable* ipv2 = new  VEVPMFHDG3DIPVariable(hasBodyForce,inter);

  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}



void VEVPMFHDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }

  ipv = new  VEVPMFHDG3DIPVariable(hasBodyForce,inter);

}



double VEVPMFHDG3DMaterialLaw::soundSpeed() const{
  return _glaw->soundSpeed();
}



void VEVPMFHDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  /* get ipvariable */
  VEVPMFHDG3DIPVariable* ipvcur;
  const VEVPMFHDG3DIPVariable* ipvprev;

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {
    ipvcur = static_cast<VEVPMFHDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const VEVPMFHDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<VEVPMFHDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const VEVPMFHDG3DIPVariable*>(ipvp);
  }
  _glaw->checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());

}



void VEVPMFHDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  VEVPMFHDG3DIPVariable* ipvcur;
  const VEVPMFHDG3DIPVariable* ipvprev;

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {
    ipvcur = static_cast<VEVPMFHDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const VEVPMFHDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }

  else
  {
    ipvcur = static_cast<VEVPMFHDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const VEVPMFHDG3DIPVariable*>(ipvp);
  }

  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

  IPVEVPMFH* q1 = ipvcur->getIPVEVPMFH();
  const IPVEVPMFH* q0 = ipvprev->getIPVEVPMFH();

  _glaw->constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),
                        stiff, NULL,dTangent,ipvcur->getPtrTodCmdFm());

  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);

}


double VEVPMFHDG3DMaterialLaw::scaleFactor() const{
  return _glaw->scaleFactor();

}


void VEVPMFHDG3DMaterialLaw::setMacroSolver(const nonLinearMechSolver* sv){
	dG3DMaterialLaw::setMacroSolver(sv);
	if (_glaw != NULL){
		_glaw->setMacroSolver(sv);
	}
}


//=========================================================ViscoelastiDG3DMaterialLaw===============================================================//
std::vector<double> ViscoelasticDG3DMaterialLaw::read_file(const char* file_name, int file_size)
{
    std::ifstream file(file_name);
    std::vector<double> output;

    if(file.is_open())
    {
        for(int i = 0; i < file_size; ++i)
        {
            std::string curr_string;
            file >> curr_string;
            double curr_number = std::atof(curr_string.c_str());
            output.push_back(curr_number);
        }
    }

    return output;
}

ViscoelasticDG3DMaterialLaw::ViscoelasticDG3DMaterialLaw(const int num,const double rho,
                                   const double K, const std::string &array_eta, int array_size_eta, const std::string &array_mu,
                                   const double tol, const bool pert, const double eps) :
                                    dG3DMaterialLaw(num,rho,true), _Vislaw(num,K,rho, array_eta, array_size_eta, array_mu, tol,pert,eps)
{
    std::vector<double> vec_mu;
    vec_mu = read_file(array_mu.c_str(),array_size_eta+1);
    double sum_mu = 0.;

    for(int i=0;i<vec_mu.size();++i)
    {
        sum_mu += vec_mu.at(i);
    }

    double E = 9.*K*sum_mu/(3.*K+sum_mu);

    double nu = (3.*K - 2.*sum_mu)/2./(3.*K + sum_mu);

    fillElasticStiffness(E, nu, elasticStiffness);

}


ViscoelasticDG3DMaterialLaw::ViscoelasticDG3DMaterialLaw(const ViscoelasticDG3DMaterialLaw &source) : dG3DMaterialLaw(source), _Vislaw(source._Vislaw)
{


}

void ViscoelasticDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{

  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  IPVariable* ipvi = new  ViscoelasticDG3DIPVariable(_Vislaw,hasBodyForce,inter);
  IPVariable* ipv1 = new  ViscoelasticDG3DIPVariable(_Vislaw,hasBodyForce,inter);
  IPVariable* ipv2 = new  ViscoelasticDG3DIPVariable(_Vislaw,hasBodyForce,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void ViscoelasticDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  ipv = new  ViscoelasticDG3DIPVariable(_Vislaw,hasBodyForce,inter);
}

void ViscoelasticDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  // get ipvariable
  ViscoelasticDG3DIPVariable* ipvcur;
  const ViscoelasticDG3DIPVariable* ipvprev;

    ipvcur = static_cast<ViscoelasticDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const ViscoelasticDG3DIPVariable*>(ipvp);

  _Vislaw.checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());
};

void ViscoelasticDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  // get ipvariable
  ViscoelasticDG3DIPVariable* ipvcur;
  const ViscoelasticDG3DIPVariable* ipvprev;

    ipvcur = static_cast<ViscoelasticDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const ViscoelasticDG3DIPVariable*>(ipvp);

  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

  IPViscoelastic* IPVis = ipvcur->getIPViscoelastic();
  const IPViscoelastic* IPVisprev = ipvprev->getIPViscoelastic();
  //static STensor63 dCalgdeps;
  STensor43& elasticL = ipvcur->getRefToElasticTangentModuli();

  // compute stress
  _Vislaw.constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),IPVisprev,IPVis,ipvcur->getRefToTangentModuli(),stiff,&elasticL,dTangent,ipvcur->getPtrTodCmdFm());
  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);

}


//-------------------------------------------EOSDG3DMaterialLaw (compatible for all the other material laws)-------------------//

EOSDG3DMaterialLaw::EOSDG3DMaterialLaw(const int num,
                                       const double rho,const double K, materialLaw *_mlaw, const double Pressure0) : dG3DMaterialLaw(num,rho,true),_EOSlaw(num,rho,K,Pressure0)
{
    // Accoustic EOS for non-artificial viscosity case
    maplaw.insert(std::pair<int,materialLaw*>(_mlaw->getNum(),_mlaw));

}

EOSDG3DMaterialLaw::EOSDG3DMaterialLaw(int flag, const int num,
                                       const double rho,const double P1, double P2, double P3, materialLaw *_mlaw, const double Pressure0) : dG3DMaterialLaw(num,rho,true),_EOSlaw(flag,num,rho,P1,P2,P3,Pressure0)
{
    // P1 = s, P2 = C0, P3 = Gamma: Hugoniot EOS for non-artificial viscosity case
    // P1 = K, P2 = c1, P3 = cl: Accoustic EOS for artificial viscosity case

    maplaw.insert(std::pair<int,materialLaw*>(_mlaw->getNum(),_mlaw));

}
EOSDG3DMaterialLaw::EOSDG3DMaterialLaw(int flag, const int num,
        const double rho,const double s, const double C0, double c1, double cl, const double Gamma, materialLaw *_mlaw) : dG3DMaterialLaw(num,rho,true),_EOSlaw(flag, num,rho, s, C0,c1,cl,Gamma)
{
    // Hugoniot EOS for artificial viscosity case
    maplaw.insert(std::pair<int,materialLaw*>(_mlaw->getNum(),_mlaw));

}

EOSDG3DMaterialLaw::EOSDG3DMaterialLaw(int flag, int flagC, const int num,
        const double rho,const double K, double c1, double cl, double ESqrd, double ESlin, materialLaw *_mlaw, const double Pressure0) : dG3DMaterialLaw(num,rho,true),_EOSlaw(flag,num,rho,K,c1,cl,Pressure0)
{
    maplaw.insert(std::pair<int,materialLaw*>(_mlaw->getNum(),_mlaw));

}

EOSDG3DMaterialLaw::EOSDG3DMaterialLaw(const EOSDG3DMaterialLaw &source) : dG3DMaterialLaw(source),_EOSlaw(source._EOSlaw),maplaw(source.maplaw)
{

}

void EOSDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
    dG3DMaterialLaw *DG3Dit = dynamic_cast<dG3DMaterialLaw*>(maplaw.begin()->second);
    materialLaw::matname mlaw_dev = DG3Dit->getType();

    if(mlaw_dev == materialLaw::J2 )//J2linear
    {
        J2LinearDG3DMaterialLaw *DG3DJ2linearit = dynamic_cast<J2LinearDG3DMaterialLaw*>(DG3Dit);
        bool inter=true;
        const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
        if(iele==NULL) inter=false;
        double size = 2.*(( const_cast<MElement*>(ele) )->getInnerRadius());

        J2LinearDG3DIPVariable* ipvi_J2 = new  J2LinearDG3DIPVariable(DG3DJ2linearit->_j2law,hasBodyForce,inter);
        J2LinearDG3DIPVariable* ipv1_J2 = new  J2LinearDG3DIPVariable(DG3DJ2linearit->_j2law,hasBodyForce,inter);
        J2LinearDG3DIPVariable* ipv2_J2 = new  J2LinearDG3DIPVariable(DG3DJ2linearit->_j2law,hasBodyForce,inter);

        IPVariable* ipvi = new  EOSDG3DIPVariable(size, ipvi_J2->getIPJ2linear(),hasBodyForce,inter);
        IPVariable* ipv1 = new  EOSDG3DIPVariable(size, ipv1_J2->getIPJ2linear(),hasBodyForce,inter);
        IPVariable* ipv2 = new  EOSDG3DIPVariable(size, ipv2_J2->getIPJ2linear(),hasBodyForce,inter);
        if(ips != NULL) delete ips;
        ips = new IP3State(state_,ipvi,ipv1,ipv2);
    }
    else if(mlaw_dev == materialLaw::viscoelastic)//viscoelastic
    {
        ViscoelasticDG3DMaterialLaw *DG3Dvisit = dynamic_cast<ViscoelasticDG3DMaterialLaw*>(DG3Dit);

        bool inter=true;
        const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
        if(iele==NULL) inter=false;
        double size = 2.*(( const_cast<MElement*>(ele) )->getInnerRadius());

        ViscoelasticDG3DIPVariable* ipvi_vis = new  ViscoelasticDG3DIPVariable(DG3Dvisit->_Vislaw,hasBodyForce,inter);
        ViscoelasticDG3DIPVariable* ipv1_vis = new  ViscoelasticDG3DIPVariable(DG3Dvisit->_Vislaw,hasBodyForce,inter);
        ViscoelasticDG3DIPVariable* ipv2_vis = new  ViscoelasticDG3DIPVariable(DG3Dvisit->_Vislaw,hasBodyForce,inter);

        IPVariable* ipvi = new  EOSDG3DIPVariable(size, ipvi_vis->getIPViscoelastic(),hasBodyForce,inter);
        IPVariable* ipv1 = new  EOSDG3DIPVariable(size, ipv1_vis->getIPViscoelastic(),hasBodyForce,inter);
        IPVariable* ipv2 = new  EOSDG3DIPVariable(size, ipv2_vis->getIPViscoelastic(),hasBodyForce,inter);
        if(ips != NULL) delete ips;
        ips = new IP3State(state_,ipvi,ipv1,ipv2);
    }
}

void EOSDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
        dG3DMaterialLaw *DG3Dit = dynamic_cast<dG3DMaterialLaw*>(maplaw.begin()->second);
        matname mlaw_dev = DG3Dit->getType();

        if(mlaw_dev == materialLaw::J2)//J2linear
        {
            J2LinearDG3DMaterialLaw *DG3DJ2linearit = dynamic_cast<J2LinearDG3DMaterialLaw*>(DG3Dit);
            bool inter=true;
            const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
            if(iele==NULL) inter=false;
            double size = 2.*(( const_cast<MElement*>(ele) )->getInnerRadius());
            J2LinearDG3DIPVariable* ipv_J2 = new  J2LinearDG3DIPVariable(DG3DJ2linearit->_j2law,hasBodyForce,inter);
            ipv = new  EOSDG3DIPVariable(size, ipv_J2,hasBodyForce,inter);
        }
        else if(mlaw_dev == materialLaw::viscoelastic)//viscoelastic
        {

            ViscoelasticDG3DMaterialLaw *DG3Dvisit = dynamic_cast<ViscoelasticDG3DMaterialLaw*>(DG3Dit);

            bool inter=true;
            const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
            if(iele==NULL) inter=false;
            double size = 2.*(( const_cast<MElement*>(ele) )->getInnerRadius());
            ViscoelasticDG3DIPVariable* ipv_vis = new  ViscoelasticDG3DIPVariable(DG3Dvisit->_Vislaw,hasBodyForce,inter);
            ipv = new  EOSDG3DIPVariable(size, ipv_vis,hasBodyForce,inter);
        }
}

void EOSDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  EOSDG3DIPVariable *DG3DEOSipv = dynamic_cast<EOSDG3DIPVariable*>(ipv);
  const EOSDG3DIPVariable *DG3DEOSipvp = static_cast<const EOSDG3DIPVariable*>(ipvp);
  _EOSlaw.checkInternalState(DG3DEOSipv->getInternalData(),DG3DEOSipvp->getInternalData());
};


void EOSDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{

    dG3DMaterialLaw *DG3Dit = dynamic_cast<dG3DMaterialLaw*>(maplaw.begin()->second);
    materialLaw::matname mlaw_dev = DG3Dit->getType();

    EOSDG3DIPVariable *DG3DEOSipv = dynamic_cast<EOSDG3DIPVariable*>(ipv);
    const EOSDG3DIPVariable *DG3DEOSipvp = static_cast<const EOSDG3DIPVariable*>(ipvp);

    IPEOS* IPeosCUR = DG3DEOSipv->getIPEOS();
    const IPEOS* IPeosPREV = DG3DEOSipvp->getIPEOS();

    STensor3 Fn(0.);
    Fn = DG3DEOSipv->getConstRefToDeformationGradient();

    STensor3 F0(0.);
    F0 = DG3DEOSipvp->getConstRefToDeformationGradient();

    STensor3 Fdot(0.);
//    STensor43 ViselasticStiff(0.);

    if(mlaw_dev == materialLaw::J2) //J2linear
    {
        J2LinearDG3DMaterialLaw *DG3DJ2linearit = dynamic_cast<J2LinearDG3DMaterialLaw*>(DG3Dit);
        IPJ2linear *J2linearipv = dynamic_cast<IPJ2linear*>(IPeosCUR->_IPV);
        const IPJ2linear *J2linearipvp = dynamic_cast<const IPJ2linear*>(IPeosPREV->_IPV);
        //static STensor63 dCalgdeps;
        //STensor43& elasticL = ipvcur->getRefToElasticTangentModuli();

        /* compute stress */
        DG3DJ2linearit->_j2law.constitutive(F0,Fn,DG3DEOSipv->getRefToFirstPiolaKirchhoffStress(),J2linearipvp,J2linearipv, DG3DEOSipv->getRefToTangentModuli(),stiff,NULL);

        TotalelasticStiffness = DG3DJ2linearit->elasticStiffness;

        // Careful, make sure the volumetric properties of your deviatoric law are the same as in your EOS law, as the elastic stiffness is only calcualted through the deviatoric law
    }
    else if(mlaw_dev == materialLaw::viscoelastic)//viscoelastic
    {
        ViscoelasticDG3DMaterialLaw *DG3Dvisit = dynamic_cast<ViscoelasticDG3DMaterialLaw*>(DG3Dit);
        IPViscoelastic *Visipv = dynamic_cast<IPViscoelastic*>(IPeosCUR->_IPV);
        const IPViscoelastic *Visipvp = dynamic_cast<const IPViscoelastic*>(IPeosPREV->_IPV);
        //static STensor63 dCalgdeps;
        //STensor43& elasticL = ipvcur->getRefToElasticTangentModuli();

        DG3Dvisit->_Vislaw.constitutive(F0,Fn,DG3DEOSipv->getRefToFirstPiolaKirchhoffStress(),Visipvp,Visipv,DG3DEOSipv->getRefToTangentModuli(),stiff,NULL);
        TotalelasticStiffness = DG3Dvisit->elasticStiffness;

        // Careful, make sure the volumetric properties of your deviatoric law are the same as in your EOS law, as the elastic stiffness is only calcualted through the deviatoric law

    }

    _EOSlaw.constitutive(F0,Fn,DG3DEOSipv->getRefToFirstPiolaKirchhoffStress(),DG3DEOSipv->getFirstPiolaViscous(), DG3DEOSipv->getRefToFirstPiolaKirchhoffStressb4AV(), IPeosCUR,IPeosPREV,Fdot);

    DG3DEOSipv->setRefToDGElasticTangentModuli(TotalelasticStiffness);

}



VUMATinterfaceDG3DMaterialLaw::VUMATinterfaceDG3DMaterialLaw(const int num, const double rho, const char *propName) :
                                                              dG3DMaterialLaw(num,rho,true),
                                                             _vumatlaw(num,rho,propName)
{
  double nu = 0.3; // NOTE: This is to get an order of magnitude; another way of doing it would be to
                   //       change vumat_soundspeed to vumat_mu_nu which would give mu and nu
                   //       then reconstruct mlawVUMATinterface::soundSpeed to use this instead
                   //       and create mlawVUMATinterface::poissonRation/shearModulus using it too
  double soundSpeed = _vumatlaw.soundSpeed();
  double lambda_plus_two_mu = soundSpeed*soundSpeed*rho;
  double E = lambda_plus_two_mu * ( ((1+nu)*(1-2*nu)) / (1-nu) );
  fillElasticStiffness(E, nu, elasticStiffness);
}

VUMATinterfaceDG3DMaterialLaw::VUMATinterfaceDG3DMaterialLaw(const VUMATinterfaceDG3DMaterialLaw &source) :
                                                             dG3DMaterialLaw(source), _vumatlaw(source._vumatlaw)
{

}

void VUMATinterfaceDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  int nsdv = _vumatlaw.getNsdv();
  double size = 2.*(( const_cast<MElement*>(ele) )->getInnerRadius());
  IPVariable* ipvi = new  VUMATinterfaceDG3DIPVariable(nsdv,size,hasBodyForce,inter);
  IPVariable* ipv1 = new  VUMATinterfaceDG3DIPVariable(nsdv,size,hasBodyForce,inter);
  IPVariable* ipv2 = new  VUMATinterfaceDG3DIPVariable(nsdv,size,hasBodyForce,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void VUMATinterfaceDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  ipv = new  VUMATinterfaceDG3DIPVariable(_vumatlaw.getNsdv(),2.*(( const_cast<MElement*>(ele) )->getInnerRadius()),hasBodyForce,inter);
}

void VUMATinterfaceDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  /* get ipvariable */
  VUMATinterfaceDG3DIPVariable* ipvcur;
  const VUMATinterfaceDG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<VUMATinterfaceDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const VUMATinterfaceDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<VUMATinterfaceDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const VUMATinterfaceDG3DIPVariable*>(ipvp);
  }

  _vumatlaw.checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());
};

void VUMATinterfaceDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  /* get ipvariable */
  VUMATinterfaceDG3DIPVariable* ipvcur;
  const VUMATinterfaceDG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<VUMATinterfaceDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const VUMATinterfaceDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<VUMATinterfaceDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const VUMATinterfaceDG3DIPVariable*>(ipvp);
  }

  /* strain xyz */
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

  /* data for VUMAT law */
  IPVUMATinterface* q1 = ipvcur->getIPVUMATinterface();
  const IPVUMATinterface* q0 = ipvprev->getIPVUMATinterface();
  //static STensor63 dCalgdeps;
  //STensor43& elasticL = ipvcur->getRefToElasticTangentModuli();

  /* compute stress */
  ipvcur->getRefToFirstPiolaKirchhoffStress()=( const_cast<VUMATinterfaceDG3DIPVariable*>(ipvprev) )->getRefToFirstPiolaKirchhoffStress();
  _vumatlaw.constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),stiff,NULL,dTangent,ipvcur->getPtrTodCmdFm());

  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);
}

//

LocalDamageIsotropicElasticityDG3DMaterialLaw::LocalDamageIsotropicElasticityDG3DMaterialLaw(const int num,const double rho,
                                         double E,const double nu,  const DamageLaw &damLaw):
      dG3DMaterialLaw(num,rho,true){
  _nlLaw = new mlawLocalDamageIsotropicElasticity(num,rho,E,nu,damLaw);
	fillElasticStiffness(E, nu, elasticStiffness);
};
LocalDamageIsotropicElasticityDG3DMaterialLaw::LocalDamageIsotropicElasticityDG3DMaterialLaw(const LocalDamageIsotropicElasticityDG3DMaterialLaw& src):
dG3DMaterialLaw(src)
{
  _nlLaw = NULL;
  if (src._nlLaw != NULL){
    _nlLaw = dynamic_cast<mlawLocalDamageIsotropicElasticity*>(src._nlLaw->clone());
  }
};
void LocalDamageIsotropicElasticityDG3DMaterialLaw::setTime(const double t,const double dtime)
{
	dG3DMaterialLaw::setTime(t,dtime);
	_nlLaw->setTime(t,dtime);
}

void LocalDamageIsotropicElasticityDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
	bool inter=true;
	const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
	if(iele==NULL) inter=false;

	IPVariable* ipvi = new  localDamageIsotropicElasticityDG3DIPVariable(*_nlLaw,hasBodyForce,inter);
	IPVariable* ipv1 = new  localDamageIsotropicElasticityDG3DIPVariable(*_nlLaw,hasBodyForce,inter);
	IPVariable* ipv2 = new  localDamageIsotropicElasticityDG3DIPVariable(*_nlLaw,hasBodyForce,inter);
	if(ips != NULL) delete ips;
	ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void LocalDamageIsotropicElasticityDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
	bool inter=true;
	const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
	if(iele == NULL) inter=false;

	if(ipv !=NULL) delete ipv;
	ipv = new  localDamageIsotropicElasticityDG3DIPVariable(*_nlLaw,hasBodyForce,inter);
}

void LocalDamageIsotropicElasticityDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  /* get ipvariable */
  localDamageIsotropicElasticityDG3DIPVariable* ipvcur;
  const localDamageIsotropicElasticityDG3DIPVariable* ipvprev;

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {
      ipvcur = static_cast<localDamageIsotropicElasticityDG3DIPVariable*>(ipvtmp->getIPvBulk());
      const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
      ipvprev = static_cast<const localDamageIsotropicElasticityDG3DIPVariable*>(ipvtmp2->getIPvBulk());
  }
  else
  {
      ipvcur = static_cast<localDamageIsotropicElasticityDG3DIPVariable*>(ipv);
      ipvprev = static_cast<const localDamageIsotropicElasticityDG3DIPVariable*>(ipvp);
  }
  _nlLaw->checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());
};

void LocalDamageIsotropicElasticityDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
    /* get ipvariable */
    localDamageIsotropicElasticityDG3DIPVariable* ipvcur;
    const localDamageIsotropicElasticityDG3DIPVariable* ipvprev;

    FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
    if(ipvtmp !=NULL)
		{
				ipvcur = static_cast<localDamageIsotropicElasticityDG3DIPVariable*>(ipvtmp->getIPvBulk());
				const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
				ipvprev = static_cast<const localDamageIsotropicElasticityDG3DIPVariable*>(ipvtmp2->getIPvBulk());
		}
    else
		{
				ipvcur = static_cast<localDamageIsotropicElasticityDG3DIPVariable*>(ipv);
				ipvprev = static_cast<const localDamageIsotropicElasticityDG3DIPVariable*>(ipvp);
		}

    /* Deformation gradient F */
    const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
    const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

    /* data for non local isotropic elastic law */
    IPLocalDamageIsotropicElasticity* q1 = ipvcur->getIPLocalDamageIsotropicElasticity();
    const IPLocalDamageIsotropicElasticity* q0 = ipvprev->getIPLocalDamageIsotropicElasticity();
    //static STensor63 dCalgdeps;
    STensor43& elasticL = ipvcur->getRefToElasticTangentModuli();

    /* compute stress */
    _nlLaw->constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),stiff,&elasticL,dTangent,ipvcur->getPtrTodCmdFm());

    ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);   // Is it correct ????
}
//

LocalDamageHyperelasticDG3DMaterialLaw::LocalDamageHyperelasticDG3DMaterialLaw(const int num, const double rho, const elasticPotential& elP, const DamageLaw &damLaw):
      dG3DMaterialLaw(num,rho,true){
  _nlLaw = new mlawLocalDamageHyperelastic(num,rho,elP,damLaw);

  double E = elP.getYoungModulus();
	double nu = elP.getPoissonRatio();
	fillElasticStiffness(E, nu, elasticStiffness);
};
LocalDamageHyperelasticDG3DMaterialLaw::LocalDamageHyperelasticDG3DMaterialLaw(const LocalDamageHyperelasticDG3DMaterialLaw& src):
dG3DMaterialLaw(src)
{
  _nlLaw = NULL;
  if (src._nlLaw != NULL){
    _nlLaw = dynamic_cast<mlawLocalDamageHyperelastic*>(src._nlLaw->clone());
  }
};
void LocalDamageHyperelasticDG3DMaterialLaw::setTime(const double t,const double dtime)
{
	dG3DMaterialLaw::setTime(t,dtime);
	_nlLaw->setTime(t,dtime);
}

void LocalDamageHyperelasticDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
	bool inter=true;
	const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
	if(iele==NULL) inter=false;

	IPVariable* ipvi = new  localDamageIsotropicElasticityDG3DIPVariable(*_nlLaw,hasBodyForce,inter);
	IPVariable* ipv1 = new  localDamageIsotropicElasticityDG3DIPVariable(*_nlLaw,hasBodyForce,inter);
	IPVariable* ipv2 = new  localDamageIsotropicElasticityDG3DIPVariable(*_nlLaw,hasBodyForce,inter);
	if(ips != NULL) delete ips;
	ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void LocalDamageHyperelasticDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
	bool inter=true;
	const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
	if(iele == NULL) inter=false;

	if(ipv !=NULL) delete ipv;
	ipv = new  localDamageIsotropicElasticityDG3DIPVariable(*_nlLaw,hasBodyForce,inter);
}

void LocalDamageHyperelasticDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  /* get ipvariable */
  localDamageIsotropicElasticityDG3DIPVariable* ipvcur;
  const localDamageIsotropicElasticityDG3DIPVariable* ipvprev;

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {
      ipvcur = static_cast<localDamageIsotropicElasticityDG3DIPVariable*>(ipvtmp->getIPvBulk());
      const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
      ipvprev = static_cast<const localDamageIsotropicElasticityDG3DIPVariable*>(ipvtmp2->getIPvBulk());
  }
  else
  {
      ipvcur = static_cast<localDamageIsotropicElasticityDG3DIPVariable*>(ipv);
      ipvprev = static_cast<const localDamageIsotropicElasticityDG3DIPVariable*>(ipvp);
  }
  _nlLaw->checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());
};

void LocalDamageHyperelasticDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
    /* get ipvariable */
    localDamageIsotropicElasticityDG3DIPVariable* ipvcur;
    const localDamageIsotropicElasticityDG3DIPVariable* ipvprev;

    FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
    if(ipvtmp !=NULL)
		{
				ipvcur = static_cast<localDamageIsotropicElasticityDG3DIPVariable*>(ipvtmp->getIPvBulk());
				const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
				ipvprev = static_cast<const localDamageIsotropicElasticityDG3DIPVariable*>(ipvtmp2->getIPvBulk());
		}
    else
		{
				ipvcur = static_cast<localDamageIsotropicElasticityDG3DIPVariable*>(ipv);
				ipvprev = static_cast<const localDamageIsotropicElasticityDG3DIPVariable*>(ipvp);
		}

    /* Deformation gradient F */
    const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
    const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

    /* data for non local isotropic elastic law */
    IPLocalDamageIsotropicElasticity* q1 = ipvcur->getIPLocalDamageIsotropicElasticity();
    const IPLocalDamageIsotropicElasticity* q0 = ipvprev->getIPLocalDamageIsotropicElasticity();
    //static STensor63 dCalgdeps;
    STensor43& elasticL = ipvcur->getRefToElasticTangentModuli();


    _nlLaw->constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),stiff,&elasticL,dTangent,ipvcur->getPtrTodCmdFm());


    ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);   // Is it correct ????

}

// *********************
LocalAnisotropicDamageHyperelasticDG3DMaterialLaw::LocalAnisotropicDamageHyperelasticDG3DMaterialLaw(const int num, const double rho,
                                        const elasticPotential& elP,const DamageLaw &damLaw): LocalDamageHyperelasticDG3DMaterialLaw(num,rho,elP,damLaw){
  if (_nlLaw != NULL) delete _nlLaw;
  _nlLaw = new mlawLocalAnisotropicDamageHyperelastic(num,rho,elP,damLaw);
};

//
LocalDamageJ2HyperDG3DMaterialLaw::LocalDamageJ2HyperDG3DMaterialLaw(const int num,const double rho,
                   double E,const double nu,const J2IsotropicHardening &_j2IH,
                   const DamageLaw &_damLaw,const double tol, bool matrixByPerturbation, double pertTol) :
                                                              dG3DMaterialLaw(num,rho,true)
{
  _nldJ2Hyperlaw = new mlawLocalDamageJ2Hyper(num,E,nu,rho,_j2IH,_damLaw,tol,matrixByPerturbation,pertTol);
  fillElasticStiffness(E, nu, elasticStiffness);
}

LocalDamageJ2HyperDG3DMaterialLaw::~LocalDamageJ2HyperDG3DMaterialLaw() {
	if (_nldJ2Hyperlaw !=NULL){
		delete _nldJ2Hyperlaw;
		_nldJ2Hyperlaw = NULL;
	}
};

void LocalDamageJ2HyperDG3DMaterialLaw::setStrainOrder(const int i)
{
  _nldJ2Hyperlaw->setStrainOrder(i);
}

LocalDamageJ2HyperDG3DMaterialLaw::LocalDamageJ2HyperDG3DMaterialLaw(const LocalDamageJ2HyperDG3DMaterialLaw &source) :
                                                             dG3DMaterialLaw(source)
{
	_nldJ2Hyperlaw = NULL;
	if (source._nldJ2Hyperlaw !=NULL){
		_nldJ2Hyperlaw = new mlawLocalDamageJ2Hyper(*(source._nldJ2Hyperlaw));

	}
}

void
LocalDamageJ2HyperDG3DMaterialLaw::setTime(const double t,const double dtime){
  dG3DMaterialLaw::setTime(t,dtime);
  _nldJ2Hyperlaw->setTime(t,dtime);
}

materialLaw::matname
LocalDamageJ2HyperDG3DMaterialLaw::getType() const {return _nldJ2Hyperlaw->getType();}

void LocalDamageJ2HyperDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  IPVariable* ipvi = new  localDamageJ2HyperDG3DIPVariable(*_nldJ2Hyperlaw,hasBodyForce,inter);
  IPVariable* ipv1 = new  localDamageJ2HyperDG3DIPVariable(*_nldJ2Hyperlaw,hasBodyForce,inter);
  IPVariable* ipv2 = new  localDamageJ2HyperDG3DIPVariable(*_nldJ2Hyperlaw,hasBodyForce,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void LocalDamageJ2HyperDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  ipv = new  localDamageJ2HyperDG3DIPVariable(*_nldJ2Hyperlaw,hasBodyForce,inter);
}

double LocalDamageJ2HyperDG3DMaterialLaw::soundSpeed() const{return _nldJ2Hyperlaw->soundSpeed();} // or change value ??

void LocalDamageJ2HyperDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  /* get ipvariable */
  localDamageJ2HyperDG3DIPVariable* ipvcur;
  const localDamageJ2HyperDG3DIPVariable* ipvprev;

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<localDamageJ2HyperDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const localDamageJ2HyperDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<localDamageJ2HyperDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const localDamageJ2HyperDG3DIPVariable*>(ipvp);
  }

  _nldJ2Hyperlaw->checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());
};

void LocalDamageJ2HyperDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  /* get ipvariable */
  localDamageJ2HyperDG3DIPVariable* ipvcur;
  const localDamageJ2HyperDG3DIPVariable* ipvprev;

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<localDamageJ2HyperDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const localDamageJ2HyperDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<localDamageJ2HyperDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const localDamageJ2HyperDG3DIPVariable*>(ipvp);
  }

  /* strain xyz */
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

  /* data for J2 law */
  IPLocalDamageJ2Hyper* q1 = ipvcur->getIPLocalDamageJ2Hyper();
  const IPLocalDamageJ2Hyper* q0 = ipvprev->getIPLocalDamageJ2Hyper();
  //static STensor63 dCalgdeps;
  STensor43& elasticL = ipvcur->getRefToElasticTangentModuli();

  /* compute stress */
  _nldJ2Hyperlaw->constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),stiff,&elasticL,dTangent,ipvcur->getPtrTodCmdFm());

  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);

}
//
//
LocalDamageJ2SmallStrainDG3DMaterialLaw::LocalDamageJ2SmallStrainDG3DMaterialLaw(const int num,const double rho,
                   double E,const double nu,const J2IsotropicHardening &_j2IH,
                   const DamageLaw &_damLaw,const double tol, bool matrixByPerturbation, double pertTol) :
                                                              dG3DMaterialLaw(num,rho,true)
{
  _nldJ2 = new mlawLocalDamageJ2SmallStrain(num,E,nu,rho,_j2IH,_damLaw,tol,matrixByPerturbation,pertTol);
  fillElasticStiffness(E, nu, elasticStiffness);
}

LocalDamageJ2SmallStrainDG3DMaterialLaw::~LocalDamageJ2SmallStrainDG3DMaterialLaw() {
	if (_nldJ2 !=NULL){
		delete _nldJ2;
		_nldJ2 = NULL;
	}
};

LocalDamageJ2SmallStrainDG3DMaterialLaw::LocalDamageJ2SmallStrainDG3DMaterialLaw(const LocalDamageJ2SmallStrainDG3DMaterialLaw &source) :
                                                             dG3DMaterialLaw(source)
{
	_nldJ2 = NULL;
	if (source._nldJ2 !=NULL){
		_nldJ2 = new mlawLocalDamageJ2SmallStrain(*(source._nldJ2));

	}
}

void LocalDamageJ2SmallStrainDG3DMaterialLaw::setTime(const double t,const double dtime){
  dG3DMaterialLaw::setTime(t,dtime);
  _nldJ2->setTime(t,dtime);
}

materialLaw::matname LocalDamageJ2SmallStrainDG3DMaterialLaw::getType() const {return _nldJ2->getType();}

void LocalDamageJ2SmallStrainDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  IPVariable* ipvi = new  localDamageJ2HyperDG3DIPVariable(*_nldJ2,hasBodyForce,inter);
  IPVariable* ipv1 = new  localDamageJ2HyperDG3DIPVariable(*_nldJ2,hasBodyForce,inter);
  IPVariable* ipv2 = new  localDamageJ2HyperDG3DIPVariable(*_nldJ2,hasBodyForce,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void LocalDamageJ2SmallStrainDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  ipv = new  localDamageJ2HyperDG3DIPVariable(*_nldJ2,hasBodyForce,inter);
}

double LocalDamageJ2SmallStrainDG3DMaterialLaw::soundSpeed() const{return _nldJ2->soundSpeed();} // or change value ??

void LocalDamageJ2SmallStrainDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  /* get ipvariable */
  localDamageJ2HyperDG3DIPVariable* ipvcur;
  const localDamageJ2HyperDG3DIPVariable* ipvprev;

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<localDamageJ2HyperDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const localDamageJ2HyperDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<localDamageJ2HyperDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const localDamageJ2HyperDG3DIPVariable*>(ipvp);
  }

  _nldJ2->checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());
};

void LocalDamageJ2SmallStrainDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  /* get ipvariable */
  localDamageJ2HyperDG3DIPVariable* ipvcur;
  const localDamageJ2HyperDG3DIPVariable* ipvprev;

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<localDamageJ2HyperDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const localDamageJ2HyperDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<localDamageJ2HyperDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const localDamageJ2HyperDG3DIPVariable*>(ipvp);
  }

  /* strain xyz */
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

  /* data for J2 law */
  IPLocalDamageJ2Hyper* q1 = ipvcur->getIPLocalDamageJ2Hyper();
  const IPLocalDamageJ2Hyper* q0 = ipvprev->getIPLocalDamageJ2Hyper();
  //static STensor63 dCalgdeps;
  STensor43& elasticL = ipvcur->getRefToElasticTangentModuli();

  /* compute stress */
  _nldJ2->constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),stiff,&elasticL,dTangent,ipvcur->getPtrTodCmdFm());

  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);
}

//

TransverseIsotropicDG3DMaterialLaw::TransverseIsotropicDG3DMaterialLaw(
                                   const int num,const double rho,
                                   double E,const double nu,
                                   const double EA, const double GA, const double nu_minor,
                                   const double Ax, const double Ay, const double Az) :
                                                             dG3DMaterialLaw(num,rho,true),
                                                             _tilaw(num,E,nu,rho,EA,GA,nu_minor, Ax,Ay,Az), _E(E), _nu(nu), _nu_minor(nu_minor), _EA(EA), _GA(GA)

{

  STensor3 F1;
  STensorOperation::unity(F1);
  STensor3 F0;
  STensorOperation::unity(F0);
  STensor3 P;
  STensor43 T;
  
  /* data for J2 law */
  IPTransverseIsotropic q1;
  IPTransverseIsotropic q0;
  
  /* compute stress */
  _tilaw.constitutive(F0,F1,P,&q0,&q1,T,true,&elasticStiffness,false,NULL);

  //fillElasticStiffness(E, nu, elasticStiffness);
}

TransverseIsotropicDG3DMaterialLaw::TransverseIsotropicDG3DMaterialLaw(const TransverseIsotropicDG3DMaterialLaw &source) : dG3DMaterialLaw(source), _tilaw(source._tilaw),
                                                                     _E(source._E), _nu(source._nu), _EA(source._EA),
                                                                     _GA(source._GA), _nu_minor(source._nu_minor)
{

}

void TransverseIsotropicDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  IPVariable* ipvi = new  TransverseIsotropicDG3DIPVariable(hasBodyForce,inter);
  IPVariable* ipv1 = new  TransverseIsotropicDG3DIPVariable(hasBodyForce,inter);
  IPVariable* ipv2 = new  TransverseIsotropicDG3DIPVariable(hasBodyForce,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void TransverseIsotropicDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  ipv = new  TransverseIsotropicDG3DIPVariable(hasBodyForce,inter);
}

void TransverseIsotropicDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  /* get ipvariable */
  TransverseIsotropicDG3DIPVariable* ipvcur;
  const TransverseIsotropicDG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<TransverseIsotropicDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const TransverseIsotropicDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<TransverseIsotropicDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const TransverseIsotropicDG3DIPVariable*>(ipvp);
  }

  _tilaw.checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());
};

void TransverseIsotropicDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  /* get ipvariable */
  TransverseIsotropicDG3DIPVariable* ipvcur;
  const TransverseIsotropicDG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<TransverseIsotropicDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const TransverseIsotropicDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<TransverseIsotropicDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const TransverseIsotropicDG3DIPVariable*>(ipvp);
  }

  /* strain xyz */
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

  /* data for J2 law */
  IPTransverseIsotropic* q1 = ipvcur->getIPTransverseIsotropic();
  const IPTransverseIsotropic* q0 = ipvprev->getIPTransverseIsotropic();
  STensor43& elasticL = ipvcur->getRefToElasticTangentModuli();
  //static STensor63 dCalgdeps;

  /* compute stress */
  _tilaw.constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),stiff,&elasticL,dTangent,ipvcur->getPtrTodCmdFm());

  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);

}



TransverseIsoCurvatureDG3DMaterialLaw::TransverseIsoCurvatureDG3DMaterialLaw(
                                   const int num,const double rho,
                                   double E,const double nu,
                                   const double EA, const double GA, const double nu_minor,
                                   const double Centroid_x, const double Centroid_y, const double Centroid_z,
                                   const double PointStart1_x, const double PointStart1_y, const double PointStart1_z,
                                   const double PointStart2_x, const double PointStart2_y, const double PointStart2_z,
                                   const double init_Angle) :
                                                             dG3DMaterialLaw(num,rho,true),
                                                             _tilaw(num,E,nu,rho,EA,GA,nu_minor, Centroid_x, Centroid_y, Centroid_z, PointStart1_x, PointStart1_y, PointStart1_z,  PointStart2_x, PointStart2_y, PointStart2_z,init_Angle), _E(E), _nu(nu), _nu_minor(nu_minor), _EA(EA), _GA(GA)

{
  fillElasticStiffness(E, nu, elasticStiffness);
}

TransverseIsoCurvatureDG3DMaterialLaw::TransverseIsoCurvatureDG3DMaterialLaw(const TransverseIsoCurvatureDG3DMaterialLaw &source) : dG3DMaterialLaw(source), _tilaw(source._tilaw),
                                                                     _E(source._E), _nu(source._nu), _EA(source._EA),
                                                                     _GA(source._GA), _nu_minor(source._nu_minor)
{

}

void TransverseIsoCurvatureDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;

  SPoint3 pt_Global;
  ele->pnt(GP->pt[0],GP->pt[1],GP->pt[2],pt_Global);
  SVector3 GaussP(pt_Global);

  IPVariable* ipvi = new  TransverseIsoCurvatureDG3DIPVariable(GaussP, _tilaw.getCentroid(), _tilaw.getPointStart1(), _tilaw.getPointStart2(), _tilaw.getInit_Angle(),  hasBodyForce,inter);
  IPVariable* ipv1 = new  TransverseIsoCurvatureDG3DIPVariable(GaussP, _tilaw.getCentroid(), _tilaw.getPointStart1(), _tilaw.getPointStart2(), _tilaw.getInit_Angle(), hasBodyForce,inter);
  IPVariable* ipv2 = new  TransverseIsoCurvatureDG3DIPVariable(GaussP, _tilaw.getCentroid(), _tilaw.getPointStart1(), _tilaw.getPointStart2(), _tilaw.getInit_Angle(), hasBodyForce,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}



void TransverseIsoCurvatureDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{

  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }

  SPoint3 pt_Global;
  ele->pnt(GP->pt[0],GP->pt[1],GP->pt[2],pt_Global);
  SVector3 GaussP(pt_Global);

  ipv = new  TransverseIsoCurvatureDG3DIPVariable(GaussP, _tilaw.getCentroid(), _tilaw.getPointStart1(), _tilaw.getPointStart2(),_tilaw.getInit_Angle(), hasBodyForce, inter);
}

void TransverseIsoCurvatureDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  /* get ipvariable */
  TransverseIsoCurvatureDG3DIPVariable* ipvcur;
  const TransverseIsoCurvatureDG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<TransverseIsoCurvatureDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const TransverseIsoCurvatureDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<TransverseIsoCurvatureDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const TransverseIsoCurvatureDG3DIPVariable*>(ipvp);
  }

  _tilaw.checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());
};


void TransverseIsoCurvatureDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  /* get ipvariable */
  TransverseIsoCurvatureDG3DIPVariable* ipvcur;
  const TransverseIsoCurvatureDG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<TransverseIsoCurvatureDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const TransverseIsoCurvatureDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<TransverseIsoCurvatureDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const TransverseIsoCurvatureDG3DIPVariable*>(ipvp);
  }

  /* strain xyz */
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

  /* data for J2 law */
  IPTransverseIsoCurvature* q1 = ipvcur->getIPTransverseIsoCurvature();
  const IPTransverseIsoCurvature* q0 = ipvprev->getIPTransverseIsoCurvature();
  //static STensor63 dCalgdeps;
  STensor43& elasticL = ipvcur->getRefToElasticTangentModuli();

  /* compute stress */
  _tilaw.constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),stiff,&elasticL,dTangent,ipvcur->getPtrTodCmdFm());

  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);

}
//
TransverseIsoUserDirectionDG3DMaterialLaw::TransverseIsoUserDirectionDG3DMaterialLaw(
                                   const int num,const double rho,
                                   double E,const double nu,
                                   const double EA, const double GA, const double nu_minor,
                                   const UserDirectionGenerator& dirGen) :
dG3DMaterialLaw(num,rho,true),
_tilaw(num,E,nu,rho,EA,GA,nu_minor,dirGen), _E(E), _nu(nu), _nu_minor(nu_minor), _EA(EA), _GA(GA)

{
  fillElasticStiffness(E, nu, elasticStiffness);
}

TransverseIsoUserDirectionDG3DMaterialLaw::TransverseIsoUserDirectionDG3DMaterialLaw(const TransverseIsoUserDirectionDG3DMaterialLaw &source) :
      dG3DMaterialLaw(source), _tilaw(source._tilaw),
    _E(source._E), _nu(source._nu), _EA(source._EA),
    _GA(source._GA), _nu_minor(source._nu_minor)
{

}

void TransverseIsoUserDirectionDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;

  SPoint3 pt_Global;
  ele->pnt(GP->pt[0],GP->pt[1],GP->pt[2],pt_Global);
  SVector3 GaussP(pt_Global);
  SVector3 Dir;
  _tilaw.getUserDirectionGenerator().getTransverseDirection(Dir,GaussP);
  Dir.normalize();
  IPVariable* ipvi = new  TransverseIsoUserDirectionDG3DIPVariable(Dir,hasBodyForce, inter);
  IPVariable* ipv1 = new  TransverseIsoUserDirectionDG3DIPVariable(Dir, hasBodyForce, inter);
  IPVariable* ipv2 = new  TransverseIsoUserDirectionDG3DIPVariable(Dir, hasBodyForce, inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}



void TransverseIsoUserDirectionDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{

  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  SPoint3 pt_Global;
  ele->pnt(GP->pt[0],GP->pt[1],GP->pt[2],pt_Global);
  SVector3 GaussP(pt_Global);
  SVector3 Dir;
  _tilaw.getUserDirectionGenerator().getTransverseDirection(Dir,GaussP);
  Dir.normalize();
  ipv = new TransverseIsoUserDirectionDG3DIPVariable(Dir,hasBodyForce, inter);
}

void TransverseIsoUserDirectionDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  /* get ipvariable */
  TransverseIsoUserDirectionDG3DIPVariable* ipvcur;
  const TransverseIsoUserDirectionDG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<TransverseIsoUserDirectionDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const TransverseIsoUserDirectionDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<TransverseIsoUserDirectionDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const TransverseIsoUserDirectionDG3DIPVariable*>(ipvp);
  }

  _tilaw.checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());
};


void TransverseIsoUserDirectionDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  /* get ipvariable */
  TransverseIsoUserDirectionDG3DIPVariable* ipvcur;
  const TransverseIsoUserDirectionDG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<TransverseIsoUserDirectionDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const TransverseIsoUserDirectionDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<TransverseIsoUserDirectionDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const TransverseIsoUserDirectionDG3DIPVariable*>(ipvp);
  }

  /* strain xyz */
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

  /* data for J2 law */
  IPTransverseIsoUserDirection* q1 = ipvcur->getIPTransverseIsoUserDirection();
  const IPTransverseIsoUserDirection* q0 = ipvprev->getIPTransverseIsoUserDirection();
  //static STensor63 dCalgdeps;
  STensor43& elasticL = ipvcur->getRefToElasticTangentModuli();

  /* compute stress */
  _tilaw.constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),stiff,&elasticL,dTangent,ipvcur->getPtrTodCmdFm());

  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);

};

//Constructor for yarn
TransverseIsoYarnBDG3DMaterialLaw::TransverseIsoYarnBDG3DMaterialLaw(int lawnum,//tag of the material law
                                                                     double rho, //material density
                                                                     int physicnum,//physic number of
                                                                     char* yarnInfo, //Yarn information file
                                                                     int isunidir,  //
                                                                     double rDSvDT,//To initialize '_CriticalrDSvDT'
                                                                     double MaxStrainRate, //The maximal value of the strain rate
			                                             double Cxx11,
		                                                     double Cxx21,
			                                             double Cxx22,
			                                             double Cxx31,
			                                             double Cxx32,
			                                             double Cyy11,
			                                             double Cyy21,
			                                             double Cyy22,
			                                             double Cyy31,
			                                             double Cyy32,
			                                             double Cxy11,
			                                             double Cxy21,
			                                             double Cxy22,
			                                             double Cxy31,
			                                             double Cxy32,
			                                             double Cyz11,
			                                             double Cyz21,
			                                             double Cyz22,
			                                             double Cyz31,
			                                             double Cyz32,
                                                                     double nuxy,
                                                                     double nuxz,
                                                                     double nuyz) :
                                                                    dG3DMaterialLaw(lawnum,rho,true),
                                                                   _tilaw(lawnum,rho, physicnum,yarnInfo,
                                                                          isunidir, rDSvDT, MaxStrainRate,
                                                                          Cxx11, Cxx21, Cxx22, Cxx31, Cxx32,
			                                                  Cyy11, Cyy21, Cyy22, Cyy31, Cyy32,
			                                                  Cxy11, Cxy21, Cxy22, Cxy31, Cxy32,
			                                                  Cyz11, Cyz21, Cyz22, Cyz31, Cyz32,
                                                                          nuxy,  nuxz,  nuyz)
{
  double EArray[4] = {Cxx11,Cyy11,Cxy11,Cyz11};
  int MaxEArrayIndex = maxArray_pos(EArray);
  double E = EArray[MaxEArrayIndex];

  double nu = 0.4;

  fillElasticStiffness(E,nu,elasticStiffness);
  cout<<"Constitutive law of physic volume "<< physicnum <<" has been built!" <<endl<<endl;
}
TransverseIsoYarnBDG3DMaterialLaw::TransverseIsoYarnBDG3DMaterialLaw(const TransverseIsoYarnBDG3DMaterialLaw &source) :
						dG3DMaterialLaw(source), _tilaw(source._tilaw)
{

}


//Constructor for matrix
TransverseIsoYarnBDG3DMaterialLaw::TransverseIsoYarnBDG3DMaterialLaw(int lawnum,//tag of the material law
                                                                     double rho, //material density
                                                                     int physicnum,//physic number of
                                                                     double rDSvDT,//To initialize '_CriticalrDSvDT'
                                                                     double MaxStrainRate, //The maximal value of the strain rate
			                                             double Cxx11,
		                                                     double Cxx21,
			                                             double Cxx22,
			                                             double Cxx31,
			                                             double Cxx32,
			                                             double Cyy11,
			                                             double Cyy21,
			                                             double Cyy22,
			                                             double Cyy31,
			                                             double Cyy32,
			                                             double Cxy11,
			                                             double Cxy21,
			                                             double Cxy22,
			                                             double Cxy31,
			                                             double Cxy32,
			                                             double Cyz11,
			                                             double Cyz21,
			                                             double Cyz22,
			                                             double Cyz31,
			                                             double Cyz32,
                                                                     double nuxy,
                                                                     double nuxz,
                                                                     double nuyz) :
                                                                    dG3DMaterialLaw(lawnum,rho,true),
                                                                   _tilaw(lawnum,rho, physicnum,
                                                                          rDSvDT, MaxStrainRate,
                                                                          Cxx11, Cxx21, Cxx22, Cxx31, Cxx32,
			                                                  Cyy11, Cyy21, Cyy22, Cyy31, Cyy32,
			                                                  Cxy11, Cxy21, Cxy22, Cxy31, Cxy32,
			                                                  Cyz11, Cyz21, Cyz22, Cyz31, Cyz32,
                                                                          nuxy,  nuxz,  nuyz)
{
  double EArray[4] = {Cxx11,Cyy11,Cxy11,Cyz11};
  int MaxEArrayIndex = maxArray_pos(EArray);
  double E = EArray[MaxEArrayIndex];

  double nu = 0.4;

  fillElasticStiffness(E,nu,elasticStiffness);
  cout<<"Constitutive law of physic volume "<< physicnum <<" has been built!" <<endl<<endl;
}


void TransverseIsoYarnBDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce,
                                                      const bool* state_,
						      const MElement *ele,
						      const int nbFF,
  						      const IntPt *GP,
						      const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;

  SPoint3 pt_Global;
  ele->pnt(GP->pt[0],GP->pt[1],GP->pt[2],pt_Global);
  SVector3 GaussP(pt_Global);

  IPVariable* ipvi;
  IPVariable* ipv1;
  IPVariable* ipv2;


  int isUniDir = _tilaw.get_IsUniDir();
  switch(isUniDir)
  {
   case 0:
        ipvi = new  TransverseIsoYarnBDG3DIPVariable(GaussP, _tilaw.get_Yarn(), hasBodyForce,inter);
        ipv1 = new  TransverseIsoYarnBDG3DIPVariable(GaussP, _tilaw.get_Yarn(), hasBodyForce,inter);
        ipv2 = new  TransverseIsoYarnBDG3DIPVariable(GaussP, _tilaw.get_Yarn(), hasBodyForce,inter);
        break;
   case 1:
        ipvi = new  TransverseIsoYarnBDG3DIPVariable(GaussP, _tilaw.get_UniDir(), hasBodyForce,inter);
        ipv1 = new  TransverseIsoYarnBDG3DIPVariable(GaussP, _tilaw.get_UniDir(), hasBodyForce,inter);
        ipv2 = new  TransverseIsoYarnBDG3DIPVariable(GaussP, _tilaw.get_UniDir(), hasBodyForce,inter);
        break;
   case 2:
        ipvi = new  TransverseIsoYarnBDG3DIPVariable(GaussP, hasBodyForce,inter);
        ipv1 = new  TransverseIsoYarnBDG3DIPVariable(GaussP, hasBodyForce,inter);
        ipv2 = new  TransverseIsoYarnBDG3DIPVariable(GaussP, hasBodyForce,inter);
        break;
  }

  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}


void TransverseIsoYarnBDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce,
                                                         const MElement *ele,
							 const int nbFF,
							 const IntPt *GP,
							 const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }

  SPoint3 pt_Global;
  ele->pnt(GP->pt[0],GP->pt[1],GP->pt[2],pt_Global);
  SVector3 GaussP(pt_Global);

  int isUniDir = _tilaw.get_IsUniDir();

  switch(isUniDir)
  {
   case 0:
        ipv = new  TransverseIsoYarnBDG3DIPVariable(GaussP, _tilaw.get_Yarn(), hasBodyForce,inter);
        break;
   case 1:
        ipv = new  TransverseIsoYarnBDG3DIPVariable(GaussP, _tilaw.get_UniDir(), hasBodyForce,inter);
        break;
   case 2:
        ipv = new  TransverseIsoYarnBDG3DIPVariable(GaussP, hasBodyForce,inter);
        break;
  }
}

void TransverseIsoYarnBDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  //get ipvariable
  TransverseIsoYarnBDG3DIPVariable* ipvcur;
  const TransverseIsoYarnBDG3DIPVariable* ipvprev;

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {
    ipvcur = static_cast<TransverseIsoYarnBDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const TransverseIsoYarnBDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<TransverseIsoYarnBDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const TransverseIsoYarnBDG3DIPVariable*>(ipvp);
  }

  _tilaw.checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());
};

void TransverseIsoYarnBDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  //get ipvariable
  TransverseIsoYarnBDG3DIPVariable* ipvcur;
  const TransverseIsoYarnBDG3DIPVariable* ipvprev;

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {
    ipvcur = static_cast<TransverseIsoYarnBDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const TransverseIsoYarnBDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<TransverseIsoYarnBDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const TransverseIsoYarnBDG3DIPVariable*>(ipvp);
  }


  //strain xyz
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();


  //data for J2 law
  IPTransverseIsoYarnB* q1 = ipvcur->getIPTransverseIsoYarnB();
  const IPTransverseIsoYarnB* q0 = ipvprev->getIPTransverseIsoYarnB();
  //static STensor63 dCalgdeps;
  STensor43& elasticL = ipvcur->getRefToElasticTangentModuli();


  // compute stress
  _tilaw.constitutive(F0, F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,
                      ipvcur->getRefToTangentModuli(), stiff,&elasticL,dTangent,ipvcur->getPtrTodCmdFm());

  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);
}

AnisotropicDG3DMaterialLaw::AnisotropicDG3DMaterialLaw(
                                   const int num,const double rho,
				   const double Ex,const double Ey,const double Ez,
				   const double Vxy,const double Vxz,const double Vyz,
				   const double MUxy,const double MUxz,const double MUyz,
                                   const double euler[3]) :
                                     	dG3DMaterialLaw(num,rho,true),
                                     	_tilaw(num,rho,Ex,Ey,Ez,Vxy,Vxz,Vyz,
                                            MUxy,MUxz,MUyz,euler[0],euler[1],euler[2]),
				     	_ISTensor3(1.)
{
  double nu = _tilaw.poissonRatio();
  double mu = _tilaw.shearModulus();
  double E = 2.*mu*(1.+nu); //Where does this come from? To Have the E corresponding to the Vmax, MUmax
		            //In the linear & homogeneous & isotropic case ?
  fillElasticStiffness(E, nu, elasticStiffness); // WTF here? not sure to get it ...
}
AnisotropicDG3DMaterialLaw::AnisotropicDG3DMaterialLaw(
                                   const int num,const double rho,
				   const double Ex,const double Ey,const double Ez,
				   const double Vxy,const double Vxz,const double Vyz,
				   const double MUxy,const double MUxz,const double MUyz,
                                   const double alpha, const double beta, const double gamma) :
                                     	dG3DMaterialLaw(num,rho,true),
                                     	_tilaw(num,rho,Ex,Ey,Ez,Vxy,Vxz,Vyz,
                                            MUxy,MUxz,MUyz,alpha, beta, gamma),
				     	_ISTensor3(1.)
{
  double nu = _tilaw.poissonRatio();
  double mu = _tilaw.shearModulus();
  double E = 2.*mu*(1.+nu); //Where does this come from? To Have the E corresponding to the Vmax, MUmax
		            //In the linear & homogeneous & isotropic case ?
  fillElasticStiffness(Ex,Ey,Ez,Vxy,Vxz,Vyz,MUxy,MUxz,MUyz,alpha,beta,gamma,elasticStiffness);
}
AnisotropicDG3DMaterialLaw::AnisotropicDG3DMaterialLaw(const AnisotropicDG3DMaterialLaw &source) :
								     dG3DMaterialLaw(source),
								     _tilaw(source._tilaw),
                                                                     _ISTensor3(source._ISTensor3)
{

}

void AnisotropicDG3DMaterialLaw::initLaws(const std::map<int,materialLaw*> &maplaw){
	if (!_initialized){
		elasticStiffness = _tilaw.getElasticityTensor();
		_initialized = true;
	};
};

void AnisotropicDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  IPVariable* ipvi = new  AnisotropicDG3DIPVariable(hasBodyForce,inter);
  IPVariable* ipv1 = new  AnisotropicDG3DIPVariable(hasBodyForce,inter);
  IPVariable* ipv2 = new  AnisotropicDG3DIPVariable(hasBodyForce,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}


void AnisotropicDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  ipv = new  AnisotropicDG3DIPVariable(hasBodyForce,inter);
}

void AnisotropicDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  /* get ipvariable */
  AnisotropicDG3DIPVariable* ipvcur;
  const AnisotropicDG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<AnisotropicDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const AnisotropicDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<AnisotropicDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const AnisotropicDG3DIPVariable*>(ipvp);
  }

  _tilaw.checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());
};


void AnisotropicDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  /* get ipvariable */
  AnisotropicDG3DIPVariable* ipvcur;
  const AnisotropicDG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<AnisotropicDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const AnisotropicDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<AnisotropicDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const AnisotropicDG3DIPVariable*>(ipvp);
  }

  /* strain xyz */
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

  /* data for J2 law */
  IPAnisotropic* q1 = ipvcur->getIPAnisotropic();
  const IPAnisotropic* q0 = ipvprev->getIPAnisotropic();
  //static STensor63 dCalgdeps;
  STensor43& elasticL = ipvcur->getRefToElasticTangentModuli();

  /* compute stress */
  _tilaw.constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),stiff,&elasticL,dTangent,ipvcur->getPtrTodCmdFm());

  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);

}


AnisotropicStochDG3DMaterialLaw::AnisotropicStochDG3DMaterialLaw(const int num, const double rho, const double alpha,
                                  const double beta, const double gamma, const double OrigX, const double OrigY,
                                  const char *propName, const int intpl):
                                  dG3DMaterialLaw(num,rho,true), _tilaw(num, rho, alpha, beta, gamma, OrigX, OrigY, propName, intpl)
{
  double nu = _tilaw.poissonRatio();
  double mu = _tilaw.shearModulus();
  double E = 2.*mu*(1.+nu);

  fillElasticStiffness(E, nu, elasticStiffness);
}

AnisotropicStochDG3DMaterialLaw::AnisotropicStochDG3DMaterialLaw(const AnisotropicStochDG3DMaterialLaw &source) :
								     dG3DMaterialLaw(source), _tilaw(source._tilaw){}

void AnisotropicStochDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;

  SPoint3 pt_Global;
  ele->pnt(GP->pt[0],GP->pt[1],GP->pt[2],pt_Global);
  SVector3 GaussP(pt_Global);

  IPVariable* ipvi = new AnisotropicStochDG3DIPVariable(GaussP, _tilaw.getExMat(), _tilaw.getEyMat(), _tilaw.getEzMat(),
                         _tilaw.getVxyMat(), _tilaw.getVxzMat(), _tilaw.getVyzMat(), _tilaw.getMUxyMat(), _tilaw.getMUxzMat(),
                         _tilaw.getMUyzMat(), _tilaw.getDeltaX(), _tilaw.getDeltaY(), _tilaw.getOrigX(), _tilaw.getOrigY(), _tilaw.getInterpoly(),hasBodyForce,inter);

  IPVariable* ipv1 = new AnisotropicStochDG3DIPVariable(GaussP, _tilaw.getExMat(), _tilaw.getEyMat(), _tilaw.getEzMat(),
                         _tilaw.getVxyMat(), _tilaw.getVxzMat(), _tilaw.getVyzMat(), _tilaw.getMUxyMat(), _tilaw.getMUxzMat(),
                         _tilaw.getMUyzMat(), _tilaw.getDeltaX(), _tilaw.getDeltaY(), _tilaw.getOrigX(), _tilaw.getOrigY(), _tilaw.getInterpoly(),hasBodyForce,inter);

  IPVariable* ipv2 = new AnisotropicStochDG3DIPVariable(GaussP, _tilaw.getExMat(), _tilaw.getEyMat(), _tilaw.getEzMat(),
                         _tilaw.getVxyMat(), _tilaw.getVxzMat(), _tilaw.getVyzMat(), _tilaw.getMUxyMat(), _tilaw.getMUxzMat(),
                         _tilaw.getMUyzMat(), _tilaw.getDeltaX(), _tilaw.getDeltaY(), _tilaw.getOrigX(), _tilaw.getOrigY(), _tilaw.getInterpoly(),hasBodyForce,inter);

  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void AnisotropicStochDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){ inter=false; }

  SPoint3 pt_Global;
  ele->pnt(GP->pt[0],GP->pt[1],GP->pt[2],pt_Global);
  SVector3 GaussP(pt_Global);

  ipv = new AnisotropicStochDG3DIPVariable(GaussP, _tilaw.getExMat(), _tilaw.getEyMat(), _tilaw.getEzMat(),
                         _tilaw.getVxyMat(), _tilaw.getVxzMat(), _tilaw.getVyzMat(), _tilaw.getMUxyMat(), _tilaw.getMUxzMat(),
                         _tilaw.getMUyzMat(), _tilaw.getDeltaX(), _tilaw.getDeltaY(), _tilaw.getOrigX(), _tilaw.getOrigY(), _tilaw.getInterpoly(),hasBodyForce,inter);
}

void AnisotropicStochDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  /* get ipvariable */
  AnisotropicStochDG3DIPVariable* ipvcur;
  const AnisotropicStochDG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<AnisotropicStochDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const AnisotropicStochDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<AnisotropicStochDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const AnisotropicStochDG3DIPVariable*>(ipvp);
  }

  _tilaw.checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());

};


void AnisotropicStochDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  /* get ipvariable */
  AnisotropicStochDG3DIPVariable* ipvcur;
  const AnisotropicStochDG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<AnisotropicStochDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const AnisotropicStochDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<AnisotropicStochDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const AnisotropicStochDG3DIPVariable*>(ipvp);
  }

  /* strain xyz */
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

  /* data for J2 law */
  IPAnisotropicStoch* q1 = ipvcur->getIPAnisotropicStoch();
  const IPAnisotropicStoch* q0 = ipvprev->getIPAnisotropicStoch();
  //static STensor63 dCalgdeps;
  STensor43& elasticL = ipvcur->getRefToElasticTangentModuli();

  /* compute stress */
  _tilaw.constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),stiff,&elasticL,dTangent,ipvcur->getPtrTodCmdFm());

  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);

}

//

ClusterDG3DMaterialLaw::ClusterDG3DMaterialLaw(const int num, Clustering* cl, bool homo):
                                  dG3DMaterialLaw(num,0.,false), _clusteringData(cl),_homogeneous(homo)
{
  double nu = 0.; //_clusterlaw.poissonRatio();
  double mu = 0.;//_clusterlaw.shearModulus();
  double E = 2.*mu*(1.+nu);

  fillElasticStiffness(E, nu, elasticStiffness);
}

ClusterDG3DMaterialLaw::ClusterDG3DMaterialLaw(const ClusterDG3DMaterialLaw &source) :
								     dG3DMaterialLaw(source), _clusteringData(source._clusteringData), _homogeneous(source._homogeneous),
                     _matlawptr(source._matlawptr)
{
  _initialized = false; // to make sure the material will reiniatilzed
}

void ClusterDG3DMaterialLaw::setTime(const double t,const double dtime){
  dG3DMaterialLaw::setTime(t,dtime);
  for (std::map<int,dG3DMaterialLaw *>::const_iterator it = _matlawptr.begin(); it != _matlawptr.end(); it++)
  {
    it->second->setTime(t,dtime);
  }
};

double ClusterDG3DMaterialLaw::soundSpeed() const
{
  double soundSpeed = 0.;
  for (std::map<int,dG3DMaterialLaw *>::const_iterator it = _matlawptr.begin(); it != _matlawptr.end(); it++)
  {
    double curSoundSpeed = it->second->soundSpeed();
    if (curSoundSpeed > soundSpeed)
    {
      soundSpeed = curSoundSpeed;
    }
  }
  return soundSpeed;
};

double ClusterDG3DMaterialLaw::scaleFactor() const
{
  double factor = 1.;
  for (std::map<int,dG3DMaterialLaw *>::const_iterator it = _matlawptr.begin(); it != _matlawptr.end(); it++)
  {
    double curF = it->second->scaleFactor();
    if (curF > factor)
    {
      factor = curF;
    }
  }
  return factor;
};

bool ClusterDG3DMaterialLaw::withEnergyDissipation() const
{
  for (std::map<int,dG3DMaterialLaw *>::const_iterator it = _matlawptr.begin(); it != _matlawptr.end(); it++)
  {
    if (it->second->withEnergyDissipation())
    {
      return true;
    }
  }
  return false;
}
void ClusterDG3DMaterialLaw::setMacroSolver(const nonLinearMechSolver* sv){
  dG3DMaterialLaw::setMacroSolver(sv);
  for (std::map<int,dG3DMaterialLaw *>::const_iterator it = _matlawptr.begin(); it != _matlawptr.end(); it++)
  {
    it->second->setMacroSolver(sv);
  }
};

void ClusterDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ips != NULL) delete ips;
  ips = NULL;
    //find the cluster nb
  int eleNb=ele->getNum();
  int clusterIndex = _clusteringData->getClusterIndex(eleNb,gpt);
  std::map<int,dG3DMaterialLaw *>::const_iterator itmlaw=_matlawptr.find(clusterIndex);
  itmlaw->second->createIPState(ips,hasBodyForce,state_,ele,nbFF_,GP,gpt);
  std::vector<IPVariable*> allIPs;
  ips->getAllIPVariable(allIPs);
  int tag = numericalMaterialBase::createTypeWithTwoInts(eleNb,gpt);
  for (int i=0; i< allIPs.size(); i++)
  {
    dG3DIPVariableBase* ipdg = dynamic_cast<dG3DIPVariableBase*>(allIPs[i]);
    ipdg->setTag(tag);
  }
};

void ClusterDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  ipv = NULL;
    //find the cluster nb
  int eleNb=ele->getNum();
  int clusterIndex = _clusteringData->getClusterIndex(eleNb,gpt);
  std::map<int, dG3DMaterialLaw *>::const_iterator itmlaw=_matlawptr.find(clusterIndex);
  itmlaw->second->createIPVariable(ipv, hasBodyForce, ele,nbFF_,GP,gpt);
  dG3DIPVariableBase* ipdg = dynamic_cast<dG3DIPVariableBase*>(ipv);
  int tag = numericalMaterialBase::createTypeWithTwoInts(eleNb,gpt);
  ipdg->setTag(tag);
};

void ClusterDG3DMaterialLaw::initLaws(const std::map<int,materialLaw*> &maplaw)
{
  if (!_initialized)
  {
    _matlawptr.clear();
    const std::map<int,int>& materialMap =  _clusteringData->getClusterMaterialMap();
    for (std::map<int,int>::const_iterator itcluster =materialMap.begin(); itcluster!= materialMap.end(); itcluster++)
    {
      int clusternb=itcluster->first;
      int matlawnb=itcluster->second;

      bool findlaw=false;
      for(std::map<int,materialLaw*>::const_iterator it = maplaw.begin(); it != maplaw.end(); ++it)
      {
        int num = it->first;
        if(num == matlawnb){
          findlaw=true;
          it->second->initLaws(maplaw);
          _matlawptr.insert(std::pair<int, dG3DMaterialLaw*>(clusternb,dynamic_cast<dG3DMaterialLaw*>(it->second)));
          break;
        }
      }
      if(!findlaw) Msg::Error("The law is not initialize for cluster number %d and material law number %d",clusternb,matlawnb);
    }
    std::map<int,dG3DMaterialLaw*>::const_iterator itMat =_matlawptr.begin();
    elasticStiffness = itMat->second->elasticStiffness;
    elasticStiffness.print("elasticStiffness ClusterDG3DMaterialLaw");
    _initialized = true;
  }
}


void ClusterDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  /* get ipvariable */
  dG3DIPVariableBase* ipvcur;
  const dG3DIPVariableBase* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<dG3DIPVariableBase*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const dG3DIPVariableBase*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<dG3DIPVariableBase*>(ipv);
    ipvprev = static_cast<const dG3DIPVariableBase*>(ipvp);
  }

  int tag = ipvcur->getTag();
  int eleNum,gpt;
  numericalMaterialBase::getTwoIntsFromType(tag,eleNum,gpt);
  int clusterIndex = _clusteringData->getClusterIndex(eleNum,gpt);

  std::map<int,dG3DMaterialLaw *>::const_iterator itmlaw=_matlawptr.find(clusterIndex);
  if (itmlaw == _matlawptr.end())
  {
    Msg::Error("material law for cluster %d is not found",clusterIndex);
  }

  itmlaw->second->checkInternalState(ipv,ipvp);
};


void ClusterDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  /* get ipvariable */
  dG3DIPVariableBase* ipvcur;
  const dG3DIPVariableBase* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<dG3DIPVariableBase*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const dG3DIPVariableBase*>(ipvtmp2->getIPvBulk());
  }
  else
  {
    ipvcur = static_cast<dG3DIPVariableBase*>(ipv);
    ipvprev = static_cast<const dG3DIPVariableBase*>(ipvp);
  }

  int tag = ipvcur->getTag();
  int eleNum,gpt;
  numericalMaterialBase::getTwoIntsFromType(tag,eleNum,gpt);
  int clusterIndex = _clusteringData->getClusterIndex(eleNum,gpt);

  std::map<int, dG3DMaterialLaw *>::const_iterator itmlaw=_matlawptr.find(clusterIndex);
  if (itmlaw == _matlawptr.end())
  {
    Msg::Error("material law for cluster %d is not found",clusterIndex);
    Msg::Exit(0);
  }

  // modify current deformation gradient with eigen strain
  _eigenMethod = _clusteringData->getEigenMethod();
  STensor3 Feig(1.);
  if(_eigenMethod==0)
  {
    Feig = _clusteringData->getEigenStrain(tag,_homogeneous);
  }
  else if(_eigenMethod==1)
  {
    STensor43 Cel,Sel;
    Cel = this->elasticStiffness;
    STensorOperation::inverseSTensor43(Cel,Sel);
    const STensor3& sigEig = _clusteringData->getEigenStress(tag,_homogeneous);
    STensorOperation::multSTensor43STensor3Add(Sel,sigEig,-1.,Feig);
  }
  else
  {
    Msg::Error("this not implemented");
  }

  static STensor3 Fn;
  Fn = ipvcur->getConstRefToDeformationGradient();
  STensorOperation::multSTensor3(Fn,Feig,ipvcur->getRefToDeformationGradient());
  itmlaw->second->stress(ipv,ipvp,stiff,checkfrac,dTangent);
  ipvcur->getRefToDeformationGradient() = Fn;
  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);
  if (stiff)
  {
    static STensor43 Ttemp;
    Ttemp = ipvcur->getConstRefToTangentModuli();
    STensor43& Tangent = ipvcur->getRefToTangentModuli();
    STensorOperation::zero(Tangent);
    for (int i=0; i<3; i++)
    {
      for (int j=0; j<3; j++)
      {
        for (int k=0; k<3; k++)
        {
          for (int l=0; l<3; l++)
          {
            for (int m=0; m<3; m++)
            {
              Tangent(i,j,k,l)  += Ttemp(i,j,k,m)*Feig(l,m);
            }
          }
        }
      }
    }
  }
}
//
/*TFADG3DMaterialLaw::TFADG3DMaterialLaw(const int num, double rho):
                                  dG3DMaterialLaw(num,rho,true), _TFAlaw(num, rho)
{
  double nu = _TFAlaw.poissonRatio();
  double mu = _TFAlaw.shearModulus();
  double E = 2.*mu*(1.+nu);

  fillElasticStiffness(E, nu, elasticStiffness);
}

TFADG3DMaterialLaw::TFADG3DMaterialLaw(const TFADG3DMaterialLaw &source) :
								     dG3DMaterialLaw(source), _TFAlaw(source._TFAlaw){}

void TFADG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;

  IPTFA* ipvicluster=NULL;
  _TFAlaw.createIPVariable(ipvicluster, hasBodyForce, ele,nbFF_, GP, gpt);
  IPTFA* ipv1cluster=NULL;
  _TFAlaw.createIPVariable(ipv1cluster, hasBodyForce, ele,nbFF_, GP, gpt);
  IPTFA* ipv2cluster=NULL;
  _TFAlaw.createIPVariable(ipv2cluster, hasBodyForce, ele,nbFF_, GP, gpt);

  IPVariable* ipvi = new  TFADG3DIPVariable(ipvicluster,hasBodyForce,inter);
  IPVariable* ipv1 = new  TFADG3DIPVariable(ipv1cluster,hasBodyForce,inter);
  IPVariable* ipv2 = new  TFADG3DIPVariable(ipv2cluster,hasBodyForce,inter);

  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void TFADG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){ inter=false; }

  IPTFA* ipvicluster =NULL;
  _TFAlaw.createIPVariable(ipvicluster, hasBodyForce, ele,nbFF_, GP, gpt);
  ipv = new  TFADG3DIPVariable(ipvicluster,hasBodyForce,inter);

}

void TFADG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  // get ipvariable
  TFADG3DIPVariable* ipvcur;
  const TFADG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<TFADG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const TFADG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<TFADG3DIPVariable*>(ipv);
    ipvprev = static_cast<const TFADG3DIPVariable*>(ipvp);
  }

  _TFAlaw.checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());

};


void TFADG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  // get ipvariable
  TFADG3DIPVariable* ipvcur;
  const TFADG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<TFADG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const TFADG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<TFADG3DIPVariable*>(ipv);
    ipvprev = static_cast<const TFADG3DIPVariable*>(ipvp);
  }

  // strain xyz
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

  IPTFA* q1 = ipvcur->getIPTFA();
  const IPTFA* q0 = ipvprev->getIPTFA();
  //static STensor63 dCalgdeps;
  STensor43& elasticL = ipvcur->getRefToElasticTangentModuli();

  // compute stress
  _TFAlaw.constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),stiff,&elasticL,dTangent,ipvcur->getPtrTodCmdFm());

  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);

}

void TFADG3DMaterialLaw::loadClusterSummaryAndInteractionTensorsFromFiles(const std::string clusterSummaryFileName,
                                        const std::string interactionTensorsFileName)
{
  _TFAlaw.loadClusterSummaryAndInteractionTensorsFromFiles(clusterSummaryFileName,interactionTensorsFileName);
}

void TFADG3DMaterialLaw::loadClusterSummaryAndInteractionTensorsHomogenizedFrameFromFiles(const std::string clusterSummaryFileName,
                                        const std::string interactionTensorsFileName)
{
  _TFAlaw.loadClusterSummaryAndInteractionTensorsHomogenizedFrameFromFiles(clusterSummaryFileName,interactionTensorsFileName);
}

void TFADG3DMaterialLaw::loadClusterStandardDeviationFromFile(const std::string clusterSTDFileName)
{
  _TFAlaw.loadClusterStandardDeviationFromFile(clusterSTDFileName);
}


void TFADG3DMaterialLaw::loadPlasticEqStrainConcentrationFromFile(const std::string PlasticEqStrainConcentrationFileName,
                                                                  const std::string PlasticEqStrainConcentrationGeometricFileName,
                                                                  const std::string PlasticEqStrainConcentrationHarmonicFileName,
                                                                  const std::string PlasticEqStrainConcentrationPowerFileName)
{
  _TFAlaw.loadPlasticEqStrainConcentrationFromFile(PlasticEqStrainConcentrationFileName,PlasticEqStrainConcentrationGeometricFileName,
                                                   PlasticEqStrainConcentrationHarmonicFileName,PlasticEqStrainConcentrationPowerFileName);
};

void TFADG3DMaterialLaw::loadElasticStrainConcentrationDerivativeFromFile(const std::string ElasticStrainConcentrationDerivative)
{
  _TFAlaw.loadElasticStrainConcentrationDerivativeFromFile(ElasticStrainConcentrationDerivative);
}

void TFADG3DMaterialLaw::loadEshelbyInteractionTensorsFromFile(const std::string EshelbyinteractionTensorsFileName)
{
  _TFAlaw.loadEshelbyInteractionTensorsFromFile(EshelbyinteractionTensorsFileName);
}

void TFADG3DMaterialLaw::loadInteractionTensorsMatrixFrameELFromFile(const std::string InteractionTensorsMatrixFrameELFileName)
{
  _TFAlaw.loadInteractionTensorsMatrixFrameELFromFile(InteractionTensorsMatrixFrameELFileName);
}

void TFADG3DMaterialLaw::loadInteractionTensorsHomogenizedFrameELFromFile(const std::string InteractionTensorsHomogenizedFrameELFileName)
{
  _TFAlaw.loadInteractionTensorsHomogenizedFrameELFromFile(InteractionTensorsHomogenizedFrameELFileName);
}

void TFADG3DMaterialLaw::loadReferenceStiffnessFromFile(const std::string referenceStiffnessFileName)
{
  _TFAlaw.loadReferenceStiffnessFromFile(referenceStiffnessFileName);
}

void TFADG3DMaterialLaw::loadReferenceStiffnessIsoFromFile(const std::string referenceStiffnessIsoFileName)
{
  _TFAlaw.loadReferenceStiffnessIsoFromFile(referenceStiffnessIsoFileName);
}

void TFADG3DMaterialLaw::loadInteractionTensorIsoFromFile(const std::string interactionTensorsIsoFileName)
{
  _TFAlaw.loadInteractionTensorIsoFromFile(interactionTensorsIsoFileName);
}

void TFADG3DMaterialLaw::loadClusterFiberOrientationFromFile(const std::string OrientationDataFileName)
{
  _TFAlaw.loadClusterFiberOrientationFromFile(OrientationDataFileName);
}


void TFADG3DMaterialLaw::setTFAMethod(int TFAMethodNum, int dim, int correction, int solverType,  int tag)
{
  _TFAlaw.setTFAMethod(TFAMethodNum, dim, correction, solverType, tag);
}

void TFADG3DMaterialLaw::setClusterIncOrientation()
{
  _TFAlaw.setClusterIncOrientation();
}*/


GenericTFADG3DMaterialLaw::GenericTFADG3DMaterialLaw(const int num, double rho):
                                  dG3DMaterialLaw(num,rho,true)
{
  _GenericTFAlaw = new mlawGenericTFAMaterialLaws(num,rho);
  double nu = _GenericTFAlaw->poissonRatio();
  double mu = _GenericTFAlaw->shearModulus();
  double E = 2.*mu*(1.+nu);
  fillElasticStiffness(E, nu, elasticStiffness);
}

GenericTFADG3DMaterialLaw::GenericTFADG3DMaterialLaw(const GenericTFADG3DMaterialLaw &source) :
								     dG3DMaterialLaw(source), _GenericTFAlaw(source._GenericTFAlaw){}

void GenericTFADG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;

  IPTFA* ipvicluster=NULL;
  _GenericTFAlaw->createIPVariable(ipvicluster, hasBodyForce, ele,nbFF_, GP, gpt);
  IPTFA* ipv1cluster=NULL;
  _GenericTFAlaw->createIPVariable(ipv1cluster, hasBodyForce, ele,nbFF_, GP, gpt);
  IPTFA* ipv2cluster=NULL;
  _GenericTFAlaw->createIPVariable(ipv2cluster, hasBodyForce, ele,nbFF_, GP, gpt);

  IPVariable* ipvi = new  TFADG3DIPVariable(ipvicluster,hasBodyForce,inter);
  IPVariable* ipv1 = new  TFADG3DIPVariable(ipv1cluster,hasBodyForce,inter);
  IPVariable* ipv2 = new  TFADG3DIPVariable(ipv2cluster,hasBodyForce,inter);

  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void GenericTFADG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){ inter=false; }

  IPTFA* ipvicluster =NULL;
  _GenericTFAlaw->createIPVariable(ipvicluster, hasBodyForce, ele,nbFF_, GP, gpt);
  ipv = new  TFADG3DIPVariable(ipvicluster,hasBodyForce,inter);

}

void GenericTFADG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  // get ipvariable
  TFADG3DIPVariable* ipvcur;
  const TFADG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<TFADG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const TFADG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<TFADG3DIPVariable*>(ipv);
    ipvprev = static_cast<const TFADG3DIPVariable*>(ipvp);
  }

  _GenericTFAlaw->checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());

};


void GenericTFADG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  // get ipvariable
  TFADG3DIPVariable* ipvcur;
  const TFADG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<TFADG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const TFADG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<TFADG3DIPVariable*>(ipv);
    ipvprev = static_cast<const TFADG3DIPVariable*>(ipvp);
  }

  // strain xyz
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

  IPTFA* q1 = ipvcur->getIPTFA();
  const IPTFA* q0 = ipvprev->getIPTFA();
  //static STensor63 dCalgdeps;
  STensor43& elasticL = ipvcur->getRefToElasticTangentModuli();

  // compute stress
  _GenericTFAlaw->constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),stiff,&elasticL,dTangent,ipvcur->getPtrTodCmdFm());

  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);

}

void GenericTFADG3DMaterialLaw::loadClusterSummaryAndInteractionTensorsFromFiles(const std::string clusterSummaryFileName,
                                        const std::string interactionTensorsFileName)
{
  _GenericTFAlaw->loadClusterSummaryAndInteractionTensorsFromFiles(clusterSummaryFileName,interactionTensorsFileName);
}

void GenericTFADG3DMaterialLaw::loadClusterSummaryAndInteractionTensorsHomogenizedFrameFromFiles(const std::string clusterSummaryFileName,
                                        const std::string interactionTensorsFileName)
{
  _GenericTFAlaw->loadClusterSummaryAndInteractionTensorsHomogenizedFrameFromFiles(clusterSummaryFileName,interactionTensorsFileName);
}

void GenericTFADG3DMaterialLaw::loadClusterStandardDeviationFromFile(const std::string clusterSTDFileName)
{
  _GenericTFAlaw->loadClusterStandardDeviationFromFile(clusterSTDFileName);
}


void GenericTFADG3DMaterialLaw::loadPlasticEqStrainConcentrationFromFile(const std::string PlasticEqStrainConcentrationFileName,
                                                                  const std::string PlasticEqStrainConcentrationGeometricFileName,
                                                                  const std::string PlasticEqStrainConcentrationHarmonicFileName)
{
  _GenericTFAlaw->loadPlasticEqStrainConcentrationFromFile(PlasticEqStrainConcentrationFileName,PlasticEqStrainConcentrationGeometricFileName,PlasticEqStrainConcentrationHarmonicFileName);
};

void GenericTFADG3DMaterialLaw::loadElasticStrainConcentrationDerivativeFromFile(const std::string ElasticStrainConcentrationDerivative)
{
  _GenericTFAlaw->loadElasticStrainConcentrationDerivativeFromFile(ElasticStrainConcentrationDerivative);
}

void GenericTFADG3DMaterialLaw::loadEshelbyInteractionTensorsFromFile(const std::string EshelbyinteractionTensorsFileName)
{
  _GenericTFAlaw->loadEshelbyInteractionTensorsFromFile(EshelbyinteractionTensorsFileName);
}

void GenericTFADG3DMaterialLaw::loadInteractionTensorsMatrixFrameELFromFile(const std::string InteractionTensorsMatrixFrameELFileName)
{
  _GenericTFAlaw->loadInteractionTensorsMatrixFrameELFromFile(InteractionTensorsMatrixFrameELFileName);
}

void GenericTFADG3DMaterialLaw::loadInteractionTensorsHomogenizedFrameELFromFile(const std::string InteractionTensorsHomogenizedFrameELFileName)
{
  _GenericTFAlaw->loadInteractionTensorsHomogenizedFrameELFromFile(InteractionTensorsHomogenizedFrameELFileName);
}

void GenericTFADG3DMaterialLaw::loadReferenceStiffnessFromFile(const std::string referenceStiffnessFileName)
{
  _GenericTFAlaw->loadReferenceStiffnessFromFile(referenceStiffnessFileName);
}

void GenericTFADG3DMaterialLaw::loadReferenceStiffnessIsoFromFile(const std::string referenceStiffnessIsoFileName)
{
  _GenericTFAlaw->loadReferenceStiffnessIsoFromFile(referenceStiffnessIsoFileName);
}

void GenericTFADG3DMaterialLaw::loadInteractionTensorIsoFromFile(const std::string interactionTensorsIsoFileName)
{
  _GenericTFAlaw->loadInteractionTensorIsoFromFile(interactionTensorsIsoFileName);
}

void GenericTFADG3DMaterialLaw::loadClusterFiberOrientationFromFile(const std::string OrientationDataFileName)
{
  _GenericTFAlaw->loadClusterFiberOrientationFromFile(OrientationDataFileName);
}


void GenericTFADG3DMaterialLaw::setTFAMethod(int TFAMethodNum, int dim, int correction, int solverType,  int tag)
{
  _GenericTFAlaw->setTFAMethod(TFAMethodNum, dim, correction, solverType, tag);
}

void GenericTFADG3DMaterialLaw::setClusterIncOrientation()
{
  _GenericTFAlaw->setClusterIncOrientation();
}


TFADG3DMaterialLaw::TFADG3DMaterialLaw(const int num, double rho): GenericTFADG3DMaterialLaw(num, rho)
{
  if (_GenericTFAlaw != NULL) delete _GenericTFAlaw;
  _GenericTFAlaw = new mlawTFAMaterialLaws(num,rho);
}


HierarchicalTFADG3DMaterialLaw::HierarchicalTFADG3DMaterialLaw(const int num, double rho): GenericTFADG3DMaterialLaw(num, rho)
{
  if (_GenericTFAlaw != NULL) delete _GenericTFAlaw;
  _GenericTFAlaw = new mlawTFAHierarchicalMaterialLaws(num,rho);
}
void HierarchicalTFADG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;

  IPTFA2Levels* ipvicluster=NULL;
  dynamic_cast<mlawTFAHierarchicalMaterialLaws*>(_GenericTFAlaw)->createIPVariable(ipvicluster, hasBodyForce, ele,nbFF_, GP, gpt);
  IPTFA2Levels* ipv1cluster=NULL;
  dynamic_cast<mlawTFAHierarchicalMaterialLaws*>(_GenericTFAlaw)->createIPVariable(ipv1cluster, hasBodyForce, ele,nbFF_, GP, gpt);
  IPTFA2Levels* ipv2cluster=NULL;
  dynamic_cast<mlawTFAHierarchicalMaterialLaws*>(_GenericTFAlaw)->createIPVariable(ipv2cluster, hasBodyForce, ele,nbFF_, GP, gpt);

  IPVariable* ipvi = new  TFA2LevelsDG3DIPVariable(ipvicluster,hasBodyForce,inter);
  IPVariable* ipv1 = new  TFA2LevelsDG3DIPVariable(ipv1cluster,hasBodyForce,inter);
  IPVariable* ipv2 = new  TFA2LevelsDG3DIPVariable(ipv2cluster,hasBodyForce,inter);

  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}
void HierarchicalTFADG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){ inter=false; }

  IPTFA2Levels* ipvicluster =NULL;
  dynamic_cast<mlawTFAHierarchicalMaterialLaws*>(_GenericTFAlaw)->createIPVariable(ipvicluster, hasBodyForce, ele,nbFF_, GP, gpt);
  ipv = new  TFA2LevelsDG3DIPVariable(ipvicluster,hasBodyForce,inter);
}
void HierarchicalTFADG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  // get ipvariable
  TFA2LevelsDG3DIPVariable* ipvcur;
  const TFA2LevelsDG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<TFA2LevelsDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const TFA2LevelsDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<TFA2LevelsDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const TFA2LevelsDG3DIPVariable*>(ipvp);
  }
  _GenericTFAlaw->checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());
}
void HierarchicalTFADG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  // get ipvariable
  TFA2LevelsDG3DIPVariable* ipvcur;
  const TFA2LevelsDG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<TFA2LevelsDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const TFA2LevelsDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<TFA2LevelsDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const TFA2LevelsDG3DIPVariable*>(ipvp);
  }

  // strain xyz
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

  IPTFA2Levels* q1 = ipvcur->getIPTFA();
  const IPTFA2Levels* q0 = ipvprev->getIPTFA();
  //static STensor63 dCalgdeps;
  STensor43& elasticL = ipvcur->getRefToElasticTangentModuli();

  // compute stress
  dynamic_cast<mlawTFAHierarchicalMaterialLaws*>(_GenericTFAlaw)->constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),
                                                                               q0,q1,ipvcur->getRefToTangentModuli(),stiff,&elasticL,dTangent,ipvcur->getPtrTodCmdFm());

  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);
}
void HierarchicalTFADG3DMaterialLaw::setTFAMethod(int TFAMethodNum, int dim, int correction, int solverType,  int tag)
{
  dynamic_cast<mlawTFAHierarchicalMaterialLaws*>(_GenericTFAlaw)->setTFAMethod(TFAMethodNum, dim, correction, solverType, tag);
}
void HierarchicalTFADG3DMaterialLaw::setTFALawID(int tfaLawID)
{
  dynamic_cast<mlawTFAHierarchicalMaterialLaws*>(_GenericTFAlaw)->setTFALawID(tfaLawID);
}
void HierarchicalTFADG3DMaterialLaw::loadClusterSummaryAndInteractionTensorsLevel1FromFiles(const std::string clusterSummaryFileName,
                                                                                            const std::string interactionTensorsFileName)
{
  dynamic_cast<mlawTFAHierarchicalMaterialLaws*>(_GenericTFAlaw)->loadClusterSummaryAndInteractionTensorsLevel1FromFiles(clusterSummaryFileName,interactionTensorsFileName);
}

//

LinearThermoMechanicsDG3DMaterialLaw::LinearThermoMechanicsDG3DMaterialLaw(const int num,const double rho,const double Ex, const double Ey, const double Ez, const double Vxy,
									   const double Vxz,const double Vyz, const double MUxy, const double MUxz, const double MUyz,const double alpha,
									   const double beta, const double gamma ,const double t0,const double Kx,const double Ky, const double Kz,
									   const double alphax,const double alphay,const double alphaz,const double cp) :
                                                              dG3DMaterialLaw(num,rho,true)

{
  _lawLinearTM = new mlawLinearThermoMechanics(num,rho,Ex,Ey,Ez,Vxy,Vxz,Vyz,MUxy,MUxz,MUyz,alpha,beta,gamma,t0,Kx,Ky,Kz,alphax,alphay,alphaz,cp);

  double nu = _lawLinearTM->poissonRatio();
  double mu = _lawLinearTM->shearModulus();
  double E = 2.*mu*(1.+nu); //Where does this come from? To Have the E corresponding to the Vmax, MUmax
		            //In the linear & homogeneous & isotropic case ?

  linearK     = _lawLinearTM->getConductivityTensor();
  Stiff_alphaDilatation=_lawLinearTM->getStiff_alphaDilatation();

  fillElasticStiffness(E, nu, elasticStiffness);
}

LinearThermoMechanicsDG3DMaterialLaw::LinearThermoMechanicsDG3DMaterialLaw(const LinearThermoMechanicsDG3DMaterialLaw &source) :
                                                             dG3DMaterialLaw(source)
{
	_lawLinearTM = NULL;
	if (source._lawLinearTM != NULL){
		_lawLinearTM  = new mlawLinearThermoMechanics(*(source._lawLinearTM));
		// _linearTM = new mlawLinearThermoMechanics(*(source._lawLinearTM));  or this
	}
  linearK   = source.linearK;
  Stiff_alphaDilatation=source.Stiff_alphaDilatation;

}

void LinearThermoMechanicsDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  IPVariable* ipvi = new  LinearThermoMechanicsDG3DIPVariable(_lawLinearTM->getInitialTemperature(),hasBodyForce,inter);
  IPVariable* ipv1 = new  LinearThermoMechanicsDG3DIPVariable(_lawLinearTM->getInitialTemperature(),hasBodyForce,inter);
  IPVariable* ipv2 = new  LinearThermoMechanicsDG3DIPVariable(_lawLinearTM->getInitialTemperature(),hasBodyForce,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void LinearThermoMechanicsDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  ipv = new  LinearThermoMechanicsDG3DIPVariable(_lawLinearTM->getInitialTemperature(),hasBodyForce,inter);
}

double LinearThermoMechanicsDG3DMaterialLaw::getExtraDofStoredEnergyPerUnitField(double T) const
{
  return _lawLinearTM->getExtraDofStoredEnergyPerUnitField(T);
}

double LinearThermoMechanicsDG3DMaterialLaw::getInitialExtraDofStoredEnergyPerUnitField() const{
	return _lawLinearTM->getInitialExtraDofStoredEnergyPerUnitField();
};

void LinearThermoMechanicsDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
   /* get ipvariable */
  LinearThermoMechanicsDG3DIPVariable* ipvcur;
  const LinearThermoMechanicsDG3DIPVariable* ipvprev;

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<LinearThermoMechanicsDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const LinearThermoMechanicsDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<LinearThermoMechanicsDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const LinearThermoMechanicsDG3DIPVariable*>(ipvp);
  }

  _lawLinearTM->checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());
};

void LinearThermoMechanicsDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  /* get ipvariable */
  LinearThermoMechanicsDG3DIPVariable* ipvcur;
  const LinearThermoMechanicsDG3DIPVariable* ipvprev;

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<LinearThermoMechanicsDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const LinearThermoMechanicsDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<LinearThermoMechanicsDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const LinearThermoMechanicsDG3DIPVariable*>(ipvp);
  }

  /* strain xyz */
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();
  const double T = ipvcur->getConstRefToTemperature();
  const double T0 = ipvprev->getConstRefToTemperature();
  const SVector3& gradT= ipvcur->getConstRefToGradT();
        STensor3& dPdT=ipvcur->getRefTodPdT();
        SVector3& fluxT=ipvcur->getRefToThermalFlux();
        STensor3& dqdgradT=ipvcur->getRefTodThermalFluxdGradT();
        SVector3& dqdT=ipvcur->getRefTodThermalFluxdT();
        STensor33& dqdF=ipvcur->getRefTodThermalFluxdF();
  //const STensor3 *linearK()=ipvcur-> getConstRefTolinearK();

        STensor3& P=ipvcur->getRefToFirstPiolaKirchhoffStress();
        STensor43& Tangent=ipvcur->getRefToTangentModuli();
        double &w = ipvcur->getRefToThermalSource();
        double &dwdt = ipvcur->getRefTodThermalSourcedField();
        STensor3 &dwdf = ipvcur->getRefTodThermalSourcedF();
	const SVector3&N=ipvcur->getRefToReferenceOutwardNormal();
	const SVector3& ujump=ipvcur->getRefToJump()(0);
	const double &Tjump = ipvcur->getRefToFieldJump()(0);
        double& Cp = ipvcur->getRefToExtraDofFieldCapacityPerUnitField()(0);


  /* data for J2 law */
  IPLinearThermoMechanics* q1 = ipvcur->getIPLinearThermoMechanics();
  const IPLinearThermoMechanics* q0 = ipvprev->getIPLinearThermoMechanics();

 _lawLinearTM->	constitutive(F0,F1, P,q0, q1,Tangent,T0,T, gradT,fluxT,dPdT,dqdgradT,dqdT,dqdF,w,dwdt,dwdf,stiff);
  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);
  ipvcur->setRefToLinearConductivity(this->linearK);
  ipvcur->setRefToStiff_alphaDilatation(this->Stiff_alphaDilatation);

 /* if (checkfrac==true)    // to get the H1 norm in the interface
     q1->_fracEnergy=defoEnergy(ujump,Tjump,N);
   else
      q1->_elasticEnergy=defoEnergy(F1, gradT);*/
	Cp = _lawLinearTM->getExtraDofStoredEnergyPerUnitField(T);
}



  double LinearThermoMechanicsDG3DMaterialLaw::defoEnergy(const STensor3& Fn,const SVector3& gradT) const // If i have sigma ...
{

      static STensor3 defo;
      for (int i=0; i<3; i++)
        for (int j=0; j<3; j++)
          defo(i,j)=(Fn(j,i)+Fn(i,j))*0.5;
      defo(0,0)-=1.;
      defo(1,1)-=1.;
      defo(2,2)-=1.;
      double En=0.;
      for(int i=0;i<3;i++)
      {
	for(int j=0;j<3;j++)
	{
	 // En+=defo(i,j)*defo(i,j);
	}
	 En+=gradT(i)*gradT(i);
      }

      return En;
}

 double LinearThermoMechanicsDG3DMaterialLaw::defoEnergy( const SVector3&ujump,const double &Tjump, const SVector3& N) const // If i have sigma ...
{
  double e_interface=0.;
	 for(int i=0;i<3;i++)
	    {
	//   for(int j=0;j<3;j++)
	// {
         //e_interface+=ujump(i)*ujump(i);//+1./2.*Tjump*N(i)*N(i)*Tjump;
	e_interface+=Tjump*N(i)*N(i)*Tjump;
	 }
	// }
  return fabs(e_interface);
}




J2ThermoMechanicsDG3DMaterialLaw::J2ThermoMechanicsDG3DMaterialLaw(const int num,const double rho,
                                   double E, const double nu, const J2IsotropicHardening &_j2IH,
                                   const double t0,const double Kther, const double alphather, const double cp,
                                   const double tol) : dG3DMaterialLaw(num,rho,true), _j2law(num,E,nu,rho,_j2IH,t0,Kther,alphather,cp,tol)
{
  fillElasticStiffness(E, nu, elasticStiffness);
  linearK     = _j2law.getConductivityTensor();
  Stiff_alphaDilatation=_j2law.getStiff_alphaDilatation();
}

J2ThermoMechanicsDG3DMaterialLaw::J2ThermoMechanicsDG3DMaterialLaw(const J2ThermoMechanicsDG3DMaterialLaw &source) : dG3DMaterialLaw(source), _j2law(source._j2law)
{
  linearK     = source.linearK;
  Stiff_alphaDilatation=source.Stiff_alphaDilatation;
}

void J2ThermoMechanicsDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  IPVariable* ipvi = new  J2ThermoMechanicsDG3DIPVariable(_j2law.getInitialTemperature(),_j2law,hasBodyForce,inter);
  IPVariable* ipv1 = new  J2ThermoMechanicsDG3DIPVariable(_j2law.getInitialTemperature(),_j2law,hasBodyForce,inter);
  IPVariable* ipv2 = new  J2ThermoMechanicsDG3DIPVariable(_j2law.getInitialTemperature(),_j2law,hasBodyForce,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void J2ThermoMechanicsDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  ipv = new  J2ThermoMechanicsDG3DIPVariable(_j2law.getInitialTemperature(),_j2law,hasBodyForce,inter);
}

double J2ThermoMechanicsDG3DMaterialLaw::getExtraDofStoredEnergyPerUnitField(double T) const
{
  return _j2law.getExtraDofStoredEnergyPerUnitField(T);
}

double J2ThermoMechanicsDG3DMaterialLaw::getInitialExtraDofStoredEnergyPerUnitField() const{
	return _j2law.getInitialExtraDofStoredEnergyPerUnitField();
}

void J2ThermoMechanicsDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
   /* get ipvariable */
  J2ThermoMechanicsDG3DIPVariable* ipvcur;
  const J2ThermoMechanicsDG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<J2ThermoMechanicsDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const J2ThermoMechanicsDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<J2ThermoMechanicsDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const J2ThermoMechanicsDG3DIPVariable*>(ipvp);
  }

  _j2law.checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());
};

void J2ThermoMechanicsDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  /* get ipvariable */
  J2ThermoMechanicsDG3DIPVariable* ipvcur;
  const J2ThermoMechanicsDG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<J2ThermoMechanicsDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const J2ThermoMechanicsDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<J2ThermoMechanicsDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const J2ThermoMechanicsDG3DIPVariable*>(ipvp);
  }

  /* strain xyz */
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();
  const double T = ipvcur->getConstRefToTemperature();
  const double T0 = ipvprev->getConstRefToTemperature();
  const SVector3& gradT= ipvcur->getConstRefToGradT();
        STensor3& dPdT=ipvcur->getRefTodPdT();
        SVector3& fluxT=ipvcur->getRefToThermalFlux();
        STensor3& dqdgradT=ipvcur->getRefTodThermalFluxdGradT();
        SVector3& dqdT=ipvcur->getRefTodThermalFluxdT();
        STensor33& dqdF=ipvcur->getRefTodThermalFluxdF();
  //const STensor3 *linearK()=ipvcur-> getConstRefTolinearK();

        STensor3& P=ipvcur->getRefToFirstPiolaKirchhoffStress();
        STensor43& Tangent=ipvcur->getRefToTangentModuli();
        double &w = ipvcur->getRefToThermalSource();
        double &dwdt = ipvcur->getRefTodThermalSourcedField();
        STensor3 &dwdf = ipvcur->getRefTodThermalSourcedF();
	double& Cp = ipvcur->getRefToExtraDofFieldCapacityPerUnitField()(0);

  /* data for J2 law */
  IPJ2ThermoMechanics* q1 = ipvcur->getIPJ2ThermoMechanics();
  const IPJ2ThermoMechanics* q0 = ipvprev->getIPJ2ThermoMechanics();

  /* compute stress */
  _j2law.constitutive(F0,F1, P,q0, q1,Tangent,T0,T, gradT,fluxT,dPdT,dqdgradT,dqdT,dqdF,w,dwdt,dwdf,stiff);
  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);
  ipvcur->setRefToLinearConductivity(this->linearK);
  ipvcur->setRefToStiff_alphaDilatation(this->Stiff_alphaDilatation);
  Cp = _j2law.getExtraDofStoredEnergyPerUnitField(T);
}


FullJ2ThermoMechanicsDG3DMaterialLaw::FullJ2ThermoMechanicsDG3DMaterialLaw(const int num, const double E, const double nu,const double rho,
                               const double sy0,const double h,const double tol,
                               const bool matrixbyPerturbation, const double pert):
													dG3DMaterialLaw(num,rho),_j2FullThermo(num,E,nu,rho,sy0,h,tol,matrixbyPerturbation,pert,true){
	fillElasticStiffness(E, nu, elasticStiffness);
};

FullJ2ThermoMechanicsDG3DMaterialLaw::FullJ2ThermoMechanicsDG3DMaterialLaw(const FullJ2ThermoMechanicsDG3DMaterialLaw &src):
	dG3DMaterialLaw(src),_j2FullThermo(src._j2FullThermo){};

void FullJ2ThermoMechanicsDG3DMaterialLaw::setStrainOrder(const int i){
  _j2FullThermo.setStrainOrder(i);
};

void FullJ2ThermoMechanicsDG3DMaterialLaw::setReferenceTemperature(const double Tr){
  _j2FullThermo.setReferenceTemperature(Tr);
};

void FullJ2ThermoMechanicsDG3DMaterialLaw::setIsotropicHardeningLaw(const J2IsotropicHardening& isoHard){
  _j2FullThermo.setIsotropicHardeningLaw(isoHard);
};

void FullJ2ThermoMechanicsDG3DMaterialLaw::setReferenceThermalExpansionCoefficient(const double al){
  _j2FullThermo.setReferenceThermalExpansionCoefficient(al);
};
void FullJ2ThermoMechanicsDG3DMaterialLaw::setTemperatureFunction_ThermalExpansionCoefficient(const scalarFunction& Tfunc){
  _j2FullThermo.setTemperatureFunction_ThermalExpansionCoefficient(Tfunc);
};

void FullJ2ThermoMechanicsDG3DMaterialLaw::setReferenceThermalConductivity(const double KK){
  _j2FullThermo.setReferenceThermalConductivity(KK);
};
void FullJ2ThermoMechanicsDG3DMaterialLaw::setTemperatureFunction_ThermalConductivity(const scalarFunction& Tfunc){
  _j2FullThermo.setTemperatureFunction_ThermalConductivity(Tfunc);
};

void FullJ2ThermoMechanicsDG3DMaterialLaw::setReferenceCp(const double Cp){
  _j2FullThermo.setReferenceCp(Cp);
};
void FullJ2ThermoMechanicsDG3DMaterialLaw::setTemperatureFunction_Cp(const scalarFunction& Tfunc){
  _j2FullThermo.setTemperatureFunction_Cp(Tfunc);
};

void FullJ2ThermoMechanicsDG3DMaterialLaw::setTemperatureFunction_Hardening(const scalarFunction& Tfunc){
  _j2FullThermo.setTemperatureFunction_Hardening(Tfunc);
};
void FullJ2ThermoMechanicsDG3DMaterialLaw::setTemperatureFunction_InitialYieldStress(const scalarFunction& Tfunc){
  _j2FullThermo.setTemperatureFunction_InitialYieldStress(Tfunc);
};
void FullJ2ThermoMechanicsDG3DMaterialLaw::setTemperatureFunction_BulkModulus(const scalarFunction& Tfunc){
  _j2FullThermo.setTemperatureFunction_BulkModulus(Tfunc);
};
void FullJ2ThermoMechanicsDG3DMaterialLaw::setTemperatureFunction_ShearModulus(const scalarFunction& Tfunc){
  _j2FullThermo.setTemperatureFunction_ShearModulus(Tfunc);
};
void FullJ2ThermoMechanicsDG3DMaterialLaw::setTaylorQuineyFactor(const double f){
  _j2FullThermo.setTaylorQuineyFactor(f);
};
void FullJ2ThermoMechanicsDG3DMaterialLaw::setThermalEstimationPreviousConfig(const bool flag){
  _j2FullThermo.setThermalEstimationPreviousConfig(flag);
}

void FullJ2ThermoMechanicsDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
	bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  IPVariable* ipvi = new J2FullThermoMechanicsDG3DIPVariable(_j2FullThermo.getJ2IsotropicHardening(),hasBodyForce,inter);
  IPVariable* ipv1 = new J2FullThermoMechanicsDG3DIPVariable(_j2FullThermo.getJ2IsotropicHardening(),hasBodyForce,inter);
  IPVariable* ipv2 = new J2FullThermoMechanicsDG3DIPVariable(_j2FullThermo.getJ2IsotropicHardening(),hasBodyForce,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void FullJ2ThermoMechanicsDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
	bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;

  if(ipv !=NULL) delete ipv;
  ipv = new J2FullThermoMechanicsDG3DIPVariable(_j2FullThermo.getJ2IsotropicHardening(),hasBodyForce,inter);
}

void FullJ2ThermoMechanicsDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  J2FullThermoMechanicsDG3DIPVariable* ipvcur = static_cast<J2FullThermoMechanicsDG3DIPVariable*>(ipv);
  const J2FullThermoMechanicsDG3DIPVariable* ipvprev = static_cast<const J2FullThermoMechanicsDG3DIPVariable*>(ipvp);

  _j2FullThermo.checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());
};

void FullJ2ThermoMechanicsDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  J2FullThermoMechanicsDG3DIPVariable* ipvcur = static_cast<J2FullThermoMechanicsDG3DIPVariable*>(ipv);
  const J2FullThermoMechanicsDG3DIPVariable* ipvprev = static_cast<const J2FullThermoMechanicsDG3DIPVariable*>(ipvp);

	/* strain xyz */
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();
  const double T = ipvcur->getConstRefToTemperature();
  const double T0 = ipvprev->getConstRefToTemperature();
	const SVector3& gradT0= ipvprev->getConstRefToGradT();
  const SVector3& gradT= ipvcur->getConstRefToGradT();
        STensor3& dPdT=ipvcur->getRefTodPdT();
        SVector3& fluxT=ipvcur->getRefToThermalFlux();
        STensor3& dqdgradT=ipvcur->getRefTodThermalFluxdGradT();
        SVector3& dqdT=ipvcur->getRefTodThermalFluxdT();
        STensor33& dqdF=ipvcur->getRefTodThermalFluxdF();

        STensor3& P=ipvcur->getRefToFirstPiolaKirchhoffStress();
        STensor43& Tangent=ipvcur->getRefToTangentModuli();
        double &w = ipvcur->getRefToThermalSource();
        double &dwdt = ipvcur->getRefTodThermalSourcedField();
        STensor3 &dwdf = ipvcur->getRefTodThermalSourcedF();

	double&  mechSource =  ipvcur->getRefToMechanicalSource()(0);
        double& dmechSourcedt = ipvcur->getRefTodMechanicalSourcedField()(0,0);
        STensor3 & dmechSourcedf = ipvcur->getRefTodMechanicalSourcedF()[0];

	double& Cp = ipvcur->getRefToExtraDofFieldCapacityPerUnitField()(0);

  /* data for J2 law */
  IPJ2ThermoMechanics* q1 = ipvcur->getIPJ2ThermoMechanics();
  const IPJ2ThermoMechanics* q0 = ipvprev->getIPJ2ThermoMechanics();

  /* compute stress */
    _j2FullThermo.constitutive(F0,F1,P,q0,q1,Tangent,T0,T,gradT0,gradT,fluxT,dPdT,dqdgradT,dqdT,dqdF,
                      w,dwdt,dwdf,mechSource,dmechSourcedt,dmechSourcedf,stiff);


  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);
  ipvcur->setRefToLinearConductivity(_j2FullThermo.getConductivityTensor());
  ipvcur->setRefToStiff_alphaDilatation(_j2FullThermo.getStiff_alphaDilatation());
	Cp = _j2FullThermo.getExtraDofStoredEnergyPerUnitField(T);
}

double FullJ2ThermoMechanicsDG3DMaterialLaw::getInitialExtraDofStoredEnergyPerUnitField() const{
  return _j2FullThermo.getInitialExtraDofStoredEnergyPerUnitField();
};

double FullJ2ThermoMechanicsDG3DMaterialLaw::getExtraDofStoredEnergyPerUnitField(double T) const
{
 return _j2FullThermo.getExtraDofStoredEnergyPerUnitField(T);

}
double FullJ2ThermoMechanicsDG3DMaterialLaw::scaleFactor() const {
  return  _j2FullThermo.shearModulus();
};


mlawAnIsotropicTherMechDG3DMaterialLaw::mlawAnIsotropicTherMechDG3DMaterialLaw(const int num,const double E, const double nu,const double rho,const double EA, const double GA, const double nu_minor,
                           const double Ax, const double Ay, const double Az, const double alpha, const double beta, const double gamma,const double t0,
			   const double kx,const double ky,const double kz,const double cp,const double alphath):

                                                              dG3DMaterialLaw(num,rho,true)
{
  _lawAnTM = new mlawAnIsotropicTherMech(num,E,nu,rho,EA,GA,nu_minor,Ax,Ay,Az,alpha, beta,gamma,t0, kx, ky,kz,cp, alphath);

    linearK     = _lawAnTM->getConductivityTensor();
   Stiff_alphaDilatation=_lawAnTM->getStiff_alphaDilatation();

 fillElasticStiffness(E, nu, elasticStiffness);
}

mlawAnIsotropicTherMechDG3DMaterialLaw::mlawAnIsotropicTherMechDG3DMaterialLaw(const mlawAnIsotropicTherMechDG3DMaterialLaw &source) :
                                                          dG3DMaterialLaw  (source)// , dG3DMaterialLaw (source)
{
	_lawAnTM = NULL;
	if (source._lawAnTM !=NULL){
		//_lawTM  = new mlawAnIsotropicTherMech(*(source._lawTM));
		_lawAnTM  = new mlawAnIsotropicTherMech(*(source._lawAnTM));
		// _linearTM = new mlawLinearThermoMechanics(*(source._lawLinearTM));  or this
	}
  linearK     = source.linearK;
  Stiff_alphaDilatation=source. Stiff_alphaDilatation;
}

void mlawAnIsotropicTherMechDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
   IPVariable* ipvi = new AnIsotropicTherMechDG3DIPVariable(_lawAnTM->getInitialTemperature(),hasBodyForce,inter);
   IPVariable* ipv1 = new AnIsotropicTherMechDG3DIPVariable(_lawAnTM->getInitialTemperature(),hasBodyForce,inter);
   IPVariable* ipv2 = new AnIsotropicTherMechDG3DIPVariable(_lawAnTM->getInitialTemperature(),hasBodyForce,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void mlawAnIsotropicTherMechDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  ipv = new AnIsotropicTherMechDG3DIPVariable(_lawAnTM->getInitialTemperature(),hasBodyForce,inter);
}

void mlawAnIsotropicTherMechDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  AnIsotropicTherMechDG3DIPVariable* ipvcur;
  const AnIsotropicTherMechDG3DIPVariable* ipvprev;

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<AnIsotropicTherMechDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const AnIsotropicTherMechDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<AnIsotropicTherMechDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const AnIsotropicTherMechDG3DIPVariable*>(ipvp);
  }

  _lawAnTM->checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());
};

void mlawAnIsotropicTherMechDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{

  AnIsotropicTherMechDG3DIPVariable* ipvcur;
  const AnIsotropicTherMechDG3DIPVariable* ipvprev;

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<AnIsotropicTherMechDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const AnIsotropicTherMechDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<AnIsotropicTherMechDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const AnIsotropicTherMechDG3DIPVariable*>(ipvp);
  }


  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();
  const double T = ipvcur->getConstRefToTemperature();
  const double T0 = ipvprev->getConstRefToTemperature();
  const SVector3& gradT= ipvcur->getConstRefToGradT();
        STensor3& dPdT=ipvcur->getRefTodPdT();
        SVector3& fluxT=ipvcur->getRefToThermalFlux();
        STensor3& dqdgradT=ipvcur->getRefTodThermalFluxdGradT();
        SVector3& dqdT=ipvcur->getRefTodThermalFluxdT();
        STensor33& dqdF=ipvcur->getRefTodThermalFluxdF();
  //const STensor3 *linearK()=ipvcur-> getConstRefTolinearK();

        STensor3& P=ipvcur->getRefToFirstPiolaKirchhoffStress();
        STensor43& Tangent=ipvcur->getRefToTangentModuli();
        double &w = ipvcur->getRefToThermalSource();
        double &dwdt = ipvcur->getRefTodThermalSourcedField();
        STensor3 &dwdf = ipvcur->getRefTodThermalSourcedF();




  IPAnIsotropicTherMech* q1 = ipvcur->getIPAnIsotropicTherMech();
  const IPAnIsotropicTherMech* q0 = ipvprev->getIPAnIsotropicTherMech();

 _lawAnTM->constitutive(F0,F1, P,q0, q1,Tangent,T0,T, gradT,fluxT,dPdT,dqdgradT,dqdT,dqdF,w,dwdt,dwdf,stiff);

 ipvcur->setRefToLinearConductivity(this->linearK);
 ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);
 ipvcur->setRefToStiff_alphaDilatation(this->Stiff_alphaDilatation);

}



SMPDG3DMaterialLaw::SMPDG3DMaterialLaw(const int num,const double rho,const double alpha, const double beta, const double gamma ,const double t0, const double Kx,const double Ky, const double Kz,
       const double mu_groundState3,const double Im3, const double pi,const double Tr, const double Nmu_groundState2 ,const double mu_groundState2 ,
       const double Im2,const double Sgl2,const double Sr2,const double Delta,const double m2, const double epsilonr ,const double n,const double epsilonp02,
       const double alphar1,const double alphagl1,const double Ggl1,const double Gr1 ,const double Mgl1,const double Mr1,const double Mugl1,
       const double Mur1,const double epsilon01,const double Qgl1,const double Qr1,const double epsygl1 ,const double d1,
      const double m1,const double V1,const double alphap, const double  Sa0,const double ha1,const double b1,const double g1,
       const double phia01,const double Z1, const double r1,const double s1,const double Sb01,const double Hgl1,const double Lgl1,const double Hr1,
       const double Lr1,const double l1,const double Kb,const double be1,const double c0,const double wp,const double c1):
		          dG3DMaterialLaw (num,rho, true)
{
  _lawTMSMP = new mlawSMP( num,rho,alpha,beta,gamma,t0,Kx,Ky,Kz,mu_groundState3,Im3,pi,Tr,Nmu_groundState2, mu_groundState2,
			   Im2,Sgl2, Sr2, Delta, m2,epsilonr,n,epsilonp02, alphar1, alphagl1, Ggl1, Gr1, Mgl1, Mr1, Mugl1, Mur1, epsilon01, Qgl1,Qr1, epsygl1 , d1,  m1, V1,  alphap,
			   Sa0, ha1, b1, g1, phia01, Z1,r1, s1, Sb01, Hgl1, Lgl1, Hr1, Lr1, l1, Kb, be1,c0,wp,c1);

   double nu = Mr1;
   double mu = Gr1;
   double E = 2.*mu*(1.+nu); //Where does this come from? To Have the E corresponding to the Vmax, MUmax
		            //In the linear & homogeneous & isotropic case ?

  linearK  = _lawTMSMP->getInitialConductivityTensor();
  Stiff_alphaDilatation=_lawTMSMP->getStiff_alphaDilatation();

  fillElasticStiffness(E, nu, elasticStiffness);
}

SMPDG3DMaterialLaw::SMPDG3DMaterialLaw(const SMPDG3DMaterialLaw &source) :
                                                          dG3DMaterialLaw  (source)// , dG3DMaterialLaw (source)
{
	_lawTMSMP = NULL;
	if (source._lawTMSMP != NULL){
		_lawTMSMP  = new mlawSMP(*(source._lawTMSMP));
		// _linearTM = new mlawLinearThermoMechanics(*(source._lawLinearTM));  or this
	}
  linearK   = source.linearK;
  Stiff_alphaDilatation   = source.Stiff_alphaDilatation;

}

void SMPDG3DMaterialLaw::setStrainOrder(const int order ){
  _lawTMSMP->setStrainOrder(order);
};
void SMPDG3DMaterialLaw::setMechanism2(bool mechanism2) {
   _lawTMSMP->setMechanism2(mechanism2);
};

double SMPDG3DMaterialLaw::getExtraDofStoredEnergyPerUnitField(double T) const
{
  return _lawTMSMP->getExtraDofStoredEnergyPerUnitField(T);
}


void SMPDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  IPVariable* ipvi = new  SMPDG3DIPVariable(_lawTMSMP->getInitialTemperature(),*_lawTMSMP,hasBodyForce,inter);
  IPVariable* ipv1 = new  SMPDG3DIPVariable(_lawTMSMP->getInitialTemperature(),*_lawTMSMP,hasBodyForce,inter);
  IPVariable* ipv2 = new  SMPDG3DIPVariable(_lawTMSMP->getInitialTemperature(),*_lawTMSMP,hasBodyForce,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void SMPDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  ipv = new  SMPDG3DIPVariable(_lawTMSMP->getInitialTemperature(),*_lawTMSMP,hasBodyForce,inter);
}

void SMPDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  /* get ipvariable */
  SMPDG3DIPVariable* ipvcur;
  const SMPDG3DIPVariable* ipvprev;

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<SMPDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const SMPDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<SMPDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const SMPDG3DIPVariable*>(ipvp);
  }
  _lawTMSMP->checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());
};

void SMPDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  /* get ipvariable */
  SMPDG3DIPVariable* ipvcur;
  const SMPDG3DIPVariable* ipvprev;

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<SMPDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const SMPDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<SMPDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const SMPDG3DIPVariable*>(ipvp);
  }

  /* strain xyz */
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();
  const double temperature = ipvcur->getConstRefToTemperature();
  const double T0 = ipvprev->getConstRefToTemperature();
  const SVector3& gradT= ipvcur->getConstRefToGradT();

        STensor3& dPdT=ipvcur->getRefTodPdT();
        SVector3& fluxT=ipvcur->getRefToThermalFlux();
        STensor3& dqdgradT=ipvcur->getRefTodThermalFluxdGradT();
        SVector3& dqdT=ipvcur->getRefTodThermalFluxdT();
        STensor33& dqdF=ipvcur->getRefTodThermalFluxdF();

        STensor3& P=ipvcur->getRefToFirstPiolaKirchhoffStress();
        STensor43& Tangent=ipvcur->getRefToTangentModuli();
        double &w = ipvcur->getRefToThermalSource();
	double &dwdt = ipvcur->getRefTodThermalSourcedField();
	STensor3& dwdf = ipvcur->getRefTodThermalSourcedF();
  /* data for J2 law */
  IPSMP* q1 = ipvcur->getIPSMP();
  const IPSMP* q0 = ipvprev->getIPSMP();
 _lawTMSMP->	 constitutive(F0,F1, P,q0, q1,Tangent,T0,temperature, gradT,fluxT,dPdT,dqdgradT,dqdT,dqdF,stiff,w,dwdt,dwdf);


 ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);
 ipvcur->setRefToLinearConductivity(this->linearK);
 ipvcur->setRefToStiff_alphaDilatation(this->Stiff_alphaDilatation);
}



//===========/start/PhenomenologicalSMPDG3DMaterialLaw===================================================================================
PhenomenologicalSMPDG3DMaterialLaw::PhenomenologicalSMPDG3DMaterialLaw(const int num, const double rho, const double alpha, const double beta, const double gamma, const double t0,
                            const double Kxg, const double Kyg, const double Kzg, const double Kxr, const double Kyr, const double Kzr,
                            const double KxAM, const double KyAM, const double KzAM,
                            const double cfG, const double cfR, const double cfAM,
                            const double cg, const double cr, const double cAM,
                            const double Tm0, const double Tc0, const double xi, const double wm0, const double wc0,
                            const double alphag0, const double alphar0, const double alphaAM0, const double alphaAD0,
                            const double Eg, const double Er,const double EAM,
                            const double nug, const double nur, const double nuAM,
                            const double TaylorQuineyG,const double TaylorQuineyR, const double TaylorQuineyAM,
                            const double zAM): dG3DMaterialLaw (num, rho, true)
{
    _lawTMSMP = new mlawPhenomenologicalSMP( num, rho,alpha, beta, gamma,  t0,
                            Kxg, Kyg, Kzg, Kxr, Kyr, Kzr,
                            KxAM, KyAM, KzAM,
                            cfG, cfR, cfAM,
                            cg, cr, cAM,
                            Tm0, Tc0, xi, wm0, wc0,
                            alphag0, alphar0, alphaAM0, alphaAD0,
                            Eg, Er,EAM,
                            nug, nur, nuAM,
                            TaylorQuineyG,TaylorQuineyR, TaylorQuineyAM,
                            zAM);

    linearK               =_lawTMSMP->getConductivityTensor();
    Stiff_alphaDilatation =_lawTMSMP->getStiff_alphaDilatation();

    fillElasticStiffness(Eg, nug, elasticStiffness);
}

PhenomenologicalSMPDG3DMaterialLaw::PhenomenologicalSMPDG3DMaterialLaw(const PhenomenologicalSMPDG3DMaterialLaw &source)
                                   :dG3DMaterialLaw(source)
{
    _lawTMSMP = NULL;
    if (source._lawTMSMP != NULL) {_lawTMSMP  = new mlawPhenomenologicalSMP(*(source._lawTMSMP));}
    linearK                 = source.linearK;
    Stiff_alphaDilatation   = source.Stiff_alphaDilatation;
}

void PhenomenologicalSMPDG3DMaterialLaw::setTemperatureFunction_ThermalExpansionCoefficient_glassy(const scalarFunction& Tfunc)
{_lawTMSMP->setTemperatureFunction_ThermalExpansionCoefficient_glassy(Tfunc);};

void PhenomenologicalSMPDG3DMaterialLaw::setTemperatureFunction_ThermalExpansionCoefficient_rubbery(const scalarFunction& Tfunc)
{ _lawTMSMP->setTemperatureFunction_ThermalExpansionCoefficient_rubbery(Tfunc);};

void PhenomenologicalSMPDG3DMaterialLaw::setTemperatureFunction_ThermalExpansionCoefficient_AM(const scalarFunction& Tfunc)
{ _lawTMSMP->setTemperatureFunction_ThermalExpansionCoefficient_AM(Tfunc);};

void PhenomenologicalSMPDG3DMaterialLaw::setTemperatureFunction_ThermalConductivity_glassy(const scalarFunction& Tfunc)
{_lawTMSMP->setTemperatureFunction_ThermalConductivity_glassy(Tfunc);};

void PhenomenologicalSMPDG3DMaterialLaw::setTemperatureFunction_ThermalConductivity_rubbery(const scalarFunction& Tfunc)
{_lawTMSMP->setTemperatureFunction_ThermalConductivity_rubbery(Tfunc);};

void PhenomenologicalSMPDG3DMaterialLaw::setTemperatureFunction_ThermalConductivity_AM(const scalarFunction& Tfunc)
{_lawTMSMP->setTemperatureFunction_ThermalConductivity_AM(Tfunc);};

void PhenomenologicalSMPDG3DMaterialLaw::setTemperatureFunction_Cp_glassy(const scalarFunction& Tfunc)
{_lawTMSMP->setTemperatureFunction_Cp_glassy(Tfunc);};

void PhenomenologicalSMPDG3DMaterialLaw::setTemperatureFunction_Cp_rubbery(const scalarFunction& Tfunc)
{_lawTMSMP->setTemperatureFunction_Cp_rubbery(Tfunc);};

void PhenomenologicalSMPDG3DMaterialLaw::setTemperatureFunction_Cp_AM(const scalarFunction& Tfunc)
{_lawTMSMP->setTemperatureFunction_Cp_AM(Tfunc);};


void PhenomenologicalSMPDG3DMaterialLaw::setFunctionYieldCrystallinityG(const scalarFunction& Tfunc)
{_lawTMSMP->setFunctionYieldCrystallinityG(Tfunc);};

void PhenomenologicalSMPDG3DMaterialLaw::setFunctionYieldCrystallinityR(const scalarFunction& Tfunc)
{_lawTMSMP->setFunctionYieldCrystallinityR(Tfunc);};

void PhenomenologicalSMPDG3DMaterialLaw::setFunctionYieldCrystallinityAM(const scalarFunction& Tfunc)
{_lawTMSMP->setFunctionYieldCrystallinityAM(Tfunc);};

void PhenomenologicalSMPDG3DMaterialLaw::setFunctionCrystallizationVolumeRate(const scalarFunction& Tfunc)
{_lawTMSMP->setFunctionCrystallizationVolumeRate(Tfunc);};

void PhenomenologicalSMPDG3DMaterialLaw::setElasticPotentialFunctionR(const elasticPotential& EPfuncR)
{_lawTMSMP->setElasticPotentialFunctionR(EPfuncR);};
void PhenomenologicalSMPDG3DMaterialLaw::setElasticPotentialFunctionAM(const elasticPotential& EPfuncAM)
{_lawTMSMP->setElasticPotentialFunctionAM(EPfuncAM);};


void PhenomenologicalSMPDG3DMaterialLaw::setTcTmWcWm(const double Atc, const double Btm, const double Cwc, const double Dwm,
                             const double alphaAtc, const double alphaBtm, const double alphaCwc, const double alphaDwm)
{_lawTMSMP->setTcTmWcWm(Atc, Btm, Cwc, Dwm,alphaAtc,alphaBtm, alphaCwc, alphaDwm);};

void PhenomenologicalSMPDG3DMaterialLaw::setAlphaParam
                      (const double GP, const double RP, const double ADP, const double AMP, const double GN, const double RN, const double ADN, const double AMN,
                const double alphaGP, const double alphaRP, const double alphaADP, const double alphaAMP,
                const double alphaGN, const double alphaRN, const double alphaADN, const double alphaAMN)
{_lawTMSMP->setAlphaParam(GP, RP, ADP, AMP, GN, RN, ADN, AMN,alphaGP, alphaRP, alphaADP, alphaAMP, alphaGN, alphaRN, alphaADN, alphaAMN);};


double PhenomenologicalSMPDG3DMaterialLaw::getExtraDofStoredEnergyPerUnitField(double zg) const
{return _lawTMSMP->getExtraDofStoredEnergyPerUnitField(zg);}


void PhenomenologicalSMPDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_, const MElement *ele, const int nbFF_,
                                                                                                        const IntPt *GP, const int gpt) const
{
    // check interface element
    bool inter=true;
    const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
    if(iele==NULL) inter=false;

    IPVariable* ipvi = new PhenomenologicalSMPDG3DIPVariable(_lawTMSMP->getInitialTemperature(),*_lawTMSMP,hasBodyForce,inter);
    IPVariable* ipv1 = new PhenomenologicalSMPDG3DIPVariable(_lawTMSMP->getInitialTemperature(),*_lawTMSMP,hasBodyForce,inter);
    IPVariable* ipv2 = new PhenomenologicalSMPDG3DIPVariable(_lawTMSMP->getInitialTemperature(),*_lawTMSMP,hasBodyForce,inter);
    if(ips != NULL) delete ips;
    ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void PhenomenologicalSMPDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){inter=false;}

  ipv = new  PhenomenologicalSMPDG3DIPVariable(_lawTMSMP->getInitialTemperature(),*_lawTMSMP,hasBodyForce,inter);
}

void PhenomenologicalSMPDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  /* get ipvariable */
  PhenomenologicalSMPDG3DIPVariable* ipvcur;        //= static_cast <nonLocalDamageDG3DIPVariable *> (ipv);
  const PhenomenologicalSMPDG3DIPVariable* ipvprev; //= static_cast <const  nonLocalDamageDG3DIPVariable *> (ipvp);

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<PhenomenologicalSMPDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const PhenomenologicalSMPDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<PhenomenologicalSMPDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const PhenomenologicalSMPDG3DIPVariable*>(ipvp);
  }
  _lawTMSMP->checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());
};

void PhenomenologicalSMPDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  /* get ipvariable */
//      std::cout<<"dg3D stiff "<<stiff<<std::endl;

  PhenomenologicalSMPDG3DIPVariable* ipvcur;        //= static_cast < nonLocalDamageDG3DIPVariable *> (ipv);
  const PhenomenologicalSMPDG3DIPVariable* ipvprev; //= static_cast <const  nonLocalDamageDG3DIPVariable *> (ipvp);

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);

  if(ipvtmp !=NULL)
  {
    ipvcur = static_cast<PhenomenologicalSMPDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const PhenomenologicalSMPDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<PhenomenologicalSMPDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const PhenomenologicalSMPDG3DIPVariable*>(ipvp);
  }

    /* strain xyz */
    const STensor3& F1          =ipvcur->getConstRefToDeformationGradient();
    const STensor3& F0          =ipvprev->getConstRefToDeformationGradient();
    const double temperature    =ipvcur->getConstRefToTemperature();
    const double T0             =ipvprev->getConstRefToTemperature();
    const SVector3& gradT       =ipvcur->getConstRefToGradT();

    STensor3& dPdT              =ipvcur->getRefTodPdT();
    SVector3& fluxT             =ipvcur->getRefToThermalFlux();
    STensor3& dqdgradT          =ipvcur->getRefTodThermalFluxdGradT();
    SVector3& dqdT              =ipvcur->getRefTodThermalFluxdT();
    STensor33& dqdF             =ipvcur->getRefTodThermalFluxdF();

    STensor3& P                 =ipvcur->getRefToFirstPiolaKirchhoffStress();
    const STensor3& P0          =ipvprev->getConstRefToFirstPiolaKirchhoffStress();
    STensor43& Tangent          =ipvcur->getRefToTangentModuli();
    double &w                   =ipvcur->getRefToThermalSource();
    double &dwdt                =ipvcur->getRefTodThermalSourcedField();
    STensor3& dwdf              =ipvcur->getRefTodThermalSourcedF();

    double& mechSource          =ipvcur->getRefToMechanicalSource()(0);
    double& dmechSourcedt       =ipvcur->getRefTodMechanicalSourcedField()(0,0);
    STensor3& dmechSourcedf     =ipvcur->getRefTodMechanicalSourcedF()[0];

    /* data for J2 law */
    IPPhenomenologicalSMP* q1         =ipvcur ->getIPPhenomenologicalSMP();
    const IPPhenomenologicalSMP* q0   =ipvprev->getIPPhenomenologicalSMP();

    _lawTMSMP->constitutive(F0,F1, P, P0, q0, q1,Tangent,T0,temperature, gradT,fluxT,dPdT,dqdgradT,dqdT,dqdF,stiff,w,dwdt,dwdf,
                            mechSource,dmechSourcedt,dmechSourcedf);

    ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);
    ipvcur->setRefToLinearConductivity    (this->linearK);
    ipvcur->setRefToStiff_alphaDilatation (this->Stiff_alphaDilatation);
}
//===========/end/PhenomenologicalSMPDG3DMaterialLaw===================================================================================
//
crystalPlasticityDG3DMaterialLaw::crystalPlasticityDG3DMaterialLaw(const int num, const double rho, const double temperature, const char *propName, const char *matName) :
                                                              dG3DMaterialLaw(num,rho,true)
{
  _cplaw = new mlawCrystalPlasticity(num,rho,temperature,propName,matName);
  double nu = _cplaw->poissonRatio();
  double mu = _cplaw->shearModulus();
  double E = 2.*mu*(1.+nu);
  fillElasticStiffness(E, nu, elasticStiffness);
}

crystalPlasticityDG3DMaterialLaw::~crystalPlasticityDG3DMaterialLaw()
{
  if (_cplaw !=NULL) delete _cplaw;
  _cplaw = NULL;
};


crystalPlasticityDG3DMaterialLaw::crystalPlasticityDG3DMaterialLaw(const crystalPlasticityDG3DMaterialLaw &source) :
                                                             dG3DMaterialLaw(source)
{
	_cplaw = NULL;
	if (source._cplaw != NULL){
		_cplaw = dynamic_cast<mlawCrystalPlasticity*>(source._cplaw->clone());
	}

}

void crystalPlasticityDG3DMaterialLaw::setTime(const double t,const double dtime){
  dG3DMaterialLaw::setTime(t,dtime);
  _cplaw->setTime(t,dtime);
  return;
}

materialLaw::matname crystalPlasticityDG3DMaterialLaw::getType() const {return _cplaw->getType();}

bool crystalPlasticityDG3DMaterialLaw::withEnergyDissipation() const {return _cplaw->withEnergyDissipation();};

void crystalPlasticityDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;

  int nsdv = _cplaw->getNsdv();
  double size = 2.*(( const_cast<MElement*>(ele) )->getInnerRadius());

  IPVariable* ipvi = new  crystalPlasticityDG3DIPVariable(nsdv,size,hasBodyForce,inter);
  IPVariable* ipv1 = new  crystalPlasticityDG3DIPVariable(nsdv,size,hasBodyForce,inter);
  IPVariable* ipv2 = new  crystalPlasticityDG3DIPVariable(nsdv,size,hasBodyForce,inter);

  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
  _cplaw->createIPState((static_cast <crystalPlasticityDG3DIPVariable*> (ipvi))->getIPCrystalPlasticity(),
                         (static_cast <crystalPlasticityDG3DIPVariable*> (ipv1))->getIPCrystalPlasticity(),
             			 (static_cast <crystalPlasticityDG3DIPVariable*> (ipv2))->getIPCrystalPlasticity());

}

void crystalPlasticityDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL) inter=false;
  double size = 2.*(( const_cast<MElement*>(ele) )->getInnerRadius());

  ipv = new  crystalPlasticityDG3DIPVariable(_cplaw->getNsdv(),size,hasBodyForce,inter);

  IPCrystalPlasticity * ipvnl = static_cast <crystalPlasticityDG3DIPVariable*>(ipv)->getIPCrystalPlasticity();
  _cplaw->createIPVariable(ipvnl, hasBodyForce, ele,nbFF_,GP,gpt);
}


double crystalPlasticityDG3DMaterialLaw::soundSpeed() const{return _cplaw->soundSpeed();}

void crystalPlasticityDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  /* get ipvariable */
  crystalPlasticityDG3DIPVariable* ipvcur; //= static_cast < nonLocalDamageDG3DIPVariable *> (ipv);
  const crystalPlasticityDG3DIPVariable* ipvprev; //= static_cast <const  nonLocalDamageDG3DIPVariable *> (ipvp);

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<crystalPlasticityDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const crystalPlasticityDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<crystalPlasticityDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const crystalPlasticityDG3DIPVariable*>(ipvp);
  }
  _cplaw->checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());

};

void crystalPlasticityDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  /* get ipvariable */
  crystalPlasticityDG3DIPVariable* ipvcur; //= static_cast < nonLocalDamageDG3DIPVariable *> (ipv);
  const crystalPlasticityDG3DIPVariable* ipvprev; //= static_cast <const  nonLocalDamageDG3DIPVariable *> (ipvp);

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<crystalPlasticityDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const crystalPlasticityDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<crystalPlasticityDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const crystalPlasticityDG3DIPVariable*>(ipvp);
  }

  /* strain xyz */
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

  /* data for J2 law */
  IPCrystalPlasticity* q1 = ipvcur->getIPCrystalPlasticity();
  const IPCrystalPlasticity* q0 = ipvprev->getIPCrystalPlasticity();
  //static STensor63 dCalgdeps;
  //STensor43& elasticL = ipvcur->getRefToElasticTangentModuli();

  /* compute stress */
  _cplaw->constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),
                        stiff,NULL,dTangent,ipvcur->getPtrTodCmdFm());

  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);

}

double crystalPlasticityDG3DMaterialLaw::scaleFactor() const{return _cplaw->scaleFactor();};

void crystalPlasticityDG3DMaterialLaw::setMacroSolver(const nonLinearMechSolver* sv){
	dG3DMaterialLaw::setMacroSolver(sv);
	if (_cplaw != NULL){
		_cplaw->setMacroSolver(sv);
	}
};
//

//
VEVPUMATDG3DMaterialLaw::VEVPUMATDG3DMaterialLaw(const int num, const char *propName) :
                               dG3DMaterialLaw(num,0.,true)
{
  _vevplaw = new mlawVEVPUMAT(num,propName);
  double nu = _vevplaw->poissonRatio();
  double mu = _vevplaw->shearModulus();
  double E = 2.*mu*(1.+nu);
  _rho = _vevplaw->getDensity();
  fillElasticStiffness(E, nu, elasticStiffness);


}

VEVPUMATDG3DMaterialLaw::~VEVPUMATDG3DMaterialLaw()
{
  if (_vevplaw !=NULL) delete _vevplaw;
  _vevplaw = NULL;
};


VEVPUMATDG3DMaterialLaw::VEVPUMATDG3DMaterialLaw(const VEVPUMATDG3DMaterialLaw &source) :
                                                             dG3DMaterialLaw(source)
{
	_vevplaw = NULL;
	if (source._vevplaw != NULL){
		_vevplaw = dynamic_cast<mlawVEVPUMAT*>(source._vevplaw->clone());
	}

}

void VEVPUMATDG3DMaterialLaw::setTime(const double t,const double dtime){
  dG3DMaterialLaw::setTime(t,dtime);
  _vevplaw->setTime(t,dtime);
  return;
}

materialLaw::matname VEVPUMATDG3DMaterialLaw::getType() const {return _vevplaw->getType();}

bool VEVPUMATDG3DMaterialLaw::withEnergyDissipation() const {return _vevplaw->withEnergyDissipation();};

void VEVPUMATDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;

  int nsdv = _vevplaw->getNsdv();
  double size = 2.*(( const_cast<MElement*>(ele) )->getInnerRadius());

  IPVariable* ipvi = new  VEVPUMATDG3DIPVariable(nsdv,size,hasBodyForce,inter);
  IPVariable* ipv1 = new  VEVPUMATDG3DIPVariable(nsdv,size,hasBodyForce,inter);
  IPVariable* ipv2 = new  VEVPUMATDG3DIPVariable(nsdv,size,hasBodyForce,inter);

  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
  _vevplaw->createIPState((static_cast <VEVPUMATDG3DIPVariable*> (ipvi))->getIPVEVPUMAT(),
                         (static_cast <VEVPUMATDG3DIPVariable*> (ipv1))->getIPVEVPUMAT(),
             			 (static_cast <VEVPUMATDG3DIPVariable*> (ipv2))->getIPVEVPUMAT());

}

void VEVPUMATDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL) inter=false;
  double size = 2.*(( const_cast<MElement*>(ele) )->getInnerRadius());

  ipv = new  VEVPUMATDG3DIPVariable(_vevplaw->getNsdv(),size,hasBodyForce,inter);

  IPVEVPUMAT * ipvnl = static_cast <VEVPUMATDG3DIPVariable*>(ipv)->getIPVEVPUMAT();
  _vevplaw->createIPVariable(ipvnl,hasBodyForce, ele,nbFF_,GP,gpt);
}


double VEVPUMATDG3DMaterialLaw::soundSpeed() const{return _vevplaw->soundSpeed();}

void VEVPUMATDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  /* get ipvariable */
  VEVPUMATDG3DIPVariable* ipvcur; //= static_cast < nonLocalDamageDG3DIPVariable *> (ipv);
  const VEVPUMATDG3DIPVariable* ipvprev; //= static_cast <const  nonLocalDamageDG3DIPVariable *> (ipvp);

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<VEVPUMATDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const VEVPUMATDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<VEVPUMATDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const VEVPUMATDG3DIPVariable*>(ipvp);
  }
  _vevplaw->checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());

};

void VEVPUMATDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  /* get ipvariable */
  VEVPUMATDG3DIPVariable* ipvcur; //= static_cast < nonLocalDamageDG3DIPVariable *> (ipv);
  const VEVPUMATDG3DIPVariable* ipvprev; //= static_cast <const  nonLocalDamageDG3DIPVariable *> (ipvp);

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<VEVPUMATDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const VEVPUMATDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<VEVPUMATDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const VEVPUMATDG3DIPVariable*>(ipvp);
  }

  /* strain xyz */
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

  /* data for J2 law */
  IPVEVPUMAT* q1 = ipvcur->getIPVEVPUMAT();
  const IPVEVPUMAT* q0 = ipvprev->getIPVEVPUMAT();

  /* compute stress */
  _vevplaw->constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),
                        stiff, NULL,dTangent,ipvcur->getPtrTodCmdFm());

  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);

}

double VEVPUMATDG3DMaterialLaw::scaleFactor() const{return _vevplaw->scaleFactor();};

void VEVPUMATDG3DMaterialLaw::setMacroSolver(const nonLinearMechSolver* sv){
	dG3DMaterialLaw::setMacroSolver(sv);
	if (_vevplaw != NULL){
		_vevplaw->setMacroSolver(sv);
	}
};
//

//
IMDEACPUMATDG3DMaterialLaw::IMDEACPUMATDG3DMaterialLaw(const int num, const char *propName) :
                               dG3DMaterialLaw(num,0.,true)
{
  _vevplaw = new mlawIMDEACPUMAT(num,propName);
  double nu = _vevplaw->poissonRatio();
  double mu = _vevplaw->shearModulus();
  double E = 2.*mu*(1.+nu);
  _rho = _vevplaw->getDensity();
  fillElasticStiffness(E, nu, elasticStiffness);


}

IMDEACPUMATDG3DMaterialLaw::~IMDEACPUMATDG3DMaterialLaw()
{
  if (_vevplaw !=NULL) delete _vevplaw;
  _vevplaw = NULL;
};


IMDEACPUMATDG3DMaterialLaw::IMDEACPUMATDG3DMaterialLaw(const IMDEACPUMATDG3DMaterialLaw &source) :
                                                             dG3DMaterialLaw(source)
{
	_vevplaw = NULL;
	if (source._vevplaw != NULL){
		_vevplaw = dynamic_cast<mlawIMDEACPUMAT*>(source._vevplaw->clone());
	}

}

void IMDEACPUMATDG3DMaterialLaw::setTime(const double t,const double dtime){
  dG3DMaterialLaw::setTime(t,dtime);
  _vevplaw->setTime(t,dtime);
  return;
}

materialLaw::matname IMDEACPUMATDG3DMaterialLaw::getType() const {return _vevplaw->getType();}

bool IMDEACPUMATDG3DMaterialLaw::withEnergyDissipation() const {return _vevplaw->withEnergyDissipation();};

void IMDEACPUMATDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;

  int nsdv = _vevplaw->getNsdv();
  double size = 2.*(( const_cast<MElement*>(ele) )->getInnerRadius());

  IPVariable* ipvi = new  IMDEACPUMATDG3DIPVariable(nsdv,size,hasBodyForce,inter);
  IPVariable* ipv1 = new  IMDEACPUMATDG3DIPVariable(nsdv,size,hasBodyForce,inter);
  IPVariable* ipv2 = new  IMDEACPUMATDG3DIPVariable(nsdv,size,hasBodyForce,inter);

  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
  _vevplaw->createIPState((static_cast <IMDEACPUMATDG3DIPVariable*> (ipvi))->getIPIMDEACPUMAT(),
                         (static_cast <IMDEACPUMATDG3DIPVariable*> (ipv1))->getIPIMDEACPUMAT(),
             			 (static_cast <IMDEACPUMATDG3DIPVariable*> (ipv2))->getIPIMDEACPUMAT());

}

void IMDEACPUMATDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL) inter=false;
  double size = 2.*(( const_cast<MElement*>(ele) )->getInnerRadius());

  ipv = new  IMDEACPUMATDG3DIPVariable(_vevplaw->getNsdv(),size,hasBodyForce,inter);

  IPIMDEACPUMAT * ipvnl = static_cast <IMDEACPUMATDG3DIPVariable*>(ipv)->getIPIMDEACPUMAT();
  _vevplaw->createIPVariable(ipvnl,hasBodyForce, ele,nbFF_,GP,gpt);
}


double IMDEACPUMATDG3DMaterialLaw::soundSpeed() const{return _vevplaw->soundSpeed();}

void IMDEACPUMATDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  /* get ipvariable */
  IMDEACPUMATDG3DIPVariable* ipvcur; //= static_cast < nonLocalDamageDG3DIPVariable *> (ipv);
  const IMDEACPUMATDG3DIPVariable* ipvprev; //= static_cast <const  nonLocalDamageDG3DIPVariable *> (ipvp);

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<IMDEACPUMATDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const IMDEACPUMATDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<IMDEACPUMATDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const IMDEACPUMATDG3DIPVariable*>(ipvp);
  }
  _vevplaw->checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());

};

void IMDEACPUMATDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  /* get ipvariable */
  IMDEACPUMATDG3DIPVariable* ipvcur; //= static_cast < nonLocalDamageDG3DIPVariable *> (ipv);
  const IMDEACPUMATDG3DIPVariable* ipvprev; //= static_cast <const  nonLocalDamageDG3DIPVariable *> (ipvp);

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<IMDEACPUMATDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const IMDEACPUMATDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<IMDEACPUMATDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const IMDEACPUMATDG3DIPVariable*>(ipvp);
  }

  /* strain xyz */
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

  /* data for J2 law */
  IPIMDEACPUMAT* q1 = ipvcur->getIPIMDEACPUMAT();
  const IPIMDEACPUMAT* q0 = ipvprev->getIPIMDEACPUMAT();

  /* compute stress */
  _vevplaw->constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),
                        stiff, NULL,dTangent,ipvcur->getPtrTodCmdFm());

  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);

}

double IMDEACPUMATDG3DMaterialLaw::scaleFactor() const{return _vevplaw->scaleFactor();};

void IMDEACPUMATDG3DMaterialLaw::setMacroSolver(const nonLinearMechSolver* sv){
	dG3DMaterialLaw::setMacroSolver(sv);
	if (_vevplaw != NULL){
		_vevplaw->setMacroSolver(sv);
	}
};


//
gursonUMATDG3DMaterialLaw::gursonUMATDG3DMaterialLaw(const int num, const double temperature, const char *propName) :
                               dG3DMaterialLaw(num,0.,true)
{
  _glaw = new mlawGursonUMAT(num,temperature,propName);
  double nu = _glaw->poissonRatio();
  double mu = _glaw->shearModulus();
  double E = 2.*mu*(1.+nu);
  _rho = _glaw->getDensity();
  fillElasticStiffness(E, nu, elasticStiffness);


}

gursonUMATDG3DMaterialLaw::~gursonUMATDG3DMaterialLaw()
{
  if (_glaw !=NULL) delete _glaw;
  _glaw = NULL;
};


gursonUMATDG3DMaterialLaw::gursonUMATDG3DMaterialLaw(const gursonUMATDG3DMaterialLaw &source) :
                                                             dG3DMaterialLaw(source)
{
	_glaw = NULL;
	if (source._glaw != NULL){
		_glaw = dynamic_cast<mlawGursonUMAT*>(source._glaw->clone());
	}

}

void gursonUMATDG3DMaterialLaw::setTime(const double t,const double dtime){
  dG3DMaterialLaw::setTime(t,dtime);
  _glaw->setTime(t,dtime);
  return;
}

materialLaw::matname gursonUMATDG3DMaterialLaw::getType() const {return _glaw->getType();}

bool gursonUMATDG3DMaterialLaw::withEnergyDissipation() const {return _glaw->withEnergyDissipation();};

void gursonUMATDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;

  int nsdv = _glaw->getNsdv();
  double size = 2.*(( const_cast<MElement*>(ele) )->getInnerRadius());

  IPVariable* ipvi = new  gursonUMATDG3DIPVariable(nsdv,size,hasBodyForce,inter);
  IPVariable* ipv1 = new  gursonUMATDG3DIPVariable(nsdv,size,hasBodyForce,inter);
  IPVariable* ipv2 = new  gursonUMATDG3DIPVariable(nsdv,size,hasBodyForce,inter);

  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
  _glaw->createIPState((static_cast <gursonUMATDG3DIPVariable*> (ipvi))->getIPGursonUMAT(),
                         (static_cast <gursonUMATDG3DIPVariable*> (ipv1))->getIPGursonUMAT(),
             			 (static_cast <gursonUMATDG3DIPVariable*> (ipv2))->getIPGursonUMAT());

}

void gursonUMATDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL) inter=false;
  double size = 2.*(( const_cast<MElement*>(ele) )->getInnerRadius());

  ipv = new  gursonUMATDG3DIPVariable(_glaw->getNsdv(),size,hasBodyForce,inter);

  IPGursonUMAT * ipvnl = static_cast <gursonUMATDG3DIPVariable*>(ipv)->getIPGursonUMAT();
  _glaw->createIPVariable(ipvnl,hasBodyForce, ele,nbFF_,GP,gpt);
}


double gursonUMATDG3DMaterialLaw::soundSpeed() const{return _glaw->soundSpeed();}

void gursonUMATDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  /* get ipvariable */
  gursonUMATDG3DIPVariable* ipvcur; //= static_cast < nonLocalDamageDG3DIPVariable *> (ipv);
  const gursonUMATDG3DIPVariable* ipvprev; //= static_cast <const  nonLocalDamageDG3DIPVariable *> (ipvp);

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<gursonUMATDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const gursonUMATDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<gursonUMATDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const gursonUMATDG3DIPVariable*>(ipvp);
  }
  _glaw->checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());

};

void gursonUMATDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  /* get ipvariable */
  gursonUMATDG3DIPVariable* ipvcur; //= static_cast < nonLocalDamageDG3DIPVariable *> (ipv);
  const gursonUMATDG3DIPVariable* ipvprev; //= static_cast <const  nonLocalDamageDG3DIPVariable *> (ipvp);

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<gursonUMATDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const gursonUMATDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<gursonUMATDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const gursonUMATDG3DIPVariable*>(ipvp);
  }

  /* strain xyz */
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

  /* data for J2 law */
  IPGursonUMAT* q1 = ipvcur->getIPGursonUMAT();
  const IPGursonUMAT* q0 = ipvprev->getIPGursonUMAT();

  /* compute stress */
  _glaw->constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),
                        stiff, NULL,dTangent,ipvcur->getPtrTodCmdFm());

  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);

}

double gursonUMATDG3DMaterialLaw::scaleFactor() const{return _glaw->scaleFactor();};

void gursonUMATDG3DMaterialLaw::setMacroSolver(const nonLinearMechSolver* sv){
	dG3DMaterialLaw::setMacroSolver(sv);
	if (_glaw != NULL){
		_glaw->setMacroSolver(sv);
	}
};



LinearElecTherMechDG3DMaterialLaw::LinearElecTherMechDG3DMaterialLaw(const int num,const double rho,const double Ex, const double Ey, const double Ez, const double Vxy,
			 const double Vxz, const double Vyz,const double MUxy, const double MUxz, const double MUyz, const double alpha, const double beta, const double gamma,
			 const double t0,const double Kx,const double Ky,const double Kz,const double alphax,const double alphay,const double alphaz,const  double lx,const  double ly,
			 const  double lz,const double seebeck,double cp,const double v0):

                                                              dG3DMaterialLaw(num,rho,true)
{
  _lawLinearETM = new mlawLinearElecTherMech(num,rho, Ex, Ey,  Ez,  Vxy, Vxz, Vyz, MUxy, MUxz, MUyz,alpha, beta,gamma,  t0, Kx, Ky,Kz, alphax, alphay, alphaz,lx,ly,lz,seebeck,cp,v0);

  double nu = _lawLinearETM->poissonRatio();
  double mu = _lawLinearETM->shearModulus();
  double E = 2.*mu*(1.+nu); //Where does this come from? To Have the E corresponding to the Vmax, MUmax
		            //In the linear & homogeneous & isotropic case ?

  fillElasticStiffness(E, nu, elasticStiffness);
   Stiff_alphaDilatation=_lawLinearETM->getStiff_alphaDilatation();
}

LinearElecTherMechDG3DMaterialLaw::LinearElecTherMechDG3DMaterialLaw(const LinearElecTherMechDG3DMaterialLaw &source) :
                                                             dG3DMaterialLaw(source)
{
	_lawLinearETM = NULL;
	if (source._lawLinearETM != NULL){
		_lawLinearETM  = new mlawLinearElecTherMech(*(source._lawLinearETM));
		// _linearETM = new mlawLinearElecTherMech(*(source._lawLinearETM));  or this
	}
  Stiff_alphaDilatation=source.Stiff_alphaDilatation;
}

void LinearElecTherMechDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  IPVariable* ipvi = new LinearElecTherMechDG3DIPVariable(_lawLinearETM->getInitialTemperature(),_lawLinearETM->getInitialVoltage(),hasBodyForce,inter);
  IPVariable* ipv1 = new LinearElecTherMechDG3DIPVariable(_lawLinearETM->getInitialTemperature(),_lawLinearETM->getInitialVoltage(),hasBodyForce,inter);
  IPVariable* ipv2 = new LinearElecTherMechDG3DIPVariable(_lawLinearETM->getInitialTemperature(),_lawLinearETM->getInitialVoltage(),hasBodyForce,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}


void LinearElecTherMechDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  ipv = new LinearElecTherMechDG3DIPVariable(_lawLinearETM->getInitialTemperature(),_lawLinearETM->getInitialVoltage(),hasBodyForce,inter);
}

void LinearElecTherMechDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  /* get ipvariable */
  LinearElecTherMechDG3DIPVariable* ipvcur;
  const LinearElecTherMechDG3DIPVariable* ipvprev;

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<LinearElecTherMechDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const LinearElecTherMechDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<LinearElecTherMechDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const LinearElecTherMechDG3DIPVariable*>(ipvp);
  }

  _lawLinearETM->checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());
};

void LinearElecTherMechDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  /* get ipvariable */
  LinearElecTherMechDG3DIPVariable* ipvcur;
  const LinearElecTherMechDG3DIPVariable* ipvprev;

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<LinearElecTherMechDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const LinearElecTherMechDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<LinearElecTherMechDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const LinearElecTherMechDG3DIPVariable*>(ipvp);
  }

  /* strain xyz */
  const STensor3& Fn = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();
  const double temperature = ipvcur->getConstRefToTemperature();
  const double T0 = ipvprev->getConstRefToTemperature();
  const double voltage = ipvcur->getConstRefToVoltage();
  const double voltage0 = ipvprev->getConstRefToVoltage();
  const SVector3& gradT= ipvcur->getConstRefToGradT();
  const SVector3& gradV= ipvcur->getConstRefToGradV();

    STensor3& dPdT=ipvcur->getRefTodPdT();
	STensor3& dPdV=ipvcur->getRefTodPdV();
    SVector3 fluxT;
	SVector3& fluxV=ipvcur->getRefToFluxV();
    STensor3  dqdgradT;
    STensor3  dqdgradV;
	STensor3& dedgradV=ipvcur->getRefTodFluxVdGradV();
	STensor3& dedgradT=ipvcur->getRefTodFluxVdGradT();
    SVector3  dqdT;
	SVector3  dqdV;
	SVector3& dedV=ipvcur->getRefTodFluxVdV();
	SVector3& dedT=ipvcur->getRefTodFluxVdT();
    STensor33  dqdF;
	STensor33& dedF=ipvcur->getRefTodFluxVdF();
        STensor3& P=ipvcur->getRefToFirstPiolaKirchhoffStress();
        STensor43& Tangent=ipvcur->getRefToTangentModuli();
	double &w = ipvcur->getRefToThermalSource();
	double &dwdt = ipvcur->getRefTodThermalSourcedT();
	STensor3& dwdf = ipvcur->getRefTodThermalSourcedF();

	ipvcur->setRefToTherConductivity(this->lineark10);
        ipvcur->setRefToCrossTherConductivity(this->lineark20);
	ipvcur->setRefToElecConductivity(this->linearl10);
        ipvcur->setRefToCrossElecConductivity(this->linearl20);
	STensor3& dk10dT = ipvcur->getRefTodTherConductivitydT();
	STensor3& dk20dT = ipvcur->getRefTodCrossTherConductivitydT();
	STensor3& dl10dT = ipvcur->getRefTodElecConductivitydT();
	STensor3& dl20dT = ipvcur->getRefTodCrossElecConductivitydT();
	STensor3& dk10dv = ipvcur->getRefTodTherConductivitydv();
	STensor3& dk20dv = ipvcur->getRefTodCrossTherConductivitydv();
	STensor3& dl10dv = ipvcur->getRefTodElecConductivitydv();
	STensor3& dl20dv = ipvcur->getRefTodCrossElecConductivitydv();
	STensor43& dl10dF=ipvcur->getRefTodElecConductivitydF();
	STensor43& dl20dF=ipvcur->getRefTodCrossElecConductivitydF();
	STensor43& dk10dF=ipvcur->getRefTodTherConductivitydF();
	STensor43& dk20dF=ipvcur->getRefTodCrossTherConductivitydF();

	SVector3 &fluxjy=ipvcur->getRefToFluxEnergy();
	SVector3 &djydV=ipvcur->getRefTodFluxEnergydV();
	STensor3 &djydgradV=ipvcur->getRefTodFluxEnergydGradV();
	SVector3 &djydT=ipvcur->getRefTodFluxEnergydT();
	STensor3 &djydgradT=ipvcur->getRefTodFluxEnergydGradT();
        STensor33 &djydF=ipvcur->getRefTodFluxEnergydF();

        /*ipvcur->setRefToEnergyConductivity(this->jy1);
	STensor3 &djy1dT= ipvcur->getRefTodEnergyConductivitydT();
	STensor3 &djy1dV= ipvcur->getRefTodEnergyConductivitydV();
	STensor43 &djy1dF=ipvcur->getRefTodEnergyConductivitydF();*/

	STensor3 &djydgradVdT= ipvcur->getRefTodFluxEnergydGradVdT();
	STensor3 &djydgradVdV= ipvcur->getRefTodFluxEnergydGradVdV();
	STensor43 &djydgradVdF=ipvcur->getRefTodFluxEnergydGradVdF();
	STensor3 &djydgradTdT= ipvcur->getRefTodFluxEnergydGradTdT();
	STensor3 &djydgradTdV= ipvcur->getRefTodFluxEnergydGradTdV();
	STensor43 &djydgradTdF=ipvcur->getRefTodFluxEnergydGradTdF();

  /* data for J2 law */
  IPLinearElecTherMech* q1 = ipvcur->getIPLinearElecTherMech();
  const IPLinearElecTherMech* q0 = ipvprev->getIPLinearElecTherMech();

_lawLinearETM->constitutive(  F0,Fn,P, q0, q1, Tangent,T0, temperature, voltage0,voltage,gradT, gradV, fluxT,fluxV, dPdT,dPdV, dqdgradT,dqdT, dqdgradV,dqdV,
			      dedV,dedgradV,dedT,dedgradT,dqdF,dedF,w,dwdt,dwdf,linearl10,linearl20,lineark10,lineark20,dl10dT,dl20dT,dk10dT,dk20dT,dl10dv,dl20dv,dk10dv,dk20dv,dl10dF,dl20dF,dk10dF,dk20dF
			      ,fluxjy,djydV,djydgradV,djydT,djydgradT,djydF,djydgradVdT,djydgradVdV,djydgradVdF,djydgradTdT,djydgradTdV,djydgradTdF,stiff);


 ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);
 ipvcur->setRefToStiff_alphaDilatation(this->Stiff_alphaDilatation);


}



// Linear ElectroMagTherMech
LinearElecMagTherMechDG3DMaterialLaw::LinearElecMagTherMechDG3DMaterialLaw(const int num,const double rho,const double Ex,
const double Ey, const double Ez, const double Vxy, const double Vxz, const double Vyz,const double MUxy, const double MUxz,
const double MUyz, const double alpha, const double beta, const double gamma, const double t0,const double Kx,
const double Ky,const double Kz,const double alphax,const double alphay,const double alphaz,const  double lx,
const  double ly, const  double lz,const double seebeck,double cp,const double v0,
const double mu_x, const double mu_y, const double mu_z, const double A0_x, const double A0_y, const double A0_z,
const double Irms, const double freq, const unsigned int nTurnsCoil,
const double coilLength_x, const double coilLength_y, const double coilLength_z, const double coilWidth, const bool useFluxT,
const bool evaluateCurlField)

: dG3DMaterialLaw(num,rho,true)
{
  _lawLinearEMTM = new mlawLinearElecMagTherMech(num,rho, Ex, Ey,  Ez,  Vxy, Vxz, Vyz, MUxy, MUxz, MUyz,alpha, beta,gamma,
  t0, Kx, Ky,Kz, alphax, alphay, alphaz,lx,ly,lz,seebeck,cp,v0,mu_x,mu_y,mu_z,A0_x,A0_y,A0_z,Irms,freq,nTurnsCoil,
  coilLength_x,coilLength_y,coilLength_z,coilWidth,useFluxT,evaluateCurlField);

  this->_useFluxT = useFluxT;
  this->_evaluateCurlField = evaluateCurlField;

  double nu = _lawLinearEMTM->poissonRatio();
  double mu = _lawLinearEMTM->shearModulus();
  double E = 2.*mu*(1.+nu);   //In the linear & homogeneous & isotropic case

  fillElasticStiffness(E, nu, elasticStiffness);
}

LinearElecMagTherMechDG3DMaterialLaw::LinearElecMagTherMechDG3DMaterialLaw(const LinearElecMagTherMechDG3DMaterialLaw &src)
: dG3DMaterialLaw(src)
{
    _lawLinearEMTM = NULL;
	if (src._lawLinearEMTM != NULL)
	{
        _lawLinearEMTM  = new mlawLinearElecMagTherMech(*(src._lawLinearEMTM));
	}
	this->_useFluxT = src._useFluxT;
    this->_evaluateCurlField = src._evaluateCurlField;
}

void LinearElecMagTherMechDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele,
const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL)
    inter=false;
  IPVariable* ipvi = new LinearElecMagTherMechDG3DIPVariable(_lawLinearEMTM->getInitialTemperature(),
  _lawLinearEMTM->getInitialVoltage(),_lawLinearEMTM->getInitialMagPotential(),hasBodyForce,inter);
  IPVariable* ipv1 = new LinearElecMagTherMechDG3DIPVariable(_lawLinearEMTM->getInitialTemperature(),
  _lawLinearEMTM->getInitialVoltage(),_lawLinearEMTM->getInitialMagPotential(),hasBodyForce,inter);
  IPVariable* ipv2 = new LinearElecMagTherMechDG3DIPVariable(_lawLinearEMTM->getInitialTemperature(),
  _lawLinearEMTM->getInitialVoltage(),_lawLinearEMTM->getInitialMagPotential(),hasBodyForce,inter);
  if(ips != NULL)
    delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void LinearElecMagTherMechDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_,
const IntPt *GP, const int gpt) const
{
    if(ipv !=NULL)
        delete ipv;
    bool inter=true;
    const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
    if(iele == NULL)
    {
        inter=false;
    }
    ipv = new LinearElecMagTherMechDG3DIPVariable(_lawLinearEMTM->getInitialTemperature(),
    _lawLinearEMTM->getInitialVoltage(),_lawLinearEMTM->getInitialMagPotential(),hasBodyForce,inter);
}

void LinearElecMagTherMechDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const
{
    /* get ipvariable */
    LinearElecMagTherMechDG3DIPVariable* ipvcur;
    const LinearElecMagTherMechDG3DIPVariable* ipvprev;

    FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
    if(ipvtmp !=NULL)
    {
        ipvcur = static_cast<LinearElecMagTherMechDG3DIPVariable*>(ipvtmp->getIPvBulk());
        const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
        ipvprev = static_cast<const LinearElecMagTherMechDG3DIPVariable*>(ipvtmp2->getIPvBulk());
    }
    else
    {
        ipvcur = static_cast<LinearElecMagTherMechDG3DIPVariable*>(ipv);
        ipvprev = static_cast<const LinearElecMagTherMechDG3DIPVariable*>(ipvp);
    }

    _lawLinearEMTM->checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());
}

void LinearElecMagTherMechDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff,
const bool checkfrac, const bool dTangent)
{
    /* get ipvariable */
    LinearElecMagTherMechDG3DIPVariable* ipvcur;
    const LinearElecMagTherMechDG3DIPVariable* ipvprev;

    FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
    if(ipvtmp !=NULL)
    {
        ipvcur = static_cast<LinearElecMagTherMechDG3DIPVariable*>(ipvtmp->getIPvBulk());
        const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
        ipvprev = static_cast<const LinearElecMagTherMechDG3DIPVariable*>(ipvtmp2->getIPvBulk());
    }
    else
    {
        ipvcur = static_cast<LinearElecMagTherMechDG3DIPVariable*>(ipv);
        ipvprev = static_cast<const LinearElecMagTherMechDG3DIPVariable*>(ipvp);
    }

    /* strain xyz */
    const STensor3& Fn = ipvcur->getConstRefToDeformationGradient();
    const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();
    const double temperature = ipvcur->getConstRefToTemperature();
    const double T0 = ipvprev->getConstRefToTemperature();
    const double voltage = ipvcur->getConstRefToVoltage();
    const double voltage0 = ipvprev->getConstRefToVoltage();
    const SVector3& gradT= ipvcur->getConstRefToGradT();
    const SVector3& gradV= ipvcur->getConstRefToGradV();

    STensor3& dPdT=ipvcur->getRefTodPdT();
    STensor3& dPdV=ipvcur->getRefTodPdV();
    SVector3& fluxje=ipvcur->getRefToFluxje();
    STensor3& dedgradV=ipvcur->getRefTodFluxjedGradV();
    STensor3& dedgradT=ipvcur->getRefTodFluxjedGradT();
    SVector3& dedV=ipvcur->getRefTodFluxjedV();
    SVector3& dedT=ipvcur->getRefTodFluxjedT();
    STensor33& dedF=ipvcur->getRefTodFluxjedF();
    STensor3& P=ipvcur->getRefToFirstPiolaKirchhoffStress();
    STensor43& Tangent=ipvcur->getRefToTangentModuli();

    double &w_T = ipvcur->getRefToThermalSource();
    double &dw_TdT = ipvcur->getRefTodThermalSourcedT();
    double &dw_TdV = ipvcur->getRefTodThermalSourcedV();
    STensor3& dw_TdF = ipvcur->getRefTodThermalSourcedF();

    double &w_V = ipvcur->getRefToThermalSource();
    double &dw_VdT = ipvcur->getRefTodThermalSourcedT();
    double &dw_VdV = ipvcur->getRefTodThermalSourcedV();
    STensor3& dw_VdF = ipvcur->getRefTodThermalSourcedF();

    ipvcur->setRefToTherConductivity(this->lineark10);
    ipvcur->setRefToCrossTherConductivity(this->lineark20);
    ipvcur->setRefToElecConductivity(this->linearl10);
    ipvcur->setRefToCrossElecConductivity(this->linearl20);
    STensor3& dk10dT = ipvcur->getRefTodTherConductivitydT();
    STensor3& dk20dT = ipvcur->getRefTodCrossTherConductivitydT();
    STensor3& dl10dT = ipvcur->getRefTodElecConductivitydT();
    STensor3& dl20dT = ipvcur->getRefTodCrossElecConductivitydT();
    STensor3& dk10dv = ipvcur->getRefTodTherConductivitydv();
    STensor3& dk20dv = ipvcur->getRefTodCrossTherConductivitydv();
    STensor3& dl10dv = ipvcur->getRefTodElecConductivitydv();
    STensor3& dl20dv = ipvcur->getRefTodCrossElecConductivitydv();
    STensor43& dl10dF=ipvcur->getRefTodElecConductivitydF();
    STensor43& dl20dF=ipvcur->getRefTodCrossElecConductivitydF();
    STensor43& dk10dF=ipvcur->getRefTodTherConductivitydF();
    STensor43& dk20dF=ipvcur->getRefTodCrossTherConductivitydF();

    SVector3 Vector3Tmp(0.0);
    STensor3 Tensor3Tmp(0.0);
    STensor33 Tensor33Tmp(0.0);
    STensor43 Tensor43Tmp(0.0);

    SVector3* fluxT=&Vector3Tmp;
    STensor3* dqdgradT=&Tensor3Tmp;
    STensor3* dqdgradV=&Tensor3Tmp;
    SVector3* dqdT=&Vector3Tmp;
    SVector3* dqdV=&Vector3Tmp;
    STensor33* dqdF=&Tensor33Tmp;
    STensor3* dfluxTdA=&Tensor3Tmp;
    STensor3* dfluxTdB=&Tensor3Tmp;

    SVector3 *fluxjy=&Vector3Tmp;
    SVector3 *djydV=&Vector3Tmp;
    STensor3 *djydgradV=&Tensor3Tmp;
    SVector3 *djydT=&Vector3Tmp;
    STensor3 *djydgradT=&Tensor3Tmp;
    STensor33 *djydF=&Tensor33Tmp;
    STensor3 * djydA =&Tensor3Tmp;
    STensor3 * djydB =&Tensor3Tmp;

    STensor3 *djydgradVdT=&Tensor3Tmp;
    STensor3 *djydgradVdV=&Tensor3Tmp;
    STensor43 *djydgradVdF=&Tensor43Tmp;
    STensor3 *djydgradTdT=&Tensor3Tmp;
    STensor3 *djydgradTdV=&Tensor3Tmp;
    STensor43 *djydgradTdF=&Tensor43Tmp;

    if (this->_useFluxT)
    {
        fluxT=&(ipvcur->getRefToFluxT());
        dqdgradT=&(ipvcur->getRefTodFluxTdGradT());
        dqdgradV=&(ipvcur->getRefTodFluxTdGradV());
        dqdT=&(ipvcur->getRefTodFluxTdT());
        dqdV=&(ipvcur->getRefTodFluxTdV());
        dqdF=&(ipvcur->getRefTodFluxTdF());
        dfluxTdA=&(ipvcur->getRefTodFluxTdMagneticVectorPotential());
        dfluxTdB=&(ipvcur->getRefTodFluxTdMagneticVectorCurl());
    }
    else
    {
        fluxjy=&(ipvcur->getRefToFluxEnergy());
        djydV=&(ipvcur->getRefTodFluxEnergydV());
        djydgradV=&(ipvcur->getRefTodFluxEnergydGradV());
        djydT=&(ipvcur->getRefTodFluxEnergydT());
        djydgradT=&(ipvcur->getRefTodFluxEnergydGradT());
        djydF=&(ipvcur->getRefTodFluxEnergydF());
        djydA = &(ipvcur->getRefTodFluxEnergydMagneticVectorPotential());
        djydB = &(ipvcur->getRefTodFluxEnergydMagneticVectorCurl());

        djydgradVdT= &(ipvcur->getRefTodFluxEnergydGradVdT());
        djydgradVdV= &(ipvcur->getRefTodFluxEnergydGradVdV());
        djydgradVdF=&(ipvcur->getRefTodFluxEnergydGradVdF());
        djydgradTdT= &(ipvcur->getRefTodFluxEnergydGradTdT());
        djydgradTdV= &(ipvcur->getRefTodFluxEnergydGradTdV());
        djydgradTdF=&(ipvcur->getRefTodFluxEnergydGradTdF());
    }

    /* data for J2 law */
    IPLinearElecMagTherMech* q1 = ipvcur->getIPLinearElecMagTherMech();
    const IPLinearElecMagTherMech* q0 = ipvprev->getIPLinearElecMagTherMech();

    const SVector3 & An = ipvcur->getConstRefToMagneticVectorPotential();
    const SVector3 & A0 = ipvprev->getConstRefToMagneticVectorPotential();
    const SVector3 & B = ipvcur->getConstRefToMagneticInduction();
    SVector3 & H = ipvcur->getRefToMagneticField();
    SVector3 & js0 = ipvcur->getRefToinductorSourceVectorField();
    SVector3 dBdT;
    STensor3 dBdGradT;
    SVector3 dBdV;
    STensor3 dBdGradV;
    STensor33 dBdF;
    STensor3 dBdA;
    STensor3 &dfluxjedA = ipvcur->getRefTodFluxjedMagneticVectorPotential();
    STensor3 & dfluxjedB = ipvcur->getRefTodFluxjedMagneticVectorCurl();
    SVector3 & dw_TdA = ipvcur->getRefTodFieldSourcedMagneticVectorPotential();
    SVector3 & dw_TdB = ipvcur->getRefTodFieldSourcedMagneticVectorCurl();
    SVector3 & dmechSourcedB = ipvcur->getRefTodMechanicalSourcedMagneticVectorCurl();
    SVector3 & sourceVectorField = ipvcur->getRefTosourceVectorField(); // Magnetic source vector field (T, grad V, F)
    SVector3 & dsourceVectorFielddt = ipvcur->getRefTodsourceVectorFielddt();
    double & ThermalEMFieldSource = ipvcur->getRefToThermalEMFieldSource();
    double & VoltageEMFieldSource = ipvcur->getRefToVoltageEMFieldSource();
    STensor3 & dHdA = ipvcur->getRefTodMagneticFielddMagneticVectorPotential();
    STensor3 & dHdB = ipvcur->getRefTodMagneticFielddMagneticVectorCurl();
    STensor33 & dPdB = ipvcur->getRefTodPdMagneticVectorCurl();
    STensor33 & dHdF = ipvcur->getRefTodMagneticFielddF();
    STensor33 & dsourceVectorFielddF = ipvcur->getRefTodsourceVectorFielddF();
    SVector3 & mechFieldSource = ipvcur->getRefTomechanicalFieldSource(); // Electromagnetic force
    STensor3 & dmechFieldSourcedB = ipvcur->getRefTodMechanicalFieldSourcedMagneticVectorCurl();
    STensor33 & dmechFieldSourcedF = ipvcur->getRefTodmechanicalFieldSourcedF();
    SVector3 & dHdT = ipvcur->getRefTodMagneticFielddT();
    STensor3 & dHdgradT = ipvcur->getRefTodMagneticFielddGradT();
    SVector3 & dHdV = ipvcur->getRefTodMagneticFielddV();
    STensor3 & dHdgradV = ipvcur->getRefTodMagneticFielddGradV();
    SVector3 & dsourceVectorFielddT = ipvcur->getRefTodSourceVectorFielddT();
    STensor3 & dsourceVectorFielddgradT = ipvcur->getRefTodSourceVectorFielddGradT();
    SVector3 & dsourceVectorFielddV = ipvcur->getRefTodSourceVectorFielddV();
    STensor3 & dsourceVectorFielddgradV = ipvcur->getRefTodSourceVectorFielddGradV();
    STensor3 & dsourceVectorFielddA = ipvcur->getRefTodSourceVectorFielddMagneticVectorPotential();
    STensor3 & dsourceVectorFielddB = ipvcur->getRefTodSourceVectorFielddMagneticVectorCurl();
    SVector3 & dmechFieldSourcedT = ipvcur->getRefTodMechanicalFieldSourcedT();
    STensor3 & dmechFieldSourcedgradT = ipvcur->getRefTodMechanicalFieldSourcedGradT();
    SVector3 & dmechFieldSourcedV = ipvcur->getRefTodMechanicalFieldSourcedV();
    STensor3 & dmechFieldSourcedgradV = ipvcur->getRefTodMechanicalFieldSourcedGradV();
    SVector3 & dThermalEMFieldSourcedA = ipvcur->getRefTodThermalEMFieldSourcedMagneticVectorPotential();
    SVector3 & dThermalEMFieldSourcedB = ipvcur->getRefTodThermalEMFieldSourcedMagneticVectorCurl();
    STensor3 & dThermalEMFieldSourcedF = ipvcur->getRefTodThermalEMFieldSourcedF();
    double & dThermalEMFieldSourcedT = ipvcur->getRefTodThermalEMFieldSourcedT();
    SVector3 & dThermalEMFieldSourcedGradT = ipvcur->getRefTodThermalEMFieldSourcedGradT();
    double & dThermalEMFieldSourcedV = ipvcur->getRefTodThermalEMFieldSourcedV();
    SVector3 & dThermalEMFieldSourcedGradV = ipvcur->getRefTodThermalEMFieldSourcedGradV();
    SVector3 & dVoltageEMFieldSourcedA = ipvcur->getRefTodVoltageEMFieldSourcedMagneticVectorPotential();
    SVector3 & dVoltageEMFieldSourcedB = ipvcur->getRefTodVoltageEMFieldSourcedMagneticVectorCurl();
    STensor3 & dVoltageEMFieldSourcedF = ipvcur->getRefTodVoltageEMFieldSourcedF();
    double & dVoltageEMFieldSourcedT = ipvcur->getRefTodVoltageEMFieldSourcedT();
    SVector3 & dVoltageEMFieldSourcedGradT = ipvcur->getRefTodVoltageEMFieldSourcedGradT();
    double & dVoltageEMFieldSourcedV = ipvcur->getRefTodVoltageEMFieldSourcedV();
    SVector3 & dVoltageEMFieldSourcedGradV = ipvcur->getRefTodVoltageEMFieldSourcedGradV();

    _lawLinearEMTM->constitutive(F0,Fn,P, q0, q1, Tangent,T0, temperature, voltage0,voltage,gradT, gradV,
    *fluxT,fluxje, dPdT,dPdV, *dqdgradT,*dqdT, *dqdgradV,*dqdV,dedV,dedgradV,dedT,dedgradT,*dqdF,dedF,
    w_T,dw_TdT,dw_TdV,dw_TdF,w_V,dw_VdT,dw_VdV,dw_VdF,linearl10,linearl20,lineark10,lineark20,dl10dT,dl20dT,dk10dT,dk20dT,dl10dv,
    dl20dv,dk10dv,dk20dv,dl10dF,dl20dF,dk10dF,dk20dF,*fluxjy,*djydV,*djydgradV,*djydT,*djydgradT,
    *djydF,*djydA, *djydB, *djydgradVdT,*djydgradVdV,*djydgradVdF,*djydgradTdT,*djydgradTdV,*djydgradTdF,stiff,
    A0, An, B, H, js0, dBdT, dBdGradT, dBdV, dBdGradV, dBdF, dBdA, *dfluxTdA, dfluxjedA, *dfluxTdB, dfluxjedB, dw_TdA, dw_TdB, dmechSourcedB, sourceVectorField,
    dsourceVectorFielddt, ThermalEMFieldSource, VoltageEMFieldSource, dHdA, dHdB, dPdB, dHdF, dsourceVectorFielddF, mechFieldSource, dmechFieldSourcedB, dmechFieldSourcedF,
    dHdT, dHdgradT, dHdV, dHdgradV, dsourceVectorFielddT, dsourceVectorFielddgradT, dsourceVectorFielddV, dsourceVectorFielddgradV, dsourceVectorFielddA,
    dsourceVectorFielddB, dmechFieldSourcedT, dmechFieldSourcedgradT, dmechFieldSourcedV, dmechFieldSourcedgradV,
    dThermalEMFieldSourcedA, dThermalEMFieldSourcedB, dThermalEMFieldSourcedF, dThermalEMFieldSourcedT, dThermalEMFieldSourcedGradT,
    dThermalEMFieldSourcedV, dThermalEMFieldSourcedGradV, dVoltageEMFieldSourcedA, dVoltageEMFieldSourcedB, dVoltageEMFieldSourcedF,
    dVoltageEMFieldSourcedT, dVoltageEMFieldSourcedGradT, dVoltageEMFieldSourcedV, dVoltageEMFieldSourcedGradV);

    ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);
}

// LinearElecMagInductor
LinearElecMagInductorDG3DMaterialLaw::LinearElecMagInductorDG3DMaterialLaw(int num,const double rho, const double Ex, const double Ey, const double Ez,
const double Vxy, const double Vxz, const double Vyz, const double MUxy, const double MUxz,
const double MUyz,  const double alpha, const double beta, const double gamma,
const double t0,const double Kx,const double Ky, const double Kz,const double alphax,
const double alphay,const double alphaz, const  double lx,const  double ly,
const  double lz,const double seebeck,const double cp,const double v0,
const double mu_x, const double mu_y, const double mu_z, const double A0_x, const double A0_y,
const double A0_z, const double Irms, const double freq, const unsigned int nTurnsCoil,
const double coilLength_x, const double coilLength_y, const double coilLength_z, const double coilWidth,
const double Centroid_x, const double Centroid_y, const double Centroid_z,
const double CentralAxis_x, const double CentralAxis_y, const double CentralAxis_z, const bool useFluxT,
const bool evaluateCurlField)
: dG3DMaterialLaw(num, rho, true)
{
  _lawLinearEMInductor = new mlawLinearElecMagInductor(num, rho, Ex, Ey, Ez, Vxy, Vxz, Vyz, MUxy, MUxz, MUyz,
  alpha, beta, gamma, t0, Kx, Ky, Kz, alphax, alphay, alphaz, lx, ly, lz, seebeck, cp, v0,
  mu_x, mu_y, mu_z, A0_x, A0_y, A0_z, Irms, freq, nTurnsCoil,
  coilLength_x, coilLength_y, coilLength_z, coilWidth,
  Centroid_x, Centroid_y, Centroid_z, CentralAxis_x, CentralAxis_y, CentralAxis_z, useFluxT,
  evaluateCurlField);

  this->_useFluxT = useFluxT;
  this->_evaluateCurlField = evaluateCurlField;

  double nu = _lawLinearEMInductor->poissonRatio();
  double mu = _lawLinearEMInductor->shearModulus();
  double E = 2.*mu*(1.+nu);   //In the linear & homogeneous & isotropic case

  fillElasticStiffness(E, nu, elasticStiffness);
}

LinearElecMagInductorDG3DMaterialLaw::LinearElecMagInductorDG3DMaterialLaw(const LinearElecMagInductorDG3DMaterialLaw &src)
: dG3DMaterialLaw(src)
{
  _lawLinearEMInductor = NULL;
	if (src._lawLinearEMInductor != NULL)
	{
    _lawLinearEMInductor = new mlawLinearElecMagInductor(*(src._lawLinearEMInductor));
	}
	this->_useFluxT = src._useFluxT;
    this->_evaluateCurlField = src._evaluateCurlField;
}

void LinearElecMagInductorDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele,
const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL)
    inter=false;

  SPoint3 pt_Global;
  ele->pnt(GP->pt[0],GP->pt[1],GP->pt[2],pt_Global);
  SVector3 GaussP(pt_Global);

  IPVariable* ipvi = new LinearElecMagInductorDG3DIPVariable(GaussP, _lawLinearEMInductor->getCentroid(),
  _lawLinearEMInductor->getCentralAxis(), hasBodyForce,inter);
  IPVariable* ipv1 = new LinearElecMagInductorDG3DIPVariable(GaussP,_lawLinearEMInductor->getCentroid(),
  _lawLinearEMInductor->getCentralAxis(), hasBodyForce,inter);
  IPVariable* ipv2 = new LinearElecMagInductorDG3DIPVariable(GaussP,_lawLinearEMInductor->getCentroid(),
  _lawLinearEMInductor->getCentralAxis(), hasBodyForce,inter);
  if(ips != NULL)
    delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void LinearElecMagInductorDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_,
const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL)
      delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL)
  {
      inter=false;
  }

  SPoint3 pt_Global;
  ele->pnt(GP->pt[0],GP->pt[1],GP->pt[2],pt_Global);
  SVector3 GaussP(pt_Global);

  ipv = new LinearElecMagInductorDG3DIPVariable(GaussP,_lawLinearEMInductor->getCentroid(),
  _lawLinearEMInductor->getCentralAxis(),hasBodyForce,inter);
}

void LinearElecMagInductorDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const
{
  /* get ipvariable */
  LinearElecMagInductorDG3DIPVariable* ipvcur;
  const LinearElecMagInductorDG3DIPVariable* ipvprev;

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {
      ipvcur = static_cast<LinearElecMagInductorDG3DIPVariable*>(ipvtmp->getIPvBulk());
      const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
      ipvprev = static_cast<const LinearElecMagInductorDG3DIPVariable*>(ipvtmp2->getIPvBulk());
  }
  else
  {
      ipvcur = static_cast<LinearElecMagInductorDG3DIPVariable*>(ipv);
      ipvprev = static_cast<const LinearElecMagInductorDG3DIPVariable*>(ipvp);
  }

  _lawLinearEMInductor->checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());
}

void LinearElecMagInductorDG3DMaterialLaw::stress(IPVariable *ipv, const IPVariable *ipvp, const bool stiff,
const bool checkfrac, const bool dTangent)
{
  /* get ipvariable */
  LinearElecMagInductorDG3DIPVariable* ipvcur;
  const LinearElecMagInductorDG3DIPVariable* ipvprev;

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {
      ipvcur = static_cast<LinearElecMagInductorDG3DIPVariable*>(ipvtmp->getIPvBulk());
      const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
      ipvprev = static_cast<const LinearElecMagInductorDG3DIPVariable*>(ipvtmp2->getIPvBulk());
  }
  else
  {
      ipvcur = static_cast<LinearElecMagInductorDG3DIPVariable*>(ipv);
      ipvprev = static_cast<const LinearElecMagInductorDG3DIPVariable*>(ipvp);
  }

  /* strain xyz */
  const STensor3& Fn = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();
  const double temperature = ipvcur->getConstRefToTemperature();
  const double T0 = ipvprev->getConstRefToTemperature();
  const double voltage = ipvcur->getConstRefToVoltage();
  const double voltage0 = ipvprev->getConstRefToVoltage();
  const SVector3& gradT= ipvcur->getConstRefToGradT();
  const SVector3& gradV= ipvcur->getConstRefToGradV();

  STensor3& dPdT=ipvcur->getRefTodPdT();
  STensor3& dPdV=ipvcur->getRefTodPdV();
  SVector3& fluxje=ipvcur->getRefToFluxje();
  STensor3& dedgradV=ipvcur->getRefTodFluxjedGradV();
  STensor3& dedgradT=ipvcur->getRefTodFluxjedGradT();
  SVector3& dedV=ipvcur->getRefTodFluxjedV();
  SVector3& dedT=ipvcur->getRefTodFluxjedT();
  STensor33& dedF=ipvcur->getRefTodFluxjedF();
  STensor3& P=ipvcur->getRefToFirstPiolaKirchhoffStress();
  STensor43& Tangent=ipvcur->getRefToTangentModuli();
  double &w_T = ipvcur->getRefToThermalSource();
  double &dw_TdT = ipvcur->getRefTodThermalSourcedT();
  double &dw_TdV = ipvcur->getRefTodThermalSourcedV();
  STensor3& dw_TdF = ipvcur->getRefTodThermalSourcedF();

  double &w_V = ipvcur->getRefToThermalSource();
  double &dw_VdT = ipvcur->getRefTodThermalSourcedT();
  double &dw_VdV = ipvcur->getRefTodThermalSourcedV();
  STensor3& dw_VdF = ipvcur->getRefTodThermalSourcedF();

  ipvcur->setRefToTherConductivity(this->lineark10);
  ipvcur->setRefToCrossTherConductivity(this->lineark20);
  ipvcur->setRefToElecConductivity(this->linearl10);
  ipvcur->setRefToCrossElecConductivity(this->linearl20);
  STensor3& dk10dT = ipvcur->getRefTodTherConductivitydT();
  STensor3& dk20dT = ipvcur->getRefTodCrossTherConductivitydT();
  STensor3& dl10dT = ipvcur->getRefTodElecConductivitydT();
  STensor3& dl20dT = ipvcur->getRefTodCrossElecConductivitydT();
  STensor3& dk10dv = ipvcur->getRefTodTherConductivitydv();
  STensor3& dk20dv = ipvcur->getRefTodCrossTherConductivitydv();
  STensor3& dl10dv = ipvcur->getRefTodElecConductivitydv();
  STensor3& dl20dv = ipvcur->getRefTodCrossElecConductivitydv();
  STensor43& dl10dF=ipvcur->getRefTodElecConductivitydF();
  STensor43& dl20dF=ipvcur->getRefTodCrossElecConductivitydF();
  STensor43& dk10dF=ipvcur->getRefTodTherConductivitydF();
  STensor43& dk20dF=ipvcur->getRefTodCrossTherConductivitydF();

    SVector3 Vector3Tmp(0.0);
    STensor3 Tensor3Tmp(0.0);
    STensor33 Tensor33Tmp(0.0);
    STensor43 Tensor43Tmp(0.0);

    SVector3* fluxT=&Vector3Tmp;
    STensor3* dqdgradT=&Tensor3Tmp;
    STensor3* dqdgradV=&Tensor3Tmp;
    SVector3* dqdT=&Vector3Tmp;
    SVector3* dqdV=&Vector3Tmp;
    STensor33* dqdF=&Tensor33Tmp;
    STensor3* dfluxTdA=&Tensor3Tmp;
    STensor3* dfluxTdB=&Tensor3Tmp;

    SVector3 *fluxjy=&Vector3Tmp;
    SVector3 *djydV=&Vector3Tmp;
    STensor3 *djydgradV=&Tensor3Tmp;
    SVector3 *djydT=&Vector3Tmp;
    STensor3 *djydgradT=&Tensor3Tmp;
    STensor33 *djydF=&Tensor33Tmp;
    STensor3 * djydA =&Tensor3Tmp;
    STensor3 * djydB =&Tensor3Tmp;

    STensor3 *djydgradVdT=&Tensor3Tmp;
    STensor3 *djydgradVdV=&Tensor3Tmp;
    STensor43 *djydgradVdF=&Tensor43Tmp;
    STensor3 *djydgradTdT=&Tensor3Tmp;
    STensor3 *djydgradTdV=&Tensor3Tmp;
    STensor43 *djydgradTdF=&Tensor43Tmp;

    if (this->_useFluxT)
    {
        fluxT=&(ipvcur->getRefToFluxT());
        dqdgradT=&(ipvcur->getRefTodFluxTdGradT());
        dqdgradV=&(ipvcur->getRefTodFluxTdGradV());
        dqdT=&(ipvcur->getRefTodFluxTdT());
        dqdV=&(ipvcur->getRefTodFluxTdV());
        dqdF=&(ipvcur->getRefTodFluxTdF());
        dfluxTdA=&(ipvcur->getRefTodFluxTdMagneticVectorPotential());
        dfluxTdB=&(ipvcur->getRefTodFluxTdMagneticVectorCurl());
    }
    else
    {
        fluxjy=&(ipvcur->getRefToFluxEnergy());
        djydV=&(ipvcur->getRefTodFluxEnergydV());
        djydgradV=&(ipvcur->getRefTodFluxEnergydGradV());
        djydT=&(ipvcur->getRefTodFluxEnergydT());
        djydgradT=&(ipvcur->getRefTodFluxEnergydGradT());
        djydF=&(ipvcur->getRefTodFluxEnergydF());
        djydA = &(ipvcur->getRefTodFluxEnergydMagneticVectorPotential());
        djydB = &(ipvcur->getRefTodFluxEnergydMagneticVectorCurl());

        djydgradVdT= &(ipvcur->getRefTodFluxEnergydGradVdT());
        djydgradVdV= &(ipvcur->getRefTodFluxEnergydGradVdV());
        djydgradVdF=&(ipvcur->getRefTodFluxEnergydGradVdF());
        djydgradTdT= &(ipvcur->getRefTodFluxEnergydGradTdT());
        djydgradTdV= &(ipvcur->getRefTodFluxEnergydGradTdV());
        djydgradTdF=&(ipvcur->getRefTodFluxEnergydGradTdF());
    }

  /* data for J2 law */
  IPLinearElecMagInductor* q1 = ipvcur->getIPLinearElecMagInductor();
  const IPLinearElecMagInductor* q0 = ipvprev->getIPLinearElecMagInductor();

  const SVector3 & An = ipvcur->getConstRefToMagneticVectorPotential();
  const SVector3 & A0 = ipvprev->getConstRefToMagneticVectorPotential();
  const SVector3 & B = ipvcur->getConstRefToMagneticInduction();
  SVector3 & H = ipvcur->getRefToMagneticField();
  SVector3 & js0 = ipvcur->getRefToinductorSourceVectorField();
  SVector3 dBdT;
  STensor3 dBdGradT;
  SVector3 dBdV;
  STensor3 dBdGradV;
  STensor33 dBdF;
  STensor3 dBdA;
  STensor3 & dfluxjedA = ipvcur->getRefTodFluxjedMagneticVectorPotential();
  STensor3 & dfluxjedB = ipvcur->getRefTodFluxjedMagneticVectorCurl();
  SVector3 & dw_TdA = ipvcur->getRefTodFieldSourcedMagneticVectorPotential();
  SVector3 & dw_TdB = ipvcur->getRefTodFieldSourcedMagneticVectorCurl();
  SVector3 & dmechSourcedB = ipvcur->getRefTodMechanicalSourcedMagneticVectorCurl();
  SVector3 & sourceVectorField = ipvcur->getRefTosourceVectorField(); // Magnetic source vector field (T, grad V, F)
  SVector3 & dsourceVectorFielddt = ipvcur->getRefTodsourceVectorFielddt();
  double & ThermalEMFieldSource = ipvcur->getRefToThermalEMFieldSource();
  double & VoltageEMFieldSource = ipvcur->getRefToVoltageEMFieldSource();
  STensor3 & dHdA = ipvcur->getRefTodMagneticFielddMagneticVectorPotential();
  STensor3 & dHdB = ipvcur->getRefTodMagneticFielddMagneticVectorCurl();
  STensor33 & dPdB = ipvcur->getRefTodPdMagneticVectorCurl();
  STensor33 & dHdF = ipvcur->getRefTodMagneticFielddF();
  STensor33 & dsourceVectorFielddF = ipvcur->getRefTodsourceVectorFielddF();
  SVector3 & mechFieldSource = ipvcur->getRefTomechanicalFieldSource(); // Electromagnetic force
  STensor3 & dmechFieldSourcedB = ipvcur->getRefTodMechanicalFieldSourcedMagneticVectorCurl();
  STensor33 & dmechFieldSourcedF = ipvcur->getRefTodmechanicalFieldSourcedF();
  SVector3 & dHdT = ipvcur->getRefTodMagneticFielddT();
  STensor3 & dHdgradT = ipvcur->getRefTodMagneticFielddGradT();
  SVector3 & dHdV = ipvcur->getRefTodMagneticFielddV();
  STensor3 & dHdgradV = ipvcur->getRefTodMagneticFielddGradV();
  SVector3 & dsourceVectorFielddT = ipvcur->getRefTodSourceVectorFielddT();
  STensor3 & dsourceVectorFielddgradT = ipvcur->getRefTodSourceVectorFielddGradT();
  SVector3 & dsourceVectorFielddV = ipvcur->getRefTodSourceVectorFielddV();
  STensor3 & dsourceVectorFielddgradV = ipvcur->getRefTodSourceVectorFielddGradV();
  STensor3 & dsourceVectorFielddA = ipvcur->getRefTodSourceVectorFielddMagneticVectorPotential();
  STensor3 & dsourceVectorFielddB = ipvcur->getRefTodSourceVectorFielddMagneticVectorCurl();
  SVector3 & dmechFieldSourcedT = ipvcur->getRefTodMechanicalFieldSourcedT();
  STensor3 & dmechFieldSourcedgradT = ipvcur->getRefTodMechanicalFieldSourcedGradT();
  SVector3 & dmechFieldSourcedV = ipvcur->getRefTodMechanicalFieldSourcedV();
  STensor3 & dmechFieldSourcedgradV = ipvcur->getRefTodMechanicalFieldSourcedGradV();
  SVector3 & dThermalEMFieldSourcedA = ipvcur->getRefTodThermalEMFieldSourcedMagneticVectorPotential();
  SVector3 & dThermalEMFieldSourcedB = ipvcur->getRefTodThermalEMFieldSourcedMagneticVectorCurl();
  STensor3 & dThermalEMFieldSourcedF = ipvcur->getRefTodThermalEMFieldSourcedF();
  double & dThermalEMFieldSourcedT = ipvcur->getRefTodThermalEMFieldSourcedT();
  SVector3 & dThermalEMFieldSourcedGradT = ipvcur->getRefTodThermalEMFieldSourcedGradT();
  double & dThermalEMFieldSourcedV = ipvcur->getRefTodThermalEMFieldSourcedV();
  SVector3 & dThermalEMFieldSourcedGradV = ipvcur->getRefTodThermalEMFieldSourcedGradV();
  SVector3 & dVoltageEMFieldSourcedA = ipvcur->getRefTodVoltageEMFieldSourcedMagneticVectorPotential();
  SVector3 & dVoltageEMFieldSourcedB = ipvcur->getRefTodVoltageEMFieldSourcedMagneticVectorCurl();
  STensor3 & dVoltageEMFieldSourcedF = ipvcur->getRefTodVoltageEMFieldSourcedF();
  double & dVoltageEMFieldSourcedT = ipvcur->getRefTodVoltageEMFieldSourcedT();
  SVector3 & dVoltageEMFieldSourcedGradT = ipvcur->getRefTodVoltageEMFieldSourcedGradT();
  double & dVoltageEMFieldSourcedV = ipvcur->getRefTodVoltageEMFieldSourcedV();
  SVector3 & dVoltageEMFieldSourcedGradV = ipvcur->getRefTodVoltageEMFieldSourcedGradV();

  _lawLinearEMInductor->constitutive(F0,Fn,P, q0, q1, Tangent,T0, temperature, voltage0,voltage,gradT, gradV,
    *fluxT,fluxje, dPdT,dPdV, *dqdgradT,*dqdT, *dqdgradV,*dqdV,dedV,dedgradV,dedT,dedgradT,*dqdF,dedF,
    w_T,dw_TdT,dw_TdV,dw_TdF,w_V,dw_VdT,dw_VdV,dw_VdF,linearl10,linearl20,lineark10,lineark20,dl10dT,dl20dT,dk10dT,dk20dT,dl10dv,
    dl20dv,dk10dv,dk20dv,dl10dF,dl20dF,dk10dF,dk20dF,*fluxjy,*djydV,*djydgradV,*djydT,*djydgradT,
    *djydF, *djydA, *djydB, *djydgradVdT,*djydgradVdV,*djydgradVdF,*djydgradTdT,*djydgradTdV,*djydgradTdF,stiff,
    A0, An, B, H, js0, dBdT, dBdGradT, dBdV, dBdGradV, dBdF, dBdA, *dfluxTdA, dfluxjedA, *dfluxTdB, dfluxjedB, dw_TdA, dw_TdB, dmechSourcedB,
    sourceVectorField, dsourceVectorFielddt, ThermalEMFieldSource, VoltageEMFieldSource, dHdA, dHdB, dPdB, dHdF, dsourceVectorFielddF, mechFieldSource, dmechFieldSourcedB, dmechFieldSourcedF,
    dHdT, dHdgradT, dHdV, dHdgradV, dsourceVectorFielddT, dsourceVectorFielddgradT, dsourceVectorFielddV, dsourceVectorFielddgradV, dsourceVectorFielddA,
    dsourceVectorFielddB, dmechFieldSourcedT, dmechFieldSourcedgradT, dmechFieldSourcedV, dmechFieldSourcedgradV,
    dThermalEMFieldSourcedA, dThermalEMFieldSourcedB, dThermalEMFieldSourcedF, dThermalEMFieldSourcedT, dThermalEMFieldSourcedGradT,
    dThermalEMFieldSourcedV, dThermalEMFieldSourcedGradV, dVoltageEMFieldSourcedA, dVoltageEMFieldSourcedB, dVoltageEMFieldSourcedF,
    dVoltageEMFieldSourcedT, dVoltageEMFieldSourcedGradT, dVoltageEMFieldSourcedV, dVoltageEMFieldSourcedGradV);

  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);
}

mlawAnIsotropicElecTherMechDG3DMaterialLaw::mlawAnIsotropicElecTherMechDG3DMaterialLaw(const int num,const double E, const double nu,const double rho,const double EA, const double GA, const double nu_minor,
                           const double Ax, const double Ay, const double Az, const double alpha, const double beta, const double gamma,const double t0,const  double lx,
			   const  double ly,const  double lz,const double seebeck,const double kx,const double ky,
			   const double kz,const double cp,const double alphath,const double v0):

                                                              dG3DMaterialLaw(num,rho,true)
{
  _lawETM = new mlawAnIsotropicElecTherMech(num,E,nu,rho,EA,GA,nu_minor,Ax,Ay,Az,alpha, beta,gamma,t0, lx,ly,lz,seebeck,kx, ky,kz,cp, alphath,v0);

   Stiff_alphaDilatation=_lawETM->getStiff_alphaDilatation();

 fillElasticStiffness(E, nu, elasticStiffness);
}

mlawAnIsotropicElecTherMechDG3DMaterialLaw::mlawAnIsotropicElecTherMechDG3DMaterialLaw(const mlawAnIsotropicElecTherMechDG3DMaterialLaw &source) :
                                                          dG3DMaterialLaw  (source)// , dG3DMaterialLaw (source)
{
	_lawETM = NULL;
	if (source._lawETM != NULL){
		_lawETM  = new mlawAnIsotropicElecTherMech(*(source._lawETM));
		// _linearTM = new mlawLinearThermoMechanics(*(source._lawLinearTM));  or this
	}
  Stiff_alphaDilatation=source. Stiff_alphaDilatation;
}

void mlawAnIsotropicElecTherMechDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
   IPVariable* ipvi = new AnIsotropicElecTherMechDG3DIPVariable(_lawETM->getInitialTemperature(),_lawETM->getInitialVoltage(),*_lawETM,hasBodyForce,inter);
   IPVariable* ipv1 = new AnIsotropicElecTherMechDG3DIPVariable(_lawETM->getInitialTemperature(),_lawETM->getInitialVoltage(),*_lawETM,hasBodyForce,inter);
   IPVariable* ipv2 = new AnIsotropicElecTherMechDG3DIPVariable(_lawETM->getInitialTemperature(),_lawETM->getInitialVoltage(),*_lawETM,hasBodyForce,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void mlawAnIsotropicElecTherMechDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  ipv = new AnIsotropicElecTherMechDG3DIPVariable(_lawETM->getInitialTemperature(),_lawETM->getInitialVoltage(),*_lawETM,hasBodyForce,inter);
}

void mlawAnIsotropicElecTherMechDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  AnIsotropicElecTherMechDG3DIPVariable* ipvcur;
  const AnIsotropicElecTherMechDG3DIPVariable* ipvprev;

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<AnIsotropicElecTherMechDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const AnIsotropicElecTherMechDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<AnIsotropicElecTherMechDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const AnIsotropicElecTherMechDG3DIPVariable*>(ipvp);
  }

  _lawETM->checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());
};

void mlawAnIsotropicElecTherMechDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{

  AnIsotropicElecTherMechDG3DIPVariable* ipvcur;
  const AnIsotropicElecTherMechDG3DIPVariable* ipvprev;

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<AnIsotropicElecTherMechDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const AnIsotropicElecTherMechDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<AnIsotropicElecTherMechDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const AnIsotropicElecTherMechDG3DIPVariable*>(ipvp);
  }


  const STensor3& Fn = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();
  const double temperature = ipvcur->getConstRefToTemperature();
  const double T0          = ipvprev->getConstRefToTemperature();
  const double voltage = ipvcur->getConstRefToVoltage();
  const double voltage0 = ipvprev->getConstRefToVoltage();
  const SVector3& gradT= ipvcur->getConstRefToGradT();
  const SVector3& gradV= ipvcur->getConstRefToGradV();
        STensor3& dPdT =ipvcur->getRefTodPdT();
	STensor3& dPdV =ipvcur->getRefTodPdV();
        SVector3  fluxT;
	SVector3& fluxV=ipvcur->getRefToFluxV();
        STensor3 dqdgradT;
	STensor3 dqdgradV;
	STensor3& djedgradV=ipvcur->getRefTodFluxVdGradV();
	STensor3& djedgradT=ipvcur->getRefTodFluxVdGradT();
        SVector3  dqdT;
	SVector3  dqdV;
	SVector3& djedV =ipvcur->getRefTodFluxVdV();
	SVector3& djedT =ipvcur->getRefTodFluxVdT();
        STensor33 dqdF;
	STensor33& djedF=ipvcur->getRefTodFluxVdF();
        STensor3& P=ipvcur->getRefToFirstPiolaKirchhoffStress();
        STensor43& Tangent=ipvcur->getRefToTangentModuli();
	double &w = ipvcur->getRefToThermalSource();
	double &dwdT = ipvcur->getRefTodThermalSourcedT();
	STensor3& dwdf = ipvcur->getRefTodThermalSourcedF();

	ipvcur->setRefToTherConductivity(this->Kref10);
        ipvcur->setRefToCrossTherConductivity(this->Kref20);
	ipvcur->setRefToElecConductivity(this->Lref10);
        ipvcur->setRefToCrossElecConductivity(this->Lref20);
	STensor3& dk10dT = ipvcur->getRefTodTherConductivitydT();
	STensor3& dk20dT = ipvcur->getRefTodCrossTherConductivitydT();
	STensor3& dl10dT = ipvcur->getRefTodElecConductivitydT();
	STensor3& dl20dT = ipvcur->getRefTodCrossElecConductivitydT();
	STensor3& dk10dv = ipvcur->getRefTodTherConductivitydv();
	STensor3& dk20dv = ipvcur->getRefTodCrossTherConductivitydv();
	STensor3& dl10dv = ipvcur->getRefTodElecConductivitydv();
	STensor3& dl20dv = ipvcur->getRefTodCrossElecConductivitydv();
	STensor43& dl10dF=ipvcur->getRefTodElecConductivitydF();
	STensor43& dl20dF=ipvcur->getRefTodCrossElecConductivitydF();
	STensor43& dk10dF=ipvcur->getRefTodTherConductivitydF();
	STensor43& dk20dF=ipvcur->getRefTodCrossTherConductivitydF();

	SVector3 &fluxjy=ipvcur->getRefToFluxEnergy();
	SVector3 &djydV=ipvcur->getRefTodFluxEnergydV();
	STensor3 &djydgradV=ipvcur->getRefTodFluxEnergydGradV();
	SVector3 &djydT=ipvcur->getRefTodFluxEnergydT();
	STensor3 &djydgradT=ipvcur->getRefTodFluxEnergydGradT();
        STensor33 &djydF=ipvcur->getRefTodFluxEnergydF();

	/*ipvcur->setRefToEnergyConductivity(this->jy1);
	STensor3 &djy1dT= ipvcur->getRefTodEnergyConductivitydT();
	STensor3 &djy1dV= ipvcur->getRefTodEnergyConductivitydV();
	STensor43 &djy1dF=ipvcur->getRefTodEnergyConductivitydF();*/

	STensor3 &djydgradVdT= ipvcur->getRefTodFluxEnergydGradVdT();
	STensor3 &djydgradVdV= ipvcur->getRefTodFluxEnergydGradVdV();
	STensor43 &djydgradVdF=ipvcur->getRefTodFluxEnergydGradVdF();
	STensor3 &djydgradTdT= ipvcur->getRefTodFluxEnergydGradTdT();
	STensor3 &djydgradTdV= ipvcur->getRefTodFluxEnergydGradTdV();
	STensor43 &djydgradTdF=ipvcur->getRefTodFluxEnergydGradTdF();



  IPAnIsotropicElecTherMech* q1 = ipvcur->getIPAnIsotropicElecTherMech();
  const IPAnIsotropicElecTherMech* q0 = ipvprev->getIPAnIsotropicElecTherMech();

_lawETM->constitutive(  F0,Fn,P, q0, q1, Tangent, T0, temperature, voltage0, voltage,gradT, gradV, fluxT,fluxV, dPdT,dPdV, dqdgradT,dqdT, dqdgradV,dqdV,
	                djedV,djedgradV,djedT,djedgradT,dqdF,djedF,w,dwdT,dwdf,Lref10,Lref20,Kref10,Kref20,dl10dT,dl20dT,dk10dT,dk20dT,dl10dv,dl20dv,dk10dv,dk20dv,dl10dF,dl20dF,dk10dF,dk20dF
			      ,fluxjy,djydV,djydgradV,djydT,djydgradT,djydF,djydgradVdT,djydgradVdV,djydgradVdF,djydgradTdT,djydgradTdV,djydgradTdF,stiff);


 ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);
 ipvcur->setRefToStiff_alphaDilatation(this->Stiff_alphaDilatation);

}



mlawElecSMPDG3DMaterialLaw::mlawElecSMPDG3DMaterialLaw(const int num,const double rho, const double alpha, const double beta, const double gamma,const double t0,const double Kx,const double Ky,
			 const double Kz, const double mu_groundState3,const double Im3, const double pi  ,const double Tr,
		          const double Nmu_groundState2,const double mu_groundState2, const double Im2,const double Sgl2,const double Sr2,const double Delta,const double m2, const double epsilonr,
			  const double n, const double epsilonp02,const double alphar1,const double alphagl1,const double Ggl1,const double Gr1,const double Mgl1,const double Mr1,const double Mugl1
			  ,const double Mur1, const double epsilon01,const double QglOnKb1,const double QrOnKb1,const double epsygl1 ,const double d1,const double m1,const double VOnKb1,
			  const double alphap,const double  Sa0,const double ha1,const double b1,const double g1,const double phia01,const double Z1,const double r1,const double s1,const double Sb01
			  ,const double Hgl1,const double Lgl1,const double Hr1,const double Lr1,const double l1,const double Kb,const double be1,const double c0,const double wp,const double c1
			  ,const double lx,const double ly,const double lz,const double seebeck,const double v0):

                                                              dG3DMaterialLaw(num,rho,true)
{
  _lawETMSMP = new mlawElecSMP( num,rho,alpha,beta,gamma,t0,Kx,Ky,Kz,mu_groundState3,Im3,pi,
Tr,Nmu_groundState2, mu_groundState2,  Im2,Sgl2, Sr2, Delta, m2,epsilonr,n,epsilonp02, alphar1, alphagl1, Ggl1, Gr1, Mgl1, Mr1, Mugl1, Mur1, epsilon01, QglOnKb1,
QrOnKb1, epsygl1 , d1,  m1, VOnKb1,  alphap, Sa0, ha1, b1, g1, phia01, Z1,r1, s1, Sb01, Hgl1, Lgl1, Hr1, Lr1, l1, Kb, be1,c0,wp,c1,lx,ly,lz,seebeck,v0);


   double nu = Mur1;
   double mu = Gr1 ;
   double E = 2.*mu*(1.+nu); //Where does this come from? To Have the E corresponding to the Vmax, MUmax
		            //In the linear & homogeneous & isotropic case ?


 fillElasticStiffness(E, nu, elasticStiffness);

 Stiff_alphaDilatation= _lawETMSMP->getStiff_alphaDilatation();
}

mlawElecSMPDG3DMaterialLaw::mlawElecSMPDG3DMaterialLaw(const mlawElecSMPDG3DMaterialLaw &source) :
                                                          dG3DMaterialLaw  (source)// , dG3DMaterialLaw (source)
{
	_lawETMSMP = NULL;
	if (source._lawETMSMP != NULL){
		_lawETMSMP  = new mlawElecSMP(*(source._lawETMSMP));
	}
  Stiff_alphaDilatation=source. Stiff_alphaDilatation;
 // _linearTM = new mlawLinearThermoMechanics(*(source._lawLinearTM));  or this
 /* K10   = source.K10;
  K20   = source.K20;
  L10   = source.L10;
  L20   = source.L20;*/

}

void mlawElecSMPDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
   IPVariable* ipvi = new ElecSMPDG3DIPVariable(_lawETMSMP->getInitialTemperature(),_lawETMSMP->getInitialVoltage(),*_lawETMSMP,hasBodyForce,inter);
   IPVariable* ipv1 = new ElecSMPDG3DIPVariable(_lawETMSMP->getInitialTemperature(),_lawETMSMP->getInitialVoltage(),*_lawETMSMP,hasBodyForce,inter);
   IPVariable* ipv2 = new ElecSMPDG3DIPVariable(_lawETMSMP->getInitialTemperature(),_lawETMSMP->getInitialVoltage(),*_lawETMSMP,hasBodyForce,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void mlawElecSMPDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  ipv = new ElecSMPDG3DIPVariable(_lawETMSMP->getInitialTemperature(),_lawETMSMP->getInitialVoltage(),*_lawETMSMP,hasBodyForce,inter);
}

void mlawElecSMPDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
  ElecSMPDG3DIPVariable* ipvcur;
  const ElecSMPDG3DIPVariable* ipvprev;

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<ElecSMPDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const ElecSMPDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<ElecSMPDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const ElecSMPDG3DIPVariable*>(ipvp);
  }

  _lawETMSMP->checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());
};

void mlawElecSMPDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{

  ElecSMPDG3DIPVariable* ipvcur;
  const ElecSMPDG3DIPVariable* ipvprev;

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<ElecSMPDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const ElecSMPDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<ElecSMPDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const ElecSMPDG3DIPVariable*>(ipvp);
  }


  const STensor3& Fn = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();
  const double temperature = ipvcur->getConstRefToTemperature();
  const double T0          = ipvprev->getConstRefToTemperature();
  const double voltage = ipvcur->getConstRefToVoltage();
  const double voltage0 = ipvprev->getConstRefToVoltage();
  const SVector3& gradT= ipvcur->getConstRefToGradT();
  const SVector3& gradV= ipvcur->getConstRefToGradV();
        STensor3& dPdT = ipvcur->getRefTodPdT();
	STensor3& dPdV = ipvcur->getRefTodPdV();
        SVector3  fluxT;
	SVector3& fluxV= ipvcur->getRefToFluxV();
        STensor3 dqdgradT;
	STensor3 dqdgradV;
	STensor3& dedgradV= ipvcur->getRefTodFluxVdGradV();
	STensor3& dedgradT= ipvcur->getRefTodFluxVdGradT();
        SVector3  dqdT;
	SVector3  dqdV;
	SVector3& dedV= ipvcur->getRefTodFluxVdV();
	SVector3& dedT= ipvcur->getRefTodFluxVdT();
        STensor33 dqdF;
	STensor33& dedF= ipvcur->getRefTodFluxVdF();
        STensor3&  P   = ipvcur->getRefToFirstPiolaKirchhoffStress();
        STensor43& Tangent=ipvcur->getRefToTangentModuli();
	double &w      = ipvcur->getRefToThermalSource();
	double &dwdT   = ipvcur->getRefTodThermalSourcedT();
	STensor3& dwdf = ipvcur->getRefTodThermalSourcedF();

	ipvcur->setRefToTherConductivity(this->K10);
        ipvcur->setRefToCrossTherConductivity(this->K20);
	ipvcur->setRefToElecConductivity(this->L10);
        ipvcur->setRefToCrossElecConductivity(this->L20);
	STensor3& dk10dT = ipvcur->getRefTodTherConductivitydT();
	STensor3& dk20dT = ipvcur->getRefTodCrossTherConductivitydT();
	STensor3& dl10dT = ipvcur->getRefTodElecConductivitydT();
	STensor3& dl20dT = ipvcur->getRefTodCrossElecConductivitydT();
	STensor3& dk10dv = ipvcur->getRefTodTherConductivitydv();
	STensor3& dk20dv = ipvcur->getRefTodCrossTherConductivitydv();
	STensor3& dl10dv = ipvcur->getRefTodElecConductivitydv();
	STensor3& dl20dv = ipvcur->getRefTodCrossElecConductivitydv();
	STensor43& dl10dF=ipvcur->getRefTodElecConductivitydF();
	STensor43& dl20dF=ipvcur->getRefTodCrossElecConductivitydF();
	STensor43& dk10dF=ipvcur->getRefTodTherConductivitydF();
	STensor43& dk20dF=ipvcur->getRefTodCrossTherConductivitydF();
	SVector3 &fluxjy   = ipvcur->getRefToFluxEnergy();
	SVector3 &djydV    = ipvcur->getRefTodFluxEnergydV();
	STensor3 &djydgradV= ipvcur->getRefTodFluxEnergydGradV();
	SVector3 &djydT    = ipvcur->getRefTodFluxEnergydT();
	STensor3 &djydgradT= ipvcur->getRefTodFluxEnergydGradT();
        STensor33 &djydF   = ipvcur->getRefTodFluxEnergydF();

	/*ipvcur->setRefToEnergyConductivity(this->jy1);
	STensor3 &djy1dT   = ipvcur->getRefTodEnergyConductivitydT();
	STensor3 &djy1dV   = ipvcur->getRefTodEnergyConductivitydV();
	STensor43 &djy1dF  = ipvcur->getRefTodEnergyConductivitydF();*/


	STensor3 &djydgradVdT= ipvcur->getRefTodFluxEnergydGradVdT();
	STensor3 &djydgradVdV= ipvcur->getRefTodFluxEnergydGradVdV();
	STensor43 &djydgradVdF=ipvcur->getRefTodFluxEnergydGradVdF();
	STensor3 &djydgradTdT= ipvcur->getRefTodFluxEnergydGradTdT();
	STensor3 &djydgradTdV= ipvcur->getRefTodFluxEnergydGradTdV();
	STensor43 &djydgradTdF=ipvcur->getRefTodFluxEnergydGradTdF();


 /* IPElecSMP* q1 = ipvcur->getIPElecSMP();
  const IPElecSMP* q0 = ipvprev->getIPElecSMP();*/

  IPElecSMP* q1 = ipvcur->getIPElecSMP();
  const IPElecSMP* q0 = ipvprev->getIPElecSMP();

  _lawETMSMP->constitutive(  F0,Fn,P, q0, q1, Tangent, T0, temperature, voltage0,voltage,gradT, gradV, fluxT,fluxV, dPdT,dPdV, dqdgradT,dqdT, dqdgradV,dqdV,
			      dedV,dedgradV,dedT,dedgradT,dqdF,dedF,w,dwdT,dwdf,L10,L20,K10,K20,dl10dT,dl20dT,dk10dT,dk20dT,dl10dv,dl20dv,dk10dv,dk20dv,dl10dF,dl20dF,dk10dF,dk20dF
			      ,fluxjy,djydV,djydgradV,djydT,djydgradT,djydF,djydgradVdT,djydgradVdV,djydgradVdF,djydgradTdT,djydgradTdV,djydgradTdF,stiff);



 ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);
 ipvcur->setRefToStiff_alphaDilatation(this->Stiff_alphaDilatation);
}
void mlawElecSMPDG3DMaterialLaw::setStrainOrder(const int order ){
  _lawETMSMP->setStrainOrder(order);
};
void mlawElecSMPDG3DMaterialLaw::setMechanism2(bool mechanism2)
{
  _lawETMSMP->setMechanism2(mechanism2);
};


// Generic ThermoMechanics law

GenericThermoMechanicsDG3DMaterialLaw::GenericThermoMechanicsDG3DMaterialLaw(const int num, const double rho, const double alpha,
                                                                             const double beta, const double gamma ,const double t0,const double Kx,const double Ky, const double Kz,
                                                                             const double alphax,const double alphay,const double alphaz,const double cp) :
                                                                             dG3DMaterialLaw(num,rho,true)

{
    _lawGenericTM = new mlawGenericTM(num,alpha,beta,
                                      gamma,t0,Kx,Ky,Kz,alphax,alphay,alphaz,cp);


}

GenericThermoMechanicsDG3DMaterialLaw::GenericThermoMechanicsDG3DMaterialLaw(const GenericThermoMechanicsDG3DMaterialLaw &source) :
dG3DMaterialLaw(source)
{
    _lawGenericTM = NULL;
    if (source._lawGenericTM != NULL){
        _lawGenericTM  = new mlawGenericTM(*(source._lawGenericTM));
    }
    linearK   = source.linearK;
    Stiff_alphaDilatation=source.Stiff_alphaDilatation;

}

void GenericThermoMechanicsDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
    // check interface element
    bool inter=true;
    const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
    if(iele==NULL) inter=false;

    IPGenericTM* ipvimeca=NULL;
    _lawGenericTM->createIPVariable(ipvimeca, hasBodyForce, ele,nbFF_, GP, gpt);
    IPGenericTM* ipv1meca=NULL;
    _lawGenericTM->createIPVariable(ipv1meca, hasBodyForce, ele,nbFF_, GP, gpt);
    IPGenericTM* ipv2meca=NULL;
    _lawGenericTM->createIPVariable(ipv2meca, hasBodyForce, ele,nbFF_, GP, gpt);

    IPVariable* ipvi = new  GenericThermoMechanicsDG3DIPVariable(ipvimeca,_lawGenericTM->getInitialTemperature(),hasBodyForce,inter);
    IPVariable* ipv1 = new  GenericThermoMechanicsDG3DIPVariable(ipv1meca,_lawGenericTM->getInitialTemperature(),hasBodyForce,inter);
    IPVariable* ipv2 = new  GenericThermoMechanicsDG3DIPVariable(ipv2meca,_lawGenericTM->getInitialTemperature(),hasBodyForce,inter);

    if(ips != NULL) delete ips;
    ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void GenericThermoMechanicsDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
    if(ipv !=NULL) delete ipv;
    bool inter=true;
    const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
    if(iele == NULL){
        inter=false;
    }

    IPGenericTM* ipvimeca =NULL;
    _lawGenericTM->createIPVariable(ipvimeca, hasBodyForce, ele,nbFF_, GP, gpt);
    ipv = new  GenericThermoMechanicsDG3DIPVariable(ipvimeca,_lawGenericTM->getInitialTemperature(),hasBodyForce,inter);

    dynamic_cast<GenericThermoMechanicsDG3DIPVariable*>(ipv)->setIpMeca(ipvimeca->getRefToIpMeca());
}

double GenericThermoMechanicsDG3DMaterialLaw::getExtraDofStoredEnergyPerUnitField(double T) const
{
    return _lawGenericTM->getExtraDofStoredEnergyPerUnitField(T);
}

double GenericThermoMechanicsDG3DMaterialLaw::getInitialExtraDofStoredEnergyPerUnitField() const{
    return _lawGenericTM->getInitialExtraDofStoredEnergyPerUnitField();
};

void GenericThermoMechanicsDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
    /* get ipvariable */
    GenericThermoMechanicsDG3DIPVariable* ipvcur;
    const GenericThermoMechanicsDG3DIPVariable* ipvprev;

    FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
    if(ipvtmp !=NULL)
    {

        ipvcur = static_cast<GenericThermoMechanicsDG3DIPVariable*>(ipvtmp->getIPvBulk());
        const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
        ipvprev = static_cast<const GenericThermoMechanicsDG3DIPVariable*>(ipvtmp2->getIPvBulk());
    }
    else
    {
        ipvcur = static_cast<GenericThermoMechanicsDG3DIPVariable*>(ipv);
        ipvprev = static_cast<const GenericThermoMechanicsDG3DIPVariable*>(ipvp);
    }

    _lawGenericTM->checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());
};

void GenericThermoMechanicsDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
    /* get ipvariable */
    GenericThermoMechanicsDG3DIPVariable* ipvcur;
    const GenericThermoMechanicsDG3DIPVariable* ipvprev;

    FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
    if(ipvtmp !=NULL)
    {

        ipvcur = static_cast<GenericThermoMechanicsDG3DIPVariable*>(ipvtmp->getIPvBulk());
        const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
        ipvprev = static_cast<const GenericThermoMechanicsDG3DIPVariable*>(ipvtmp2->getIPvBulk());
    }
    else
    {
        ipvcur = static_cast<GenericThermoMechanicsDG3DIPVariable*>(ipv);
        ipvprev = static_cast<const GenericThermoMechanicsDG3DIPVariable*>(ipvp);
    }

    /* strain xyz */
    const STensor3& Fn = ipvcur->getConstRefToDeformationGradient();
    const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();
    const double T = ipvcur->getConstRefToTemperature();
    const double T0 = ipvprev->getConstRefToTemperature();
    const SVector3& gradT= ipvcur->getConstRefToGradT();
    STensor3& dPdT=ipvcur->getRefTodPdT();
    SVector3& fluxT=ipvcur->getRefToThermalFlux();
    STensor3& dqdgradT=ipvcur->getRefTodThermalFluxdGradT();
    SVector3& dqdT=ipvcur->getRefTodThermalFluxdT();
    STensor33& dqdF=ipvcur->getRefTodThermalFluxdF();

    STensor3& P=ipvcur->getRefToFirstPiolaKirchhoffStress();
    const STensor3 & P0 = ipvprev->getConstRefToFirstPiolaKirchhoffStress();
    STensor43& Tangent=ipvcur->getRefToTangentModuli();
    double &w = ipvcur->getRefToThermalSource();
    double &dwdt = ipvcur->getRefTodThermalSourcedField();
    STensor3 &dwdf = ipvcur->getRefTodThermalSourcedF();
    double& cp = ipvcur->getRefToExtraDofFieldCapacityPerUnitField()(0);

    double&  mechSource =  ipvcur->getRefToMechanicalSource()(0);
    double& dmechSourcedt = ipvcur->getRefTodMechanicalSourcedField()(0,0);
    STensor3 & dmechSourcedf = ipvcur->getRefTodMechanicalSourcedF()[0];


    IPVariable* q1 = ipvcur->getInternalData();
    const IPVariable* q0 = ipvprev->getInternalData();

    _lawGenericTM->constitutive(F0,Fn,P,P0,q0,q1,Tangent,T0,T,gradT,fluxT,dPdT,dqdgradT,dqdT,dqdF,stiff,w,dwdt,dwdf,
                                mechSource, dmechSourcedt, dmechSourcedf);
    ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);
    ipvcur->setRefToLinearConductivity(this->linearK);
    ipvcur->setRefToStiff_alphaDilatation(this->Stiff_alphaDilatation);

    cp = _lawGenericTM->getExtraDofStoredEnergyPerUnitField(T);
}

void GenericThermoMechanicsDG3DMaterialLaw::setLawForCp(const scalarFunction& funcCp)
{
    _lawGenericTM->setLawForCp(funcCp);
}
const materialLaw &GenericThermoMechanicsDG3DMaterialLaw::getConstRefMechanicalMaterialLaw() const
{
    return _lawGenericTM->getConstRefMechanicalMaterialLaw();
}
materialLaw &GenericThermoMechanicsDG3DMaterialLaw::getRefMechanicalMaterialLaw()
{
    return _lawGenericTM->getRefMechanicalMaterialLaw();
}
void GenericThermoMechanicsDG3DMaterialLaw::setMechanicalMaterialLaw(const dG3DMaterialLaw *mlaw)
{
    _lawGenericTM->setMechanicalMaterialLaw(mlaw->getConstNonLinearSolverMaterialLaw());

    linearK     = _lawGenericTM->getConductivityTensor();
    //_lawGenericTM->getStiff_alphaDilatation(Stiff_alphaDilatation);
    //_lawGenericTM->ElasticStiffness(&elasticStiffness);
    elasticStiffness=mlaw->elasticStiffness;
    //Here fill Stiff_alphaDilatation

    for(int i=0;i<3;i++)
    {
        for(int j=0;j<3;j++)
        {
            Stiff_alphaDilatation(i,j)=0.;
            for(int k=0;k<3;k++)
            {
                for(int l=0;l<3;l++)
                {
                    Stiff_alphaDilatation(i,j)+=elasticStiffness(i,j,k,l)*_lawGenericTM->getAlphaDilatation()(k,l);
                }
            }
        }
    }

}
void GenericThermoMechanicsDG3DMaterialLaw::setApplyReferenceF(const bool rf)
{
    _lawGenericTM->setApplyReferenceF(rf);
}


// Generic ThermoMechanics law

GenericResidualMechanicsDG3DMaterialLaw::GenericResidualMechanicsDG3DMaterialLaw(const int num, const double rho, const char* inputfilename, const bool init) :
                                                                             dG3DMaterialLaw(num,rho,true)

{
    _lawGenericRM = new mlawGenericRM(num, inputfilename, init);


}

GenericResidualMechanicsDG3DMaterialLaw::GenericResidualMechanicsDG3DMaterialLaw(const GenericResidualMechanicsDG3DMaterialLaw &source) :
dG3DMaterialLaw(source)
{
    _lawGenericRM = NULL;
    if (source._lawGenericRM != NULL){
        _lawGenericRM  = new mlawGenericRM(*(source._lawGenericRM));
    }

}

void GenericResidualMechanicsDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
    // check interface element
    bool inter=true;
    const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
    if(iele==NULL) inter=false;

    IPGenericRM* ipvimeca=NULL;
    _lawGenericRM->createIPVariable(ipvimeca, hasBodyForce, ele,nbFF_, GP, gpt);
    IPGenericRM* ipv1meca=NULL;
    _lawGenericRM->createIPVariable(ipv1meca, hasBodyForce, ele,nbFF_, GP, gpt);
    IPGenericRM* ipv2meca=NULL;
    _lawGenericRM->createIPVariable(ipv2meca, hasBodyForce, ele,nbFF_, GP, gpt);

    IPVariable* ipvi = new  GenericResidualMechanicsDG3DIPVariable(ipvimeca,hasBodyForce,inter);
    IPVariable* ipv1 = new  GenericResidualMechanicsDG3DIPVariable(ipv1meca,hasBodyForce,inter);
    IPVariable* ipv2 = new  GenericResidualMechanicsDG3DIPVariable(ipv2meca,hasBodyForce,inter);

    if(ips != NULL) delete ips;
    ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void GenericResidualMechanicsDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
    if(ipv !=NULL) delete ipv;
    bool inter=true;
    const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
    if(iele == NULL){
        inter=false;
    }

    IPGenericRM* ipvimeca =NULL;
    _lawGenericRM->createIPVariable(ipvimeca, hasBodyForce, ele,nbFF_, GP, gpt);
    ipv = new  GenericResidualMechanicsDG3DIPVariable(ipvimeca,hasBodyForce,inter);

    dynamic_cast<GenericResidualMechanicsDG3DIPVariable*>(ipv)->setIpMeca(ipvimeca->getRefToIpMeca());
}

void GenericResidualMechanicsDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
    /* get ipvariable */
    GenericResidualMechanicsDG3DIPVariable* ipvcur;
    const GenericResidualMechanicsDG3DIPVariable* ipvprev;

    FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
    if(ipvtmp !=NULL)
    {

         ipvcur = static_cast<GenericResidualMechanicsDG3DIPVariable*>(ipvtmp->getIPvBulk());
         const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
         ipvprev = static_cast<const GenericResidualMechanicsDG3DIPVariable*>(ipvtmp2->getIPvBulk());
    }
    else
    {
         ipvcur = static_cast<GenericResidualMechanicsDG3DIPVariable*>(ipv);
         ipvprev = static_cast<const GenericResidualMechanicsDG3DIPVariable*>(ipvp);
    }
    ipvcur = static_cast<GenericResidualMechanicsDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const GenericResidualMechanicsDG3DIPVariable*>(ipvp);

    _lawGenericRM->checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());
};

void GenericResidualMechanicsDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
    /* get ipvariable */
    GenericResidualMechanicsDG3DIPVariable* ipvcur;
    const GenericResidualMechanicsDG3DIPVariable* ipvprev;

    FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
    if(ipvtmp !=NULL)
    {

         ipvcur = static_cast<GenericResidualMechanicsDG3DIPVariable*>(ipvtmp->getIPvBulk());
         const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
         ipvprev = static_cast<const GenericResidualMechanicsDG3DIPVariable*>(ipvtmp2->getIPvBulk());
    }
    else
    {
         ipvcur = static_cast<GenericResidualMechanicsDG3DIPVariable*>(ipv);
         ipvprev = static_cast<const GenericResidualMechanicsDG3DIPVariable*>(ipvp);
    }

    ipvcur = static_cast<GenericResidualMechanicsDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const GenericResidualMechanicsDG3DIPVariable*>(ipvp);


    /* strain xyz */
    const STensor3& Fn = ipvcur->getConstRefToDeformationGradient();
    const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

    STensor3& P=ipvcur->getRefToFirstPiolaKirchhoffStress();
    const STensor3 & P0 = ipvprev->getConstRefToFirstPiolaKirchhoffStress();
    STensor43& Tangent=ipvcur->getRefToTangentModuli();
  
    IPVariable* q1 = ipvcur->getInternalData();
    const IPVariable* q0 = ipvprev->getInternalData();

    //static STensor63 dCalgdeps;
    /* compute stress */
    STensor43& elasticL = ipvcur->getRefToElasticTangentModuli();
    _lawGenericRM->constitutive(F0,Fn,P,q0,q1,Tangent,stiff,&elasticL,dTangent,ipvcur->getPtrTodCmdFm());
   //_lawGenericRM->constitutive(F0,Fn,P,P0,q0,q1,Tangent,T0,T,gradT,fluxT,dPdT,dqdgradT,dqdT,dqdF,stiff,w,dwdt,dwdf,mechSource, dmechSourcedt, dmechSourcedf);

 

    ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);

}

void GenericResidualMechanicsDG3DMaterialLaw::setLawForFRes(const scalarFunction& funcCp)
{
    _lawGenericRM->setLawForFRes(funcCp);
}
const materialLaw &GenericResidualMechanicsDG3DMaterialLaw::getConstRefMechanicalMaterialLaw() const
{
    return _lawGenericRM->getConstRefMechanicalMaterialLaw();
}
materialLaw &GenericResidualMechanicsDG3DMaterialLaw::getRefMechanicalMaterialLaw()
{
    return _lawGenericRM->getRefMechanicalMaterialLaw();
}
void GenericResidualMechanicsDG3DMaterialLaw::setMechanicalMaterialLaw(const dG3DMaterialLaw *mlaw)
{
    _lawGenericRM->setMechanicalMaterialLaw(mlaw->getConstNonLinearSolverMaterialLaw());

    //_lawGenericRM->ElasticStiffness(&elasticStiffness);
    elasticStiffness=mlaw->elasticStiffness;
    
}

// ElecMag GenericThermoMech law

ElecMagGenericThermoMechanicsDG3DMaterialLaw::ElecMagGenericThermoMechanicsDG3DMaterialLaw(const int num, const double rho,
                                                                                           const double alpha, const double beta, const double gamma,
                                                                                           const  double lx,const double ly,const double lz,
                                                                                           const double seebeck, const double v0,
                                                                                           const double mu_x, const double mu_y, const double mu_z,
                                                                                           const double epsilon_4,const double epsilon_5,const double epsilon_6,
                                                                                           const double mu_7,const double mu_8,const double mu_9,
                                                                                           const double A0_x, const double A0_y, const double A0_z,
                                                                                           const double Irms, const double freq, const unsigned int nTurnsCoil,
                                                                                           const double coilLength_x, const double coilLength_y,
                                                                                           const double coilLength_z, const double coilWidth,
                                                                                           const bool useFluxT, const bool evaluateCurlField,
                                                                                           const bool evaluateTemperature):
                                                                                           dG3DMaterialLaw(num, rho, true)
{
    _lawElecMagGenericThermoMech = new mlawElecMagGenericThermoMech(num, alpha, beta, gamma, lx, ly, lz,
                                                                    seebeck, v0, mu_x, mu_y, mu_z,
                                                                    epsilon_4, epsilon_5, epsilon_6,
                                                                    mu_7, mu_8, mu_9,
                                                                    A0_x, A0_y, A0_z, Irms, freq, nTurnsCoil,
                                                                    coilLength_x, coilLength_y, coilLength_z,
                                                                    coilWidth, useFluxT, evaluateCurlField,
                                                                    evaluateTemperature);

    this->_useFluxT = useFluxT;
    this->_evaluateCurlField = evaluateCurlField;
    this->_evaluateTemperature = evaluateTemperature;
}

ElecMagGenericThermoMechanicsDG3DMaterialLaw::ElecMagGenericThermoMechanicsDG3DMaterialLaw(const ElecMagGenericThermoMechanicsDG3DMaterialLaw &source):
        dG3DMaterialLaw(source)
{
    _lawElecMagGenericThermoMech = NULL;
    if (source._lawElecMagGenericThermoMech != NULL){
        _lawElecMagGenericThermoMech  = new mlawElecMagGenericThermoMech(*(source._lawElecMagGenericThermoMech));
    }

    _useFluxT = source._useFluxT;
    _evaluateCurlField = source._evaluateCurlField;
    _evaluateTemperature = source._evaluateTemperature;
}

void ElecMagGenericThermoMechanicsDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce,
                                                                 const bool* state_,const MElement *ele,
                                                                 const int nbFF_, const IntPt *GP, const int gpt) const
{
    // check interface element
    bool inter=true;
    const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
    if(iele==NULL) inter=false;

    IPElecMagGenericThermoMech* ipvithermomeca=NULL;
    _lawElecMagGenericThermoMech->createIPVariable(ipvithermomeca, hasBodyForce, ele,nbFF_, GP, gpt);
    IPElecMagGenericThermoMech* ipv1thermomeca=NULL;
    _lawElecMagGenericThermoMech->createIPVariable(ipv1thermomeca, hasBodyForce, ele,nbFF_, GP, gpt);
    IPElecMagGenericThermoMech* ipv2thermomeca=NULL;
    _lawElecMagGenericThermoMech->createIPVariable(ipv2thermomeca, hasBodyForce, ele,nbFF_, GP, gpt);

    IPVariable* ipvi = new  ElecMagGenericThermoMechanicsDG3DIPVariable(ipvithermomeca,
                                                                        _lawElecMagGenericThermoMech->getInitialTemperature(),
                                                                        _lawElecMagGenericThermoMech->getInitialVoltage(),
                                                                        _lawElecMagGenericThermoMech->getInitialMagPotential(),
                                                                        hasBodyForce,inter);
    IPVariable* ipv1 = new  ElecMagGenericThermoMechanicsDG3DIPVariable(ipv1thermomeca,
                                                                        _lawElecMagGenericThermoMech->getInitialTemperature(),
                                                                        _lawElecMagGenericThermoMech->getInitialVoltage(),
                                                                        _lawElecMagGenericThermoMech->getInitialMagPotential(),
                                                                        hasBodyForce,inter);
    IPVariable* ipv2 = new  ElecMagGenericThermoMechanicsDG3DIPVariable(ipv2thermomeca,
                                                                        _lawElecMagGenericThermoMech->getInitialTemperature(),
                                                                        _lawElecMagGenericThermoMech->getInitialVoltage(),
                                                                        _lawElecMagGenericThermoMech->getInitialMagPotential(),
                                                                        hasBodyForce,inter);

    if(ips != NULL) delete ips;
    ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void ElecMagGenericThermoMechanicsDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
    if(ipv !=NULL) delete ipv;
    bool inter=true;
    const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
    if(iele == NULL){
        inter=false;
    }

    IPElecMagGenericThermoMech* ipvithermomeca =NULL;
    _lawElecMagGenericThermoMech->createIPVariable(ipvithermomeca, hasBodyForce, ele,nbFF_, GP, gpt);
    ipv = new  ElecMagGenericThermoMechanicsDG3DIPVariable(ipvithermomeca,
                                                           _lawElecMagGenericThermoMech->getInitialTemperature(),
                                                           _lawElecMagGenericThermoMech->getInitialVoltage(),
                                                           _lawElecMagGenericThermoMech->getInitialMagPotential(),
                                                           hasBodyForce,inter);

    dynamic_cast<ElecMagGenericThermoMechanicsDG3DIPVariable*>(ipv)->setIpThermoMech(ipvithermomeca->getRefToIpThermoMech());
}

double ElecMagGenericThermoMechanicsDG3DMaterialLaw::getExtraDofStoredEnergyPerUnitField(double T) const
{
    return _lawElecMagGenericThermoMech->getExtraDofStoredEnergyPerUnitField(T);
}

double ElecMagGenericThermoMechanicsDG3DMaterialLaw::getInitialExtraDofStoredEnergyPerUnitField() const{
    return _lawElecMagGenericThermoMech->getInitialExtraDofStoredEnergyPerUnitField();
};

void ElecMagGenericThermoMechanicsDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const
{
    /* get ipvariable */
    ElecMagGenericThermoMechanicsDG3DIPVariable* ipvcur;
    const ElecMagGenericThermoMechanicsDG3DIPVariable* ipvprev;

    FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
    if(ipvtmp !=NULL)
    {

        ipvcur = static_cast<ElecMagGenericThermoMechanicsDG3DIPVariable*>(ipvtmp->getIPvBulk());
        const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
        ipvprev = static_cast<const ElecMagGenericThermoMechanicsDG3DIPVariable*>(ipvtmp2->getIPvBulk());
    }
    else
    {
        ipvcur = static_cast<ElecMagGenericThermoMechanicsDG3DIPVariable*>(ipv);
        ipvprev = static_cast<const ElecMagGenericThermoMechanicsDG3DIPVariable*>(ipvp);
    }

    _lawElecMagGenericThermoMech->checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());
}

void ElecMagGenericThermoMechanicsDG3DMaterialLaw::stress(IPVariable*ipv, const IPVariable*ipvp,
                                                          const bool stiff, const bool checkfrac,
                                                          const bool dTangent)
{
    /* get ipvariable */
    ElecMagGenericThermoMechanicsDG3DIPVariable* ipvcur;
    const ElecMagGenericThermoMechanicsDG3DIPVariable* ipvprev;

    FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
    if(ipvtmp !=NULL)
    {
        ipvcur = static_cast<ElecMagGenericThermoMechanicsDG3DIPVariable*>(ipvtmp->getIPvBulk());
        const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
        ipvprev = static_cast<const ElecMagGenericThermoMechanicsDG3DIPVariable*>(ipvtmp2->getIPvBulk());
    }
    else
    {
        ipvcur = static_cast<ElecMagGenericThermoMechanicsDG3DIPVariable*>(ipv);
        ipvprev = static_cast<const ElecMagGenericThermoMechanicsDG3DIPVariable*>(ipvp);
    }

    /* strain xyz */
    const STensor3& Fn = ipvcur->getConstRefToDeformationGradient();
    const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();
    const double temperature = ipvcur->getConstRefToTemperature();
    const double T0 = ipvprev->getConstRefToTemperature();
    const double voltage = ipvcur->getConstRefToVoltage();
    const double voltage0 = ipvprev->getConstRefToVoltage();
    const SVector3& gradT= ipvcur->getConstRefToGradT();
    const SVector3& gradV= ipvcur->getConstRefToGradV();

    SVector3& fluxje=ipvcur->getRefToFluxje();
    STensor3& dedgradV=ipvcur->getRefTodFluxjedGradV();
    STensor3& dedgradT=ipvcur->getRefTodFluxjedGradT();
    SVector3& dedV=ipvcur->getRefTodFluxjedV();
    SVector3& dedT=ipvcur->getRefTodFluxjedT();
    STensor33& dedF=ipvcur->getRefTodFluxjedF();
    SVector3& D=ipvcur->getRefToElecDisplacement();
    STensor3& dDdgradV=ipvcur->getRefTodElecDisplacementdGradV();
    STensor3& dDdgradT=ipvcur->getRefTodElecDisplacementdGradT();
    SVector3& dDdV=ipvcur->getRefTodElecDisplacementdV();
    SVector3& dDdT=ipvcur->getRefTodElecDisplacementdT();
    STensor33& dDdF=ipvcur->getRefTodElecDispldF();
    STensor3& P=ipvcur->getRefToFirstPiolaKirchhoffStress();
    const STensor3 & P0 = ipvprev->getConstRefToFirstPiolaKirchhoffStress();
    STensor3& dPdT=ipvcur->getRefTodPdT();
    STensor3& dPdV=ipvcur->getRefTodPdV();
    STensor33& dPdE=ipvcur->getRefTodPdGradV();
    STensor43& Tangent=ipvcur->getRefToTangentModuli();
    double &w_T = ipvcur->getRefToThermalSource();
    double &dw_TdT = ipvcur->getRefTodThermalSourcedT();
    double &dw_TdV = ipvcur->getRefTodThermalSourcedV();
    STensor3& dw_TdF = ipvcur->getRefTodThermalSourcedF();

    double &w_V = ipvcur->getRefToVoltageSource();
    double &dw_VdT = ipvcur->getRefTodVoltageSourcedT();
    double &dw_VdV = ipvcur->getRefTodVoltageSourcedV();
    STensor3& dw_VdF = ipvcur->getRefTodVoltageSourcedF();

    double&  mechSource =  ipvcur->getRefToMechanicalSource()(0);
    double& dmechSourcedt = ipvcur->getRefTodMechanicalSourcedField()(0,0);
    double& dmechSourcedV = ipvcur->getRefTodMechanicalSourcedField()(0,1);
    STensor3 & dmechSourcedf = ipvcur->getRefTodMechanicalSourcedF()[0];

    ipvcur->setRefToTherConductivity(this->lineark10);
    ipvcur->setRefToCrossTherConductivity(this->lineark20);
    ipvcur->setRefToElecConductivity(this->linearl10);
    ipvcur->setRefToCrossElecConductivity(this->linearl20);
    STensor3& dk10dT = ipvcur->getRefTodTherConductivitydT();
    STensor3& dk20dT = ipvcur->getRefTodCrossTherConductivitydT();
    STensor3& dl10dT = ipvcur->getRefTodElecConductivitydT();
    STensor3& dl20dT = ipvcur->getRefTodCrossElecConductivitydT();
    STensor3& dk10dv = ipvcur->getRefTodTherConductivitydv();
    STensor3& dk20dv = ipvcur->getRefTodCrossTherConductivitydv();
    STensor3& dl10dv = ipvcur->getRefTodElecConductivitydv();
    STensor3& dl20dv = ipvcur->getRefTodCrossElecConductivitydv();
    STensor43& dl10dF=ipvcur->getRefTodElecConductivitydF();
    STensor43& dl20dF=ipvcur->getRefTodCrossElecConductivitydF();
    STensor43& dk10dF=ipvcur->getRefTodTherConductivitydF();
    STensor43& dk20dF=ipvcur->getRefTodCrossTherConductivitydF();

    SVector3 Vector3Tmp(0.0);
    STensor3 Tensor3Tmp(0.0);
    STensor33 Tensor33Tmp(0.0);
    STensor43 Tensor43Tmp(0.0);

    SVector3* fluxT=&Vector3Tmp;
    STensor3* dqdgradT=&Tensor3Tmp;
    STensor3* dqdgradV=&Tensor3Tmp;
    SVector3* dqdT=&Vector3Tmp;
    SVector3* dqdV=&Vector3Tmp;
    STensor33* dqdF=&Tensor33Tmp;
    STensor3* dfluxTdA=&Tensor3Tmp;
    STensor3* dfluxTdB=&Tensor3Tmp;

    SVector3 *fluxjy=&Vector3Tmp;
    SVector3 *djydV=&Vector3Tmp;
    STensor3 *djydgradV=&Tensor3Tmp;
    SVector3 *djydT=&Vector3Tmp;
    STensor3 *djydgradT=&Tensor3Tmp;
    STensor33 *djydF=&Tensor33Tmp;
    STensor3 * djydA =&Tensor3Tmp;
    STensor3 * djydB =&Tensor3Tmp;

    STensor3 *djydgradVdT=&Tensor3Tmp;
    STensor3 *djydgradVdV=&Tensor3Tmp;
    STensor43 *djydgradVdF=&Tensor43Tmp;
    STensor3 *djydgradTdT=&Tensor3Tmp;
    STensor3 *djydgradTdV=&Tensor3Tmp;
    STensor43 *djydgradTdF=&Tensor43Tmp;

    if (this->_useFluxT)
    {
        fluxT=&(ipvcur->getRefToFluxT());
        dqdgradT=&(ipvcur->getRefTodFluxTdGradT());
        dqdgradV=&(ipvcur->getRefTodFluxTdGradV());
        dqdT=&(ipvcur->getRefTodFluxTdT());
        dqdV=&(ipvcur->getRefTodFluxTdV());
        dqdF=&(ipvcur->getRefTodFluxTdF());
        dfluxTdA=&(ipvcur->getRefTodFluxTdMagneticVectorPotential());
        dfluxTdB=&(ipvcur->getRefTodFluxTdMagneticVectorCurl());
    }
    else
    {
        fluxjy=&(ipvcur->getRefToFluxEnergy());
        djydV=&(ipvcur->getRefTodFluxEnergydV());
        djydgradV=&(ipvcur->getRefTodFluxEnergydGradV());
        djydT=&(ipvcur->getRefTodFluxEnergydT());
        djydgradT=&(ipvcur->getRefTodFluxEnergydGradT());
        djydF=&(ipvcur->getRefTodFluxEnergydF());
        djydA = &(ipvcur->getRefTodFluxEnergydMagneticVectorPotential());
        djydB = &(ipvcur->getRefTodFluxEnergydMagneticVectorCurl());

        djydgradVdT= &(ipvcur->getRefTodFluxEnergydGradVdT());
        djydgradVdV= &(ipvcur->getRefTodFluxEnergydGradVdV());
        djydgradVdF=&(ipvcur->getRefTodFluxEnergydGradVdF());
        djydgradTdT= &(ipvcur->getRefTodFluxEnergydGradTdT());
        djydgradTdV= &(ipvcur->getRefTodFluxEnergydGradTdV());
        djydgradTdF=&(ipvcur->getRefTodFluxEnergydGradTdF());
    }

    IPVariable* q1 = ipvcur->getInternalData();
    const IPVariable* q0 = ipvprev->getInternalData();

    const SVector3 & An = ipvcur->getConstRefToMagneticVectorPotential();
    const SVector3 & A0 = ipvprev->getConstRefToMagneticVectorPotential();
    const SVector3 & B = ipvcur->getConstRefToMagneticInduction();
    SVector3 & H = ipvcur->getRefToMagneticField();
    SVector3 & js0 = ipvcur->getRefToinductorSourceVectorField();
    SVector3 dBdT;
    STensor3 dBdGradT;
    SVector3 dBdV;
    STensor3 dBdGradV;
    STensor33 dBdF;
    STensor3 dBdA;
    STensor3 &dfluxjedA = ipvcur->getRefTodFluxjedMagneticVectorPotential();
    STensor3 & dfluxjedB = ipvcur->getRefTodFluxjedMagneticVectorCurl();
    STensor3 &dDdA = ipvcur->getRefTodElecDisplacementdMagneticVectorPotential();
    STensor3 & dDdB = ipvcur->getRefTodElecDisplacementdMagneticVectorCurl();
    SVector3 & dw_TdA = ipvcur->getRefTodFieldSourcedMagneticVectorPotential();
    SVector3 & dw_TdB = ipvcur->getRefTodFieldSourcedMagneticVectorCurl();
    SVector3 & dmechSourcedB = ipvcur->getRefTodMechanicalSourcedMagneticVectorCurl();
    SVector3 & sourceVectorField = ipvcur->getRefTosourceVectorField(); // Magnetic source vector field (T, grad V, F)
    SVector3 & dsourceVectorFielddt = ipvcur->getRefTodsourceVectorFielddt();
    double & ThermalEMFieldSource = ipvcur->getRefToThermalEMFieldSource();
    double & VoltageEMFieldSource = ipvcur->getRefToVoltageEMFieldSource();
    STensor3 & dHdA = ipvcur->getRefTodMagneticFielddMagneticVectorPotential();
    STensor3 & dHdB = ipvcur->getRefTodMagneticFielddMagneticVectorCurl();
    STensor33 & dPdB = ipvcur->getRefTodPdMagneticVectorCurl();
    STensor33 & dHdF = ipvcur->getRefTodMagneticFielddF();
    STensor33 & dsourceVectorFielddF = ipvcur->getRefTodsourceVectorFielddF();
    SVector3 & mechFieldSource = ipvcur->getRefTomechanicalFieldSource(); // Electromagnetic force
    STensor3 & dmechFieldSourcedB = ipvcur->getRefTodMechanicalFieldSourcedMagneticVectorCurl();
    STensor33 & dmechFieldSourcedF = ipvcur->getRefTodmechanicalFieldSourcedF();
    SVector3 & dHdT = ipvcur->getRefTodMagneticFielddT();
    STensor3 & dHdgradT = ipvcur->getRefTodMagneticFielddGradT();
    SVector3 & dHdV = ipvcur->getRefTodMagneticFielddV();
    STensor3 & dHdgradV = ipvcur->getRefTodMagneticFielddGradV();
    SVector3 & dsourceVectorFielddT = ipvcur->getRefTodSourceVectorFielddT();
    STensor3 & dsourceVectorFielddgradT = ipvcur->getRefTodSourceVectorFielddGradT();
    SVector3 & dsourceVectorFielddV = ipvcur->getRefTodSourceVectorFielddV();
    STensor3 & dsourceVectorFielddgradV = ipvcur->getRefTodSourceVectorFielddGradV();
    STensor3 & dsourceVectorFielddA = ipvcur->getRefTodSourceVectorFielddMagneticVectorPotential();
    STensor3 & dsourceVectorFielddB = ipvcur->getRefTodSourceVectorFielddMagneticVectorCurl();
    SVector3 & dmechFieldSourcedT = ipvcur->getRefTodMechanicalFieldSourcedT();
    STensor3 & dmechFieldSourcedgradT = ipvcur->getRefTodMechanicalFieldSourcedGradT();
    SVector3 & dmechFieldSourcedV = ipvcur->getRefTodMechanicalFieldSourcedV();
    STensor3 & dmechFieldSourcedgradV = ipvcur->getRefTodMechanicalFieldSourcedGradV();
    SVector3 & dThermalEMFieldSourcedA = ipvcur->getRefTodThermalEMFieldSourcedMagneticVectorPotential();
    SVector3 & dThermalEMFieldSourcedB = ipvcur->getRefTodThermalEMFieldSourcedMagneticVectorCurl();
    STensor3 & dThermalEMFieldSourcedF = ipvcur->getRefTodThermalEMFieldSourcedF();
    double & dThermalEMFieldSourcedT = ipvcur->getRefTodThermalEMFieldSourcedT();
    SVector3 & dThermalEMFieldSourcedGradT = ipvcur->getRefTodThermalEMFieldSourcedGradT();
    double & dThermalEMFieldSourcedV = ipvcur->getRefTodThermalEMFieldSourcedV();
    SVector3 & dThermalEMFieldSourcedGradV = ipvcur->getRefTodThermalEMFieldSourcedGradV();
    SVector3 & dVoltageEMFieldSourcedA = ipvcur->getRefTodVoltageEMFieldSourcedMagneticVectorPotential();
    SVector3 & dVoltageEMFieldSourcedB = ipvcur->getRefTodVoltageEMFieldSourcedMagneticVectorCurl();
    STensor3 & dVoltageEMFieldSourcedF = ipvcur->getRefTodVoltageEMFieldSourcedF();
    double & dVoltageEMFieldSourcedT = ipvcur->getRefTodVoltageEMFieldSourcedT();
    SVector3 & dVoltageEMFieldSourcedGradT = ipvcur->getRefTodVoltageEMFieldSourcedGradT();
    double & dVoltageEMFieldSourcedV = ipvcur->getRefTodVoltageEMFieldSourcedV();
    SVector3 & dVoltageEMFieldSourcedGradV = ipvcur->getRefTodVoltageEMFieldSourcedGradV();

    _lawElecMagGenericThermoMech->constitutive(F0,Fn,P, P0, q0, q1, Tangent,T0, temperature,
                                               voltage0,voltage,gradT, gradV,*fluxT,fluxje,D,
                                               dPdT,dPdV, dPdE, *dqdgradT,*dqdT, *dqdgradV,*dqdV,*dqdF,
                                               dedgradT,dedT,dedgradV,dedV,dedF,
                                               dDdgradT,dDdT,dDdgradV,dDdV,dDdF,
                                               stiff, w_T,dw_TdT,dw_TdV,dw_TdF,w_V,dw_VdT,dw_VdV,dw_VdF,linearl10,linearl20,
                                               lineark10,lineark20,
                                               dl10dT,dl20dT,dk10dT,dk20dT,dl10dv,
                                               dl20dv,dk10dv,dk20dv,dl10dF,dl20dF,dk10dF,dk20dF,
                                               mechSource, dmechSourcedt, dmechSourcedV, dmechSourcedf,
                                               *fluxjy,*djydV,*djydgradV,*djydT,*djydgradT,
                                               *djydF,*djydA, *djydB, *djydgradVdT,*djydgradVdV,*djydgradVdF,
                                               *djydgradTdT,*djydgradTdV,*djydgradTdF,
                                               A0, An, B, H, js0, dBdT, dBdGradT, dBdV,
                                               dBdGradV, dBdF, dBdA, *dfluxTdA, dfluxjedA, dDdA, *dfluxTdB,
                                               dfluxjedB, dDdB, dw_TdA, dw_TdB, dmechSourcedB, sourceVectorField,
                                               dsourceVectorFielddt, ThermalEMFieldSource, VoltageEMFieldSource,
                                               dHdA, dHdB, dPdB,dHdF, dsourceVectorFielddF,
                                               mechFieldSource, dmechFieldSourcedB, dmechFieldSourcedF,
                                               dHdT, dHdgradT, dHdV, dHdgradV, dsourceVectorFielddT,
                                               dsourceVectorFielddgradT,dsourceVectorFielddV, dsourceVectorFielddgradV,
                                               dsourceVectorFielddA,dsourceVectorFielddB, dmechFieldSourcedT,
                                               dmechFieldSourcedgradT,dmechFieldSourcedV, dmechFieldSourcedgradV,
                                               dThermalEMFieldSourcedA,dThermalEMFieldSourcedB, dThermalEMFieldSourcedF,
                                               dThermalEMFieldSourcedT,dThermalEMFieldSourcedGradT, dThermalEMFieldSourcedV,
                                               dThermalEMFieldSourcedGradV,dVoltageEMFieldSourcedA, dVoltageEMFieldSourcedB,
                                               dVoltageEMFieldSourcedF,dVoltageEMFieldSourcedT, dVoltageEMFieldSourcedGradT,
                                               dVoltageEMFieldSourcedV,dVoltageEMFieldSourcedGradV);

    ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);
}

const materialLaw &ElecMagGenericThermoMechanicsDG3DMaterialLaw::getConstRefToThermoMechanicalMaterialLaw() const
{
    return _lawElecMagGenericThermoMech->getConstRefToThermoMechanicalMaterialLaw();
}
materialLaw &ElecMagGenericThermoMechanicsDG3DMaterialLaw::getRefToThermoMechanicalMaterialLaw()
{
    return _lawElecMagGenericThermoMech->getRefToThermoMechanicalMaterialLaw();
}
void ElecMagGenericThermoMechanicsDG3DMaterialLaw::setThermoMechanicalMaterialLaw(const dG3DMaterialLaw *mlaw)
{
    _lawElecMagGenericThermoMech->setThermoMechanicalMaterialLaw(mlaw->getConstNonLinearSolverMaterialLaw());

    elasticStiffness=mlaw->elasticStiffness;
}

void ElecMagGenericThermoMechanicsDG3DMaterialLaw::setApplyReferenceF(const bool rf)
{
    _lawElecMagGenericThermoMech->setApplyReferenceF(rf);
}

void ElecMagGenericThermoMechanicsDG3DMaterialLaw::setUseEMStress(const bool emStress)
{
    _lawElecMagGenericThermoMech->setUseEMStress(emStress);
}

void ElecMagGenericThermoMechanicsDG3DMaterialLaw::setEMFieldDependentShearModulus(const bool EM_ShearModulus)
{
    _lawElecMagGenericThermoMech->setEMFieldDependentShearModulus(EM_ShearModulus);
}

void ElecMagGenericThermoMechanicsDG3DMaterialLaw::setEMFieldDependentShearModulusTestParameters(const double alpha_e,
                                                                                                 const double m_e,
                                                                                                 const double g1)
{
    _lawElecMagGenericThermoMech->setEMFieldDependentShearModulusTestParameters(alpha_e,m_e,g1);
}

ElecMagInductorDG3DMaterialLaw::ElecMagInductorDG3DMaterialLaw(const int num, const double rho,
                                                               const double alpha, const double beta, const double gamma,
                                                               const  double lx,const double ly,const double lz,
                                                               const double seebeck, const double v0,
                                                               const double mu_x, const double mu_y, const double mu_z,
                                                               const double A0_x, const double A0_y, const double A0_z,
                                                               const double Irms, const double freq, const unsigned int nTurnsCoil,
                                                               const double coilLength_x, const double coilLength_y,
                                                               const double coilLength_z, const double coilWidth,
                                                               const double Centroid_x, const double Centroid_y,
                                                               const double Centroid_z, const double CentralAxis_x,
                                                               const double CentralAxis_y, const double CentralAxis_z,
                                                               const bool useFluxT, const bool evaluateCurlField):
                                                               dG3DMaterialLaw(num, rho, true)
{
    _lawElecMagInductor = new mlawElecMagInductor(num, alpha, beta, gamma, lx, ly, lz,
                                                  seebeck, v0, mu_x, mu_y, mu_z, A0_x,
                                                  A0_y, A0_z, Irms, freq, nTurnsCoil,
                                                  coilLength_x, coilLength_y, coilLength_z,
                                                  coilWidth, Centroid_x, Centroid_y, Centroid_z,
                                                  CentralAxis_x, CentralAxis_y, CentralAxis_z,
                                                  useFluxT, evaluateCurlField);

    this->_useFluxT = useFluxT;
    this->_evaluateCurlField = evaluateCurlField;
}

ElecMagInductorDG3DMaterialLaw::ElecMagInductorDG3DMaterialLaw(const ElecMagInductorDG3DMaterialLaw &source):
dG3DMaterialLaw(source)
{
    _lawElecMagInductor = NULL;
    if (source._lawElecMagInductor != NULL)
    {
        _lawElecMagInductor = new mlawElecMagInductor(*(source._lawElecMagInductor));
    }
    this->_useFluxT = source._useFluxT;
    this->_evaluateCurlField = source._evaluateCurlField;
}

void ElecMagInductorDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,
                                                   const MElement *ele, const int nbFF_,
                                                   const IntPt *GP, const int gpt) const
{
    // check interface element
    bool inter=true;
    const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
    if(iele==NULL)
        inter=false;

    IPElecMagInductor* ipvithermomeca=NULL;
    _lawElecMagInductor->createIPVariable(ipvithermomeca, hasBodyForce, ele,nbFF_, GP, gpt);
    IPElecMagInductor* ipv1thermomeca=NULL;
    _lawElecMagInductor->createIPVariable(ipv1thermomeca, hasBodyForce, ele,nbFF_, GP, gpt);
    IPElecMagInductor* ipv2thermomeca=NULL;
    _lawElecMagInductor->createIPVariable(ipv2thermomeca, hasBodyForce, ele,nbFF_, GP, gpt);

    SPoint3 pt_Global;
    ele->pnt(GP->pt[0],GP->pt[1],GP->pt[2],pt_Global);
    SVector3 GaussP(pt_Global);

    IPVariable* ipvi = new ElecMagInductorDG3DIPVariable(ipvithermomeca,
                                                         _lawElecMagInductor->getInitialTemperature(),
                                                         _lawElecMagInductor->getInitialVoltage(),
                                                         _lawElecMagInductor->getInitialMagPotential(),
                                                         hasBodyForce,inter);
    IPVariable* ipv1 = new ElecMagInductorDG3DIPVariable(ipv1thermomeca,
                                                         _lawElecMagInductor->getInitialTemperature(),
                                                         _lawElecMagInductor->getInitialVoltage(),
                                                         _lawElecMagInductor->getInitialMagPotential(),
                                                         hasBodyForce,inter);
    IPVariable* ipv2 = new ElecMagInductorDG3DIPVariable(ipv2thermomeca,
                                                         _lawElecMagInductor->getInitialTemperature(),
                                                         _lawElecMagInductor->getInitialVoltage(),
                                                         _lawElecMagInductor->getInitialMagPotential(),
                                                         hasBodyForce,inter);
    if(ips != NULL)
        delete ips;
    ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void ElecMagInductorDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce,
                                                      const MElement *ele, const int nbFF_,
                                                      const IntPt *GP, const int gpt) const
{
    if(ipv !=NULL)
        delete ipv;
    bool inter=true;
    const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
    if(iele == NULL)
    {
        inter=false;
    }

    IPElecMagInductor* ipvithermomeca =NULL;
    _lawElecMagInductor->createIPVariable(ipvithermomeca, hasBodyForce, ele,nbFF_, GP, gpt);

    SPoint3 pt_Global;
    ele->pnt(GP->pt[0],GP->pt[1],GP->pt[2],pt_Global);
    SVector3 GaussP(pt_Global);

    ipv = new ElecMagInductorDG3DIPVariable(ipvithermomeca,
                                            _lawElecMagInductor->getInitialTemperature(),
                                            _lawElecMagInductor->getInitialVoltage(),
                                            _lawElecMagInductor->getInitialMagPotential(),
                                            hasBodyForce,inter);

    dynamic_cast<ElecMagInductorDG3DIPVariable*>(ipv)->setIpThermoMech(ipvithermomeca->getRefToIpThermoMech());


}

void ElecMagInductorDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const
{
    /* get ipvariable */
    ElecMagInductorDG3DIPVariable* ipvcur;
    const ElecMagInductorDG3DIPVariable* ipvprev;

    FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
    if(ipvtmp !=NULL)
    {
        ipvcur = static_cast<ElecMagInductorDG3DIPVariable*>(ipvtmp->getIPvBulk());
        const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
        ipvprev = static_cast<const ElecMagInductorDG3DIPVariable*>(ipvtmp2->getIPvBulk());
    }
    else
    {
        ipvcur = static_cast<ElecMagInductorDG3DIPVariable*>(ipv);
        ipvprev = static_cast<const ElecMagInductorDG3DIPVariable*>(ipvp);
    }

    _lawElecMagInductor->checkInternalState(ipvcur->getInternalData(),ipvprev->getInternalData());
}

void ElecMagInductorDG3DMaterialLaw::stress(IPVariable*ipv, const IPVariable*ipvp, const bool stiff,
                                            const bool checkfrac, const bool dTangent)
{
    /* get ipvariable */
    ElecMagInductorDG3DIPVariable* ipvcur;
    const ElecMagInductorDG3DIPVariable* ipvprev;

    FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
    if(ipvtmp !=NULL)
    {
        ipvcur = static_cast<ElecMagInductorDG3DIPVariable*>(ipvtmp->getIPvBulk());
        const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
        ipvprev = static_cast<const ElecMagInductorDG3DIPVariable*>(ipvtmp2->getIPvBulk());
    }
    else
    {
        ipvcur = static_cast<ElecMagInductorDG3DIPVariable*>(ipv);
        ipvprev = static_cast<const ElecMagInductorDG3DIPVariable*>(ipvp);
    }

    /* strain xyz */
    const STensor3& Fn = ipvcur->getConstRefToDeformationGradient();
    const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();
    const double temperature = ipvcur->getConstRefToTemperature();
    const double T0 = ipvprev->getConstRefToTemperature();
    const double voltage = ipvcur->getConstRefToVoltage();
    const double voltage0 = ipvprev->getConstRefToVoltage();
    const SVector3& gradT= ipvcur->getConstRefToGradT();
    const SVector3& gradV= ipvcur->getConstRefToGradV();

    SVector3& fluxje=ipvcur->getRefToFluxje();
    STensor3& dedgradV=ipvcur->getRefTodFluxjedGradV();
    STensor3& dedgradT=ipvcur->getRefTodFluxjedGradT();
    SVector3& dedV=ipvcur->getRefTodFluxjedV();
    SVector3& dedT=ipvcur->getRefTodFluxjedT();
    STensor33& dedF=ipvcur->getRefTodFluxjedF();
    SVector3& D=ipvcur->getRefToElecDisplacement();
    STensor3& dDdgradV=ipvcur->getRefTodElecDisplacementdGradV();
    STensor3& dDdgradT=ipvcur->getRefTodElecDisplacementdGradT();
    SVector3& dDdV=ipvcur->getRefTodElecDisplacementdV();
    SVector3& dDdT=ipvcur->getRefTodElecDisplacementdT();
    STensor33& dDdF=ipvcur->getRefTodElecDispldF();
    STensor3& P=ipvcur->getRefToFirstPiolaKirchhoffStress();
    const STensor3 & P0 = ipvprev->getConstRefToFirstPiolaKirchhoffStress();
    STensor3& dPdT=ipvcur->getRefTodPdT();
    STensor3& dPdV=ipvcur->getRefTodPdV();
    STensor33& dPdE=ipvcur->getRefTodPdGradV();
    STensor43& Tangent=ipvcur->getRefToTangentModuli();

    double &w_T = ipvcur->getRefToThermalSource();
    double &dw_TdT = ipvcur->getRefTodThermalSourcedT();
    double &dw_TdV = ipvcur->getRefTodThermalSourcedV();
    STensor3& dw_TdF = ipvcur->getRefTodThermalSourcedF();

    double &w_V = ipvcur->getRefToVoltageSource();
    double &dw_VdT = ipvcur->getRefTodVoltageSourcedT();
    double &dw_VdV = ipvcur->getRefTodVoltageSourcedV();
    STensor3& dw_VdF = ipvcur->getRefTodVoltageSourcedF();

    double&  mechSource =  ipvcur->getRefToMechanicalSource()(0);
    double& dmechSourcedt = ipvcur->getRefTodMechanicalSourcedField()(0,0);
    double& dmechSourcedV = ipvcur->getRefTodMechanicalSourcedField()(0,1);
    STensor3 & dmechSourcedf = ipvcur->getRefTodMechanicalSourcedF()[0];

    ipvcur->setRefToTherConductivity(this->lineark10);
    ipvcur->setRefToCrossTherConductivity(this->lineark20);
    ipvcur->setRefToElecConductivity(this->linearl10);
    ipvcur->setRefToCrossElecConductivity(this->linearl20);
    STensor3& dk10dT = ipvcur->getRefTodTherConductivitydT();
    STensor3& dk20dT = ipvcur->getRefTodCrossTherConductivitydT();
    STensor3& dl10dT = ipvcur->getRefTodElecConductivitydT();
    STensor3& dl20dT = ipvcur->getRefTodCrossElecConductivitydT();
    STensor3& dk10dv = ipvcur->getRefTodTherConductivitydv();
    STensor3& dk20dv = ipvcur->getRefTodCrossTherConductivitydv();
    STensor3& dl10dv = ipvcur->getRefTodElecConductivitydv();
    STensor3& dl20dv = ipvcur->getRefTodCrossElecConductivitydv();
    STensor43& dl10dF=ipvcur->getRefTodElecConductivitydF();
    STensor43& dl20dF=ipvcur->getRefTodCrossElecConductivitydF();
    STensor43& dk10dF=ipvcur->getRefTodTherConductivitydF();
    STensor43& dk20dF=ipvcur->getRefTodCrossTherConductivitydF();

    SVector3 Vector3Tmp(0.0);
    STensor3 Tensor3Tmp(0.0);
    STensor33 Tensor33Tmp(0.0);
    STensor43 Tensor43Tmp(0.0);

    SVector3* fluxT=&Vector3Tmp;
    STensor3* dqdgradT=&Tensor3Tmp;
    STensor3* dqdgradV=&Tensor3Tmp;
    SVector3* dqdT=&Vector3Tmp;
    SVector3* dqdV=&Vector3Tmp;
    STensor33* dqdF=&Tensor33Tmp;
    STensor3* dfluxTdA=&Tensor3Tmp;
    STensor3* dfluxTdB=&Tensor3Tmp;

    SVector3 *fluxjy=&Vector3Tmp;
    SVector3 *djydV=&Vector3Tmp;
    STensor3 *djydgradV=&Tensor3Tmp;
    SVector3 *djydT=&Vector3Tmp;
    STensor3 *djydgradT=&Tensor3Tmp;
    STensor33 *djydF=&Tensor33Tmp;
    STensor3 * djydA =&Tensor3Tmp;
    STensor3 * djydB =&Tensor3Tmp;

    STensor3 *djydgradVdT=&Tensor3Tmp;
    STensor3 *djydgradVdV=&Tensor3Tmp;
    STensor43 *djydgradVdF=&Tensor43Tmp;
    STensor3 *djydgradTdT=&Tensor3Tmp;
    STensor3 *djydgradTdV=&Tensor3Tmp;
    STensor43 *djydgradTdF=&Tensor43Tmp;

    if (this->_useFluxT)
    {
        fluxT=&(ipvcur->getRefToFluxT());
        dqdgradT=&(ipvcur->getRefTodFluxTdGradT());
        dqdgradV=&(ipvcur->getRefTodFluxTdGradV());
        dqdT=&(ipvcur->getRefTodFluxTdT());
        dqdV=&(ipvcur->getRefTodFluxTdV());
        dqdF=&(ipvcur->getRefTodFluxTdF());
        dfluxTdA=&(ipvcur->getRefTodFluxTdMagneticVectorPotential());
        dfluxTdB=&(ipvcur->getRefTodFluxTdMagneticVectorCurl());
    }
    else
    {
        fluxjy=&(ipvcur->getRefToFluxEnergy());
        djydV=&(ipvcur->getRefTodFluxEnergydV());
        djydgradV=&(ipvcur->getRefTodFluxEnergydGradV());
        djydT=&(ipvcur->getRefTodFluxEnergydT());
        djydgradT=&(ipvcur->getRefTodFluxEnergydGradT());
        djydF=&(ipvcur->getRefTodFluxEnergydF());
        djydA = &(ipvcur->getRefTodFluxEnergydMagneticVectorPotential());
        djydB = &(ipvcur->getRefTodFluxEnergydMagneticVectorCurl());

        djydgradVdT= &(ipvcur->getRefTodFluxEnergydGradVdT());
        djydgradVdV= &(ipvcur->getRefTodFluxEnergydGradVdV());
        djydgradVdF=&(ipvcur->getRefTodFluxEnergydGradVdF());
        djydgradTdT= &(ipvcur->getRefTodFluxEnergydGradTdT());
        djydgradTdV= &(ipvcur->getRefTodFluxEnergydGradTdV());
        djydgradTdF=&(ipvcur->getRefTodFluxEnergydGradTdF());
    }

    IPVariable* q1 = ipvcur->getInternalData();
    const IPVariable* q0 = ipvprev->getInternalData();
    //IPElecMagInductor* q1 = ipvcur->getIPElecMagInductor();
    //const IPElecMagInductor* q0 = ipvprev->getIPElecMagInductor();

    const SVector3 & An = ipvcur->getConstRefToMagneticVectorPotential();
    const SVector3 & A0 = ipvprev->getConstRefToMagneticVectorPotential();
    const SVector3 & B = ipvcur->getConstRefToMagneticInduction();
    SVector3 & H = ipvcur->getRefToMagneticField();
    SVector3 & js0 = ipvcur->getRefToinductorSourceVectorField();
    SVector3 dBdT;
    STensor3 dBdGradT;
    SVector3 dBdV;
    STensor3 dBdGradV;
    STensor33 dBdF;
    STensor3 dBdA;
    STensor3 &dfluxjedA = ipvcur->getRefTodFluxjedMagneticVectorPotential();
    STensor3 & dfluxjedB = ipvcur->getRefTodFluxjedMagneticVectorCurl();
    STensor3 &dDdA = ipvcur->getRefTodElecDisplacementdMagneticVectorPotential();
    STensor3 & dDdB = ipvcur->getRefTodElecDisplacementdMagneticVectorCurl();
    SVector3 & dw_TdA = ipvcur->getRefTodFieldSourcedMagneticVectorPotential();
    SVector3 & dw_TdB = ipvcur->getRefTodFieldSourcedMagneticVectorCurl();
    SVector3 & dmechSourcedB = ipvcur->getRefTodMechanicalSourcedMagneticVectorCurl();
    SVector3 & sourceVectorField = ipvcur->getRefTosourceVectorField(); // Magnetic source vector field (T, grad V, F)
    SVector3 & dsourceVectorFielddt = ipvcur->getRefTodsourceVectorFielddt();
    double & ThermalEMFieldSource = ipvcur->getRefToThermalEMFieldSource();
    double & VoltageEMFieldSource = ipvcur->getRefToVoltageEMFieldSource();
    STensor3 & dHdA = ipvcur->getRefTodMagneticFielddMagneticVectorPotential();
    STensor3 & dHdB = ipvcur->getRefTodMagneticFielddMagneticVectorCurl();
    STensor33 & dPdB = ipvcur->getRefTodPdMagneticVectorCurl();
    STensor33 & dHdF = ipvcur->getRefTodMagneticFielddF();
    STensor33 & dsourceVectorFielddF = ipvcur->getRefTodsourceVectorFielddF();
    SVector3 & mechFieldSource = ipvcur->getRefTomechanicalFieldSource(); // Electromagnetic force
    STensor3 & dmechFieldSourcedB = ipvcur->getRefTodMechanicalFieldSourcedMagneticVectorCurl();
    STensor33 & dmechFieldSourcedF = ipvcur->getRefTodmechanicalFieldSourcedF();
    SVector3 & dHdT = ipvcur->getRefTodMagneticFielddT();
    STensor3 & dHdgradT = ipvcur->getRefTodMagneticFielddGradT();
    SVector3 & dHdV = ipvcur->getRefTodMagneticFielddV();
    STensor3 & dHdgradV = ipvcur->getRefTodMagneticFielddGradV();
    SVector3 & dsourceVectorFielddT = ipvcur->getRefTodSourceVectorFielddT();
    STensor3 & dsourceVectorFielddgradT = ipvcur->getRefTodSourceVectorFielddGradT();
    SVector3 & dsourceVectorFielddV = ipvcur->getRefTodSourceVectorFielddV();
    STensor3 & dsourceVectorFielddgradV = ipvcur->getRefTodSourceVectorFielddGradV();
    STensor3 & dsourceVectorFielddA = ipvcur->getRefTodSourceVectorFielddMagneticVectorPotential();
    STensor3 & dsourceVectorFielddB = ipvcur->getRefTodSourceVectorFielddMagneticVectorCurl();
    SVector3 & dmechFieldSourcedT = ipvcur->getRefTodMechanicalFieldSourcedT();
    STensor3 & dmechFieldSourcedgradT = ipvcur->getRefTodMechanicalFieldSourcedGradT();
    SVector3 & dmechFieldSourcedV = ipvcur->getRefTodMechanicalFieldSourcedV();
    STensor3 & dmechFieldSourcedgradV = ipvcur->getRefTodMechanicalFieldSourcedGradV();
    SVector3 & dThermalEMFieldSourcedA = ipvcur->getRefTodThermalEMFieldSourcedMagneticVectorPotential();
    SVector3 & dThermalEMFieldSourcedB = ipvcur->getRefTodThermalEMFieldSourcedMagneticVectorCurl();
    STensor3 & dThermalEMFieldSourcedF = ipvcur->getRefTodThermalEMFieldSourcedF();
    double & dThermalEMFieldSourcedT = ipvcur->getRefTodThermalEMFieldSourcedT();
    SVector3 & dThermalEMFieldSourcedGradT = ipvcur->getRefTodThermalEMFieldSourcedGradT();
    double & dThermalEMFieldSourcedV = ipvcur->getRefTodThermalEMFieldSourcedV();
    SVector3 & dThermalEMFieldSourcedGradV = ipvcur->getRefTodThermalEMFieldSourcedGradV();
    SVector3 & dVoltageEMFieldSourcedA = ipvcur->getRefTodVoltageEMFieldSourcedMagneticVectorPotential();
    SVector3 & dVoltageEMFieldSourcedB = ipvcur->getRefTodVoltageEMFieldSourcedMagneticVectorCurl();
    STensor3 & dVoltageEMFieldSourcedF = ipvcur->getRefTodVoltageEMFieldSourcedF();
    double & dVoltageEMFieldSourcedT = ipvcur->getRefTodVoltageEMFieldSourcedT();
    SVector3 & dVoltageEMFieldSourcedGradT = ipvcur->getRefTodVoltageEMFieldSourcedGradT();
    double & dVoltageEMFieldSourcedV = ipvcur->getRefTodVoltageEMFieldSourcedV();
    SVector3 & dVoltageEMFieldSourcedGradV = ipvcur->getRefTodVoltageEMFieldSourcedGradV();

    _lawElecMagInductor->constitutive(F0,Fn,P, P0, q0, q1, Tangent,T0, temperature,
                                      voltage0,voltage,gradT, gradV,*fluxT,fluxje,D,
                                      dPdT,dPdV, dPdE, *dqdgradT,*dqdT, *dqdgradV,*dqdV,*dqdF,
                                      dedgradT,dedT,dedgradV,dedV,dedF,
                                      dDdgradT,dDdT,dDdgradV,dDdV,dDdF,
                                      stiff, w_T,dw_TdT,dw_TdV,dw_TdF,w_V,dw_VdT,dw_VdV,dw_VdF,linearl10,linearl20,
                                      lineark10,lineark20,
                                      dl10dT,dl20dT,dk10dT,dk20dT,dl10dv,
                                      dl20dv,dk10dv,dk20dv,dl10dF,dl20dF,dk10dF,dk20dF,
                                      mechSource, dmechSourcedt, dmechSourcedV, dmechSourcedf,
                                      *fluxjy,*djydV,*djydgradV,*djydT,*djydgradT,
                                      *djydF,*djydA, *djydB, *djydgradVdT,*djydgradVdV,*djydgradVdF,
                                      *djydgradTdT,*djydgradTdV,*djydgradTdF,
                                      A0, An, B, H, js0, dBdT, dBdGradT, dBdV,
                                      dBdGradV, dBdF, dBdA, *dfluxTdA, dfluxjedA, dDdA, *dfluxTdB,
                                      dfluxjedB, dDdB, dw_TdA, dw_TdB, dmechSourcedB, sourceVectorField,
                                      dsourceVectorFielddt, ThermalEMFieldSource, VoltageEMFieldSource,
                                      dHdA, dHdB, dPdB,dHdF, dsourceVectorFielddF,
                                      mechFieldSource, dmechFieldSourcedB, dmechFieldSourcedF,
                                      dHdT, dHdgradT, dHdV, dHdgradV, dsourceVectorFielddT,
                                      dsourceVectorFielddgradT,dsourceVectorFielddV, dsourceVectorFielddgradV,
                                      dsourceVectorFielddA,dsourceVectorFielddB, dmechFieldSourcedT,
                                      dmechFieldSourcedgradT,dmechFieldSourcedV, dmechFieldSourcedgradV,
                                      dThermalEMFieldSourcedA,dThermalEMFieldSourcedB, dThermalEMFieldSourcedF,
                                      dThermalEMFieldSourcedT,dThermalEMFieldSourcedGradT, dThermalEMFieldSourcedV,
                                      dThermalEMFieldSourcedGradV,dVoltageEMFieldSourcedA, dVoltageEMFieldSourcedB,
                                      dVoltageEMFieldSourcedF,dVoltageEMFieldSourcedT, dVoltageEMFieldSourcedGradT,
                                      dVoltageEMFieldSourcedV,dVoltageEMFieldSourcedGradV);

    ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);
}

double ElecMagInductorDG3DMaterialLaw::getExtraDofStoredEnergyPerUnitField(double T) const
{
    return  _lawElecMagInductor->getConstRefToThermoMechanicalMaterialLaw().getExtraDofStoredEnergyPerUnitField(T);
}

double ElecMagInductorDG3DMaterialLaw::getInitialExtraDofStoredEnergyPerUnitField() const
{
    return  _lawElecMagInductor->getConstRefToThermoMechanicalMaterialLaw().getInitialExtraDofStoredEnergyPerUnitField();
}

const materialLaw &ElecMagInductorDG3DMaterialLaw::getConstRefToThermoMechanicalMaterialLaw() const
{
    return _lawElecMagInductor->getConstRefToThermoMechanicalMaterialLaw();
}
materialLaw &ElecMagInductorDG3DMaterialLaw::getRefToThermoMechanicalMaterialLaw()
{
    return _lawElecMagInductor->getRefToThermoMechanicalMaterialLaw();
}
void ElecMagInductorDG3DMaterialLaw::setThermoMechanicalMaterialLaw(const dG3DMaterialLaw *mlaw)
{
    _lawElecMagInductor->setThermoMechanicalMaterialLaw(mlaw->getConstNonLinearSolverMaterialLaw());

    elasticStiffness=mlaw->elasticStiffness;
}

void ElecMagInductorDG3DMaterialLaw::setApplyReferenceF(const bool rf)
{
    _lawElecMagInductor->setApplyReferenceF(rf);
}

void ElecMagInductorDG3DMaterialLaw::setUseEMStress(const bool emStress)
{
    _lawElecMagInductor->setUseEMStress(emStress);
}

void ElecMagInductorDG3DMaterialLaw::setEMFieldDependentShearModulus(const bool EM_ShearModulus)
{
    _lawElecMagInductor->setEMFieldDependentShearModulus(EM_ShearModulus);
}

void ElecMagInductorDG3DMaterialLaw::setEMFieldDependentShearModulusTestParameters(const double alpha_e,
                                                                                   const double m_e,
                                                                                   const double g1)
{
    _lawElecMagInductor->setEMFieldDependentShearModulusTestParameters(alpha_e,m_e,g1);
}

// NonLinearTVM Law Interface =================================================================== BEGIN

// Constructor - Added Tinitial and thermal properties
NonLinearTVMDG3DMaterialLaw::NonLinearTVMDG3DMaterialLaw(const int num, const double rho, const double E, const double nu,
                        const double Tinitial, const double Alpha, const double KThCon, const double Cp,
                        const bool matrixbyPerturbation, const double pert, const bool thermalEstimationPreviousConfig)
                        : dG3DMaterialLaw(num,rho), _viscoLaw(num,E,nu,rho,0.,Tinitial,Alpha, KThCon, Cp, matrixbyPerturbation,pert, thermalEstimationPreviousConfig)
{
  fillElasticStiffness(E, nu, elasticStiffness);   // Why are some of the variables defined in this constructor?  --- WHAT??
};

// Copy Constructor - No Changes Yet
NonLinearTVMDG3DMaterialLaw::NonLinearTVMDG3DMaterialLaw(const NonLinearTVMDG3DMaterialLaw& src):  dG3DMaterialLaw(src), _viscoLaw(src._viscoLaw){};

void NonLinearTVMDG3DMaterialLaw::setStrainOrder(const int order){
  _viscoLaw.setStrainOrder(order);
};
void NonLinearTVMDG3DMaterialLaw::setViscoelasticMethod(const int method){
  _viscoLaw.setViscoelasticMethod(method);
};
void NonLinearTVMDG3DMaterialLaw::setViscoElasticNumberOfElement(const int N){
  _viscoLaw.setViscoElasticNumberOfElement(N);
};
void NonLinearTVMDG3DMaterialLaw::setViscoElasticData(const int i, const double Ei, const double taui){
  _viscoLaw.setViscoElasticData(i,Ei,taui);
};
void NonLinearTVMDG3DMaterialLaw::setViscoElasticData_Bulk(const int i, const double Ki, const double ki){
  _viscoLaw.setViscoElasticData_Bulk(i,Ki,ki);
};
void NonLinearTVMDG3DMaterialLaw::setViscoElasticData_Shear(const int i, const double Gi, const double gi){
  _viscoLaw.setViscoElasticData_Shear(i,Gi,gi);
};
void NonLinearTVMDG3DMaterialLaw::setViscoElasticData(const std::string filename){
  _viscoLaw.setViscoElasticData(filename);
};

void NonLinearTVMDG3DMaterialLaw::setReferenceTemperature(const double Tref){
  _viscoLaw.setReferenceTemperature(Tref);
};

void NonLinearTVMDG3DMaterialLaw::setShiftFactorConstantsWLF(const double C1, const double C2){
  _viscoLaw.setShiftFactorConstantsWLF(C1,C2);
};
void NonLinearTVMDG3DMaterialLaw::setTemperatureFunction_BulkModulus(const scalarFunction& Tfunc){
  _viscoLaw.setTemperatureFunction_BulkModulus(Tfunc);
};
void NonLinearTVMDG3DMaterialLaw::setTemperatureFunction_ShearModulus(const scalarFunction& Tfunc){
  _viscoLaw.setTemperatureFunction_ShearModulus(Tfunc);
};
void NonLinearTVMDG3DMaterialLaw::setTemperatureFunction_ThermalDilationCoefficient(const scalarFunction& Tfunc){
  _viscoLaw.setTemperatureFunction_ThermalDilationCoefficient(Tfunc);
};
void NonLinearTVMDG3DMaterialLaw::setTemperatureFunction_ThermalConductivity(const scalarFunction& Tfunc){
  _viscoLaw.setTemperatureFunction_ThermalConductivity(Tfunc);
};
void NonLinearTVMDG3DMaterialLaw::setTemperatureFunction_SpecificHeat(const scalarFunction& Tfunc){
  _viscoLaw.setTemperatureFunction_SpecificHeat(Tfunc);
};
void NonLinearTVMDG3DMaterialLaw::useExtraBranchBool(const bool useExtraBranch){
  _viscoLaw.useExtraBranchBool(useExtraBranch);
};
void NonLinearTVMDG3DMaterialLaw::useExtraBranchBool_TVE(const bool useExtraBranch_TVE, const int ExtraBranch_TVE_option){
  _viscoLaw.useExtraBranchBool_TVE(useExtraBranch_TVE, ExtraBranch_TVE_option);
};
void NonLinearTVMDG3DMaterialLaw::useRotationCorrectionBool(const bool useRotationCorrection,  const int rotationCorrectionScheme){
  _viscoLaw.useRotationCorrectionBool(useRotationCorrection,rotationCorrectionScheme);
};
void NonLinearTVMDG3DMaterialLaw::setVolumeCorrection(const double _vc, const double _xivc, const double _zetavc, const double _dc, const double _thetadc, const double _pidc){
  _viscoLaw.setVolumeCorrection(_vc,_xivc,_zetavc,_dc,_thetadc,_pidc);
};
void NonLinearTVMDG3DMaterialLaw::setAdditionalVolumeCorrections(const double V3, const double V4, const double V5, const double D3, const double D4, const double D5){
  _viscoLaw.setAdditionalVolumeCorrections(V3,V4,V5,D3,D4,D5);
};
void NonLinearTVMDG3DMaterialLaw::setExtraBranch_CompressionParameter(const double compCorrection, const double compCorrection_2, const double compCorrection_3){
  _viscoLaw.setExtraBranch_CompressionParameter(compCorrection,compCorrection_2,compCorrection_3);
};
void NonLinearTVMDG3DMaterialLaw::setTensionCompressionRegularisation(const double tensionCompressionRegularisation){
  _viscoLaw.setTensionCompressionRegularisation(tensionCompressionRegularisation);
};
void NonLinearTVMDG3DMaterialLaw::setExtraBranchNLType(const int method){
  _viscoLaw.setExtraBranchNLType(method);
};
void NonLinearTVMDG3DMaterialLaw::setCorrectionsAllBranchesTVE(const int i, const double V0,const double V1, const double V2, const double D0,const double D1, const double D2){
  _viscoLaw.setCorrectionsAllBranchesTVE(i,V0,V1,V2,D0,D1,D2);
};
void NonLinearTVMDG3DMaterialLaw::setAdditionalCorrectionsAllBranchesTVE(const int i, const double V3, const double V4, const double V5, const double D3, const double D4, const double D5){
  _viscoLaw.setAdditionalCorrectionsAllBranchesTVE(i,V3,V4,V5,D3,D4,D5);
};
void NonLinearTVMDG3DMaterialLaw::setCompressionCorrectionsAllBranchesTVE(const int i, const double Ci){
  _viscoLaw.setCompressionCorrectionsAllBranchesTVE(i,Ci);
};
void NonLinearTVMDG3DMaterialLaw::setMullinsEffect(const mullinsEffect& mullins){
  _viscoLaw.setMullinsEffect(mullins);
};

// Added extra argument in the NonLinearTVMDG3DIPVariable contructor - Tinitial   (added from LinearThermoMech)
void NonLinearTVMDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  IPVariable* ipvi = new  NonLinearTVMDG3DIPVariable(_viscoLaw,_viscoLaw.getInitialTemperature(),hasBodyForce,inter);   // Why 3 times? --- WHAT??    Add getInitialTemperature in Material Law
  IPVariable* ipv1 = new  NonLinearTVMDG3DIPVariable(_viscoLaw,_viscoLaw.getInitialTemperature(),hasBodyForce,inter);
  IPVariable* ipv2 = new  NonLinearTVMDG3DIPVariable(_viscoLaw,_viscoLaw.getInitialTemperature(),hasBodyForce,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void NonLinearTVMDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  ipv = new  NonLinearTVMDG3DIPVariable(_viscoLaw,_viscoLaw.getInitialTemperature(),hasBodyForce,inter);
}

// 2 Member Functions added from LinearThermoMechanicsDG3DMaterialLaw  --  Add these functions in the material law
double NonLinearTVMDG3DMaterialLaw::getExtraDofStoredEnergyPerUnitField(double T) const{
  return _viscoLaw.getExtraDofStoredEnergyPerUnitField(T);
}

double NonLinearTVMDG3DMaterialLaw::getInitialExtraDofStoredEnergyPerUnitField() const{
	return _viscoLaw.getInitialExtraDofStoredEnergyPerUnitField();
};

void NonLinearTVMDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
 // get ipvariable
  NonLinearTVMDG3DIPVariable* ipvcur;
  const NonLinearTVMDG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {
    ipvcur = static_cast<NonLinearTVMDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const NonLinearTVMDG3DIPVariable*>(ipvtmp2->getIPvBulk());
  }
  else
  {
    ipvcur = static_cast<NonLinearTVMDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const NonLinearTVMDG3DIPVariable*>(ipvp);
  }
  _viscoLaw.checkInternalState(ipvcur->getInternalData(), ipvprev->getInternalData());
};

void NonLinearTVMDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent){

  // get ipvariable
  NonLinearTVMDG3DIPVariable* ipvcur;
  const NonLinearTVMDG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {
    ipvcur = static_cast<NonLinearTVMDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const NonLinearTVMDG3DIPVariable*>(ipvtmp2->getIPvBulk());
  }
  else
  {
    ipvcur = static_cast<NonLinearTVMDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const NonLinearTVMDG3DIPVariable*>(ipvp);
  }

// Added a bunch of ip variables and derivatives from LinearThermoMechanicsDG3DMaterialLaw in dG3DMaterialLaw.cpp
  // Current and Previous Deformation Gradient (F) and Temperature (T), Current Temp Gradient (gradT) and other current derivatives.
     // THe new variables need to be added in the constitutive function of material law.
  const STensor3& F1       = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0       = ipvprev->getConstRefToDeformationGradient();
  const double T           = ipvcur->getConstRefToTemperature();
  const double T0          = ipvprev->getConstRefToTemperature();
  const SVector3& gradT0   = ipvprev->getConstRefToGradT();
  const SVector3& gradT    = ipvcur->getConstRefToGradT();
        STensor3& dPdT     = ipvcur->getRefTodPdT();
        SVector3& fluxT    = ipvcur->getRefToThermalFlux();
        STensor3& dqdgradT = ipvcur->getRefTodThermalFluxdGradT();
        SVector3& dqdT     = ipvcur->getRefTodThermalFluxdT();
        STensor33& dqdF    = ipvcur->getRefTodThermalFluxdF();
//const STensor3 *linearK()=ipvcur-> getConstRefTolinearK();

        STensor3& P        = ipvcur->getRefToFirstPiolaKirchhoffStress();
        STensor43& Tangent = ipvcur->getRefToTangentModuli();
  const double& w0         = ipvprev->getConstRefToThermalSource();
        double& w          = ipvcur->getRefToThermalSource();
        double& dwdt       = ipvcur->getRefTodThermalSourcedField();
        STensor3& dwdf     = ipvcur->getRefTodThermalSourcedF();
  const SVector3& N        = ipvcur->getRefToReferenceOutwardNormal();
  const SVector3& ujump    = ipvcur->getRefToJump()(0);
  const double& Tjump      = ipvcur->getRefToFieldJump()(0);
        double& Cp         = ipvcur->getRefToExtraDofFieldCapacityPerUnitField()(0);
  const double& mechSource0 = ipvprev->getConstRefToMechanicalSource()(0);
        double& mechSource = ipvcur->getRefToMechanicalSource()(0);
        double& dmechSourcedt = ipvcur->getRefTodMechanicalSourcedField()(0,0);
        STensor3& dmechSourcedf = ipvcur->getRefTodMechanicalSourcedF()[0];

  // data for J2 law                  ---------------------------------------------   WHAT is J2law here?
  IPNonLinearTVM* q1 = ipvcur->getIPNonLinearTVM();
  const IPNonLinearTVM* q0 = ipvprev->getIPNonLinearTVM();
  // static STensor63 dCalgdeps;
  // STensor43& elasticL = ipvcur->getRefToElasticTangentModuli();

  // Compute Stress
  // _viscoLaw.constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),stiff,NULL,dTangent,NULL);   //-- Without Thermomech Coupling
  // _viscoLaw.constitutive(F0,F1,P,q0, q1,Tangent,T0,T, gradT,fluxT,dPdT,dqdgradT,dqdT,dqdF,w,dwdt,dwdf,stiff); //LinearThermoMech mlaw
_viscoLaw.constitutive(F0,F1,P,q0,q1,Tangent,T0,T,gradT0,gradT,fluxT,dPdT,dqdgradT,dqdT,dqdF,w,dwdt,dwdf,mechSource,dmechSourcedt,dmechSourcedf,stiff);  // FUllJ2 mlaw

  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);  // --------------------------- WHAT does this line do?
  ipvcur->setRefToLinearConductivity(_viscoLaw.getConductivityTensor());
  ipvcur->setRefToStiff_alphaDilatation(_viscoLaw.getStiff_alphaDilatation());

// Add the following lines here from LinearThermoMechanicsDG3DMaterialLaw ??
        // Fracture hasn't been defined as of yet.
/*
   if (checkfrac==true)    // to get the H1 norm in the interface
   q1->_fracEnergy=defoEnergy(ujump,Tjump,N);
 else
    q1->_elasticEnergy=defoEnergy(F1, gradT); */
  Cp = _viscoLaw.getExtraDofStoredEnergyPerUnitField(T);            // ---------------------------- WHAT is this line?

}

// defoEnergy functions from LinearThermoMechanicsDG3DMaterialLaw - add them?

// NonLinearTVM Law Interface =================================================================== END

// NonLinearTVP Law Interface =================================================================== BEGIN

NonLinearTVPDG3DMaterialLaw::NonLinearTVPDG3DMaterialLaw(const int num, const double rho, const double E, const double nu, const double tol,
                        const double Tinitial, const double Alpha, const double KThCon, const double Cp,
                        const bool matrixbyPerturbation, const double pert, const bool thermalEstimationPreviousConfig):
                        dG3DMaterialLaw(num,rho), _viscoLaw(num,E,nu,rho,tol,Tinitial,Alpha, KThCon, Cp, matrixbyPerturbation,pert, thermalEstimationPreviousConfig)
{
  fillElasticStiffness(E, nu, elasticStiffness);
};

NonLinearTVPDG3DMaterialLaw::NonLinearTVPDG3DMaterialLaw(const NonLinearTVPDG3DMaterialLaw& src):  dG3DMaterialLaw(src), _viscoLaw(src._viscoLaw){};

void NonLinearTVPDG3DMaterialLaw::setStrainOrder(const int order){
  _viscoLaw.setStrainOrder(order);
};
void NonLinearTVPDG3DMaterialLaw::setViscoelasticMethod(const int method){
  _viscoLaw.setViscoelasticMethod(method);
};
void NonLinearTVPDG3DMaterialLaw::setViscoElasticNumberOfElement(const int N){
  _viscoLaw.setViscoElasticNumberOfElement(N);
};
void NonLinearTVPDG3DMaterialLaw::setViscoElasticData(const int i, const double Ei, const double taui){
  _viscoLaw.setViscoElasticData(i,Ei,taui);
};
/*
void NonLinearTVPDG3DMaterialLaw::setViscoElasticData(const int i, const double Ei, const double taui, const double Alphai){
  _viscoLaw.setViscoElasticData(i,Ei,taui,Alphai);
};*/
void NonLinearTVPDG3DMaterialLaw::setViscoElasticData_Bulk(const int i, const double Ki, const double ki){
  _viscoLaw.setViscoElasticData_Bulk(i,Ki,ki);
};
void NonLinearTVPDG3DMaterialLaw::setViscoElasticData_Shear(const int i, const double Gi, const double gi){
  _viscoLaw.setViscoElasticData_Shear(i,Gi,gi);
};
/*
void NonLinearTVPDG3DMaterialLaw::setViscoElasticData_Alpha(const int i, const double Alphai){
  _viscoLaw.setViscoElasticData_Alpha(i,Alphai);
};
*/
void NonLinearTVPDG3DMaterialLaw::setViscoElasticData(const std::string filename){
  _viscoLaw.setViscoElasticData(filename);
};
void NonLinearTVPDG3DMaterialLaw::setIsotropicHardeningCoefficients(const double HR1, const double HR2, const double HR3){
  _viscoLaw.setIsotropicHardeningCoefficients(HR1,HR2,HR3);
};
void NonLinearTVPDG3DMaterialLaw::setPolynomialOrderChabocheCoeffs(const int order){
  _viscoLaw.setPolynomialOrderChabocheCoeffs(order);
};
void NonLinearTVPDG3DMaterialLaw::setPolynomialCoeffsChabocheCoeffs(const int i, const double val){
  _viscoLaw.setPolynomialCoeffsChabocheCoeffs(i,val);
};
void NonLinearTVPDG3DMaterialLaw::setReferenceTemperature(const double Tref){
  _viscoLaw.setReferenceTemperature(Tref);
};
/*
void NonLinearTVPDG3DMaterialLaw::setTempFuncOption(const double TemFuncOpt){           // To be discarded
  _viscoLaw.setTempFuncOption(TemFuncOpt);
};
void NonLinearTVPDG3DMaterialLaw::setModelOption(const int modelFlag){
  _viscoLaw.setModelOption(modelFlag);
};
void NonLinearTVPDG3DMaterialLaw::setTestOption(const int testFlag){
  _viscoLaw.setTestOption(testFlag);
};
*/
void NonLinearTVPDG3DMaterialLaw::setShiftFactorConstantsWLF(const double C1, const double C2){
  _viscoLaw.setShiftFactorConstantsWLF(C1,C2);
};
void NonLinearTVPDG3DMaterialLaw::setTemperatureFunction_BulkModulus(const scalarFunction& Tfunc){
  _viscoLaw.setTemperatureFunction_BulkModulus(Tfunc);
};
void NonLinearTVPDG3DMaterialLaw::setTemperatureFunction_ShearModulus(const scalarFunction& Tfunc){
  _viscoLaw.setTemperatureFunction_ShearModulus(Tfunc);
};
void NonLinearTVPDG3DMaterialLaw::setTemperatureFunction_ThermalDilationCoefficient(const scalarFunction& Tfunc){
  _viscoLaw.setTemperatureFunction_ThermalDilationCoefficient(Tfunc);
};
void NonLinearTVPDG3DMaterialLaw::setTemperatureFunction_ThermalConductivity(const scalarFunction& Tfunc){
  _viscoLaw.setTemperatureFunction_ThermalConductivity(Tfunc);
};
void NonLinearTVPDG3DMaterialLaw::setTemperatureFunction_SpecificHeat(const scalarFunction& Tfunc){
  _viscoLaw.setTemperatureFunction_SpecificHeat(Tfunc);
};
void NonLinearTVPDG3DMaterialLaw::setTemperatureFunction_ElasticCorrection_Bulk(const scalarFunction& Tfunc){
  _viscoLaw.setTemperatureFunction_ElasticCorrection_Bulk(Tfunc);
};
void NonLinearTVPDG3DMaterialLaw::setTemperatureFunction_ElasticCorrection_Shear(const scalarFunction& Tfunc){
  _viscoLaw.setTemperatureFunction_ElasticCorrection_Shear(Tfunc);
};
/*
void NonLinearTVPDG3DMaterialLaw::setTemperatureFunction_BranchBulkModuli(const scalarFunction& Tfunc, int i){
  _viscoLaw.setTemperatureFunction_BranchBulkModuli(Tfunc,i);
};
void NonLinearTVPDG3DMaterialLaw::setTemperatureFunction_BranchShearModuli(const scalarFunction& Tfunc, int i){
  _viscoLaw.setTemperatureFunction_BranchShearModuli(Tfunc,i);
};
void NonLinearTVPDG3DMaterialLaw::setTemperatureFunction_BranchThermalDilationCoefficient(const scalarFunction& Tfunc, int i){
  _viscoLaw.setTemperatureFunction_BranchThermalDilationCoefficient(Tfunc,i);
};
*/
void NonLinearTVPDG3DMaterialLaw::setCompressionHardening(const J2IsotropicHardening& comp){
  _viscoLaw.setCompressionHardening(comp);
};
void NonLinearTVPDG3DMaterialLaw::setTractionHardening(const J2IsotropicHardening& trac){
  _viscoLaw.setTractionHardening(trac);
};
void NonLinearTVPDG3DMaterialLaw::setKinematicHardening(const kinematicHardening& kin){
  _viscoLaw.setKinematicHardening(kin);
};
void NonLinearTVPDG3DMaterialLaw::setViscosityEffect(const viscosityLaw& v, const double p){
  _viscoLaw.setViscosityEffect(v,p);
};
void NonLinearTVPDG3DMaterialLaw::setMullinsEffect(const mullinsEffect& mullins){
  _viscoLaw.setMullinsEffect(mullins);
};
void NonLinearTVPDG3DMaterialLaw::setYieldPowerFactor(const double n){
  _viscoLaw.setPowerFactor(n);
};
void NonLinearTVPDG3DMaterialLaw::nonAssociatedFlowRuleFactor(const double b){
  _viscoLaw.nonAssociatedFlowRuleFactor(b);
};
void NonLinearTVPDG3DMaterialLaw::setPlasticPoissonRatio(const double nup){
  _viscoLaw.setPlasticPoissonRatio(nup);
};
void NonLinearTVPDG3DMaterialLaw::setNonAssociatedFlow(const bool flag){
  _viscoLaw.setNonAssociatedFlow(flag);
};

void NonLinearTVPDG3DMaterialLaw::setTaylorQuineyFactor(const double f, const bool flag){
  _viscoLaw.setTaylorQuineyFactor(f, flag);
};
void NonLinearTVPDG3DMaterialLaw::setTemperatureFunction_InitialYieldStress(const scalarFunction& Tfunc){
  _viscoLaw.setTemperatureFunction_InitialYieldStress(Tfunc);
};
void NonLinearTVPDG3DMaterialLaw::setTemperatureFunction_Hardening(const scalarFunction& Tfunc){
  _viscoLaw.setTemperatureFunction_Hardening(Tfunc);
};
void NonLinearTVPDG3DMaterialLaw::setTemperatureFunction_KinematicHardening(const scalarFunction& Tfunc){
  _viscoLaw.setTemperatureFunction_KinematicHardening(Tfunc);
};
void NonLinearTVPDG3DMaterialLaw::setTemperatureFunction_Viscosity(const scalarFunction& Tfunc){
  _viscoLaw.setTemperatureFunction_Viscosity(Tfunc);
};
void NonLinearTVPDG3DMaterialLaw::useExtraBranchBool(const bool useExtraBranch){
  _viscoLaw.useExtraBranchBool(useExtraBranch);
};
void NonLinearTVPDG3DMaterialLaw::setVolumeCorrection(const double _vc, const double _xivc, const double _zetavc, const double _dc, const double _thetadc, const double _pidc){
  _viscoLaw.setVolumeCorrection(_vc,_xivc,_zetavc,_dc,_thetadc,_pidc);
};
void NonLinearTVPDG3DMaterialLaw::setAdditionalVolumeCorrections(const double V3, const double V4, const double V5, const double D3, const double D4, const double D5){
  _viscoLaw.setAdditionalVolumeCorrections(V3,V4,V5,D3,D4,D5);
};
void NonLinearTVPDG3DMaterialLaw::setExtraBranch_CompressionParameter(const double compCorrection, const double compCorrection_2, const double compCorrection_3){
  _viscoLaw.setExtraBranch_CompressionParameter(compCorrection,compCorrection_2,compCorrection_3);
};
void NonLinearTVPDG3DMaterialLaw::setExtraBranchNLType(const int method){
  _viscoLaw.setExtraBranchNLType(method);
};
void NonLinearTVPDG3DMaterialLaw::setTensionCompressionRegularisation(const double tensionCompressionRegularisation){
  _viscoLaw.setTensionCompressionRegularisation(tensionCompressionRegularisation);
};
// Added extra argument in the NonLinearTVEDG3DIPVariable contructor - Tinitial   (added from LinearThermoMech)
void NonLinearTVPDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  IPVariable* ipvi = new  NonLinearTVPDG3DIPVariable(_viscoLaw,_viscoLaw.getInitialTemperature(),hasBodyForce,inter);   // Why 3 times? --- WHAT??    Add getInitialTemperature in Material Law
  IPVariable* ipv1 = new  NonLinearTVPDG3DIPVariable(_viscoLaw,_viscoLaw.getInitialTemperature(),hasBodyForce,inter);
  IPVariable* ipv2 = new  NonLinearTVPDG3DIPVariable(_viscoLaw,_viscoLaw.getInitialTemperature(),hasBodyForce,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void NonLinearTVPDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  ipv = new  NonLinearTVPDG3DIPVariable(_viscoLaw,_viscoLaw.getInitialTemperature(),hasBodyForce,inter);
}

// 2 Member Functions added from LinearThermoMechanicsDG3DMaterialLaw  --  Add these functions in the material law
double NonLinearTVPDG3DMaterialLaw::getExtraDofStoredEnergyPerUnitField(double T) const{
  return _viscoLaw.getExtraDofStoredEnergyPerUnitField(T);
}

double NonLinearTVPDG3DMaterialLaw::getInitialExtraDofStoredEnergyPerUnitField() const{
	return _viscoLaw.getInitialExtraDofStoredEnergyPerUnitField();
};

void NonLinearTVPDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
 // get ipvariable
  NonLinearTVPDG3DIPVariable* ipvcur;
  const NonLinearTVPDG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {
    ipvcur = static_cast<NonLinearTVPDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const NonLinearTVPDG3DIPVariable*>(ipvtmp2->getIPvBulk());
  }
  else
  {
    ipvcur = static_cast<NonLinearTVPDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const NonLinearTVPDG3DIPVariable*>(ipvp);
  }
  _viscoLaw.checkInternalState(ipvcur->getInternalData(), ipvprev->getInternalData());
};

void NonLinearTVPDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent){

  // get ipvariable
  NonLinearTVPDG3DIPVariable* ipvcur;
  const NonLinearTVPDG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {
    ipvcur = static_cast<NonLinearTVPDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const NonLinearTVPDG3DIPVariable*>(ipvtmp2->getIPvBulk());
  }
  else
  {
    ipvcur = static_cast<NonLinearTVPDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const NonLinearTVPDG3DIPVariable*>(ipvp);
  }

// All IP Variables passed to the Constitutive function defined in mlaw
  const STensor3& F1       = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0       = ipvprev->getConstRefToDeformationGradient();
  const double T           = ipvcur->getConstRefToTemperature();
  const double T0          = ipvprev->getConstRefToTemperature();
  const SVector3& gradT0   = ipvprev->getConstRefToGradT();
  const SVector3& gradT    = ipvcur->getConstRefToGradT();
        STensor3& dPdT     = ipvcur->getRefTodPdT();
        SVector3& fluxT    = ipvcur->getRefToThermalFlux();
        STensor3& dqdgradT = ipvcur->getRefTodThermalFluxdGradT();
        SVector3& dqdT     = ipvcur->getRefTodThermalFluxdT();
        STensor33& dqdF    = ipvcur->getRefTodThermalFluxdF();

        STensor3& P        = ipvcur->getRefToFirstPiolaKirchhoffStress();
        STensor43& Tangent = ipvcur->getRefToTangentModuli();
  const double& w0         = ipvprev->getConstRefToThermalSource();
        double& w          = ipvcur->getRefToThermalSource();
        double& dwdt       = ipvcur->getRefTodThermalSourcedField();
        STensor3& dwdf     = ipvcur->getRefTodThermalSourcedF();
  const SVector3& N        = ipvcur->getRefToReferenceOutwardNormal();
  const SVector3& ujump    = ipvcur->getRefToJump()(0);
  const double& Tjump      = ipvcur->getRefToFieldJump()(0);
        double& Cp         = ipvcur->getRefToExtraDofFieldCapacityPerUnitField()(0);
  const double& mechSource0 = ipvprev->getConstRefToMechanicalSource()(0);
        double& mechSource = ipvcur->getRefToMechanicalSource()(0);
        double& dmechSourcedt = ipvcur->getRefTodMechanicalSourcedField()(0,0);
        STensor3& dmechSourcedf = ipvcur->getRefTodMechanicalSourcedF()[0];

  // data for J2 law
  IPNonLinearTVP* q1 = ipvcur->getIPNonLinearTVP();
  const IPNonLinearTVP* q0 = ipvprev->getIPNonLinearTVP();
  // static STensor63 dCalgdeps;
  STensor43& elasticL = ipvcur->getRefToElasticTangentModuli();

  // Compute Stress
  _viscoLaw.constitutive(F0,F1,P,q0,q1,Tangent,T0,T,gradT0,gradT,fluxT,dPdT,dqdgradT,dqdT,dqdF,w,dwdt,dwdf,mechSource,dmechSourcedt,dmechSourcedf,stiff,&elasticL);

  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);
  ipvcur->setRefToLinearConductivity(_viscoLaw.getConductivityTensor());
  ipvcur->setRefToStiff_alphaDilatation(_viscoLaw.getStiff_alphaDilatation());

  Cp = _viscoLaw.getExtraDofStoredEnergyPerUnitField(T);
}

// NonLinearTVP Law Interface =================================================================== END

// NonLinearTVENonLinearTVP Law Interface =================================================================== BEGIN

NonLinearTVENonLinearTVPDG3DMaterialLaw::NonLinearTVENonLinearTVPDG3DMaterialLaw(const int num, const double rho, const double E, const double nu, const double tol,
                        const double Tinitial, const double Alpha, const double KThCon, const double Cp,
                        const bool matrixbyPerturbation, const double pert, const double stressIteratorTol, const bool thermalEstimationPreviousConfig):
                        dG3DMaterialLaw(num,rho), _viscoLaw(num,E,nu,rho,tol,Tinitial,Alpha, KThCon, Cp, matrixbyPerturbation,pert, stressIteratorTol, thermalEstimationPreviousConfig)
{
  fillElasticStiffness(E, nu, elasticStiffness);   // Why are some of the variables defined in this constructor?  --- WHAT??
};

NonLinearTVENonLinearTVPDG3DMaterialLaw::NonLinearTVENonLinearTVPDG3DMaterialLaw(const NonLinearTVENonLinearTVPDG3DMaterialLaw& src):  dG3DMaterialLaw(src), _viscoLaw(src._viscoLaw){};

void NonLinearTVENonLinearTVPDG3DMaterialLaw::setStrainOrder(const int order){
  _viscoLaw.setStrainOrder(order);
};
void NonLinearTVENonLinearTVPDG3DMaterialLaw::setViscoelasticMethod(const int method){
  _viscoLaw.setViscoelasticMethod(method);
};
void NonLinearTVENonLinearTVPDG3DMaterialLaw::setViscoElasticNumberOfElement(const int N){
  _viscoLaw.setViscoElasticNumberOfElement(N);
};
void NonLinearTVENonLinearTVPDG3DMaterialLaw::setViscoElasticData(const int i, const double Ei, const double taui){
  _viscoLaw.setViscoElasticData(i,Ei,taui);
};
/*
void NonLinearTVENonLinearTVPDG3DMaterialLaw::setViscoElasticData(const int i, const double Ei, const double taui, const double Alphai){
  _viscoLaw.setViscoElasticData(i,Ei,taui,Alphai);
};*/
void NonLinearTVENonLinearTVPDG3DMaterialLaw::setViscoElasticData_Bulk(const int i, const double Ki, const double ki){
  _viscoLaw.setViscoElasticData_Bulk(i,Ki,ki);
};
void NonLinearTVENonLinearTVPDG3DMaterialLaw::setViscoElasticData_Shear(const int i, const double Gi, const double gi){
  _viscoLaw.setViscoElasticData_Shear(i,Gi,gi);
};
/*
void NonLinearTVENonLinearTVPDG3DMaterialLaw::setViscoElasticData_Alpha(const int i, const double Alphai){
  _viscoLaw.setViscoElasticData_Alpha(i,Alphai);
};
*/
void NonLinearTVENonLinearTVPDG3DMaterialLaw::setViscoElasticData(const std::string filename){
  _viscoLaw.setViscoElasticData(filename);
};
void NonLinearTVENonLinearTVPDG3DMaterialLaw::setIsotropicHardeningCoefficients(const double HR1, const double HR2, const double HR3){
  _viscoLaw.setIsotropicHardeningCoefficients(HR1,HR2,HR3);
};
void NonLinearTVENonLinearTVPDG3DMaterialLaw::setPolynomialOrderChabocheCoeffs(const int order){
  _viscoLaw.setPolynomialOrderChabocheCoeffs(order);
};
void NonLinearTVENonLinearTVPDG3DMaterialLaw::setPolynomialCoeffsChabocheCoeffs(const int i, const double val){
  _viscoLaw.setPolynomialCoeffsChabocheCoeffs(i,val);
};
void NonLinearTVENonLinearTVPDG3DMaterialLaw::setReferenceTemperature(const double Tref){
  _viscoLaw.setReferenceTemperature(Tref);
};
/*
void NonLinearTVENonLinearTVPDG3DMaterialLaw::setTempFuncOption(const double TemFuncOpt){           // To be discarded
  _viscoLaw.setTempFuncOption(TemFuncOpt);
};
void NonLinearTVENonLinearTVPDG3DMaterialLaw::setModelOption(const int modelFlag){
  _viscoLaw.setModelOption(modelFlag);
};
void NonLinearTVENonLinearTVPDG3DMaterialLaw::setTestOption(const int testFlag){
  _viscoLaw.setTestOption(testFlag);
};
*/
void NonLinearTVENonLinearTVPDG3DMaterialLaw::setShiftFactorConstantsWLF(const double C1, const double C2){
  _viscoLaw.setShiftFactorConstantsWLF(C1,C2);
};
void NonLinearTVENonLinearTVPDG3DMaterialLaw::setTemperatureFunction_BulkModulus(const scalarFunction& Tfunc){
  _viscoLaw.setTemperatureFunction_BulkModulus(Tfunc);
};
void NonLinearTVENonLinearTVPDG3DMaterialLaw::setTemperatureFunction_ShearModulus(const scalarFunction& Tfunc){
  _viscoLaw.setTemperatureFunction_ShearModulus(Tfunc);
};
void NonLinearTVENonLinearTVPDG3DMaterialLaw::setTemperatureFunction_ThermalDilationCoefficient(const scalarFunction& Tfunc){
  _viscoLaw.setTemperatureFunction_ThermalDilationCoefficient(Tfunc);
};
void NonLinearTVENonLinearTVPDG3DMaterialLaw::setTemperatureFunction_ThermalConductivity(const scalarFunction& Tfunc){
  _viscoLaw.setTemperatureFunction_ThermalConductivity(Tfunc);
};
void NonLinearTVENonLinearTVPDG3DMaterialLaw::setTemperatureFunction_SpecificHeat(const scalarFunction& Tfunc){
  _viscoLaw.setTemperatureFunction_SpecificHeat(Tfunc);
};
void NonLinearTVENonLinearTVPDG3DMaterialLaw::setTemperatureFunction_ElasticCorrection_Bulk(const scalarFunction& Tfunc){
  _viscoLaw.setTemperatureFunction_ElasticCorrection_Bulk(Tfunc);
};
void NonLinearTVENonLinearTVPDG3DMaterialLaw::setTemperatureFunction_ElasticCorrection_Shear(const scalarFunction& Tfunc){
  _viscoLaw.setTemperatureFunction_ElasticCorrection_Shear(Tfunc);
};
/*
void NonLinearTVENonLinearTVPDG3DMaterialLaw::setTemperatureFunction_BranchBulkModuli(const scalarFunction& Tfunc, int i){
  _viscoLaw.setTemperatureFunction_BranchBulkModuli(Tfunc,i);
};
void NonLinearTVENonLinearTVPDG3DMaterialLaw::setTemperatureFunction_BranchShearModuli(const scalarFunction& Tfunc, int i){
  _viscoLaw.setTemperatureFunction_BranchShearModuli(Tfunc,i);
};
void NonLinearTVENonLinearTVPDG3DMaterialLaw::setTemperatureFunction_BranchThermalDilationCoefficient(const scalarFunction& Tfunc, int i){
  _viscoLaw.setTemperatureFunction_BranchThermalDilationCoefficient(Tfunc,i);
};
*/
void NonLinearTVENonLinearTVPDG3DMaterialLaw::setCompressionHardening(const J2IsotropicHardening& comp){
  _viscoLaw.setCompressionHardening(comp);
};
void NonLinearTVENonLinearTVPDG3DMaterialLaw::setTractionHardening(const J2IsotropicHardening& trac){
  _viscoLaw.setTractionHardening(trac);
};
void NonLinearTVENonLinearTVPDG3DMaterialLaw::setKinematicHardening(const kinematicHardening& kin){
  _viscoLaw.setKinematicHardening(kin);
};
void NonLinearTVENonLinearTVPDG3DMaterialLaw::setViscosityEffect(const viscosityLaw& v, const double p){
  _viscoLaw.setViscosityEffect(v,p);
};
void NonLinearTVENonLinearTVPDG3DMaterialLaw::setMullinsEffect(const mullinsEffect& mullins){
  _viscoLaw.setMullinsEffect(mullins);
};

void NonLinearTVENonLinearTVPDG3DMaterialLaw::setYieldPowerFactor(const double n){
  _viscoLaw.setPowerFactor(n);
};
void NonLinearTVENonLinearTVPDG3DMaterialLaw::nonAssociatedFlowRuleFactor(const double b){
  _viscoLaw.nonAssociatedFlowRuleFactor(b);
};
void NonLinearTVENonLinearTVPDG3DMaterialLaw::setPlasticPoissonRatio(const double nup){
  _viscoLaw.setPlasticPoissonRatio(nup);
};
void NonLinearTVENonLinearTVPDG3DMaterialLaw::setNonAssociatedFlow(const bool flag){
  _viscoLaw.setNonAssociatedFlow(flag);
};

void NonLinearTVENonLinearTVPDG3DMaterialLaw::setTaylorQuineyFactor(const double f, const bool flag){
  _viscoLaw.setTaylorQuineyFactor(f, flag);
};
void NonLinearTVENonLinearTVPDG3DMaterialLaw::setTemperatureFunction_InitialYieldStress(const scalarFunction& Tfunc){
  _viscoLaw.setTemperatureFunction_InitialYieldStress(Tfunc);
};
void NonLinearTVENonLinearTVPDG3DMaterialLaw::setTemperatureFunction_Hardening(const scalarFunction& Tfunc){
  _viscoLaw.setTemperatureFunction_Hardening(Tfunc);
};
void NonLinearTVENonLinearTVPDG3DMaterialLaw::setTemperatureFunction_KinematicHardening(const scalarFunction& Tfunc){
  _viscoLaw.setTemperatureFunction_KinematicHardening(Tfunc);
};
void NonLinearTVENonLinearTVPDG3DMaterialLaw::setTemperatureFunction_Viscosity(const scalarFunction& Tfunc){
  _viscoLaw.setTemperatureFunction_Viscosity(Tfunc);
};
void NonLinearTVENonLinearTVPDG3DMaterialLaw::useExtraBranchBool(const bool useExtraBranch){
  _viscoLaw.useExtraBranchBool(useExtraBranch);
};
void NonLinearTVENonLinearTVPDG3DMaterialLaw::useExtraBranchBool_TVE(const bool useExtraBranch_TVE, const int ExtraBranch_TVE_option){
  _viscoLaw.useExtraBranchBool_TVE(useExtraBranch_TVE, ExtraBranch_TVE_option);
};
void NonLinearTVENonLinearTVPDG3DMaterialLaw::useRotationCorrectionBool(const bool useRotationCorrection,  const int rotationCorrectionScheme){
  _viscoLaw.useRotationCorrectionBool(useRotationCorrection,rotationCorrectionScheme);
};
void NonLinearTVENonLinearTVPDG3DMaterialLaw::setVolumeCorrection(const double _vc, const double _xivc, const double _zetavc, const double _dc, const double _thetadc, const double _pidc){
  _viscoLaw.setVolumeCorrection(_vc,_xivc,_zetavc,_dc,_thetadc,_pidc);
};
void NonLinearTVENonLinearTVPDG3DMaterialLaw::setAdditionalVolumeCorrections(const double V3, const double V4, const double V5, const double D3, const double D4, const double D5){
  _viscoLaw.setAdditionalVolumeCorrections(V3,V4,V5,D3,D4,D5);
};
void NonLinearTVENonLinearTVPDG3DMaterialLaw::setExtraBranch_CompressionParameter(const double compCorrection, const double compCorrection_2, const double compCorrection_3){
  _viscoLaw.setExtraBranch_CompressionParameter(compCorrection,compCorrection_2,compCorrection_3);
};
void NonLinearTVENonLinearTVPDG3DMaterialLaw::setExtraBranchNLType(const int method){
  _viscoLaw.setExtraBranchNLType(method);
};
void NonLinearTVENonLinearTVPDG3DMaterialLaw::setTensionCompressionRegularisation(const double tensionCompressionRegularisation){
  _viscoLaw.setTensionCompressionRegularisation(tensionCompressionRegularisation);
};
void NonLinearTVENonLinearTVPDG3DMaterialLaw::setCorrectionsAllBranchesTVE(const int i, const double V0,const double V1, const double V2, const double D0,const double D1, const double D2){
  _viscoLaw.setCorrectionsAllBranchesTVE(i,V0,V1,V2,D0,D1,D2);
};
void NonLinearTVENonLinearTVPDG3DMaterialLaw::setAdditionalCorrectionsAllBranchesTVE(const int i, const double V3, const double V4, const double V5, const double D3, const double D4, const double D5){
  _viscoLaw.setAdditionalCorrectionsAllBranchesTVE(i,V3,V4,V5,D3,D4,D5);
};
void NonLinearTVENonLinearTVPDG3DMaterialLaw::setCompressionCorrectionsAllBranchesTVE(const int i, const double Ci){
  _viscoLaw.setCompressionCorrectionsAllBranchesTVE(i,Ci);
};

// Added extra argument in the NonLinearTVEDG3DIPVariable contructor - Tinitial   (added from LinearThermoMech)
void NonLinearTVENonLinearTVPDG3DMaterialLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  IPVariable* ipvi = new  NonLinearTVPDG3DIPVariable(_viscoLaw,_viscoLaw.getInitialTemperature(),hasBodyForce,inter);   // Why 3 times? --- WHAT??    Add getInitialTemperature in Material Law
  IPVariable* ipv1 = new  NonLinearTVPDG3DIPVariable(_viscoLaw,_viscoLaw.getInitialTemperature(),hasBodyForce,inter);
  IPVariable* ipv2 = new  NonLinearTVPDG3DIPVariable(_viscoLaw,_viscoLaw.getInitialTemperature(),hasBodyForce,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void NonLinearTVENonLinearTVPDG3DMaterialLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  ipv = new  NonLinearTVPDG3DIPVariable(_viscoLaw,_viscoLaw.getInitialTemperature(),hasBodyForce,inter);
}

// 2 Member Functions added from LinearThermoMechanicsDG3DMaterialLaw  --  Add these functions in the material law
double NonLinearTVENonLinearTVPDG3DMaterialLaw::getExtraDofStoredEnergyPerUnitField(double T) const{
  return _viscoLaw.getExtraDofStoredEnergyPerUnitField(T);
}

double NonLinearTVENonLinearTVPDG3DMaterialLaw::getInitialExtraDofStoredEnergyPerUnitField() const{
	return _viscoLaw.getInitialExtraDofStoredEnergyPerUnitField();
};

void NonLinearTVENonLinearTVPDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{
 // get ipvariable
  NonLinearTVPDG3DIPVariable* ipvcur;
  const NonLinearTVPDG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {
    ipvcur = static_cast<NonLinearTVPDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const NonLinearTVPDG3DIPVariable*>(ipvtmp2->getIPvBulk());
  }
  else
  {
    ipvcur = static_cast<NonLinearTVPDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const NonLinearTVPDG3DIPVariable*>(ipvp);
  }
  _viscoLaw.checkInternalState(ipvcur->getInternalData(), ipvprev->getInternalData());
};

void NonLinearTVENonLinearTVPDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent){

  // get ipvariable
  NonLinearTVPDG3DIPVariable* ipvcur;
  const NonLinearTVPDG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {
    ipvcur = static_cast<NonLinearTVPDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const NonLinearTVPDG3DIPVariable*>(ipvtmp2->getIPvBulk());
  }
  else
  {
    ipvcur = static_cast<NonLinearTVPDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const NonLinearTVPDG3DIPVariable*>(ipvp);
  }

// All IP Variables passed to the Constitutive function defined in mlaw
  const STensor3& F1       = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0       = ipvprev->getConstRefToDeformationGradient();
  const double T           = ipvcur->getConstRefToTemperature();
  const double T0          = ipvprev->getConstRefToTemperature();
  const SVector3& gradT0   = ipvprev->getConstRefToGradT();
  const SVector3& gradT    = ipvcur->getConstRefToGradT();
        STensor3& dPdT     = ipvcur->getRefTodPdT();
        SVector3& fluxT    = ipvcur->getRefToThermalFlux();
        STensor3& dqdgradT = ipvcur->getRefTodThermalFluxdGradT();
        SVector3& dqdT     = ipvcur->getRefTodThermalFluxdT();
        STensor33& dqdF    = ipvcur->getRefTodThermalFluxdF();

        STensor3& P        = ipvcur->getRefToFirstPiolaKirchhoffStress();
        STensor43& Tangent = ipvcur->getRefToTangentModuli();
  const double& w0         = ipvprev->getConstRefToThermalSource();
        double& w          = ipvcur->getRefToThermalSource();
        double& dwdt       = ipvcur->getRefTodThermalSourcedField();
        STensor3& dwdf     = ipvcur->getRefTodThermalSourcedF();
  const SVector3& N        = ipvcur->getRefToReferenceOutwardNormal();
  const SVector3& ujump    = ipvcur->getRefToJump()(0);
  const double& Tjump      = ipvcur->getRefToFieldJump()(0);
        double& Cp         = ipvcur->getRefToExtraDofFieldCapacityPerUnitField()(0);
  const double& mechSource0 = ipvprev->getConstRefToMechanicalSource()(0);
        double& mechSource = ipvcur->getRefToMechanicalSource()(0);
        double& dmechSourcedt = ipvcur->getRefTodMechanicalSourcedField()(0,0);
        STensor3& dmechSourcedf = ipvcur->getRefTodMechanicalSourcedF()[0];

  // data for J2 law
  IPNonLinearTVP* q1 = ipvcur->getIPNonLinearTVP();
  const IPNonLinearTVP* q0 = ipvprev->getIPNonLinearTVP();
  // static STensor63 dCalgdeps;
  STensor43& elasticL = ipvcur->getRefToElasticTangentModuli();

  // Compute Stress
  _viscoLaw.constitutive(F0,F1,P,q0,q1,Tangent,T0,T,gradT0,gradT,fluxT,dPdT,dqdgradT,dqdT,dqdF,w,dwdt,dwdf,mechSource,dmechSourcedt,dmechSourcedf,stiff,&elasticL);

  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);
  ipvcur->setRefToLinearConductivity(_viscoLaw.getConductivityTensor());
  ipvcur->setRefToStiff_alphaDilatation(_viscoLaw.getStiff_alphaDilatation());

  Cp = _viscoLaw.getExtraDofStoredEnergyPerUnitField(T);
}

// NonLinearTVENonLinearTVP Law Interface =================================================================== END
