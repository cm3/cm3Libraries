//
// Author: Van Dung NGUYEN, (C) 2016
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef DG3DMULTISCALECOHESIVEMATERIALLAW_H_
#define DG3DMULTISCALECOHESIVEMATERIALLAW_H_

#include "dG3DCohesiveMaterialLaw.h"
#include "nonLinearMicroBC.h"

class GeneralMultiscaleBulkFollwedCohesive3DLaw : public GeneralBulkFollwedCohesive3DLaw{
	protected:
		bool _rotateRVEFollowingInterfaceNormal; // true if performing rotation of RVE following local basis
		nonLinearMicroBC* _microBCFailure;
		bool _checkFailureOnsetWithInterfaceNormal;

  public:
    #ifndef SWIG
    GeneralMultiscaleBulkFollwedCohesive3DLaw(const int num, const bool init = true);
    GeneralMultiscaleBulkFollwedCohesive3DLaw(const GeneralMultiscaleBulkFollwedCohesive3DLaw& src);
    virtual ~GeneralMultiscaleBulkFollwedCohesive3DLaw();
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){} // nothing to do for this law
    virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const = 0;
    virtual void createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const = 0;

    virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false) = 0;
    virtual void checkCohesiveInsertion(IPStateBase* ipsm, IPStateBase* ipsp, const bool forcedInsert= false) const = 0;
    virtual void transferInterfaceDataToBulk(IPVariable* ipv, const IPVariable* ipvprev,  const bool stiff) const = 0;

		virtual materialLaw* clone() const = 0;

		virtual bool getRotationOfRVEFollowingInterfaceLocalBasis() const {return _rotateRVEFollowingInterfaceNormal;};
		virtual bool getCheckFailureOnsetWithInterfaceNormal() const {return _checkFailureOnsetWithInterfaceNormal;}

		nonLinearMicroBC* getMicroBCFailure() {return _microBCFailure;}
		const nonLinearMicroBC* getMicroBCFailure() const {return _microBCFailure;}

		bool isSwitchedToFailureBC() const;
    #endif // SWIG
		void addMicroBCFailure(const nonLinearMicroBC* mbc);
		void setRotationOfRVEFollowingInterfaceLocalBasis(const bool fl) {_rotateRVEFollowingInterfaceNormal = fl;};
		void setCheckFailureOnsetWithInterfaceNormal(const bool fl) {_checkFailureOnsetWithInterfaceNormal = fl;}
};

class dG3DMultiscaleCohesiveLaw : public GeneralMultiscaleBulkFollwedCohesive3DLaw {
  protected:
    bool _fixedCrackFaceFlag; // introduce a fixed crack length over RVE
		double _fixedCrackFace;

  public:
    dG3DMultiscaleCohesiveLaw(const int num);
    void setFixedCrackFace(const double crackFace) {_fixedCrackFaceFlag = true; _fixedCrackFace = crackFace;};
    #ifndef SWIG
    dG3DMultiscaleCohesiveLaw(const dG3DMultiscaleCohesiveLaw& src);
    virtual ~dG3DMultiscaleCohesiveLaw(){}
		virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;

    virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent =false);
    virtual void checkCohesiveInsertion(IPStateBase* ipsm, IPStateBase* ipsp, const bool forcedInsert= false) const;
    virtual void transferInterfaceDataToBulk(IPVariable* ipv, const IPVariable* ipvprev,  const bool stiff) const;
		virtual materialLaw* clone() const {return new dG3DMultiscaleCohesiveLaw(*this);};
		virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{}; // do nothing
    #endif // SWIG
};

class TwoFieldMultiscaleCohesive3DLaw : public GeneralMultiscaleBulkFollwedCohesive3DLaw{
	protected:
    bool _fixedCrackFaceFlag; // introduce a fixed crack length over RVE
		double _fixedCrackFace;

  public:
    TwoFieldMultiscaleCohesive3DLaw(const int num);
    void setFixedCrackFace(const double crackFace) {_fixedCrackFaceFlag = true; _fixedCrackFace = crackFace;};
    #ifndef SWIG
    TwoFieldMultiscaleCohesive3DLaw(const TwoFieldMultiscaleCohesive3DLaw& src);
    virtual ~TwoFieldMultiscaleCohesive3DLaw(){}
		virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;

    virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent =false);
    virtual void checkCohesiveInsertion(IPStateBase* ipsm, IPStateBase* ipsp, const bool forcedInsert= false) const;
    virtual void transferInterfaceDataToBulk(IPVariable* ipv, const IPVariable* ipvprev,  const bool stiff) const;
		virtual materialLaw* clone() const {return new TwoFieldMultiscaleCohesive3DLaw(*this);};
		virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{}; // do nothing
    #endif // SWIG
};


class MultiscaleAdhesive3DLaw : public GeneralMultiscaleBulkFollwedCohesive3DLaw{

  public:
    MultiscaleAdhesive3DLaw(const int num);
    #ifndef SWIG
    MultiscaleAdhesive3DLaw(const MultiscaleAdhesive3DLaw& src);
    virtual ~MultiscaleAdhesive3DLaw(){}
		virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;

    virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent =false);
    virtual void checkCohesiveInsertion(IPStateBase* ipsm, IPStateBase* ipsp, const bool forcedInsert= false) const;
    virtual void transferInterfaceDataToBulk(IPVariable* ipv, const IPVariable* ipvprev,  const bool stiff) const;
		virtual materialLaw* clone() const {return new MultiscaleAdhesive3DLaw(*this);};
		virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const{}; // do nothing
    #endif // SWIG

};
#endif // DG3DMULTISCALECOHESIVEMATERIALLAW_H_
