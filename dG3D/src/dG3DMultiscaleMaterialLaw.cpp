//
// Author: Van Dung NGUYEN, (C) 2016
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "dG3DMultiscaleMaterialLaw.h"
#include "math.h"
#include "nonLinearMechSolver.h"
#include "dG3DMultiscaleCohesiveMaterialLaw.h"

bool dG3DMultiscaleMaterialLawBase::withEnergyDissipation() const {return _microSolver->withEnergyDissipation();};


dG3DMultiscaleMaterialLawBase::dG3DMultiscaleMaterialLawBase(const int lnum, const int tag)
                            : dG3DMaterialLaw(lnum,0,false),numericalMaterial(tag){}

dG3DMultiscaleMaterialLawBase::dG3DMultiscaleMaterialLawBase(const dG3DMultiscaleMaterialLawBase& src)
                            : dG3DMaterialLaw(src),numericalMaterial(src){};
void dG3DMultiscaleMaterialLawBase::setMacroSolver(const nonLinearMechSolver* sv){
  dG3DMaterialLaw::setMacroSolver(sv);
  numericalMaterial::_macroSolver = sv;
};

void dG3DMultiscaleMaterialLawBase::fillElasticStiffness(double & rho, STensor43& L){
  printf("get elastic properties\n");
  this->assignMeshId(0,0);
  nonLinearMechSolver* solver = this->createMicroSolver(0,0);
	solver->tangentAveragingFlag(true);
	solver->setExtractCohesiveLawFromMicroDamage(false);
	solver->setExtractIrreversibleEnergyFlag(false);
	solver->initMicroSolver();

	L = solver->getHomogenizationState(IPStateBase::current)->getHomogenizedTangentOperator_F_F();
  rho = solver->getHomogenizedDensity();
  delete solver;
};

dG3DElasticMultiscaleMaterialLaw::dG3DElasticMultiscaleMaterialLaw(const int lnum, const int tag):
			dG3DMultiscaleMaterialLawBase(lnum,tag),_elasticPotential(NULL){
};

dG3DElasticMultiscaleMaterialLaw::dG3DElasticMultiscaleMaterialLaw(const dG3DElasticMultiscaleMaterialLaw& src)
                            : dG3DMultiscaleMaterialLawBase(src)
{
  if (src._elasticPotential) _elasticPotential = src._elasticPotential->clone();
  else _elasticPotential = NULL;
};
dG3DElasticMultiscaleMaterialLaw::~dG3DElasticMultiscaleMaterialLaw(){
  if (_elasticPotential) delete _elasticPotential;
};

void dG3DElasticMultiscaleMaterialLaw::initLaws(const std::map<int,materialLaw*> &maplaw){
  if (!_initialized){
    this->fillElasticStiffness(_rho,elasticStiffness);
		elasticStiffness.print("elasticStiffness");
    if (_elasticPotential != NULL) delete _elasticPotential;
    _elasticPotential = new largeStrainAnisotropicPotential(getNum(),elasticStiffness);
    _initialized = true;
  }
};

void dG3DElasticMultiscaleMaterialLaw::createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  IPVariable* ipvi = new  LinearElasticDG3DIPVariable(hasBodyForce,inter);
  IPVariable* ipv1 = new  LinearElasticDG3DIPVariable(hasBodyForce,inter);
  IPVariable* ipv2 = new  LinearElasticDG3DIPVariable(hasBodyForce,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void dG3DElasticMultiscaleMaterialLaw::createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  ipv = new  LinearElasticDG3DIPVariable(hasBodyForce,inter);
}


void dG3DElasticMultiscaleMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac, const bool dTangent)
{
  /* get ipvariable */
  dG3DIPVariableBase* ipvcur = static_cast<dG3DIPVariableBase*>(ipv);;
  const dG3DIPVariableBase* ipvprev = static_cast<const dG3DIPVariableBase*>(ipvp);;
  /* strain xyz */
  const STensor3& F = ipvcur->getConstRefToDeformationGradient();
  STensor3& P = ipvcur->getRefToFirstPiolaKirchhoffStress();
	STensor43& H = ipvcur->getRefToTangentModuli();

	_elasticPotential->constitutive(F,P,stiff,&H,dTangent,ipvcur->getPtrTodCmdFm());

  ipvcur->setRefToDGElasticTangentModuli(this->elasticStiffness);
}
double dG3DElasticMultiscaleMaterialLaw::scaleFactor() const{
	return elasticStiffness(0,0,0,0);
};

double dG3DElasticMultiscaleMaterialLaw::soundSpeed() const
{
  Msg::Warning("dG3DElasticMultiscaleMaterialLaw::soundSpeed() is not correct");
	return 0.;
};

dG3DMultiscaleMaterialLaw::dG3DMultiscaleMaterialLaw(const int lnum, const int tag)
                            : dG3DMultiscaleMaterialLawBase(lnum,tag),
                            _blockDamageAfterFailureOnset(false),
                            _lostSolutionUniquenssTolerance(-1.e10){};
                            
dG3DMultiscaleMaterialLaw::dG3DMultiscaleMaterialLaw(const dG3DMultiscaleMaterialLaw& src)
                            : dG3DMultiscaleMaterialLawBase(src),
                            _blockDamageAfterFailureOnset(src._blockDamageAfterFailureOnset),
                            _lostSolutionUniquenssTolerance(src._lostSolutionUniquenssTolerance){};
                            

void dG3DMultiscaleMaterialLaw::initLaws(const std::map<int,materialLaw*> &maplaw){
  if (!_initialized){
    this->fillElasticStiffness(_rho,elasticStiffness);

		if (_blockDamageAfterFailureOnset and (_lostSolutionUniquenssTolerance<0.)){
      // find a smallest tolerance
			_lostSolutionUniquenssTolerance = 1e10;
			for (std::map<int,materialLaw*>::const_iterator it = maplaw.begin(); it != maplaw.end(); it++){
				const GeneralBulkFollwedCohesive3DLaw* cohLaw = dynamic_cast<const GeneralBulkFollwedCohesive3DLaw*>(it->second);
				if (cohLaw!= NULL){
					if (_lostSolutionUniquenssTolerance > cohLaw->getLostSolutionUniquenssTolerance()){
						_lostSolutionUniquenssTolerance = cohLaw->getLostSolutionUniquenssTolerance();
					}
				}
			}
			printf("tolerance of lost of ellipticity at bulk ip %f \n",_lostSolutionUniquenssTolerance);
		}

    if (_lostSolutionUniquenssTolerance < 0){
      _lostSolutionUniquenssTolerance = 0.;
    }
    _initialized = true;
  }
};

bool dG3DMultiscaleMaterialLaw::brokenCheck(const IPVariable* ipv) const{
	const dG3DMultiscaleIPVariable* mipv = dynamic_cast<const dG3DMultiscaleIPVariable*>(ipv);
  if (mipv != NULL)
    return mipv->getConstRefToLostEllipticityCriterion() >= 0.;
  else{
    Msg::Error("dG3DMultiscaleIPVariable must be used dG3DMultiscaleMaterialLaw::brokenCheck");
    return false;
  }
};

void dG3DMultiscaleMaterialLaw::createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const{
  this->createIPState(true,ips,hasBodyForce, state_,ele,nbFF_,GP,gpt);
};

void dG3DMultiscaleMaterialLaw::createIPState(const bool isSolve, IPStateBase* &ips,bool hasBodyForce, const bool* state,
                                 const MElement *ele, const int nbFF, const IntPt *GP, const int gpt) const{
	bool oninter = true;
	const MInterfaceElement* iele = dynamic_cast<const MInterfaceElement*>(ele);
	if (iele == NULL) oninter = false;

	IPVariable* ipvi = new dG3DMultiscaleIPVariable(hasBodyForce,oninter);
	IPVariable* ipv1 = new dG3DMultiscaleIPVariable(hasBodyForce,oninter);
	IPVariable* ipv2 = new dG3DMultiscaleIPVariable(hasBodyForce,oninter);
	if(ips != NULL) delete ips;
	ips = new IP3State(state,ipvi,ipv1,ipv2);

  // if a cohesive law is added, ipstate at interface is created based on combined fracture law
  // otherwise, only blocked failure will be considered
  // _blockDamageAfterFailureOnset is active for bulk homogenization
  // and interface in case of fullDG formulation

  if (isSolve){
    int el = ele->getNum();
		nonLinearMechSolver* solver =  this->createMicroSolver(el,gpt);
		if (_blockDamageAfterFailureOnset){
			solver->setLostSolutionUniquenssTolerance(_lostSolutionUniquenssTolerance);
			bool checkFailure = true;
			bool withNormal = false;
			solver->setCheckFailureOnset(checkFailure,withNormal);
		}
		else{
			solver->setCheckFailureOnset(false,false);
		}
		solver->initMicroSolver();
		ips->setMicroSolver(solver);
  }
};

void dG3DMultiscaleMaterialLaw::createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const{
  bool oninter = true;
  const MInterfaceElement* iele = dynamic_cast<const MInterfaceElement*>(ele);
  if (iele == NULL) oninter = false;
  ipv = new dG3DMultiscaleIPVariable(hasBodyForce,oninter);
};

void dG3DMultiscaleMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{

};

void dG3DMultiscaleMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff, const bool checkfrac, const bool dTangent){
	dG3DMultiscaleIPVariable* mipv = NULL;
	const dG3DMultiscaleIPVariable* mipvprev = NULL;

	FractureCohesive3DIPVariable* ipvFrac = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
	if (ipvFrac != NULL){
		mipv = dynamic_cast<dG3DMultiscaleIPVariable*>(ipvFrac->getIPvBulk());
		const FractureCohesive3DIPVariable* ipvprevFrac = dynamic_cast<const FractureCohesive3DIPVariable*>(ipvprev);
		mipvprev = dynamic_cast<const dG3DMultiscaleIPVariable*>(ipvprevFrac->getIPvBulk());
	}
	else{
		mipv = dynamic_cast<dG3DMultiscaleIPVariable*>(ipv);
		mipvprev = dynamic_cast<const dG3DMultiscaleIPVariable*>(ipvprev);
	}

  nonLinearMechSolver* solver = mipv->getMicroSolver();
  // set kinematics input
  const STensor3& F = mipv->getConstRefToDeformationGradient();

	solver->getMicroBC()->setDeformationGradient(F);

	  // set tangent averaging flag
  solver->tangentAveragingFlag(stiff);
  // solve micro problem
  bool success = solver->microSolve();
  if (success)
  {
    // get homogenized properties form solved micro problem
    mipv->getRefToFirstPiolaKirchhoffStress() = solver->getHomogenizationState(IPStateBase::current)->getHomogenizedStress();

    mipv->getRefToDefoEnergy() = solver->getHomogenizationState(IPStateBase::current)->getDeformationEnergy();
    mipv->getRefToPlasticEnergy() = solver->getHomogenizationState(IPStateBase::current)->getPlasticEnergy();
    mipv->getRefToIrreversibleEnergy() = solver->getHomogenizationState(IPStateBase::current)->getIrreversibleEnergy();
    mipv->getRefToLostEllipticityCriterion() = solver->getHomogenizationState(IPStateBase::current)->getLostOfSolutionUniquenessCriterion();

    if (stiff){
      mipv->getRefToTangentModuli() = solver->getHomogenizationState(IPStateBase::current)->getHomogenizedTangentOperator_F_F();
      mipv->getRefToDIrreversibleEnergyDDeformationGradient() = solver->getHomogenizationState(IPStateBase::current)->getHomogenizedTangentOperator_IrreversibleEnergy_F();
    };
    mipv->setRefToDGElasticTangentModuli(elasticStiffness);
    
  }
  else
  {
    // to force solver make other NR step with a smaller time step
    mipv->getRefToFirstPiolaKirchhoffStress()(0,0) = sqrt(-1.);
    mipv->getRefToFirstPiolaKirchhoffStress()(1,1) = sqrt(-1.);
    mipv->getRefToFirstPiolaKirchhoffStress()(2,2) = sqrt(-1.);
  }
};

void dG3DMultiscaleMaterialLaw::setElasticStiffness(IPVariable* ipv) const{
  dG3DIPVariableBase* dGipv =dynamic_cast<dG3DIPVariableBase*>(ipv);
  if (dGipv){
    dGipv->setRefToDGElasticTangentModuli(elasticStiffness);
	}
  else
    Msg::Error("dG3DIPVariableBase must be used");
};

void dG3DMultiscaleMaterialLaw::initialIPVariable(IPVariable* ipv, bool stiff){
	dG3DIPVariableBase* dGipv =dynamic_cast<dG3DIPVariableBase*>(ipv);
  if (dGipv){
		dGipv->setRefToDGElasticTangentModuli(elasticStiffness);
		dGipv->getRefToTangentModuli() = elasticStiffness;
	}
	else{
		Msg::Error("dG3DIPVariableBase must be used");
	};
};


MultiscaleThermoMechanicsDG3DMaterialLaw::MultiscaleThermoMechanicsDG3DMaterialLaw(const int lnum, const int tag)
                            : dG3DMaterialLaw(lnum,0,false),numericalMaterial(tag){};

MultiscaleThermoMechanicsDG3DMaterialLaw::MultiscaleThermoMechanicsDG3DMaterialLaw(const MultiscaleThermoMechanicsDG3DMaterialLaw& src):
	dG3DMaterialLaw(src),numericalMaterial(src),linearK(src.linearK),Stiff_alphaDilatation(src.Stiff_alphaDilatation){};


void MultiscaleThermoMechanicsDG3DMaterialLaw::fillElasticStiffness(double& rho, STensor43& L, STensor3& Klinear, STensor3& alphaDilatation){
	printf("get elastic properties\n");

  this->assignMeshId(0,0);
  nonLinearMechSolver* solver = this->createMicroSolver(0,0);
	solver->tangentAveragingFlag(true);
	solver->initMicroSolver();

	rho = solver->getHomogenizedDensity();
	L = solver->getHomogenizationState(IPStateBase::current)->getHomogenizedTangentOperator_F_F();
	Klinear = solver->getHomogenizationState(IPStateBase::current)->getHomogenizedTangentOperator_gradT_gradT(0,0);
	alphaDilatation = solver->getHomogenizationState(IPStateBase::current)->getHomogenizedTangentOperator_F_T(0);

  delete solver;
};

void MultiscaleThermoMechanicsDG3DMaterialLaw::initLaws(const std::map<int,materialLaw*> &maplaw){
  if (!_initialized){
    this->fillElasticStiffness(_rho,elasticStiffness,linearK,Stiff_alphaDilatation);
    _initialized = true;
  }
};

bool MultiscaleThermoMechanicsDG3DMaterialLaw::withEnergyDissipation() const  {
  return _microSolver->withEnergyDissipation();
}

void MultiscaleThermoMechanicsDG3DMaterialLaw::createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const{
  this->createIPState(true,ips,hasBodyForce, state_,ele,nbFF_,GP,gpt);
};

void MultiscaleThermoMechanicsDG3DMaterialLaw::createIPState(const bool isSolve, IPStateBase* &ips,bool hasBodyForce, const bool* state,
                                 const MElement *ele, const int nbFF, const IntPt *GP, const int gpt) const{

	bool oninter = true;
	const MInterfaceElement* iele = dynamic_cast<const MInterfaceElement*>(ele);
	if (iele == NULL) oninter = false;

	IPVariable* ipvi = new MultiscaleThermoMechanicsDG3DIPVariable(hasBodyForce,oninter);
	IPVariable* ipv1 = new MultiscaleThermoMechanicsDG3DIPVariable(hasBodyForce,oninter);
	IPVariable* ipv2 = new MultiscaleThermoMechanicsDG3DIPVariable(hasBodyForce,oninter);
	if(ips != NULL) delete ips;
	ips = new IP3State(state,ipvi,ipv1,ipv2);

	if (isSolve){
		int el = ele->getNum();
		nonLinearMechSolver* solver = this->createMicroSolver(el,gpt);
		solver->initMicroSolver();
		ips->setMicroSolver(solver);
	}
};

void MultiscaleThermoMechanicsDG3DMaterialLaw::createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const{
  bool oninter = true;
  const MInterfaceElement* iele = dynamic_cast<const MInterfaceElement*>(ele);
  if (iele == NULL) oninter = false;
  ipv = new MultiscaleThermoMechanicsDG3DIPVariable(hasBodyForce,oninter);
};

void MultiscaleThermoMechanicsDG3DMaterialLaw::checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{
  // do nothing
};

void MultiscaleThermoMechanicsDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff, const bool checkfrac, const bool dTangent){
 MultiscaleThermoMechanicsDG3DIPVariable* mipv = dynamic_cast<MultiscaleThermoMechanicsDG3DIPVariable*>(ipv);
  const MultiscaleThermoMechanicsDG3DIPVariable* mipvprev = dynamic_cast<const MultiscaleThermoMechanicsDG3DIPVariable*>(ipvprev);

  // kinematic variables
  const STensor3& F = mipv->getConstRefToDeformationGradient();
  const SVector3& gradT= mipv->getConstRefToGradField()[0];
  const double T = mipv->getConstRefToField(0);
  const double T0 = mipvprev->getConstRefToField(0);
  // set to solver
  nonLinearMechSolver* solver = mipv->getMicroSolver();
  solver->getMicroBC()->setDeformationGradient(F);
  solver->getMicroBC()->setConstitutiveExtraDofDiffusionGradient(0,gradT);
  solver->getMicroBC()->setConstitutiveExtraDofDiffusionValue(0,T);
  solver->tangentAveragingFlag(stiff);
  // solve microscopic problem
  bool success = solver->microSolve();
  if (success)
  {
    const homogenizedData* homoData = solver->getHomogenizationState(IPStateBase::current);
    // get stress
    mipv->getRefToFirstPiolaKirchhoffStress() = homoData->getHomogenizedStress();
    mipv->getRefToFlux()[0] = homoData->getHomogenizedConstitutiveExtraDofFlux(0);
    mipv->getRefToMechanicalSource()(0) = homoData->getHomogenizedConstitutiveExtraDofMechanicalSource(0);
    mipv->getRefToExtraDofFieldCapacityPerUnitField()(0) = homoData->getHomogenizedConstitutiveExtraDofFieldCapacityPerUnitField(0);

    double&w = mipv->getRefToFieldSource()(0);
    // thermal source is compute from previous step to avoid derivatives
    double Cpprev = mipvprev->getConstRefToExtraDofFieldCapacityPerUnitField()(0);
    w = -Cpprev*(T-T0)/this->getTimeStep();

    if (stiff){
      // stress
      mipv->getRefToTangentModuli() = homoData->getHomogenizedTangentOperator_F_F();
      mipv->getRefTodPdGradField()[0] = homoData->getHomogenizedTangentOperator_F_gradT(0);
      mipv->getRefTodPdField()[0] = homoData->getHomogenizedTangentOperator_F_T(0);;


      // flux
      mipv->getRefTodFluxdGradField()[0][0] = homoData->getHomogenizedTangentOperator_gradT_gradT(0,0);
      mipv->getRefTodFluxdField()[0][0] =  homoData->getHomogenizedTangentOperator_gradT_T(0,0);
      mipv->getRefTodFluxdF()[0] = homoData->getHomogenizedTangentOperator_gradT_F(0);


      double &dwdT = mipv->getRefTodFieldSourcedField()(0,0);
      SVector3& dwdgradT = mipv->getRefTodFieldSourcedGradField()[0][0];
      STensor3 &dwdF = mipv->getRefTodFieldSourcedF()[0];
      dwdT = -Cpprev/this->getTimeStep();
      STensorOperation::zero(dwdgradT);
      STensorOperation::zero(dwdF);

      mipv->getRefTodMechanicalSourcedField()(0,0) = homoData->getHomogenizedTangentOperator_ConstitutiveExtraDofMechanicalSource_T(0,0);
      mipv->getRefTodMechanicalSourcedGradField()[0][0] = homoData->getHomogenizedTangentOperator_ConstitutiveExtraDofMechanicalSource_gradT(0,0);
      mipv->getRefTodMechanicalSourcedF()[0] = homoData->getHomogenizedTangentOperator_ConstitutiveExtraDofMechanicalSource_F(0);
    }
  }
  else
  {
    // to force solver make other NR step with a smaller time step  
    mipv->getRefToFirstPiolaKirchhoffStress()(0,0) = sqrt(-1);
    mipv->getRefToFirstPiolaKirchhoffStress()(1,1) = sqrt(-1);
    mipv->getRefToFirstPiolaKirchhoffStress()(2,2) = sqrt(-1);
  }
};

void MultiscaleThermoMechanicsDG3DMaterialLaw::setElasticStiffness(IPVariable* ipv) const{
  ThermoMechanicsDG3DIPVariableBase* dGipv =dynamic_cast<ThermoMechanicsDG3DIPVariableBase*>(ipv);
  if (dGipv){
    dGipv->setRefToDGElasticTangentModuli(elasticStiffness);
    dGipv->setConstRefToLinearK(this->linearK,0,0);
    dGipv->setConstRefToLinearSymmetrizationCoupling(this->Stiff_alphaDilatation,0);
  }
  else
    Msg::Error("ThermoMechanicsDG3DIPVariableBase must be used");
};

void MultiscaleThermoMechanicsDG3DMaterialLaw::initialIPVariable(IPVariable* ipv, bool stiff){
  ThermoMechanicsDG3DIPVariableBase* dGipv =dynamic_cast<ThermoMechanicsDG3DIPVariableBase*>(ipv);
  if (dGipv){
    dGipv->setRefToDGElasticTangentModuli(elasticStiffness);
    dGipv->setConstRefToLinearK(this->linearK,0,0);
    dGipv->setConstRefToLinearSymmetrizationCoupling(this->Stiff_alphaDilatation,0);

    dGipv->getRefToTangentModuli() = elasticStiffness;
    dGipv->getRefTodFluxdGradField()[0][0] = linearK;
    dGipv->getRefTodPdField()[0] = Stiff_alphaDilatation;
  }
  else{
    Msg::Error("ThermoMechanicsDG3DIPVariableBase must be used");
  };
};
