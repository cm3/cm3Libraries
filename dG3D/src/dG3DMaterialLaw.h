//
//
// Description: Class of materials for non linear dg
//
//
// Author:  <L. Noels>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef _DG3DMATERIALLAW_H_
#define _DG3DMATERIALLAW_H_
#ifndef SWIG
#include "dG3DIPVariable.h"
#include "mlawVEVPMFH.h"
#include "mlawJ2linear.h"
#include "mlawVUMATinterface.h"
#include "mlawTransverseIsotropic.h"
#include "mlawTransverseIsoCurvature.h"
#include "mlawTransversUserDirection.h"
#include "mlawTransverseIsoYarnB.h"
#include "mlawAnisotropic.h"
#include "mlawAnisotropicStoch.h"
#include "j2IsotropicHardening.h"
#include "mlawLinearThermoMechanics.h"
#include "mlawJ2VMSmallStrain.h"
#include "mlawSMP.h"
#include "mlawPhenomenologicalSMP.h"
#include "mlawEOS.h"
#include "mlawCrystalPlasticity.h"
#include "mlawGursonUMAT.h"
#include "mlawVEVPUMAT.h"
#include "mlawIMDEACPUMAT.h"
#include "mlawViscoelastic.h"
#include "mlawLinearElecTherMech.h"
#include "mlawAnIsotropicElecTherMech.h"
#include "mlawElecSMP.h"
#include "mlawAnIsotropicTherMech.h"
#include "mlawCohesive.h"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include "mlawJ2FullyCoupledThermoMechanics.h"
#include "mlawHyperelasticDamage.h"
#include "mlawTFAMaterialLaws.h"
#include "mlawLocalDamageJ2SmallStrain.h"
#include "mlawLocalDamageIsotropicElasticity.h"
#include "mlawNonLocalPorousCoalescence.h"
#include "MIMOFunction.h"
#include "mlawLinearElecMagTherMech.h"
#include "mlawLinearElecMagInductor.h"
#include "dG3DDeepMaterialNetworks.h"
#include "mlawGenericTM.h"
#include "mlawGenericRM.h"
#include "mlawElecMagGenericThermoMech.h"
#include "mlawElecMagInductor.h"
#include "mlawNonLinearTVM.h"
#include "mlawNonLinearTVP.h"
#include "mlawNonLinearTVENonLinearTVP.h"
#include "mullinsEffect.h"
#include "mlawHyperelasticWithPotential.h"
#include "extraVariablesAtGaussPoints.h"
#endif
#if defined(HAVE_TORCH)
#include <torch/torch.h>
#include <torch/script.h>
#endif

class dG3DMaterialLaw : public materialLaw{
 public:

  STensor43 elasticStiffness;
  STensor43 TotalelasticStiffness;

 protected:
  double _rho;
  double _NLRho;
  bool _useBarF;

 public:
  virtual void setUseBarF(bool useBarF) { _useBarF=useBarF;}
  virtual void setNLDensity(double rho)   {_NLRho=rho;}
  #ifndef SWIG
  dG3DMaterialLaw(const int num, const double rho,
                   const bool init=true) : materialLaw(num,init), _rho(rho), _useBarF(false), _NLRho(0.)
  {

  }
  dG3DMaterialLaw(const dG3DMaterialLaw &source) : materialLaw(source),
                                                      _rho(source._rho), elasticStiffness(source.elasticStiffness),TotalelasticStiffness(source.TotalelasticStiffness),
         _useBarF(source._useBarF), _NLRho(source._NLRho)

  {

  }
  virtual ~dG3DMaterialLaw()
  {
  }
  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const=0;
  // To allow initialization of bulk ip in case of fracture
  virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const=0;
  virtual void calledBy2lawFracture(){}
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw)=0;
  virtual double density() const{return _rho;}
  virtual double nLDensity() const{return _NLRho;}
  // As to be defined for explicit scheme. If law for implicit only
  // please define this function with return 0
  virtual double soundSpeed() const=0;
  // If law enable fracture the bool checkfrac allow to detect fracture. This bool is unused otherwise
  // The bool is necessery for computation of matrix by perturbation in this case the check of fracture
  // has not to be performed.
  virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false)=0;

  // 3D to 1D
  virtual void stress3DTo1D(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff=true, const bool checkfrac=true);

  // 3D to 1D with constant T
  virtual void stress3DTo1D_constantTriaxiality(double T, IPVariable* ipv, const IPVariable* ipvprev, const bool stiff=true, const bool checkfrac=true);

  // 3D to 2D
  virtual void stress3DTo2D(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
  virtual void stress3DTo2D_XZ(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);

  virtual void fillElasticStiffness(double E, double nu, STensor43 &tangent) const;
  virtual void fillElasticStiffness(double Ex,double Ey,double Ez,double Vxy,double Vxz,double Vyz,double MUxy,double MUxz,double MUyz,double alpha,double beta,double gamma,
                                    STensor43 &tangent) const;
  virtual void setElasticStiffness(IPVariable* ipv) const {Msg::Error("define setElasticStiffness if using multiscale simulation");};
  virtual void initialIPVariable(IPVariable* ipv, bool stiff);
  virtual materialLaw* clone() const = 0;
  virtual void constitutive(const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPVariable *q0,       // array of initial internal variable
                            IPVariable *q1,              // updated array of internal variable (in ipvcur on output),
			    STensor43 &Tangent,         // constitutive tangents (output)
			    const bool stiff,
                            STensor43* elasticTangent = NULL,
                            const bool dTangent =false,
                            STensor63* dCalgdeps = NULL
			    ) const
      {
        Msg::Error("dG3DMaterialLaw constitutive not defined");
      }
  virtual bool getUseBarF() const { return _useBarF;}
  virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const  = 0;
  virtual materialLaw *getNonLinearSolverMaterialLaw() = 0;
  
  virtual void getParameterDerivative(const IPVariable*ipv, std::vector< fullMatrix<double> >& dPdPn, std::vector< fullMatrix<double> >&dPdPv) const
  {
     Msg::Error("Function getParameterDerivative has not been defined for this material law");
  };
  virtual void reset_Parameter(std::vector<std::vector<double>>& Para_Norm, std::vector<std::vector<double>>& Para_Wt, const double Vf=0.0)
  {
     Msg::Error("Function reset_Parameter has not been defined for this material law");
  };
  virtual void reset_Parameter(const char* Para)
  {
     Msg::Error("Function reset_Parameter has not been defined for this material law");
  };
  #endif // SWIG
};

class dG3DMaterialLawWithTangentByPerturbation : public dG3DMaterialLaw
{
  protected:
    dG3DMaterialLaw* _stressLaw;
    double _pertFactor;
    dG3DIPVariableBase* ipvTemp; // cache

  public:
    dG3DMaterialLawWithTangentByPerturbation(dG3DMaterialLaw& law, double pert);
    #ifndef SWIG
    dG3DMaterialLawWithTangentByPerturbation(const dG3DMaterialLawWithTangentByPerturbation& src);
    virtual ~dG3DMaterialLawWithTangentByPerturbation();
    virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    // To allow initialization of bulk ip in case of fracture
    virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce,  const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
    virtual void calledBy2lawFracture();
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw);
    virtual double density() const;
    // As to be defined for explicit scheme. If law for implicit only
    // please define this function with return 0
    virtual double soundSpeed() const;
    // If law enable fracture the bool checkfrac allow to detect fracture. This bool is unused otherwise
    // The bool is necessery for computation of matrix by perturbation in this case the check of fracture
    // has not to be performed.
    virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
    virtual void fillElasticStiffness(double E, double nu, STensor43 &tangent) const;
    virtual void setElasticStiffness(IPVariable* ipv) const;
    virtual bool withEnergyDissipation() const;
    // function to define when one IP is broken for block dissipation
    virtual bool brokenCheck(const IPVariable* ipv) const;
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;

    virtual void setTime(const double ctime,const double dtime);
    virtual materialLaw::matname getType() const;
    virtual double scaleFactor() const;
    virtual bool isHighOrder() const;
    virtual bool isNumeric() const;
    virtual int getNumberOfExtraDofsDiffusion() const;
    virtual double getInitialExtraDofStoredEnergyPerUnitField() const;
    virtual double getExtraDofStoredEnergyPerUnitField(double T) const;
    virtual double getCharacteristicLength() const;
    virtual void setMacroSolver(const nonLinearMechSolver* sv);

    virtual materialLaw* clone() const {return new dG3DMaterialLawWithTangentByPerturbation(*_stressLaw,_pertFactor);};
    virtual void constitutive(const STensor3& F0,         // initial deformation gradient (input @ time n)
                              const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                              STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                          // contains the initial values on input
                              const IPVariable *q0,       // array of initial internal variable
                              IPVariable *q1,              // updated array of internal variable (in ipvcur on output),
                              STensor43 &Tangent,         // constitutive tangents (output)
                              const bool stiff,
                              STensor43* elasticTangent = NULL,
                              const bool dTangent =false,
                              STensor63* dCalgdeps = NULL) const;
    #endif // SWIG
    virtual void setUseBarF(bool useBarF);
    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const;
    virtual materialLaw *getNonLinearSolverMaterialLaw();
};

class dG3DLinearElasticMaterialLaw : public dG3DMaterialLaw{
	protected:
	#ifndef SWIG
    smallStrainLinearElasticPotential _elasticLaw;
	#endif

	public:
		dG3DLinearElasticMaterialLaw(const int num, const double rho, const double E, const double nu);
		#ifndef SWIG
		dG3DLinearElasticMaterialLaw(const dG3DLinearElasticMaterialLaw& src);
		virtual ~dG3DLinearElasticMaterialLaw(){
		};
		virtual materialLaw::matname getType() const {return materialLaw::linearElastic;}
		virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  // To allow initialization of bulk ip in case of fracture
		virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
		virtual void initLaws(const std::map<int,materialLaw*> &maplaw);
		virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
		virtual double scaleFactor() const;
		virtual double soundSpeed() const;
		virtual materialLaw* clone() const {return new dG3DLinearElasticMaterialLaw(*this);};
		virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing
    virtual bool withEnergyDissipation() const {return false;};
    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
      Msg::Error("getConstNonLinearSolverMaterialLaw in dG3DLinearElasticMaterialLaw does not exist");
      return NULL;
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
      Msg::Error("getNonLinearSolverMaterialLaw in dG3DLinearElasticMaterialLaw does not exist");
      return NULL;
    }
#endif //SWIG
};

class dG3DLinearElasticAnisotropicMaterialLaw : public dG3DMaterialLaw{
	protected:
	#ifndef SWIG
    smallStrainLinearElasticPotential _elasticLaw;
	#endif

	public:
		dG3DLinearElasticAnisotropicMaterialLaw(const int num, const double rho,
		                                        const double Ex, const double Ey, const double Ez,
                                                        const double Vxy, const double Vxz, const double Vyz,
                                                        const double MUxy, const double MUxz, const double MUyz,
                                                        const double alpha, const double beta, const double gamma);
		#ifndef SWIG
		dG3DLinearElasticAnisotropicMaterialLaw(const dG3DLinearElasticAnisotropicMaterialLaw& src);
		virtual ~dG3DLinearElasticAnisotropicMaterialLaw(){
		};
		virtual materialLaw::matname getType() const {return materialLaw::linearElastic;}
		virtual void createIPState(IPStateBase* &ips,bool hasBodyForce,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  // To allow initialization of bulk ip in case of fracture
		virtual void createIPVariable(IPVariable* &ipv,bool hasBodyForce,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
		virtual void initLaws(const std::map<int,materialLaw*> &maplaw);
		virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
		virtual double scaleFactor() const;
		virtual double soundSpeed() const;
		virtual materialLaw* clone() const {return new dG3DLinearElasticAnisotropicMaterialLaw(*this);};
		virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing
    virtual bool withEnergyDissipation() const {return false;};
    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
      Msg::Error("getConstNonLinearSolverMaterialLaw in dG3DLinearElasticAnisotropicMaterialLaw does not exist");
      return NULL;
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
      Msg::Error("getNonLinearSolverMaterialLaw in dG3DLinearElasticAnisotropicMaterialLaw does not exist");
      return NULL;
    }
#endif //SWIG
};

class dG3DLinearElasticTriclinicMaterialLaw : public dG3DMaterialLaw{
	protected:
	#ifndef SWIG
    smallStrainLinearElasticPotential _elasticLaw;
	#endif

	public:
		dG3DLinearElasticTriclinicMaterialLaw(const int num, const double rho,
		                                      const double C00, const double C11, const double C22, const double C33, const double C44, const double C55,
		                                      const double C01, const double C02, const double C03, const double C04, const double C05,
		                                      const double C12, const double C13, const double C14, const double C15,
		                                      const double C23, const double C24, const double C25,
		                                      const double C34, const double C35,
		                                      const double C45);
		#ifndef SWIG
		dG3DLinearElasticTriclinicMaterialLaw(const dG3DLinearElasticTriclinicMaterialLaw& src);
		virtual ~dG3DLinearElasticTriclinicMaterialLaw(){
		};
		virtual materialLaw::matname getType() const {return materialLaw::linearElastic;}
		virtual void createIPState(IPStateBase* &ips,bool hasBodyForce,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  // To allow initialization of bulk ip in case of fracture
		virtual void createIPVariable(IPVariable* &ipv,bool hasBodyForce,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
		virtual void initLaws(const std::map<int,materialLaw*> &maplaw);
		virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
		virtual double scaleFactor() const;
		virtual double soundSpeed() const;
		virtual materialLaw* clone() const {return new dG3DLinearElasticTriclinicMaterialLaw(*this);};
		virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing
    virtual bool withEnergyDissipation() const {return false;};
    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
      Msg::Error("getConstNonLinearSolverMaterialLaw in dG3DLinearElasticTriclinicMaterialLaw does not exist");
      return NULL;
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
      Msg::Error("getNonLinearSolverMaterialLaw in dG3DLinearElasticTriclinicMaterialLaw does not exist");
      return NULL;
    }
#endif //SWIG
};

class dG3DHyperelasticMaterialLaw : public dG3DMaterialLaw
{
	protected:
	#ifndef SWIG
    mlawHyperelasticWithPotential _elasticLaw;
	#endif

	public:
		dG3DHyperelasticMaterialLaw(const int num, const double rho, const elasticPotential& potential);
		#ifndef SWIG
		dG3DHyperelasticMaterialLaw(const dG3DHyperelasticMaterialLaw& src);
		virtual ~dG3DHyperelasticMaterialLaw(){};

		virtual materialLaw::matname getType() const {return _elasticLaw.getType();}
		virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  // To allow initialization of bulk ip in case of fracture
		virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
		virtual void initLaws(const std::map<int,materialLaw*> &maplaw);
		virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
		virtual double scaleFactor() const;
		virtual double soundSpeed() const;
		virtual materialLaw* clone() const {return new dG3DHyperelasticMaterialLaw(*this);};
		virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing
    virtual bool withEnergyDissipation() const {return false;};
    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
      return &_elasticLaw;
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
      return &_elasticLaw;
    }
    #endif //SWIG
};

class DMNBasedDG3DMaterialLaw : public dG3DMaterialLaw
{
  protected:
    #ifndef SWIG
    NetworkInteraction* _networkInteraction;
    std::map<int, dG3DMaterialLaw*> _mapLaw;
    std::vector<int> _fixedComp;
    bool _withPerturbation;
    double _pertTol;
    bool _withSubstepping;
    int _numStep;
    std::map<int,std::set<int> > _viewBulkIPs;
    std::map<std::pair<int,int>,std::set<int> > _viewInterfaceIPs;
    std::set<int> _gpForViewAll;
    DeepMaterialNetwork* _DMNForSetting;
    bool _lineSearch;
    //
    DeepMaterialNetwork* createDMN(const MElement *ele, int gpt) const;
    #endif //SWIG
  public:
    DMNBasedDG3DMaterialLaw(const int num, const double rho, const Tree* T, bool withPer=false, double tol=1e-6);
    DMNBasedDG3DMaterialLaw(const int num, const double rho, const std::string interactionFname, bool withPer=false, double tol=1e-6);
    DeepMaterialNetwork& getDMNFromSetting();
    void addLaw(int matIndex, dG3DMaterialLaw* law);
    void setLineSearch(bool fl);
    void fixDofsByComp(int comp);
    void setSubstepping(bool fl, int ns);
    void viewInternalData(int i);
    void viewIPAveragePerPhase(int iphase, int icomp);
    void viewIPAverage(int icomp);
    void viewBulkIPs(int ele, int gp);
    void viewInterfaceIPs(int eleminus, int eleplus, int gp);
    void viewAllIPs(int gp);
    #ifndef SWIG
    DMNBasedDG3DMaterialLaw(const DMNBasedDG3DMaterialLaw& src);
    virtual ~DMNBasedDG3DMaterialLaw();
    virtual materialLaw::matname getType() const {return materialLaw::DMN;}
    virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  // To allow initialization of bulk ip in case of fracture
    virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw);
    virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
    virtual double scaleFactor() const {return 1.;};
    virtual double soundSpeed() const {return 0.;};
    virtual materialLaw* clone() const {return new DMNBasedDG3DMaterialLaw(*this);};
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing
    virtual bool withEnergyDissipation() const {return false;};
    virtual void initialIPVariable(IPVariable* ipv, bool stiff);
    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
      Msg::Error("getConstNonLinearSolverMaterialLaw in DMNBasedDG3DMaterialLaw does not exist");
      return NULL;
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
      Msg::Error("getNonLinearSolverMaterialLaw in DMNBasedDG3DMaterialLaw does not exist");
      return NULL;
    }
    #endif //SWIG
};

class MultipleDG3DMaterialLaw : public dG3DMaterialLaw
{
  protected:
    #ifndef SWIG
    std::vector<dG3DMaterialLaw*> _allLaws;
    generalMapping* _weightGenerator;
    #endif //SWIG
  public:
    MultipleDG3DMaterialLaw(const int num, const double rho);
    void addLaw(dG3DMaterialLaw* law);
    void setWeightGenerator(const generalMapping* wGen);
    #ifndef SWIG
    MultipleDG3DMaterialLaw(const MultipleDG3DMaterialLaw& src);
    virtual ~MultipleDG3DMaterialLaw();
    virtual void setTime(const double t, double dtime);
    virtual materialLaw::matname getType() const {return materialLaw::MultipleLaws;}
    virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  // To allow initialization of bulk ip in case of fracture
    virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw);
    virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
    virtual double scaleFactor() const {return 1.;};
    virtual double soundSpeed() const {return 0.;};
    virtual materialLaw* clone() const {return new MultipleDG3DMaterialLaw(*this);};
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
    virtual bool withEnergyDissipation() const;
    virtual void initialIPVariable(IPVariable* ipv, bool stiff);
    virtual void setMacroSolver(const nonLinearMechSolver* sv);
    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
      Msg::Error("getConstNonLinearSolverMaterialLaw in DMNBasedDG3DMaterialLaw does not exist");
      return NULL;
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
      Msg::Error("getNonLinearSolverMaterialLaw in DMNBasedDG3DMaterialLaw does not exist");
      return NULL;
    }
    #endif //SWIG
};

class ANNBasedDG3DMaterialLaw : public dG3DMaterialLaw{
  public:
    enum KinematicInput{EGL=1,U=2,smallStrain=3};  // EGL - Green Lagraneg strain, U-Biot strain, smallStrain=small strain
	protected:
	#ifndef SWIG
    MIMOFunction* _mimoFct;
    int _numberOfInternalVariables;
    bool _tangentByPerturbation;
    double _pertTol;
    KinematicInput _kinematicInput; // using to speicfy the input kinematic variable, EGL is chosen by default
    // note: input =EGL (0.5*(FT*F-I))) -> ouput =second PK
    //       input =U (sqr(FTF)-I) ->  ouput =second PK
    //       input =smallStrain (0.5(FT+F)-I)-> ouput =cauchy=P
    std::map<int,double> _initialInternalValues; // initial values of internal state
	#endif

	public:
		ANNBasedDG3DMaterialLaw(const int num, const double rho, const int numInternalVars, const MIMOFunction& fct, bool pert=false, double tol = 1e-5);
		void setKinematicInput(const int i);
    void setInitialInternalComp(int comp, double val);
    #ifndef SWIG
		ANNBasedDG3DMaterialLaw(const ANNBasedDG3DMaterialLaw& src);
		virtual ~ANNBasedDG3DMaterialLaw();
		virtual materialLaw::matname getType() const {return materialLaw::ANN;}
		virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  // To allow initialization of bulk ip in case of fracture
		virtual void createIPVariable(IPVariable* &ipv,  bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
		virtual void initLaws(const std::map<int,materialLaw*> &maplaw);
		virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
		virtual double scaleFactor() const {return 1.;};
		virtual double soundSpeed() const;
		virtual materialLaw* clone() const {return new ANNBasedDG3DMaterialLaw(*this);};
		virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing
    virtual bool withEnergyDissipation() const {return false;};

    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
      Msg::Error("getConstNonLinearSolverMaterialLaw in ANNBasedDG3DMaterialLaw does not exist");
      return NULL;
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
      Msg::Error("getNonLinearSolverMaterialLaw in ANNBasedDG3DMaterialLaw does not exist");
      return NULL;
    }

  private:
    void convertFullMatrixToSTensor43(const fullMatrix<double>& DSDE, STensor43& K) const;
    void setInitialInternalState(fullMatrix<double>& q) const;
    #endif //SWIG
};

class torchANNBasedDG3DMaterialLaw : public dG3DMaterialLaw{
  public:
    enum KinematicInput{EGL=1,U=2,smallStrain=3,EGLInc=4,UInc=5,smallStrainInc=6};  // EGL - Green Lagraneg strain, U-Biot strain, smallStrain=small strain; Inc for increments
  protected:
    #ifndef SWIG
    int _numberOfInput;
    int _numberOfInternalVariables;
    double _initialValue_h;
    int _time_arg; // If one of the extra variables is time, store its position in the array. @Mohib
    bool _NeedExtraNorm;
    bool _DoubleInput;
    int _numberOfExtraInput; // Number of additional inputs other than 6 (or 3 for 2D) strain components @Mohib
    std::vector<double> _initialValue_ExtraInput; // initial values for the extra inputs @Mohib
    std::vector<double> _normParams_ExtraInput; // Container for additional inputs, it is needed for  normalization @Mohib
    std::string _fileExtraInputsGp;
    ExtraVariablesAtGaussPoints _extraVariablesAtGaussPoints;
    double _ctime; // variable for storing current time @Mohib

#if defined(HAVE_TORCH)
    torch::jit::script::Module module;
#endif
    double _EXXmean;       //Green Lagraneg strain Normalization
    double _EXXstd;
    double _EXYmean;
    double _EXYstd;
    double _EYYmean;
    double _EYYstd;
    double _EYZmean;
    double _EYZstd;
    double _EZZmean;
    double _EZZstd;
    double _EZXmean;
    double _EZXstd;

    double _SXXmean;
    double _SXXstd;
    double _SXYmean;
    double _SXYstd;
    double _SYYmean;
    double _SYYstd;
    double _SYZmean;
    double _SYZstd;
    double _SZZmean;
    double _SZZstd;
    double _SZXmean;
    double _SZXstd;
    bool _tangentByPerturbation;
    double _pertTol;
#if defined(HAVE_TORCH)
    torch::Tensor VXX;
    torch::Tensor VXY;
    torch::Tensor VYY;
    torch::Tensor VYZ;
    torch::Tensor VZZ;
    torch::Tensor VZX;
#endif

    KinematicInput _kinematicInput; // using to speicfy the input kinematic variable, EGL is chosen by default
    // note: input =EGL (0.5*(FT*F-I))) -> ouput =second PK
    //       input =U (sqr(FTF)-I) ->  ouput =second PK
    //       input =smallStrain (0.5(FT+F)-I)-> ouput =cauchy=P
	#endif

	public:
		torchANNBasedDG3DMaterialLaw(const int num, const double rho, const int numberOfInput,
                const int numInternalVars, const char* nameTorch, const double EXXmean, const double EXXstd, const double EXYmean,
                const double EXYstd, const double EYYmean, const double EYYstd, const double EYZmean, const double EYZstd, const double EZZmean,
                const double EZZstd, const double EZXmean, const double EZXstd, const double SXXmean, const double SXXstd, const double SXYmean,
                const double SXYstd, const double SYYmean, const double SYYstd, const double SYZmean, const double SYZstd, const double SZZmean,
                const double SZZstd, const double SZXmean, const double SZXstd, bool pert=false, double tol = 1e-5);

		void setKinematicInput(const int i);
		void setNeedExtraNorm (const bool NeedExtraNorm){
		    _NeedExtraNorm = NeedExtraNorm;
		    if(_NeedExtraNorm) Msg::Info("Extra Norm is applied");
                };

                void setDoubleInput (const bool DoubleInput){
		    _DoubleInput = DoubleInput;
		    if(_DoubleInput) Msg::Info("Double Input is applied");
                };


        void setInitialHValue(double initialValue_h) {_initialValue_h=initialValue_h;};
        // Setter for no of Extra Input Parameters @Mohib
        void setNumExtraInput(const int value);
        // Setter for storing time position (arg) in the extra array @Mohib
        void setTimeArg(const int value);
        // Setter for init values of Extra Inputs @Mohib
        void setInitialExtraInput(const double value);
        // Setter for storing norm params of Extra Inputs @Mohib
        void setNormExtraInp(const double mean, const double std);



        void setFileExtraInputsGp(const std::string fname)
        {
            _fileExtraInputsGp=fname;
            _extraVariablesAtGaussPoints.fill(fname);
            //_extraVariablesAtGaussPoints.print();
            //const std::vector<double> &rf=_extraVariablesAtGaussPoints.getFieldAtClosestLocation(SVector3(25,28.873,28.873));
            //double val=_extraVariablesAtGaussPoints.returnFieldAtClosestLocation(0,SVector3(25,28.873,28.873));
            //Msg::Info(" Field 0 at GP is %f",val);
        }

        void fillExtraInputAtGp(int nb1, int nb2)
        {
           _extraVariablesAtGaussPoints.fillExtraInputAtGp(nb1,nb2);
        }
    #ifndef SWIG
		torchANNBasedDG3DMaterialLaw(const torchANNBasedDG3DMaterialLaw& src);
		virtual ~torchANNBasedDG3DMaterialLaw();
		virtual materialLaw::matname getType() const {return materialLaw::torchANN;}
    virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    // Overloading for handling extra variables needed at GP @Mohib
    // virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0, const bool extraInput=0) const;
    // To allow initialization of bulk ip in case of fracture
		virtual void createIPVariable(IPVariable* &ipv,  bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
    // Overloading for handling extra variables needed at GP @Mohib
    // virtual void createIPVariable(IPVariable* &ipv,  bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt, const bool extraInput=0) const;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw);
		virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
    // Set time at GP @Mohib
    virtual void setTime(const double ctime, const double dtime);
    virtual double scaleFactor() const {return 1.;}
		virtual double soundSpeed() const;
		virtual materialLaw* clone() const {return new torchANNBasedDG3DMaterialLaw(*this);};
		virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing
    virtual bool withEnergyDissipation() const {return false;};

    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
      Msg::Error("getConstNonLinearSolverMaterialLaw in torchANNBasedDG3DMaterialLaw does not exist");
      return NULL;
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
      Msg::Error("getNonLinearSolverMaterialLaw in torchANNBasedDG3DMaterialLaw does not exist");
      return NULL;
    }

  private:
    void convertFullMatrixToSTensor43(const fullMatrix<double>& DSDE, STensor43& K) const;
    void Normalize_strain(const fullMatrix<double>& E1, vector<float>& E_norm) const;
    void Normalize_geo(const fullMatrix<double>& Geo1, vector<float>& Geo_norm) const;
    void Normalize_dt(const fullMatrix<double>& Geo1, vector<float>& Geo_norm) const;
#if defined(HAVE_TORCH)
    void InverseNormalize_stress(const torch::Tensor& S_norm, fullMatrix<double>& S) const;
    void RNNstress_stiff(const fullMatrix<double>& E0,const fullMatrix<double>& E1, const torch::Tensor& h0, torch::Tensor& h1, fullMatrix<double>& S, const bool stiff, fullMatrix<double>& DSDE);
    void RNNstressGeo_stiff(const fullMatrix<double>& E0, const fullMatrix<double>& E1, const fullMatrix<double>& Geo0, const fullMatrix<double>& Geo1, const torch::Tensor& h0, torch::Tensor& h1, fullMatrix<double>& S, const bool stiff, fullMatrix<double>& DSDE);


#endif
    #endif //SWIG

};


class StochDMNDG3DMaterialLaw : public dG3DMaterialLaw{
  protected:
    #ifndef SWIG
    bool _ReadTree;
    bool _porous;
    int _TotalLevel;
    int _NLeaf;
    int _NInterface;
    int _N_Node;
    int _NPara_Wt;
    int _NPara_Norm;
    int _Dim;
    double _Vf;
    double _tol;   
    std::vector<std::vector<int>> _LevelNode;  // keep the Nodes at each level
    std::vector<int> _NodeLevel;    // _NodeLevel[Node] = Level of Node
    std::vector<int> _Mat_Id;
    std::vector<int> _Parent;       //Parent[Node] = Parent of Node
    std::vector<std::vector<int>> _Child;      // Child[Node][0] = left child ; Child[Node][1] = right child of Node
 
    
    std::vector<SPoint2> _Para_Wt;
    std::vector<SPoint3> _Para_Norm; 
    fullMatrix<double> _ParaAngle;   
    std::vector<dG3DMaterialLaw*> _mapLaw;
    fullMatrix<double> _Nv;
    fullMatrix<double> _NTV;
    fullMatrix<double> _NT;
    fullMatrix<double> _V, _I;
    std::vector<double> _VA;
    std::vector<double> _VI;  
    
    fullMatrix<double> _C;
    fullMatrix<double> Jacobian;
        
    void fill_NLocal(const int pos, double N[][3])const;
    void fill_W_WI(const int pos,  double W[][2]);
    void fill_Matrices();
    void read_parameter(const char *ParaFile);
    void read_TreeData(const char *TreeFile); 
    virtual void getLeafOfInterface(const int pos, int &node_start, int &node_end) const;
    virtual void TwoPhasesInteract(const std::vector<STensor43>& C, fullMatrix<double> &mat, const int pos);
    virtual void getdVAdP(std::vector< std::vector <SPoint2> >& dVAdPv) const;
    virtual void getdNLocaldP(const int pos, double dNda[][3], double dNdb[][3]) const;
    virtual void get_matrixdVdPv(std::vector< std::vector <SPoint2> >& dVAdPv, std::vector<std::vector<std::vector<std::vector<double>>>>& d_VdPv) const; 
    virtual void get_matrixdNdP(std::vector< std::vector <SPoint2> >& dVAdPv, std::vector<std::vector<std::vector<std::vector<double>>>>& d_NvdPv, std::vector<std::vector<std::vector<std::vector<double>>>>& d_NvdPn, std::vector<std::vector<std::vector<std::vector<double>>>>& d_NTdPn) const; 
    virtual void get_matrixdNTdPv(std::vector<std::vector<std::vector<std::vector<double>>>>& d_NTdPv) const;
    #endif //SWIG
  public:
    StochDMNDG3DMaterialLaw(const int num, const double rho, const double E,const double nu, const char *ParaFile, const bool ReadTree=false, const bool porous=false,const double tol=1e-6);
    void addLaw(dG3DMaterialLaw* law);
    #ifndef SWIG
    StochDMNDG3DMaterialLaw(const StochDMNDG3DMaterialLaw &source);
    virtual ~StochDMNDG3DMaterialLaw();

    virtual void setTime(const double t,const double dtime);
    virtual materialLaw::matname getType() const {return materialLaw::StochDMN;}

    virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
    virtual void initialIPVariable(IPVariable* ipv, bool stiff);   
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw);
    virtual double scaleFactor() const {return 1.;};
    virtual double soundSpeed() const {return 0.;};
    virtual materialLaw* clone() const {return new StochDMNDG3DMaterialLaw(*this);};
    virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);

    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
    virtual bool withEnergyDissipation() const {return false;};
    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
      Msg::Error("getConstNonLinearSolverMaterialLaw in  StochDMNDG3DMaterialLaw does not exist");
      return NULL;
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
      Msg::Error("getNonLinearSolverMaterialLaw in  StochDMNDG3DMaterialLaw does not exist");
      return NULL;
    }
    virtual void setMacroSolver(const nonLinearMechSolver* sv);
    virtual void getParameterDerivative(const IPVariable*ipv, std::vector< fullMatrix<double> >& dPdPn, std::vector< fullMatrix<double> >&dPdPv) const;
    virtual void reset_Parameter(std::vector<std::vector<double>>& Para_Norm, std::vector<std::vector<double>>& Para_Wt, const double Vf = 0.0);
    virtual void reset_Parameter(const char* Para);
    #endif //SWIG
 };   

class J2SmallStrainDG3DMaterialLaw : public dG3DMaterialLaw
{
 protected:
  #ifndef SWIG
   mlawJ2VMSmallStrain _j2law;
  #endif //SWIG

 public:
  J2SmallStrainDG3DMaterialLaw(const int num,const double rho,
                   double E,const double nu,const double sy0,const double h,const double tol=1.e-6,
                   const bool tangentByPart= false, const double pert=1e-8);
  J2SmallStrainDG3DMaterialLaw(const int num,const double rho,
                   double E,const double nu,const J2IsotropicHardening &_j2IH,const double tol=1.e-6,
                   const bool tangentByPart= false, const double pert=1e-8);
 #ifndef SWIG
  J2SmallStrainDG3DMaterialLaw(const J2SmallStrainDG3DMaterialLaw &source);
  virtual ~J2SmallStrainDG3DMaterialLaw()
  {

  }
  // set the time of _j2law
   virtual void setTime(const double t,const double dtime){
    dG3DMaterialLaw::setTime(t,dtime);
    _j2law.setTime(t,dtime);
  }
  virtual materialLaw::matname getType() const {return _j2law.getType();}
  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
  virtual double soundSpeed() const{return _j2law.soundSpeed();} // or change value ??
  virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
  virtual const double bulkModulus() const {return _j2law.bulkModulus();}
	virtual double scaleFactor() const{return _j2law.scaleFactor();};
	virtual materialLaw* clone() const{return new J2SmallStrainDG3DMaterialLaw(*this);};
	virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
  virtual bool withEnergyDissipation() const {return _j2law.withEnergyDissipation();};

	virtual void setMacroSolver(const nonLinearMechSolver* sv){
		dG3DMaterialLaw::setMacroSolver(sv);
		_j2law.setMacroSolver(sv);
	};

  virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
  {
    return &_j2law;
  }
  virtual materialLaw *getNonLinearSolverMaterialLaw()
  {
    return &_j2law;
  }
 #endif // SWIG
};

class AnisotropicPlasticityDG3DMaterialLaw : public dG3DMaterialLaw
{
 protected:
    mlawAnisotropicPlasticity* _anisoLaw;

 public:

    AnisotropicPlasticityDG3DMaterialLaw(const int num, std::string lawType, const double rho,
                   double E,const double nu,const J2IsotropicHardening &_j2IH,const double tol=1.e-6,
                   const bool matrixBypert=false, const double tolPert=1e-8);
    void setStrainOrder(const int order);
    void setStressFormulation(int s);
    void setYieldParameters(const std::vector<double>& vals);
    void setYieldParameters(const fullVector<double>& vals);
    #ifndef SWIG
    AnisotropicPlasticityDG3DMaterialLaw(const AnisotropicPlasticityDG3DMaterialLaw &source);
    virtual ~AnisotropicPlasticityDG3DMaterialLaw();

    // set the time of _anisoLaw
     virtual void setTime(const double t,const double dtime){
      dG3DMaterialLaw::setTime(t,dtime);
      _anisoLaw->setTime(t,dtime);
    }
    virtual materialLaw::matname getType() const {return _anisoLaw->getType();}
    virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
    virtual double soundSpeed() const{return _anisoLaw->soundSpeed();} // or change value ??
    virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
    virtual const double bulkModulus() const {return _anisoLaw->bulkModulus();}
    virtual double scaleFactor() const{return _anisoLaw->scaleFactor();}
    virtual materialLaw* clone() const{return new AnisotropicPlasticityDG3DMaterialLaw(*this);};
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
    virtual bool withEnergyDissipation() const {return _anisoLaw->withEnergyDissipation();};
    virtual void setMacroSolver(const nonLinearMechSolver* sv){
      dG3DMaterialLaw::setMacroSolver(sv);
      _anisoLaw->setMacroSolver(sv);
    };
    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
      return _anisoLaw;
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
      return _anisoLaw;
    }
   #endif // SWIG
};

class J2LinearDG3DMaterialLaw : public dG3DMaterialLaw
{
    friend class EOSDG3DMaterialLaw;
 protected:
  mlawJ2linear _j2law;
  //double _E,_nu;
 private: // cache data
  //mutable STensor43 _tangent3D;
  //const STensor3 _ISTensor3;
  //mutable STensor3 tau,PKxyz,PKsh,invF,tauXYZ,Fp1,Fp0;
 public:
  J2LinearDG3DMaterialLaw(const int num,const double rho,
                   double E,const double nu,const double sy0,const double h,const double tol=1.e-6,
                   const bool matrixBypert=false, const double tolPert=1e-8);
  J2LinearDG3DMaterialLaw(const int num,const double rho,
                   double E,const double nu,const J2IsotropicHardening &_j2IH,const double tol=1.e-6,
                   const bool matrixBypert=false, const double tolPert=1e-8);
  void setStrainOrder(const int order);
  void setViscosityEffect(const viscosityLaw& v, const double p);
  void setStressFormulation(int s);
 #ifndef SWIG
  J2LinearDG3DMaterialLaw(const J2LinearDG3DMaterialLaw &source);
  virtual ~J2LinearDG3DMaterialLaw()
  {

  }
  // set the time of _j2law
   virtual void setTime(const double t,const double dtime){
    dG3DMaterialLaw::setTime(t,dtime);
    _j2law.setTime(t,dtime);
  }
  virtual materialLaw::matname getType() const {return _j2law.getType();}
  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
  virtual double soundSpeed() const{return _j2law.soundSpeed();} // or change value ??
  virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
  virtual const double bulkModulus() const {return _j2law.bulkModulus();}
	virtual double scaleFactor() const{return _j2law.scaleFactor();}
	virtual materialLaw* clone() const{return new J2LinearDG3DMaterialLaw(*this);};
	virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
  virtual bool withEnergyDissipation() const {return _j2law.withEnergyDissipation();};
	virtual void setMacroSolver(const nonLinearMechSolver* sv){
		dG3DMaterialLaw::setMacroSolver(sv);
		_j2law.setMacroSolver(sv);
	};
  virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
  {
    return &_j2law;
  }
  virtual materialLaw *getNonLinearSolverMaterialLaw()
  {
    return &_j2law;
  }
 #endif // SWIG
};

class TrescaDG3DMaterialLaw : public dG3DMaterialLaw
{
 protected:
    mlawNonLocalPorousThomasonLawWithMSS* _trescaLaw;
  //double _E,_nu;
 public:
  TrescaDG3DMaterialLaw(const int num,const double rho,
                   double E,const double nu,const J2IsotropicHardening &_j2IH,const double tol=1.e-6,
                   const bool matrixBypert=false, const double tolPert=1e-8);
  void setStrainOrder(const int order);
  void setStressFormulation(int s);
  void setSmoothFactor(double factor);
 #ifndef SWIG
  TrescaDG3DMaterialLaw(const TrescaDG3DMaterialLaw &source);
  virtual ~TrescaDG3DMaterialLaw();
   virtual void setTime(const double t,const double dtime){
    dG3DMaterialLaw::setTime(t,dtime);
    _trescaLaw->setTime(t,dtime);
  }
  virtual materialLaw::matname getType() const {return _trescaLaw->getType();}
  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw);
  virtual double soundSpeed() const{return _trescaLaw->soundSpeed();} // or change value ??
  virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
  virtual const double bulkModulus() const {return _trescaLaw->bulkModulus();}
	virtual double scaleFactor() const{return _trescaLaw->scaleFactor();}
	virtual materialLaw* clone() const{return new TrescaDG3DMaterialLaw(*this);};
	virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
  virtual bool withEnergyDissipation() const {return _trescaLaw->withEnergyDissipation();};
	virtual void setMacroSolver(const nonLinearMechSolver* sv){
		dG3DMaterialLaw::setMacroSolver(sv);
		_trescaLaw->setMacroSolver(sv);
	};
  virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
  {
    return _trescaLaw;
  }
  virtual materialLaw *getNonLinearSolverMaterialLaw()
  {
    return _trescaLaw;
  }
 #endif // SWIG
};

class HyperViscoElasticDG3DMaterialLaw : public dG3DMaterialLaw{
  #ifndef SWIG
  protected:
    mlawHyperViscoElastic _viscoLaw;

  #endif //SWIG
  public:
    HyperViscoElasticDG3DMaterialLaw(const int num, const double rho, const double E,const double nu,
                        const bool matrixbyPerturbation = false, const double pert = 1e-8);
    void setStrainOrder(const int order);
    void setViscoelasticMethod(const int method);
    void setViscoElasticNumberOfElement(const int N);
    void setViscoElasticData(const int i, const double Ei, const double taui);
    void setViscoElasticData_Bulk(const int i, const double Ki, const double ki);
    void setViscoElasticData_Shear(const int i, const double Gi, const double gi);
    void setViscoElasticData(const std::string filename);
    void setVolumeCorrection(double _vc, double _xivc, double _zetavc, double _dc, double _thetadc, double _pidc);
    void setAdditionalVolumeCorrections(const double V3, const double V4, const double V5, const double D3, const double D4, const double D5);
    void setExtraBranch_CompressionParameter(const double compCorrection, const double compCorrection_2, const double compCorrection_3);
    void setExtraBranchNLType(int type);
    void setTensionCompressionRegularisation(double tensionCompressionRegularisation);
    #ifndef SWIG
     HyperViscoElasticDG3DMaterialLaw(const HyperViscoElasticDG3DMaterialLaw &source);
    virtual ~HyperViscoElasticDG3DMaterialLaw(){}
    // set the time of _j2law
    virtual void setTime(const double t,const double dtime){
      dG3DMaterialLaw::setTime(t,dtime);
      _viscoLaw.setTime(t,dtime);
    }
    virtual materialLaw::matname getType() const {return _viscoLaw.getType();}
    virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
    virtual double soundSpeed() const{return _viscoLaw.soundSpeed();} // or change value ??
    virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
    virtual const double bulkModulus() const {return _viscoLaw.bulkModulus();}
    virtual double scaleFactor() const{return _viscoLaw.scaleFactor();}
    virtual materialLaw* clone() const{return new HyperViscoElasticDG3DMaterialLaw(*this);};
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
    virtual bool withEnergyDissipation() const {return _viscoLaw.withEnergyDissipation();};
    virtual void setMacroSolver(const nonLinearMechSolver* sv){
      dG3DMaterialLaw::setMacroSolver(sv);
      _viscoLaw.setMacroSolver(sv);
    };

    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
      return &_viscoLaw;
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
      return &_viscoLaw;
    }
    #endif //SWIG
};

class HyperViscoElastoPlasticPowerYieldDG3DMaterialLaw  : public dG3DMaterialLaw{
  protected:
    #ifndef SWIG
    mlawPowerYieldHyper _viscoLaw;
    #endif //SWIG
  public:
    HyperViscoElastoPlasticPowerYieldDG3DMaterialLaw(const int num, const double rho, const double E,const double nu, const double tol =1e-6,
                        const bool matrixbyPerturbation = false, const double pert = 1e-8);
    void setStrainOrder(const int order);
    void setViscoelasticMethod(const int method);
    void setViscoElasticNumberOfElement(const int N);
    void setViscoElasticData(const int i, const double Ei, const double taui);
    void setViscoElasticData_Bulk(const int i, const double Ki, const double ki);
    void setViscoElasticData_Shear(const int i, const double Gi, const double gi);
    void setViscoElasticData(const std::string filename);
    void setVolumeCorrection(double _vc, double _xivc, double _zetavc, double _dc, double _thetadc, double _pidc);
    void setAdditionalVolumeCorrections(const double V3, const double V4, const double V5, const double D3, const double D4, const double D5);
    void setExtraBranch_CompressionParameter(const double compCorrection, const double compCorrection_2, const double compCorrection_3);
    void setExtraBranchNLType(int type);
    void setTensionCompressionRegularisation(double tensionCompressionRegularisation);

    void setCompressionHardening(const J2IsotropicHardening& comp);
    void setTractionHardening(const J2IsotropicHardening& trac);
    void setKinematicHardening(const kinematicHardening& kin);
    void setViscosityEffect(const viscosityLaw& v, const double p);

    void setYieldPowerFactor(const double n);
    void nonAssociatedFlowRuleFactor(const double b);
    void setPlasticPoissonRatio(const double nup);
    void setNonAssociatedFlow(const bool flag);

    #ifndef SWIG
    HyperViscoElastoPlasticPowerYieldDG3DMaterialLaw(const HyperViscoElastoPlasticPowerYieldDG3DMaterialLaw& src);
    virtual ~HyperViscoElastoPlasticPowerYieldDG3DMaterialLaw(){};
    virtual void setTime(const double t,const double dtime){
      dG3DMaterialLaw::setTime(t,dtime);
      _viscoLaw.setTime(t,dtime);
    }
    virtual materialLaw::matname getType() const {return _viscoLaw.getType();}
    virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
    virtual double soundSpeed() const{return _viscoLaw.soundSpeed();} // or change value ??
    virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
    virtual const double bulkModulus() const {return _viscoLaw.bulkModulus();}
    virtual double scaleFactor() const{return _viscoLaw.scaleFactor();}
    virtual materialLaw* clone() const{return new HyperViscoElastoPlasticPowerYieldDG3DMaterialLaw(*this);};
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
    virtual bool withEnergyDissipation() const {return _viscoLaw.withEnergyDissipation();};
    virtual void setMacroSolver(const nonLinearMechSolver* sv){
      dG3DMaterialLaw::setMacroSolver(sv);
      _viscoLaw.setMacroSolver(sv);
    };
    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
      return &_viscoLaw;
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
      return &_viscoLaw;
    }
    #endif //SWIG
};


//======================================================VEVPDG3DMaterialLaw===============================================================//
class VEVPMFHDG3DMaterialLaw : public dG3DMaterialLaw{
  protected:
    #ifndef SWIG
    mlawVEVPMFH *_glaw;
    #endif //SWIG

  public:
    VEVPMFHDG3DMaterialLaw(const int num, const double rho, const char *propName);
    #ifndef SWIG
    VEVPMFHDG3DMaterialLaw(const VEVPMFHDG3DMaterialLaw &source);
    virtual ~VEVPMFHDG3DMaterialLaw();

    // set the time of _cplaw
    virtual void setTime(const double t,const double dtime);
    virtual materialLaw::matname getType() const {return _glaw->getType();}
    virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
    virtual double soundSpeed() const;// or change value ??
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const;
    virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
    virtual double scaleFactor() const;
    virtual materialLaw* clone() const{return new VEVPMFHDG3DMaterialLaw(*this);};
    virtual bool withEnergyDissipation() const {return _glaw->withEnergyDissipation();};
    virtual void setMacroSolver(const nonLinearMechSolver* sv);
    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
      return _glaw;
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
      return _glaw;
    }
    #endif
};

//======================================================ViscoelasticDG3DMaterialLaw===============================================================//
class ViscoelasticDG3DMaterialLaw : public dG3DMaterialLaw
{
    friend class EOSDG3DMaterialLaw;
 protected:
  mlawViscoelastic _Vislaw;
 private: // cache data

 public:

  ViscoelasticDG3DMaterialLaw(const int num,const double rho,
                              const double K, const std::string &array_eta, int array_size_eta, const std::string &array_mu,
                              const double tol=1.e-6, const bool pert= false, const double eps=1.e-8);


 #ifndef SWIG
  ViscoelasticDG3DMaterialLaw(const ViscoelasticDG3DMaterialLaw &source);
  virtual ~ViscoelasticDG3DMaterialLaw()
  {

  }
   virtual void setTime(const double t,const double dtime){
    dG3DMaterialLaw::setTime(t,dtime);
    _Vislaw.setTime(t,dtime);

  }
  virtual materialLaw::matname getType() const {return _Vislaw.getType();}
  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
  virtual double soundSpeed() const{return _Vislaw.soundSpeed();}
  virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
  virtual const double bulkModulus() const {return _Vislaw.bulkModulus();}
  virtual std::vector<double> read_file(const char* file_name, int file_size); //read from files
  virtual materialLaw* clone() const{return new ViscoelasticDG3DMaterialLaw(*this);};
  virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
  virtual bool withEnergyDissipation() const {return _Vislaw.withEnergyDissipation();};
  virtual void setMacroSolver(const nonLinearMechSolver* sv){
		dG3DMaterialLaw::setMacroSolver(sv);
		_Vislaw.setMacroSolver(sv);
	};
  virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
  {
    return &_Vislaw;
  }
  virtual materialLaw *getNonLinearSolverMaterialLaw()
  {
    return &_Vislaw;
  }
 #endif // SWIG
};


//--------------------------------------------EOSDG3DMaterialLaw (compatible with the other material laws)-------------------------//
class EOSDG3DMaterialLaw : public dG3DMaterialLaw
{
    friend class J2LinearDG3DMaterialLaw;
    friend class ViscoelasticDG3DMaterialLaw;
  protected:
  std::map<int,materialLaw*> maplaw;   // chosen deviatoric material law
  mlawEOS _EOSlaw;

  public:

//  static void cout2DMat(const STensor3 &a, const STensor3 &b, STensor3 &c);

  EOSDG3DMaterialLaw(const int num,
               const double rho,const double K, materialLaw *_mlaw, const double Pressure0 = 0.);
  // Accoustic EOS for non-artificial viscosity case
  EOSDG3DMaterialLaw(int flag, const int num,
          const double rho,const double P1, double P2, double P3, materialLaw *_mlaw, const double Pressure0 = 0.);
  // P1 = s, P2 = C0, P3 = Gamma: Hugoniot EOS for non-artificial viscosity case
  // P1 = K, P2 = c1, P3 = cl: Accoustic EOS for artificial viscosity case
  EOSDG3DMaterialLaw(int flag, const int num,
          const double rho,const double s, const double C0,double c1, double cl, const double Gamma, materialLaw *_mlaw);
  // Hugoniot EOS for artificial viscosity case
  EOSDG3DMaterialLaw(int flag, int flagC, const int num,
          const double rho,const double K, double c1, double cl, double ESqrd, double ESlin, materialLaw *_mlaw, const double Pressure0 = 0.);
 #ifndef SWIG
  EOSDG3DMaterialLaw(const EOSDG3DMaterialLaw &source);

  virtual ~EOSDG3DMaterialLaw()
  {

  }
   virtual void setTime(const double t,const double dtime){
    dG3DMaterialLaw::setTime(t,dtime);
    maplaw.begin()->second->setTime(t,dtime);
    _EOSlaw.setTime(t,dtime);
  }

  virtual materialLaw::matname getType() const {return materialLaw::EOS;}
  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
  virtual double soundSpeed() const {return _EOSlaw.soundSpeed();}
  virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
	virtual materialLaw* clone() const{return new EOSDG3DMaterialLaw(*this);};
	virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
  virtual bool withEnergyDissipation() const {return _EOSlaw.withEnergyDissipation();};
	virtual void setMacroSolver(const nonLinearMechSolver* sv){
		dG3DMaterialLaw::setMacroSolver(sv);
		_EOSlaw.setMacroSolver(sv);
		for (std::map<int,materialLaw*>::iterator it = maplaw.begin(); it != maplaw.end(); it++){
			it->second->setMacroSolver(sv);
		}
	};
  virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
  {
    return &_EOSlaw;
  }
  virtual materialLaw *getNonLinearSolverMaterialLaw()
  {
    return &_EOSlaw;
  }
 #endif // SWIG
};


class VUMATinterfaceDG3DMaterialLaw : public dG3DMaterialLaw
{

 protected:
  mlawVUMATinterface _vumatlaw;

 public:
  VUMATinterfaceDG3DMaterialLaw(const int num, const double rho,
                                const char *propName);
  virtual ~VUMATinterfaceDG3DMaterialLaw() {};

#ifndef SWIG
  VUMATinterfaceDG3DMaterialLaw(const VUMATinterfaceDG3DMaterialLaw &source);
  // set the time of _vumatlaw
  virtual void setTime(const double t,const double dtime){
    dG3DMaterialLaw::setTime(t,dtime);
    _vumatlaw.setTime(t,dtime);
  }
  virtual materialLaw::matname getType() const {return _vumatlaw.getType();}
  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
  virtual double soundSpeed() const{return _vumatlaw.soundSpeed();}
  virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
	virtual materialLaw* clone() const{return new VUMATinterfaceDG3DMaterialLaw(*this);};
	virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
  virtual bool withEnergyDissipation() const {return _vumatlaw.withEnergyDissipation();};
	virtual void setMacroSolver(const nonLinearMechSolver* sv){
		dG3DMaterialLaw::setMacroSolver(sv);
		_vumatlaw.setMacroSolver(sv);
	};
  virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
  {
    return &_vumatlaw;
  }
  virtual materialLaw *getNonLinearSolverMaterialLaw()
  {
    return &_vumatlaw;
  }
#endif
};

class TransverseIsotropicDG3DMaterialLaw : public dG3DMaterialLaw
{
 protected:
  mlawTransverseIsotropic _tilaw;
  double _E,_nu, _EA, _GA, _nu_minor;

 public:
  TransverseIsotropicDG3DMaterialLaw(const int num,const double rho,
                   double E,const double nu, const double EA,
		   const double GA, const double nu_minor,
                   const double Ax, const double Ay, const double Az);
 #ifndef SWIG
  TransverseIsotropicDG3DMaterialLaw(const TransverseIsotropicDG3DMaterialLaw &source);
  virtual ~TransverseIsotropicDG3DMaterialLaw(){}
  // set the time of _tilaw
   virtual void setTime(const double t,const double dtime){
    dG3DMaterialLaw::setTime(t,dtime);
    _tilaw.setTime(t,dtime);
  }
  virtual materialLaw::matname getType() const {return _tilaw.getType();}
  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
  virtual double soundSpeed() const{return _tilaw.soundSpeed();} // or change value ??
  virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
	virtual double scaleFactor() const{return _tilaw.scaleFactor();};
	virtual materialLaw* clone() const{return new TransverseIsotropicDG3DMaterialLaw(*this);};
	virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
  virtual bool withEnergyDissipation() const {return _tilaw.withEnergyDissipation();};
	virtual void setMacroSolver(const nonLinearMechSolver* sv){
		dG3DMaterialLaw::setMacroSolver(sv);
		_tilaw.setMacroSolver(sv);
	};
  virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
  {
    return &_tilaw;
  }
  virtual materialLaw *getNonLinearSolverMaterialLaw()
  {
    return &_tilaw;
  }

 #endif // SWIG
};




class TransverseIsoCurvatureDG3DMaterialLaw : public dG3DMaterialLaw
{
 protected:
  mlawTransverseIsoCurvature _tilaw;
  double _E,_nu, _EA, _GA, _nu_minor;

 public:
  TransverseIsoCurvatureDG3DMaterialLaw(const int num,const double rho,
                   double E,const double nu, const double EA,
		   const double GA, const double nu_minor,
                   const double Centroid_x, const double Centroid_y, const double Centroid_z,
                   const double PointStart1_x, const double PointStart1_y, const double PointStart1_z,
                   const double PointStart2_x,  const double PointStart2_y, const double PointStart2_z, const double init_Angle);
 #ifndef SWIG
  TransverseIsoCurvatureDG3DMaterialLaw(const TransverseIsoCurvatureDG3DMaterialLaw &source);
  virtual ~TransverseIsoCurvatureDG3DMaterialLaw(){}
  // set the time of _tilaw
   virtual void setTime(const double t,const double dtime){
    dG3DMaterialLaw::setTime(t,dtime);
    _tilaw.setTime(t,dtime);
  }
  virtual materialLaw::matname getType() const {return _tilaw.getType();}
  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
  virtual double soundSpeed() const{return _tilaw.soundSpeed();} // or change value ??
  virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
	virtual double scaleFactor() const{return _tilaw.scaleFactor();};
	virtual materialLaw* clone() const{return new TransverseIsoCurvatureDG3DMaterialLaw(*this);};
	virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
  virtual bool withEnergyDissipation() const {return _tilaw.withEnergyDissipation();};
	virtual void setMacroSolver(const nonLinearMechSolver* sv){
		dG3DMaterialLaw::setMacroSolver(sv);
		_tilaw.setMacroSolver(sv);
	};
  virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
  {
    return &_tilaw;
  }
  virtual materialLaw *getNonLinearSolverMaterialLaw()
  {
    return &_tilaw;
  }
 #endif // SWIG
};

class TransverseIsoUserDirectionDG3DMaterialLaw : public dG3DMaterialLaw
{
 protected:
  mlawTransverseIsoUserDirection _tilaw;
  double _E,_nu, _EA, _GA, _nu_minor;

 public:
  TransverseIsoUserDirectionDG3DMaterialLaw(const int num,const double rho,
                   double E,const double nu, const double EA,
                    const double GA, const double nu_minor,
                    const UserDirectionGenerator& dirGen);
 #ifndef SWIG
  TransverseIsoUserDirectionDG3DMaterialLaw(const TransverseIsoUserDirectionDG3DMaterialLaw &source);
  virtual ~TransverseIsoUserDirectionDG3DMaterialLaw(){}
  // set the time of _tilaw
   virtual void setTime(const double t,const double dtime){
    dG3DMaterialLaw::setTime(t,dtime);
    _tilaw.setTime(t,dtime);
  }
  virtual materialLaw::matname getType() const {return _tilaw.getType();}
  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
  virtual double soundSpeed() const{return _tilaw.soundSpeed();} // or change value ??
  virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
	virtual double scaleFactor() const{return _tilaw.scaleFactor();};
	virtual materialLaw* clone() const{return new TransverseIsoUserDirectionDG3DMaterialLaw(*this);};
	virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
  virtual bool withEnergyDissipation() const {return _tilaw.withEnergyDissipation();};
	virtual void setMacroSolver(const nonLinearMechSolver* sv){
		dG3DMaterialLaw::setMacroSolver(sv);
		_tilaw.setMacroSolver(sv);
	};
  virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
  {
    return &_tilaw;
  }
  virtual materialLaw *getNonLinearSolverMaterialLaw()
  {
    return &_tilaw;
  }
 #endif // SWIG
};

class TransverseIsoYarnBDG3DMaterialLaw : public dG3DMaterialLaw
{
 protected:
  mlawTransverseIsoYarnB _tilaw;

 public:
  //Remarks: Two constructors are defined, the first one is for the yarn and the second one is for the matrix.
  //Constructor for yarn
  TransverseIsoYarnBDG3DMaterialLaw(int lawnum,//tag of the material law
                                    double rho, //material density
                                    int physicnum,//physic number of
                                    char* meshfile, //vector container of YarnDirections
                                    int isunidir,   // 0:Woven yarn. 1:Straight yarn. 2:Homogenuous material
                                    double rDSvDT,  //To initialize '_CriticalrDSvDT'
                                    double MaxStrainRate, //The maximal value of the strain rate
			            double Cxx11,
		                    double Cxx21,
			            double Cxx22,
			            double Cxx31,
			            double Cxx32,
			            double Cyy11,
			            double Cyy21,
			            double Cyy22,
			            double Cyy31,
			            double Cyy32,
			            double Cxy11,
			            double Cxy21,
			            double Cxy22,
			            double Cxy31,
			            double Cxy32,
			            double Cyz11,
			            double Cyz21,
			            double Cyz22,
			            double Cyz31,
			            double Cyz32,
                                    double nuxy,
                                    double nuxz,
                                    double nuyz);

  //Constructor for matrix
  TransverseIsoYarnBDG3DMaterialLaw(int lawnum,//tag of the material law
                                    double rho, //material density
                                    int physicnum,//physic number of
                                    double rDSvDT,  //To initialize '_CriticalrDSvDT'
                                    double MaxStrainRate, //The maximal value of the strain rate
			            double Cxx11,
		                    double Cxx21,
			            double Cxx22,
			            double Cxx31,
			            double Cxx32,
			            double Cyy11,
			            double Cyy21,
			            double Cyy22,
			            double Cyy31,
			            double Cyy32,
			            double Cxy11,
			            double Cxy21,
			            double Cxy22,
			            double Cxy31,
			            double Cxy32,
			            double Cyz11,
			            double Cyz21,
			            double Cyz22,
			            double Cyz31,
			            double Cyz32,
                                    double nuxy,
                                    double nuxz,
                                    double nuyz);

 #ifndef SWIG

  virtual ~TransverseIsoYarnBDG3DMaterialLaw(){}
  // set the time of _tilaw

   virtual void setTime(const double t,const double dtime){
    dG3DMaterialLaw::setTime(t,dtime);
    _tilaw.setTime(t,dtime);
  }


  virtual materialLaw::matname getType() const{return _tilaw.getType();}

  virtual void createIPState(IPStateBase* &ips,  bool hasBodyForce,
                             const bool* state_=NULL,
                             const MElement *ele=NULL,
                             const int nbFF_=0,
                             const IntPt *GP=NULL,
                             const int gpt = 0) const;

  virtual void createIPVariable(IPVariable* &ipv,  bool hasBodyForce,
                                const MElement *ele,
                                const int nbFF,
                                const IntPt *GP,
                                const int gpt) const;

  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}

  virtual double soundSpeed() const{return _tilaw.soundSpeed();}
  virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
  virtual materialLaw* clone() const {return new TransverseIsoYarnBDG3DMaterialLaw(*this);};
  virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
  virtual bool withEnergyDissipation() const {return _tilaw.withEnergyDissipation();};
  TransverseIsoYarnBDG3DMaterialLaw(const TransverseIsoYarnBDG3DMaterialLaw &source);
	virtual void setMacroSolver(const nonLinearMechSolver* sv){
		dG3DMaterialLaw::setMacroSolver(sv);
		_tilaw.setMacroSolver(sv);
	};

  virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
  {
    return &_tilaw;
  }
  virtual materialLaw *getNonLinearSolverMaterialLaw()
  {
    return &_tilaw;
  }
 #endif // SWIG
};

class AnisotropicDG3DMaterialLaw : public dG3DMaterialLaw
{
 protected:
  mlawAnisotropic _tilaw;
 private: // cache data
  mutable STensor43 _tangent3D;
  const STensor3 _ISTensor3;
  mutable STensor3 tau,PKxyz,PKsh,invF,tauXYZ,Fp1,Fp0;
 public:
  AnisotropicDG3DMaterialLaw(const int num,const double rho,
				const double Ex,const double Ey,const double Ez,
				const double Vxy,const double Vxz,const double Vyz,
				const double MUxy,const double MUxz,const double MUyz,
                   		const double alpha, const double beta, const double gamma);
  AnisotropicDG3DMaterialLaw(const int num,const double rho,
				const double Ex,const double Ey,const double Ez,
				const double Vxy,const double Vxz,const double Vyz,
				const double MUxy,const double MUxz,const double MUyz,
                   		const double euler[3]);
 #ifndef SWIG
  AnisotropicDG3DMaterialLaw(const AnisotropicDG3DMaterialLaw &source);
  virtual ~AnisotropicDG3DMaterialLaw(){}
  // set the time of _tilaw
   virtual void setTime(const double t,const double dtime){
    dG3DMaterialLaw::setTime(t,dtime);
    _tilaw.setTime(t,dtime);
  }
  virtual materialLaw::matname getType() const {return _tilaw.getType();}
  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw);
  virtual double soundSpeed() const{return _tilaw.soundSpeed();} // or change value ??
  virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
	virtual double scaleFactor() const{return _tilaw.scaleFactor();}
	virtual materialLaw* clone() const{return new AnisotropicDG3DMaterialLaw(*this);};
	virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
  virtual bool withEnergyDissipation() const {return _tilaw.withEnergyDissipation();};
	virtual void setMacroSolver(const nonLinearMechSolver* sv){
		dG3DMaterialLaw::setMacroSolver(sv);
		_tilaw.setMacroSolver(sv);
	};
  virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
  {
    return &_tilaw;
  }
  virtual materialLaw *getNonLinearSolverMaterialLaw()
  {
    return &_tilaw;
  }
 #endif // SWIG
};


class AnisotropicStochDG3DMaterialLaw : public dG3DMaterialLaw
{
 protected:
  mlawAnisotropicStoch _tilaw;

 public:
  AnisotropicStochDG3DMaterialLaw(const int num, const double rho, const double alpha,
                                  const double beta, const double gamma, const double OrigX, const double OrigY, const char *propName, const int intpl);

 #ifndef SWIG
  AnisotropicStochDG3DMaterialLaw(const AnisotropicStochDG3DMaterialLaw &source);
  virtual ~AnisotropicStochDG3DMaterialLaw(){}
  // set the time of _tilaw
   virtual void setTime(const double t,const double dtime){
    dG3DMaterialLaw::setTime(t,dtime);
    _tilaw.setTime(t,dtime);
  }
  virtual materialLaw::matname getType() const {return _tilaw.getType();}
  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
  virtual double soundSpeed() const{return _tilaw.soundSpeed();} // or change value ??
  virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
	virtual double scaleFactor() const{return _tilaw.scaleFactor();}
	virtual materialLaw* clone() const{return new AnisotropicStochDG3DMaterialLaw(*this);};
	virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
  virtual bool withEnergyDissipation() const {return _tilaw.withEnergyDissipation();};
	virtual void setMacroSolver(const nonLinearMechSolver* sv){
		dG3DMaterialLaw::setMacroSolver(sv);
		_tilaw.setMacroSolver(sv);
	};
  virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
  {
    return &_tilaw;
  }
  virtual materialLaw *getNonLinearSolverMaterialLaw()
  {
    return &_tilaw;
  }
 #endif // SWIG
};


class ClusterDG3DMaterialLaw : public dG3DMaterialLaw
{
 protected:
  Clustering* _clusteringData;
  std::map<int, dG3DMaterialLaw*> _matlawptr; //give the material law pointer for cluster i
  bool _homogeneous;
  int _eigenMethod=0; //0: zero-strain BC, 1: zero-stress BC

 public:
  ClusterDG3DMaterialLaw(const int num, Clustering* cl, bool homo=true);
 #ifndef SWIG
  ClusterDG3DMaterialLaw(const ClusterDG3DMaterialLaw &source);
  virtual ~ClusterDG3DMaterialLaw(){}
  // set the time of _tilaw
  virtual void setTime(const double t,const double dtime);
  virtual materialLaw::matname getType() const {return materialLaw::cluster;}
  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw);
  virtual double soundSpeed() const;
  virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
  virtual double scaleFactor() const;
  virtual materialLaw* clone() const{return new ClusterDG3DMaterialLaw(*this);};
  virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
  virtual bool withEnergyDissipation() const;
  virtual void setMacroSolver(const nonLinearMechSolver* sv);
  virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
  {
    Msg::Error("getConstNonLinearSolverMaterialLaw in ClusterDG3DMaterialLaw does not exit");
    return NULL;
  }
  virtual materialLaw *getNonLinearSolverMaterialLaw()
  {
    Msg::Error("getNonLinearSolverMaterialLaw in ClusterDG3DMaterialLaw does not exit");
    return NULL;
  }
 #endif // SWIG
};


/*class TFADG3DMaterialLaw : public dG3DMaterialLaw
{
 protected:
  mlawTFAMaterialLaws _TFAlaw;

 public:
  TFADG3DMaterialLaw(const int num, double rho);
  void setTFAMethod(int TFAMethodNum, int dim, int correction=0, int solverType=0, int tag=1000);
  void loadClusterSummaryAndInteractionTensorsFromFiles(const std::string clusterSummaryFileName,
                                        const std::string interactionTensorsFileName);
  void loadClusterSummaryAndInteractionTensorsHomogenizedFrameFromFiles(const std::string clusterSummaryFileName,
                                        const std::string interactionTensorsFileName);
  void loadClusterStandardDeviationFromFile(const std::string clusterSTDFileName);

  void loadPlasticEqStrainConcentrationFromFile(const std::string PlasticEqStrainConcentrationFileName,
                                                const std::string PlasticEqStrainConcentrationGeometricFileName,
                                                const std::string PlasticEqStrainConcentrationHarmonicFileName,
                                                const std::string PlasticEqStrainConcentrationPowerFileName);

  void loadElasticStrainConcentrationDerivativeFromFile(const std::string ElasticStrainConcentrationDerivative);

  void loadEshelbyInteractionTensorsFromFile(const std::string EshelbyinteractionTensorsFileName);
  void loadInteractionTensorsMatrixFrameELFromFile(const std::string InteractionTensorsMatrixFrameELFileName);
  void loadInteractionTensorsHomogenizedFrameELFromFile(const std::string InteractionTensorsHomogenizedFrameELFileName);

  void loadReferenceStiffnessFromFile(const std::string referenceStiffnessFileName);
  void loadReferenceStiffnessIsoFromFile(const std::string referenceStiffnessIsoFileName);
  void loadInteractionTensorIsoFromFile(const std::string interactionTensorsIsoFileName);

  void setClusterIncOrientation();
  void loadClusterFiberOrientationFromFile(const std::string OrientationDataFileName);

  void addClusterMaterialLaw(int clusterNb, dG3DMaterialLaw* clusterLaw)
  {
    _TFAlaw.addClusterMaterialLaw(clusterNb, clusterLaw->getNonLinearSolverMaterialLaw());
  }


 #ifndef SWIG
  TFADG3DMaterialLaw(const TFADG3DMaterialLaw &source);
  virtual ~TFADG3DMaterialLaw(){}
  // set the time of _tilaw
   virtual void setTime(const double t,const double dtime){
    dG3DMaterialLaw::setTime(t,dtime);
    _TFAlaw.setTime(t,dtime);
  }
  virtual materialLaw::matname getType() const {return _TFAlaw.getType();}
  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw)
  {
    _TFAlaw.initLaws(maplaw);
  }
  virtual double soundSpeed() const{return _TFAlaw.soundSpeed();} // or change value ??
  virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
  virtual double scaleFactor() const{return _TFAlaw.scaleFactor();}
  virtual materialLaw* clone() const{return new TFADG3DMaterialLaw(*this);};
  virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
  virtual bool withEnergyDissipation() const {return _TFAlaw.withEnergyDissipation();};
  virtual void setMacroSolver(const nonLinearMechSolver* sv){
		dG3DMaterialLaw::setMacroSolver(sv);
		_TFAlaw.setMacroSolver(sv);
  };
  virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
  {
    return &_TFAlaw;
  }
  virtual materialLaw *getNonLinearSolverMaterialLaw()
  {
    return &_TFAlaw;
  }
 #endif // SWIG
};
*/


class GenericTFADG3DMaterialLaw : public dG3DMaterialLaw
{
 protected:
  mlawGenericTFAMaterialLaws* _GenericTFAlaw;

 public:
  GenericTFADG3DMaterialLaw(const int num, double rho);
  void setTFAMethod(int TFAMethodNum, int dim, int correction=0, int solverType=0, int tag=1000);
  void loadClusterSummaryAndInteractionTensorsFromFiles(const std::string clusterSummaryFileName,
                                        const std::string interactionTensorsFileName);
  void loadClusterSummaryAndInteractionTensorsHomogenizedFrameFromFiles(const std::string clusterSummaryFileName,
                                        const std::string interactionTensorsFileName);
  void loadClusterStandardDeviationFromFile(const std::string clusterSTDFileName);

  void loadPlasticEqStrainConcentrationFromFile(const std::string PlasticEqStrainConcentrationFileName,
                                                const std::string PlasticEqStrainConcentrationGeometricFileName,
                                                const std::string PlasticEqStrainConcentrationHarmonicFileName);

  void loadElasticStrainConcentrationDerivativeFromFile(const std::string ElasticStrainConcentrationDerivative);

  void loadEshelbyInteractionTensorsFromFile(const std::string EshelbyinteractionTensorsFileName);
  void loadInteractionTensorsMatrixFrameELFromFile(const std::string InteractionTensorsMatrixFrameELFileName);
  void loadInteractionTensorsHomogenizedFrameELFromFile(const std::string InteractionTensorsHomogenizedFrameELFileName);

  void loadReferenceStiffnessFromFile(const std::string referenceStiffnessFileName);
  void loadReferenceStiffnessIsoFromFile(const std::string referenceStiffnessIsoFileName);
  void loadInteractionTensorIsoFromFile(const std::string interactionTensorsIsoFileName);

  void setClusterIncOrientation();
  void loadClusterFiberOrientationFromFile(const std::string OrientationDataFileName);

  void addClusterMaterialLaw(int clusterNb, dG3DMaterialLaw* clusterLaw)
  {
    _GenericTFAlaw->addClusterMaterialLaw(clusterNb, clusterLaw->getNonLinearSolverMaterialLaw());
  }


 #ifndef SWIG
  GenericTFADG3DMaterialLaw(const GenericTFADG3DMaterialLaw &source);
  virtual ~GenericTFADG3DMaterialLaw(){
    if (_GenericTFAlaw) delete _GenericTFAlaw;
  };
  // set the time of _tilaw
   virtual void setTime(const double t,const double dtime){
    dG3DMaterialLaw::setTime(t,dtime);
    _GenericTFAlaw->setTime(t,dtime);
  }
  virtual materialLaw::matname getType() const {return _GenericTFAlaw->getType();}
  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw)
  {
    _GenericTFAlaw->initLaws(maplaw);
  }
  virtual double soundSpeed() const{return _GenericTFAlaw->soundSpeed();} // or change value ??
  virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
  virtual double scaleFactor() const{return _GenericTFAlaw->scaleFactor();}
  virtual materialLaw* clone() const{return new GenericTFADG3DMaterialLaw(*this);};
  virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
  virtual bool withEnergyDissipation() const {return _GenericTFAlaw->withEnergyDissipation();};
  virtual void setMacroSolver(const nonLinearMechSolver* sv){
		dG3DMaterialLaw::setMacroSolver(sv);
		_GenericTFAlaw->setMacroSolver(sv);
  };
  virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
  {
    return _GenericTFAlaw;
  }
  virtual materialLaw *getNonLinearSolverMaterialLaw()
  {
    return _GenericTFAlaw;
  }
 #endif // SWIG
};


class TFADG3DMaterialLaw : public GenericTFADG3DMaterialLaw
{

 public:
  TFADG3DMaterialLaw(const int num, double rho);

 #ifndef SWIG
  TFADG3DMaterialLaw(const TFADG3DMaterialLaw &source): GenericTFADG3DMaterialLaw(source) {};
  virtual ~TFADG3DMaterialLaw(){};

 #endif // SWIG
};

class HierarchicalTFADG3DMaterialLaw : public GenericTFADG3DMaterialLaw
{
 public:
  HierarchicalTFADG3DMaterialLaw(const int num, double rho);

  void setTFAMethod(int TFAMethodNum, int dim, int correction=0, int solverType=0, int tag=1000);
  void setTFALawID(int tfaLawID);

  void loadClusterSummaryAndInteractionTensorsLevel1FromFiles(const std::string clusterSummaryFileName,
                                                              const std::string interactionTensorsFileName);

 #ifndef SWIG
  HierarchicalTFADG3DMaterialLaw(const HierarchicalTFADG3DMaterialLaw &source): GenericTFADG3DMaterialLaw(source) {};
  virtual ~HierarchicalTFADG3DMaterialLaw(){};
  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
  virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
  virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);

 #endif // SWIG
};



class LocalDamageIsotropicElasticityDG3DMaterialLaw: public dG3DMaterialLaw{
protected:
	#ifndef SWIG
		mlawLocalDamageIsotropicElasticity* _nlLaw;
	#endif
	public:
		LocalDamageIsotropicElasticityDG3DMaterialLaw(const int num,const double rho,
                                         double E,const double nu,  const DamageLaw &damLaw);

		#ifndef SWIG
		LocalDamageIsotropicElasticityDG3DMaterialLaw(const LocalDamageIsotropicElasticityDG3DMaterialLaw& src);
		virtual ~LocalDamageIsotropicElasticityDG3DMaterialLaw(){
      if (_nlLaw) delete _nlLaw;
    };
		virtual void setTime(const double t,const double dtime);
    virtual materialLaw::matname getType() const {return _nlLaw->getType();};

    virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw) {}
    virtual double soundSpeed() const {return _nlLaw->soundSpeed();};
    virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
		virtual double scaleFactor() const {return _nlLaw->scaleFactor();}
		virtual materialLaw* clone() const{return new LocalDamageIsotropicElasticityDG3DMaterialLaw(*this);};
		virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
    virtual bool withEnergyDissipation() const {return _nlLaw->withEnergyDissipation();};
		virtual void setMacroSolver(const nonLinearMechSolver* sv){
			dG3DMaterialLaw::setMacroSolver(sv);
			_nlLaw->setMacroSolver(sv);
		};
    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
      return _nlLaw;
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
      return _nlLaw;
    }
		#endif
};

class LocalDamageHyperelasticDG3DMaterialLaw: public dG3DMaterialLaw{
protected:
	#ifndef SWIG
		mlawLocalDamageHyperelastic* _nlLaw;
	#endif
	public:
		LocalDamageHyperelasticDG3DMaterialLaw(const int num, const double rho, const elasticPotential& elP, const DamageLaw &damLaw);

		#ifndef SWIG
		LocalDamageHyperelasticDG3DMaterialLaw(const LocalDamageHyperelasticDG3DMaterialLaw& src);
		virtual ~LocalDamageHyperelasticDG3DMaterialLaw(){
      if (_nlLaw) delete _nlLaw;
    };
		virtual void setTime(const double t,const double dtime);
    virtual materialLaw::matname getType() const {return _nlLaw->getType();};

    virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw) {}
    virtual double soundSpeed() const {return _nlLaw->soundSpeed();};
    virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
		virtual double scaleFactor() const {return _nlLaw->scaleFactor();}
		virtual materialLaw* clone() const{return new LocalDamageHyperelasticDG3DMaterialLaw(*this);};
		virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
    virtual bool withEnergyDissipation() const {return _nlLaw->withEnergyDissipation();};
		virtual void setMacroSolver(const nonLinearMechSolver* sv){
			dG3DMaterialLaw::setMacroSolver(sv);
			_nlLaw->setMacroSolver(sv);
		};
    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
      return _nlLaw;
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
      return _nlLaw;
    }
		#endif
};

class LocalAnisotropicDamageHyperelasticDG3DMaterialLaw: public LocalDamageHyperelasticDG3DMaterialLaw{

  public:
		LocalAnisotropicDamageHyperelasticDG3DMaterialLaw(const int num, const double rho, const elasticPotential& elP, const DamageLaw &damLaw);

		#ifndef SWIG
		LocalAnisotropicDamageHyperelasticDG3DMaterialLaw(const LocalAnisotropicDamageHyperelasticDG3DMaterialLaw& src): LocalDamageHyperelasticDG3DMaterialLaw(src){};
		virtual ~LocalAnisotropicDamageHyperelasticDG3DMaterialLaw(){
    };
    #endif //SWIG
};

class LocalDamageJ2HyperDG3DMaterialLaw : public dG3DMaterialLaw
{

 protected:
  mlawLocalDamageJ2Hyper *_nldJ2Hyperlaw; // pointer to allow to choose between LLN style or Gmsh Style. The choice is performed by the constructor (2 constructors with != arguments)

 public:
  LocalDamageJ2HyperDG3DMaterialLaw(const int num,const double rho,
                   double E,const double nu,const J2IsotropicHardening &_j2IH,
                   const DamageLaw &_damLaw,const double tol=1.e-6, bool matrixByPerturbation=false, double pertTol = 1e-8);
  virtual ~LocalDamageJ2HyperDG3DMaterialLaw();
  void setStrainOrder(const int i);
#ifndef SWIG
  LocalDamageJ2HyperDG3DMaterialLaw(const LocalDamageJ2HyperDG3DMaterialLaw &source);
  // set the time of _nldlaw
  virtual void setTime(const double t,const double dtime);
  virtual materialLaw::matname getType() const;

  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
  virtual double soundSpeed() const;
  virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
	virtual double scaleFactor() const{return _nldJ2Hyperlaw->scaleFactor();}
	virtual materialLaw* clone() const{ return new LocalDamageJ2HyperDG3DMaterialLaw(*this);};
	virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
  virtual bool withEnergyDissipation() const {return _nldJ2Hyperlaw->withEnergyDissipation();};
	virtual void setMacroSolver(const nonLinearMechSolver* sv){
		dG3DMaterialLaw::setMacroSolver(sv);
		if (_nldJ2Hyperlaw != NULL){
			_nldJ2Hyperlaw->setMacroSolver(sv);
		}
	}
  virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
  {
    return _nldJ2Hyperlaw;
  }
  virtual materialLaw *getNonLinearSolverMaterialLaw()
  {
    return _nldJ2Hyperlaw;
  }
#endif
};

class LocalDamageJ2SmallStrainDG3DMaterialLaw : public dG3DMaterialLaw
{

 protected:
  mlawLocalDamageJ2SmallStrain *_nldJ2; // pointer to allow to choose between LLN style or Gmsh Style. The choice is performed by the constructor (2 constructors with != arguments)

 public:
  LocalDamageJ2SmallStrainDG3DMaterialLaw(const int num,const double rho,
                   double E,const double nu,const J2IsotropicHardening &_j2IH,
                   const DamageLaw &_damLaw,const double tol=1.e-6, bool matrixByPerturbation=false, double pertTol = 1e-8);
  virtual ~LocalDamageJ2SmallStrainDG3DMaterialLaw();
#ifndef SWIG
  LocalDamageJ2SmallStrainDG3DMaterialLaw(const LocalDamageJ2SmallStrainDG3DMaterialLaw &source);
  // set the time of _nldlaw
  virtual void setTime(const double t,const double dtime);
  virtual materialLaw::matname getType() const;

  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
  virtual double soundSpeed() const;
  virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
	virtual double scaleFactor() const{return _nldJ2->scaleFactor();}
	virtual materialLaw* clone() const{ return new LocalDamageJ2SmallStrainDG3DMaterialLaw(*this);};
	virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
  virtual bool withEnergyDissipation() const {return _nldJ2->withEnergyDissipation();};
	virtual void setMacroSolver(const nonLinearMechSolver* sv){
		dG3DMaterialLaw::setMacroSolver(sv);
		if (_nldJ2 != NULL){
			_nldJ2->setMacroSolver(sv);
		}
	}
  virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
  {
    return _nldJ2;
  }
  virtual materialLaw *getNonLinearSolverMaterialLaw()
  {
    return _nldJ2;
  }
#endif
};


class LinearThermoMechanicsDG3DMaterialLaw : public dG3DMaterialLaw // public materialLaw
{

 protected:
  mlawLinearThermoMechanics *_lawLinearTM; // pointer to allow to choose between LLN style or Gmsh Style. The choice is performed by the constructor (2 constructors with != arguments)

  STensor3                  linearK;
  STensor3		    Stiff_alphaDilatation;
 public:
  LinearThermoMechanicsDG3DMaterialLaw(const int num,const double rho, const double Ex, const double Ey, const double Ez, const double Vxy, const double Vxz, const double Vyz,
				       const double MUxy, const double MUxz, const double MUyz, const double alpha, const double beta, const double gamma,const double t0,
				       const double Kx,const double Ky,const double Kz,const double alphax,const double alphay,const double alphaz,const double cp);
  virtual ~LinearThermoMechanicsDG3DMaterialLaw() {
		if (_lawLinearTM != NULL){
			delete _lawLinearTM;
			_lawLinearTM = NULL;
		}
	};

#ifndef SWIG
 LinearThermoMechanicsDG3DMaterialLaw(const LinearThermoMechanicsDG3DMaterialLaw &source);
  // set the time of _nldlaw
  virtual void setTime(const double t,const double dtime){
    dG3DMaterialLaw::setTime(t,dtime);
    _lawLinearTM->setTime(t,dtime);
  }
  virtual materialLaw::matname getType() const {return _lawLinearTM->getType();}
  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
  virtual double soundSpeed() const{return _lawLinearTM->soundSpeed();} // or change value ??
  virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
	virtual double scaleFactor() const {return _lawLinearTM->shearModulus();};
  virtual int getNumberOfExtraDofsDiffusion() const {return 1;}
  virtual double getExtraDofStoredEnergyPerUnitField(double T) const;
	virtual double getInitialExtraDofStoredEnergyPerUnitField() const;
  virtual double defoEnergy(const STensor3& Fn,const SVector3& gradT) const;
  virtual double defoEnergy( const SVector3&ujump,const double &Tjump, const SVector3& N) const;
	virtual materialLaw* clone() const{ return new LinearThermoMechanicsDG3DMaterialLaw(*this);};
	virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
  virtual bool withEnergyDissipation() const {return _lawLinearTM->withEnergyDissipation();};
	virtual void setMacroSolver(const nonLinearMechSolver* sv){
		dG3DMaterialLaw::setMacroSolver(sv);
		if (_lawLinearTM!=NULL){
			_lawLinearTM->setMacroSolver(sv);
		}
	}
  virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
  {
    return _lawLinearTM;
  }
  virtual materialLaw *getNonLinearSolverMaterialLaw()
  {
    return _lawLinearTM;
  }
#endif
};

class J2ThermoMechanicsDG3DMaterialLaw : public dG3DMaterialLaw
{
    friend class EOSDG3DMaterialLaw;
 protected:
  mlawJ2ThermoMechanics _j2law;
  STensor3            linearK;
  STensor3	      Stiff_alphaDilatation;
 public:
  J2ThermoMechanicsDG3DMaterialLaw(const int num,const double rho,
                   double E,const double nu,const J2IsotropicHardening &_j2IH,
                   const double t0,const double Kther, const double alphather, const double cp, const double tol=1.e-6);
 #ifndef SWIG
  J2ThermoMechanicsDG3DMaterialLaw(const J2ThermoMechanicsDG3DMaterialLaw &source);
  virtual ~J2ThermoMechanicsDG3DMaterialLaw()
  {

  }
  // set the time of _j2law
   virtual void setTime(const double t,const double dtime){
    dG3DMaterialLaw::setTime(t,dtime);
    _j2law.setTime(t,dtime);
  }
  virtual materialLaw::matname getType() const {return _j2law.getType();}
  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
  virtual double soundSpeed() const{return _j2law.soundSpeed();} // or change value ??
  virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
  virtual const double bulkModulus() const {return _j2law.bulkModulus();}
	virtual double scaleFactor() const{return _j2law.shearModulus();};
  virtual int getNumberOfExtraDofsDiffusion() const {return 1;}
  virtual double getExtraDofStoredEnergyPerUnitField(double T) const;
	virtual double getInitialExtraDofStoredEnergyPerUnitField() const;
	virtual materialLaw* clone() const{ return new J2ThermoMechanicsDG3DMaterialLaw(*this);};
	virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
  virtual bool withEnergyDissipation() const {return _j2law.withEnergyDissipation();};
	virtual void setMacroSolver(const nonLinearMechSolver* sv){
		dG3DMaterialLaw::setMacroSolver(sv);
		_j2law.setMacroSolver(sv);
	}
  virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
  {
    return &_j2law;
  }
  virtual materialLaw *getNonLinearSolverMaterialLaw()
  {
    return &_j2law;
  }
 #endif // SWIG
};

class FullJ2ThermoMechanicsDG3DMaterialLaw : public dG3DMaterialLaw{
  protected:
    #ifndef SWIG
    mlawJ2FullyCoupledThermoMechanics _j2FullThermo;
    #endif // SWIG

  public:
    FullJ2ThermoMechanicsDG3DMaterialLaw(const int num, const double E, const double nu,const double rho,
                               const double sy0,const double h,const double tol=1.e-6,
                               const bool matrixbyPerturbation = false, const double pert = 1e-8);

    void setStrainOrder(const int i);
    void setReferenceTemperature(const double Tr);
    void setIsotropicHardeningLaw(const J2IsotropicHardening& isoHard);

    void setReferenceThermalExpansionCoefficient(const double al);
    void setTemperatureFunction_ThermalExpansionCoefficient(const scalarFunction& Tfunc);

    void setReferenceThermalConductivity(const double KK);
    void setTemperatureFunction_ThermalConductivity(const scalarFunction& Tfunc);

    void setReferenceCp(const double Cp);
    void setTemperatureFunction_Cp(const scalarFunction& Tfunc);

    void setTemperatureFunction_Hardening(const scalarFunction& Tfunc);
    void setTemperatureFunction_InitialYieldStress(const scalarFunction& Tfunc);
    void setTemperatureFunction_BulkModulus(const scalarFunction& Tfunc);
    void setTemperatureFunction_ShearModulus(const scalarFunction& Tfunc);
    void setTaylorQuineyFactor(const double f);
    void setThermalEstimationPreviousConfig(const bool flag);
    #ifndef SWIG
    FullJ2ThermoMechanicsDG3DMaterialLaw(const FullJ2ThermoMechanicsDG3DMaterialLaw &source);
    virtual ~FullJ2ThermoMechanicsDG3DMaterialLaw(){}
    // set the time of _nldlaw
    virtual materialLaw::matname getType() const {return _j2FullThermo.getType();}
    virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
    virtual double soundSpeed() const{return _j2FullThermo.soundSpeed();}
    virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true,const bool checkfrac=true, const bool dTangent=false);
    virtual double scaleFactor() const;
    virtual int getNumberOfExtraDofsDiffusion() const {return 1;}
    virtual double getInitialExtraDofStoredEnergyPerUnitField() const;
    virtual double getExtraDofStoredEnergyPerUnitField(double T) const;
    // set the time of _nldlaw
    virtual void setTime(const double t,const double dtime){
      dG3DMaterialLaw::setTime(t,dtime);
      _j2FullThermo.setTime(t,dtime);
    }
    virtual materialLaw* clone() const{ return new FullJ2ThermoMechanicsDG3DMaterialLaw(*this);};
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
    virtual bool withEnergyDissipation() const {return _j2FullThermo.withEnergyDissipation();};
    virtual void setMacroSolver(const nonLinearMechSolver* sv){
      dG3DMaterialLaw::setMacroSolver(sv);
      _j2FullThermo.setMacroSolver(sv);
    }
    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
      return &_j2FullThermo;
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
      return &_j2FullThermo;
    }
    #endif // SWIG
};



class mlawAnIsotropicTherMechDG3DMaterialLaw :public dG3DMaterialLaw
{

 protected:
   mlawAnIsotropicTherMech  *_lawAnTM;
   STensor3 	 Stiff_alphaDilatation;
   STensor3                  linearK;

 public:


  mlawAnIsotropicTherMechDG3DMaterialLaw(const int num,const double E, const double nu,const double rho,const double EA, const double GA,const double nu_minor,const double Ax,
					     const double Ay, const double Az, const double alpha, const double beta, const double gamma,const double t0,
					    const double kx,const double ky, const double kz,const double cp,const double alphath);

#ifndef SWIG
	virtual ~mlawAnIsotropicTherMechDG3DMaterialLaw() {
		if (_lawAnTM != NULL){
			delete _lawAnTM;
			_lawAnTM = NULL;
		};
	}
  mlawAnIsotropicTherMechDG3DMaterialLaw(const mlawAnIsotropicTherMechDG3DMaterialLaw &source);
  // set the time of _nldlaw
  virtual void setTime(const double t,const double dtime){
    dG3DMaterialLaw::setTime(t,dtime);
    _lawAnTM->setTime(t,dtime);
  }
  virtual materialLaw::matname getType() const {return _lawAnTM->getType();}
  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
  virtual double soundSpeed() const{return _lawAnTM->soundSpeed();} // or change value ??
  virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
  virtual int getNumberOfExtraDofsDiffusion() const {return 2;}
  double getExtraDofStoredEnergyPerUnitField(double T) const
  {
   Msg::Error("getExtraDofStoredEnergyPerUnitField not defined");
   return 0.;
  }
	virtual materialLaw* clone() const{ return new mlawAnIsotropicTherMechDG3DMaterialLaw(*this);};
	virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
  virtual bool withEnergyDissipation() const {return _lawAnTM->withEnergyDissipation();};
	virtual void setMacroSolver(const nonLinearMechSolver* sv){
		dG3DMaterialLaw::setMacroSolver(sv);
		if (_lawAnTM!=NULL)
			_lawAnTM->setMacroSolver(sv);
	}
  virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
  {
    return _lawAnTM;
  }
  virtual materialLaw *getNonLinearSolverMaterialLaw()
  {
    return _lawAnTM;
  }
#endif
};
//class LinearSMPDG3DMaterialLaw : public LinearThermoMechanicsDG3DMaterialLaw  // public materialLaw
class SMPDG3DMaterialLaw :public dG3DMaterialLaw // ,public mlawSMP
{

 protected:
  mlawSMP *_lawTMSMP; // pointer to allow to choose between LLN style or Gmsh Style. The choice is performed by the constructor (2 constructors with != arguments)

  STensor3            linearK;
  STensor3 	      Stiff_alphaDilatation;
 public:

  SMPDG3DMaterialLaw(const int num,const double rho,const double alpha, const double beta, const double gamma ,const double t0, const double Kx,const double Ky, const double Kz,
       const double mu_groundState3,const double Im3, const double pi,const double Tr, const double Nmu_groundState2 ,const double mu_groundState2 ,
       const double Im2,const double Sgl2,const double Sr2,const double Delta,const double m2, const double epsilonr ,const double n,const double epsilonp02,
       const double alphar1,const double alphagl1,const double Ggl1,const double Gr1 ,const double Mgl1,const double Mr1,const double Mugl1,
       const double Mur1,const double epsilon01,const double Qgl1,const double Qr1,const double epsygl1 ,const double d1,
      const double m1,const double V1,const double alphap, const double  Sa0,const double ha1,const double b1,const double g1,
       const double phia01,const double Z1, const double r1,const double s1,const double Sb01,const double Hgl1,const double Lgl1,const double Hr1,
       const double Lr1,const double l1,const double Kb,const double be1,const double c0,const double wp,const double c1);

  void setStrainOrder(const int order);
  virtual void setMechanism2(bool mechanism2);

#ifndef SWIG
	virtual ~SMPDG3DMaterialLaw() {
		if (_lawTMSMP !=NULL) {delete _lawTMSMP; _lawTMSMP = NULL;};
	};
 SMPDG3DMaterialLaw(const SMPDG3DMaterialLaw &source);
  // set the time of _nldlaw
  virtual void setTime(const double t,const double dtime){
    dG3DMaterialLaw::setTime(t,dtime);
    _lawTMSMP->setTime(t,dtime);
  }
  virtual materialLaw::matname getType() const {return _lawTMSMP->getType();}
  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
  virtual double soundSpeed() const{return _lawTMSMP->soundSpeed();} // or change value ??
  virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
  virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
  virtual int getNumberOfExtraDofsDiffusion() const {return 1;}
  double getExtraDofStoredEnergyPerUnitField(double T) const;
	virtual materialLaw* clone() const{ return new SMPDG3DMaterialLaw(*this);};
  virtual bool withEnergyDissipation() const {return _lawTMSMP->withEnergyDissipation();};
	virtual void setMacroSolver(const nonLinearMechSolver* sv){
		dG3DMaterialLaw::setMacroSolver(sv);
		if (_lawTMSMP!=NULL)
			_lawTMSMP->setMacroSolver(sv);
	}
  virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
  {
    return _lawTMSMP;
  }
  virtual materialLaw *getNonLinearSolverMaterialLaw()
  {
    return _lawTMSMP;
  }
#endif
};




//===========/start/PhenomenologicalSMPDG3DMaterialLaw===================================================================================
class PhenomenologicalSMPDG3DMaterialLaw :public dG3DMaterialLaw // ,public mlawSMP
{
protected:
    mlawPhenomenologicalSMP *_lawTMSMP; // pointer to allow to choose between LLN style or Gmsh Style.
                                        // The choice is performed by the constructor (2 constructors with != arguments)
    STensor3  linearK;
    STensor3  Stiff_alphaDilatation;


public:
    PhenomenologicalSMPDG3DMaterialLaw(const int num, const double rho, const double alpha, const double beta, const double gamma, const double t0,
                            const double Kxg, const double Kyg, const double Kzg, const double Kxr, const double Kyr, const double Kzr,
                            const double KxAM, const double KyAM, const double KzAM,
                            const double cfG, const double cfR, const double cfAM,
                            const double cg, const double cr, const double cAM,
                            const double Tm0, const double Tc0, const double xi, const double wm0, const double wc0,
                            const double alphag0, const double alphar0, const double alphaAM0, const double alphaAD0,
                            const double Eg, const double Er,const double EAM,
                            const double nug, const double nur, const double nuAM,
                            const double TaylorQuineyG,const double TaylorQuineyR, const double TaylorQuineyAM,
                            const double zAM);



    virtual void setStrainOrder(const int order)
    {
      _lawTMSMP->setStrainOrder(order);
    }

    virtual void setViscoelasticMethod(const int method)
    {
      _lawTMSMP->setViscoelasticMethod(method);
    }
    virtual void setViscoElasticNumberOfElementG(const int N)
    {
      _lawTMSMP->setViscoElasticNumberOfElementG(N);
    }
    virtual void setViscoElasticNumberOfElementR(const int N)
    {
      _lawTMSMP->setViscoElasticNumberOfElementR(N);
    }
    virtual void setViscoElasticNumberOfElementAM(const int N)
    {
      _lawTMSMP->setViscoElasticNumberOfElementAM(N);
    }
    virtual void setViscoElasticDataG(const int i, const double Ei, const double taui)
    {
      _lawTMSMP->setViscoElasticDataG( i,  Ei, taui);
    }
    virtual void setViscoElasticDataR(const int i, const double Ei, const double taui)
    {
     _lawTMSMP->setViscoElasticDataR( i,  Ei, taui);
    }
    virtual void setViscoElasticDataAM(const int i, const double Ei, const double taui)
    {
      _lawTMSMP->setViscoElasticDataAM( i,  Ei, taui);
    }
    virtual void setViscoElasticData_BulkG(const int i, const double Ei, const double taui)
    {
      _lawTMSMP->setViscoElasticData_BulkG( i,  Ei, taui);
    }
    virtual void setViscoElasticData_BulkR(const int i, const double Ei, const double taui)
    {
      _lawTMSMP->setViscoElasticData_BulkR( i,  Ei, taui);
    }
    virtual void setViscoElasticData_BulkAM(const int i, const double Ei, const double taui)
    {
      _lawTMSMP->setViscoElasticData_BulkAM( i,  Ei, taui);
    }
    virtual void setViscoElasticData_ShearG(const int i, const double Ei, const double taui)
    {
      _lawTMSMP->setViscoElasticData_ShearG( i,  Ei, taui);
    }
    virtual void setViscoElasticData_ShearR(const int i, const double Ei, const double taui)
    {
      _lawTMSMP->setViscoElasticData_ShearR( i,  Ei, taui);
    }
    virtual void setViscoElasticData_ShearAM(const int i, const double Ei, const double taui)
    {
      _lawTMSMP->setViscoElasticData_ShearAM( i,  Ei, taui);
    }
    virtual void setViscoElasticDataG(const std::string filename)
    {
      _lawTMSMP->setViscoElasticDataG(filename);
    }
    virtual void setViscoElasticDataR(const std::string filename)
    {
      _lawTMSMP->setViscoElasticDataR( filename);
    }
    virtual void setViscoElasticDataAM(const std::string filename)
    {
      _lawTMSMP->setViscoElasticDataAM( filename);
    }
    virtual void setVolumeCorrectionG(double _vc, double _xivc, double _zetavc, double _dc, double _thetadc, double _pidc)
    {
      _lawTMSMP->setVolumeCorrectionG(_vc, _xivc, _zetavc, _dc, _thetadc, _pidc);
    }
    virtual void setVolumeCorrectionR(double _vc, double _xivc, double _zetavc, double _dc, double _thetadc, double _pidc)
    {
      _lawTMSMP->setVolumeCorrectionR(_vc, _xivc, _zetavc, _dc, _thetadc, _pidc);
    }
    virtual void setVolumeCorrectionAM(double _vc, double _xivc, double _zetavc, double _dc, double _thetadc, double _pidc)
    {
      _lawTMSMP->setVolumeCorrectionAM(_vc, _xivc, _zetavc, _dc, _thetadc, _pidc);
    }

    virtual void setJ2IsotropicHardeningCompressionG(const J2IsotropicHardening& isoHard) const {
      _lawTMSMP->setJ2IsotropicHardeningCompressionG(isoHard);
    };
    virtual void setJ2IsotropicHardeningTractionG(const J2IsotropicHardening& isoHard) const {
      _lawTMSMP->setJ2IsotropicHardeningTractionG(isoHard);
    };
    virtual void setKinematicHardeningG(const kinematicHardening& isoHard) const {
      _lawTMSMP->setKinematicHardeningG(isoHard);
    };

    virtual void setJ2IsotropicHardeningCompressionR(const J2IsotropicHardening& isoHard) const {
      _lawTMSMP->setJ2IsotropicHardeningCompressionR(isoHard);
    };
    virtual void setJ2IsotropicHardeningTractionR(const J2IsotropicHardening& isoHard) const {
      _lawTMSMP->setJ2IsotropicHardeningTractionR(isoHard);
    };
    virtual void setKinematicHardeningR(const kinematicHardening& isoHard) const {
      _lawTMSMP->setKinematicHardeningR(isoHard);
    };

    virtual void setJ2IsotropicHardeningCompressionAM(const J2IsotropicHardening& isoHard) const {
      _lawTMSMP->setJ2IsotropicHardeningCompressionAM(isoHard);
    };
    virtual void setJ2IsotropicHardeningTractionAM(const J2IsotropicHardening& isoHard) const {
      _lawTMSMP->setJ2IsotropicHardeningTractionAM(isoHard);
    };
    virtual void setKinematicHardeningAM(const kinematicHardening& isoHard) const {
      _lawTMSMP->setKinematicHardeningAM(isoHard);
    };

    virtual void setViscosityEffectG(const viscosityLaw& isoHard, const double p) const {
      _lawTMSMP->setViscosityEffectG(isoHard,p);
    };
   virtual void setViscosityEffectR(const viscosityLaw& isoHard, const double p) const {
     _lawTMSMP->setViscosityEffectR(isoHard,p);
    };
   virtual void setViscosityEffectAM(const viscosityLaw& isoHard, const double p) const {
     _lawTMSMP->setViscosityEffectAM(isoHard,p);
    };


    virtual void setYieldPowerFactorG(const double n) const
    {
      _lawTMSMP->setPowerFactorG(n);
    };
    virtual void setYieldPowerFactorR(const double n) const
    {
      _lawTMSMP->setPowerFactorR(n);
    };
    virtual void setYieldPowerFactorAM(const double n) const
    {
      _lawTMSMP->setPowerFactorAM(n);
    };
    virtual void nonAssociatedFlowRuleFactorG(const double n) const
    {
      _lawTMSMP->nonAssociatedFlowRuleFactorG(n);
    };
    virtual void nonAssociatedFlowRuleFactorR(const double n) const
    {
      _lawTMSMP->nonAssociatedFlowRuleFactorR(n);
    };
    virtual void nonAssociatedFlowRuleFactorAM(const double n) const
    {
      _lawTMSMP->nonAssociatedFlowRuleFactorAM(n);
    };
    virtual void setPlasticPoissonRatioG(const double n) const
    {
      _lawTMSMP->setPlasticPoissonRatioG(n);
    };
    virtual void setPlasticPoissonRatioR(const double n) const
    {
      _lawTMSMP->setPlasticPoissonRatioR(n);
    };
    virtual void setPlasticPoissonRatioAM(const double n) const
    {
      _lawTMSMP->setPlasticPoissonRatioAM(n);
    };
    virtual void setNonAssociatedFlowG(const bool n) const
    {
      _lawTMSMP->setNonAssociatedFlowG(n);
    };
    virtual void setNonAssociatedFlowR(const bool n) const
    {
      _lawTMSMP->setNonAssociatedFlowR(n);
    };
    virtual void setNonAssociatedFlowAM(const bool n) const
    {
      _lawTMSMP->setNonAssociatedFlowAM(n);
    };

    virtual void setTemperatureFunction_ThermalExpansionCoefficient_glassy(const scalarFunction& Tfunc);
    virtual void setTemperatureFunction_ThermalExpansionCoefficient_rubbery(const scalarFunction& Tfunc);
    virtual void setTemperatureFunction_ThermalExpansionCoefficient_AM(const scalarFunction& Tfunc);
    virtual void setTemperatureFunction_ThermalConductivity_glassy(const scalarFunction& Tfunc);
    virtual void setTemperatureFunction_ThermalConductivity_rubbery(const scalarFunction& Tfunc);
    virtual void setTemperatureFunction_ThermalConductivity_AM(const scalarFunction& Tfunc);
    virtual void setTemperatureFunction_Cp_glassy(const scalarFunction& Tfunc);
    virtual void setTemperatureFunction_Cp_rubbery(const scalarFunction& Tfunc);
    virtual void setTemperatureFunction_Cp_AM(const scalarFunction& Tfunc);
    virtual void setFunctionYieldCrystallinityG(const scalarFunction& Tfunc);
    virtual void setFunctionYieldCrystallinityR(const scalarFunction& Tfunc);
    virtual void setFunctionYieldCrystallinityAM(const scalarFunction& Tfunc);
    virtual void setFunctionCrystallizationVolumeRate(const scalarFunction &Tfunc);

    virtual void setElasticPotentialFunctionR(const elasticPotential& EPfuncR);
    virtual void setElasticPotentialFunctionAM(const elasticPotential& EPfuncAM);

    virtual void setTcTmWcWm(const double Atc, const double Btm, const double Cwc, const double Dwm,
                             const double alphaAtc, const double alphaBtm, const double alphaCwc, const double alphaDwm);
    virtual void setAlphaParam(const double GP, const double RP, const double ADP, const double AMP, const double GN, const double RN, const double ADN, const double AMN,
                const double alphaGP, const double alphaRP, const double alphaADP, const double alphaAMP,
                const double alphaGN, const double alphaRN, const double alphaADN, const double alphaAMN);




#ifndef SWIG
    virtual ~PhenomenologicalSMPDG3DMaterialLaw()
    {
        if (_lawTMSMP !=NULL)
        {
            delete _lawTMSMP;
            _lawTMSMP = NULL;
        };
    };
    PhenomenologicalSMPDG3DMaterialLaw(const PhenomenologicalSMPDG3DMaterialLaw &source);
    // set the time of _nldlaw
    virtual void setTime(const double t, const double dtime)
    {
        dG3DMaterialLaw::setTime(t,dtime);
        _lawTMSMP->setTime(t,dtime);
    }

    virtual materialLaw::matname getType()const                             {return _lawTMSMP->getType();}
    virtual void createIPState(IPStateBase* &ips,  bool hasBodyForce, const bool* state_=NULL, const MElement *ele=NULL,
                                         const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;

    virtual void createIPVariable       (IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt)const;
    virtual void initLaws               (const std::map<int, materialLaw*> &maplaw)  {}
    virtual double soundSpeed           ()const     {return _lawTMSMP->soundSpeed();} // or change value ??
    virtual void checkInternalState     (IPVariable *ipv, const IPVariable *ipvprev) const;
    virtual void stress(IPVariable *ipv, const IPVariable *ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
    virtual int getNumberOfExtraDofsDiffusion   ()const   {return 1;}
            double getExtraDofStoredEnergyPerUnitField  (double T)const;
    virtual materialLaw* clone          ()const     { return new PhenomenologicalSMPDG3DMaterialLaw(*this);};
    virtual bool withEnergyDissipation  ()const     {return _lawTMSMP->withEnergyDissipation();};
    virtual void setMacroSolver              (const nonLinearMechSolver* sv)
    {
        dG3DMaterialLaw::setMacroSolver(sv);
        if (_lawTMSMP!=NULL) _lawTMSMP->setMacroSolver(sv);
    }
    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
      return _lawTMSMP;
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
      return _lawTMSMP;
    }

#endif
};
//===========/end/PhenomenologicalSMPDG3DMaterialLaw===================================================================================



class crystalPlasticityDG3DMaterialLaw : public dG3DMaterialLaw{
  protected:
    #ifndef SWIG
    mlawCrystalPlasticity *_cplaw;
    #endif //SWIG

  public:
    crystalPlasticityDG3DMaterialLaw(const int num, const double rho, const double temperature,
                                const char *propName, const char *matName);
    #ifndef SWIG
    crystalPlasticityDG3DMaterialLaw(const crystalPlasticityDG3DMaterialLaw &source);
    virtual ~crystalPlasticityDG3DMaterialLaw();

    // set the time of _cplaw
    virtual void setTime(const double t,const double dtime);
    virtual materialLaw::matname getType() const;
    virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
    virtual double soundSpeed() const;// or change value ??
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const;
    virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
    virtual double scaleFactor() const;
    virtual materialLaw* clone() const{return new crystalPlasticityDG3DMaterialLaw(*this);};
    virtual bool withEnergyDissipation() const;
    virtual void setMacroSolver(const nonLinearMechSolver* sv);
    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
      return _cplaw;
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
      return _cplaw;
    }
    #endif
};

class VEVPUMATDG3DMaterialLaw : public dG3DMaterialLaw{
  protected:
    #ifndef SWIG
    mlawVEVPUMAT *_vevplaw;
    #endif //SWIG

  public:
    VEVPUMATDG3DMaterialLaw(const int num, const char *propName);
    #ifndef SWIG
    VEVPUMATDG3DMaterialLaw(const VEVPUMATDG3DMaterialLaw &source);
    virtual ~VEVPUMATDG3DMaterialLaw();

    // set the time of _cplaw
    virtual void setTime(const double t,const double dtime);
    virtual materialLaw::matname getType() const;
    virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
    virtual double soundSpeed() const;// or change value ??
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const;
    virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
    virtual double scaleFactor() const;
    virtual materialLaw* clone() const{return new VEVPUMATDG3DMaterialLaw(*this);};
    virtual bool withEnergyDissipation() const;
    virtual void setMacroSolver(const nonLinearMechSolver* sv);
    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
      return _vevplaw;
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
      return _vevplaw;
    }
    #endif
};

class IMDEACPUMATDG3DMaterialLaw : public dG3DMaterialLaw{
  protected:
    #ifndef SWIG
    mlawIMDEACPUMAT *_vevplaw;
    #endif //SWIG

  public:
    IMDEACPUMATDG3DMaterialLaw(const int num, const char *propName);
    #ifndef SWIG
    IMDEACPUMATDG3DMaterialLaw(const IMDEACPUMATDG3DMaterialLaw &source);
    virtual ~IMDEACPUMATDG3DMaterialLaw();

    // set the time of _cplaw
    virtual void setTime(const double t,const double dtime);
    virtual materialLaw::matname getType() const;
    virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
    virtual double soundSpeed() const;// or change value ??
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const;
    virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
    virtual double scaleFactor() const;
    virtual materialLaw* clone() const{return new IMDEACPUMATDG3DMaterialLaw(*this);};
    virtual bool withEnergyDissipation() const;
    virtual void setMacroSolver(const nonLinearMechSolver* sv);
    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
      return _vevplaw;
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
      return _vevplaw;
    }
    #endif
};

class gursonUMATDG3DMaterialLaw : public dG3DMaterialLaw{
  protected:
    #ifndef SWIG
    mlawGursonUMAT *_glaw;
    #endif //SWIG

  public:
    gursonUMATDG3DMaterialLaw(const int num, const double temperature, const char *propName);
    #ifndef SWIG
    gursonUMATDG3DMaterialLaw(const gursonUMATDG3DMaterialLaw &source);
    virtual ~gursonUMATDG3DMaterialLaw();

    // set the time of _cplaw
    virtual void setTime(const double t,const double dtime);
    virtual materialLaw::matname getType() const;
    virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
    virtual double soundSpeed() const;// or change value ??
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const;
    virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
    virtual double scaleFactor() const;
    virtual materialLaw* clone() const{return new gursonUMATDG3DMaterialLaw(*this);};
    virtual bool withEnergyDissipation() const;
    virtual void setMacroSolver(const nonLinearMechSolver* sv);
    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
      return _glaw;
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
      return _glaw;
    }
    #endif
};





class LinearElecTherMechDG3DMaterialLaw : public dG3DMaterialLaw // public materialLaw
{

 protected:
  mlawLinearElecTherMech *_lawLinearETM; // pointer to allow to choose between LLN style or Gmsh Style. The choice is performed by the constructor (2 constructors with != arguments)
  STensor3 	         Stiff_alphaDilatation;
  STensor3                lineark10, lineark20, linearl10, linearl20, jy1;
 public:
  LinearElecTherMechDG3DMaterialLaw(const int num,const double rho,const double Ex, const double Ey, const double Ez, const double Vxy, const double Vxz, const double Vyz,
		const double MUxy, const double MUxz, const double MUyz, const double alpha, const double beta, const double gamma,const double t0,const double Kx,
		const double Ky,const double Kz,const double alphax,const double alphay, const double alphaz,const  double lx,const double ly,const double lz,const double seebeck,
		const double c,const double v0);




#ifndef SWIG
	virtual ~LinearElecTherMechDG3DMaterialLaw() {
		if (_lawLinearETM !=NULL){ delete _lawLinearETM; _lawLinearETM = NULL;};
	}
 LinearElecTherMechDG3DMaterialLaw(const LinearElecTherMechDG3DMaterialLaw &source);
  // set the time of _nldlaw
  virtual void setTime(const double t,const double dtime){
    dG3DMaterialLaw::setTime(t,dtime);
    _lawLinearETM->setTime(t,dtime);
  }
  virtual materialLaw::matname getType() const {return _lawLinearETM->getType();}
  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
  virtual double soundSpeed() const{return _lawLinearETM->soundSpeed();} // or change value ??
  virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
  virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
  virtual int getNumberOfExtraDofsDiffusion() const {return 2;}
  double getExtraDofStoredEnergyPerUnitField(double T) const
  {
   Msg::Error("getExtraDofStoredEnergyPerUnitField not defined");
   return 0.;
  }
	virtual materialLaw* clone() const{ return new LinearElecTherMechDG3DMaterialLaw(*this);};
  virtual bool withEnergyDissipation() const {return _lawLinearETM->withEnergyDissipation();};
	virtual void setMacroSolver(const nonLinearMechSolver* sv){
		dG3DMaterialLaw::setMacroSolver(sv);
		if (_lawLinearETM!=NULL)
			_lawLinearETM->setMacroSolver(sv);
	}
  virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
  {
    return _lawLinearETM;
  }
  virtual materialLaw *getNonLinearSolverMaterialLaw()
  {
    return _lawLinearETM;
  }
#endif
};



class LinearElecMagTherMechDG3DMaterialLaw : public dG3DMaterialLaw
{
    protected:
    mlawLinearElecMagTherMech *_lawLinearEMTM;

    STensor3                lineark10, lineark20, linearl10, linearl20, jy1;
    bool _useFluxT;
    bool _evaluateCurlField;

    public:
    LinearElecMagTherMechDG3DMaterialLaw(const int num,const double rho,const double Ex, const double Ey, const double Ez,
    const double Vxy, const double Vxz, const double Vyz, const double MUxy, const double MUxz, const double MUyz,
    const double alpha, const double beta, const double gamma, const double t0,const double Kx,
	const double Ky,const double Kz,const double alphax,const double alphay, const double alphaz,
	const  double lx,const double ly,const double lz,const double seebeck, const double c,const double v0,
	const double mu_x, const double mu_y, const double mu_z, const double A0_x, const double A0_y,
	const double A0_z, const double Irms, const double freq, const unsigned int nTurnsCoil,
    const double coilLength_x, const double coilLength_y, const double coilLength_z, const double coilWidth, const bool useFluxT,
    const bool evaluateCurlField = true);

	#ifndef SWIG
	virtual ~LinearElecMagTherMechDG3DMaterialLaw()
	{
        if(_lawLinearEMTM != NULL)
        {
            delete _lawLinearEMTM;
            _lawLinearEMTM = NULL;
        }
	}

	LinearElecMagTherMechDG3DMaterialLaw(const LinearElecMagTherMechDG3DMaterialLaw &src);

	virtual void setTime(const double t,const double dtime)
	{
        dG3DMaterialLaw::setTime(t,dtime);
        _lawLinearEMTM->setTime(t,dtime);
    }

    virtual materialLaw::matname getType() const
    {
        return _lawLinearEMTM->getType();
    }
    virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0,
    const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
    virtual double soundSpeed() const
    {
        return _lawLinearEMTM->soundSpeed();  // or change value ??
    }
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
    virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true,
    const bool checkfrac=true, const bool dTangent=false);
    virtual int getNumberOfExtraDofsDiffusion() const {return 2;} // Change in value?
    double getExtraDofStoredEnergyPerUnitField(double T) const
    {
        Msg::Error("getExtraDofStoredEnergyPerUnitField not defined");
        return 0.;
    }
    virtual int getNumCurlVariable() const {return 1;} // Magnetic vector potential
    virtual materialLaw* clone() const{ return new LinearElecMagTherMechDG3DMaterialLaw(*this);}
    virtual bool withEnergyDissipation() const {return _lawLinearEMTM->withEnergyDissipation();}
    virtual void setMacroSolver(const nonLinearMechSolver* sv)
    {
        dG3DMaterialLaw::setMacroSolver(sv);
        if (_lawLinearEMTM!=NULL)
            _lawLinearEMTM->setMacroSolver(sv);
    }
    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
      return _lawLinearEMTM;
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
      return _lawLinearEMTM;
    }
	#endif // SWIG

};

class LinearElecMagInductorDG3DMaterialLaw : public dG3DMaterialLaw
{
protected:
  mlawLinearElecMagInductor *_lawLinearEMInductor;
  STensor3 lineark10, lineark20, linearl10, linearl20;
  bool _useFluxT;
  bool _evaluateCurlField;

public:
  LinearElecMagInductorDG3DMaterialLaw(int num,const double rho, const double Ex, const double Ey, const double Ez,
    const double Vxy, const double Vxz, const double Vyz, const double MUxy, const double MUxz,
    const double MUyz,  const double alpha, const double beta, const double gamma,
    const double t0,const double Kx,const double Ky, const double Kz,const double alphax,
    const double alphay,const double alphaz, const  double lx,const  double ly,
    const  double lz,const double seebeck,const double cp,const double v0,
    const double mu_x, const double mu_y, const double mu_z, const double A0_x, const double A0_y,
    const double A0_z, const double Irms, const double freq, const unsigned int nTurnsCoil,
    const double coilLength_x, const double coilLength_y, const double coilLength_z, const double coilWidth,
    const double Centroid_x, const double Centroid_y, const double Centroid_z,
    const double CentralAxis_x, const double CentralAxis_y, const double CentralAxis_z, const bool useFluxT,
    const bool evaluateCurlField = true);

  #ifndef SWIG
  LinearElecMagInductorDG3DMaterialLaw(const LinearElecMagInductorDG3DMaterialLaw &source);
  virtual ~LinearElecMagInductorDG3DMaterialLaw()
  {
    if(_lawLinearEMInductor != NULL)
        {
            delete _lawLinearEMInductor;
            _lawLinearEMInductor = NULL;
        }
  }

  virtual void setTime(const double t,const double dtime)
	{
        dG3DMaterialLaw::setTime(t,dtime);
        _lawLinearEMInductor->setTime(t,dtime);
  }

  virtual materialLaw::matname getType() const
  {
      return _lawLinearEMInductor->getType();
  }
  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0,
    const IntPt *GP=NULL, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
  virtual double soundSpeed() const
  {
      return _lawLinearEMInductor->soundSpeed();  // or change value ??
  }
  virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
  virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true,
    const bool checkfrac=true, const bool dTangent=false);
  virtual materialLaw* clone() const{ return new LinearElecMagInductorDG3DMaterialLaw(*this);}
  virtual bool withEnergyDissipation() const {return _lawLinearEMInductor->withEnergyDissipation();}
  virtual void setMacroSolver(const nonLinearMechSolver* sv)
  {
      dG3DMaterialLaw::setMacroSolver(sv);
      if (_lawLinearEMInductor!=NULL)
          _lawLinearEMInductor->setMacroSolver(sv);
  }
  virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
  {
    return _lawLinearEMInductor;
  }
  virtual materialLaw *getNonLinearSolverMaterialLaw()
  {
    return _lawLinearEMInductor;
  }
  #endif // SWIG

};


class mlawAnIsotropicElecTherMechDG3DMaterialLaw :public dG3DMaterialLaw
{

 protected:
   mlawAnIsotropicElecTherMech  *_lawETM;
   STensor3 	 Stiff_alphaDilatation;

 // pointer to allow to choose between LLN style or Gmsh Style. The choice is performed by the constructor (2 constructors with != arguments)
 //modified
  STensor3                Kref10, Kref20, Lref10, Lref20, jy1;
 public:


  mlawAnIsotropicElecTherMechDG3DMaterialLaw(const int num,const double E, const double nu,const double rho,const double EA, const double GA,const double nu_minor,const double Ax,
					     const double Ay, const double Az, const double alpha, const double beta, const double gamma,const double t0,const  double lx,
					     const  double ly,const  double lz,const double seebeck,const double kx,const double ky, const double kz,const double cp,const double alphath,const double v0);


#ifndef SWIG
 mlawAnIsotropicElecTherMechDG3DMaterialLaw(const mlawAnIsotropicElecTherMechDG3DMaterialLaw &source);
 virtual ~mlawAnIsotropicElecTherMechDG3DMaterialLaw() {
	 if (_lawETM != NULL){
		delete _lawETM;
		_lawETM = NULL;
	 }
 }
  // set the time of _nldlaw
  virtual void setTime(const double t,const double dtime){
    dG3DMaterialLaw::setTime(t,dtime);
    _lawETM->setTime(t,dtime);
  }
  virtual materialLaw::matname getType() const {return _lawETM->getType();}
  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
  virtual double soundSpeed() const{return _lawETM->soundSpeed();} // or change value ??
  virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
  virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
  virtual int getNumberOfExtraDofsDiffusion() const {return 2;}
  double getExtraDofStoredEnergyPerUnitField(double T) const
  {
   Msg::Error("getExtraDofStoredEnergyPerUnitField not defined");
   return 0.;
  }
	virtual materialLaw* clone() const{ return new mlawAnIsotropicElecTherMechDG3DMaterialLaw(*this);};
  virtual bool withEnergyDissipation() const {return  _lawETM->withEnergyDissipation();};
	virtual void setMacroSolver(const nonLinearMechSolver* sv){
		dG3DMaterialLaw::setMacroSolver(sv);
		if (_lawETM!=NULL)
			_lawETM->setMacroSolver(sv);
	}
  virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
  {
    return _lawETM;
  }
  virtual materialLaw *getNonLinearSolverMaterialLaw()
  {
    return _lawETM;
  }
#endif
};



class mlawElecSMPDG3DMaterialLaw :public dG3DMaterialLaw
{

 protected:
   mlawElecSMP  *_lawETMSMP;
   STensor3	 Stiff_alphaDilatation;
 // pointer to allow to choose between LLN style or Gmsh Style. The choice is performed by the constructor (2 constructors with != arguments)
//modified
 STensor3                K10, K20, L10, L20, jy1;
 public:


  mlawElecSMPDG3DMaterialLaw(const int num,const double rho, const double alpha, const double beta, const double gamma,const double t0,const double Kx,const double Ky,
			 const double Kz, const double mu_groundState3,const double Im3, const double pi  ,const double Tr,
		          const double Nmu_groundState2,const double mu_groundState2, const double Im2,const double Sgl2,const double Sr2,const double Delta,const double m2, const double epsilonr,
			  const double n, const double epsilonp02,const double alphar1,const double alphagl1,const double Ggl1,const double Gr1,const double Mgl1,const double Mr1,const double Mugl1
			  ,const double Mur1, const double epsilon01,const double QglOnKb1,const double QrOnKb1,const double epsygl1 ,const double d1,const double m1,const double VOnKb1,
			  const double alphap,const double  Sa0,const double ha1,const double b1,const double g1,const double phia01,const double Z1,const double r1,const double s1,const double Sb01
			  ,const double Hgl1,const double Lgl1,const double Hr1,const double Lr1,const double l1,const double Kb,const double be1,const double c0,const double wp,const double c1
			  ,const double lx,const double ly,const double lz,const double seebeck,const double v0);

  void setStrainOrder(const int order);
  virtual void setMechanism2(bool mechanism2);

#ifndef SWIG
	virtual ~mlawElecSMPDG3DMaterialLaw() {
		if (_lawETMSMP != NULL){
			delete _lawETMSMP;
			_lawETMSMP = NULL;
		}
	};
 mlawElecSMPDG3DMaterialLaw(const mlawElecSMPDG3DMaterialLaw &source);
  // set the time of _nldlaw
  virtual void setTime(const double t,const double dtime){
    dG3DMaterialLaw::setTime(t,dtime);
    _lawETMSMP->setTime(t,dtime);
  }
  virtual materialLaw::matname getType() const {return _lawETMSMP->getType();}
  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
  virtual double soundSpeed() const{return _lawETMSMP->soundSpeed();} // or change value ??
  virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
  virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
  virtual int getNumberOfExtraDofsDiffusion() const {return 2;}
  double getExtraDofStoredEnergyPerUnitField(double T) const
  {
   Msg::Error("getExtraDofStoredEnergyPerUnitField not defined");
   return 0.;
  }
	virtual materialLaw* clone() const{ return new mlawElecSMPDG3DMaterialLaw(*this);};
  virtual bool withEnergyDissipation() const {return _lawETMSMP->withEnergyDissipation();};
	virtual void setMacroSolver(const nonLinearMechSolver* sv){
		dG3DMaterialLaw::setMacroSolver(sv);
		if (_lawETMSMP!=NULL)
			_lawETMSMP->setMacroSolver(sv);
	}
  virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
  {
    return _lawETMSMP;
  }
  virtual materialLaw *getNonLinearSolverMaterialLaw()
  {
    return _lawETMSMP;
  }
#endif
};

class GenericThermoMechanicsDG3DMaterialLaw : public dG3DMaterialLaw
{

protected:
    mlawGenericTM *_lawGenericTM;

    STensor3 linearK;
    STensor3 Stiff_alphaDilatation;
public:
    GenericThermoMechanicsDG3DMaterialLaw(const int num, const double rho, const double alpha,
                                         const double beta, const double gamma, const double t0,
                                         const double Kx, const double Ky, const double Kz, const double alphax,
                                         const double alphay, const double alphaz, const double cp);

    virtual ~GenericThermoMechanicsDG3DMaterialLaw()
    {
        if (_lawGenericTM != NULL)
        {
            delete _lawGenericTM;
            _lawGenericTM = NULL;
        }
    };
    void setLawForCp(const scalarFunction& funcCp);
    const materialLaw &getConstRefMechanicalMaterialLaw() const;
    materialLaw &getRefMechanicalMaterialLaw();
    void setMechanicalMaterialLaw(const dG3DMaterialLaw *mlaw);
    void setApplyReferenceF(const bool rf);
#ifndef SWIG
    GenericThermoMechanicsDG3DMaterialLaw(const GenericThermoMechanicsDG3DMaterialLaw &source);
    // set the time of _nldlaw
    virtual void setTime(const double t,const double dtime){
        dG3DMaterialLaw::setTime(t,dtime);
        _lawGenericTM->setTime(t,dtime);
    }
    virtual materialLaw::matname getType() const {return _lawGenericTM->getType();}
    virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
    virtual double soundSpeed() const{return _lawGenericTM->soundSpeed();} // or change value ??
    virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
    virtual double scaleFactor() const {return getConstRefMechanicalMaterialLaw().scaleFactor();};
    virtual int getNumberOfExtraDofsDiffusion() const {return 1;}
    virtual double getExtraDofStoredEnergyPerUnitField(double T) const;
    virtual double getInitialExtraDofStoredEnergyPerUnitField() const;
    virtual double defoEnergy(const STensor3& Fn,const SVector3& gradT) const{} // do nothing for now
    virtual double defoEnergy( const SVector3&ujump,const double &Tjump, const SVector3& N) const{} // do nothing for now
    virtual materialLaw* clone() const{ return new GenericThermoMechanicsDG3DMaterialLaw(*this);};
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
    virtual bool withEnergyDissipation() const {return _lawGenericTM->withEnergyDissipation();};
    virtual void setMacroSolver(const nonLinearMechSolver* sv){
        dG3DMaterialLaw::setMacroSolver(sv);
        if (_lawGenericTM!=NULL){
            _lawGenericTM->setMacroSolver(sv);
        }
    }
    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
        return _lawGenericTM;
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
        return _lawGenericTM;
    }
#endif
};

class GenericResidualMechanicsDG3DMaterialLaw : public dG3DMaterialLaw
{

protected:
    mlawGenericRM *_lawGenericRM;

public:
    GenericResidualMechanicsDG3DMaterialLaw(const int num, const double rho, const char* inputfilename, const bool init = true);

    virtual ~GenericResidualMechanicsDG3DMaterialLaw()
    {
        if (_lawGenericRM != NULL)
        {
            delete _lawGenericRM;
            _lawGenericRM = NULL;
        }
    };
    void setLawForFRes(const scalarFunction& funcCp);
    const materialLaw &getConstRefMechanicalMaterialLaw() const;
    materialLaw &getRefMechanicalMaterialLaw();
    void setMechanicalMaterialLaw(const dG3DMaterialLaw *mlaw);
#ifndef SWIG
    GenericResidualMechanicsDG3DMaterialLaw(const GenericResidualMechanicsDG3DMaterialLaw &source);
    // set the time of _nldlaw
    virtual void setTime(const double t,const double dtime){
        dG3DMaterialLaw::setTime(t,dtime);
        _lawGenericRM->setTime(t,dtime);
    }
    virtual materialLaw::matname getType() const {return _lawGenericRM->getType();}
    virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
    virtual double soundSpeed() const{return _lawGenericRM->soundSpeed();} // or change value ??
    virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
    //virtual double defoEnergy(const STensor3& Fn,const SVector3& gradT) const{} // do nothing for now
    //virtual double defoEnergy( const SVector3&ujump,const double &Tjump, const SVector3& N) const{} // do nothing for now
    virtual materialLaw* clone() const{ return new GenericResidualMechanicsDG3DMaterialLaw(*this);};
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
    virtual bool withEnergyDissipation() const {return _lawGenericRM->withEnergyDissipation();};
    virtual void setMacroSolver(const nonLinearMechSolver* sv){
        dG3DMaterialLaw::setMacroSolver(sv);
        if (_lawGenericRM!=NULL){
            _lawGenericRM->setMacroSolver(sv);
        }
    }
    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
        return _lawGenericRM;
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
        return _lawGenericRM;
    }
#endif
};


class ElecMagGenericThermoMechanicsDG3DMaterialLaw : public dG3DMaterialLaw
{
protected:
    mlawElecMagGenericThermoMech *_lawElecMagGenericThermoMech;

    bool _useFluxT;
    bool _evaluateCurlField;
    bool _evaluateTemperature;
    STensor3 lineark10, lineark20, linearl10, linearl20, jy1;

public:
    ElecMagGenericThermoMechanicsDG3DMaterialLaw(const int num, const double rho,
                                                 const double alpha, const double beta, const double gamma,
                                                 const  double lx,const double ly,const double lz,
                                                 const double seebeck, const double v0,
                                                 const double mu_x, const double mu_y, const double mu_z,
                                                 const double epsilon_4,const double epsilon_5,const double epsilon_6, // for constitutive modeling
                                                 const double mu_7,const double mu_8,const double mu_9, // for constitutive modeling
                                                 const double A0_x, const double A0_y, const double A0_z,
                                                 const double Irms, const double freq, const unsigned int nTurnsCoil,
                                                 const double coilLength_x, const double coilLength_y,
                                                 const double coilLength_z, const double coilWidth,
                                                 const bool useFluxT, const bool evaluateCurlField = true,
                                                 const bool evaluateTemperature = true);

    virtual ~ElecMagGenericThermoMechanicsDG3DMaterialLaw()
    {
        if (_lawElecMagGenericThermoMech != NULL)
        {
            delete _lawElecMagGenericThermoMech;
            _lawElecMagGenericThermoMech = NULL;
        }
    };
    const materialLaw &getConstRefToThermoMechanicalMaterialLaw() const;
    materialLaw &getRefToThermoMechanicalMaterialLaw();
    void setThermoMechanicalMaterialLaw(const dG3DMaterialLaw *mlaw);
    void setApplyReferenceF(const bool rf);
    void setUseEMStress(const bool emStress);
    void setEMFieldDependentShearModulus(const bool EM_ShearModulus);
    void setEMFieldDependentShearModulusTestParameters(const double alpha_e,const double m_e,const double g1=0.);
    void setCVoltage(double cVoltage){   _lawElecMagGenericThermoMech->setCVoltage(cVoltage);}
#ifndef SWIG
    ElecMagGenericThermoMechanicsDG3DMaterialLaw(const ElecMagGenericThermoMechanicsDG3DMaterialLaw &source);
    // set the time of _nldlaw
    virtual void setTime(const double t,const double dtime){
        dG3DMaterialLaw::setTime(t,dtime);
        _lawElecMagGenericThermoMech->setTime(t,dtime);
    }
    virtual materialLaw::matname getType() const {return _lawElecMagGenericThermoMech->getType();}
    virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
    virtual double soundSpeed() const{return _lawElecMagGenericThermoMech->soundSpeed();} // or change value ??
    virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
    virtual double scaleFactor() const {return getConstRefToThermoMechanicalMaterialLaw().scaleFactor();};
    virtual int getNumberOfExtraDofsDiffusion() const {return 2;}
    virtual int getNumCurlVariable() const {return 1;} // Magnetic vector potential
    virtual double getExtraDofStoredEnergyPerUnitField(double T) const;
    virtual double getInitialExtraDofStoredEnergyPerUnitField() const;
    virtual double defoEnergy(const STensor3& Fn,const SVector3& gradT) const{} // do nothing for now
    virtual double defoEnergy( const SVector3&ujump,const double &Tjump, const SVector3& N) const{} // do nothing for now
    virtual materialLaw* clone() const{ return new ElecMagGenericThermoMechanicsDG3DMaterialLaw(*this);};
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
    virtual bool withEnergyDissipation() const {return _lawElecMagGenericThermoMech->withEnergyDissipation();};
    virtual void setMacroSolver(const nonLinearMechSolver* sv){
        dG3DMaterialLaw::setMacroSolver(sv);
        if (_lawElecMagGenericThermoMech!=NULL){
            _lawElecMagGenericThermoMech->setMacroSolver(sv);
        }
    }
    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
        return _lawElecMagGenericThermoMech;
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
        return _lawElecMagGenericThermoMech;
    }
#endif
};

class ElecMagInductorDG3DMaterialLaw : public  dG3DMaterialLaw
{
protected:
    mlawElecMagInductor *_lawElecMagInductor;
    STensor3 lineark10, lineark20, linearl10, linearl20;
    bool _useFluxT;
    bool _evaluateCurlField;

public:
    ElecMagInductorDG3DMaterialLaw(const int num, const double rho,
                                   const double alpha, const double beta, const double gamma,
                                   const  double lx,const double ly,const double lz,
                                   const double seebeck, const double v0,
                                   const double mu_x, const double mu_y, const double mu_z,
                                   const double A0_x, const double A0_y, const double A0_z,
                                   const double Irms, const double freq, const unsigned int nTurnsCoil,
                                   const double coilLength_x, const double coilLength_y,
                                   const double coilLength_z, const double coilWidth,
                                   const double Centroid_x, const double Centroid_y, const double Centroid_z,
                                   const double CentralAxis_x, const double CentralAxis_y, const double CentralAxis_z,
                                   const bool useFluxT, const bool evaluateCurlField = true);

    virtual ~ElecMagInductorDG3DMaterialLaw()
    {
        if (_lawElecMagInductor != NULL)
        {
            delete _lawElecMagInductor;
            _lawElecMagInductor = NULL;
        }
    };
    const materialLaw &getConstRefToThermoMechanicalMaterialLaw() const;
    materialLaw &getRefToThermoMechanicalMaterialLaw();
    void setThermoMechanicalMaterialLaw(const dG3DMaterialLaw *mlaw);
    void setApplyReferenceF(const bool rf);
    void setUseEMStress(const bool emStress);
    void setEMFieldDependentShearModulus(const bool EM_ShearModulus);
    void setEMFieldDependentShearModulusTestParameters(const double alpha_e,const double m_e,const double g1=0.);
#ifndef SWIG
    ElecMagInductorDG3DMaterialLaw(const ElecMagInductorDG3DMaterialLaw &source);
    virtual void setTime(const double t,const double dtime)
    {
        dG3DMaterialLaw::setTime(t,dtime);
        _lawElecMagInductor->setTime(t,dtime);
    }

    virtual materialLaw::matname getType() const
    {
        return _lawElecMagInductor->getType();
    }
    virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0,
                               const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
    virtual double soundSpeed() const
    {
        return _lawElecMagInductor->soundSpeed();  // or change value ??
    }
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
    virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true,
                        const bool checkfrac=true, const bool dTangent=false);
    virtual int getNumberOfExtraDofsDiffusion() const {return 2;}
    virtual int getNumCurlVariable() const {return 1;} // Magnetic vector potential
    virtual double getExtraDofStoredEnergyPerUnitField(double T) const;
    virtual double getInitialExtraDofStoredEnergyPerUnitField() const;
    virtual double defoEnergy(const STensor3& Fn,const SVector3& gradT) const{} // do nothing for now
    virtual double defoEnergy( const SVector3&ujump,const double &Tjump, const SVector3& N) const{} // do nothing for now
    virtual materialLaw* clone() const{ return new ElecMagInductorDG3DMaterialLaw(*this);}
    virtual bool withEnergyDissipation() const {return _lawElecMagInductor->withEnergyDissipation();}
    virtual void setMacroSolver(const nonLinearMechSolver* sv)
    {
        dG3DMaterialLaw::setMacroSolver(sv);
        if (_lawElecMagInductor!=NULL)
            _lawElecMagInductor->setMacroSolver(sv);
    }
    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
        return _lawElecMagInductor;
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
        return _lawElecMagInductor;
    }
#endif // SWIG
};


// NonLinearTVM Law Interface =================================================================== BEGIN

class NonLinearTVMDG3DMaterialLaw : public dG3DMaterialLaw{   // public MaterialLaw
  #ifndef SWIG
  protected:
    mlawNonLinearTVM _viscoLaw;

  #endif //SWIG
  public:

    NonLinearTVMDG3DMaterialLaw(const int num, const double rho, const double E, const double nu,
                        const double Tinitial, const double Alpha, const double KThCon, const double Cp,
                        const bool matrixbyPerturbation = false, const double pert = 1e-8, const bool thermalEstimationPreviousConfig = false);

    void setStrainOrder(const int order);
    void setViscoelasticMethod(const int method);
    void setViscoElasticNumberOfElement(const int N);
    void setViscoElasticData(const int i, const double Ei, const double taui);
    void setViscoElasticData_Bulk(const int i, const double Ki, const double ki);
    void setViscoElasticData_Shear(const int i, const double Gi, const double gi);
    void setViscoElasticData(const std::string filename);
    void setVolumeCorrection(const double _vc, const double _xivc, const double _zetavc, const double _dc, const double _thetadc, const double _pidc);
    void setAdditionalVolumeCorrections(const double V3, const double V4, const double V5, const double D3, const double D4, const double D5);
    void setExtraBranch_CompressionParameter(const double compCorrection, const double compCorrection_2, const double compCorrection_3);
    void setExtraBranchNLType(const int type);
    void setTensionCompressionRegularisation(const double tensionCompressionRegularisation);
    void setCorrectionsAllBranchesTVE(const int i, const double V0,const double V1, const double V2, const double D0, const double D1, const double D2);
    void setAdditionalCorrectionsAllBranchesTVE(const int i, const double V3, const double V4, const double V5, const double D3, const double D4, const double D5);
    void setCompressionCorrectionsAllBranchesTVE(const int i, const double Ci);

    void setReferenceTemperature(const double Tref);
    void setShiftFactorConstantsWLF(const double C1, const double C2);
    void useExtraBranchBool(const bool useExtraBranch);
    void useExtraBranchBool_TVE(const bool useExtraBranch_TVE, const int ExtraBranch_TVE_option);
    void useRotationCorrectionBool(const bool useRotationCorrection, const int rotationCorrectionScheme);
    void setTemperatureFunction_BulkModulus(const scalarFunction& Tfunc);
    void setTemperatureFunction_ShearModulus(const scalarFunction& Tfunc);
    void setTemperatureFunction_ThermalDilationCoefficient(const scalarFunction& Tfunc);
    void setTemperatureFunction_ThermalConductivity(const scalarFunction& Tfunc);
    void setTemperatureFunction_SpecificHeat(const scalarFunction& Tfunc);

    void setMullinsEffect(const mullinsEffect& mullins);

    #ifndef SWIG
     NonLinearTVMDG3DMaterialLaw(const NonLinearTVMDG3DMaterialLaw &source);
    virtual ~NonLinearTVMDG3DMaterialLaw(){}
    // set the time of _j2law
    virtual void setTime(const double t,const double dtime){
      dG3DMaterialLaw::setTime(t,dtime);
      _viscoLaw.setTime(t,dtime);
    }
    virtual materialLaw::matname getType() const {return _viscoLaw.getType();}
    virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
    virtual double soundSpeed() const{return _viscoLaw.soundSpeed();} // or change value ??
    virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
    virtual const double bulkModulus() const {return _viscoLaw.bulkModulus();}
    virtual double scaleFactor() const{return _viscoLaw.scaleFactor();}

    virtual int getNumberOfExtraDofsDiffusion() const {return 1;}
    virtual double getExtraDofStoredEnergyPerUnitField(double T) const;
    virtual double getInitialExtraDofStoredEnergyPerUnitField() const;

    virtual materialLaw* clone() const{return new NonLinearTVMDG3DMaterialLaw(*this);};
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
    virtual bool withEnergyDissipation() const {return _viscoLaw.withEnergyDissipation();};
    virtual void setMacroSolver(const nonLinearMechSolver* sv){
      dG3DMaterialLaw::setMacroSolver(sv);
      _viscoLaw.setMacroSolver(sv);
    };

    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
      return &_viscoLaw;
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
      return &_viscoLaw;
    }
    #endif //SWIG
};

// NonLinearTVM Law Interface =================================================================== END

// NonLinearTVP Law Interface =================================================================== START

class NonLinearTVPDG3DMaterialLaw : public dG3DMaterialLaw{   // public MaterialLaw
  #ifndef SWIG
  protected:
    mlawNonLinearTVP _viscoLaw;

  #endif //SWIG
  public:
    NonLinearTVPDG3DMaterialLaw(const int num, const double rho, const double E, const double nu, const double tol,
                        const double Tinitial, const double Alpha, const double KThCon, const double Cp,
                        const bool matrixbyPerturbation = false, const double pert = 1e-8, const bool thermalEstimationPreviousConfig = false);

    void setStrainOrder(const int order);
    void setViscoelasticMethod(const int method);
    void setViscoElasticNumberOfElement(const int N);
    void setViscoElasticData(const int i, const double Ei, const double taui);
    // void setViscoElasticData(const int i, const double Ei, const double taui, const double Alphai);
    void setViscoElasticData_Bulk(const int i, const double Ki, const double ki);
    void setViscoElasticData_Shear(const int i, const double Gi, const double gi);
    // void setViscoElasticData_Alpha(const int i, const double Alphai);
    void setViscoElasticData(const std::string filename);
    void setIsotropicHardeningCoefficients(const double HR1, const double HR2, const double HR3);
    void setPolynomialOrderChabocheCoeffs(const int order);
    void setPolynomialCoeffsChabocheCoeffs(const int i, const double val);

    void setReferenceTemperature(const double Tref);
    // void setTempFuncOption(const double TemFuncOpt);
    // void setModelOption(const int modelFlag);
    // void setTestOption(const int testFlag);
    void setShiftFactorConstantsWLF(const double C1, const double C2);
    void setTemperatureFunction_BulkModulus(const scalarFunction& Tfunc);
    void setTemperatureFunction_ShearModulus(const scalarFunction& Tfunc);
    void setTemperatureFunction_ThermalDilationCoefficient(const scalarFunction& Tfunc);
    void setTemperatureFunction_ThermalConductivity(const scalarFunction& Tfunc);
    void setTemperatureFunction_SpecificHeat(const scalarFunction& Tfunc);
    void setTemperatureFunction_ElasticCorrection_Bulk(const scalarFunction& Tfunc);
    void setTemperatureFunction_ElasticCorrection_Shear(const scalarFunction& Tfunc);
    // void setTemperatureFunction_BranchBulkModuli(const scalarFunction& Tfunc,int i);
    // void setTemperatureFunction_BranchShearModuli(const scalarFunction& Tfunc,int i);
    // void setTemperatureFunction_BranchThermalDilationCoefficient(const scalarFunction& Tfunc,int i);
    void useExtraBranchBool(const bool useExtraBranch);
    void setVolumeCorrection(const double _vc, const double _xivc, const double _zetavc, const double _dc, const double _thetadc, const double _pidc);
    void setAdditionalVolumeCorrections(const double V3, const double V4, const double V5, const double D3, const double D4, const double D5);
    void setExtraBranch_CompressionParameter(const double compCorrection, const double compCorrection_2, const double compCorrection_3);
    void setExtraBranchNLType(const int type);
    void setTensionCompressionRegularisation(const double tensionCompressionRegularisation);

    void setCompressionHardening(const J2IsotropicHardening& comp);
    void setTractionHardening(const J2IsotropicHardening& trac);
    void setKinematicHardening(const kinematicHardening& kin);
    void setViscosityEffect(const viscosityLaw& v, const double p);
    void setMullinsEffect(const mullinsEffect& mullins);

    void setYieldPowerFactor(const double n);
    void nonAssociatedFlowRuleFactor(const double b);
    void setPlasticPoissonRatio(const double nup);
    void setNonAssociatedFlow(const bool flag);

    void setTaylorQuineyFactor(const double f, const bool flag = false);
    void setTemperatureFunction_InitialYieldStress(const scalarFunction& Tfunc);
    void setTemperatureFunction_Hardening(const scalarFunction& Tfunc);
    void setTemperatureFunction_KinematicHardening(const scalarFunction& Tfunc);
    void setTemperatureFunction_Viscosity(const scalarFunction& Tfunc);

    #ifndef SWIG
     NonLinearTVPDG3DMaterialLaw(const NonLinearTVPDG3DMaterialLaw &source);
    virtual ~NonLinearTVPDG3DMaterialLaw(){}
    // set the time of _j2law
    virtual void setTime(const double t,const double dtime){
      dG3DMaterialLaw::setTime(t,dtime);
      _viscoLaw.setTime(t,dtime);
    }
    virtual materialLaw::matname getType() const {return _viscoLaw.getType();}
    virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
    virtual double soundSpeed() const{return _viscoLaw.soundSpeed();} // or change value ??
    virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
    virtual const double bulkModulus() const {return _viscoLaw.bulkModulus();}
    virtual double scaleFactor() const{return _viscoLaw.scaleFactor();}

    virtual int getNumberOfExtraDofsDiffusion() const {return 1;}
    virtual double getExtraDofStoredEnergyPerUnitField(double T) const;
    virtual double getInitialExtraDofStoredEnergyPerUnitField() const;

    virtual materialLaw* clone() const{return new NonLinearTVPDG3DMaterialLaw(*this);};
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
    virtual bool withEnergyDissipation() const {return _viscoLaw.withEnergyDissipation();};
    virtual void setMacroSolver(const nonLinearMechSolver* sv){
      dG3DMaterialLaw::setMacroSolver(sv);
      _viscoLaw.setMacroSolver(sv);
    };

    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
      return &_viscoLaw;
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
      return &_viscoLaw;
    }
    #endif //SWIG
};

// NonLinearTVP Law Interface =================================================================== END

// NonLinearTVENonLinearTVP Law Interface =================================================================== START

class NonLinearTVENonLinearTVPDG3DMaterialLaw : public dG3DMaterialLaw{   // public MaterialLaw
  #ifndef SWIG
  protected:
    mlawNonLinearTVENonLinearTVP _viscoLaw;

  #endif //SWIG
  public:
    NonLinearTVENonLinearTVPDG3DMaterialLaw(const int num, const double rho, const double E, const double nu, const double tol,
                        const double Tinitial, const double Alpha, const double KThCon, const double Cp,
                        const bool matrixbyPerturbation = false, const double pert = 1e-8, const double stressIteratorTol = 1e-9, const bool thermalEstimationPreviousConfig = false);

    void setStrainOrder(const int order);
    void setViscoelasticMethod(const int method);
    void setViscoElasticNumberOfElement(const int N);
    void setViscoElasticData(const int i, const double Ei, const double taui);
    // void setViscoElasticData(const int i, const double Ei, const double taui, const double Alphai);
    void setViscoElasticData_Bulk(const int i, const double Ki, const double ki);
    void setViscoElasticData_Shear(const int i, const double Gi, const double gi);
    // void setViscoElasticData_Alpha(const int i, const double Alphai);
    void setViscoElasticData(const std::string filename);
    void setIsotropicHardeningCoefficients(const double HR1, const double HR2, const double HR3);
    void setPolynomialOrderChabocheCoeffs(const int order);
    void setPolynomialCoeffsChabocheCoeffs(const int i, const double val);

    void setReferenceTemperature(const double Tref);
    // void setTempFuncOption(const double TemFuncOpt);
    // void setModelOption(const int modelFlag);
    // void setTestOption(const int testFlag);
    void setShiftFactorConstantsWLF(const double C1, const double C2);
    void setTemperatureFunction_BulkModulus(const scalarFunction& Tfunc);
    void setTemperatureFunction_ShearModulus(const scalarFunction& Tfunc);
    void setTemperatureFunction_ThermalDilationCoefficient(const scalarFunction& Tfunc);
    void setTemperatureFunction_ThermalConductivity(const scalarFunction& Tfunc);
    void setTemperatureFunction_SpecificHeat(const scalarFunction& Tfunc);
    void setTemperatureFunction_ElasticCorrection_Bulk(const scalarFunction& Tfunc);
    void setTemperatureFunction_ElasticCorrection_Shear(const scalarFunction& Tfunc);
    // void setTemperatureFunction_BranchBulkModuli(const scalarFunction& Tfunc,int i);
    // void setTemperatureFunction_BranchShearModuli(const scalarFunction& Tfunc,int i);
    // void setTemperatureFunction_BranchThermalDilationCoefficient(const scalarFunction& Tfunc,int i);
    void useExtraBranchBool(const bool useExtraBranch);
    void useExtraBranchBool_TVE(const bool useExtraBranch_TVE, const int ExtraBranch_TVE_option);
    void useRotationCorrectionBool(const bool useRotationCorrection, const int rotationCorrectionScheme);
    void setVolumeCorrection(const double _vc, const double _xivc, const double _zetavc, const double _dc, const double _thetadc, const double _pidc);
    void setAdditionalVolumeCorrections(const double V3, const double V4, const double V5, const double D3, const double D4, const double D5);
    void setExtraBranch_CompressionParameter(const double compCorrection, const double compCorrection_2, const double compCorrection_3);
    void setExtraBranchNLType(const int type);
    void setTensionCompressionRegularisation(const double tensionCompressionRegularisation);
    void setCorrectionsAllBranchesTVE(const int i, const double V0,const double V1, const double V2, const double D0, const double D1, const double D2);
    void setAdditionalCorrectionsAllBranchesTVE(const int i, const double V3, const double V4, const double V5, const double D3, const double D4, const double D5);
    void setCompressionCorrectionsAllBranchesTVE(const int i, const double Ci);

    void setCompressionHardening(const J2IsotropicHardening& comp);
    void setTractionHardening(const J2IsotropicHardening& trac);
    void setKinematicHardening(const kinematicHardening& kin);
    void setViscosityEffect(const viscosityLaw& v, const double p);
    void setMullinsEffect(const mullinsEffect& mullins);

    void setYieldPowerFactor(const double n);
    void nonAssociatedFlowRuleFactor(const double b);
    void setPlasticPoissonRatio(const double nup);
    void setNonAssociatedFlow(const bool flag);

    void setTaylorQuineyFactor(const double f, const bool flag = false);
    void setTemperatureFunction_InitialYieldStress(const scalarFunction& Tfunc);
    void setTemperatureFunction_Hardening(const scalarFunction& Tfunc);
    void setTemperatureFunction_KinematicHardening(const scalarFunction& Tfunc);
    void setTemperatureFunction_Viscosity(const scalarFunction& Tfunc);

    #ifndef SWIG
     NonLinearTVENonLinearTVPDG3DMaterialLaw(const NonLinearTVENonLinearTVPDG3DMaterialLaw &source);
    virtual ~NonLinearTVENonLinearTVPDG3DMaterialLaw(){}
    // set the time of _j2law
    virtual void setTime(const double t,const double dtime){
      dG3DMaterialLaw::setTime(t,dtime);
      _viscoLaw.setTime(t,dtime);
    }
    virtual materialLaw::matname getType() const {return _viscoLaw.getType();}
    virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
    virtual double soundSpeed() const{return _viscoLaw.soundSpeed();} // or change value ??
    virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent=false);
    virtual const double bulkModulus() const {return _viscoLaw.bulkModulus();}
    virtual double scaleFactor() const{return _viscoLaw.scaleFactor();}

    virtual int getNumberOfExtraDofsDiffusion() const {return 1;}
    virtual double getExtraDofStoredEnergyPerUnitField(double T) const;
    virtual double getInitialExtraDofStoredEnergyPerUnitField() const;

    virtual materialLaw* clone() const{return new NonLinearTVENonLinearTVPDG3DMaterialLaw(*this);};
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
    virtual bool withEnergyDissipation() const {return _viscoLaw.withEnergyDissipation();};
    virtual void setMacroSolver(const nonLinearMechSolver* sv){
      dG3DMaterialLaw::setMacroSolver(sv);
      _viscoLaw.setMacroSolver(sv);
    };

    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
      return &_viscoLaw;
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
      return &_viscoLaw;
    }
    #endif //SWIG
};

// NonLinearTVENonLinearTVP Law Interface =================================================================== END
#endif
