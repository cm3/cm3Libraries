//
//
// Description: pathfollowing terms
//
// Author:  <Van Dung NGUYEN>, (C) 2016
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef PATHFOLLOWINGTERMS_H_
#define PATHFOLLOWINGTERMS_H_

#include "dG3DTerms.h"
#include "ipField.h"
#include "dG3DCohesiveBandIPVariable.h"

class dG3DDissipationPathFollowingBulkScalarTerm : public ScalarTermBase<double>{
	protected:
		const IPField* _ipf;
		const partDomain* _dom;
			
	public:
		dG3DDissipationPathFollowingBulkScalarTerm(const partDomain* dom, const IPField* ip):_dom(dom),_ipf(ip){}
		virtual ~dG3DDissipationPathFollowingBulkScalarTerm(){}
		virtual void get(MElement *ele, int npts, IntPt *GP, double &val) const;
		virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<double> &vval) const {
			Msg::Error("dG3DDissipationPathFollowingBulkScalarTerm::get(MElement *ele, int npts, IntPt *GP, std::vector<T2> &vval) is not defined");
		};
		virtual ScalarTermBase<double>* clone () const {return new dG3DDissipationPathFollowingBulkScalarTerm(_dom,_ipf);};
};

class dG3DDissipationPathFollowingBulkLinearTerm : public  LinearTermBase<double>{
	protected:
		const IPField* _ipf;
		const partDomain* _dom;
		
	public:
		dG3DDissipationPathFollowingBulkLinearTerm(const partDomain* dom,const IPField* ip): LinearTermBase<double>(),_dom(dom),_ipf(ip){};
		virtual ~dG3DDissipationPathFollowingBulkLinearTerm(){}
		
		virtual void get(MElement *ele,int npts,IntPt *GP,fullVector<double> &m) const;
		virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullVector<double> > &mv) const
		{
			Msg::Error("Define me get by integration point dG3DDissipationPathFollowingBulkLinearTerm");
		}
		
		virtual LinearTermBase<double>* clone () const
		{
			return new dG3DDissipationPathFollowingBulkLinearTerm(_dom,_ipf);
		}
};


class dG3DDissipationPathFollowingBoundScalarTerm : public ScalarTermBase<double>{
	protected:
		const IPField* _ipf;
		const dgPartDomain* _dom;
	
	public:
		dG3DDissipationPathFollowingBoundScalarTerm(const dgPartDomain* dom, const IPField* ip):_ipf(ip),_dom(dom){}
		virtual ~dG3DDissipationPathFollowingBoundScalarTerm(){}
		virtual void get(MElement *ele, int npts, IntPt *GP, double &val) const;
		virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<double> &vval) const {
			Msg::Error("dG3DDissipationPathFollowingBoundScalarTerm::get(MElement *ele, int npts, IntPt *GP, std::vector<T2> &vval) is not defined");
		};
		virtual ScalarTermBase<double>* clone () const {return new dG3DDissipationPathFollowingBoundScalarTerm(_dom,_ipf);};
};

class dG3DDissipationPathFollowingBoundLinearTerm : public  LinearTermBase<double>{
	protected:
		const IPField* _ipf;
		const dgPartDomain* _dom;
		
	public:
		dG3DDissipationPathFollowingBoundLinearTerm(const dgPartDomain* dom,const IPField* ip):_ipf(ip),_dom(dom){};
		virtual ~dG3DDissipationPathFollowingBoundLinearTerm(){}
		
		virtual void get(MElement *ele,int npts,IntPt *GP,fullVector<double> &m) const;
		virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullVector<double> > &mv) const
		{
			Msg::Error("Define me get by integration point dG3DDissipationPathFollowingBoundLinearTerm");
		}
		
		virtual LinearTermBase<double>* clone () const
		{
			return new dG3DDissipationPathFollowingBoundLinearTerm(_dom,_ipf);
		}
};
#endif // PATHFOLLOWINGTERMS_H_
