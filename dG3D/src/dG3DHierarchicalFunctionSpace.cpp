//
// C++ Interface: Function Space
//
// Description: Hierarchical function space
//
//
// Author:  <V-D Nguyen>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "dG3DHierarchicalFunctionSpace.h"
#include "dG3DDof3IntType.h"
#include "MPoint.h"

bool g3DHierarchicalFunctionSpace::initMap = false;
std::map<MEdge, int, MEdgeLessThan> g3DHierarchicalFunctionSpace::allEdges;
std::map<MFace, int, MFaceLessThan> g3DHierarchicalFunctionSpace::allFaces;
void g3DHierarchicalFunctionSpace::numerateEdgeAndFace()
{
  if (!initMap)
  {
    initMap = true;
    GModel* pModel = GModel::current();
    int dim = pModel->getNumRegions() ? 3 : 2;
    std::map<int, std::vector<GEntity*> > groups[4];
    pModel->getPhysicalGroups(groups);
    std::map<int, std::vector<GEntity*> > &entmap = groups[dim];

    elementGroup g;
    for (std::map<int, std::vector<GEntity*> >::iterator it = entmap.begin(); it!= entmap.end(); it++){
      std::vector<GEntity*> &ent = it->second;
      for (unsigned int i = 0; i < ent.size(); i++){
        for (unsigned int j = 0; j < ent[i]->getNumMeshElements(); j++){
          MElement *e = ent[i]->getMeshElement(j);
          g.insert(e);
        }
      }
    }
    for (elementGroup::elementContainer::const_iterator it = g.begin(); it != g.end(); it++)
    {
      MElement* ele = it->second;
      for (int i=0; i< ele->getNumEdges(); i++)
      {
        MEdge ide= ele->getEdge(i);
        if (allEdges.find(ide) == allEdges.end())
        {
          int totalE = allEdges.size();
          allEdges[ide] = totalE;
        };
      }
      for (int i=0; i< ele->getNumFaces(); i++)
      {
        MFace ifa= ele->getFace(i);
        if (allFaces.find(ifa) == allFaces.end())
        {
          int totalF = allFaces.size();
          allFaces[ifa] = totalF;
        };
      }
    }
    //Msg::Error("rank %d numer of edges =%d",Msg::GetCommRank(),allEdges.size());
    //Msg::Error("rank %d numer of faces =%d",Msg::GetCommRank(),allFaces.size());
  }
};

g3DHierarchicalFunctionSpace::g3DHierarchicalFunctionSpace(int id, int order, int ncomp): HierarchicalFunctionSpace(id,order,ncomp)
{
  comp.resize(ncomp);
  for (int i=0; i< ncomp; i++)
  {
    comp[i] = i;
  }
}

g3DHierarchicalFunctionSpace::g3DHierarchicalFunctionSpace(int id, int order, int ncomp, int comp1):HierarchicalFunctionSpace(id,order,1)
{
  comp.resize(1);
  comp[0] = comp1;
};

void g3DHierarchicalFunctionSpace::getKeysOnElement_PerProc(MElement *ele, std::vector<Dof> &keys, int p, int ghost) const
{
  if (ele->getPolynomialOrder() >1)
  {
    Msg::Error("Linear geometric element must be considered in the hierarchical function space !!!!");
    Msg::Exit(0);
  };
  
  numerateEdgeAndFace();
  
  auto getInfos = [](MElement* ele, int order, std::vector<int>& nbFunctionPerDim)
  {
    HierarchicalBasis* basis = HierarchicalFunctionSpace::getHierarchicalBasis(order,ele);
    const int vSize = basis->getnVertexFunction();
    const int eSize = basis->getnEdgeFunction();
    const int fTriSize = basis->getnTriFaceFunction();
    const int fQuadSize = basis->getnQuadFaceFunction();
    const int bSize = basis->getnBubbleFunction();  

    nbFunctionPerDim[4] = bSize;
    if (ele->getDim() >=0)
    {
      nbFunctionPerDim[0] = 1;
    }
    if (ele->getDim() >= 1)
    {
      nbFunctionPerDim[1] = eSize/ele->getNumEdges();
    }
    if (ele->getDim() >= 2)
    {
      int nf = ele->getNumFaces();
      int nTri = 0;
      int nQuad = 0;
      for (int i=0; i< nf; i++)
      {
        MFace fa = ele->getFace(i);
        if (fa.getNumVertices() == 3)
        {
          nTri++;
        }
        else if (fa.getNumVertices() == 4)
        {
          nQuad++;
        }
        else
        {
          Msg::Error("missin cases !!!");
        }
      }
      if (nTri >0) nbFunctionPerDim[2] = fTriSize/nTri;
      if (nQuad>0) nbFunctionPerDim[3] = fQuadSize/nQuad;
    }
  };
  
  // all keys  
  static std::map<std::pair<int,int>,std::vector<int> > nbFunctionPerDimMap;
  std::vector<std::vector<int> > nbFunctionPerDim(comp.size(), std::vector<int>(5,0));
  for (int j=0; j< comp.size(); j++)
  {
    int orderComp = getOrderComp(comp[j]);
    if (nbFunctionPerDimMap.find(std::pair<int,int>(ele->getNum(),orderComp)) != nbFunctionPerDimMap.end())
    {
      nbFunctionPerDim[j] = nbFunctionPerDimMap[std::pair<int,int>(ele->getNum(),orderComp)];
    }
    else
    {
      getInfos(ele,orderComp,nbFunctionPerDim[j]);
    };
  }
  
  int nv = ele->getNumVertices();
  int ne = ele->getNumEdges();
  int nf = ele->getNumFaces();
  
  // create Dof
  for (int j=0;j<_ncomp;++j)
  {
    // vertices
    for (int k=0; k< nbFunctionPerDim[j][0]; k++)
    {
      for (int i=0; i< nv; i++)
      {
        #if defined(HAVE_CG_MPI_INTERFACE)
        keys.push_back(Dof(ele->getVertex(i)->getNum(),ghost*dG3DDof4IntType::createTypeWithFourInts(comp[j],ifield[j],p,0)));
        #else
        keys.push_back(Dof(ele->getVertex(i)->getNum(),dG3DDof4IntType::createTypeWithFourInts(comp[j],ifield[j],0,0)));
        #endif
      }            
    }
    // edges
    for (int k=0; k< nbFunctionPerDim[j][1]; k++)
    {
      for (int i=0; i< ne; i++)
      {
        MEdge ide= ele->getEdge(i);
        if (allEdges.find(ide) == allEdges.end())
        {
          Msg::Error("edge %d in element %d has not been numerated in %s",i,ele->getNum(),getFunctionSpaceName().c_str());
        };
        int entity = allEdges[ide];
        #if defined(HAVE_CG_MPI_INTERFACE)
        keys.push_back(Dof(entity,ghost*dG3DDof4IntType::createTypeWithFourInts(comp[j],ifield[j],p,1+k)));
        #else
        keys.push_back(Dof(entity,dG3DDof4IntType::createTypeWithFourInts(comp[j],ifield[j],0,1+k)));
        #endif 
        
      }            
    }
    // trifaces
    for (int k=0; k< nbFunctionPerDim[j][2]; k++)
    {
      for (int i=0; i< nf; i++)
      {
        MFace ifa= ele->getFace(i);
        if (ifa.getNumVertices() ==3)
        {
          if (allFaces.find(ifa) == allFaces.end())
          {
            Msg::Error("face %d in element %d has not been numerated in %s",i,ele->getNum(),getFunctionSpaceName().c_str());
          };
          int entity = allFaces[ifa];
          #if defined(HAVE_CG_MPI_INTERFACE)
          keys.push_back(Dof(entity,ghost*dG3DDof4IntType::createTypeWithFourInts(comp[j],ifield[j],p,11+k)));
          #else
          keys.push_back(Dof(entity,dG3DDof4IntType::createTypeWithFourInts(comp[j],ifield[j],0,11+k)));
          #endif 
        }
      }            
    }
    // quadfaces
    for (int k=0; k< nbFunctionPerDim[j][3]; k++)
    {
      for (int i=0; i< nf; i++)
      {
        MFace ifa= ele->getFace(i);
        if (ifa.getNumVertices() == 4)
        {
          if (allFaces.find(ifa) == allFaces.end())
          {
            Msg::Error("face %d in element %d has not been numerated in %s",i,ele->getNum(),getFunctionSpaceName().c_str());
          };
          int entity = allFaces[ifa];
          #if defined(HAVE_CG_MPI_INTERFACE)
          keys.push_back(Dof(entity,ghost*dG3DDof4IntType::createTypeWithFourInts(comp[j],ifield[j],p,21+k)));
          #else
          keys.push_back(Dof(entity,dG3DDof4IntType::createTypeWithFourInts(comp[j],ifield[j],0,21+k)));
          #endif 
        }
      }            
    }
    // volumes
    for (int k=0; k< nbFunctionPerDim[j][4]; k++)
    {
      #if defined(HAVE_CG_MPI_INTERFACE)
      keys.push_back(Dof(ele->getNum(),ghost*dG3DDof4IntType::createTypeWithFourInts(comp[j],ifield[j],p,31+k)));
      #else
      keys.push_back(Dof(ele->getNum(),dG3DDof4IntType::createTypeWithFourInts(comp[j],ifield[j],0,31+k)));
      #endif
    }
  }
};

void g3DHierarchicalFunctionSpace::getKeysOnElement(MElement *ele, std::vector<Dof> &keys) const
{
  // negative type in mpi if the Dof are not located on this partition
  #if defined(HAVE_MPI) // small duplication to avoid multiple if in a loop
  if( (ele->getPartition() != 0) and (ele->getPartition() != Msg::GetCommRank() +1))
  {
    getKeysOnElement_PerProc(ele,keys,ele->getPartition(),-1);
  }
  else
  #endif // HAVE_MPI
  {
    getKeysOnElement_PerProc(ele,keys,ele->getPartition(),1);
  }
  
}

void g3DHierarchicalFunctionSpace::getShapeFunctions(int c, MElement *ele, double u, double v, double w,  std::vector<double>& vals) const
{
  int nbFF = getNumShapeFunctions(ele,c);
  vals.resize(nbFF);
  // get field inxed
  int orderComp = getOrderComp(c);
  HierarchicalBasis* basis = HierarchicalFunctionSpace::getHierarchicalBasis(orderComp,ele);
  const int vSize = basis->getnVertexFunction();
  const int eSize = basis->getnEdgeFunction();
  const int fTriSize = basis->getnTriFaceFunction();
  const int fQuadSize = basis->getnQuadFaceFunction();
  const int bSize = basis->getnBubbleFunction();  
  
  std::vector<double> vertexBasis(vSize, 0);
  std::vector<double> edgeBasis(eSize, 0);
  std::vector<double> faceBasis(fTriSize+fQuadSize, 0);
  std::vector<double> bubbleBasis(bSize, 0);      
  basis->generateBasis(u,v,w,vertexBasis,edgeBasis,faceBasis,bubbleBasis);
  
  // orient egde
  std::vector<double> edgeBasisOriented(eSize, 0);
  if (eSize > 0)
  {
    std::vector<double> edgeBasisNedgative(edgeBasis);
    basis->orientEdgeFunctionsForNegativeFlag(edgeBasisNedgative);
    
    for(int j = 0; j < ele->getNumEdges(); ++j) 
    {
      MEdge edge = ele->getEdge(j);
      MEdge edgeSolin = ele->getEdgeSolin(j);
      int orientationFlag =   (edge.getMinVertex() != edgeSolin.getVertex(0) ? -1 : 1);
      basis->orientEdge(orientationFlag, j, edgeBasisOriented, edgeBasis, edgeBasisNedgative);
    }
  }
  // oriented faces
  std::vector<double> faceBasisOriented(fTriSize+fQuadSize, 0);
  if (fTriSize+fQuadSize > 0)
  {
    std::vector<double> quadFaceBasisAllOrientation(8*fQuadSize, 0);
    std::vector<double> triFaceBasisAllOrientation(6*fTriSize, 0);
    basis->addAllOrientedFaceFunctions(u,v,w,faceBasis,quadFaceBasisAllOrientation,triFaceBasisAllOrientation);
    for (int facenumber=0; facenumber<  ele->getNumFaces(); facenumber++)
    {
      MFace iface = ele->getFace(facenumber);
      std::vector<int> allflag(3,0);
      iface.getOrientationFlagForFace(allflag);
      basis->orientFace(allflag[0],allflag[1],allflag[2],facenumber,quadFaceBasisAllOrientation,triFaceBasisAllOrientation,faceBasisOriented);
    };
  };
  
  // vertex basis
  for (int i=0; i< vSize; i++)
  {
    vals[i] = vertexBasis[i];
  }
  for (int i=0; i< eSize; i++)
  {
    vals[vSize+i] = edgeBasisOriented[i];
  }
  for (int i=0; i< fTriSize+fQuadSize; i++)
  {
    vals[vSize+eSize+i] = faceBasisOriented[i];
  }
  for (int i=0; i< bSize; i++)
  {
    vals[vSize+eSize+fTriSize+fQuadSize+i] = bubbleBasis[i];
  }
};
void g3DHierarchicalFunctionSpace::getGradShapeFunctions(int c, MElement *ele, double u, double v, double w,  std::vector<std::vector<double> >& grads) const
{
  int nbFF = getNumShapeFunctions(ele,c);
  grads.resize(nbFF,std::vector<double>(3,0));
  // get field inxed
  int orderComp = getOrderComp(c);
  HierarchicalBasis* basis = HierarchicalFunctionSpace::getHierarchicalBasis(orderComp,ele);
  const int vSize = basis->getnVertexFunction();
  const int eSize = basis->getnEdgeFunction();
  const int fTriSize = basis->getnTriFaceFunction();
  const int fQuadSize = basis->getnQuadFaceFunction();
  const int bSize = basis->getnBubbleFunction();  
  
  std::vector<std::vector<double> > vertexBasis(vSize, std::vector<double>(3,0));
  std::vector<std::vector<double> > edgeBasis(eSize, std::vector<double>(3,0));
  std::vector<std::vector<double> > faceBasis(fTriSize+fQuadSize, std::vector<double>(3,0));
  std::vector<std::vector<double> > bubbleBasis(bSize, std::vector<double>(3,0));      
  basis->generateBasis(u,v,w,vertexBasis,edgeBasis,faceBasis,bubbleBasis,"GradH1Legendre");
  
  // orient egde
  std::vector<std::vector<double> > edgeBasisOriented(eSize, std::vector<double>(3,0));
  if (eSize > 0)
  {
    std::vector<std::vector<double> > edgeBasisNedgative(edgeBasis);
    basis->orientEdgeFunctionsForNegativeFlag(edgeBasisNedgative);
    
    for(int j = 0; j < ele->getNumEdges(); ++j) 
    {
      MEdge edge = ele->getEdge(j);
      MEdge edgeSolin = ele->getEdgeSolin(j);

      int orientationFlag = (edge.getMinVertex() != edgeSolin.getVertex(0) ? -1 : 1);
      basis->orientEdge(orientationFlag, j, edgeBasisOriented, edgeBasis, edgeBasisNedgative);
    }
  }
  // oriented faces
  std::vector<std::vector<double> > faceBasisOriented(fTriSize+fQuadSize, std::vector<double>(3,0));
  if (fTriSize+fQuadSize > 0)
  {
    std::vector<std::vector<double>> quadFaceBasisAllOrientation(8*fQuadSize, std::vector<double>(3,0));
    std::vector<std::vector<double>> triFaceBasisAllOrientation(6*fTriSize, std::vector<double>(3,0));
    basis->addAllOrientedFaceFunctions(u,v,w,faceBasis,quadFaceBasisAllOrientation,triFaceBasisAllOrientation,"GradH1Legendre");
    for (int facenumber=0; facenumber<  ele->getNumFaces(); facenumber++)
    {
      MFace iface = ele->getFace(facenumber);
      std::vector<int> allflag(3,0);
      iface.getOrientationFlagForFace(allflag);
      basis->orientFace(allflag[0],allflag[1],allflag[2],facenumber,quadFaceBasisAllOrientation,triFaceBasisAllOrientation,faceBasisOriented);
    };
  };
  
  // vertex basis
  for (int i=0; i< vSize; i++)
  {
    grads[i] = vertexBasis[i];
  }
  for (int i=0; i< eSize; i++)
  {
    grads[vSize+i] = edgeBasisOriented[i];
  }
  for (int i=0; i< fTriSize+fQuadSize; i++)
  {
    grads[vSize+eSize+i] = faceBasisOriented[i];
  }
  for (int i=0; i< bSize; i++)
  {
    grads[vSize+eSize+fTriSize+fQuadSize+i] = bubbleBasis[i];
  };
};


void g3DDirichletBoundaryConditionHierarchicalFunctionSpace::getKeysOnElement(MElement *ele, std::vector<Dof> &keys) const
{
  #if defined(HAVE_MPI) // small duplication to avoid multiple if in a loop
  if( (ele->getPartition() != 0) and (ele->getPartition() != Msg::GetCommRank() +1))
  {
    if(ele->getPartition()!=0)
    { 
      getKeysOnElement_PerProc(ele,keys,ele->getPartition(),-1);
    }
    else
    { 
      for(int p=0;p<=Msg::GetCommSize();p++)
      {
        getKeysOnElement_PerProc(ele,keys,p,-1);
      }
    }
  }
  else 
  #endif // HAVE_MPI
  {
    getKeysOnElement_PerProc(ele,keys,ele->getPartition(),1);
  }
};

void g3DNeumannBoundaryConditionHierarchicalFunctionSpace::getKeysOnElement(MElement *ele, std::vector<Dof> &keys) const
{
  int p = (_parallelMesh) ? Msg::GetCommRank()+1 : 0;
  getKeysOnElement_PerProc(ele,keys,p,1);
};


dG3DHierarchicalFunctionSpace::dG3DHierarchicalFunctionSpace(int id, int order, int ncomp): HierarchicalFunctionSpace(id,order,ncomp)
{
  comp.resize(ncomp);
  for (int i=0; i< ncomp; i++)
  {
    comp[i] = i;
  }
}

dG3DHierarchicalFunctionSpace::dG3DHierarchicalFunctionSpace(int id, int order, int ncomp, int comp1):HierarchicalFunctionSpace(id,order,1)
{
  comp.resize(1);
  comp[0] = comp1;
};


void dG3DHierarchicalFunctionSpace::getKeysOnElement(MElement *ele, std::vector<Dof> &keys) const
{
  if (ele->getPolynomialOrder() >1)
  {
    Msg::Error("Linear geometric element must be considered in the hierarchical function space !!!!");
    Msg::Exit(0);
  };
  
  #if defined(HAVE_MPI) // small duplication to avoid multiple if in a loop
  if( (ele->getPartition() != 0)and (ele->getPartition() != Msg::GetCommRank() +1))
  {
    for (int j=0;j<_ncomp;++j)
    {
      int orderComp = getOrderComp(comp[j]);
      HierarchicalBasis* basis = HierarchicalFunctionSpace::getHierarchicalBasis(orderComp,ele);
      const int vSize = basis->getnVertexFunction();
      const int eSize = basis->getnEdgeFunction();
      const int fSize = basis->getnTriFaceFunction()+basis->getnQuadFaceFunction();
      const int bSize = basis->getnBubbleFunction();  
      int nk = vSize+eSize+fSize+bSize;
      for (int i=0;i<nk;i++)
        keys.push_back(Dof(ele->getNum(),-dG3DDof3IntType::createTypeWithThreeInts(comp[j],ifield[j],i)));
    }
  }
  else
  #endif // HAVE_MPI
  {
    for (int j=0;j<_ncomp;++j)
    {
      int orderComp = getOrderComp(comp[j]);
      HierarchicalBasis* basis = HierarchicalFunctionSpace::getHierarchicalBasis(orderComp,ele);
      const int vSize = basis->getnVertexFunction();
      const int eSize = basis->getnEdgeFunction();
      const int fSize = basis->getnTriFaceFunction()+basis->getnQuadFaceFunction();
      const int bSize = basis->getnBubbleFunction();  
      int nk = vSize+eSize+fSize+bSize;
      for (int i=0;i<nk;i++)
        keys.push_back(Dof(ele->getNum(),dG3DDof3IntType::createTypeWithThreeInts(comp[j],ifield[j],i)));
        
    }
  }
};

void dG3DHierarchicalFunctionSpace::getKeysOnLowerDimensionPart(MElement *ebulk, MElement* lowEle, const std::vector<int> &vercomp, std::vector<Dof> &keys) const
{
  dG3DBoundaryConditionHierarchicalFunctionSpace::mapInfos info;
  dG3DBoundaryConditionHierarchicalFunctionSpace::getLocalLocation(lowEle,ebulk,info);
    
  #if defined(HAVE_MPI) // small duplication to avoid multiple if in a loop
  if( (ebulk->getPartition() != 0)and (ebulk->getPartition() != Msg::GetCommRank() +1))
  {
    
    for (int j=0;j<vercomp.size();++j)
    {
      int orderComp = getOrderComp(vercomp[j]);
      int idComp = getIdComp(vercomp[j]);
      HierarchicalBasis* basis = HierarchicalFunctionSpace::getHierarchicalBasis(orderComp,ebulk);
      const int vSize = basis->getnVertexFunction();
      const int eSize = basis->getnEdgeFunction();
      const int fSize = basis->getnTriFaceFunction()+basis->getnQuadFaceFunction();

      int nv = ebulk->getNumVertices();
      int ne = ebulk->getNumEdges();
      int nf = ebulk->getNumFaces();
      
      int nbFunctionPerNode = 0;
      int nbFunctionPerEdge = 0;
      int nbFunctionPerFace = 0;
      
      if (nv > 0)  nbFunctionPerNode = vSize/nv;
      if (ne >0) nbFunctionPerEdge = eSize/ne;
      if (nf>0) nbFunctionPerFace = fSize/nf;
      
      
      std::vector<int> indexes;
      if (nbFunctionPerNode > 0)
      {
        for (int i=0; i< info.vertices.size(); i++)
        {
          indexes.push_back(info.vertices[i]);
        }            
      }
      for (int k=0; k< nbFunctionPerEdge; k++)
      {
        for (int i=0; i< info.edges.size(); i++)
        {
          indexes.push_back(vSize+info.edges[i]*nbFunctionPerEdge+k);
        }            
      }
      for (int k=0; k< nbFunctionPerFace; k++)
      {
        for (int i=0; i< info.faces.size(); i++)
        {
          indexes.push_back(vSize+eSize+info.faces[i]*nbFunctionPerFace+k); 
        }            
      }
      for (int i=0;i<indexes.size();i++)
        keys.push_back(Dof(ebulk->getNum(),-dG3DDof3IntType::createTypeWithThreeInts(vercomp[j],idComp,indexes[i])));
        
    }
  }
  else
  #endif // HAVE_MPI
  {
    for (int j=0;j<vercomp.size();++j)
    {
      int orderComp = getOrderComp(vercomp[j]);
      int idComp = getIdComp(vercomp[j]);
      HierarchicalBasis* basis = HierarchicalFunctionSpace::getHierarchicalBasis(orderComp,ebulk);
      const int vSize = basis->getnVertexFunction();
      const int eSize = basis->getnEdgeFunction();
      const int fSize = basis->getnTriFaceFunction()+basis->getnQuadFaceFunction();
      
      int nv = ebulk->getNumVertices();
      int ne = ebulk->getNumEdges();
      int nf = ebulk->getNumFaces();
      
      int nbFunctionPerNode = 0;
      int nbFunctionPerEdge = 0;
      int nbFunctionPerFace = 0;
      
      if (nv > 0)  nbFunctionPerNode = vSize/nv;
      if (ne >0) nbFunctionPerEdge = eSize/ne;
      if (nf>0) nbFunctionPerFace = fSize/nf;
      
      
      std::vector<int> indexes;
      if (nbFunctionPerNode > 0)
      {
        for (int i=0; i< info.vertices.size(); i++)
        {
          indexes.push_back(info.vertices[i]);
        }            
      }
      for (int k=0; k< nbFunctionPerEdge; k++)
      {
        for (int i=0; i< info.edges.size(); i++)
        {
          indexes.push_back(vSize+info.edges[i]*nbFunctionPerEdge+k);
        }            
      }
      for (int k=0; k< nbFunctionPerFace; k++)
      {
        for (int i=0; i< info.faces.size(); i++)
        {
          indexes.push_back(vSize+eSize+info.faces[i]*nbFunctionPerFace+k); 
        }            
      }
      for (int i=0;i<indexes.size();i++)
        keys.push_back(Dof(ebulk->getNum(),dG3DDof3IntType::createTypeWithThreeInts(vercomp[j],idComp,indexes[i])));
    }
  }
  
};

void dG3DHierarchicalFunctionSpace::getShapeFunctions(int c, MElement *ele, double u, double v, double w,  std::vector<double>& vals) const
{
  int nbFF = getNumShapeFunctions(ele,c);
  vals.resize(nbFF);
  // get field inxed
  int orderComp = getOrderComp(c);
  HierarchicalBasis* basis = HierarchicalFunctionSpace::getHierarchicalBasis(orderComp,ele);
  const int vSize = basis->getnVertexFunction();
  const int eSize = basis->getnEdgeFunction();
  const int fSize = basis->getnTriFaceFunction()+basis->getnQuadFaceFunction();
  const int bSize = basis->getnBubbleFunction();  
  
  std::vector<double> vertexBasis(vSize, 0);
  std::vector<double> edgeBasis(eSize, 0);
  std::vector<double> faceBasis(fSize, 0);
  std::vector<double> bubbleBasis(bSize, 0);      
  basis->generateBasis(u,v,w,vertexBasis,edgeBasis,faceBasis,bubbleBasis);
  
  // vertex basis
  for (int i=0; i< vSize; i++)
  {
    vals[i] = vertexBasis[i];
  }
  for (int i=0; i< eSize; i++)
  {
    vals[vSize+i] = edgeBasis[i];
  }
  for (int i=0; i< fSize; i++)
  {
    vals[vSize+eSize+i] = faceBasis[i];
  }
  for (int i=0; i< bSize; i++)
  {
    vals[vSize+eSize+fSize+i] = bubbleBasis[i];
  }
};
void dG3DHierarchicalFunctionSpace::getGradShapeFunctions(int c, MElement *ele, double u, double v, double w,  std::vector<std::vector<double> >& grads) const
{
  int nbFF = getNumShapeFunctions(ele,c);
  grads.resize(nbFF,std::vector<double>(3,0));
  // get field inxed
  int orderComp = getOrderComp(c);
  HierarchicalBasis* basis = HierarchicalFunctionSpace::getHierarchicalBasis(orderComp,ele);
  const int vSize = basis->getnVertexFunction();
  const int eSize = basis->getnEdgeFunction();
  const int fSize = basis->getnTriFaceFunction() + basis->getnQuadFaceFunction();
  const int bSize = basis->getnBubbleFunction();  
  
  std::vector<std::vector<double> > vertexBasis(vSize, std::vector<double>(3,0));
  std::vector<std::vector<double>> edgeBasis(eSize, std::vector<double>(3,0));
  std::vector<std::vector<double>> faceBasis(fSize, std::vector<double>(3,0));
  std::vector<std::vector<double>> bubbleBasis(bSize, std::vector<double>(3,0));      
  basis->generateBasis(u,v,w,vertexBasis,edgeBasis,faceBasis,bubbleBasis,"GradH1Legendre");
  
  // vertex basis
  for (int i=0; i< vSize; i++)
  {
    grads[i] = vertexBasis[i];
  }
  for (int i=0; i< eSize; i++)
  {
    grads[vSize+i] = edgeBasis[i];
  }
  for (int i=0; i< fSize; i++)
  {
    grads[vSize+eSize+i] = faceBasis[i];
  }
  for (int i=0; i< bSize; i++)
  {
    grads[vSize+eSize+fSize+i] = bubbleBasis[i];
  };
};

void dG3DBoundaryConditionHierarchicalFunctionSpace::getKeys(MElement *e, std::vector<Dof> &keys) const
{
  std::map<MElement*,std::map<MElement*,mapInfos> >::const_iterator it = mapBoundaryInterfaces.find(e);
  if (it == mapBoundaryInterfaces.end())
  {
    Msg::Error("mapBoundaryInterfaces is wrong initialized in dG3DBoundaryConditionLagrangeFunctionSpace::getKeys");
  }
  else
  {
    const std::map<MElement*,mapInfos >& support = it->second;
    for (std::map<MElement*,mapInfos >::const_iterator itm = support.begin(); itm != support.end(); itm++)
    {
      MElement* ebulk = itm->first;
      const mapInfos& info = itm->second;
      dG3DHierarchicalFunctionSpace::getKeysOnLowerDimensionPart(ebulk,e,comp,keys);
    }
  }
};

void dG3DBoundaryConditionHierarchicalFunctionSpace::getKeysOnVertex(MElement *ele, MVertex *v, const std::vector<int> &vercomp, std::vector<Dof> &keys) const
{
  std::map<MElement*,std::map<MElement*,mapInfos> >::const_iterator it = mapBoundaryInterfaces.find(ele);
  if (it == mapBoundaryInterfaces.end())
  {
    Msg::Error("mapBoundaryInterfaces is wrong initialized in dG3DBoundaryConditionLagrangeFunctionSpace::getKeys");
  }
  else
  {
    MPoint pt(v,0,0);
    const std::map<MElement*,mapInfos >& support = it->second;
    for (std::map<MElement*,mapInfos >::const_iterator itm = support.begin(); itm != support.end(); itm++)
    {
      MElement* ebulk = itm->first;
      const mapInfos& info = itm->second;
      dG3DHierarchicalFunctionSpace::getKeysOnLowerDimensionPart(ebulk,&pt,comp,keys);
    }
  }
};
