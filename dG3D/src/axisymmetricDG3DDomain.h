//
// C++ Interface: axisymmetric
//
// Description: Interface class for dg 3D for axisymmetric problems
//
// Author:  <V.-D. Nguyen>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef AXISYMMETRICDG3DDOMAIN_H_
#define AXISYMMETRICDG3DDOMAIN_H_

/* 
 * local and nonlocal CG domain
 * geometry must be constructed in x-z coordinates, x --> r, z--> z, y is not used
 * y-displacement must be contrained via displacementBC
 */

#include "dG3DDomain.h"

class axisymmetricDG3DDomain : public dG3DDomain{
  public:
    axisymmetricDG3DDomain(const int tag, const int phys, const int sp_, const int lnum, const int fdg, const int dim, int numNL=0, int nonConstitutiveExtraDOFVar=0);
    #ifndef SWIG
    axisymmetricDG3DDomain(const axisymmetricDG3DDomain& src);
    virtual ~axisymmetricDG3DDomain(){};
    
    virtual void computeStrain(MElement *e,  const int npts, IntPt *GP,
                             AllIPState::ipstateElementContainer *vips,
                             IPStateBase::whichState ws, fullVector<double> &disp, bool useBarF) const;
    virtual void computeStrain(AllIPState *aips,MInterfaceElement *ie, IntPt *GP,const IPStateBase::whichState ws,
                                        partDomain* efMinus, partDomain *efPlus, fullVector<double> &dispm,
                                        fullVector<double> &dispp, const bool virt, bool useBarF);
    virtual void createTerms(unknownField *uf,IPField*ip);
    
    virtual LinearTermBase<double>* createNeumannTerm(FunctionSpace<double> *spneu,  const elementGroup* g,
                                                    const simpleFunctionTime<double>* f,const unknownField *uf,
                                                    const IPField * ip, const nonLinearBoundaryCondition::location onWhat,
                                                    const nonLinearNeumannBC::NeumannBCType neumann_type,
                                                    const int comp) const;
    virtual BilinearTermBase* createNeumannMatrixTerm(FunctionSpace<double> *spneu, const elementGroup* g,
                                                      const simpleFunctionTime<double>* f, const unknownField *uf,
                                                      const IPField * ip, 
                                                      const nonLinearBoundaryCondition::location onWhat,
                                                      const nonLinearNeumannBC::NeumannBCType neumann_type,
                                                      const int comp
                                                      ) const;
    
    virtual void allocateInterfaceMPI(const int rankOtherPart, const elementGroup &groupOtherPart,dgPartDomain** dgdom);
    virtual partDomain* clone() const {return new axisymmetricDG3DDomain(*this);};
    
    virtual double computeVolumeDomain(const IPField* ipf) const;
    
    virtual double computeDeformationEnergy(const IPField* ipf) const;
    virtual double computePlasticEnergy(const IPField* ipf) const;
    virtual double computeIrreversibleEnergy(const IPField* ipf) const;
    
    virtual double computeMeanValueDomain(const int num, const IPField* ipf, double& volume, const GPFilter* filter) const;
    virtual double averagingOnActiveDissipationZone(const int num, const IPField* ipf, double & volume, const IPStateBase::whichState ws = IPStateBase::current) const;
    #endif //SWIG
};


class axisymmetricInterDomainBetween3D : public interDomainBase, public axisymmetricDG3DDomain{
 public:
  axisymmetricInterDomainBetween3D(const int tag, partDomain *dom1, partDomain *dom2, const int lnum=0, const int bnum=0);
  
#ifndef SWIG
  axisymmetricInterDomainBetween3D(const axisymmetricInterDomainBetween3D &source);
  virtual ~axisymmetricInterDomainBetween3D(){}

  virtual void setMaterialLaw(const std::map<int,materialLaw*> &maplaw);
  // FOLLOWING FUNCTION SHOULD STAY HERE BECAUSE OF THE DOUBLE DERIVATION
  virtual materialLaw* getMaterialLaw(){return _mlaw;}
  virtual const materialLaw* getMaterialLaw() const{return _mlaw;}
  virtual materialLaw* getMaterialLawMinus(){return _mlawMinus;}
  virtual const materialLaw* getMaterialLawMinus() const{return _mlawMinus;}
  virtual materialLaw* getMaterialLawPlus(){return _mlawPlus;}
  virtual const materialLaw* getMaterialLawPlus() const{return _mlawPlus;}
  virtual int getLawNum() const{return _lnum;}
  // FOLLOWING FUNCTION SHOULD STAY HERE BECAUSE OF THE DOUBLE DERIVATION
  virtual int getMinusLawNum() const{return _domMinus->getLawNum();}
  virtual int getPlusLawNum() const{return _domPlus->getLawNum();}
  virtual const partDomain* getMinusDomain() const{return _domMinus;}
  virtual const partDomain* getPlusDomain() const{return _domPlus;}
  virtual partDomain* getMinusDomain(){return _domMinus;}
  virtual partDomain* getPlusDomain() {return _domPlus;}

  virtual void createTerms(unknownField *uf,IPField*ip);
  // as there is no elerment on a interDomain empty function. The interface element on an interDomain
  // has to be created via its two domains.
  virtual void createInterface(manageInterface &maninter){}
	
	virtual partDomain* clone() const {return new axisymmetricInterDomainBetween3D(*this);};
#endif
};


#endif //AXISYMMETRICDG3DDOMAIN_H_
