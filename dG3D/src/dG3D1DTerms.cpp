//
// C++ Interface: 1D terms
//
// Description: Class of terms for dg
//
//
// Author:  <V.D Nguyen>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "dG3D1DTerms.h"
#include "dG3D1DDomain.h"

void dG3D1DForceBulk::get(MElement *ele,int npts,IntPt *GP,fullVector<double> &vFor) const{
  // Initialization of some data
  const FunctionSpaceBase* space  = _dom->getFunctionSpace();
  int nbdof = space->getNumKeys(ele);
  int nbFF = ele->getNumShapeFunctions();
  vFor.resize(nbdof,true);
  
  const dG3D1DDomain* dom1D = dynamic_cast<const dG3D1DDomain*>(_dom);
  
  static SVector3 BT;
  static SPoint3 point;

  // get gauss' point data
  _ipf->getIPv(ele,vipv);
  _crossSection->setElement(ele);
  
  for (int i = 0; i < npts; i++)
  {
    double weight = GP[i].weight;
    double & detJ = vipv[i]->getJacobianDeterminant(ele,GP[i]);

    const STensor3 *PK1 = &(vipv[i]->getConstRefToFirstPiolaKirchhoffStress());
    
    double u = GP[i].pt[0]; double v = GP[i].pt[1];  double w = GP[i].pt[2];
    ele->pnt(u,v,w,point);
    double A = _crossSection->getVal(point[0]); // crossection is in fuction of x only
    
    double ratio = detJ*weight*A;

    const std::vector<TensorialTraits<double>::GradType> &Grads = vipv[i]->gradf(space,ele,GP[i]);
    for(int j=0;j<nbFF; j++)
    {
      BT(0) = Grads[j+0*nbFF][0];
      BT(1) = Grads[j+0*nbFF][1];
      BT(2) = Grads[j+0*nbFF][2];
      // x y z
      for(int kk=0;kk<3;kk++)
      {
        for(int m=0; m<3; m++)
        {
          vFor(j+kk*nbFF) += ratio*(BT(m)*PK1->operator()(kk,m));
        }
      }

    }
    if( vipv[i]->getNumberNonLocalVariable()>getNumNonLocalVariable() ) Msg::Error("Your material law use more non local variable than your domain");
    for (int nl = 0; nl< vipv[i]->getNumberNonLocalVariable(); nl++){
      int threeTimesNbFF = 3*nbFF;
      const STensor3 *cg = &vipv[i]->getConstRefToCharacteristicLengthMatrix(nl);
      const double & nonlocalVar = vipv[i]->getConstRefToNonLocalVariable(nl);
      const SVector3 & gradNonlocalVar = vipv[i]->getConstRefToGradNonLocalVariable()[nl];
      const double& localVar = vipv[i]->getConstRefToLocalVariable(nl);
      const std::vector<TensorialTraits<double>::ValType> &Vals = vipv[i]->f(space,ele,GP[i]);
      for(int j=0;j<nbFF; j++)
      {
        vFor(j+nl*nbFF+threeTimesNbFF) += getNonLocalEqRatio()*ratio*(Vals[j+nl*nbFF+threeTimesNbFF]*nonlocalVar);
        for(int m=0; m<3; m++)
        {
          for(int n=0; n<3; n++)
          {
            vFor(j+nl*nbFF+threeTimesNbFF) += getNonLocalEqRatio()*ratio*(Grads[j+nl*nbFF+threeTimesNbFF](m)*cg->operator()(m,n)*gradNonlocalVar(n));
          }
        }
        vFor(j+nl*nbFF+threeTimesNbFF) -= getNonLocalEqRatio()*ratio*(Vals[j+nl*nbFF+threeTimesNbFF]*localVar);
      }
    }
  }
};

void dG3D1DStiffnessBulk::get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &mStiff) const{
  const FunctionSpaceBase* space  = _dom->getFunctionSpace();
  int nbdof = space->getNumKeys(ele);
  int nbFF = ele->getNumShapeFunctions();
  mStiff.resize(nbdof, nbdof,true); // true --> setAll(0.)

  // get ipvariables
  _ipf->getIPv(ele,vipv);
  _crossSection->setElement(ele);

  // sum on Gauss' points
  static SVector3 B;
  static SVector3 BT;
  static SPoint3 point;
  
  for (int i = 0; i < npts; i++)
  {
    double weight = GP[i].weight;
    double& detJ = vipv[i]->getJacobianDeterminant(ele,GP[i]);
    
    double u = GP[i].pt[0]; double v = GP[i].pt[1];  double w = GP[i].pt[2];
    ele->pnt(u,v,w,point);
    double A = _crossSection->getVal(point[0]); // crossection is in fuction of x only
    
    double ratio = detJ*weight*A;
    // x, y, z
    const STensor43 *H =&vipv[i]->getConstRefToTangentModuli();
    const std::vector<TensorialTraits<double>::GradType> &Grads = vipv[i]->gradf(space,ele,GP[i]);
    const std::vector<TensorialTraits<double>::ValType> &Vals = vipv[i]->f(space,ele,GP[i]);
    
    for(int j=0;j<nbFF; j++)
    {
      BT(0) = Grads[j][0];
      BT(1) = Grads[j][1];
      BT(2) = Grads[j][2];
      for(int k=0;k<nbFF;k++){
        // x y z
        B(0) = Grads[k][0];
        B(1) = Grads[k][1];
        B(2) = Grads[k][2];

        for(int kk=0;kk<3;kk++)
        {
          for(int ll=0; ll<3; ll++)
          {
            for(int m=0; m<3; m++)
            {
              for(int n=0; n<3; n++)
              {
                mStiff(j+kk*nbFF,k+ll*nbFF) += ratio*(BT(m)*H->operator()(kk,m,ll,n)*B(n));
              }
            }
            
          }
        }
      }
    }
    const int threeTimesNbFF = 3*nbFF;
    if( vipv[i]->getNumberNonLocalVariable()>getNumNonLocalVariable() ) Msg::Error("Your material law use more non local variable than your domain");

    for (int nlk = 0; nlk < vipv[i]->getNumberNonLocalVariable(); nlk++){
      const STensor3  *dpds = &vipv[i]->getConstRefToDLocalVariableDStrain()[nlk];
      const STensor3  *dsdp = &vipv[i]->getConstRefToDStressDNonLocalVariable()[nlk];
      const STensor3    *cg = &vipv[i]->getConstRefToCharacteristicLengthMatrix(nlk);           // by Wu Ling
      double  SpBar = vipv[i]->getConstRefToDLocalVariableDNonLocalVariable()(nlk,nlk);

      for(int j=0;j<nbFF; j++){
        for(int k=0;k<nbFF;k++){
          // principal term
          double BtB = 0.;
          for(int m=0; m<3; m++){
            for(int n=0; n<3; n++){
              BtB += Grads[j+nlk*nbFF+threeTimesNbFF](m)*(cg->operator()(m,n))*Grads[k+nlk*nbFF+threeTimesNbFF](n);
            }
          }
          mStiff(j+nlk*nbFF+threeTimesNbFF,k+nlk*nbFF+threeTimesNbFF)
                  += getNonLocalEqRatio()*ratio*(Vals[j+nlk*nbFF+threeTimesNbFF]*Vals[k+nlk*nbFF+threeTimesNbFF]*(1-SpBar)+ BtB);

          // cross terms to check
          for(int kk=0;kk<3;kk++)
          {
            for(int m=0; m<3; m++)
            {
              mStiff(j+kk*nbFF,k+nlk*nbFF+threeTimesNbFF) += ratio*(Grads[j+kk*nbFF](m)*dsdp->operator()(kk,m)*Vals[k+nlk*nbFF+threeTimesNbFF]);
              mStiff(j+nlk*nbFF+threeTimesNbFF,k+kk*nbFF) -= getNonLocalEqRatio()*ratio*(Vals[j+nlk*nbFF+threeTimesNbFF]*dpds->operator()(kk,m)*Grads[k+kk*nbFF](m));
            }
          }
        }
      }
    }
  }
};
