//
// C++ Interface: fracture ipvariable
//
// Description: Class with definition of fracture cohesive ipvarible for dG3D
//
//
// Author:  <L. Noels>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef FRACTURECOHESIVEDG3DIPVARIABLE_H_
#define FRACTURECOHESIVEDG3DIPVARIABLE_H_

#include "dG3DIPVariable.h"
#include  "dG3DCohesiveIPVariable.h"

class FractureCohesive3DIPVariable : public IPVariable2ForFracture<dG3DIPVariableBase,Cohesive3DIPVariableBase>
{
 protected:
  double _facSigmac;
 public:
  FractureCohesive3DIPVariable(const double facSigmac=1.);
  FractureCohesive3DIPVariable(const FractureCohesive3DIPVariable &source);
  virtual FractureCohesive3DIPVariable& operator=(const IPVariable &source);
  virtual ~FractureCohesive3DIPVariable(){}
  virtual const double fractureStrengthFactor() const{return _facSigmac;}
  virtual void setFractureStrengthFactor(const double fsf){_facSigmac = fsf;}

  #if defined(HAVE_MPI)
  // but you can communicate values by filling these functions for your IPVariable
  virtual int numberValuesToCommunicateMPI()const
  {
    int nb=1; //the facSigmac
    nb+= _ipvbulk->numberValuesToCommunicateMPI();
    return nb;
  }
  virtual void fillValuesToCommunicateMPI(double *arrayMPI)const
  {
    arrayMPI[0]=_facSigmac;
    if(_ipvbulk->numberValuesToCommunicateMPI()>0)
      _ipvbulk->fillValuesToCommunicateMPI(&(arrayMPI[1]));
  }
  virtual void getValuesFromMPI(const double *arrayMPI){
    _facSigmac=arrayMPI[0];
    if(_ipvbulk->numberValuesToCommunicateMPI()>0)
      _ipvbulk->getValuesFromMPI(&(arrayMPI[1]));
  }
  #endif // HAVE_MPI

  virtual IPVariable* getInternalData() {return _ipvbulk->getInternalData();};
  virtual const IPVariable* getInternalData()  const {return _ipvbulk->getInternalData();};

// access to bulk
  virtual const bool isInterface() const{return _ipvbulk->isInterface();}
  virtual double vonMises() const{return _ipvbulk->vonMises();}
  virtual double get(const int i) const;
  virtual double defoEnergy() const{return _ipvfrac->defoEnergy();}
  virtual double plasticEnergy() const{return _ipvfrac->plasticEnergy();}
  virtual double damageEnergy() const {return _ipvfrac->damageEnergy();};
  virtual int fractureEnergy(double* arrayEnergy) const{return _ipvfrac->fractureEnergy(arrayEnergy);}

  virtual void getCauchyStress(STensor3 &cauchy) const { return _ipvbulk->getCauchyStress(cauchy); }
  virtual double getJ(int dim=3) const { return _ipvbulk->getJ(dim); };

  virtual bool dissipationIsActive() const {return _ipvbulk->dissipationIsActive();};

  virtual void blockDissipation(const bool fl){_ipvbulk->blockDissipation(fl);};
  virtual bool dissipationIsBlocked() const {return _ipvbulk->dissipationIsBlocked();};

  virtual void brokenSolver(const bool fl){_ipvbulk->brokenSolver(fl);};
  virtual bool solverIsBroken() const {return _ipvbulk->solverIsBroken();};

  virtual double getBarJ() const {return _ipvbulk->getBarJ();}
  virtual double &getRefToBarJ() {return _ipvbulk->getRefToBarJ();}
  virtual double getLocalJ() const {return _ipvbulk->getLocalJ();}
  virtual double &getRefToLocalJ() {return _ipvbulk->getRefToLocalJ();}
  virtual void getBackLocalDeformationGradient(STensor3& localF) const ;

  virtual const STensor3 &getConstRefToDeformationGradient() const {return _ipvbulk->getConstRefToDeformationGradient();}
  virtual STensor3 &getRefToDeformationGradient() {return _ipvbulk->getRefToDeformationGradient();}

  virtual const STensor3 &getConstRefToFirstPiolaKirchhoffStress() const {return _ipvbulk->getConstRefToFirstPiolaKirchhoffStress();}
  virtual STensor3 &getRefToFirstPiolaKirchhoffStress() {return _ipvbulk->getRefToFirstPiolaKirchhoffStress();}

  virtual const STensor43 &getConstRefToTangentModuli() const {return _ipvbulk->getConstRefToTangentModuli();}
  virtual STensor43 &getRefToTangentModuli() {return _ipvbulk->getRefToTangentModuli();}

  virtual const STensor43 &getConstRefToElasticTangentModuli() const {return _ipvbulk->getConstRefToElasticTangentModuli();};
  virtual STensor43 &getRefToElasticTangentModuli() {return _ipvbulk->getRefToElasticTangentModuli();};

  virtual const STensor43 &getConstRefToDGElasticTangentModuli() const {return _ipvbulk->getConstRefToDGElasticTangentModuli();}
  virtual void setRefToDGElasticTangentModuli(const STensor43 &eT) {return _ipvbulk->setRefToDGElasticTangentModuli(eT);}

  virtual const SVector3 &getConstRefToReferenceOutwardNormal() const {return _ipvbulk->getConstRefToReferenceOutwardNormal();}
  virtual SVector3 &getRefToReferenceOutwardNormal() {return _ipvbulk->getRefToReferenceOutwardNormal();}

  virtual const SVector3 &getConstRefToCurrentOutwardNormal() const {return _ipvbulk->getConstRefToCurrentOutwardNormal();}
  virtual SVector3 &getRefToCurrentOutwardNormal() {return _ipvbulk->getRefToCurrentOutwardNormal();}

  virtual const SVector3 &getConstRefToJump() const {return _ipvbulk->getConstRefToJump();}
  virtual SVector3 &getRefToJump() {return _ipvbulk->getRefToJump();}

  virtual const SVector3& getConstRefToIncompatibleJump() const {return _ipvbulk->getConstRefToIncompatibleJump();};
  virtual SVector3& getRefToIncompatibleJump() {return _ipvbulk->getRefToIncompatibleJump();};

   // non local
  virtual int getNumberNonLocalVariable() const {return _ipvbulk->getNumberNonLocalVariable();};
  //
  virtual double getConstRefToLocalVariable(const int idex) const {return _ipvbulk->getConstRefToLocalVariable(idex);};
  virtual double& getRefToLocalVariable(const int idex) {return _ipvbulk->getRefToLocalVariable(idex);};
  //
  virtual double getConstRefToNonLocalVariableInMaterialLaw(const int idex) const {return _ipvbulk->getConstRefToNonLocalVariableInMaterialLaw(idex);};
  virtual double& getRefToNonLocalVariableInMaterialLaw(const int idex) {return _ipvbulk->getRefToNonLocalVariableInMaterialLaw(idex);};
  //
  virtual double getConstRefToNonLocalVariable(const int idex) const {return _ipvbulk->getConstRefToNonLocalVariable(idex);};
  virtual double& getRefToNonLocalVariable(const int idex) {return _ipvbulk->getRefToNonLocalVariable(idex);};

  virtual const std::vector<STensor3> &getConstRefToDLocalVariableDStrain() const {return _ipvbulk->getConstRefToDLocalVariableDStrain();};
  virtual std::vector<STensor3> &getRefToDLocalVariableDStrain() {return _ipvbulk->getRefToDLocalVariableDStrain();};

  virtual const std::vector<STensor3> &getConstRefToDStressDNonLocalVariable() const {return _ipvbulk->getConstRefToDStressDNonLocalVariable();};
  virtual std::vector<STensor3> &getRefToDStressDNonLocalVariable() {return _ipvbulk->getRefToDStressDNonLocalVariable();};

  virtual const fullMatrix<double> &getConstRefToDLocalVariableDNonLocalVariable() const {return _ipvbulk->getConstRefToDLocalVariableDNonLocalVariable();};
  virtual fullMatrix<double> &getRefToDLocalVariableDNonLocalVariable() {return _ipvbulk->getRefToDLocalVariableDNonLocalVariable();};

  virtual const fullVector<double> &getConstRefToNonLocalJump() const {return _ipvbulk->getConstRefToNonLocalJump(); }
  virtual fullVector<double> &getRefToNonLocalJump() {return _ipvbulk->getRefToNonLocalJump(); }

  virtual const std::vector<SVector3> &getConstRefToGradNonLocalVariable() const {return _ipvbulk->getConstRefToGradNonLocalVariable();}
  virtual std::vector<SVector3> &getRefToGradNonLocalVariable() {return _ipvbulk->getRefToGradNonLocalVariable();}
  //coupling non local with constitutiveExtraDof
  virtual const fullMatrix<double> & getConstRefTodLocalVariableDExtraDofDiffusionField() const {return _ipvbulk->getConstRefTodLocalVariableDExtraDofDiffusionField();}
  virtual fullMatrix<double> & getRefTodLocalVariableDExtraDofDiffusionField(){return _ipvbulk->getRefTodLocalVariableDExtraDofDiffusionField();}
  virtual const STensor3 &getConstRefToCharacteristicLengthMatrix(const int idx) const {return _ipvbulk->getConstRefToCharacteristicLengthMatrix(idx);};
  // coupling non local with curlData
  virtual const std::vector< std::vector< SVector3 > > & getConstRefTodLocalVariabledVectorPotential() const
  {
      return _ipvbulk->getConstRefTodLocalVariabledVectorPotential();
  }

  virtual std::vector< std::vector< SVector3 > > & getRefTodLocalVariabledVectorPotential()
  {
      return _ipvbulk->getRefTodLocalVariabledVectorPotential();
  }

  virtual const std::vector<std::vector< SVector3 > > & getConstRefTodLocalVariabledVectorCurl() const
  {
    return _ipvbulk->getConstRefTodLocalVariabledVectorCurl();
  }
  virtual std::vector<std::vector< SVector3 > > & getRefTodLocalVariabledVectorCurl()
  {
    return _ipvbulk->getRefTodLocalVariabledVectorCurl();
  }
  // higher order body force
  virtual bool hasBodyForceForHO() const
  {
    return _ipvbulk->hasBodyForceForHO();
  }  
  virtual const STensor33 &getConstRefToGM() const
  {
    return _ipvbulk->getConstRefToGM();
  }  
  virtual STensor33 &getRefToGM()
  {
   return _ipvbulk->getRefToGM();
  }  
  virtual const STensor33 &getConstRefTodBmdFm() const
  {
    return _ipvbulk->getConstRefTodBmdFm();
  }  
  virtual STensor33 &getRefTodBmdFm()
  {
   return _ipvbulk->getRefTodBmdFm();
  }  
  virtual const STensor43 &getConstRefToDFmdFM() const
  {
   return _ipvbulk->getConstRefToDFmdFM();
  }  
  virtual STensor43 &getRefToDFmdFM()
  {
   return _ipvbulk->getRefToDFmdFM();
  }  
  virtual const STensor53 &getConstRefToDFmdGM() const
  {
   return _ipvbulk->getConstRefToDFmdGM();
  }  
  virtual STensor53 &getRefToDFmdGM()
  {
   return _ipvbulk->getRefToDFmdGM();
  }

  virtual const SVector3 &getConstRefToBm() const
  {
    return _ipvbulk->getConstRefToBm();
  }  
  virtual SVector3 &getRefToBm()
  {
   return _ipvbulk->getRefToBm();
  } 
  
  virtual const STensor43 &getConstRefTodBmdGM() const
  {
    return _ipvbulk->getConstRefTodBmdGM();
  }  
  virtual STensor43 &getRefTodBmdGM()
  {
   return _ipvbulk->getRefTodBmdGM();
  } 

  virtual const STensor43 &getConstRefTodPmdFM() const
  {
    return _ipvbulk->getConstRefTodPmdFM();
  }  
  virtual STensor43 &getRefTodPmdFM()
  {
   return _ipvbulk->getRefTodPmdFM();
  }  
  virtual const STensor63 &getConstRefTodCmdFm() const
  {
    return _ipvbulk->getConstRefTodCmdFm();
  }  
  virtual STensor63 &getRefTodCmdFm()
  {
   return _ipvbulk->getRefTodCmdFm();
  }  
  
  virtual STensor63 *getPtrTodCmdFm()
  {
   return _ipvbulk->getPtrTodCmdFm();
  }

  // extradof
  virtual int getNumConstitutiveExtraDofDiffusionVariable() const {return _ipvbulk->getNumConstitutiveExtraDofDiffusionVariable();};
  virtual double getConstRefToField(const int idex) const {return _ipvbulk->getConstRefToField(idex);};
  virtual double& getRefToField(const int idex) {return _ipvbulk->getRefToField(idex);};
  virtual const std::vector<SVector3>  &getConstRefToGradField() const {return _ipvbulk->getConstRefToGradField();};
  virtual std::vector<SVector3>  &getRefToGradField() {return _ipvbulk->getRefToGradField();};
  virtual const std::vector<SVector3>  &getConstRefToFlux() const {return _ipvbulk->getConstRefToFlux();};
  virtual std::vector<SVector3>  &getRefToFlux() {return _ipvbulk->getRefToFlux();};
  virtual const std::vector<SVector3> &getConstRefToElectricDisplacement() const{return _ipvbulk->getConstRefToElectricDisplacement();}
  virtual std::vector<SVector3> &getRefToElectricDisplacement(){return _ipvbulk->getRefToElectricDisplacement();}
  virtual const std::vector<STensor3>  &getConstRefTodPdField() const {return _ipvbulk->getConstRefTodPdField();};
  virtual std::vector<STensor3>  &getRefTodPdField() {return _ipvbulk->getRefTodPdField();};
  virtual const std::vector<STensor33>  &getConstRefTodPdGradField() const {return _ipvbulk->getConstRefTodPdGradField();};
  virtual std::vector<STensor33>  &getRefTodPdGradField() {return _ipvbulk->getRefTodPdGradField();};
  virtual const std::vector<std::vector<STensor3> >  &getConstRefTodFluxdGradField() const {return _ipvbulk->getConstRefTodFluxdGradField();};
  virtual std::vector<std::vector<STensor3> >  &getRefTodFluxdGradField() {return _ipvbulk->getRefTodFluxdGradField();};
  virtual const std::vector<std::vector<SVector3> >  &getConstRefTodFluxdField() const {return _ipvbulk->getConstRefTodFluxdField();};
  virtual std::vector<std::vector<SVector3> >  &getRefTodFluxdField() {return _ipvbulk->getRefTodFluxdField();};
  virtual const std::vector<STensor33>  &getConstRefTodFluxdF() const {return _ipvbulk->getConstRefTodFluxdF();};
  virtual std::vector<STensor33>   &getRefTodFluxdF() {return _ipvbulk->getRefTodFluxdF();};
  virtual const std::vector< std::vector<SVector3> > &getConstRefTodElecDisplacementdField() const{return _ipvbulk->getConstRefTodElecDisplacementdField();}
  virtual std::vector< std::vector<SVector3> > &getRefTodElecDisplacementdField(){return _ipvbulk->getRefTodElecDisplacementdField();}
  virtual const std::vector< std::vector<STensor3> > &getConstRefTodElecDisplacementdGradField() const{return _ipvbulk->getConstRefTodElecDisplacementdGradField();}
  virtual std::vector< std::vector<STensor3> > &getRefTodElecDisplacementdGradField(){return _ipvbulk->getRefTodElecDisplacementdGradField();}
  virtual const std::vector<STensor33> &getConstRefTodElecDisplacementdF() const{return _ipvbulk->getConstRefTodElecDisplacementdF();}
  virtual std::vector<STensor33> &getRefTodElecDisplacementdF(){return _ipvbulk->getRefTodElecDisplacementdF();}
  virtual const std::vector<std::vector<const STensor3*> > &getConstRefToLinearK() const {return _ipvbulk->getConstRefToLinearK();};
  virtual void setConstRefToLinearK(const STensor3 &eT, int i, int j)  {_ipvbulk->setConstRefToLinearK(eT, i, j);};
  virtual const std::vector<std::vector<std::vector<STensor3> > >  &getConstRefTodLinearKdField() const {return _ipvbulk->getConstRefTodLinearKdField();};
  virtual std::vector<std::vector<std::vector<STensor3> > >  &getRefTodLinearKdField() {return _ipvbulk->getRefTodLinearKdField();};
  virtual const std::vector<std::vector<STensor43>>  &getConstRefTodLinearKdF() const {return _ipvbulk->getConstRefTodLinearKdF();};
  virtual std::vector<std::vector<STensor43>>  &getRefTodLinearKdF() {return _ipvbulk->getRefTodLinearKdF();};
/*  virtual const std::vector<std::vector<const STensor3*> > &getConstRefToEnergyK() const {return _ipvbulk->getConstRefToEnergyK();};
  virtual void setConstRefToEnergyK(const STensor3 &eT, int i, int j)  {_ipvbulk->setConstRefToEnergyK(eT, i, j);};
  virtual const std::vector<std::vector<std::vector<STensor3> > >  &getConstRefTodEnergyKdField() const {return _ipvbulk->getConstRefTodEnergyKdField();};
  virtual std::vector<std::vector<std::vector<STensor3> > >  &getRefTodEnergyKdField() {return _ipvbulk->getRefTodEnergyKdField();};
  virtual const std::vector<std::vector<STensor43>>  &getConstRefTodEnergyKdF() const {return _ipvbulk->getConstRefTodEnergyKdF();};
  virtual std::vector<std::vector<STensor43>>  &getRefTodEnergyKdF() {return _ipvbulk->getRefTodEnergyKdF();}; */
  virtual const std::vector<std::vector<std::vector<STensor3> > >  &getConstRefTodFluxdGradFielddField() const {return _ipvbulk->getConstRefTodFluxdGradFielddField();};
  virtual std::vector<std::vector<std::vector<STensor3> > >  &getRefTodFluxdGradFielddField() {return _ipvbulk->getRefTodFluxdGradFielddField();};
  virtual const std::vector<std::vector<STensor43>>  &getConstRefTodFluxdGradFielddF() const {return _ipvbulk->getConstRefTodFluxdGradFielddF();};
  virtual std::vector<std::vector<STensor43>>  &getRefTodFluxdGradFielddF() {return _ipvbulk->getRefTodFluxdGradFielddF();};
  virtual const fullVector<double>  &getConstRefToFieldSource() const {return _ipvbulk->getConstRefToFieldSource();};
  virtual fullVector<double>  &getRefToFieldSource() {return _ipvbulk->getRefToFieldSource();};
  virtual const fullMatrix<double>  &getConstRefTodFieldSourcedField() const {return _ipvbulk->getConstRefTodFieldSourcedField();};
  virtual fullMatrix<double>  &getRefTodFieldSourcedField() {return _ipvbulk->getRefTodFieldSourcedField();};
  virtual const std::vector<std::vector<SVector3> >  &getConstRefTodFieldSourcedGradField() const {return _ipvbulk->getConstRefTodFieldSourcedGradField();};
  virtual std::vector<std::vector<SVector3> >  &getRefTodFieldSourcedGradField() {return _ipvbulk->getRefTodFieldSourcedGradField();};
  virtual const std::vector<STensor3>  &getConstRefTodFieldSourcedF() const {return _ipvbulk->getConstRefTodFieldSourcedF();};
  virtual std::vector<STensor3>  &getRefTodFieldSourcedF() {return _ipvbulk->getRefTodFieldSourcedF();};
  virtual const fullVector<double>  &getConstRefToMechanicalSource() const {return _ipvbulk->getConstRefToMechanicalSource();};
  virtual fullVector<double>  &getRefToMechanicalSource() {return _ipvbulk->getRefToMechanicalSource();};
  virtual const fullMatrix<double>  &getConstRefTodMechanicalSourcedField() const {return _ipvbulk->getConstRefTodMechanicalSourcedField();};
  virtual fullMatrix<double>  &getRefTodMechanicalSourcedField() {return _ipvbulk->getRefTodMechanicalSourcedField();};
  virtual const std::vector<std::vector<SVector3> >  &getConstRefTodMechanicalSourcedGradField() const {return _ipvbulk->getConstRefTodMechanicalSourcedGradField();};
  virtual std::vector<std::vector<SVector3> >  &getRefTodMechanicalSourcedGradField() {return _ipvbulk->getRefTodMechanicalSourcedGradField();};
  virtual const std::vector<STensor3>  &getConstRefTodMechanicalSourcedF() const {return _ipvbulk->getConstRefTodMechanicalSourcedF();};
  virtual std::vector<STensor3>  &getRefTodMechanicalSourcedF() {return _ipvbulk->getRefTodMechanicalSourcedF();};
  virtual const fullVector<double>  &getConstRefToExtraDofFieldCapacityPerUnitField() const {return _ipvbulk->getConstRefToExtraDofFieldCapacityPerUnitField();};
  virtual fullVector<double>  &getRefToExtraDofFieldCapacityPerUnitField() {return _ipvbulk->getRefToExtraDofFieldCapacityPerUnitField();};
  virtual const fullVector<double> & getConstRefToEMFieldSource() const {return _ipvbulk->getConstRefToEMFieldSource();}
  virtual fullVector<double> & getRefToEMFieldSource() {return _ipvbulk->getRefToEMFieldSource();}
  virtual const fullMatrix<double> & getConstRefTodEMFieldSourcedField() const {return _ipvbulk->getConstRefTodEMFieldSourcedField();}
  virtual fullMatrix<double> & getRefTodEMFieldSourcedField() {return _ipvbulk->getRefTodEMFieldSourcedField();}
  virtual const std::vector< std::vector< SVector3 > > & getConstRefTodEMFieldSourcedGradField() const {return _ipvbulk->getConstRefTodEMFieldSourcedGradField();}
  virtual std::vector< std::vector< SVector3 > > & getRefTodEMFieldSourcedGradField() {return _ipvbulk->getRefTodEMFieldSourcedGradField();}
  virtual const fullVector<double>  &getConstRefToFieldJump() const {return _ipvbulk->getConstRefToFieldJump();};
  virtual fullVector<double>  &getRefToFieldJump() {return _ipvbulk->getRefToFieldJump();};
  virtual const fullVector<double>  &getConstRefToOneOverFieldJump() const {return _ipvbulk->getConstRefToOneOverFieldJump();};
  virtual fullVector<double>  &getRefToOneOverFieldJump() {return _ipvbulk->getRefToOneOverFieldJump();};
  virtual const fullMatrix<double>  &getConstRefTodOneOverFieldJumpdFieldm() const {return _ipvbulk->getConstRefTodOneOverFieldJumpdFieldm();};
  virtual fullMatrix<double>  &getRefTodOneOverFieldJumpdFieldm() {return _ipvbulk->getRefTodOneOverFieldJumpdFieldm() ;};
  virtual const fullMatrix<double>  &getConstRefTodOneOverFieldJumpdFieldp() const {return _ipvbulk->getConstRefTodOneOverFieldJumpdFieldp();};
  virtual fullMatrix<double>  &getRefTodOneOverFieldJumpdFieldp() {return _ipvbulk->getRefTodOneOverFieldJumpdFieldp();};
  virtual const std::vector<SVector3>  &getConstRefToInterfaceFlux() const {return _ipvbulk->getConstRefToInterfaceFlux();};
  virtual std::vector<SVector3>  &getRefToInterfaceFlux() {return _ipvbulk->getRefToInterfaceFlux();};
  
  // curl data
  virtual int getNumConstitutiveCurlVariable() const { return _ipvbulk->getNumConstitutiveCurlVariable();}
  virtual const SVector3 & getConstRefToVectorPotential(const int idex) const { return _ipvbulk->getConstRefToVectorPotential(idex);}
  virtual SVector3 & getRefToVectorPotential(const int idex) { return _ipvbulk->getRefToVectorPotential(idex);}
  virtual const SVector3 & getConstRefToVectorCurl(const int idex) const { return _ipvbulk->getConstRefToVectorCurl(idex);}
  virtual SVector3 & getRefToVectorCurl(const int idex) { return _ipvbulk->getRefToVectorCurl(idex);}
  virtual const SVector3 & getConstRefToVectorField(const int idex) const { return _ipvbulk->getConstRefToVectorField(idex);}
  virtual SVector3 & getRefToVectorField(const int idex) { return _ipvbulk->getRefToVectorField(idex);}
  virtual const SVector3 & getConstRefToSourceVectorField(const int idex) const { return _ipvbulk->getConstRefToSourceVectorField(idex);}
  virtual SVector3 & getRefToSourceVectorField(const int idex) { return _ipvbulk->getRefToSourceVectorField(idex);}
  virtual const SVector3 & getConstRefToMechanicalFieldSource(const int idex) const { return _ipvbulk->getConstRefToMechanicalFieldSource(idex);}
  virtual SVector3 & getRefToMechanicalFieldSource(const int idex) { return _ipvbulk->getRefToMechanicalFieldSource(idex);}

  virtual const std::vector<std::vector< STensor3 > > & getConstRefTodVectorFielddVectorPotential() const { return _ipvbulk->getConstRefTodVectorFielddVectorPotential();}
  virtual const std::vector<std::vector< STensor3 > > & getConstRefTodVectorFielddVectorCurl() const { return _ipvbulk->getConstRefTodVectorFielddVectorCurl();}
  virtual const std::vector< STensor33 > & getConstRefTodVectorFielddF() const { return _ipvbulk->getConstRefTodVectorFielddF();}
  virtual const std::vector<std::vector< SVector3 > > & getConstRefTodVectorFielddExtraDofField() const { return _ipvbulk->getConstRefTodVectorFielddExtraDofField();}
  virtual const std::vector<std::vector< STensor3 > > & getConstRefTodVectorFielddGradExtraDofField() const { return _ipvbulk->getConstRefTodVectorFielddGradExtraDofField();}
  virtual const std::vector< STensor33 > & getConstRefTodPdVectorCurl() const { return _ipvbulk->getConstRefTodPdVectorCurl();}
  virtual const std::vector< STensor33 > & getConstRefTodPdVectorPotential() const {return _ipvbulk->getConstRefTodPdVectorPotential();}

  virtual std::vector<std::vector< STensor3 > > & getRefTodVectorFielddVectorPotential() { return _ipvbulk->getRefTodVectorFielddVectorPotential();}
  virtual std::vector<std::vector< STensor3 > > & getRefTodVectorFielddVectorCurl() { return _ipvbulk->getRefTodVectorFielddVectorCurl();}
  virtual std::vector< STensor33 > & getRefTodVectorFielddF() { return _ipvbulk->getRefTodVectorFielddF();}
  virtual std::vector<std::vector< SVector3 > > & getRefTodVectorFielddExtraDofField() { return _ipvbulk->getRefTodVectorFielddExtraDofField();}
  virtual std::vector<std::vector< STensor3 > > & getRefTodVectorFielddGradExtraDofField() { return _ipvbulk->getRefTodVectorFielddGradExtraDofField();}
  virtual std::vector< STensor33 > & getRefTodPdVectorCurl() { return _ipvbulk->getRefTodPdVectorCurl();}
  virtual std::vector< STensor33 > & getRefTodPdVectorPotential() {return _ipvbulk->getRefTodPdVectorPotential();}

  virtual const std::vector<std::vector< SVector3 > > & getConstRefTodVectorFielddNonLocalVariable() const { return _ipvbulk->getConstRefTodVectorFielddNonLocalVariable();}
  virtual std::vector<std::vector< SVector3 > > & getRefTodVectorFielddNonLocalVariable() { return _ipvbulk->getRefTodVectorFielddNonLocalVariable();}
  virtual const std::vector< std::vector< SVector3 > > & getConstRefTodSourceVectorFielddNonLocalVariable() const { return _ipvbulk->getConstRefTodSourceVectorFielddNonLocalVariable();}
  virtual std::vector< std::vector< SVector3 > > & getRefTodSourceVectorFielddNonLocalVariable() { return _ipvbulk->getRefTodSourceVectorFielddNonLocalVariable();}

  virtual const std::vector< std::vector< SVector3 > > & getConstRefTodSourceVectorFielddExtraDofField() const { return _ipvbulk->getConstRefTodSourceVectorFielddExtraDofField();}
  virtual std::vector< std::vector< SVector3 > > & getRefTodSourceVectorFielddExtraDofField() { return _ipvbulk->getRefTodSourceVectorFielddExtraDofField();}
  virtual const std::vector< std::vector< STensor3 > > & getConstRefTodSourceVectorFielddGradExtraDofField() const { return _ipvbulk->getConstRefTodSourceVectorFielddGradExtraDofField();}
  virtual std::vector< std::vector< STensor3 > > & getRefTodSourceVectorFielddGradExtraDofField() { return _ipvbulk->getRefTodSourceVectorFielddGradExtraDofField();}
  virtual const std::vector< std::vector< STensor3 > > & getConstRefTodSourceVectorFielddVectorCurl() const { return _ipvbulk->getConstRefTodSourceVectorFielddVectorCurl();}
  virtual std::vector< std::vector< STensor3 > > & getRefTodSourceVectorFielddVectorCurl() { return _ipvbulk->getRefTodSourceVectorFielddVectorCurl();}
  virtual const std::vector< std::vector< STensor3 > > & getConstRefTodSourceVectorFielddVectorPotential() const { return _ipvbulk->getConstRefTodSourceVectorFielddVectorPotential();}
  virtual std::vector< std::vector< STensor3 > > & getRefTodSourceVectorFielddVectorPotential() { return _ipvbulk->getRefTodSourceVectorFielddVectorPotential();}
  virtual const std::vector< std::vector< STensor3 > > & getConstRefTodMechanicalFieldSourcedVectorPotential() const {return _ipvbulk->getConstRefTodMechanicalFieldSourcedVectorPotential();}
  virtual std::vector< std::vector< STensor3 > > & getRefTodMechanicalFieldSourcedVectorPotential() {return _ipvbulk->getRefTodMechanicalFieldSourcedVectorPotential();}
  virtual const std::vector< std::vector< STensor3 > > & getConstRefTodMechanicalFieldSourcedVectorCurl() const { return _ipvbulk->getConstRefTodMechanicalFieldSourcedVectorCurl();}
  virtual std::vector< std::vector< STensor3 > > & getRefTodMechanicalFieldSourcedVectorCurl() { return _ipvbulk->getRefTodMechanicalFieldSourcedVectorCurl();}
  virtual const std::vector< STensor33 > & getConstRefTodSourceVectorFielddF() const { return _ipvbulk->getConstRefTodSourceVectorFielddF();}
  virtual std::vector< STensor33 > & getRefTodSourceVectorFielddF() { return _ipvbulk->getRefTodSourceVectorFielddF();}
  virtual const std::vector< STensor33 > & getConstRefTodMechanicalFieldSourcedF() const { return _ipvbulk->getConstRefTodMechanicalFieldSourcedF();}
  virtual std::vector< STensor33 > & getRefTodMechanicalFieldSourcedF() { return _ipvbulk->getRefTodMechanicalFieldSourcedF();}
  virtual const std::vector< std::vector< SVector3 > > & getConstRefTodMechanicalFieldSourcedExtraDofField() const { return _ipvbulk->getConstRefTodMechanicalFieldSourcedExtraDofField();}
  virtual std::vector< std::vector< SVector3 > > & getRefTodMechanicalFieldSourcedExtraDofField() { return _ipvbulk->getRefTodMechanicalFieldSourcedExtraDofField();}
  virtual const std::vector< std::vector< STensor3 > > & getConstRefTodMechanicalFieldSourcedGradExtraDofField() const { return _ipvbulk->getConstRefTodMechanicalFieldSourcedGradExtraDofField();}
  virtual std::vector< std::vector< STensor3 > > & getRefTodMechanicalFieldSourcedGradExtraDofField() { return _ipvbulk->getRefTodMechanicalFieldSourcedGradExtraDofField();}
  virtual const std::vector< std::vector< SVector3 > > & getConstRefTodMechanicalFieldSourcedNonLocalVariable() const { return _ipvbulk->getConstRefTodMechanicalFieldSourcedNonLocalVariable();}
  virtual std::vector< std::vector< SVector3 > > & getRefTodMechanicalFieldSourcedNonLocalVariable() { return _ipvbulk->getRefTodMechanicalFieldSourcedNonLocalVariable();}
  virtual const SVector3 & getConstRefTodSourceVectorFielddt(const int idex) const {return _ipvbulk->getConstRefTodSourceVectorFielddt(idex);}
  virtual SVector3 & getRefTodSourceVectorFielddt(const int idex) {return _ipvbulk->getRefTodSourceVectorFielddt(idex);}

  //Energy Conjugated Field (fT & fv)
  virtual double getConstRefToEnergyConjugatedField(const int idex) const {return _ipvbulk->getConstRefToEnergyConjugatedField(idex);};
  virtual double  &getRefToEnergyConjugatedField(const int idex) {return _ipvbulk->getRefToEnergyConjugatedField(idex);};
  virtual const std::vector<SVector3>  &getConstRefToGradEnergyConjugatedField() const {return _ipvbulk->getConstRefToGradEnergyConjugatedField();};
  virtual std::vector<SVector3>  &getRefToGradEnergyConjugatedField() {return _ipvbulk->getRefToGradEnergyConjugatedField();};
  virtual const  std::vector<std::vector<double>>  &getConstRefTodFielddEnergyConjugatedField() const {return _ipvbulk->getConstRefTodFielddEnergyConjugatedField();};
  virtual std::vector<std::vector<double>>  &getRefTodFielddEnergyConjugatedField() {return _ipvbulk->getRefTodFielddEnergyConjugatedField();};
  virtual const std::vector<std::vector<SVector3>>  &getConstRefTodGradFielddEnergyConjugatedField() const {return _ipvbulk->getConstRefTodGradFielddEnergyConjugatedField();};
  virtual std::vector<std::vector<SVector3>>  &getRefTodGradFielddEnergyConjugatedField() {return _ipvbulk->getRefTodGradFielddEnergyConjugatedField();};
  virtual const std::vector<std::vector<STensor3>>  &getConstRefTodGradFielddGradEnergyConjugatedField() const {return _ipvbulk->getConstRefTodGradFielddGradEnergyConjugatedField();};
  virtual std::vector<std::vector<STensor3>>  &getRefTodGradFielddGradEnergyConjugatedField() {return _ipvbulk->getRefTodGradFielddGradEnergyConjugatedField();};
  virtual const fullVector<double>  &getConstRefToEnergyConjugatedFieldJump() const {return _ipvbulk->getConstRefToEnergyConjugatedFieldJump();};
  virtual fullVector<double>  &getRefToEnergyConjugatedFieldJump() {return _ipvbulk->getRefToEnergyConjugatedFieldJump();};
  virtual const  std::vector<std::vector<double>>  &getConstRefTodEnergyConjugatedFieldJumpdFieldm() const {return _ipvbulk->getConstRefTodEnergyConjugatedFieldJumpdFieldm();};
  virtual std::vector<std::vector<double>>  &getRefTodEnergyConjugatedFieldJumpdFieldm() {return _ipvbulk->getRefTodEnergyConjugatedFieldJumpdFieldm();};
  virtual const  std::vector<std::vector<double>>  &getConstRefTodEnergyConjugatedFieldJumpdFieldp() const {return _ipvbulk->getConstRefTodEnergyConjugatedFieldJumpdFieldp();};
  virtual std::vector<std::vector<double>>  &getRefTodEnergyConjugatedFieldJumpdFieldp() {return _ipvbulk->getRefTodEnergyConjugatedFieldJumpdFieldp();};

  //coupling constitutive extra dof with curl data
  virtual const std::vector< std::vector< STensor3 > > & getConstRefTodFluxdVectorPotential() const
  {
      return _ipvbulk->getConstRefTodFluxdVectorPotential();
  }
  virtual std::vector< std::vector< STensor3 > > & getRefTodFluxdVectorPotential()
  {
      return _ipvbulk->getRefTodFluxdVectorPotential();
  }
  virtual const std::vector< std::vector< STensor3 > > & getConstRefTodFluxdVectorCurl() const
  {
    return _ipvbulk->getConstRefTodFluxdVectorCurl();
  }
    virtual const std::vector< std::vector< SVector3 > > & getConstRefTodFieldSourcedVectorPotential() const
    {
        return _ipvbulk->getConstRefTodFieldSourcedVectorPotential();
    }
    virtual const std::vector< std::vector< SVector3 > > & getConstRefTodMechanicalSourcedVectorPotential() const
    {
        return _ipvbulk->getConstRefTodMechanicalSourcedVectorPotential();
    }
  virtual const std::vector< std::vector< SVector3 > > & getConstRefTodFieldSourcedVectorCurl() const
  {
    return _ipvbulk->getConstRefTodFieldSourcedVectorCurl();
  }
  virtual const std::vector< std::vector< SVector3 > > & getConstRefTodMechanicalSourcedVectorCurl() const
  {
    return _ipvbulk->getConstRefTodMechanicalSourcedVectorCurl();
  }

  virtual std::vector< std::vector< STensor3 > > & getRefTodFluxdVectorCurl()
  {
    return _ipvbulk->getRefTodFluxdVectorCurl();
  }
    virtual std::vector< std::vector< SVector3 > > & getRefTodFieldSourcedVectorPotential()
    {
        return _ipvbulk->getRefTodFieldSourcedVectorPotential();
    }
    virtual std::vector< std::vector< SVector3 > > & getRefTodMechanicalSourcedVectorPotential()
    {
        return _ipvbulk->getRefTodMechanicalSourcedVectorPotential();
    }
  virtual std::vector< std::vector< SVector3 > > & getRefTodFieldSourcedVectorCurl()
  {
    return _ipvbulk->getRefTodFieldSourcedVectorCurl();
  }
  virtual std::vector< std::vector< SVector3 > > & getRefTodMechanicalSourcedVectorCurl()
  {
    return _ipvbulk->getRefTodMechanicalSourcedVectorCurl();
  }
  virtual const std::vector< std::vector< SVector3 > > & getConstRefTodEMFieldSourcedVectorPotential() const
  {
      return _ipvbulk->getConstRefTodEMFieldSourcedVectorPotential();
  }
  virtual std::vector< std::vector< SVector3 > > & getRefTodEMFieldSourcedVectorPotential()
  {
      return _ipvbulk->getRefTodEMFieldSourcedVectorPotential();
  }
  virtual const std::vector< std::vector< SVector3 > > & getConstRefTodEMFieldSourcedVectorCurl() const
  {
      return _ipvbulk->getConstRefTodEMFieldSourcedVectorCurl();
  }
  virtual std::vector< std::vector< SVector3 > > & getRefTodEMFieldSourcedVectorCurl()
  {
      return _ipvbulk->getRefTodEMFieldSourcedVectorCurl();
  }
  virtual const std::vector< STensor3 > & getConstRefTodEMFieldSourcedF() const
  {
      return _ipvbulk->getConstRefTodEMFieldSourcedF();
  }
  virtual std::vector< STensor3 > & getRefTodEMFieldSourcedF()
  {
      return _ipvbulk->getRefTodEMFieldSourcedF();
  }
  virtual const std::vector<std::vector< STensor3 > > & getConstRefTodElecDisplacementdVectorPotential() const
  {
      return _ipvbulk->getConstRefTodElecDisplacementdVectorPotential();
  }
  virtual const std::vector<std::vector< STensor3 > > & getConstRefTodElecDisplacementdVectorCurl() const
  {
      return _ipvbulk->getConstRefTodElecDisplacementdVectorCurl();
  }
  virtual std::vector<std::vector< STensor3 > > & getRefTodElecDisplacementdVectorPotential()
  {
      return _ipvbulk->getRefTodElecDisplacementdVectorPotential();
  }
  virtual std::vector<std::vector< STensor3 > > & getRefTodElecDisplacementdVectorCurl()
  {
      return _ipvbulk->getRefTodElecDisplacementdVectorCurl();
  }

  //coupling constitutive extra dof with nonLocal
  virtual const std::vector<std::vector<SVector3> > & getConstRefTodFluxdNonLocalVariable() const {return _ipvbulk->getConstRefTodFluxdNonLocalVariable();}
  virtual std::vector<std::vector<SVector3> > & getRefTodFluxdNonLocalVariable(){return _ipvbulk->getRefTodFluxdNonLocalVariable();}
  virtual const fullMatrix<double> &getConstRefTodFieldSourcedNonLocalVariable() const{return _ipvbulk->getConstRefTodFieldSourcedNonLocalVariable();}
  virtual fullMatrix<double> &getRefTodFieldSourcedNonLocalVariable(){return _ipvbulk->getRefTodFieldSourcedNonLocalVariable();}
  virtual const fullMatrix<double> &getConstRefTodMechanicalSourcedNonLocalVariable() const{return _ipvbulk->getConstRefTodMechanicalSourcedNonLocalVariable();}
  virtual fullMatrix<double> &getRefTodMechanicalSourcedNonLocalVariable(){return _ipvbulk->getRefTodMechanicalSourcedNonLocalVariable();}
  virtual const fullMatrix<double> & getConstRefTodEMFieldSourcedNonLocalVariable() const {return _ipvbulk->getConstRefTodEMFieldSourcedNonLocalVariable();}
  virtual fullMatrix<double> & getRefTodEMFieldSourcedNonLocalVariable() {return _ipvbulk->getRefTodEMFieldSourcedNonLocalVariable();}
  virtual const std::vector< std::vector<SVector3> > &getConstRefTodElecDisplacementdNonLocalVariable() const{return _ipvbulk->getConstRefTodElecDisplacementdNonLocalVariable();}
  virtual std::vector< std::vector<SVector3> > &getRefTodElecDisplacementdNonLocalVariable(){return _ipvbulk->getRefTodElecDisplacementdNonLocalVariable();}


  virtual const std::vector<const STensor3*> &getConstRefToLinearSymmetrizationCoupling() const  {return _ipvbulk->getConstRefToLinearSymmetrizationCoupling();};
  virtual void setConstRefToLinearSymmetrizationCoupling(const STensor3 &eT, int i)  {_ipvbulk->setConstRefToLinearSymmetrizationCoupling(eT,i);};


  //------------------get 1st piola b4 AV ---------------//

  virtual const STensor3 &getConstRefToFirstPiolaKirchhoffStressb4AV() const {return _ipvbulk->getConstRefToFirstPiolaKirchhoffStressb4AV();}
  virtual STensor3 &getRefToFirstPiolaKirchhoffStressb4AV() {return _ipvbulk->getRefToFirstPiolaKirchhoffStressb4AV();}

  virtual void setNonLocalToLocal(const bool fl){_ipvbulk->setNonLocalToLocal(fl);};
  virtual bool getNonLocalToLocal() const {return _ipvbulk->getNonLocalToLocal();};

  // interface
  virtual bool withDeformationGradient() const {return _ipvfrac->withDeformationGradient();}; // with deformation gradient
  virtual bool withJumpGradient() const {return _ipvfrac->withJumpGradient();}; // with jump gradient
  virtual bool withIncompatibleJump() const {return _ipvfrac->withIncompatibleJump();};

  virtual const STensor3 &getConstRefToJumpGradient() const {return _ipvfrac->getConstRefToJumpGradient();}
  virtual STensor3 &getRefToJumpGradient() {return _ipvfrac->getRefToJumpGradient();}

	 virtual const SVector3 &getConstRefToInterfaceForce() const {return _ipvfrac->getConstRefToInterfaceForce();}
  virtual SVector3 &getRefToInterfaceForce() {return _ipvfrac->getRefToInterfaceForce();}

  virtual const STensor3 &getConstRefToDInterfaceForceDjump() const {return _ipvfrac->getConstRefToDInterfaceForceDjump();}
  virtual STensor3 &getRefToDInterfaceForceDjump() {return _ipvfrac->getRefToDInterfaceForceDjump();}

  virtual const STensor33 &getConstRefToDInterfaceForceDjumpGradient() const {return _ipvfrac->getConstRefToDInterfaceForceDjumpGradient();}
  virtual STensor33 &getRefToDInterfaceForceDjumpGradient() {return _ipvfrac->getRefToDInterfaceForceDjumpGradient();}

  virtual const STensor33 &getConstRefToDInterfaceForceDDeformationGradient() const {return _ipvfrac->getConstRefToDInterfaceForceDDeformationGradient();};
  virtual STensor33 &getRefToDInterfaceForceDDeformationGradient() {return _ipvfrac->getRefToDInterfaceForceDDeformationGradient();};

  virtual const STensor3& getConstRefToDInterfaceForceDIncompatibleJump() const {return _ipvfrac->getConstRefToDInterfaceForceDIncompatibleJump();};
  virtual STensor3& getRefToDInterfaceForceDIncompatibleJump() {return _ipvfrac->getRefToDInterfaceForceDIncompatibleJump();};

	// for path following based on  irreversibe energt
	virtual double irreversibleEnergy() const  {return _ipvfrac->irreversibleEnergy();};
	virtual double& getRefToIrreversibleEnergy() {return _ipvfrac->getRefToIrreversibleEnergy();};
	// to avoid duplicate allocation
	virtual const STensor3& getConstRefToDIrreversibleEnergyDDeformationGradient() const {return _ipvfrac->getConstRefToDIrreversibleEnergyDDeformationGradient();};
	virtual STensor3& getRefToDIrreversibleEnergyDDeformationGradient() {return _ipvfrac->getRefToDIrreversibleEnergyDDeformationGradient();};

	// irreversible ennergy depend on nonlocal variale
	virtual const double& getConstRefToDIrreversibleEnergyDNonLocalVariable(const int index) const {return _ipvbulk->getConstRefToDIrreversibleEnergyDNonLocalVariable(index);};
	virtual double& getRefToDIrreversibleEnergyDNonLocalVariable(const int index) {return _ipvbulk->getRefToDIrreversibleEnergyDNonLocalVariable(index);};

	virtual SVector3& getRefToDIrreversibleEnergyDJump() {return _ipvfrac->getRefToDIrreversibleEnergyDJump();};
	virtual const SVector3& getConstRefToDIrreversibleEnergyDJump() const {return _ipvfrac->getConstRefToDIrreversibleEnergyDJump();};

	virtual SVector3& getRefToDIrreversibleEnergyDIncompatibleJump() {return _ipvfrac->getRefToDIrreversibleEnergyDIncompatibleJump();};
	virtual const SVector3& getConstRefToDIrreversibleEnergyDIncompatibleJump() const {return _ipvfrac->getConstRefToDIrreversibleEnergyDIncompatibleJump();};
  // access to cohesive law

  virtual const SVector3& getConstRefToDGJump() const {return _ipvfrac->getConstRefToDGJump();};
  virtual SVector3& getRefToDGJump() {return _ipvfrac->getRefToDGJump();};

  virtual const SVector3& getConstRefToCohesiveJump() const {return _ipvfrac->getConstRefToCohesiveJump();};
  virtual SVector3& getRefToCohesiveJump() {return _ipvfrac->getRefToCohesiveJump();};

  virtual const STensor33& getConstRefToDCohesiveJumpDDeformationGradient() const {return _ipvfrac->getConstRefToDCohesiveJumpDDeformationGradient();};
  virtual STensor33& getRefToDCohesiveJumpDDeformationGradient() {return _ipvfrac->getRefToDCohesiveJumpDDeformationGradient();};

  virtual const STensor3& getConstRefToDCohesiveJumpDJump() const  {return _ipvfrac->getConstRefToDCohesiveJumpDJump();};
  virtual STensor3& getRefToDCohesiveJumpDJump() {return _ipvfrac->getRefToDCohesiveJumpDJump();};

  virtual const STensor3& getConstRefToDCohesiveJumpDIncompatibleJump() const{return _ipvfrac->getConstRefToDCohesiveJumpDIncompatibleJump();}
  virtual STensor3& getRefToDCohesiveJumpDIncompatibleJump() {return _ipvfrac->getRefToDCohesiveJumpDIncompatibleJump();}

  virtual const SVector3& getConstRefToInitialCohesiveJump() const {return _ipvfrac->getConstRefToInitialCohesiveJump();};
  virtual SVector3& getRefToInitialCohesiveJump() {return _ipvfrac->getRefToInitialCohesiveJump();};

  virtual bool isDeleted() const {return _ipvfrac->isDeleted();};
  virtual void Delete(const bool fl) {IPVariable2ForFracture<dG3DIPVariableBase,Cohesive3DIPVariableBase>::Delete(fl); _ipvbulk->Delete(fl); _ipvfrac->Delete(fl);}
	virtual bool ifTension() const {return _ipvfrac->ifTension();};
	virtual void setTensionFlag(const bool tensionflag) {_ipvfrac->setTensionFlag(tensionflag);};

  virtual const STensor3& getConstRefToBulkDeformationGradientAtFailureOnset() const {return _ipvfrac->getConstRefToBulkDeformationGradientAtFailureOnset();};
  virtual STensor3& getRefToBulkDeformationGradientAtFailureOnset() {return _ipvfrac->getRefToBulkDeformationGradientAtFailureOnset();};

  virtual const SVector3& getConstRefToIncompatibleJumpAtFailureOnset() const {return _ipvfrac->getConstRefToIncompatibleJumpAtFailureOnset();};
  virtual SVector3& getRefToIncompatibleJumpAtFailureOnset() {return _ipvfrac->getRefToIncompatibleJumpAtFailureOnset();};

  // deformation gradient without otherparts relating to the displacement jump
  virtual const STensor3& getBulkDeformationGradient() const {return _ipvfrac->getBulkDeformationGradient();};
  virtual void setBulkDeformationGradient(const STensor3& Fb) {_ipvfrac->setBulkDeformationGradient(Fb);};

  virtual const STensor33& getConstRefToDInterfaceDeformationGradientDJump() const {return _ipvfrac->getConstRefToDInterfaceDeformationGradientDJump();};
  virtual STensor33& getRefToDInterfaceDeformationGradientDJump() {return _ipvfrac->getRefToDInterfaceDeformationGradientDJump();};

  virtual const STensor43& getConstRefToDInterfaceDeformationGradientDDeformationGradient() const {return _ipvfrac->getConstRefToDInterfaceDeformationGradientDDeformationGradient();};
  virtual STensor43& getRefToDInterfaceDeformationGradientDDeformationGradient() {return _ipvfrac->getRefToDInterfaceDeformationGradientDDeformationGradient();};

  virtual const STensor33& getConstRefToDInterfaceDeformationGradientDIncompatibleJump() const {return _ipvfrac->getConstRefToDInterfaceDeformationGradientDIncompatibleJump();};
  virtual STensor33& getRefToDInterfaceDeformationGradientDIncompatibleJump() {return _ipvfrac->getRefToDInterfaceDeformationGradientDIncompatibleJump();};

  //current basis
  virtual const SVector3& getConstRefToCohesiveNormal() const {return _ipvfrac->getConstRefToCohesiveNormal();};
  virtual SVector3& getRefToCohesiveNormal() {return _ipvfrac->getRefToCohesiveNormal();};
  virtual const SVector3& getConstRefToCohesiveTangent() const {return _ipvfrac->getConstRefToCohesiveTangent();};
  virtual SVector3& getRefToCohesiveTangent() {return _ipvfrac->getRefToCohesiveTangent();};
  virtual const SVector3& getConstRefToCohesiveBiTangent() const {return _ipvfrac->getConstRefToCohesiveBiTangent();};
  virtual SVector3& getRefToCohesiveBiTangent() {return _ipvfrac->getRefToCohesiveBiTangent();};


  //reference basis
  virtual const SVector3& getConstRefToCohesiveReferenceNormal() const {return _ipvfrac->getConstRefToCohesiveReferenceNormal();};
  virtual SVector3& getRefToCohesiveReferenceNormal() {return _ipvfrac->getRefToCohesiveReferenceNormal();};
  virtual const SVector3& getConstRefToCohesiveReferenceTangent() const {return _ipvfrac->getConstRefToCohesiveReferenceTangent();};
  virtual SVector3& getRefToCohesiveReferenceTangent() {return _ipvfrac->getRefToCohesiveReferenceTangent();};
  virtual const SVector3& getConstRefToCohesiveReferenceBiTangent() const {return _ipvfrac->getConstRefToCohesiveReferenceBiTangent();};
  virtual SVector3& getRefToCohesiveReferenceBiTangent() {return _ipvfrac->getRefToCohesiveReferenceBiTangent();};


  void initializeFracture(const SVector3 &ujump_, const double Kp,
                          const double smax, const double snor, const double tau, const double Gc,
                          const double beta_, const bool ift, const STensor3 &cauchy, const double K, const SVector3& norm, const SVector3& f0,
                          const IPVariable* bulkIPv)
  {  _ipvfrac->initializeFracture(ujump_,Kp, smax, snor, tau, Gc, beta_, ift, cauchy,K,norm,f0,bulkIPv);}


  virtual IPVariable* clone() const {return new FractureCohesive3DIPVariable(*this);};
  virtual void restart();
};


class MultiscaleFractureCohesive3DIPVariable : public FractureCohesive3DIPVariable{
	protected:

  public:
    MultiscaleFractureCohesive3DIPVariable(const double facSigmac=1.):FractureCohesive3DIPVariable(facSigmac){
    };
    MultiscaleFractureCohesive3DIPVariable(const MultiscaleFractureCohesive3DIPVariable &src):FractureCohesive3DIPVariable(src){};
    virtual MultiscaleFractureCohesive3DIPVariable& operator=(const IPVariable &source){
      FractureCohesive3DIPVariable::operator=(source);
			const MultiscaleFractureCohesive3DIPVariable* psrc = dynamic_cast<const MultiscaleFractureCohesive3DIPVariable*>(&source);
			if (psrc!=NULL){


			}
      return *this;
    };
    virtual ~MultiscaleFractureCohesive3DIPVariable(){}

		virtual void broken();
		virtual void nobroken();

    virtual IPVariable* clone() const {return new MultiscaleFractureCohesive3DIPVariable(*this);};
    virtual void restart(){
      FractureCohesive3DIPVariable::restart();
    };
};


#endif // FRACTURECOHESIVEIPVARIABLE_H_
