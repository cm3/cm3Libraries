%feature("autodoc","1");
#ifdef _DEBUG
  %module dG3DpyDebug
#else
  %module dG3Dpy
#endif


%include nlmechsolpy.i

%{
  #include "dG3DMaterialLaw.h"
  #include "nonLocalDamageDG3DMaterialLaw.h"
  #include "dG3DCohesiveMaterialLaw.h"
  #include "dG3DMultiscaleCohesiveMaterialLaw.h"	
  #include "FractureCohesiveDG3DMaterialLaw.h"
  #include "dG3DDomain.h"
  #include "dG3DContactDomain.h"
  #include "dG3DCohesiveBandMaterialLaw.h"
  #include "dG3DMultiscaleMaterialLaw.h"
  #include "hoDGPartDomain.h"
  #include "hoDGMaterialLaw.h"
  #include "NonLocalDamageHyperelasticDG3DMaterialLaw.h"
  #include "axisymmetricDG3DDomain.h"
  #include "dG3D1DDomain.h"
  #include "computeWithMaterialLaw.h"
  #include "dG3DDeepMaterialNetworks.h"
%}
%nodefaultctor dG3DMaterialLaw;
%nodefaultctor NonLocalPorousDuctileDamageDG3DMaterialLaw;
%nodefaultctor dG3DMultiscaleMaterialLawBase;
%nodefaultctor Cohesive3DLaw;
%nodefaultctor BaseCohesive3DLaw;
%nodefaultctor BaseTransverseIsotropicLinearCohesive3D;
%nodefaultctor GeneralBulkFollwedCohesive3DLaw;
%nodefaultctor GeneralMultiscaleBulkFollwedCohesive3DLaw;
%nodefaultctor fractureBy2Laws<dG3DMaterialLaw,Cohesive3DLaw>;
%nodefaultctor ExtraDofDiffusionDG3DDomain;
%nodefaultctor ExtraDofDiffusionInterDomainBetween3D;

%template(fractureBy2LawsDG3DMaterialLawCohesive3DLaw) fractureBy2Laws<dG3DMaterialLaw,Cohesive3DLaw>;
%include "dG3DMaterialLaw.h"
%include "nonLocalDamageDG3DMaterialLaw.h"
%include "dG3DCohesiveMaterialLaw.h"
%include "dG3DMultiscaleCohesiveMaterialLaw.h"
%include "FractureCohesiveDG3DMaterialLaw.h"
%include "dG3DDomain.h"
%include "dG3DContactDomain.h"
%include "dG3DCohesiveBandMaterialLaw.h"
%include "dG3DMultiscaleMaterialLaw.h"
%include "hoDGPartDomain.h"
%include "hoDGMaterialLaw.h"
%include "NonLocalDamageHyperelasticDG3DMaterialLaw.h"
%include "axisymmetricDG3DDomain.h"
%include "dG3D1DDomain.h"
%include "computeWithMaterialLaw.h"
%include "dG3DDeepMaterialNetworks.h"
