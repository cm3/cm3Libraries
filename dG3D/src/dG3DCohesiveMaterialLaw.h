//
//
// Description: Class of materials for non linear dg
//
//
// Author:  <L. Noels>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef _DG3DCOHESIVEMATERIALLAW_H_
#define _DG3DCOHESIVEMATERIALLAW_H_

#include "mlaw.h"
#include "mlawCohesive.h"
#include "dG3DCohesiveIPVariable.h"


class Cohesive3DLaw : public materialLaw{

#ifndef SWIG
 public :
  Cohesive3DLaw(const int num, const bool init=true);
  Cohesive3DLaw(const Cohesive3DLaw &source);
  virtual ~Cohesive3DLaw(){}
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw)=0;
  virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const=0;
  virtual void createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const=0;
  // get operation
  virtual double getKp() const {return 0.;};
  virtual double getKn() const {return 0.;};
  virtual double getGc() const{return 0.;}
  virtual double getSigmac() const{return 0.;}
  virtual double getBeta() const{return 1.;}
  virtual double getMu() const{return 0.;}
  virtual double getFractureStrengthFactorMin() const{return 1.;}
  virtual double getFractureStrengthFactorMax() const{return 1.;}
  virtual double getViscosity() const {return 0.;}
  virtual void setViscosity(double vis) {}

  // onto this form to compute easily matrix by perturbation
  virtual matname getType() const{return materialLaw::cohesiveLaw;}
  virtual double soundSpeed() const
  {
    Msg::Warning("Use a cohesive law to determine sound speed ?? value =1");
    return 1;
  }
  // default for cohesive law full broken if delta > delta_c
  virtual bool fullBroken(const Cohesive3DIPVariableBase *ipv) const
  {
    return ipv->isDeleted();
  }
  virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent =false)=0;
  // check cohesive insert , if forcedInsert = true, the cohesive element is inserted at any condition
  virtual void checkCohesiveInsertion(IPStateBase* ipsm, IPStateBase* ipsp, const bool forcedInsert=false) const = 0;
  // set function allows to prescribe the deformation gradient at bulk ipv equal to mean F
  virtual void transferInterfaceDataToBulk(IPVariable* ipv, const IPVariable* ipvprev,  const bool stiff) const = 0;
  virtual double getCharacteristicLength() const {return 1.;};
	virtual materialLaw* clone() const = 0;
  virtual bool withEnergyDissipation() const {return true;}
  virtual bool brokenCheck(const IPVariable* ipv) const { return false;} // alway false
  virtual void constitutive(const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPVariable *q0,       // array of initial internal variable
                            IPVariable *q1,              // updated array of internal variable (in ipvcur on output),
			    STensor43 &Tangent,         // constitutive tangents (output)
			    const bool stiff,
                            STensor43* elasticTangent = NULL,
                            const bool dTangent =false,
                            STensor63* dCalgdeps =NULL
			    ) const
      {
        Msg::Error("Cohesive3DLaw constitutive not defined");
      }
#endif
};

class BaseCohesive3DLaw : public Cohesive3DLaw
{
 #ifndef SWIG
 protected:
  const double _Kp; // penalty parameter
  const double _fscmin,_fscmax; // Boundary value for uncertainty on sigma_c (default min = max = 1)
  double _viscosity; //for stabilisation
  CohesiveLawBase* _cohesiveLaw;
  #endif // SWIG

 public:
#ifndef SWIG
  BaseCohesive3DLaw(const int num, const double fractureStrengthFactorMin = 1., const double fractureStrengthFactorMax = 1.,
                         const double Kp= 1e6);
  BaseCohesive3DLaw(const BaseCohesive3DLaw &source);
  virtual ~BaseCohesive3DLaw();

	virtual void setMacroSolver(nonLinearMechSolver* sv){
		Cohesive3DLaw::setMacroSolver(sv);
		_cohesiveLaw->setMacroSolver(sv);
	};
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){} // nothing to do for this law

  virtual double getKp() const {return _Kp;};
  virtual double getKn() const {return _Kp;};
  virtual double getFractureStrengthFactorMin() const{return _fscmin;}
  virtual double getFractureStrengthFactorMax() const{return _fscmax;}
  virtual double getViscosity() const {return _viscosity;}


  virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent =false);
  //virtual void checkCohesiveInsertion(IPStateBase* ipsm, IPStateBase* ipsp, const bool forcedInsert=false) const;
  virtual void transferInterfaceDataToBulk(IPVariable* ipv, const IPVariable* ipvprev,  const bool stiff) const {}
  //virtual materialLaw* clone() const{return new BaseCohesive3DLaw(*this);}
#endif
};

class ElasticCohesive3DLaw : public BaseCohesive3DLaw
{
 #ifndef SWIG
 protected:
  // All cohesive law must have at least these four variable
  const double _beta;  
  const double _Kn; // normal penalty
 #endif // SWIG

 public:
  ElasticCohesive3DLaw(const int num, const cohesiveElasticPotential& poten, const double beta, const double Kn, const double Kp);
  #ifndef SWIG
  ElasticCohesive3DLaw(const ElasticCohesive3DLaw &source);
  virtual ~ElasticCohesive3DLaw();
  virtual double getKn() const {return _Kn;};
  virtual double getBeta() const{return _beta;}
  virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;

  virtual void checkCohesiveInsertion(IPStateBase* ipsm, IPStateBase* ipsp, const bool forcedInsert=false) const{};
  virtual materialLaw* clone() const{return new ElasticCohesive3DLaw(*this);}
  virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing
#endif
};


class LinearCohesive3DLaw : public BaseCohesive3DLaw
{
 #ifndef SWIG
 protected:
  // All cohesive law must have at least these four variable
  const double _Gc;
  const double _sigmac;
  const double _beta;
  const double _mu;
  #endif // SWIG

 public:
  LinearCohesive3DLaw(const int num, const double Gc, const double sigmac, const double beta, const double mu,
                         const double fractureStrengthFactorMin = 1., const double fractureStrengthFactorMax = 1.,
                         const double Kp= 1e6);

  LinearCohesive3DLaw(const int num, const cohesiveElasticPotential& poten, const cohesiveDamageLaw& dam, const double Gc, const double sigmac, const double beta, const double mu,
                         const double fractureStrengthFactorMin = 1., const double fractureStrengthFactorMax = 1.,
                         const double Kp= 1e6);
  virtual void setViscosity(double vis) {_viscosity=vis;}
#ifndef SWIG
  LinearCohesive3DLaw(const LinearCohesive3DLaw &source);
  virtual ~LinearCohesive3DLaw();

  virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;

  virtual double getGc() const{return _Gc;}
  virtual double getSigmac() const{return _sigmac;}
  virtual double getBeta() const{return _beta;}
  virtual double getMu() const{return _mu;}

  //virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff=true, const bool checkfrac=true);
  virtual void checkCohesiveInsertion(IPStateBase* ipsm, IPStateBase* ipsp, const bool forcedInsert=false) const;
  //virtual void transferInterfaceDataToBulk(IPVariable* ipv, const IPVariable* ipvprev,  const bool stiff) const {}
  virtual materialLaw* clone() const{return new LinearCohesive3DLaw(*this);}
  virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing
#endif
};


class BaseTransverseIsotropicLinearCohesive3D : public BaseCohesive3DLaw
{
 #ifndef SWIG
 protected:
  // All cohesive law must have at least these four variable
  const double _GcL;
  const double _sigmacL;
  const double _GcT;
  const double _sigmacT;
  const double _beta;
  const double _mu;
  #endif // SWIG

 public:
#ifndef SWIG
  BaseTransverseIsotropicLinearCohesive3D(const int num, const double GcL, const double sigmaL, const double GcT, const double sigmaT, const double beta, const double mu,
                         const double fractureStrengthFactorMin = 1., const double fractureStrengthFactorMax = 1.,
                         const double Kp= 1e6);
  BaseTransverseIsotropicLinearCohesive3D(const BaseTransverseIsotropicLinearCohesive3D &source);
  virtual ~BaseTransverseIsotropicLinearCohesive3D();

  //virtual void createIPState(IPStateBase* &ips,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  //virtual void createIPVariable(IPVariable* &ipv,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;

  virtual double getGc() const{Msg::Error("getGc not defined in BaseTransverseIsotropicLinearCohesive3D");return 0.;}
  virtual double getSigmac() const{Msg::Error("getSigmac not defined in BaseTransverseIsotropicLinearCohesive3D");return 0.;}
  virtual double getGcL() const{return _GcL;}
  virtual double getSigmacL() const{return _sigmacL;}
  virtual double getGcT() const{return _GcT;}
  virtual double getSigmacT() const{return _sigmacT;}
  virtual double getBeta() const{return _beta;}
  virtual double getMu() const{return _mu;}

  //virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff=true, const bool checkfrac=true);
  virtual void checkCohesiveInsertion(IPStateBase* ipsm, IPStateBase* ipsp, const bool forcedInsert=false) const;
  //virtual void transferInterfaceDataToBulk(IPVariable* ipv, const IPVariable* ipvprev,  const bool stiff) const {}
  //virtual materialLaw* clone() const{return new BaseTransverseIsotropicLinearCohesive3D(*this);}
#endif
};

class TransverseIsotropicLinearCohesive3D : public BaseTransverseIsotropicLinearCohesive3D
{
 #ifndef SWIG
 protected:
  #endif // SWIG

 public:
  TransverseIsotropicLinearCohesive3D(const int num, const double GcL, const double sigmaL, const double GcT, const double sigmaT, const double beta, const double mu,
                         const double Ax, const double Ay, const double Az,
                         const double fractureStrengthFactorMin = 1., const double fractureStrengthFactorMax = 1.,
                         const double Kp= 1e6);
  virtual void setViscosity(double vis) {_viscosity=vis;}
#ifndef SWIG
  TransverseIsotropicLinearCohesive3D(const TransverseIsotropicLinearCohesive3D &source);
  virtual ~TransverseIsotropicLinearCohesive3D();

  virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;


  //virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff=true, const bool checkfrac=true);
  //virtual void checkCohesiveInsertion(IPStateBase* ipsm, IPStateBase* ipsp, const bool forcedInsert=false) const;
  //virtual void transferInterfaceDataToBulk(IPVariable* ipv, const IPVariable* ipvprev,  const bool stiff) const {}
  virtual materialLaw* clone() const{return new TransverseIsotropicLinearCohesive3D(*this);}
  virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing
#endif
};

class TransverseIsoCurvatureLinearCohesive3D : public BaseTransverseIsotropicLinearCohesive3D
{
 #ifndef SWIG
 protected:
  #endif // SWIG

 public:
  TransverseIsoCurvatureLinearCohesive3D(const int num, const double GcL, const double sigmaL, const double GcT, const double sigmaT, const double beta, const double mu,
                          const double Centroid_x, const double Centroid_y, const double Centroid_z,
                   const double PointStart1_x, const double PointStart1_y, const double PointStart1_z,
                   const double PointStart2_x,  const double PointStart2_y, const double PointStart2_z, const double init_Angle,
                         const double fractureStrengthFactorMin = 1., const double fractureStrengthFactorMax = 1.,
                         const double Kp= 1e6);
  virtual void setViscosity(double vis) {_viscosity=vis;}
#ifndef SWIG
  TransverseIsoCurvatureLinearCohesive3D(const TransverseIsoCurvatureLinearCohesive3D &source);
  virtual ~TransverseIsoCurvatureLinearCohesive3D();

  virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;


  //virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff=true, const bool checkfrac=true);
  //virtual void checkCohesiveInsertion(IPStateBase* ipsm, IPStateBase* ipsp, const bool forcedInsert=false) const;
  //virtual void transferInterfaceDataToBulk(IPVariable* ipv, const IPVariable* ipvprev,  const bool stiff) const {}
  virtual materialLaw* clone() const{return new TransverseIsoCurvatureLinearCohesive3D(*this);}
  virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing
#endif
};

class NonLocalDamageLinearCohesive3DLaw : public LinearCohesive3DLaw
{
 protected:
  const double _Dc;
	bool _damageBased; // true if using damage as criterion, false if using effective stress
 public:
  NonLocalDamageLinearCohesive3DLaw(const int num, const double Gc, const double sigmac, const double Dc, const double beta,
                         const double fractureStrengthFactorMin = 1., const double fractureStrengthFactorMax = 1., const double Kp = 1e6,
												 const bool damageCheck = false);

  NonLocalDamageLinearCohesive3DLaw(const int num, const cohesiveElasticPotential& poten, const cohesiveDamageLaw& dam, const double Gc, const double sigmac, const double Dc, const double beta,
                         const double fractureStrengthFactorMin = 1., const double fractureStrengthFactorMax = 1., const double Kp = 1e6,
												 const bool damageCheck = false);

#ifndef SWIG
  NonLocalDamageLinearCohesive3DLaw(const NonLocalDamageLinearCohesive3DLaw &source);
  virtual ~NonLocalDamageLinearCohesive3DLaw(){};

  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){} // nothing to do for this law
  virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;

  virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent =false);
  virtual void checkCohesiveInsertion(IPStateBase* ipsm, IPStateBase* ipsp, const bool forcedInsert=false) const;

	virtual materialLaw* clone() const{ return new NonLocalDamageLinearCohesive3DLaw(*this);};
	virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing

#endif
};


class NonLocalDamageTransverseIsotropicLinearCohesive3DLaw : public BaseTransverseIsotropicLinearCohesive3D
{
 protected:
  const double _DcL;
  const double _DcT;
  bool _damageBased; // true if using damage as criterion, false if using effective stress
 public:
  NonLocalDamageTransverseIsotropicLinearCohesive3DLaw(const int num, const double GcL, const double sigmacL, const double DcL, const double GcT, const double sigmacT, const double DcT, const double beta,
                         const double mu, const double Ax, const double Ay, const double Az, const double fractureStrengthFactorMin = 1., const double fractureStrengthFactorMax = 1., 
                         const double Kp = 1e6, const bool damageCheck = false);
  virtual void setViscosity(double vis) {_viscosity=vis;}

#ifndef SWIG
  NonLocalDamageTransverseIsotropicLinearCohesive3DLaw(const NonLocalDamageTransverseIsotropicLinearCohesive3DLaw &source);
  virtual ~NonLocalDamageTransverseIsotropicLinearCohesive3DLaw(){};

  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){} // nothing to do for this law
  virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;

  virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff=true, const bool checkfrac=true);
  virtual void checkCohesiveInsertion(IPStateBase* ipsm, IPStateBase* ipsp, const bool forcedInsert=false) const;

  virtual materialLaw* clone() const{ return new NonLocalDamageTransverseIsotropicLinearCohesive3DLaw(*this);};
  virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing

#endif
};



class DelaminationLinearCohesive3DLaw : public LinearCohesive3DLaw
{
  protected:
    double mixedModeSigmaRatio;
    double mixedModeGcRatio;
    double alphaGc;
    double xiI, xiII; // power law in case of exponential damage law
  public:
    double getGcII() const{return getGc()*mixedModeGcRatio;}
    double getSigmacII() const{return getSigmac()*mixedModeSigmaRatio;}
    double getAlphaGc() const{return alphaGc;}
    DelaminationLinearCohesive3DLaw(const int num, const double GcI, const double GcII, const double sigmacI,const double sigmacII, const double alphaGc,
                  const double fractureStrengthFactorMin = 1., const double fractureStrengthFactorMax = 1.,
                  const double Kp = 1e6);
    DelaminationLinearCohesive3DLaw(const int num, const cohesiveElasticPotential& poten, const cohesiveDamageLaw& dam, const double GcI, const double GcII, const double sigmacI,const double sigmacII, const double alphaGc,
                  const double fractureStrengthFactorMin = 1., const double fractureStrengthFactorMax = 1.,
                  const double Kp = 1e6);
  virtual void setViscosity(double vis) {_viscosity=vis;}
  virtual void setPowerParameter(double cI, double cII)
  {
    xiI = cI;
    xiII = cII;
  }
#ifndef SWIG
    DelaminationLinearCohesive3DLaw(const DelaminationLinearCohesive3DLaw &source);
    virtual ~DelaminationLinearCohesive3DLaw(){}
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){} // nothing to do for this law
    virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
    virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent =false);
    virtual void checkCohesiveInsertion(IPStateBase* ipsm, IPStateBase* ipsp, const bool forcedInsert=false) const;
		virtual materialLaw* clone() const{ return new DelaminationLinearCohesive3DLaw(*this);};
		virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing
#endif
};

class GeneralBulkFollwedCohesive3DLaw : public Cohesive3DLaw{
  protected:
    double _L; // RVE length perpendicular to the localization band
		double _lostSolutionUniquenssTolerance;
		double _Kp; // penalty to avoid penetration

	public:
    #ifndef SWIG
    GeneralBulkFollwedCohesive3DLaw(const int num, const bool init = true);
    GeneralBulkFollwedCohesive3DLaw(const GeneralBulkFollwedCohesive3DLaw& src);
    virtual ~GeneralBulkFollwedCohesive3DLaw(){}
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){} // nothing to do for this law
    virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const = 0;
    virtual void createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const = 0;
    virtual double getCharacteristicLength() const {return _L;};
    virtual double getKp() const {return _Kp;};
    virtual double getKn() const {return _Kp;};

    virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent =false) = 0;
    virtual void checkCohesiveInsertion(IPStateBase* ipsm, IPStateBase* ipsp, const bool forcedInsert= false) const = 0;
    virtual void transferInterfaceDataToBulk(IPVariable* ipv, const IPVariable* ipvprev,  const bool stiff) const = 0;
		virtual materialLaw* clone() const = 0;
		double getLostSolutionUniquenssTolerance() const {return _lostSolutionUniquenssTolerance;};
    #endif // SWIG

    void setCharacteristicLength(const double L) {_L= L;}
		void setLostSolutionUniquenssTolerance(const double tol);
		void setPenaltyParameter(const double K) {_Kp = K;};
};

class BulkFollwedCohesive3DLaw : public GeneralBulkFollwedCohesive3DLaw{
  public:
    BulkFollwedCohesive3DLaw(const int num);
    #ifndef SWIG
    BulkFollwedCohesive3DLaw(const BulkFollwedCohesive3DLaw& src): GeneralBulkFollwedCohesive3DLaw(src){};
    virtual ~BulkFollwedCohesive3DLaw(){};
    virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
    virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent =false);
    virtual void checkCohesiveInsertion(IPStateBase* ipsm, IPStateBase* ipsp, const bool forcedInsert= false) const;
    virtual void transferInterfaceDataToBulk(IPVariable* ipv, const IPVariable* ipvprev,  const bool stiff) const;
    virtual materialLaw* clone() const{return new BulkFollwedCohesive3DLaw(*this);};
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing
    #endif // SWIG
};

#endif //_DG3DCOHESIVEMATERIALLAW_H_
