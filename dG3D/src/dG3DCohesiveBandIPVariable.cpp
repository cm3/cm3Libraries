
#include "dG3DCohesiveBandIPVariable.h"
#include "dG3DIPVariable.h"

/* Constructors & Destructors */
CohesiveBand3DIPVariable::CohesiveBand3DIPVariable(const bool wjg): BulkFollowedCohesive3DIPVariable(),
    _sigmac(0.),_deltaMax_n(0.),_deltaMax_t(0.),_deltaMax_s(0.),_delta0(0.),
    _cohesiveNormal(0.), _cohesiveTangent(0.), _cohesiveBiTangent(0.),
    _cohesiveRefNormal(0.), _cohesiveRefTangent(0.), _cohesiveRefBiTangent(0.),
    _Gradjump(0.),_Gradjump0(0.),_DInterfaceForceDjumpGradient(0.),
    _dFband_dGradjump(0.), _withJumpGradient(wjg),
    _DirreversibleEnergy_dGradjump(0.){};

CohesiveBand3DIPVariable::CohesiveBand3DIPVariable(const CohesiveBand3DIPVariable& src): BulkFollowedCohesive3DIPVariable(src),
    _sigmac(src._sigmac), _deltaMax_n(src._deltaMax_n),_deltaMax_t(src._deltaMax_t),_deltaMax_s(src._deltaMax_s),_delta0(src._delta0),
		_cohesiveNormal(src._cohesiveNormal), _cohesiveTangent(src._cohesiveTangent), _cohesiveBiTangent(src._cohesiveBiTangent),
    _cohesiveRefNormal(src._cohesiveRefNormal), _cohesiveRefTangent(src._cohesiveRefTangent), _cohesiveRefBiTangent(src._cohesiveRefBiTangent),
    _Gradjump(src._Gradjump), _Gradjump0(src._Gradjump0),  _withJumpGradient(src._withJumpGradient),
    _DInterfaceForceDjumpGradient(src._DInterfaceForceDjumpGradient),
    _dFband_dGradjump(src._dFband_dGradjump),
    _DirreversibleEnergy_dGradjump(src._DirreversibleEnergy_dGradjump) {};

CohesiveBand3DIPVariable& CohesiveBand3DIPVariable::operator=(const IPVariable& src){
    BulkFollowedCohesive3DIPVariable::operator=(src);
    const CohesiveBand3DIPVariable* psrc = dynamic_cast<const CohesiveBand3DIPVariable*>(&src);
    if (psrc != NULL)
    {
        _sigmac = psrc->_sigmac;
        _deltaMax_n = psrc->_deltaMax_n;
        _deltaMax_t = psrc->_deltaMax_t;
        _deltaMax_s = psrc->_deltaMax_s;
        _delta0 = psrc->_delta0;
        _cohesiveNormal = psrc->_cohesiveNormal;
        _cohesiveTangent = psrc->_cohesiveTangent;
        _cohesiveBiTangent = psrc->_cohesiveBiTangent;
        _cohesiveRefNormal = psrc->_cohesiveRefNormal;
        _cohesiveRefTangent = psrc->_cohesiveRefTangent;
        _cohesiveRefBiTangent = psrc->_cohesiveRefBiTangent;
        _withJumpGradient = psrc->_withJumpGradient;
        _Gradjump = psrc->_Gradjump;
        _Gradjump0 = psrc->_Gradjump0;
        _DInterfaceForceDjumpGradient = psrc->_DInterfaceForceDjumpGradient;
        _dFband_dGradjump = psrc->_dFband_dGradjump;
        _DirreversibleEnergy_dGradjump = psrc->_DirreversibleEnergy_dGradjump;
    }
    return *this;
};

CohesiveBand3DIPVariable::~CohesiveBand3DIPVariable(){};

/* Specific functions */
void CohesiveBand3DIPVariable::initializeFracture(const SVector3 &ujump_, const double Kp,
                            const double seff, const double snor, const double tau, const double Gc,
                            const double beta_, const bool ift, const STensor3 &cauchy, const double K, const SVector3& norm, const SVector3& f0,
                            const IPVariable* bulkIPv)
{
  BulkFollowedCohesive3DIPVariable::initializeFracture(ujump_,Kp,seff,snor,tau,Gc,beta_,ift,cauchy,K,norm,f0,bulkIPv);
  _sigmac  = seff;
  _delta0 = 1.0e-6;      // to be modfied and compute !
  _deltaMax_n = _delta0;
  _deltaMax_t = _delta0;
  _deltaMax_s = _delta0;
  _Gradjump0 = _Gradjump;
};


/* Restart function */
void CohesiveBand3DIPVariable::restart(){
  BulkFollowedCohesive3DIPVariable::restart();
  restartManager::restart(_sigmac);
  restartManager::restart(_deltaMax_n);
  restartManager::restart(_deltaMax_t);
  restartManager::restart(_deltaMax_s);
  restartManager::restart(_delta0);
  restartManager::restart(_cohesiveNormal);
  restartManager::restart(_cohesiveTangent);
  restartManager::restart(_cohesiveBiTangent);
  restartManager::restart(_cohesiveRefNormal);
  restartManager::restart(_cohesiveRefTangent);
  restartManager::restart(_cohesiveRefBiTangent);
  restartManager::restart(_Gradjump);
  restartManager::restart(_Gradjump0);
  restartManager::restart(_withJumpGradient);
  restartManager::restart(_DInterfaceForceDjumpGradient);
  restartManager::restart(_dFband_dGradjump);
  restartManager::restart(_DirreversibleEnergy_dGradjump);
};
