//
// C++ Interface: domain
//
// Description: Class with definition of finite strain domain for finite strain problem
//
//
// Author: V.D. NGUYEN 2011
//
// Copyright: See COPYING file that comes with this distribution
//
#ifndef HODGPARTDOMAIN_H_
#define HODGPARTDOMAIN_H_

#include "dG3DDomain.h"

class hoDGDomain : public dG3DDomain{
  public:
    hoDGDomain(const int tag, const int phys, const int sp, const int lnum, const int fdg, const int dim=3);
    #ifndef SWIG
    hoDGDomain(const hoDGDomain &source);
    virtual ~hoDGDomain(){};
    virtual void computeStrain(MElement *e, const int npts_bulk, IntPt *GP,
                               AllIPState::ipstateElementContainer *vips,
                               IPStateBase::whichState ws, fullVector<double> &disp,
                               bool useBarF) const;

    virtual void computeStrain(AllIPState *aips,MInterfaceElement *ie, IntPt *GP,const IPStateBase::whichState ws,
                                        partDomain* efMinus, partDomain *efPlus, fullVector<double> &dispm,
                                        fullVector<double> &dispp,  const bool virt,
                                        bool useBarF);

    virtual void createTerms(unknownField *uf,IPField*ip);

    virtual void allocateInterfaceMPI(const int rankOtherPart, const elementGroup &groupOtherPart,dgPartDomain** dgdom);

    #endif //SWIG
};

class hoDGInterDomain : public hoDGDomain, public interDomainBase{
  public:
  hoDGInterDomain(const int tag, partDomain *dom1, partDomain *dom2,  const int lnum=0, const int bnum=0);
  #ifndef SWIG
  hoDGInterDomain(const hoDGInterDomain &source);
  virtual ~hoDGInterDomain(){}
  #endif
  virtual void stabilityParameters(const double b1=10.);
  virtual void matrixByPerturbation(const int iinter, const double eps=1e-8);
  #ifndef SWIG

  virtual void setMaterialLaw(const std::map<int,materialLaw*> &maplaw);
  // FOLLOWING FUNCTION SHOULD STAY HERE BECAUSE OF THE DOUBLE DERIVATION
  virtual materialLaw* getMaterialLaw(){return _mlaw;}
  virtual const materialLaw* getMaterialLaw() const{return _mlaw;}
  virtual materialLaw* getMaterialLawMinus(){return _mlawMinus;}
  virtual const materialLaw* getMaterialLawMinus() const{return _mlawMinus;}
  virtual materialLaw* getMaterialLawPlus(){return _mlawPlus;}
  virtual const materialLaw* getMaterialLawPlus() const{return _mlawPlus;}
  virtual int getLawNum() const{return _lnum;}
  // FOLLOWING FUNCTION SHOULD STAY HERE BECAUSE OF THE DOUBLE DERIVATION
  virtual int getMinusLawNum() const{return _domMinus->getLawNum();}
  virtual int getPlusLawNum() const{return _domPlus->getLawNum();}
  virtual const partDomain* getMinusDomain() const{return _domMinus;}
  virtual const partDomain* getPlusDomain() const{return _domPlus;}
  virtual partDomain* getMinusDomain(){return _domMinus;}
  virtual partDomain* getPlusDomain() {return _domPlus;}
  virtual void createTerms(unknownField *uf,IPField*ip);
  // as there is no elerment on a interDomain empty function. The interface element on an interDomain
  // has to be created via its two domains.
  virtual void createInterface(manageInterface &maninter){}
  #endif
};



#endif // HODGPARTDOMAIN_H_
