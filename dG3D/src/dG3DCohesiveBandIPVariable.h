#ifndef DG3DIPVARIABLETRIAXIALITY_H_
#define DG3DIPVARIABLETRIAXIALITY_H_

#include "dG3DCohesiveIPVariable.h"
#include "ipField.h"

class CohesiveBand3DIPVariable : public BulkFollowedCohesive3DIPVariable{
  protected:
    double _sigmac;      // critial stress -> cohesive elements are inserted if the stress larger than this value
    double _deltaMax_n;  // maximal jump values reached in local cohesive basis (n,t,s)
    double _deltaMax_t;
    double _deltaMax_s;
    double _delta0;      // off-set to avoid infinite slope at insertion (ie. negative value of jump corresponding to zero-traction)

    // Interface vector basis
    // - Basis in current config
    SVector3 _cohesiveNormal;
    SVector3 _cohesiveTangent;      // cohesive tangent
    SVector3 _cohesiveBiTangent;    // cohesive bitangent
    // - Basis in reference config
    SVector3 _cohesiveRefNormal;    // cohesive normal in reference config
    SVector3 _cohesiveRefTangent;   // cohesive tangent
    SVector3 _cohesiveRefBiTangent; // cohesive bitangent

    // Deformation values - enhanced components
    bool _withJumpGradient; // true if jum gradient is considered
    STensor3 _Gradjump;    // idem at insertion time
    STensor3 _Gradjump0; // spatial derivatives of jump along interfaces at insertion time due to DG formulation
    
    STensor33 _DInterfaceForceDjumpGradient;

    // Kinematic tangent tensors
    STensor43 _dFband_dGradjump;    // partial band deformation gradient on partial jump gradient

    // Path following only
    STensor3 _DirreversibleEnergy_dGradjump;  // partial dissipated energy on partial jump gradient

  public:
    // Constructors & Destructors
    CohesiveBand3DIPVariable(const bool wjg);
    CohesiveBand3DIPVariable(const CohesiveBand3DIPVariable& src);
    virtual CohesiveBand3DIPVariable& operator=(const IPVariable& src);
    virtual ~CohesiveBand3DIPVariable();
    
    virtual bool withJumpGradient() const {return _withJumpGradient;}; // with jump gradient

    // normal direction
    virtual const double& getConstRefToNormalMaxOpening() const {return _deltaMax_n;};
    virtual double& getRefToNormalMaxOpening() {return _deltaMax_n;};
            // tangent direction
    virtual const double& getConstRefToTangentMaxOpening() const {return _deltaMax_t;};
    virtual double& getRefToTangentMaxOpening() {return _deltaMax_t;};
            // bitangent direction
    virtual const double& getConstRefToBiTangentMaxOpening() const {return _deltaMax_s;};
    virtual double& getRefToBiTangentMaxOpening() {return _deltaMax_s;};
        // off-set for crack opening
    virtual const double& getConstRefToOpeningOffset() const {return _delta0;};
    virtual double& getRefToOpeningOffset() {return _delta0;};

        // allows use of local basis at cohesive interface
    virtual bool useLocalBasis() const {return true;};
        // cohesive basis - in current config
    virtual const SVector3& getConstRefToCohesiveNormal() const {return _cohesiveNormal;};
		virtual SVector3& getRefToCohesiveNormal() {return _cohesiveNormal;};
    
    virtual const SVector3& getConstRefToCohesiveTangent() const {return _cohesiveTangent;};
    virtual SVector3& getRefToCohesiveTangent() {return _cohesiveTangent;};
    virtual const SVector3& getConstRefToCohesiveBiTangent() const {return _cohesiveBiTangent;};
    virtual SVector3& getRefToCohesiveBiTangent() {return _cohesiveBiTangent;};
        // cohesive normal - in reference config
    virtual const SVector3& getConstRefToCohesiveReferenceNormal() const {return _cohesiveRefNormal;};
    virtual SVector3& getRefToCohesiveReferenceNormal() {return _cohesiveRefNormal;};
    virtual const SVector3& getConstRefToCohesiveReferenceTangent() const {return _cohesiveRefTangent;};
    virtual SVector3& getRefToCohesiveReferenceTangent() {return _cohesiveRefTangent;};
    virtual const SVector3& getConstRefToCohesiveReferenceBiTangent() const {return _cohesiveRefBiTangent;};
    virtual SVector3& getRefToCohesiveReferenceBiTangent() {return _cohesiveRefBiTangent;};

  // Grad jump
   //interface depends on jump gradient
		virtual const STensor33 &getConstRefToDInterfaceForceDjumpGradient() const {return _DInterfaceForceDjumpGradient;};
		virtual STensor33 &getRefToDInterfaceForceDjumpGradient() {return _DInterfaceForceDjumpGradient;};

            // spatial gradient of jump at insertion time due to DG formulation
    virtual const STensor3& getConstRefToJumpGradient() const {return _Gradjump;};
    virtual STensor3& getRefToJumpGradient() {return _Gradjump;};;
    
    virtual const STensor3& getConstRefToJumpGradientAtFailureOnset() const {return _Gradjump0;};
    virtual STensor3& getRefToJumpGradientAtFailureOnset() {return _Gradjump0;};

            // partial band F / partial grad jump
    virtual const STensor43& getConstRefToDInterfaceDeformationGradientDJumpGradient() const {return _dFband_dGradjump;};
    virtual STensor43& getRefToDInterfaceDeformationGradientDJumpGradient() {return _dFband_dGradjump;};

    virtual STensor3& getRefToDIrreversibleEnergyDJumpGradient() {return _DirreversibleEnergy_dGradjump;};
    virtual const STensor3& getConstRefToDIrreversibleEnergyDJumpGradient() const {return _DirreversibleEnergy_dGradjump;};

	    // Initial parameters of cohesive ip at insertion time
    virtual void initializeFracture(const SVector3 &ujump_, const double Kp,
                            const double seff, const double snor, const double tau, const double Gc,
                            const double beta_, const bool ift, const STensor3 & cauchy,
                            const double K,           // penalty
                            const SVector3& normal,  // normal
                            const SVector3& f0,         // interface force at insertion time
                            const IPVariable* bulkIPv // bulk data at insertion tim
                            );

    virtual IPVariable* clone() const {return new CohesiveBand3DIPVariable(*this);}
    virtual void restart();
};


#endif // DG3DIPVARIABLETRIAXIALITY_H_
