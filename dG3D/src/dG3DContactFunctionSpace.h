//
// Description: Functional space for rigid contact
//
//
// Author:  <Van Dung NGUYEN>, (C) 2015
//
// Copyright: See COPYING file that comes with this distribution
//


#ifndef DG3DCONTACTFUNCTIONSPACE_H_
#define DG3DCONTACTFUNCTIONSPACE_H_

#include "contactFunctionSpace.h"
#include "dG3DDof3IntType.h"
#include "contactElement.h"

/*
// note in case of non-local or extra Dof, the contact is considered with 3 disp only
*/

class dG3DRigidContactSpace : public rigidContactSpaceBase{
 public:
  dG3DRigidContactSpace(const int id, FunctionSpace<double> *sp, MVertex *ver) : rigidContactSpaceBase(id,sp,ver){}
  dG3DRigidContactSpace(const int id, FunctionSpace<double> *sp, MVertex *ver, const int comp1,
                   const int comp2 = -1, const int comp3 =-1) : rigidContactSpaceBase(id,sp,ver,comp1,comp2,comp3){}
  virtual ~dG3DRigidContactSpace(){}
  virtual void getKeys(MElement *ele,std::vector<Dof> &keys) const{
    if(ele->getDim() > 0){ // if dim 0 return the key of gravity center only !!
      getConstRefToSpaceSlave()->getKeys(ele,keys);
    }
    for(int i=0;i<getConstRefToComp().size();i++){
      keys.push_back(Dof(_gc->getNum(),dG3DDof3IntType::createTypeWithThreeInts(getConstRefToComp()[i],getId())));
    }
  }
  virtual int getNumKeys(MElement *ele) const{
    int nkeysele = getConstRefToSpaceSlave()->getNumKeys(ele);
    return nkeysele + getConstRefToComp().size();
  }
  virtual void getKeysOfGravityCenter(std::vector<Dof> &keys) const{
    for(int i=0;i<getConstRefToComp().size(); i++)
      keys.push_back(Dof(_gc->getNum(),dG3DDof3IntType::createTypeWithThreeInts(getConstRefToComp()[i],getId())));
  }
  virtual int getNumKeysOfGravityCenter() const{
    return getConstRefToComp().size();
  }
  virtual void getKeys(MElement *ele, const int ind, std::vector<Dof> &keys) const{
    // generate keys of element and select the good ones after LAGRANGE OK ?? CHANGE THIS HOW TODO
    std::vector<Dof> tkeys;
    this->getKeys(ele,tkeys);
    int nkeys = this->getNumKeys(ele);
    int ncomp = getConstRefToComp().size();
    int nbver = ele->getNumVertices();
    for(int i=0;i<ncomp; i++)
      keys.push_back(tkeys[ind+i*nbver]);
    this->getKeysOfGravityCenter(keys);
  }
  virtual int getNumKeys(MElement *ele, int ind) const{
    return 2*getConstRefToComp().size();
  }
};

class dG3DDefoDefoContactSpace : public defoDefoContactSpaceBase{
 public:
  dG3DDefoDefoContactSpace(const int id, FunctionSpace<double> *spSlave, FunctionSpaceBase *spMaster) : defoDefoContactSpaceBase(id,spSlave,spMaster){}
  dG3DDefoDefoContactSpace(const int id, FunctionSpace<double> *spSlave, FunctionSpaceBase *spMaster, const int comp1,
                   const int comp2 = -1, const int comp3 =-1) : defoDefoContactSpaceBase(id,spSlave,spMaster,comp1,comp2,comp3){}
  virtual ~dG3DDefoDefoContactSpace(){}
  virtual void getKeys(contactElement *cE, std::vector<Dof> &keys) {
    MElement *eleSlave = cE->getRefToElementSlave();
    MElement *eleMaster= cE->getRefToElementMaster();
    std::vector<Dof> keys1;
    getConstRefToSpaceSlave()->getKeys(eleSlave,keys1);
     for(std::vector<Dof>::const_iterator it1=keys1.begin(); it1!=keys1.end(); it1++)
      keys.push_back(*it1);
    std::vector<Dof> keys2;
    getConstRefToSpaceMaster()->getKeys(eleMaster,keys2);
    for(std::vector<Dof>::const_iterator it2=keys2.begin(); it2!=keys2.end(); it2++)
      keys.push_back(*it2);
  }
  //virtual int getNumKeys(MElement *eleSlave, MElement *eleMaster) {
  //  int nkeyseleSlave = getRefToSpaceSlave()->getNumKeys(eleSlave);
  //  int nkeyseleMaster = getRefToSpaceMaster()->getNumKeys(eleMaster);
  //  return nkeyseleSlave + nkeyseleMaster;
  //}
  virtual int getNumKeys(contactElement *cE) {
    MElement *eleSlave=cE->getRefToElementSlave();
    MElement *eleMaster=cE->getRefToElementMaster();
    int nkeyseleSlave = getRefToSpaceSlave()->getNumKeys(eleSlave);
    int nkeyseleMaster = getRefToSpaceMaster()->getNumKeys(eleMaster);
    return nkeyseleSlave + nkeyseleMaster;
  }
  virtual int getNumKeysSlaveNodes(contactElement *cE) {
    MElement *eleSlave=cE->getRefToElementSlave();
    int nkeyseleSlave = getRefToSpaceSlave()->getNumKeys(eleSlave);
    return nkeyseleSlave;
  }
  virtual int getNumKeysMasterNodes(contactElement *cE) {
    MElement *eleMaster=cE->getRefToElementMaster();
    int nkeyseleMaster = getRefToSpaceMaster()->getNumKeys(eleMaster);
    return nkeyseleMaster;
  }
  virtual void getKeysMasterNode(contactElement *cE, const int ind, std::vector<Dof> &keys) {
    MElement *ele = cE->getRefToElementMaster();
    std::vector<Dof> tkeys;
    getConstRefToSpaceMaster()->getKeys(ele,tkeys);
    int nkeys = this->getNumKeysMasterNode(cE,ind);
    int nbver = ele->getNumVertices();
    if(ind>=nbver)
      Msg::Error("getKeysMasterNode requested invalid node nb");
    for(int i=0;i<nkeys; i++)
      keys.push_back(tkeys[ind+nbver*i]);
  }
  virtual void getKeysSlaveNode(contactElement *cE, const int ind, std::vector<Dof> &keys) {
    MElement *ele = cE->getRefToElementSlave();
    std::vector<Dof> tkeys;
    getConstRefToSpaceSlave()->getKeys(ele,tkeys);
    int nkeys = this->getNumKeysSlaveNode(cE,ind);
    int nbver = ele->getNumVertices();
    if(ind>=nbver)
      Msg::Error("getKeysSlaveNode requested invalid node nb");
    for(int i=0;i<nkeys; i++)
      keys.push_back(tkeys[ind+nbver*i]);
  }
  virtual int getNumKeysMasterNode(contactElement *cE, int ind) {
    //MElement *ele= cE->getRefToElementMaster();
    return getConstRefToComp().size();
  }
  virtual int getNumKeysSlaveNode(contactElement *cE, int ind) {
    //MElement *ele= cE->getRefToElementSlave();
    return getConstRefToComp().size();
  }
};

#endif // DG3DCONTACTFUNCTIONSPACE_H_
