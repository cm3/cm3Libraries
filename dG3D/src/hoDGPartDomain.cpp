//
// C++ Interface: domain
//
// Description: Class with definition of finite strain domain for finite strain problem
//
//
// Author: V.D. NGUYEN 2011
//
// Copyright: See COPYING file that comes with this distribution
//
#include "hoDGPartDomain.h"
#include "hoDGIPVariable.h"
#include "hoDGTerms.h"
#include "IFace.h"
#include "GaussLobatto2DQuadratureRules.h"
#include "IEdge.h"
#include "MInterfaceLine.h"
#include "dG3DFunctionSpace.h"
/* has to be moved in dG3D */
hoDGDomain::hoDGDomain(const int tag, const int phys, const int sp, const int lnum, const int fdg,const int dim)
                      :dG3DDomain(tag,phys,sp,lnum,fdg,dim)
{
  // specific space
  if(_space!=NULL) delete _space; // delete the space created in dG3DDomain
  switch(_wsp)
  {
   case functionSpaceType::Lagrange:
    if(_fullDg)
      _space = new dG3DhoDGFunctionSpace(getTag(),3);
    else
      _space = new g3DhoDGFunctionSpace(getTag(),3);
    break;
   case functionSpaceType::Inter :
    break; // InterfaceDomain set function space later
   default:
    Msg::Error("Function space type is unknown on domain %d",_phys);
  }
};

hoDGDomain::hoDGDomain(const hoDGDomain &source) :dG3DDomain(source){
};

void hoDGDomain::computeStrain(MElement *e, const int npts_bulk, IntPt *GP,
                               AllIPState::ipstateElementContainer *vips,
                               IPStateBase::whichState ws, fullVector<double> &disp, bool useBarF) const{
	if (!getElementErosionFilter()(e)) return;
	
  dG3DDomain::computeStrain(e,npts_bulk, GP,vips,ws,disp,useBarF);
  dG3DMaterialLaw* dglaw = static_cast<dG3DMaterialLaw*>(_mlaw);

  if (dglaw->isHighOrder()){

    FunctionSpace<double> *sp = static_cast<FunctionSpace<double>*>(_space);

    int nbFF = e->getNumShapeFunctions();
    for(int j=0;j<npts_bulk;j++){
      
      IPStateBase* ips = (*vips)[j];
      hoDGIPVariable *ipv = static_cast<hoDGIPVariable*>(ips->getState(ws));

      const std::vector<TensorialTraits<double>::HessType>& Hesses = ipv->hessf(_space,e,GP[j]);
      //x y z
      STensor33* gradientOfGradientDeformation = &ipv->getRefToGradientOfDeformationGradient();
      STensorOperation::zero(*gradientOfGradientDeformation);
      
      for (int i = 0; i < nbFF; i++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            gradientOfGradientDeformation->operator()(0,k,l) += Hesses[i+0*nbFF](k,l)*disp(i);
            gradientOfGradientDeformation->operator()(1,k,l) += Hesses[i+0*nbFF](k,l)*disp(i+nbFF);
            gradientOfGradientDeformation->operator()(2,k,l) += Hesses[i+0*nbFF](k,l)*disp(i+2*nbFF);
          };
        };

      };
    };
  }
};

void hoDGDomain::computeStrain(AllIPState *aips,MInterfaceElement *ie, IntPt *GP,const IPStateBase::whichState ws,
                                        partDomain* efMinus, partDomain *efPlus, fullVector<double> &dispm,
                                        fullVector<double> &dispp, const bool virt,
                                        bool useBarF){
  MElement *ele = dynamic_cast<MElement*>(ie);
  if (!getElementErosionFilter()(ele)) return;
	
  dG3DDomain::computeStrain(aips,ie,GP,ws,efMinus,efPlus,dispm,dispp,virt, useBarF);
  dG3DMaterialLaw* dgplus = static_cast<dG3DMaterialLaw*>(efPlus->getMaterialLaw());
  dG3DMaterialLaw* dgminus = static_cast<dG3DMaterialLaw*>(efMinus->getMaterialLaw());
  if (dgplus->isHighOrder() or dgminus->isHighOrder()){
    if(!virt){
      FunctionSpaceBase* spaceminus = efMinus->getFunctionSpace();
      
      int npts=integBound->getIntPoints(ele,&GP);
      // Value of Gauss points on elements
      IntPt *GPm; IntPt *GPp;
      _interQuad->getIntPoints(ie,GP,&GPm,&GPp);

      FunctionSpaceBase* spaceplus = efPlus->getFunctionSpace();
      MElement *em = ie->getElem(0);
      MElement *ep = ie->getElem(1);
       // gauss point
      int nbFFm = em->getNumVertices();
      int nbFFp = ep->getNumVertices();
      AllIPState::ipstateElementContainer *vips = aips->getIPstate(ele->getNum());
      for(int j=0;j<npts;j++){

        IPStateBase* ipsm = (*vips)[j];
        IPStateBase* ipsp = (*vips)[j+npts];

        hoDGIPVariable* ipvm = static_cast<hoDGIPVariable*>(ipsm->getState(ws));
        hoDGIPVariable* ipvp = static_cast<hoDGIPVariable*>(ipsp->getState(ws));

        const std::vector<TensorialTraits<double>::HessType> &Hessm  = ipvm->hessf(spaceminus,em, GPm[j]);
        const std::vector<TensorialTraits<double>::HessType> &Hessp = ipvp->hessf(spaceplus,ep,GPp[j]);

        STensor33* gradientOfDeformationGradientm = &ipvm->getRefToGradientOfDeformationGradient();
        STensorOperation::zero(*gradientOfDeformationGradientm);

        for (int i = 0; i < nbFFm; i++){
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              gradientOfDeformationGradientm->operator()(0,k,l) += Hessm[i+0*nbFFm](k,l)*dispm(i);
              gradientOfDeformationGradientm->operator()(1,k,l) += Hessm[i+0*nbFFm](k,l)*dispm(i+nbFFm);
              gradientOfDeformationGradientm->operator()(2,k,l) += Hessm[i+0*nbFFm](k,l)*dispm(i+2*nbFFm);
            };
          };
        };

        STensor33* gradientOfDeformationGradientp = &ipvp->getRefToGradientOfDeformationGradient();
        STensorOperation::zero(*gradientOfDeformationGradientp);
        for (int i = 0; i < nbFFp; i++){
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              gradientOfDeformationGradientp->operator()(0,k,l) += Hessp[i+0*nbFFp](k,l)*dispp(i);
              gradientOfDeformationGradientp->operator()(1,k,l) += Hessp[i+0*nbFFp](k,l)*dispp(i+nbFFp);
              gradientOfDeformationGradientp->operator()(2,k,l) += Hessp[i+0*nbFFp](k,l)*dispp(i+2*nbFFp);
            };
          };
        };

        if (_fullDg){

          std::vector<TensorialTraits<double>::ThirdDevType> &Thirdm  = ipvm->thirdDevf(spaceminus,em,GPm[j]);
          std::vector<TensorialTraits<double>::ThirdDevType> &Thirdp = ipvp->thirdDevf(spaceplus,ep,GPp[j]);

          STensor43* thirdOrderGradientm = & ipvm->getRefToThirdOrderDeformationGradient();
          STensorOperation::zero(*thirdOrderGradientm);
          
          for (int i=0; i<nbFFm; i++)
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++)
                for (int m=0; m<3; m++){
                  thirdOrderGradientm->operator()(0,k,l,m) += Thirdm[i+0*nbFFm](k,l,m)*dispm(i);
                  thirdOrderGradientm->operator()(1,k,l,m) += Thirdm[i+0*nbFFm](k,l,m)*dispm(i+1*nbFFm);
                  thirdOrderGradientm->operator()(2,k,l,m) += Thirdm[i+0*nbFFm](k,l,m)*dispm(i+2*nbFFm);
                };
                
          STensor43* thirdOrderGradientp = & ipvp->getRefToThirdOrderDeformationGradient();
          STensorOperation::zero(*thirdOrderGradientp);
          for (int i=0; i<nbFFp; i++)
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++)
                for (int m=0; m<3; m++){
                  thirdOrderGradientp->operator()(0,k,l,m) += Thirdp[i+0*nbFFm](k,l,m)*dispp(i);
                  thirdOrderGradientp->operator()(1,k,l,m) += Thirdp[i+0*nbFFm](k,l,m)*dispp(i+1*nbFFp);
                  thirdOrderGradientp->operator()(2,k,l,m) += Thirdp[i+0*nbFFm](k,l,m)*dispp(i+2*nbFFp);
                };
        };

      };
    }
    else{
      // Virtual interface element
      Msg::Error("No virtual interface for 3D");

    };
  }
};


void hoDGDomain::createTerms(unknownField *uf,IPField*ip){
  FunctionSpace<double>* space = static_cast<FunctionSpace<double>*>(_space);
  ltermBulk = new hoDGForceBulk(*space,_mlaw,_fullDg,ip,_incrementNonlocalBased);
  ltermBound = new hoDGForceInter(*space,space,_mlaw,_mlaw,_interQuad,_beta1,_fullDg,ip,_incrementNonlocalBased);
  if(_bmbp)
    btermBulk = new BilinearTermPerturbation<double>(ltermBulk,*space,*space,uf,ip,this,_eps);
  else
   btermBulk = new hoDGStiffnessBulk(*space,_mlaw,_fullDg,ip);

  if(_interByPert)
    btermBound = new BilinearTermPerturbation<double>(ltermBound,*space,*space,uf,ip,this,_eps);
  else
    btermBound = new hoDGStiffnessInter(*space,*space,_mlaw,_mlaw,_interQuad,_beta1,ip,uf,_fullDg,_incrementNonlocalBased,_eps);

  this->btermVirtBound = new BiNonLinearTermVoid();
  this->ltermVirtBound = new nonLinearTermVoid();

  if (getFunctionSpaceType() == functionSpaceType::Lagrange)
  {
    massterm = new mass3D(*space,_mlaw,getNumNonLocalVariable());
  }
  else if (getFunctionSpaceType() == functionSpaceType::Hierarchical)
  {
    massterm = new mass3DHierarchicalFE(*space,_mlaw,getNumNonLocalVariable());
  }
  else
  {
    Msg::Error("mass matrix has not implemented for this kind of functions space");
    massterm = new BiNonLinearTermVoid();
  }

};

void hoDGDomain::allocateInterfaceMPI(const int rankOtherPart, const elementGroup &groupOtherPart,dgPartDomain** dgdom)
{
  // domain creation has it function create a domain with a part of the same domain contained on an other part,
  // the type of domain is known
  if(*dgdom==NULL){ // domain doesn't exist --> creation
    (*dgdom) = new hoDGInterDomain(this->getTag(),this,this,0);
    // set the stability parameters
    hoDGDomain* newInterDom3D = dynamic_cast< hoDGDomain* >(*dgdom);
    newInterDom3D->stabilityParameters(this->_beta1);
  }
}


hoDGInterDomain::hoDGInterDomain(const int tag, partDomain *dom1,partDomain *dom2, const int lnum, const int bnum) :
                                  interDomainBase(dom1,dom2,bnum),
                                  hoDGDomain(tag,manageInterface::getKey(dom1->getPhysical(),
                                                                    dom2->getPhysical()),10000,lnum,true,dom1->getDim()){

  // create empty elementGroup
  g = new elementGroup();
  if(_space != NULL) delete _space;
  _space = new dG3DFunctionSpaceBetween2Domains(_domMinus->getFunctionSpace(),_domPlus->getFunctionSpace());
};

hoDGInterDomain::hoDGInterDomain(const hoDGInterDomain& src) : hoDGDomain(src),interDomainBase(src){};

void hoDGInterDomain::createTerms(unknownField *uf,IPField*ip){
  // void term (as they have to exist)
  this->ltermBulk = new nonLinearTermVoid();
  this->btermBulk = new BiNonLinearTermVoid();

  this->btermVirtBound = new BiNonLinearTermVoid();
  this->ltermVirtBound = new nonLinearTermVoid();
  this->massterm = new BiNonLinearTermVoid();

  FunctionSpace<double>*  sp1 = static_cast<FunctionSpace<double>*>(getMinusDomain()->getFunctionSpace());
  FunctionSpace<double>*  sp2 = static_cast<FunctionSpace<double>*>(getPlusDomain()->getFunctionSpace());

  hoDGDomain* domMinus = static_cast<hoDGDomain*>(_domMinus);
  hoDGDomain* domPlus = static_cast<hoDGDomain*>(_domPlus);

  ltermBound = new hoDGForceInter(*sp1,sp2,_mlawMinus,_mlawPlus,_interQuad,_beta1,_fullDg,ip,_incrementNonlocalBased);

  if(_interByPert)
    btermBound = new BilinearTermPerturbation<double>(ltermBound,*sp1,*sp2,uf,ip,this,_eps);
  else
    btermBound = new hoDGStiffnessInter(*sp1,*sp2,_mlawMinus, _mlawPlus,_interQuad,_beta1,ip,uf,_fullDg,_incrementNonlocalBased,_eps);
};

void hoDGInterDomain::stabilityParameters(const double b1){
  _beta1 = b1;
};

void hoDGInterDomain::matrixByPerturbation(const int iinter, const double eps){
  _interByPert = iinter;
  _eps = eps;
};

void hoDGInterDomain::setMaterialLaw(const std::map<int,materialLaw*> &maplaw){
  // oterwise the law is already set
  if(!setmaterial){
    _domMinus->setMaterialLaw(maplaw);
    _domPlus->setMaterialLaw(maplaw);
    _mlaw = NULL;
    _mlawMinus = _domMinus->getMaterialLaw();
    _mlawPlus = _domPlus->getMaterialLaw();
    _mlawMinus->initLaws(maplaw);
    _mlawPlus->initLaws(maplaw);
    setmaterial=true;
    dG3DMaterialLaw* dgminus = dynamic_cast<dG3DMaterialLaw*>(_mlawMinus);
    dG3DMaterialLaw* dgplus = dynamic_cast<dG3DMaterialLaw*>(_mlawPlus);
    if (dgminus->isHighOrder() and dgplus->isHighOrder())
      _isHighOrder = true;
    else
      _isHighOrder = false;
  }
}





