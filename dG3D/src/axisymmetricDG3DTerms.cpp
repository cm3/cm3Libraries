//
// C++ Interface: terms
//
// Description: Class of terms for dg non linear shell
//
//
// Author:  <V.D. Nguyen>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "axisymmetricDG3DTerms.h"
#include "nonLocalDamageDG3DIPVariable.h"
#include "FractureCohesiveDG3DIPVariable.h"

void axisymmetricG3DFiniteStrainsPressureTerm::get(MElement *ele,int npts,IntPt *GP,fullVector<double> &m) const
{
  // resize the force vector
  const int nbdof=this->space1.getNumKeys(ele);
  const int nbFF = ele->getNumShapeFunctions();
  m.resize(nbdof);
  m.setAll(0.);

  // get the displacements of the element
  std::vector<Dof> keys;
  this->space1.getKeys(ele,keys);
  fullVector<double> disp(keys.size());
  _ufield->get(keys,disp);

  // Get shape functions values and gradients at all Gauss points
  nlsFunctionSpace<double>* sp1 = static_cast<nlsFunctionSpace<double>*>(&(this->space1));
  std::vector<GaussPointSpaceValues<double>*> vgps;
  sp1->get(ele,npts,GP,vgps);
  
  SPoint3 point;
  for (int i = 0; i < npts; i++)
  {
    const double weight = GP[i].weight;
    const double u = GP[i].pt[0];
    const double v = GP[i].pt[1];
    const double w = GP[i].pt[2];
    std::vector<TensorialTraits<double>::ValType> &Vals = vgps[i]->_vvals;
    std::vector<TensorialTraits<double>::GradType> Grads;
    sp1->gradf(ele,u,v,w,Grads);
    
    ele->pnt(Vals,point);
    
    static STensor3 defo_grad;
    STensorOperation::unity(defo_grad);
    for (int j = 0; j < nbFF; j++)
    {
       // rr
      defo_grad(0,0) += Grads[j+0*nbFF][0]*disp(j);
      // rz
      defo_grad(0,2) += Grads[j+0*nbFF][2]*disp(j);
      // theta-theta
      defo_grad(1,1) += Vals[j+0*nbFF]*disp(j)/point[0];
      //zr
      defo_grad(2,0) += Grads[j+0*nbFF][0]*disp(j+2*nbFF);
      //zz
      defo_grad(2,2) += Grads[j+0*nbFF][2]*disp(j+2*nbFF);
    }
    if(STensorOperation::determinantSTensor3(defo_grad) < 1.e-15) Msg::Error("Negative Jacobian axisymmetricG3DFiniteStrainsPressureTerm::get");
    
    // defo gradient and jac
    double jdefo = STensorOperation::determinantSTensor3(defo_grad);
    static STensor3 invF;
    STensorOperation::inverseSTensor3(defo_grad,invF);
    
    // initial normal de l'élément (compute and store it once at the beginning)
    SVector3 phi1(0.);
    std::vector<TensorialTraits<double>::GradType> &graduv = vgps[i]->_vgrads;
    for(int j=0;j<ele->getNumVertices();++j)
    {
        phi1[0] += graduv[j](0) * ele->getVertex(j)->x();
        phi1[1] += graduv[j](0) * ele->getVertex(j)->y();
        phi1[2] += graduv[j](0) * ele->getVertex(j)->z();
    }
    SVector3 phi2(0.,1.,0.);
    SVector3 ref_normal = crossprod(phi1,phi2);
    ref_normal.normalize(); // ref_normal unit?

    // current normal using Nanson formula
    SVector3 cur_normal = ref_normal * invF;

    
    double load=Load->operator()(point.x(),point.y(),point.z());
    
    double detJ = ele->getJacobianDeterminant(u,v,w);
    double loadvalue = -load*weight*detJ*2.*M_PI*point.x(); /// minus as cur_normal == outward normal
    for (int j = 0; j < nbFF ; ++j){
      m(j)+=Vals[j]*loadvalue*jdefo*cur_normal[0];
      m(j+nbFF)+=Vals[j+nbFF]*loadvalue*jdefo*cur_normal[1];
      m(j+nbFF+nbFF)+=Vals[j+2*nbFF]*loadvalue*jdefo*cur_normal[2];
    }
  }
}

void axisymmetricG3DFiniteStrainsPressureBilinearTerm::get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const
{
  // resize the force vector
  const int nbdof=this->space1.getNumKeys(ele);
  const int nbFF = ele->getNumShapeFunctions();
  m.resize(nbdof,nbdof);
  m.setAll(0.);


  // get the displacements of the element
  std::vector<Dof> keys;
  this->space1.getKeys(ele,keys);
  fullVector<double> disp(keys.size());
  _ufield->get(keys,disp);

  // Get shape functions values and gradients at all Gauss points
  nlsFunctionSpace<double>* sp1 = static_cast<nlsFunctionSpace<double>*>(&(this->space1));
  std::vector<GaussPointSpaceValues<double>*> vgps;
  sp1->get(ele,npts,GP,vgps);
  
  fullMatrix<double> dFdq;
  dFdq.resize(9,3*nbFF);
  
  SPoint3 point;

  for (int i = 0; i < npts; i++)
  {
    const double weight = GP[i].weight;
    const double u = GP[i].pt[0];
    const double v = GP[i].pt[1];
    const double w = GP[i].pt[2];
    std::vector<TensorialTraits<double>::ValType> &Vals = vgps[i]->_vvals;
    std::vector<TensorialTraits<double>::GradType> Grads;
    sp1->gradf(ele,u,v,w,Grads);
    //
    ele->pnt(Vals,point);

    static STensor3 defo_grad;
    STensorOperation::unity(defo_grad);
    for (int j = 0; j < nbFF; j++)
    {
      // rr
      defo_grad(0,0) += Grads[j+0*nbFF][0]*disp(j);
      // rz
      defo_grad(0,2) += Grads[j+0*nbFF][2]*disp(j);
      // theta-theta
      defo_grad(1,1) += Vals[j+0*nbFF]*disp(j)/point[0];
      //zr
      defo_grad(2,0) += Grads[j+0*nbFF][0]*disp(j+2*nbFF);
      //zz
      defo_grad(2,2) += Grads[j+0*nbFF][2]*disp(j+2*nbFF);
    }
    if(STensorOperation::determinantSTensor3(defo_grad) < 1.e-15) Msg::Error("Negative Jacobian");
    
    // defo gradient and jac
    double jdefo = STensorOperation::determinantSTensor3(defo_grad);
    static STensor3 invF;
    STensorOperation::inverseSTensor3(defo_grad,invF);
    
    // initial normal de l'élément (compute and store it once at the beginning)
    SVector3 phi1(0.);
    std::vector<TensorialTraits<double>::GradType> &graduv = vgps[i]->_vgrads;
    for(int i=0;i<ele->getNumVertices();++i)
    {
        phi1[0] += graduv[i](0) * ele->getVertex(i)->x();
        phi1[1] += graduv[i](0) * ele->getVertex(i)->y();
        phi1[2] += graduv[i](0) * ele->getVertex(i)->z();
    }
    SVector3 phi2(0.,1.,0.);
    SVector3 ref_normal = crossprod(phi1,phi2);
    ref_normal.normalize(); // ref_normal unit?

    // current normal using Nanson formula
    STensor33 DJcur_normalDF(0.);
    for (int k=0; k<3; k++){
      for (int l=0; l<3; l++){
        for (int r=0; r<3; r++){
          for (int s=0; s<3; s++){
            DJcur_normalDF(l,r,s) +=  jdefo*invF(s,r)*ref_normal(k)*invF(k,l) -jdefo*0.5*ref_normal(k)*(invF(k,r)*invF(l,s)+ invF(k,s)*invF(l,r));
          }
        }
      }
    }
    
     
    double load=Load->operator()(point.x(),point.y(),point.z());
    
    dFdq.setAll(0.);
    for (int j=0; j< nbFF; j++){
      // comp rr
      int row= Tensor23::getIndex(0,0);
      dFdq(row,j) = Grads[j+0*nbFF](0);
      // comp rz
      row= Tensor23::getIndex(0,2);
      dFdq(row,j) = Grads[j+0*nbFF](2);
      // comp tt
      row =Tensor23::getIndex(1,1);
      dFdq(row,j) = Vals[j+0*nbFF]/point[0];

      // comp zr
      row =Tensor23::getIndex(2,0);
      dFdq(row,j+2*nbFF) = Grads[j+2*nbFF](0);
      
      // comp zz
      row =Tensor23::getIndex(2,2);
      dFdq(row,j+2*nbFF) = Grads[j+2*nbFF](2);
    }
    
    double detJ = ele->getJacobianDeterminant(u,v,w);
    double loadvalue = -load*weight*detJ*2.*M_PI*point.x(); /// minus as cur_normal == outward normal
    for (int j = 0; j < nbFF ; ++j){
      for (int k=0; k<3; k++){
        for (int p=0; p< 3*nbFF; p++){
          for (int q=0; q<9; q++){
            int r,s;
            Tensor23::getIntsFromIndex(q,r,s);
            m(j+k*nbFF,p) -=  Vals[j]*loadvalue*DJcur_normalDF(k,r,s)*dFdq(q,p);
          }
        }
      }
    }
  }
}

void axisymmetricG3DLoadTerm::get(MElement *ele,int npts,IntPt *GP,fullVector<double> &m) const
{
  const int nbdof=this->space1.getNumKeys(ele);
  m.resize(nbdof);
  m.setAll(0.);
  // Get shape functions values and gradients at all Gauss points
  nlsFunctionSpace<double>* sp1 = static_cast<nlsFunctionSpace<double>*>(&(this->space1));
  std::vector<GaussPointSpaceValues<double>*> vgps;
  sp1->get(ele,npts,GP,vgps);
  
  SPoint3 p;
  double jac[3][3];
  double scaleEq = 1.;
  if (comp < 3){
    scaleEq = 1.; // disp
  }
  else if (comp > 2 and comp < 3+getNumNonLocalVariable()){
    scaleEq = getNonLocalEqRatio(); // nonlocal Dof
  }
  else if (comp > 2+getNumNonLocalVariable() and comp < 3+getNumNonLocalVariable()+getNumConstitutiveExtraDofDiffusionVariable()){
    scaleEq = getConstitutiveExtraDofDiffusionEqRatio(); // extraDof
  }
  else{
    Msg::Error("comp = %d has not been wrongly introduced in axisymmetricG3DLoadTerm::get",comp);
  }
  for (int i = 0; i < npts; i++)
  {
    const double weight = GP[i].weight;
    std::vector<TensorialTraits<double>::ValType> &Vals = vgps[i]->_vvals;
    double detJ = ele->getJacobianDeterminant(GP[i].pt[0],GP[i].pt[1],GP[i].pt[2]);
    ele->pnt(Vals,p);
    double load=Load->operator()(p.x(),p.y(),p.z());
    double loadvalue = load*weight*detJ*2.*M_PI*p.x();
    for (int j = 0; j < nbdof ; ++j)
      m(j)+=Vals[j]*loadvalue*scaleEq;
  }

}

void axisymmetricDG3DForceBulk::get(MElement *ele,int npts,IntPt *GP,fullVector<double> &vFor) const
{
  int nbdof = this->space1.getNumKeys(ele);
  int nbFF = ele->getNumShapeFunctions();
  vFor.resize(nbdof);
  vFor.setAll(0.);

  // IP FIELD
  const AllIPState::ipstateElementContainer *vips = _ipf->getAips()->getIPstate(ele->getNum());
  
  SPoint3 point;

  for (int i = 0; i < npts; i++)
  {
    const IPStateBase *ips        = (*vips)[i];
    const dG3DIPVariableBase *ipv     = static_cast<const dG3DIPVariableBase*>(ips->getState(IPStateBase::current));
    const dG3DIPVariableBase *ipvprev     = static_cast<const dG3DIPVariableBase*>(ips->getState(IPStateBase::previous));
    //
    double weight = GP[i].weight;
    double & detJ = ipv->getJacobianDeterminant(ele,GP[i]);
    
    ele->pnt(GP[i].pt[0],GP[i].pt[1],GP[i].pt[2],point);

    double ratio = 2.*M_PI*point[0]*detJ*weight;
    
    const std::vector<TensorialTraits<double>::ValType> &Vals = ipv->f(&space1,ele,GP[i]);
    const std::vector<TensorialTraits<double>::GradType> &Grads = ipv->gradf(&space1,ele,GP[i]);
    const STensor3& PK1 = ipv->getConstRefToFirstPiolaKirchhoffStress();
    for (int j=0; j<nbFF; j++){
      vFor(j+0*nbFF) += ratio*(PK1(0,0)*Grads[j+0*nbFF](0) + PK1(0,2)*Grads[j+0*nbFF](2)+PK1(1,1)*Vals[j+0*nbFF]/point[0]);
      vFor(j+2*nbFF) += ratio*(PK1(2,0)*Grads[j+2*nbFF](0) + PK1(2,2)*Grads[j+2*nbFF](2));
    }
    
    if( ipv->getNumberNonLocalVariable()>getNumNonLocalVariable() ) Msg::Error("Your material law use more non local variable than your domain");
    for (int nl = 0; nl< ipv->getNumberNonLocalVariable(); nl++){
      const int threeTimesNbFF = 3*nbFF; //why in 3?
      const STensor3 *cg = &ipv->getConstRefToCharacteristicLengthMatrix(nl);
      
      double nonlocalVar =ipv->getConstRefToNonLocalVariable(nl);
      SVector3 gradNonlocalVar = ipv->getConstRefToGradNonLocalVariable()[nl];
			double localVar = ipv->getConstRefToLocalVariable(nl);
      
      if (_incrementNonlocalBased){
        nonlocalVar -= ipvprev->getConstRefToNonLocalVariable(nl);
        gradNonlocalVar -= ipvprev->getConstRefToGradNonLocalVariable()[nl];
        localVar -= ipvprev->getConstRefToLocalVariable(nl);
      }

      for(int j=0;j<nbFF; j++){
        vFor(j+nl*nbFF+threeTimesNbFF) += getNonLocalEqRatio()*ratio*(Vals[j+nl*nbFF+threeTimesNbFF]*nonlocalVar);
        for(int m=0; m<3; m++){
          for(int n=0; n<3; n++){
            vFor(j+nl*nbFF+threeTimesNbFF) += getNonLocalEqRatio()*ratio*(Grads[j+nl*nbFF+threeTimesNbFF](m)*cg->operator()(m,n)*gradNonlocalVar(n));
          }
        }
        vFor(j+nl*nbFF+threeTimesNbFF) -= getNonLocalEqRatio()*ratio*(Vals[j+nl*nbFF+threeTimesNbFF]*localVar);
      }
    }
    
    //extra dof
    if( ipv->getNumConstitutiveExtraDofDiffusionVariable()>getNumConstitutiveExtraDofDiffusionVariable() ){
         Msg::Error("Your material law uses more constitutive extra dof variables than your domain axisymmetricDG3DForceBulk::get");
    }
    for (int extraDOFField = 0; extraDOFField< ipv->getNumConstitutiveExtraDofDiffusionVariable(); extraDOFField++){
      const SVector3 &flux =   ipv-> getConstRefToFlux()[extraDOFField];
      double w = 0.;
      if (getConstitutiveExtraDofDiffusionAccountFieldSource()){
         w += ipv->getConstRefToFieldSource()(extraDOFField);
        }
      if (getConstitutiveExtraDofDiffusionAccountMecaSource()){
         w += ipv->getConstRefToMechanicalSource()(extraDOFField);
      }
      int indexField=3+getNumNonLocalVariable()+extraDOFField;

      for(int j=0;j<nbFF; j++)
      {
        for(int m=0; m<3; m++)
        {
          vFor(j+indexField*nbFF) += getConstitutiveExtraDofDiffusionEqRatio()*ratio*(Grads[j+indexField*nbFF][m]*flux(m));
        }
        if(extraDOFField==0){ // corresponds to temperature, but when considering electro-mechanics, ???
          vFor(j+indexField*nbFF) += getConstitutiveExtraDofDiffusionEqRatio()*ratio*(w*Vals[j+indexField*nbFF]);// related to cp
        }
      }
    }
    
  }
  // vFor.print("interF");
}

void axisymmetricDG3DStiffnessBulk::get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &mStiff) const
{
  if (sym)
  {
    // Initialization of some data
    int nbdof = g3DBilinearTerm<double,double>::space1.getNumKeys(ele);
    int nbFF = ele->getNumShapeFunctions();
    mStiff.resize(nbdof, nbdof,true);
    
    // get ipvariables
    _ipf->getIPv(ele,vipv);
    // sum on Gauss' points
    
    SPoint3 point; 
    fullMatrix<double> dFdq;
    dFdq.resize(9,3*nbFF);
      
    for (int i = 0; i < npts; i++)
    {
      double weight = GP[i].weight;
      double& detJ = vipv[i]->getJacobianDeterminant(ele,GP[i]);
      
      ele->pnt(GP[i].pt[0],GP[i].pt[1],GP[i].pt[2],point);
      double ratio = 2.*M_PI*point[0]*detJ*weight;
      // x, y, z
      const STensor43 & H =vipv[i]->getConstRefToTangentModuli();
      
      const std::vector<TensorialTraits<double>::ValType> &Vals = vipv[i]->f(&space1,ele,GP[i]);
      const std::vector<TensorialTraits<double>::GradType> &Grads = vipv[i]->gradf(&space1,ele,GP[i]);
      
      dFdq.setAll(0.);
      
      for (int j=0; j< nbFF; j++){
        // comp rr
        int row= Tensor23::getIndex(0,0);
        dFdq(row,j) = Grads[j+0*nbFF](0);
        // comp rz
        row= Tensor23::getIndex(0,2);
        dFdq(row,j) = Grads[j+0*nbFF](2);
        // comp tt
        row =Tensor23::getIndex(1,1);
        dFdq(row,j) = Vals[j+0*nbFF]/point[0];

        // comp zr
        row =Tensor23::getIndex(2,0);
        dFdq(row,j+2*nbFF) = Grads[j+2*nbFF](0);
        
        // comp zz
        row =Tensor23::getIndex(2,2);
        dFdq(row,j+2*nbFF) = Grads[j+2*nbFF](2);
      }
      
      for (int j=0; j<3*nbFF; j++){
        for (int k=0; k<9; k++){
          int u,v;
          Tensor23::getIntsFromIndex(k,u,v);
          for (int p=0; p< 3*nbFF; p++){
            for (int q=0; q<9; q++){
              int r,s;
              Tensor23::getIntsFromIndex(q,r,s);
              mStiff(j,p) += ratio*H(u,v,r,s)*dFdq(q,p)*dFdq(k,j);
            }
          
          }
        }
      }
      const int threeTimesNbFF = 3*nbFF;
      if( vipv[i]->getNumberNonLocalVariable()>getNumNonLocalVariable() ) Msg::Error("Your material law use more non local variable than your domain");
      for (int nlk = 0; nlk < vipv[i]->getNumberNonLocalVariable(); nlk++){
        const STensor3& dlocalVardF = vipv[i]->getConstRefToDLocalVariableDStrain()[nlk];
        const STensor3& dPdNonLocalVar = vipv[i]->getConstRefToDStressDNonLocalVariable()[nlk];
        const STensor3& cg = vipv[i]->getConstRefToCharacteristicLengthMatrix(nlk);        
				double  SpBar = vipv[i]->getConstRefToDLocalVariableDNonLocalVariable()(nlk,nlk);
				
        for(int j=0;j<nbFF; j++){
          for(int k=0;k<nbFF;k++){
            
            // principal term
            double BtB = 0.;
						for(int m=0; m<3; m++){
							 for(int n=0; n<3; n++){
							 BtB += Grads[j+nlk*nbFF+threeTimesNbFF](m)*(cg(m,n))*Grads[k+nlk*nbFF+threeTimesNbFF](n);
							}
						}
						
						mStiff(j+nlk*nbFF+threeTimesNbFF,k+nlk*nbFF+threeTimesNbFF)
									+= getNonLocalEqRatio()*ratio*(Vals[j+nlk*nbFF+threeTimesNbFF]*Vals[k+nlk*nbFF+threeTimesNbFF]*(1-SpBar)+ BtB);
										
            // cross terms to check
            for(int kk=0;kk<3;kk++){
              for(int m=0; m<3; m++){
                mStiff(j+kk*nbFF,k+nlk*nbFF+threeTimesNbFF) += ratio*(Grads[j+kk*nbFF](m)*dPdNonLocalVar(kk,m)*Vals[k+nlk*nbFF+threeTimesNbFF]);
                int row = Tensor23::getIndex(kk,m);
                mStiff(j+nlk*nbFF+threeTimesNbFF,k+kk*nbFF) -= getNonLocalEqRatio()*ratio*(Vals[j+nlk*nbFF+threeTimesNbFF]*dlocalVardF(kk,m)*dFdq(row,k+kk*nbFF));
              }
            }
          }
        }
        
        for (int extraDOFField = 0; extraDOFField < vipv[i]->getNumConstitutiveExtraDofDiffusionVariable(); extraDOFField++){
          //Msg::Error("Check non-local / extra dof coupling in dG3DStiffnessBulk::get");
          double  dLocalVariableDExtraDof = vipv[i]->getConstRefTodLocalVariableDExtraDofDiffusionField()(nlk,extraDOFField);
          for(int j=0;j<nbFF; j++){
            for(int k=0;k<nbFF;k++){
              int indexField=3+getNumNonLocalVariable()+extraDOFField;
              // principal term
              mStiff(j+nlk*nbFF+threeTimesNbFF,k+indexField*nbFF)
                    += getNonLocalEqRatio()*ratio*(Vals[j+nlk*nbFF+threeTimesNbFF]*Vals[k+indexField*nbFF]*dLocalVariableDExtraDof);
            }
          }
        }
      }
      
    }
  // mStiff.print("bulk");
  }
  else
    Msg::Error("not implemented\n");
}

void axisymmetricMass3D::get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const{
  
  // Get the needed values at all Gauss points
  SPoint3 point;
  
  if(ele->getTypeForMSH() == MSH_TRI_6){
    // Resize the matrix m
    int nbdof = BilinearTerm<double,double>::space1.getNumKeys(ele);
    m.resize(nbdof, nbdof,true); // true --> setAll 0.
    
    // Compute volume and total mass by integration on Gauss' points
    double volume = 0.;  // total volume    
    /// and loop on all the GPs
    for (int i = 0; i < npts; i++){
      double weight = GP[i].weight;
      double detJ = ele->getJacobianDeterminant(GP[i].pt[0],GP[i].pt[1],GP[i].pt[2]);
      ele->pnt(GP[i].pt[0],GP[i].pt[1],GP[i].pt[2],point);
      
      volume += 2.*M_PI*point[0]*detJ*weight;
    }
    double mass_ele = volume*_rho; // (rho = density per unit volume)
    
    // Construct local mass matrix in the three directions by distributing the total mass among the dof
    // following m_ii = ratio_i / sum(ratio_i) * mass_ele
    // ratio_i = r(i)*(1/12 or 1/4 depending on the node position))
      /// get radius of each nodes (i.e. r == the first coordinates)
    double radius[6];
    for(int k=0;k<6;k++){
      radius[k] = ele->getVertex(k)->x();
    }
      /// compute sum(ratio_i)
    double sum_ratio = (radius[0]+radius[1]+radius[2])/12. + (radius[3]+radius[4]+radius[5])/4.;
      /// loop on disp-dof only of the matrix
    int nbFF = 6;//ele->getNumShapeFunctions();
    for(int k=0; k<3; k++){
       m(  k*6,  k*6) += radius[0]/12./sum_ratio*mass_ele;
       m(1+k*6,1+k*6) += radius[1]/12./sum_ratio*mass_ele;
       m(2+k*6,2+k*6) += radius[2]/12./sum_ratio*mass_ele;
       m(3+k*6,3+k*6) += radius[3]/4. /sum_ratio*mass_ele;
       m(4+k*6,4+k*6) += radius[4]/4. /sum_ratio*mass_ele;
       m(5+k*6,5+k*6) += radius[5]/4. /sum_ratio*mass_ele;
    }
  }
  else{
    if(sym){
      // Initialization of some data
      int nbdof = BilinearTerm<double,double>::space1.getNumKeys(ele);
      int nbFF = ele->getNumShapeFunctions();
      int dim = nbdof/nbFF;
      m.resize(nbdof, nbdof,true); // true --> setAll 0.
      for (int i = 0; i < npts; i++)
      {
        double weight = GP[i].weight;
        double u = GP[i].pt[0];
        double v = GP[i].pt[1];
        double w = GP[i].pt[2];
        
        double detJ = ele->getJacobianDeterminant(u,v,w);
        ele->pnt(GP[i].pt[0],GP[i].pt[1],GP[i].pt[2],point);
        
        std::vector<TensorialTraits<double>::ValType> Vals;
        BilinearTerm<double,double>::space1.fuvw(ele,u, v, w, Vals);
        for(int j=0;j<nbFF; j++)
        {
          for(int k=0;k<nbFF;k++){
            // same mass in the 3 directions
            double mij = 2.*M_PI*point[0]*detJ*weight*_rho*Vals[j]*Vals[k];
            for(int kk=0;kk<dim;kk++)
              m(j+kk*nbFF,k+kk*nbFF) += mij;
          }
        }
      }
    // m.print("mass");
   }
   else
     Msg::Error("Mass matrix is not implemented for dg3D with two different function spaces");
  }
}







void axisymmetricDG3DForceInter::get(MElement *ele, int npts, IntPt *GP, fullVector<double> &m)const
{
  MInterfaceElement *ie = dynamic_cast<MInterfaceElement*>(ele);
  const int nbFFm = ie->getElem(0)->getNumVertices();
  const int nbFFp = ie->getElem(1)->getNumVertices();
  const int nbdofm = _minusSpace->getNumKeys(ie->getElem(0));
  const int nbdofp = _plusSpace->getNumKeys(ie->getElem(1));
  m.resize(nbdofm+nbdofp);
  m.setAll(0.);
  if(_fullDg)
  {
    // characteristic size
    const double hs = ie->getCharacteristicSize();
    const double b1hs = getStabilityParameter()/hs;

    // get value at gauss's point
    const AllIPState::ipstateElementContainer *vips = _ipf->getAips()->getIPstate(ele->getNum());
    // Values on minus and plus elements
    IntPt *GPm; IntPt *GPp;
    _interQuad->getIntPoints(ie,GP,&GPm,&GPp);
    
    // init point for position
    SPoint3 point;
    
    // loop on Gauss Point
    for(int i=0;i<npts; i++)
    {
      // IP
      const IPStateBase *ipsm        = (*vips)[i];
      const dG3DIPVariableBase *ipvm     = static_cast<const dG3DIPVariableBase*>(ipsm->getState(IPStateBase::current));
      const dG3DIPVariableBase *ipvmprev = static_cast<const dG3DIPVariableBase*>(ipsm->getState(IPStateBase::previous));

      const SVector3 &referenceOutwardNormalm = ipvm->getConstRefToReferenceOutwardNormal();

      const IPStateBase *ipsp        = (*vips)[i+npts];
      const dG3DIPVariableBase *ipvp     = static_cast<const dG3DIPVariableBase*>(ipsp->getState(IPStateBase::current));
      const dG3DIPVariableBase *ipvpprev    = static_cast<const dG3DIPVariableBase*>(ipsp->getState(IPStateBase::previous));

      const SVector3 &referenceOutwardNormalp = ipvp->getConstRefToReferenceOutwardNormal();
      const SVector3 &jump = ipvp->getConstRefToJump();

      // Weight of Gauss' point i and Jacobian's value at this point
      const double weight = GP[i].weight;
      const double& detJ = ipvm->getJacobianDeterminant(ie,GP[i]);
      ele->pnt(GP[i].pt[0],GP[i].pt[1],GP[i].pt[2],point);
      const double wJonR = 2.*M_PI*detJ* weight;
      const double wJ = wJonR*point[0];

      const std::vector<TensorialTraits<double>::GradType> &Gradsm = ipvm->gradf(_minusSpace,ie->getElem(0),GPm[i]);
      const std::vector<TensorialTraits<double>::GradType> &Gradsp = ipvp->gradf(_plusSpace,ie->getElem(1),GPp[i]);
      const std::vector<TensorialTraits<double>::ValType> &Valsm = ipvm->f(_minusSpace,ie->getElem(0),GPm[i]);
      const std::vector<TensorialTraits<double>::ValType> &Valsp = ipvp->f(_plusSpace,ie->getElem(1),GPp[i]);
      // possibility of fracture
      bool broken = false;
      if(_mlawMinus->getType() == materialLaw::fracture )
      {
        // when if failure is checked at the convergence state, previous or current is not important
        // but when failure is checked on the fly,  current must be used
        // this  corresponds to a more general case - Van Dung
        const FractureCohesive3DIPVariable *ipvmf = static_cast<const FractureCohesive3DIPVariable*>(ipvm); // broken via minus (OK broken on both sides)
        broken = ipvmf->isbroken();
      }

      if(!broken)
      {
        static SVector3 nu;
        nu = referenceOutwardNormalm;
        nu*=(1./referenceOutwardNormalm.norm());

        const std::vector<TensorialTraits<double>::ValType> &Valsm = ipvm->f(_minusSpace,ie->getElem(0),GPm[i]);
        const std::vector<TensorialTraits<double>::ValType> &Valsp = ipvp->f(_plusSpace,ie->getElem(1),GPp[i]);
        
        // Consistency \mean{P}
        static SVector3 meanPN;
        STensorOperation::zero(meanPN);
        for(int l =0; l<3; l++)
        {
          meanPN[0] += (ipvp->getConstRefToFirstPiolaKirchhoffStress()(0,l)+ipvm->getConstRefToFirstPiolaKirchhoffStress()(0,l))*(nu(l)/2.);
          meanPN[2] += (ipvp->getConstRefToFirstPiolaKirchhoffStress()(2,l)+ipvm->getConstRefToFirstPiolaKirchhoffStress()(2,l))*(nu(l)/2.);
        }
        
        for(int j=0;j<nbFFm;j++){
          m(j+0*nbFFm) -= (meanPN[0])*(Valsm[j+0*nbFFm]*wJ);
          m(j+2*nbFFm) -= (meanPN[2])*(Valsm[j+0*nbFFm]*wJ);
        }
        for(int j=0;j<nbFFp;j++){
          m(j+0*nbFFp+nbdofm) += (meanPN[0])*(Valsp[j+0*nbFFp]*wJ);
          m(j+2*nbFFp+nbdofm) += (meanPN[2])*(Valsp[j+0*nbFFp]*wJ);
        }
        
          
        // Stability  N \mean{H} (jump N)
        static STensor43 meanH;
        meanH  = ipvp->getConstRefToDGElasticTangentModuli();
        meanH += ipvm->getConstRefToDGElasticTangentModuli();
        meanH *= (0.5);

        static SVector3 NMeanHJumpNBetasc;
        STensorOperation::zero(NMeanHJumpNBetasc);
        for(int l = 0; l <3; l++){
          for(int m = 0; m <3; m++){
            for(int n = 0; n <3; n++){
              NMeanHJumpNBetasc(0) += meanH(0,l,m,n)*jump(m)*nu(n)*b1hs*nu(l);
              NMeanHJumpNBetasc(2) += meanH(2,l,m,n)*jump(m)*nu(n)*b1hs*nu(l);
            }
          }
        }
        // assembly of the consistency + stability terms (if not broken)
        for(int j=0;j<nbFFm;j++){
          for(int k=0;k<3;k++){
            m(j+k*nbFFm) -= (NMeanHJumpNBetasc[k])*(Valsm[j+0*nbFFm]*wJonR);
          }
        }
        for(int j=0;j<nbFFp;j++){
          for(int k=0;k<3;k++){
            m(j+k*nbFFp+nbdofm) += (NMeanHJumpNBetasc[k])*(Valsp[j+0*nbFFp]*wJonR);
          }
        }
        
        // symmetrisation term
        static STensor3 HmJumpN;
        static STensor3 HpJumpN;

        for(int k = 0; k <3; k++){
          for(int l = 0; l <3; l++){
            HmJumpN(k,l) = 0.;
            HpJumpN(k,l) = 0.;
            for(int m = 0; m <3; m++){
              for(int n = 0; n <3; n++){
                HmJumpN(k,l) += ipvm->getConstRefToDGElasticTangentModuli()(m,n,k,l)*jump(m)*nu(n);
                HpJumpN(k,l) += ipvp->getConstRefToDGElasticTangentModuli()(m,n,k,l)*jump(m)*nu(n);
              }
            }
          }
        }
        
        // Assembly (loop on shape function)
        for(int j=0;j<nbFFm;j++)
        {
          m(j+0*nbFFm) += (HmJumpN(0,0)*Gradsm[j+0*nbFFm][0])*(wJ/2.);
          m(j+0*nbFFm) += (HmJumpN(0,2)*Gradsm[j+0*nbFFm][2])*(wJ/2.);
          m(j+0*nbFFm) += (HmJumpN(1,1)*Valsm[j+nbFFm])*(wJ/2.)/point[0];
          m(j+2*nbFFm) += (HmJumpN(2,0)*Gradsm[j+0*nbFFm][0])*(wJ/2.);
          m(j+2*nbFFm) += (HmJumpN(2,2)*Gradsm[j+0*nbFFm][2])*(wJ/2.);
        }
        for(int j=0;j<nbFFp;j++)
        {
          m(j+0*nbFFp+nbdofm) += (HpJumpN(0,0)*Gradsp[j+0*nbFFp][0])*(wJ/2.);
          m(j+0*nbFFp+nbdofm) += (HpJumpN(0,2)*Gradsp[j+0*nbFFp][2])*(wJ/2.);
          m(j+0*nbFFp+nbdofm) += (HpJumpN(1,1)*Valsp[j+nbFFp])*(wJ/2.)/point[0];
          m(j+2*nbFFp+nbdofm) += (HpJumpN(2,0)*Gradsp[j+0*nbFFp][0])*(wJ/2.);
          m(j+2*nbFFp+nbdofm) += (HpJumpN(2,2)*Gradsp[j+0*nbFFp][2])*(wJ/2.);
        }
        
        
        
        // non local
        if( ipvm->getNumberNonLocalVariable()>getNumNonLocalVariable() ) Msg::Error("Your minus material law uses more non local variable than your domain dG3DForceInter::get");
        if( ipvp->getNumberNonLocalVariable()>getNumNonLocalVariable() ) Msg::Error("Your plus material law uses more non local variable than your domain dG3DForceInter::get");
        if( ipvm->getNumberNonLocalVariable()!= ipvm->getNumberNonLocalVariable()  ) Msg::Error("Your minus and plus material laws do not have the same number of non local variables dG3DForceInter::get");
        if(getNonLocalContinuity() && !broken)
        {
          /// stability parameter including mesh size and radial position.
          const double nbetahs = getNonLocalStabilityParameter() / hs / point[0];
 
          for (int nlk = 0; nlk< ipvp->getNumberNonLocalVariable(); nlk++){
            
            const STensor3 *cgm = &(ipvm->getConstRefToCharacteristicLengthMatrix(nlk));           // by Wu Ling
            const STensor3 *cgp = &(ipvp->getConstRefToCharacteristicLengthMatrix(nlk));           // by Wu Ling
            
            double epljump = ipvp->getConstRefToNonLocalJump()(nlk);
            SVector3 gradm = (ipvm->getConstRefToGradNonLocalVariable()[nlk]);
            SVector3 gradp = (ipvp->getConstRefToGradNonLocalVariable()[nlk]);
            if (_incrementNonlocalBased){
              epljump -= ipvpprev->getConstRefToNonLocalJump()(nlk);
              gradm -= (ipvmprev->getConstRefToGradNonLocalVariable()[nlk]);
              gradp -= (ipvpprev->getConstRefToGradNonLocalVariable()[nlk]);
            }

            SVector3 cgGradm;
            SVector3 cgGradp;

            for(int k =0; k<3; k++){
              cgGradm[k] = 0.;
              cgGradp[k] = 0.;
              for(int l =0; l<3; l++){
                cgGradm[k] += cgm->operator()(k,l)*gradm(l);
                cgGradp[k] += cgp->operator()(k,l)*gradp(l);
              }
            }

            // consistency \mean{cg*grad eps}
            double meanCgGradN =0.;
            for(int k =0; k<3; k++){
              meanCgGradN += (cgGradm(k)+cgGradp(k))*(nu(k)/2.);
            }
            // Stability  N \mean{H} (jump N)
            double NMeanCgJumpNBetasc = 0.;
            static STensor3 meanCg;
            meanCg = (*cgm);
            meanCg+= (*cgp);
            meanCg*= (0.5);
            for(int l = 0; l <3; l++){
              for(int n = 0; n <3; n++){
                NMeanCgJumpNBetasc += meanCg(l,n)*epljump*nu(n)*nbetahs*nu(l);
              }
            }
            // Assembly consistency + stability (if not broken)
            for(int j=0;j<nbFFm;j++){
              m(j+nlk*nbFFm+3*nbFFm) -= (meanCgGradN+NMeanCgJumpNBetasc)*(Valsm[j+nlk*nbFFm+3*nbFFm]*wJ*getNonLocalEqRatio());
            }
            for(int j=0;j<nbFFp;j++){
              m(j+nlk*nbFFp+3*nbFFp+nbdofm) += (meanCgGradN+NMeanCgJumpNBetasc)*(Valsp[j+nlk*nbFFp+3*nbFFp]*wJ*getNonLocalEqRatio());
            }
            // compatibility membrane (4 terms to assembly)
            SVector3 CgmJumpN;
            SVector3 CgpJumpN;

            for(int k = 0; k <3; k++){
              CgmJumpN(k) = 0.;
              CgpJumpN(k) = 0.;
              for(int m = 0; m <3; m++){
                CgmJumpN(k) += cgm->operator()(k,m)*epljump*nu(m);
                CgpJumpN(k) += cgp->operator()(k,m)*epljump*nu(m);
              }
            }
            // Assembly (loop on shape function)
            for(int j=0;j<nbFFm;j++){
              m(j+nlk*nbFFm+3*nbFFm) += (CgmJumpN(0)*Gradsm[j+nlk*nbFFm+3*nbFFm][0]) *(wJ*getNonLocalEqRatio()/2.);
              m(j+nlk*nbFFm+3*nbFFm) += (CgmJumpN(2)*Gradsm[j+nlk*nbFFm+3*nbFFm][2]) *(wJ*getNonLocalEqRatio()/2.);
            }
            for(int j=0;j<nbFFp;j++){
              m(j+nlk*nbFFp+3*nbFFp+nbdofm) += (CgpJumpN(0)*Gradsp[j+nlk*nbFFp+3*nbFFp][0])*(wJ*getNonLocalEqRatio()/2.);
              m(j+nlk*nbFFp+3*nbFFp+nbdofm) += (CgpJumpN(2)*Gradsp[j+nlk*nbFFp+3*nbFFp][2])*(wJ*getNonLocalEqRatio()/2.);
            }
          }
        }
        
        
        // constitutive extra dof - not verified !!!!
        if( ipvm->getNumConstitutiveExtraDofDiffusionVariable()>getNumConstitutiveExtraDofDiffusionVariable() ) 
            Msg::Error("Your material law uses more constitutive extra dof variables than your domain dG3DForceInter::get");
        if( ipvp->getNumConstitutiveExtraDofDiffusionVariable()>getNumConstitutiveExtraDofDiffusionVariable() ) 
            Msg::Error("Your material law uses more constitutive extra dof variables than your domain dG3DForceInter::get");
        if( ipvp->getNumConstitutiveExtraDofDiffusionVariable()!= ipvm->getNumConstitutiveExtraDofDiffusionVariable() ) 
            Msg::Error("Your plus and minus material laws do not use the same number of constitutive extra dof variables dG3DForceInter::get");
        for (int extraDOFField = 0; extraDOFField< ipvp->getNumConstitutiveExtraDofDiffusionVariable(); extraDOFField++){
          if( getConstitutiveExtraDofDiffusionContinuity() && !broken)
          {
            Msg::Error("axisymmetricDG3DForceInter::get : ExtraDofterms not implemented");
            
            /// stability parameter including mesh size and radial position
            const double nbetahs = getConstitutiveExtraDofDiffusionStabilityParameter() / hs / point[0];

            double fieldJump = ipvp->getConstRefToFieldJump()(extraDOFField);
  
            const double &fieldp = ipvp->getConstRefToField(extraDOFField);
            const double &fieldm = ipvm->getConstRefToField(extraDOFField);
            const double &oneOverFieldJump = ipvp->getConstRefToOneOverFieldJump()(extraDOFField);

            static STensor3  k0m;
            k0m = *(ipvm->getConstRefToLinearK()[extraDOFField][extraDOFField]);
            static STensor3  k0p;
            k0p = *(ipvp->getConstRefToLinearK()[extraDOFField][extraDOFField]);

            static STensor3  Stiff_alphadialitationm;
            Stiff_alphadialitationm = *(ipvm->getConstRefToLinearSymmetrizationCoupling()[extraDOFField]);
            static STensor3  Stiff_alphadialitationp;
            Stiff_alphadialitationp = *(ipvp->getConstRefToLinearSymmetrizationCoupling()[extraDOFField]);
            if(getConstitutiveExtraDofDiffusionUseEnergyConjugatedField())
            {
              k0m*=(-1.)*fieldm*fieldm;
              k0p*=(-1.)*fieldp*fieldp;
              fieldJump=oneOverFieldJump;
              Stiff_alphadialitationm*= (-1.)*fieldm*fieldm;
              Stiff_alphadialitationp*= (-1.)*fieldp*fieldp;
            }
            const SVector3  &fluxm = (ipvm->getConstRefToFlux()[extraDOFField]);
            const SVector3  &fluxp = (ipvp->getConstRefToFlux()[extraDOFField]);
            // consistency \mean{cg*grad eps}
            double meantflux =0.;
            for(int k =0; k<3; k++)
            {
              meantflux += (fluxm(k)+fluxp(k))*(nu(k)/2.);
            }
            double NMeank0JumpNBetasc = 0.;
            static STensor3 meank;
            meank = (k0m);
            meank+= (k0p);
            meank*= (0.5);

            for(int l = 0; l <3; l++)
            {
              for(int n = 0; n <3; n++)
              {
                NMeank0JumpNBetasc += meank(l,n)*fieldJump*nu(n)*nbetahs*nu(l);
              }
            }

            double Stiff_alphadialitationp_JumpN=0.;
            double Stiff_alphadialitationm_JumpN=0.;
            for(int k = 0; k <3; k++)
            {
              for(int m = 0; m <3; m++)
              {
                Stiff_alphadialitationp_JumpN += Stiff_alphadialitationp(k,m)*jump(k)*nu(m);
                Stiff_alphadialitationm_JumpN += Stiff_alphadialitationm(k,m)*jump(k)*nu(m);
              }
            }
            int indexField=3+getNumNonLocalVariable()+extraDOFField;

            for(int j=0;j<nbFFm;j++)
  
            {
              m(j+indexField*nbFFm) -= ( meantflux+NMeank0JumpNBetasc)*(Valsm[j+indexField*nbFFm]*wJ*getConstitutiveExtraDofDiffusionEqRatio());
            }
            for(int j=0;j<nbFFp;j++)
            {
              m(j+indexField*nbFFp+nbdofm) += ( meantflux+NMeank0JumpNBetasc)*(Valsp[j+indexField*nbFFp]*wJ*getConstitutiveExtraDofDiffusionEqRatio());
            }

            SVector3 k0pJumpN;
            STensorOperation::zero(k0pJumpN);
            SVector3 k0mJumpN;
            STensorOperation::zero(k0mJumpN);
            for(int k = 0; k <3; k++)
            {
              for(int m = 0; m <3; m++)
              {
                k0mJumpN(k) += k0m(k,m)*fieldJump*nu(m);
                k0pJumpN(k) += k0p(k,m)*fieldJump*nu(m);
              }
            }
            // Assembly (loop on shape function)
            for(int j=0;j<nbFFm;j++)
            {
              for(int l=0;l<3;l++)
              {
                m(j+indexField*nbFFm) += ( k0mJumpN(l)*Gradsm[j+indexField*nbFFm][l])*(wJ*getConstitutiveExtraDofDiffusionEqRatio()/2.);
              }
            }
            for(int j=0;j<nbFFp;j++)
            {
              for(int l=0;l<3;l++)
              {
                m(j+indexField*nbFFp+nbdofm) += ( k0pJumpN(l)*Gradsp[j+indexField*nbFFp][l])*(wJ*getConstitutiveExtraDofDiffusionEqRatio()/2.);
              }
            }
            //new
            double gamma=0.;
            for(int j=0;j<nbFFm;j++)
            {
              m(j+indexField*nbFFm) += gamma*( Stiff_alphadialitationm_JumpN*Valsm[j+indexField*nbFFm])*(wJ/2.)*(-1.);
            }
            for(int j=0;j<nbFFp;j++)
            {
              m(j+indexField*nbFFp+nbdofm) += gamma*( Stiff_alphadialitationp_JumpN*Valsp[j+indexField*nbFFp])*(wJ/2.)*(-1.);
            }
          }
        }
        
      }
      else // broken case - only cohesive force
      {
        const FractureCohesive3DIPVariable *ipvmf = static_cast<const FractureCohesive3DIPVariable*>(ipvm);
        const FractureCohesive3DIPVariable *ipvpf = static_cast<const FractureCohesive3DIPVariable*>(ipvp);

        static SVector3 interfaceForce;
        interfaceForce = ipvmf->getConstRefToInterfaceForce();
        interfaceForce += ipvpf->getConstRefToInterfaceForce();
        interfaceForce*= 0.5;

        const std::vector<TensorialTraits<double>::ValType> &Valsm = ipvm->f(_minusSpace,ie->getElem(0),GPm[i]);
        const std::vector<TensorialTraits<double>::ValType> &Valsp = ipvp->f(_plusSpace,ie->getElem(1),GPp[i]);

        // Assembly consistency + stability (if not broken)
        for(int j=0;j<nbFFm;j++){
          for(int k=0;k<3;k++){
            m(j+k*nbFFm) -= interfaceForce[k]*(Valsm[j+0*nbFFm]*wJ);
          }
        }
        for(int j=0;j<nbFFp;j++){
          for(int k=0;k<3;k++){
            m(j+k*nbFFp+nbdofm) += interfaceForce[k]*(Valsp[j+0*nbFFp]*wJ);
          }
        }
      }
    
    
    }
  }
}





void axisymmetricDG3DStiffnessInter::get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &stiff) const
{
  MInterfaceElement *ie = dynamic_cast<MInterfaceElement*>(ele);
  const int nbFFm = ie->getElem(0)->getNumVertices();
  const int nbFFp = ie->getElem(1)->getNumVertices();
  const int nbdofm = _minusSpace->getNumKeys(ie->getElem(0));
  const int nbdofp = _plusSpace->getNumKeys(ie->getElem(1));
  stiff.resize(nbdofm+nbdofp,nbdofm+nbdofp);
  stiff.setAll(0.);
  double jac[3][3];
  if(_fullDg)
  {
    
    // characteristic size
    const double hs = ie->getCharacteristicSize();
    const double b1hs = getStabilityParameter()/hs;

    // get gauss's point
    const AllIPState::ipstateElementContainer *vips = _ipf->getAips()->getIPstate(ele->getNum());
    
    // Values of shape functions on minus and plus elements
    IntPt *GPm; IntPt *GPp;
    _interQuad->getIntPoints(ie,GP,&GPm,&GPp);
    
    // Position associated with a GP
    SPoint3 point;
    // Association of derivatives
    fullMatrix<double> dFdqm, dFdqp;
    dFdqm.resize(9,3*nbFFm);
    dFdqp.resize(9,3*nbFFp);
    
    // loop on Gauss Point
    for(int i=0;i<npts; i++)
    {
      // IP
      const IPStateBase *ipsm        = (*vips)[i];
      const dG3DIPVariableBase *ipvm     = static_cast<const dG3DIPVariableBase*>(ipsm->getState(IPStateBase::current));
      const dG3DIPVariableBase *ipvmprev = static_cast<const dG3DIPVariableBase*>(ipsm->getState(IPStateBase::previous));
      const dG3DIPVariableBase *ipvm0    = static_cast<const dG3DIPVariableBase*>(ipsm->getState(IPStateBase::initial));

      const SVector3 &referenceOutwardNormalm = ipvm->getConstRefToReferenceOutwardNormal();

      const IPStateBase *ipsp        = (*vips)[i+npts];
      const dG3DIPVariableBase *ipvp     = static_cast<const dG3DIPVariableBase*>(ipsp->getState(IPStateBase::current));
      const dG3DIPVariableBase *ipvp0    = static_cast<const dG3DIPVariableBase*>(ipsp->getState(IPStateBase::initial));

      const SVector3 &referenceOutwardNormalp = ipvp->getConstRefToReferenceOutwardNormal();
      const SVector3 &jump = ipvp->getConstRefToJump();

      // Weight of Gauss' point i and Jacobian's value at this point
      const double weight = GP[i].weight;
      const double& detJ = ipvm->getJacobianDeterminant(ie,GP[i]);
      ele->pnt(GP[i].pt[0],GP[i].pt[1],GP[i].pt[2],point);
      const double wJonR = 2.*M_PI*detJ* weight;
      const double wJ = wJonR*point[0];
      
      const std::vector<TensorialTraits<double>::GradType> &Gradsm = ipvm->gradf(_minusSpace,ie->getElem(0),GPm[i]);
      const std::vector<TensorialTraits<double>::GradType> &Gradsp = ipvp->gradf(_plusSpace,ie->getElem(1),GPp[i]);

      // possibility of fracture
      bool broken = false;
      if(_mlawMinus->getType() == materialLaw::fracture )
      {
        const FractureCohesive3DIPVariable *ipvwf = static_cast<const FractureCohesive3DIPVariable*>(ipvm); // broken via minus (OK broken on both sides)
        broken = ipvwf->isbroken();
      }

      static SVector3 nu;
      nu = referenceOutwardNormalm;
      nu.normalize();

      const std::vector<TensorialTraits<double>::ValType> &Valsm = ipvm->f(_minusSpace,ie->getElem(0),GPm[i]);
      const std::vector<TensorialTraits<double>::ValType> &Valsp = ipvp->f(_plusSpace,ie->getElem(1),GPp[i]);


      // compute dF d dof
      dFdqm.setAll(0.);
      for (int j=0; j< nbFFm; j++){
        // comp rr
        int row = Tensor23::getIndex(0,0);
        dFdqm(row,j) = Gradsm[j+0*nbFFm](0);
        // comp rz
        row = Tensor23::getIndex(0,2);
        dFdqm(row,j) = Gradsm[j+0*nbFFm](2);
        // comp tt
        row = Tensor23::getIndex(1,1);
        dFdqm(row,j) = Valsm[j+0*nbFFm]/point[0];

        // comp zr
        row = Tensor23::getIndex(2,0);
        dFdqm(row,j+2*nbFFm) = Gradsm[j+0*nbFFm](0);
        
        // comp zz
        row = Tensor23::getIndex(2,2);
        dFdqm(row,j+2*nbFFm) = Gradsm[j+0*nbFFm](2);
      }
      
      dFdqp.setAll(0.);
      for (int j=0; j< nbFFp; j++){
        // comp rr
        int row = Tensor23::getIndex(0,0);
        dFdqp(row,j) = Gradsp[j+0*nbFFp](0);
        // comp rz
        row = Tensor23::getIndex(0,2);
        dFdqp(row,j) = Gradsp[j+0*nbFFp](2);
        // comp tt
        row = Tensor23::getIndex(1,1);
        dFdqp(row,j) = Valsp[j+0*nbFFp]/point[0];

        // comp zr
        row = Tensor23::getIndex(2,0);
        dFdqp(row,j+2*nbFFp) = Gradsp[j+0*nbFFp](0);
        
        // comp zz
        row = Tensor23::getIndex(2,2);
        dFdqp(row,j+2*nbFFp) = Gradsp[j+0*nbFFp](2);
      }



      if(broken)
      {
        const FractureCohesive3DIPVariable *ipvmf = static_cast<const FractureCohesive3DIPVariable*>(ipvm);
        const FractureCohesive3DIPVariable *ipvpf = static_cast<const FractureCohesive3DIPVariable*>(ipvp);

        static STensor3 DInterfaceForceDjump;
        DInterfaceForceDjump = ipvmf->getConstRefToDInterfaceForceDjump();
        DInterfaceForceDjump += ipvpf->getConstRefToDInterfaceForceDjump();
        DInterfaceForceDjump *= 0.5;

        // Assembly of DInterfaceForceDjump associated contribution
        for(int j=0;j<nbFFm;j++){
          for(int k=0;k<3;k++){
            
            for(int l=0;l<nbFFm;l++){
              for(int m=0;m<3;m++){
                stiff(j+k*nbFFm,l+m*nbFFm) += DInterfaceForceDjump(k,m)*(Valsm[j+0*nbFFm]*Valsm[l+0*nbFFm]*wJ);
              }
            }
            for(int l=0;l<nbFFp;l++){
              for(int m=0;m<3;m++){
                stiff(j+k*nbFFm,l+m*nbFFp+nbdofm) -= DInterfaceForceDjump(k,m)*(Valsm[j+0*nbFFm]*Valsp[l+0*nbFFp]*wJ);
              }
            }
          
          }
        }
        
        for(int j=0;j<nbFFp;j++){
          for(int k=0;k<3;k++){
            
            for(int l=0;l<nbFFm;l++){
              for(int m=0;m<3;m++){
                stiff(j+k*nbFFp+nbdofm,l+m*nbFFm) -= DInterfaceForceDjump(k,m)*(Valsp[j+0*nbFFp]*Valsm[l+0*nbFFm]*wJ);
              }
            }
            for(int l=0;l<nbFFp;l++){
              for(int m=0;m<3;m++){
                stiff(j+k*nbFFp+nbdofm,l+m*nbFFp+nbdofm) += DInterfaceForceDjump(k,m)*(Valsp[j+0*nbFFp]*Valsp[l+0*nbFFp]*wJ);
              }
            }
          
          }
        }
        
        
        if (ipvmf->withDeformationGradient() and ipvpf->withDeformationGradient()){
          // get dforce dF bulk
          const STensor33 &DInterfaceForceDFm = ipvmf->getConstRefToDInterfaceForceDDeformationGradient();
          const STensor33 &DInterfaceForceDFp = ipvpf->getConstRefToDInterfaceForceDDeformationGradient();
          
          int r,s;
          for(int j=0;j<nbFFm;j++){
            for(int k=0;k<3;k++){
             
              for (int p=0; p<3*nbFFm; p++){
                for (int q=0; q<9; q++){
                  Tensor23::getIntsFromIndex(q,r,s);
                  stiff(j+k*nbFFm,p) -= DInterfaceForceDFm(k,r,s)*dFdqm(q,p)*Valsm[j+0*nbFFm]*wJ*0.5;
                }
              }
              for (int p=0; p<3*nbFFp; p++){
                for (int q=0; q<9; q++){
                  Tensor23::getIntsFromIndex(q,r,s);
                  stiff(j+k*nbFFm,p+nbdofm) -= DInterfaceForceDFp(k,r,s)*dFdqp(q,p)*Valsm[j+0*nbFFm]*wJ*0.5;
                }
              }
             
            }
          }

          for(int j=0;j<nbFFp;j++){
            for(int k=0;k<3;k++){
              
              for (int p=0; p<3*nbFFm; p++){
                for (int q=0; q<9; q++){
                  Tensor23::getIntsFromIndex(q,r,s);
                  stiff(j+k*nbFFp+nbdofm,p) += DInterfaceForceDFm(k,r,s)*dFdqm(q,p)*Valsp[j+0*nbFFp]*wJ*0.5;
                }
              }
              for (int p=0; p<3*nbFFp; p++){
                for (int q=0; q<9; q++){
                  Tensor23::getIntsFromIndex(q,r,s);
                  stiff(j+k*nbFFp+nbdofm,p+nbdofm) += DInterfaceForceDFp(k,r,s)*dFdqp(q,p)*Valsp[j+0*nbFFp]*wJ*0.5;
                }
              }
            }
          }

        }
        
        if (ipvmf->withJumpGradient() and ipvpf->withJumpGradient()){
          //std::vector<TensorialTraits<double>::GradType> gradValInt;
          //_minusSpace->gradf(ele,GP[i].pt[0],GP[i].pt[1],GP[i].pt[2],gradValInt);
          std::vector<TensorialTraits<double>::GradType>& gradValInt = ipvm->gradf(_minusSpace,ie,GP[i]);

          int nbvertexInter = ele->getNumVertices();
          std::vector<int> vm;
          vm.resize(nbvertexInter);
          ie->getLocalVertexNum(0,vm);
          std::vector<int> vp;
          vp.resize(nbvertexInter);
          ie->getLocalVertexNum(1,vp);

          static STensor33 DInterfaceForceDjumpGrad;
          DInterfaceForceDjumpGrad = ipvmf->getConstRefToDInterfaceForceDjumpGradient();
          DInterfaceForceDjumpGrad += ipvpf->getConstRefToDInterfaceForceDjumpGradient();
          DInterfaceForceDjumpGrad *= 0.5;
          

          for(int j=0;j<nbFFm;j++){
            for(int k=0;k<3;k++){
              for(int l=0;l<nbvertexInter;l++){
                for(int m=0;m<3;m++){
                  for(int n=0;n<3;n++){
                    stiff(j+k*nbFFm,vm[l]+m*nbFFm) += DInterfaceForceDjumpGrad(k,m,n)*gradValInt[l+m*nbvertexInter](n)*Valsm[j+0*nbFFm]*wJ;
                    stiff(j+k*nbFFm,vp[l]+m*nbFFp+nbdofm) -= DInterfaceForceDjumpGrad(k,m,n)*gradValInt[l+m*nbvertexInter](n)*Valsm[j+0*nbFFm]*wJ;
                  }       
                }
              }
            }
          }
          
          for(int j=0;j<nbFFp;j++){
            for(int k=0;k<3;k++){
              for(int l=0;l<nbvertexInter;l++){
                for(int m=0;m<3;m++){
                  for(int n=0;n<3;n++){
                    stiff(j+k*nbFFp+nbdofm,vm[l]+m*nbFFm) += DInterfaceForceDjumpGrad(k,m,n)*gradValInt[l+m*nbvertexInter](n)*Valsp[j+0*nbFFp]*wJ;
                    stiff(j+k*nbFFp+nbdofm,vp[l]+m*nbFFp+nbdofm) -= DInterfaceForceDjumpGrad(k,m,n)*gradValInt[l+m*nbvertexInter](n)*Valsp[j+0*nbFFp]*wJ;
                  }       
                }
              }
            }
          }
        }
        // Here we have extra dof part
        if( ipvm->getNumConstitutiveExtraDofDiffusionVariable()>getNumConstitutiveExtraDofDiffusionVariable() ) 
            Msg::Error("Your material law uses more constitutive extra dof variable than your domain dG3DStiffnessInter::get");
        if( ipvp->getNumConstitutiveExtraDofDiffusionVariable()>getNumConstitutiveExtraDofDiffusionVariable() ) 
            Msg::Error("Your material law uses more constitutive extra dof variable than your domain dG3DStiffnessInter::get");
        if( ipvp->getNumConstitutiveExtraDofDiffusionVariable()!= ipvm->getNumConstitutiveExtraDofDiffusionVariable() ) 
            Msg::Error("Your plus and minus material laws do not use the same number of constitutive extra dof variables dG3DStiffnessInter::get");
        for (int extraDOFField = 0; extraDOFField< ipvp->getNumConstitutiveExtraDofDiffusionVariable(); extraDOFField++){
            Msg::Error("Need to implement the stiffness matrix related to the CZM/CBM and constitutiveExtraDOFVariable dG3DStiffnessInter::get");
        }
      }
      else
      {
        // Consistency term linked to \mean{P}
        const STensor43 &Hp = ipvp->getConstRefToTangentModuli();
        const STensor43 &Hm = ipvm->getConstRefToTangentModuli();
        static STensor33 HNm;
        static STensor33 HNp;
        STensorOperation::zero(HNm);
        STensorOperation::zero(HNp);
        for(int l=0; l<3; l++){
          for(int m=0; m<3; m++){
            for(int n=0; n<3; n++){ 
              HNm(0,m,n) += Hm(0,l,m,n)*(nu(l)/2.);
              HNm(2,m,n) += Hm(2,l,m,n)*(nu(l)/2.);
              HNp(0,m,n) += Hp(0,l,m,n)*(nu(l)/2.);
              HNp(2,m,n) += Hp(2,l,m,n)*(nu(l)/2.);
            }
          }
        }
        // assembly
        for(int j=0;j<nbFFm;j++){
          for(int k=0;k<3;k++){
            for(int l=0;l<nbFFm;l++){
              stiff(j+k*nbFFm,l+0*nbFFm) -= HNm(k,0,0)*(Gradsm[l+0*nbFFm][0])      *(Valsm[j+0*nbFFm]*wJ);
              stiff(j+k*nbFFm,l+0*nbFFm) -= HNm(k,0,2)*(Gradsm[l+0*nbFFm][2])      *(Valsm[j+0*nbFFm]*wJ);
              stiff(j+k*nbFFm,l+0*nbFFm) -= HNm(k,1,1)*(Valsm[l+0*nbFFm]/point[0]) *(Valsm[j+0*nbFFm]*wJ);
              stiff(j+k*nbFFm,l+2*nbFFm) -= HNm(k,2,0)*(Gradsm[l+2*nbFFm][0])      *(Valsm[j+0*nbFFm]*wJ);
              stiff(j+k*nbFFm,l+2*nbFFm) -= HNm(k,2,2)*(Gradsm[l+2*nbFFm][2])      *(Valsm[j+0*nbFFm]*wJ);
            }
            for(int l=0;l<nbFFp;l++){
              stiff(j+k*nbFFm,l+0*nbFFm+nbdofm) -= HNp(k,0,0)*(Gradsp[l+0*nbFFp][0])     *(Valsm[j+0*nbFFm]*wJ);
              stiff(j+k*nbFFm,l+0*nbFFm+nbdofm) -= HNp(k,0,2)*(Gradsp[l+0*nbFFp][2])     *(Valsm[j+0*nbFFm]*wJ);
              stiff(j+k*nbFFm,l+0*nbFFm+nbdofm) -= HNp(k,1,1)*(Valsp[l+0*nbFFp]/point[0])*(Valsm[j+0*nbFFm]*wJ);
              stiff(j+k*nbFFm,l+2*nbFFm+nbdofm) -= HNp(k,2,0)*(Gradsp[l+2*nbFFp][0])     *(Valsm[j+0*nbFFm]*wJ);
              stiff(j+k*nbFFm,l+2*nbFFm+nbdofm) -= HNp(k,2,2)*(Gradsp[l+2*nbFFp][2])     *(Valsm[j+0*nbFFm]*wJ);
            }
          }
        }
        
        for(int j=0;j<nbFFp;j++){
          for(int k=0;k<3;k++){
            for(int l=0;l<nbFFm;l++){
              stiff(j+k*nbFFp+nbdofm,l+0*nbFFm) += HNm(k,0,0)*(Gradsm[l+0*nbFFm][0])     *(Valsp[j+0*nbFFp]*wJ);
              stiff(j+k*nbFFp+nbdofm,l+0*nbFFm) += HNm(k,0,2)*(Gradsm[l+0*nbFFm][2])     *(Valsp[j+0*nbFFp]*wJ);
              stiff(j+k*nbFFp+nbdofm,l+0*nbFFm) += HNm(k,1,1)*(Valsm[l+0*nbFFm]/point[0])*(Valsp[j+0*nbFFp]*wJ);
              stiff(j+k*nbFFp+nbdofm,l+2*nbFFm) += HNm(k,2,0)*(Gradsm[l+2*nbFFm][0])     *(Valsp[j+0*nbFFp]*wJ);
              stiff(j+k*nbFFp+nbdofm,l+2*nbFFm) += HNm(k,2,2)*(Gradsm[l+2*nbFFm][2])     *(Valsp[j+0*nbFFp]*wJ);
            }
            for(int l=0;l<nbFFp;l++){
              stiff(j+k*nbFFm+nbdofm,l+0*nbFFm+nbdofm) += HNp(k,0,0)*(Gradsp[l+0*nbFFp][0])      *(Valsp[j+0*nbFFp]*wJ);
              stiff(j+k*nbFFm+nbdofm,l+0*nbFFm+nbdofm) += HNp(k,0,2)*(Gradsp[l+0*nbFFp][2])      *(Valsp[j+0*nbFFp]*wJ);
              stiff(j+k*nbFFm+nbdofm,l+0*nbFFm+nbdofm) += HNp(k,1,1)*(Valsp[l+0*nbFFp]/point[0]) *(Valsp[j+0*nbFFp]*wJ);
              stiff(j+k*nbFFm+nbdofm,l+2*nbFFm+nbdofm) += HNp(k,2,0)*(Gradsp[l+2*nbFFp][0])      *(Valsp[j+0*nbFFp]*wJ);
              stiff(j+k*nbFFm+nbdofm,l+2*nbFFm+nbdofm) += HNp(k,2,2)*(Gradsp[l+2*nbFFp][2])      *(Valsp[j+0*nbFFp]*wJ);
            }
          }
        }
        
        // Stability derivative N \mean{H} (jump N)       
        const STensor43 &eHp = ipvp->getConstRefToDGElasticTangentModuli();
        const STensor43 &eHm = ipvm->getConstRefToDGElasticTangentModuli();
        static STensor3 NMeanHNBetasc;
        STensorOperation::zero(NMeanHNBetasc);
        for(int m = 0; m <3; m++){
          for(int l = 0; l <3; l++){
            for(int n = 0; n <3; n++){
              NMeanHNBetasc(0,m) += (eHp(0,l,m,n)+eHm(0,l,m,n))*nu(n)*b1hs*nu(l)/2.;
              NMeanHNBetasc(2,m) += (eHp(2,l,m,n)+eHm(2,l,m,n))*nu(n)*b1hs*nu(l)/2.;
            }
          }
        }
        // Assembly stability (if not broken)
        for(int j=0;j<nbFFm;j++){
          for(int k=0;k<3;k++){
            for(int l=0;l<nbFFm;l++){
              for(int m=0;m<3;m++){
                stiff(j+k*nbFFm,l+m*nbFFm) += NMeanHNBetasc(k,m)*(Valsm[j+0*nbFFm]*Valsm[l+0*nbFFm]*wJonR);
              }
            }
            for(int l=0;l<nbFFp;l++){
              for(int m=0;m<3;m++){
                stiff(j+k*nbFFm,l+m*nbFFp+nbdofm) -= NMeanHNBetasc(k,m)*(Valsm[j+0*nbFFm]*Valsp[l+0*nbFFp]*wJonR);
              }
            }
          }
        }
        for(int j=0;j<nbFFp;j++){
          for(int k=0;k<3;k++){
            for(int l=0;l<nbFFm;l++){
              for(int m=0;m<3;m++)
              {
                stiff(j+k*nbFFp+nbdofm,l+m*nbFFm) -= NMeanHNBetasc(k,m)*(Valsp[j+0*nbFFp]*Valsm[l+0*nbFFm]*wJonR);
              }
            }
            for(int l=0;l<nbFFp;l++){
              for(int m=0;m<3;m++){
                stiff(j+k*nbFFp+nbdofm,l+m*nbFFp+nbdofm) += NMeanHNBetasc(k,m)*(Valsp[j+0*nbFFp]*Valsp[l+0*nbFFp]*wJonR);
              }
            }
          }
        }
        
        // Compatibility membrane 
        static STensor33 HmN;
        static STensor33 HpN;

        for(int k = 0; k <3; k++){
          for(int l = 0; l <3; l++){
            for(int m = 0; m <3; m++){
              HmN(k,l,m) = 0.;
              HpN(k,l,m) = 0.;
              for(int n = 0; n <3; n++){
                HmN(k,l,m) += ipvm->getConstRefToDGElasticTangentModuli()(m,n,k,l)*nu(n);
                HpN(k,l,m) += ipvp->getConstRefToDGElasticTangentModuli()(m,n,k,l)*nu(n);
              }
            }
          }
        }
        
        for(int j=0;j<nbFFm;j++){
          for(int l=0;l<nbFFm;l++){
            for(int m=0;m<3;m++){
              stiff(j+0*nbFFm,l+m*nbFFm) -= (HmN(0,0,m)*Valsm[l+0*nbFFm]  *Gradsm[j+0*nbFFm][0])*(wJ/2.);
              stiff(j+0*nbFFm,l+m*nbFFm) -= (HmN(0,2,m)*Valsm[l+0*nbFFm]  *Gradsm[j+0*nbFFm][2])*(wJ/2.);
              stiff(j+0*nbFFm,l+m*nbFFm) -= (HmN(1,1,m)*Valsm[l+0*nbFFm]  *Valsm[j+nbFFm])*(wJ/2.)/point[0];
              stiff(j+2*nbFFm,l+m*nbFFm) -= (HmN(2,0,m)*Valsm[l+0*nbFFm]  *Gradsm[j+0*nbFFm][0])*(wJ/2.);
              stiff(j+2*nbFFm,l+m*nbFFm) -= (HmN(2,2,m)*Valsm[l+0*nbFFm]  *Gradsm[j+0*nbFFm][2])*(wJ/2.);
            }
          }
          for(int l=0;l<nbFFp;l++){
            for(int m=0;m<3;m++){
              stiff(j+0*nbFFm,l+m*nbFFp+nbdofm) += (HmN(0,0,m)*Valsp[l+0*nbFFp]  *Gradsm[j+0*nbFFm][0])*(wJ/2.);
              stiff(j+0*nbFFm,l+m*nbFFp+nbdofm) += (HmN(0,2,m)*Valsp[l+0*nbFFp]  *Gradsm[j+0*nbFFm][2])*(wJ/2.);
              stiff(j+0*nbFFm,l+m*nbFFp+nbdofm) += (HmN(1,1,m)*Valsp[l+0*nbFFp]  *Valsm[j+nbFFm])*(wJ/2.)/point[0];
              stiff(j+2*nbFFm,l+m*nbFFp+nbdofm) += (HmN(2,0,m)*Valsp[l+0*nbFFp]  *Gradsm[j+0*nbFFm][0])*(wJ/2.);
              stiff(j+2*nbFFm,l+m*nbFFp+nbdofm) += (HmN(2,2,m)*Valsp[l+0*nbFFp]  *Gradsm[j+0*nbFFm][2])*(wJ/2.);
            }
          }
        }
        
        for(int j=0;j<nbFFp;j++){
          for(int l=0;l<nbFFm;l++){
            for(int m=0;m<3;m++){
              stiff(j+0*nbFFp+nbdofm,l+m*nbFFm) -= (HpN(0,0,m)*Valsm[l+0*nbFFm]  *Gradsp[j+0*nbFFp][0])*(wJ/2.);
              stiff(j+0*nbFFp+nbdofm,l+m*nbFFm) -= (HpN(0,2,m)*Valsm[l+0*nbFFm]  *Gradsp[j+0*nbFFp][2])*(wJ/2.);
              stiff(j+0*nbFFp+nbdofm,l+m*nbFFm) -= (HpN(1,1,m)*Valsm[l+0*nbFFm]  *Valsp[j+nbFFp])*(wJ/2.)/point[0];
              stiff(j+2*nbFFp+nbdofm,l+m*nbFFm) -= (HpN(2,0,m)*Valsm[l+0*nbFFm]  *Gradsp[j+0*nbFFp][0])*(wJ/2.);
              stiff(j+2*nbFFp+nbdofm,l+m*nbFFm) -= (HpN(2,2,m)*Valsm[l+0*nbFFm]  *Gradsp[j+0*nbFFp][2])*(wJ/2.);
            }
          }
          for(int l=0;l<nbFFp;l++){
            for(int m=0;m<3;m++){
              stiff(j+0*nbFFp+nbdofm,l+m*nbFFp+nbdofm) += (HpN(0,0,m)*Valsp[l+0*nbFFp]  *Gradsp[j+0*nbFFp][0])*(wJ/2.);
              stiff(j+0*nbFFp+nbdofm,l+m*nbFFp+nbdofm) += (HpN(0,2,m)*Valsp[l+0*nbFFp]  *Gradsp[j+0*nbFFp][2])*(wJ/2.);
              stiff(j+0*nbFFp+nbdofm,l+m*nbFFp+nbdofm) += (HpN(1,1,m)*Valsp[l+0*nbFFp]  *Valsp[j+nbFFp])*(wJ/2.)/point[0];
              stiff(j+2*nbFFp+nbdofm,l+m*nbFFp+nbdofm) += (HpN(2,0,m)*Valsp[l+0*nbFFp]  *Gradsp[j+0*nbFFp][0])*(wJ/2.);
              stiff(j+2*nbFFp+nbdofm,l+m*nbFFp+nbdofm) += (HpN(2,2,m)*Valsp[l+0*nbFFp]  *Gradsp[j+0*nbFFp][2])*(wJ/2.);
            }
          }
        }


        // Non local
        if( ipvm->getNumberNonLocalVariable()>getNumNonLocalVariable() ) Msg::Error("Your minus material law uses more non local variables than your domain dG3DStiffnessInter::get");
        if( ipvp->getNumberNonLocalVariable()>getNumNonLocalVariable() ) Msg::Error("Your plus material law uses more non local variables than your domain dG3DStiffnessInter::get");
        if( getNonLocalContinuity() && !broken)
        {
          /// stability parameter including mesh size and radial position
          const double nbetahs = getNonLocalStabilityParameter() / hs / point[0];
          
          for (int nlk = 0; nlk < ipvp->getNumberNonLocalVariable(); nlk++)
          {
            
            
            const STensor3  *cgm = &(ipvm->getConstRefToCharacteristicLengthMatrix(nlk));
            const STensor3  *cgp = &(ipvp->getConstRefToCharacteristicLengthMatrix(nlk));
            const STensor3  *dsdpp = &(ipvp->getConstRefToDStressDNonLocalVariable()[nlk]);
            const STensor3  *dsdpm = &(ipvm->getConstRefToDStressDNonLocalVariable()[nlk]);

            // Assembly consistency (from derivative of sigma with tilde{p})
            for(int j=0;j<nbFFm;j++){
              for(int k=0;k<3;k++){
                
                for(int l=0;l<nbFFm;l++){
                  for(int q = 0; q< 3; q++){
                    stiff(j+k*nbFFm,l+nlk*nbFFm+3*nbFFm) -= dsdpm->operator()(k,q)*(Valsm[l+nlk*nbFFm+3*nbFFm]*Valsm[j+k*nbFFm]*wJ/2.*nu(q));
                  }
                }
                for(int l=0;l<nbFFp;l++){
                  for(int q = 0; q< 3; q++){
                    stiff(j+k*nbFFm,l+nlk*nbFFp+3*nbFFp+nbdofm) -= dsdpp->operator()(k,q)*(Valsp[l+nlk*nbFFp+3*nbFFp]*Valsm[j+k*nbFFm]*wJ/2.*nu(q));
                  }
                }
              }
            }
            for(int j=0;j<nbFFp;j++){
              for(int k=0;k<3;k++){
                
                for(int l=0;l<nbFFm;l++){
                  for(int q = 0; q< 3; q++){
                    stiff(j+k*nbFFp+nbdofm,l+nlk*nbFFm+3*nbFFm) += dsdpm->operator()(k,q)*(Valsm[l+nlk*nbFFm+3*nbFFm]*Valsp[j+k*nbFFp]*wJ/2.*nu(q));
                  }
                }
                for(int l=0;l<nbFFp;l++){
                  for(int q = 0; q< 3; q++){
                    stiff(j+k*nbFFp+nbdofm,l+nlk*nbFFp+3*nbFFp+nbdofm) += dsdpp->operator()(k,q)*(Valsp[l+nlk*nbFFp+3*nbFFp]*Valsp[j+k*nbFFp]*wJ/2.*nu(q));
                  }
                }
              }
            }


            // Assembly consistency D\mean{cG grad epl} Depl
            for(int j=0;j<nbFFm;j++)
            {
              
              for(int l=0;l<nbFFm;l++){
                for(int q = 0; q< 3; q++){
                  stiff(j+nlk*nbFFm+3*nbFFm,l+nlk*nbFFm+3*nbFFm) -= cgm->operator()(0,q)*(Gradsm[l+nlk*nbFFm+3*nbFFm][0]*Valsm[j+nlk*nbFFm+3*nbFFm]*wJ*getNonLocalEqRatio()/2.*nu(q));
                  stiff(j+nlk*nbFFm+3*nbFFm,l+nlk*nbFFm+3*nbFFm) -= cgm->operator()(2,q)*(Gradsm[l+nlk*nbFFm+3*nbFFm][2]*Valsm[j+nlk*nbFFm+3*nbFFm]*wJ*getNonLocalEqRatio()/2.*nu(q));
                }
              }
              for(int l=0;l<nbFFp;l++){
                for(int q = 0; q< 3; q++){
                  stiff(j+nlk*nbFFm+3*nbFFm,l+nlk*nbFFp+3*nbFFp+nbdofm) -= cgp->operator()(0,q)*(Gradsp[l+nlk*nbFFp+3*nbFFp][0]*Valsm[j+nlk*nbFFm+3*nbFFm]*wJ*getNonLocalEqRatio()/2.*nu(q));
                  stiff(j+nlk*nbFFm+3*nbFFm,l+nlk*nbFFp+3*nbFFp+nbdofm) -= cgp->operator()(2,q)*(Gradsp[l+nlk*nbFFp+3*nbFFp][2]*Valsm[j+nlk*nbFFm+3*nbFFm]*wJ*getNonLocalEqRatio()/2.*nu(q));
                }
              }
            }
            
            for(int j=0;j<nbFFp;j++)
            {
              for(int l=0;l<nbFFm;l++){
                for(int q = 0; q< 3; q++){
                  stiff(j+nlk*nbFFp+3*nbFFp+nbdofm,l+nlk*nbFFm+3*nbFFm) += cgm->operator()(0,q)*(Gradsm[l+nlk*nbFFm+3*nbFFm][0]*Valsp[j+nlk*nbFFp+3*nbFFp]*wJ*getNonLocalEqRatio()/2.*nu(q));
                  stiff(j+nlk*nbFFp+3*nbFFp+nbdofm,l+nlk*nbFFm+3*nbFFm) += cgm->operator()(2,q)*(Gradsm[l+nlk*nbFFm+3*nbFFm][2]*Valsp[j+nlk*nbFFp+3*nbFFp]*wJ*getNonLocalEqRatio()/2.*nu(q));
                }
              }
              for(int l=0;l<nbFFp;l++){
                for(int q = 0; q< 3; q++){
                  stiff(j+nlk*nbFFp+3*nbFFp+nbdofm,l+nlk*nbFFp+3*nbFFp+nbdofm) += cgp->operator()(0,q)*(Gradsp[l+nlk*nbFFp+3*nbFFp][0]*Valsp[j+nlk*nbFFp+3*nbFFp]*wJ*getNonLocalEqRatio()/2.*nu(q));
                  stiff(j+nlk*nbFFp+3*nbFFp+nbdofm,l+nlk*nbFFp+3*nbFFp+nbdofm) += cgp->operator()(2,q)*(Gradsp[l+nlk*nbFFp+3*nbFFp][2]*Valsp[j+nlk*nbFFp+3*nbFFp]*wJ*getNonLocalEqRatio()/2.*nu(q));
                }
              }
            }

            // Stability  N \mean{cg} (N)
            double NMeanCgNBetasc=0.;
            for(int l = 0; l <3; l++)
            {
              for(int n = 0; n <3; n++)
              {
                NMeanCgNBetasc += (cgp->operator()(l,n)+cgm->operator()(l,n))*nu(n)*nbetahs*nu(l)/2.;
              }
            }
            // Assembly stability (if not broken)
            for(int j=0;j<nbFFm;j++)
            {
              for(int l=0;l<nbFFm;l++)
              {
                stiff(j+nlk*nbFFm+3*nbFFm,l+nlk*nbFFm+3*nbFFm) += NMeanCgNBetasc*(Valsm[j+nlk*nbFFm+3*nbFFm]*Valsm[l+nlk*nbFFm+3*nbFFm]*wJ*getNonLocalEqRatio());
              }
              for(int l=0;l<nbFFp;l++)
              {
                stiff(j+nlk*nbFFm+3*nbFFm,l+nlk*nbFFp+3*nbFFp+nbdofm) -= NMeanCgNBetasc*(Valsm[j+nlk*nbFFm+3*nbFFm]*Valsp[l+nlk*nbFFp+3*nbFFp]*wJ*getNonLocalEqRatio());
              }
            }
            for(int j=0;j<nbFFp;j++)
            {
              for(int l=0;l<nbFFm;l++)
              {
                stiff(j+nlk*nbFFp+3*nbFFp+nbdofm,l+nlk*nbFFm+3*nbFFm) -= NMeanCgNBetasc*(Valsp[j+nlk*nbFFp+3*nbFFp]*Valsm[l+nlk*nbFFm+3*nbFFm]*wJ*getNonLocalEqRatio());
              }
              for(int l=0;l<nbFFp;l++)
              {
                stiff(j+nlk*nbFFp+3*nbFFp+nbdofm,l+nlk*nbFFp+3*nbFFp+nbdofm) += NMeanCgNBetasc*(Valsp[j+nlk*nbFFp+3*nbFFp]*Valsp[l+nlk*nbFFp+3*nbFFp]*wJ*getNonLocalEqRatio());
              }
            }
            // Assembly (loop on shape function)
            for(int j=0;j<nbFFm;j++)
            {
              for(int l=0;l<nbFFm;l++){
                for(int p =0; p < 3; p++){
                  stiff(j+nlk*nbFFm+3*nbFFm,l+nlk*nbFFm+3*nbFFm) -= (cgm->operator()(p,0)*Gradsm[j+nlk*nbFFm+3*nbFFm][0])*(nu(p)*wJ*getNonLocalEqRatio()/2.*Valsm[l+nlk*nbFFm+3*nbFFm]);
                  stiff(j+nlk*nbFFm+3*nbFFm,l+nlk*nbFFm+3*nbFFm) -= (cgm->operator()(p,2)*Gradsm[j+nlk*nbFFm+3*nbFFm][2])*(nu(p)*wJ*getNonLocalEqRatio()/2.*Valsm[l+nlk*nbFFm+3*nbFFm]);
                }
              }
              for(int l=0;l<nbFFp;l++){
                for(int p =0; p < 3; p++){
                  stiff(j+nlk*nbFFm+3*nbFFm,l+nlk*nbFFp+3*nbFFp+nbdofm) += (cgm->operator()(p,0)*Gradsm[j+nlk*nbFFm+3*nbFFm][0])*(nu(p)*
                                                            wJ*getNonLocalEqRatio()/2.*Valsp[l+nlk*nbFFp+3*nbFFp]);
                  stiff(j+nlk*nbFFm+3*nbFFm,l+nlk*nbFFp+3*nbFFp+nbdofm) += (cgm->operator()(p,2)*Gradsm[j+nlk*nbFFm+3*nbFFm][2])*(nu(p)*
                                                            wJ*getNonLocalEqRatio()/2.*Valsp[l+nlk*nbFFp+3*nbFFp]);
                }
              }
            }
            for(int j=0;j<nbFFp;j++)
            {
              for(int l=0;l<nbFFm;l++){
                for(int p =0; p < 3; p++){
                  stiff(j+nlk*nbFFp+3*nbFFp+nbdofm,l+nlk*nbFFm+3*nbFFm) -= (cgp->operator()(p,0)*Gradsp[j+nlk*nbFFp+3*nbFFp][0])*(nu(p)*wJ*getNonLocalEqRatio()/2.
                                                                  *Valsm[l+nlk*nbFFm+3*nbFFm]);
                  stiff(j+nlk*nbFFp+3*nbFFp+nbdofm,l+nlk*nbFFm+3*nbFFm) -= (cgp->operator()(p,2)*Gradsp[j+nlk*nbFFp+3*nbFFp][2])*(nu(p)*wJ*getNonLocalEqRatio()/2.
                                                                  *Valsm[l+nlk*nbFFm+3*nbFFm]);
                }
              }
              for(int l=0;l<nbFFp;l++){
                for(int p =0; p < 3; p++){
                  stiff(j+nlk*nbFFp+3*nbFFp+nbdofm,l+nlk*nbFFp+3*nbFFp+nbdofm) += (cgp->operator()(p,0)*Gradsp[j+nlk*nbFFp+3*nbFFp][0])*(nu(p)*wJ*getNonLocalEqRatio()/2.
                                                                     *Valsp[l+nlk*nbFFp+3*nbFFp]);
                  stiff(j+nlk*nbFFp+3*nbFFp+nbdofm,l+nlk*nbFFp+3*nbFFp+nbdofm) += (cgp->operator()(p,2)*Gradsp[j+nlk*nbFFp+3*nbFFp][2])*(nu(p)*wJ*getNonLocalEqRatio()/2.
                                                                     *Valsp[l+nlk*nbFFp+3*nbFFp]);
                }
              }
            }
            for (int extraDOFField = 0; extraDOFField < ipvm->getNumConstitutiveExtraDofDiffusionVariable(); extraDOFField++){
              Msg::Error("Implement non-local / extra dof coupling in dG3DStiffnessBulk::get");
              double  dLocalVariableDNonLocalVariablem = ipvm->getConstRefToDLocalVariableDNonLocalVariable()(nlk,extraDOFField);
              double  dLocalVariableDNonLocalVariablep = ipvp->getConstRefToDLocalVariableDNonLocalVariable()(nlk,extraDOFField);
            }
          }
        }
      
      

      
      
        
        
        // constitutive extra dof
        if( ipvm->getNumConstitutiveExtraDofDiffusionVariable()>getNumConstitutiveExtraDofDiffusionVariable() ) 
            Msg::Error("Your material law uses more constitutive extra dof variable than your domain dG3DStiffnessInter::get");
        if( ipvp->getNumConstitutiveExtraDofDiffusionVariable()>getNumConstitutiveExtraDofDiffusionVariable() ) 
            Msg::Error("Your material law uses more constitutive extra dof variable than your domain dG3DStiffnessInter::get");
        if( ipvp->getNumConstitutiveExtraDofDiffusionVariable()!= ipvm->getNumConstitutiveExtraDofDiffusionVariable() ) 
            Msg::Error("Your plus and minus material laws do not use the same number of constitutive extra dof variables dG3DStiffnessInter::get");
        for (int extraDOFField = 0; extraDOFField< ipvp->getNumConstitutiveExtraDofDiffusionVariable(); extraDOFField++){
          if( getConstitutiveExtraDofDiffusionContinuity() && !broken)
          {
            
            Msg::Error("axisymmetricDG3DStiffnessInter::get has not yet extraDOFField implemented");
            
            static bool msg=false;
            if(getNumConstitutiveExtraDofDiffusionVariable()>1)
            {
              if(!msg)
              {
                Msg::Info("we assume no cross dependency between the extra dof sources domain dG3DStiffnessInter::get");
                msg=true;
              }
            }
            int extraDOFField2=extraDOFField;
            
            /// stability parameter including mesh size and radial position
            const double nbetahs = getConstitutiveExtraDofDiffusionStabilityParameter() / hs / point[0];

            const STensor3  *dqdGradFieldm = &(ipvm->getConstRefTodFluxdGradField()[extraDOFField][extraDOFField2]);
            const STensor3  *dqdGradFieldp = &(ipvp->getConstRefTodFluxdGradField()[extraDOFField][extraDOFField2]);
            const SVector3  *dqdFieldm = &(ipvm->getConstRefTodFluxdField()[extraDOFField][extraDOFField2]);
            const SVector3  *dqdFieldp = &(ipvp->getConstRefTodFluxdField()[extraDOFField][extraDOFField2]);
            const STensor33 *dqdFm = &(ipvm->getConstRefTodFluxdF()[extraDOFField]);
            const STensor33 *dqdFp = &(ipvp->getConstRefTodFluxdF()[extraDOFField]);
            const STensor3  *dPdFieldm = &(ipvm->getConstRefTodPdField()[extraDOFField]);
            const STensor3  *dPdFieldp = &(ipvp->getConstRefTodPdField()[extraDOFField]);

            double fieldJump = ipvp->getConstRefToFieldJump()(extraDOFField);
            const double &fieldp = ipvp->getConstRefToField(extraDOFField);
            const double &fieldm = ipvm->getConstRefToField(extraDOFField);
            const double &oneOverFieldJump = ipvp->getConstRefToOneOverFieldJump()(extraDOFField);
            double dfieldJumpdFieldp = 1.;
            double dfieldJumpdFieldm = -1.;

            static STensor3  k0m;
            k0m = *(ipvm->getConstRefToLinearK()[extraDOFField][extraDOFField2]);
            static STensor3  k0p;
            k0p = *(ipvp->getConstRefToLinearK()[extraDOFField][extraDOFField2]);
            static STensor3  Stiff_alphadialitationm;
            Stiff_alphadialitationm = *(ipvm->getConstRefToLinearSymmetrizationCoupling()[extraDOFField]);
            static STensor3  Stiff_alphadialitationp;
            Stiff_alphadialitationp = *(ipvp->getConstRefToLinearSymmetrizationCoupling()[extraDOFField]);
            static STensor3  dk0mdFieldm;
            STensorOperation::zero(dk0mdFieldm);
            static STensor3  dk0pdFieldp;
            STensorOperation::zero(dk0pdFieldp);
            static STensor3 dStiff_alphadialitationmdFieldm;
            STensorOperation::zero(dStiff_alphadialitationmdFieldm);
            static STensor3 dStiff_alphadialitationpdFieldp;
            STensorOperation::zero(dStiff_alphadialitationpdFieldp);
            if(getConstitutiveExtraDofDiffusionUseEnergyConjugatedField())
            {
              k0m*=(-1.)*fieldm*fieldm;
              k0p*=(-1.)*fieldp*fieldp;
              fieldJump=oneOverFieldJump;
              dfieldJumpdFieldp=ipvp->getConstRefTodOneOverFieldJumpdFieldp()(extraDOFField,extraDOFField2);
              dfieldJumpdFieldm=ipvm->getConstRefTodOneOverFieldJumpdFieldm()(extraDOFField,extraDOFField2);
              dk0mdFieldm=k0m;
              dk0mdFieldm*=(1.)*2./fieldm;//the - is removed
              dk0pdFieldp=k0p;
              dk0pdFieldp*=(1.)*2./fieldp;//the - is removed
              Stiff_alphadialitationm*=(-1.)*fieldm*fieldm;
              Stiff_alphadialitationp*=(-1.)*fieldp*fieldp;
              dStiff_alphadialitationmdFieldm=Stiff_alphadialitationm;//the - is removed
              dStiff_alphadialitationmdFieldm*=(1.)*2./fieldm;
              dStiff_alphadialitationpdFieldp*=Stiff_alphadialitationp;//the - is removed
              dStiff_alphadialitationpdFieldp*=(1.)*2./fieldp;
            }
            int indexField=3+getNumNonLocalVariable()+extraDOFField;

            // Assembly consistency (from derivative of sigma with extra field)
            for(int j=0;j<nbFFm;j++)
            {
              for(int k=0;k<3;k++)
              {
                for(int l=0;l<nbFFm;l++)
                {
                  for(int q = 0; q< 3; q++)
                  {
                    stiff(j+k*nbFFm,l+indexField*nbFFm) -= dPdFieldm->operator()(k,q)*(Valsm[l+indexField*nbFFm]*Valsm[j+0*nbFFm]*wJ/2.*nu(q));
                  }
                }
                for(int l=0;l<nbFFp;l++)
                {
                  for(int q = 0; q< 3; q++)
                  {
                    stiff(j+k*nbFFm,l+indexField*nbFFp+nbdofm) -= dPdFieldp->operator()(k,q)*(Valsp[l+indexField*nbFFp]*Valsm[j+0*nbFFm]*wJ/2.*nu(q));
                  }
                }
              }
            }
            for(int j=0;j<nbFFp;j++)
            {
              for(int k=0;k<3;k++)
              {
                for(int l=0;l<nbFFm;l++)
                {
                  for(int q = 0; q< 3; q++)
                  {
                    stiff(j+k*nbFFp+nbdofm,l+indexField*nbFFm) += dPdFieldm->operator()(k,q)*(Valsm[l+indexField*nbFFm]*Valsp[j+0*nbFFp]*wJ/2.*nu(q));
                  }
                }
                for(int l=0;l<nbFFp;l++)
                {
                  for(int q = 0; q< 3; q++)
                  {
                    stiff(j+k*nbFFp+nbdofm,l+indexField*nbFFp+nbdofm) += dPdFieldp->operator()(k,q)*(Valsp[l+indexField*nbFFp]*Valsp[j+0*nbFFp]*wJ/2.*nu(q));
                  }
                }
              }
            }
            for(int j=0;j<nbFFm;j++)
            {
              for(int k=0;k<3;k++)
              {
                for(int l=0;l<nbFFm;l++)
                {
                  for(int q = 0; q< 3; q++)
                  {
                    for(int N = 0; N< 3; N++)
                    {
                      stiff(j+indexField*nbFFm,l+k*nbFFm) -= dqdFm->operator()(N,k,q)*(Gradsm[l+0*nbFFm][q]*Valsm[j+indexField*nbFFm]*wJ*getConstitutiveExtraDofDiffusionEqRatio()/2.*nu(N));
                    }
                  }
                }
                for(int l=0;l<nbFFp;l++)
                {
                  for(int q = 0; q< 3; q++)
                  {
                    for(int N = 0; N< 3; N++)
                    {
                      stiff(j+indexField*nbFFm,l+k*nbFFp+nbdofm) -= dqdFp->operator()(N,k,q)*(Gradsp[l+0*nbFFp][q]*Valsm[j+indexField*nbFFm]*wJ*getConstitutiveExtraDofDiffusionEqRatio()/2.*nu(N));
                    }
                  }
                }
              }
            }
            for(int j=0;j<nbFFp;j++)
            {
              for(int k=0;k<3;k++)
              {
                for(int l=0;l<nbFFm;l++)
                {
                  for(int q = 0; q< 3; q++)
                  {
                    for(int N = 0; N< 3; N++)
                    {
                      stiff(j+indexField*nbFFp+nbdofm,l+k*nbFFm) += dqdFm->operator()(N,k,q)*(Gradsm[l+0*nbFFm][q]*Valsp[j+indexField*nbFFp]*wJ*getConstitutiveExtraDofDiffusionEqRatio()/2.*nu(N));
                    }
                  }
                }
                for(int l=0;l<nbFFp;l++)
                {
                  for(int q = 0; q< 3; q++)
                  {
                    for(int N = 0; N< 3; N++)
                    {
                      stiff(j+indexField*nbFFp+nbdofm,l+k*nbFFp+nbdofm) += dqdFp->operator()(N,k,q)*(Gradsp[l+0*nbFFp][q]*Valsp[j+indexField*nbFFp]*wJ*getConstitutiveExtraDofDiffusionEqRatio()/2.*nu(N));
                    }
                  }
                }
              }
            }
            // consistency D\mean{q} DT
            // Assembly consistency
            for(int j=0;j<nbFFm;j++)
            {
              for(int l=0;l<nbFFm;l++)
              {
                for(int p = 0; p< 3; p++)
                {
                  stiff(j+indexField*nbFFm,l+indexField*nbFFm) -= dqdFieldm->operator()(p)*
                                                       (Valsm[l+indexField*nbFFm]*Valsm[j+indexField*nbFFm]*wJ*getConstitutiveExtraDofDiffusionEqRatio()/2.*nu(p));
                  for(int q = 0; q< 3; q++)
                  {
                    stiff(j+indexField*nbFFm,l+indexField*nbFFm) -= dqdGradFieldm->operator()(q,p)*
                                                                           (Gradsm[l+indexField*nbFFm][p]*Valsm[j+indexField*nbFFm]*wJ*getConstitutiveExtraDofDiffusionEqRatio()/2.*nu(q));
                  }
                }
              }
              for(int l=0;l<nbFFp;l++)
              {
                for(int p = 0; p< 3; p++)
                {
                  stiff(j+indexField*nbFFm,l+indexField*nbFFp+nbdofm) -= dqdFieldp->operator()(p)*
                                                       (Valsp[l+indexField*nbFFp]*Valsm[j+indexField*nbFFm]*wJ*getConstitutiveExtraDofDiffusionEqRatio()/2.*nu(p)); 
                  for(int q = 0; q< 3; q++)
                  {
                    stiff(j+indexField*nbFFm,l+indexField*nbFFp+nbdofm) -= dqdGradFieldp->operator()(q,p)*
                                                       (Gradsp[l+indexField*nbFFp][p]*Valsm[j+indexField*nbFFm]*wJ*getConstitutiveExtraDofDiffusionEqRatio()/2.*nu(q));
                  }
                }
              }
            }
            for(int j=0;j<nbFFp;j++)
            {
              for(int l=0;l<nbFFm;l++)
              {
                for(int p = 0; p< 3; p++)
                {
                  stiff(j+indexField*nbFFp+nbdofm,l+indexField*nbFFm) += dqdFieldm->operator()(p)*
                                                       (Valsm[l+indexField*nbFFm]*Valsp[j+indexField*nbFFp]*wJ*getConstitutiveExtraDofDiffusionEqRatio()/2.*nu(p));
                  for(int q = 0; q< 3; q++)
                  {
                    stiff(j+indexField*nbFFp+nbdofm,l+indexField*nbFFm) += dqdGradFieldm->operator()(q,p)*
                                                       (Gradsm[l+indexField*nbFFm][p]*Valsp[j+indexField*nbFFp]*wJ*getConstitutiveExtraDofDiffusionEqRatio()/2.*nu(q));
                  }
                }
              }
              for(int l=0;l<nbFFp;l++)
              {
                for(int p = 0; p< 3; p++)
                {
                  stiff(j+indexField*nbFFp+nbdofm,l+indexField*nbFFp+nbdofm) += dqdFieldp->operator()(p)*
                                                      (Valsp[l+indexField*nbFFp]*Valsp[j+indexField*nbFFp]*wJ*getConstitutiveExtraDofDiffusionEqRatio()/2.*nu(p));
                  for(int q = 0; q< 3; q++)
                  {
                    stiff(j+indexField*nbFFp+nbdofm,l+indexField*nbFFp+nbdofm) += dqdGradFieldp->operator()(q,p)*
                                                      (Gradsp[l+indexField*nbFFp][p]*Valsp[j+indexField*nbFFp]*wJ*getConstitutiveExtraDofDiffusionEqRatio()/2.*nu(q));
                  }
                }
              }
            }
            // Stability  N \mean{k0} (N)
            double NMeankNBetasc=0.;
            double dNMeankNBetascdFieldp=0.;
            double dNMeankNBetascdFieldm=0.;
            for(int l = 0; l <3; l++)
            {
              for(int n = 0; n <3; n++)
              {
                NMeankNBetasc += (k0p(l,n)+k0m(l,n))*nu(n)*nbetahs*nu(l)/2.;
                dNMeankNBetascdFieldp+=dk0pdFieldp(l,n)*nu(n)*nbetahs*nu(l)/2.;
                dNMeankNBetascdFieldm+=dk0mdFieldm(l,n)*nu(n)*nbetahs*nu(l)/2.;
              }
            }
            // Assembly stability (if not broken)
            for(int j=0;j<nbFFm;j++)
            {
              for(int l=0;l<nbFFm;l++)
              {
                stiff(j+indexField*nbFFm,l+indexField*nbFFm) -= dfieldJumpdFieldm*NMeankNBetasc*
                                      (Valsm[j+indexField*nbFFm]*Valsm[l+indexField*nbFFm]*wJ*getConstitutiveExtraDofDiffusionEqRatio());
                if(getConstitutiveExtraDofDiffusionUseEnergyConjugatedField())
                  stiff(j+indexField*nbFFm,l+indexField*nbFFm) -= oneOverFieldJump*dNMeankNBetascdFieldm*
                                      (Valsm[j+indexField*nbFFm]*Valsm[l+indexField*nbFFm]*wJ*getConstitutiveExtraDofDiffusionEqRatio());
              }
              for(int l=0;l<nbFFp;l++)
              {
                stiff(j+indexField*nbFFm,l+indexField*nbFFp+nbdofm) -= dfieldJumpdFieldp*NMeankNBetasc*
                                      (Valsm[j+indexField*nbFFm]*Valsp[l+indexField*nbFFp]*wJ*getConstitutiveExtraDofDiffusionEqRatio());
                if(getConstitutiveExtraDofDiffusionUseEnergyConjugatedField())
                  stiff(j+indexField*nbFFm,l+indexField*nbFFp+nbdofm) -= oneOverFieldJump*dNMeankNBetascdFieldp*
                                                  (Valsm[j+indexField*nbFFm]*Valsp[l+indexField*nbFFp]*wJ*getConstitutiveExtraDofDiffusionEqRatio());
              }
            }
            for(int j=0;j<nbFFp;j++)
            {
              for(int l=0;l<nbFFm;l++)
              {
                stiff(j+indexField*nbFFp+nbdofm,l+indexField*nbFFm) += dfieldJumpdFieldm*NMeankNBetasc*
                                                  (Valsp[j+indexField*nbFFp]*Valsm[l+indexField*nbFFm]*wJ*getConstitutiveExtraDofDiffusionEqRatio());
                if(getConstitutiveExtraDofDiffusionUseEnergyConjugatedField())
                  stiff(j+indexField*nbFFp+nbdofm,l+indexField*nbFFm) += oneOverFieldJump*dNMeankNBetascdFieldm*
                                                  (Valsp[j+indexField*nbFFp]*Valsm[l+indexField*nbFFm]*wJ*getConstitutiveExtraDofDiffusionEqRatio());
              }
              for(int l=0;l<nbFFp;l++)
              {
                stiff(j+indexField*nbFFp+nbdofm,l+indexField*nbFFp+nbdofm) += dfieldJumpdFieldp*NMeankNBetasc*
                                                  (Valsp[j+indexField*nbFFp]*Valsp[l+indexField*nbFFp]*wJ*getConstitutiveExtraDofDiffusionEqRatio());
                if(getConstitutiveExtraDofDiffusionUseEnergyConjugatedField())
                  stiff(j+indexField*nbFFp+nbdofm,l+indexField*nbFFp+nbdofm) += oneOverFieldJump*dNMeankNBetascdFieldp*
                                                  (Valsp[j+indexField*nbFFp]*Valsp[l+indexField*nbFFp]*wJ*getConstitutiveExtraDofDiffusionEqRatio());
              }
            }
            // compatibility
            // Assembly (loop on shape function)
            for(int j=0;j<nbFFm;j++)
            {
              for(int l=0;l<nbFFm;l++)
              {
                for(int p =0; p < 3; p++)
                {
                  for(int q =0; q < 3; q++)
                  {
                    stiff(j+indexField*nbFFm,l+indexField*nbFFm) += dfieldJumpdFieldm*(k0m(p,q)*
                                                  Gradsm[j+indexField*nbFFm][q])*(nu(p)*wJ*getConstitutiveExtraDofDiffusionEqRatio()/2.*Valsm[l+indexField*nbFFm]);
                    if(getConstitutiveExtraDofDiffusionUseEnergyConjugatedField())
                      stiff(j+indexField*nbFFm,l+indexField*nbFFm) += oneOverFieldJump*(dk0mdFieldm(p,q)*
                                                  Gradsm[j+indexField*nbFFm][q])*(nu(p)*wJ*getConstitutiveExtraDofDiffusionEqRatio()/2.*Valsm[l+indexField*nbFFm]);
                  }
                }
              }
              for(int l=0;l<nbFFp;l++)
              {
                for(int p =0; p < 3; p++)
                {
                  for(int q =0; q < 3; q++)
                  {
                    stiff(j+indexField*nbFFm,l+indexField*nbFFp+nbdofm) += dfieldJumpdFieldp*(k0m(p,q)*
                                                Gradsm[j+indexField*nbFFm][q])*(nu(p)*wJ*getConstitutiveExtraDofDiffusionEqRatio()/2.*Valsp[l+indexField*nbFFp]);
                  }
                }
              }
            }
            for(int j=0;j<nbFFp;j++)
            {
              for(int l=0;l<nbFFm;l++)
              {
                for(int p =0; p < 3; p++)
                {
                  for(int q =0; q < 3; q++)
                  {
                    stiff(j+indexField*nbFFp+nbdofm,l+indexField*nbFFm) += dfieldJumpdFieldm*(k0p(p,q)*Gradsp[j+indexField*nbFFp][q])*
                                               (nu(p)*wJ*getConstitutiveExtraDofDiffusionEqRatio()/2.*Valsm[l+indexField*nbFFm]);
                  }
                }
              }
              for(int l=0;l<nbFFp;l++)
              {
                for(int p =0; p < 3; p++)
                {
                  for(int q =0; q < 3; q++)
                  {
                    stiff(j+indexField*nbFFp+nbdofm,l+indexField*nbFFp+nbdofm) += dfieldJumpdFieldp*(k0p(p,q)*
                                      Gradsp[j+indexField*nbFFp][q])*(nu(p)*wJ*getConstitutiveExtraDofDiffusionEqRatio()/2.*Valsp[l+indexField*nbFFp]);
                    if(getConstitutiveExtraDofDiffusionUseEnergyConjugatedField())
                      stiff(j+indexField*nbFFp+nbdofm,l+indexField*nbFFp+nbdofm) += oneOverFieldJump*
                                 (dk0pdFieldp(p,q)*Gradsp[j+indexField*nbFFp][q])*(nu(p)*wJ*getConstitutiveExtraDofDiffusionEqRatio()/2.*Valsp[l+indexField*nbFFp]);
                  }
                }
              }
            }
            double gamma=0.;
            //new related to dialetation
            for(int j=0;j<nbFFm;j++)
            {
              for(int k=0;k<3;k++)
              {
                for(int l=0;l<nbFFm;l++)
                {
                  for(int p =0; p < 3; p++)
                  {
                    stiff(j+indexField*nbFFm,l+k*nbFFm) -= gamma*(Stiff_alphadialitationm(k,p)*Valsm[j+indexField*nbFFm])*(nu(p)*wJ/2.*Valsm[l+0*nbFFm])*(-1.);
                  }
                }
                for(int l=0;l<nbFFp;l++)
                {
                  for(int p =0; p < 3; p++)
                  {
                    stiff(j+indexField*nbFFm,l+k*nbFFp+nbdofm) += gamma*(Stiff_alphadialitationm(k,p)*Valsm[j+indexField*nbFFm])*(nu(p)*wJ/2.*Valsp[l+0*nbFFp])*(-1.);
                  }
                }
              }
            }
            for(int j=0;j<nbFFp;j++)
            {
              for(int k=0;k<3;k++)
              {
                for(int l=0;l<nbFFm;l++)
                {
                  for(int p =0; p < 3; p++)
                  {
                    stiff(j+indexField*nbFFp+nbdofm,l+k*nbFFm) -= gamma*(Stiff_alphadialitationp(k,p)*Valsp[j+indexField*nbFFp])*(nu(p)*wJ/2.*Valsm[l+0*nbFFm])*(-1.);
                  }
                }
                for(int l=0;l<nbFFp;l++)
                {
                  for(int p =0; p < 3; p++)
                  {
                    stiff(j+indexField*nbFFp+nbdofm,l+k*nbFFp+nbdofm) += gamma*(Stiff_alphadialitationp(k,p)*Valsp[j+indexField*nbFFp])*(nu(p)*wJ/2.*Valsp[l+0*nbFFp])*(-1.);
                  }
                }
              }
            }

            if(getConstitutiveExtraDofDiffusionUseEnergyConjugatedField())
            {
              for(int j=0;j<nbFFm;j++)
              {
                for(int l=0;l<nbFFm;l++)
                {
                  for(int k =0; k < 3; k++)
                  {
                    for(int p =0; p < 3; p++)
                    {
                      // stiff(j+indexField*nbFFm,l+indexField*nbFFm) += jump(k)*(Stiff_alphadialitationm(k,p)*(2./fieldm)*Valsm[j+indexField*nbFFm])*nu(p)*Valsm[l+indexField*nbFFm]*wJ/2.*(-1.);
                      stiff(j+indexField*nbFFm,l+indexField*nbFFm) += gamma*jump(k)*
                          (dStiff_alphadialitationmdFieldm(k,p)*Valsm[j+indexField*nbFFm])*nu(p)*Valsm[l+indexField*nbFFm]*wJ/2.*(-1.);
                    }
                  }
                }
              }
              for(int j=0;j<nbFFp;j++)
              {
                for(int l=0;l<nbFFp;l++)
                {
                  for(int k =0; k < 3; k++)
                  {
                    for(int p =0; p< 3; p++)
                    {
                      //  stiff(j+indexField*nbFFp+nbdofm,l+indexField*nbFFp+nbdofm) += jump(k)*(Stiff_alphadialitationp(k,p)*(2./fieldp)*Valsp[j+indexField*nbFFp])*nu(p)*Valsp[l+indexField*nbFFp]*wJ/2.*(-1.);
                      stiff(j+indexField*nbFFp+nbdofm,l+indexField*nbFFp+nbdofm) += gamma*jump(k)*
                          (dStiff_alphadialitationpdFieldp(k,p)*Valsp[j+indexField*nbFFp])*nu(p)*Valsp[l+indexField*nbFFp]*wJ/2.*(-1.);
                    }
                  }
                }
              }
            }
            for (int nlk = 0; nlk < ipvp->getNumberNonLocalVariable(); nlk++){
              Msg::Error("Implement extra dof / non-local coupling in dG3DStiffnessInter::get");
              const SVector3 &dFluxdNonLocalVariablem = ipvm->getConstRefTodFluxdNonLocalVariable()[extraDOFField][nlk];
              const SVector3 &dFluxdNonLocalVariablep = ipvm->getConstRefTodFluxdNonLocalVariable()[extraDOFField][nlk];
            }
//
          }
        }
      }
    }
  }
  //stiff.print("dginter");
}

















