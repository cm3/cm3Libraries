//
//
// Description: pathfollowing terms
//
// Author:  <Van Dung NGUYEN>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef AXISYMMETRICDG3DPATHFOLLOWINGTERMS_H
#define AXISYMMETRICDG3DPATHFOLLOWINGTERMS_H

#include "pathFollowingTerms.h"

class axisymmetricDG3DDissipationPathFollowingBulkScalarTerm : public ScalarTermBase<double>{
	protected:
		const IPField* _ipf;
		const partDomain* _dom;
			
	public:
		axisymmetricDG3DDissipationPathFollowingBulkScalarTerm(const partDomain* dom, const IPField* ip):_dom(dom),_ipf(ip){}
		virtual ~axisymmetricDG3DDissipationPathFollowingBulkScalarTerm(){}
		virtual void get(MElement *ele, int npts, IntPt *GP, double &val) const;
		virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<double> &vval) const {
			Msg::Error("axisymmetricDG3DDissipationPathFollowingBulkScalarTerm::get(MElement *ele, int npts, IntPt *GP, std::vector<T2> &vval) is not defined");
		};
		virtual ScalarTermBase<double>* clone () const {return new axisymmetricDG3DDissipationPathFollowingBulkScalarTerm(_dom,_ipf);};
};

class axisymmetricDG3DDissipationPathFollowingBulkLinearTerm : public  LinearTermBase<double>{
	protected:
		const IPField* _ipf;
		const partDomain* _dom;
		
	public:
		axisymmetricDG3DDissipationPathFollowingBulkLinearTerm(const partDomain* dom,const IPField* ip): LinearTermBase<double>(),_dom(dom),_ipf(ip){};
		virtual ~axisymmetricDG3DDissipationPathFollowingBulkLinearTerm(){}
		
		virtual void get(MElement *ele,int npts,IntPt *GP,fullVector<double> &m) const;
		virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullVector<double> > &mv) const
		{
			Msg::Error("Define me get by integration point axisymmetricDG3DDissipationPathFollowingBulkLinearTerm");
		}
		
		virtual LinearTermBase<double>* clone () const
		{
			return new axisymmetricDG3DDissipationPathFollowingBulkLinearTerm(_dom,_ipf);
		}
};
#endif //AXISYMMETRICDG3DPATHFOLLOWINGTERMS_H