//
// C++ Interface: nonLocalDamageQuadYieldHyperDG3DIPVariable
//
// Description:
//
//
// Author: V.D. NGUYEN 2014
//
// Copyright: See COPYING file that comes with this distribution
//
#include "nonLocalDamageHyperelasticDG3DIPVariable.h"
#include "ipField.h"
#include "restartManager.h"
nonLocalDamageHyperelasticDG3DIPVariable::nonLocalDamageHyperelasticDG3DIPVariable(const mlawNonLocalDamagePowerYieldHyper &mlaw, const bool createBodyForceHO, const bool oninter)
          :dG3DIPVariable(createBodyForceHO,oninter,1)
{
  _nldHyperipv = new IPHyperViscoElastoPlasticNonLocalDamage(mlaw.getConstRefToCompressionHardening(),
                                                            mlaw.getConstRefToTractionHardening(),
                                                            mlaw.getConstRefToKinematicHardening(),
                                                            mlaw.getViscoElasticNumberOfElement(),
                                                            mlaw.getCLengthLaw(),mlaw.getDamageLaw());
}

  nonLocalDamageHyperelasticDG3DIPVariable::nonLocalDamageHyperelasticDG3DIPVariable(const nonLocalDamageHyperelasticDG3DIPVariable &source):
          dG3DIPVariable(source)
{
  _nldHyperipv = new IPHyperViscoElastoPlasticNonLocalDamage(*source._nldHyperipv);
}

nonLocalDamageHyperelasticDG3DIPVariable& nonLocalDamageHyperelasticDG3DIPVariable::operator=(const IPVariable &source){
  dG3DIPVariable::operator=(source);
  const nonLocalDamageHyperelasticDG3DIPVariable* src = dynamic_cast<const nonLocalDamageHyperelasticDG3DIPVariable*>(&source);
  if(src != NULL)
  {
    _nldHyperipv->operator= (*dynamic_cast<const IPVariable*>(src->_nldHyperipv));
  }
  return *this;
}

double nonLocalDamageHyperelasticDG3DIPVariable::get(const int i) const
{
  if(i == IPField::PLASTICSTRAIN)
  {
    return getIPHyperViscoElastoPlasticNonLocalDamage()->getCurrentPlasticStrain();
  }
  else if (i == IPField::NONLOCAL_PLASTICSTRAIN){
    return getIPHyperViscoElastoPlasticNonLocalDamage()->getEffectivePlasticStrain();
  }
  else if(i == IPField::DAMAGE)
  {
    return getIPHyperViscoElastoPlasticNonLocalDamage()->getDamage();
  }
  else if (i == IPField::DAMAGE_0){
    return getIPHyperViscoElastoPlasticNonLocalDamage()->getDamage();
  }
  else if (i == IPField::PLASTIC_POISSON_RATIO){
    return getIPHyperViscoElastoPlasticNonLocalDamage()->_nup;
  }
  else if (i == IPField::PLASTICSTRAIN_COMPRESSION){
    return getIPHyperViscoElastoPlasticNonLocalDamage()->getConstRefToCompressionPlasticStrain();
  }
  else if (i == IPField::PLASTICSTRAIN_TRACTION){
    return getIPHyperViscoElastoPlasticNonLocalDamage()->getConstRefToTractionPlasticStrain();
  }
  else if (i == IPField::PLASTICSTRAIN_SHEAR){
    return getIPHyperViscoElastoPlasticNonLocalDamage()->getConstRefToShearPlasticStrain();
  }
  else if (i == IPField::BACKSTRESS){
   if (getIPHyperViscoElastoPlasticNonLocalDamage()->_ipKinematic!= NULL)
      return getIPHyperViscoElastoPlasticNonLocalDamage()->_ipKinematic->getR();
    else
      return 0.;
  }
  else if (i == IPField::PLASTIC_RATE){
    return getIPHyperViscoElastoPlasticNonLocalDamage()->getConstRefToPlasticDeformationRate();
  }
  else if (i == IPField::K_XX){
    const STensor3& K = getIPHyperViscoElastoPlasticNonLocalDamage()->_kirchhoff;
    return K(0,0);
  }
  else if (i == IPField::K_YY){
    const STensor3& K = getIPHyperViscoElastoPlasticNonLocalDamage()->_kirchhoff;
    return K(1,1);
  }
  else if (i == IPField::K_ZZ){
    const STensor3& K = getIPHyperViscoElastoPlasticNonLocalDamage()->_kirchhoff;
    return K(2,2);
  }
  else if (i == IPField::K_XY){
    const STensor3& K = getIPHyperViscoElastoPlasticNonLocalDamage()->_kirchhoff;
    return K(0,1);
  }
  else if (i == IPField::K_YZ){
    const STensor3& K = getIPHyperViscoElastoPlasticNonLocalDamage()->_kirchhoff;
    return K(1,2);
  }
  else if (i == IPField::K_XZ){
    const STensor3& K = getIPHyperViscoElastoPlasticNonLocalDamage()->_kirchhoff;
    return K(0,2);
  }
  else
  {
    return dG3DIPVariable::get(i);
  }
}
double nonLocalDamageHyperelasticDG3DIPVariable::defoEnergy() const
{
  return getIPHyperViscoElastoPlasticNonLocalDamage()->defoEnergy();
}
double nonLocalDamageHyperelasticDG3DIPVariable::plasticEnergy() const
{
  return getIPHyperViscoElastoPlasticNonLocalDamage()->plasticEnergy();
}

double nonLocalDamageHyperelasticDG3DIPVariable::getConstRefToLocalVariable(const int idex) const{
  if (idex == 0){
    return getIPHyperViscoElastoPlasticNonLocalDamage()->getCurrentPlasticStrain();
  }
  else{
    Msg::Error("the non-local variable %d is not defined",idex);
    return 0.;
  }
};
double& nonLocalDamageHyperelasticDG3DIPVariable::getRefToLocalVariable(const int idex){
  if (idex == 0){
    return getIPHyperViscoElastoPlasticNonLocalDamage()->getRefToCurrentPlasticStrain();
  }
  else{
    Msg::Error("the non-local variable %d is not defined",idex);
    static double a(0.);
    return a;
  }
};

double nonLocalDamageHyperelasticDG3DIPVariable::getConstRefToNonLocalVariableInMaterialLaw(const int idex) const{
  if (idex == 0){
    return getIPHyperViscoElastoPlasticNonLocalDamage()->getEffectivePlasticStrain();
  }
  else{
    Msg::Error("the non-local variable %d is not defined",idex);
    return 0.;
  }
};
double& nonLocalDamageHyperelasticDG3DIPVariable::getRefToNonLocalVariableInMaterialLaw(const int idex){
  if (idex == 0){
    return getIPHyperViscoElastoPlasticNonLocalDamage()->getRefToEffectivePlasticStrain();
  }
  else{
    Msg::Error("the non-local variable %d is not defined",idex);
    static double a(0.);
    return a;
  }
};

void nonLocalDamageHyperelasticDG3DIPVariable::restart()
{
  dG3DIPVariable::restart();
  restartManager::restart(_nldHyperipv);
  return;
}


localDamageHyperelasticDG3DIPVariableWithFailure::localDamageHyperelasticDG3DIPVariableWithFailure(const mlawLocalDamagePowerYieldHyperWithFailure &mlaw, const bool createBodyForceHO, const bool oninter)
          :dG3DIPVariable(createBodyForceHO,oninter,0)
{
  _localIPv = new IPHyperViscoElastoPlasticMultipleLocalDamage(mlaw.getConstRefToCompressionHardening(),
                                                            mlaw.getConstRefToTractionHardening(),
                                                            mlaw.getConstRefToKinematicHardening(),
                                                            mlaw.getViscoElasticNumberOfElement(),
                                                            mlaw.getDamageLaw());
}

localDamageHyperelasticDG3DIPVariableWithFailure::localDamageHyperelasticDG3DIPVariableWithFailure(const localDamageHyperelasticDG3DIPVariableWithFailure &source):
          dG3DIPVariable(source)
{
  _localIPv = new IPHyperViscoElastoPlasticMultipleLocalDamage(*source._localIPv);
}

localDamageHyperelasticDG3DIPVariableWithFailure& localDamageHyperelasticDG3DIPVariableWithFailure::operator=(const IPVariable &source){
  dG3DIPVariable::operator=(source);
  const localDamageHyperelasticDG3DIPVariableWithFailure* src = dynamic_cast<const localDamageHyperelasticDG3DIPVariableWithFailure*>(&source);
  if(src != NULL)
  {
    _localIPv->operator= (*dynamic_cast<const IPVariable*>(src->_localIPv));
  }
  return *this;
}

double localDamageHyperelasticDG3DIPVariableWithFailure::get(const int i) const
{
  if(i == IPField::PLASTICSTRAIN)
  {
    return getIPHyperViscoElastoPlasticMultipleLocalDamage()->getConstRefToEqPlasticStrain();
  }
  else if (i == IPField::FAILURE_ONSET){
    return getIPHyperViscoElastoPlasticMultipleLocalDamage()->getFailureOnset();
  }
  else if (i == IPField::FAILURE_PLASTICSTRAIN){
    return getIPHyperViscoElastoPlasticMultipleLocalDamage()->getFailurePlasticity();
  }
  else if(i == IPField::DAMAGE)
  {
    double OneminusD = 1;
    int n =  this->getIPHyperViscoElastoPlasticMultipleLocalDamage()->getNumNonLocalVariable();
    for (int iter =0; iter < n; iter ++){
      double Diter = this->getIPHyperViscoElastoPlasticMultipleLocalDamage()->getDamage(iter);
      OneminusD *= (1.- Diter);
    }
    return (1.- OneminusD);
  }
  else if (i == IPField::DAMAGE_0){
    return getIPHyperViscoElastoPlasticMultipleLocalDamage()->getDamage(0);
  }
  else if (i == IPField::DAMAGE_1){
    return getIPHyperViscoElastoPlasticMultipleLocalDamage()->getDamage(1);
  }
  else if (i == IPField::PLASTIC_POISSON_RATIO){
    return getIPHyperViscoElastoPlasticMultipleLocalDamage()->_nup;
  }
  else if (i == IPField::PLASTICSTRAIN_COMPRESSION){
    return getIPHyperViscoElastoPlasticMultipleLocalDamage()->getConstRefToCompressionPlasticStrain();
  }
  else if (i == IPField::PLASTICSTRAIN_TRACTION){
    return getIPHyperViscoElastoPlasticMultipleLocalDamage()->getConstRefToTractionPlasticStrain();
  }
  else if (i == IPField::PLASTICSTRAIN_SHEAR){
    return getIPHyperViscoElastoPlasticMultipleLocalDamage()->getConstRefToShearPlasticStrain();
  }
  else if (i == IPField::BACKSTRESS){
   if (getIPHyperViscoElastoPlasticMultipleLocalDamage()->_ipKinematic!= NULL)
      return getIPHyperViscoElastoPlasticMultipleLocalDamage()->_ipKinematic->getR();
    else
      return 0.;
  }
  else
  {
    return dG3DIPVariable::get(i);
  }
}

void localDamageHyperelasticDG3DIPVariableWithFailure::restart()
{
  dG3DIPVariable::restart();
  restartManager::restart(_localIPv);
  return;
}




nonLocalDamageHyperelasticDG3DIPVariableWithFailure::nonLocalDamageHyperelasticDG3DIPVariableWithFailure(const mlawNonLocalDamagePowerYieldHyperWithFailure &mlaw, const bool createBodyForceHO, const bool oninter)
          :dG3DIPVariable(createBodyForceHO,oninter,2)
{
  _nldHyperipv = new IPHyperViscoElastoPlasticMultipleNonLocalDamage(mlaw.getConstRefToCompressionHardening(),
                                                            mlaw.getConstRefToTractionHardening(),
                                                            mlaw.getConstRefToKinematicHardening(),
                                                            mlaw.getViscoElasticNumberOfElement(),
                                                            mlaw.getCLengthLaw(),mlaw.getDamageLaw());
}

  nonLocalDamageHyperelasticDG3DIPVariableWithFailure::nonLocalDamageHyperelasticDG3DIPVariableWithFailure(const nonLocalDamageHyperelasticDG3DIPVariableWithFailure &source):
          dG3DIPVariable(source)
{
  _nldHyperipv = new IPHyperViscoElastoPlasticMultipleNonLocalDamage(*source._nldHyperipv);
}

nonLocalDamageHyperelasticDG3DIPVariableWithFailure& nonLocalDamageHyperelasticDG3DIPVariableWithFailure::operator=(const IPVariable &source){
  dG3DIPVariable::operator=(source);
  const nonLocalDamageHyperelasticDG3DIPVariableWithFailure* src = dynamic_cast<const nonLocalDamageHyperelasticDG3DIPVariableWithFailure*>(&source);
  if(src != NULL)
  {
    _nldHyperipv->operator= (*dynamic_cast<const IPVariable*>(src->_nldHyperipv));
  }
  return *this;
}

double nonLocalDamageHyperelasticDG3DIPVariableWithFailure::get(const int i) const
{
  if(i == IPField::PLASTICSTRAIN)
  {
    return getIPHyperViscoElastoPlasticMultipleNonLocalDamage()->getCurrentPlasticStrain();
  }
  else if (i == IPField::NONLOCAL_PLASTICSTRAIN){
    return getIPHyperViscoElastoPlasticMultipleNonLocalDamage()->getEffectivePlasticStrain();
  }
  else if (i == IPField::FAILURE_ONSET){
    return getIPHyperViscoElastoPlasticMultipleNonLocalDamage()->getFailureOnset();
  }
  else if (i == IPField::FAILURE_PLASTICSTRAIN){
    return getIPHyperViscoElastoPlasticMultipleNonLocalDamage()->getFailurePlasticity();
  }
  else if (i == IPField::NONLOCAL_FAILURE_PLASTICSTRAIN){
    return getIPHyperViscoElastoPlasticMultipleNonLocalDamage()->getNonLocalFailurePlasticity();
  }
  else if(i == IPField::DAMAGE)
  {
    double OneminusD = 1;
    int n =  this->getIPHyperViscoElastoPlasticMultipleNonLocalDamage()->getNumNonLocalVariable();
    for (int iter =0; iter < n; iter ++){
      double Diter = this->getIPHyperViscoElastoPlasticMultipleNonLocalDamage()->getDamage(iter);
      OneminusD *= (1.- Diter);
    }
    return (1.- OneminusD);
  }
  else if (i == IPField::DAMAGE_0){
    return getIPHyperViscoElastoPlasticMultipleNonLocalDamage()->getDamage(0);
  }
  else if (i == IPField::DAMAGE_1){
    return getIPHyperViscoElastoPlasticMultipleNonLocalDamage()->getDamage(1);
  }
  else if (i == IPField::PLASTIC_POISSON_RATIO){
    return getIPHyperViscoElastoPlasticMultipleNonLocalDamage()->_nup;
  }
  else if (i == IPField::PLASTICSTRAIN_COMPRESSION){
    return getIPHyperViscoElastoPlasticMultipleNonLocalDamage()->getConstRefToCompressionPlasticStrain();
  }
  else if (i == IPField::PLASTICSTRAIN_TRACTION){
    return getIPHyperViscoElastoPlasticMultipleNonLocalDamage()->getConstRefToTractionPlasticStrain();
  }
  else if (i == IPField::PLASTICSTRAIN_SHEAR){
    return getIPHyperViscoElastoPlasticMultipleNonLocalDamage()->getConstRefToShearPlasticStrain();
  }
  else if (i == IPField::BACKSTRESS){
   if (getIPHyperViscoElastoPlasticMultipleNonLocalDamage()->_ipKinematic!= NULL)
      return getIPHyperViscoElastoPlasticMultipleNonLocalDamage()->_ipKinematic->getR();
    else
      return 0.;
  }
  else if (i == IPField::PLASTIC_RATE){
    return getIPHyperViscoElastoPlasticMultipleNonLocalDamage()->getConstRefToPlasticDeformationRate();
  }
  else if (i == IPField::K_XX){
    const STensor3& K = getIPHyperViscoElastoPlasticMultipleNonLocalDamage()->_kirchhoff;
    return K(0,0);
  }
  else if (i == IPField::K_YY){
    const STensor3& K = getIPHyperViscoElastoPlasticMultipleNonLocalDamage()->_kirchhoff;
    return K(1,1);
  }
  else if (i == IPField::K_ZZ){
    const STensor3& K = getIPHyperViscoElastoPlasticMultipleNonLocalDamage()->_kirchhoff;
    return K(2,2);
  }
  else if (i == IPField::K_XY){
    const STensor3& K = getIPHyperViscoElastoPlasticMultipleNonLocalDamage()->_kirchhoff;
    return K(0,1);
  }
  else if (i == IPField::K_YZ){
    const STensor3& K = getIPHyperViscoElastoPlasticMultipleNonLocalDamage()->_kirchhoff;
    return K(1,2);
  }
  else if (i == IPField::K_XZ){
    const STensor3& K = getIPHyperViscoElastoPlasticMultipleNonLocalDamage()->_kirchhoff;
    return K(0,2);
  }
  else
  {
    return dG3DIPVariable::get(i);
  }
}
double nonLocalDamageHyperelasticDG3DIPVariableWithFailure::defoEnergy() const
{
  return getIPHyperViscoElastoPlasticMultipleNonLocalDamage()->defoEnergy();
}
double nonLocalDamageHyperelasticDG3DIPVariableWithFailure::plasticEnergy() const
{
  return getIPHyperViscoElastoPlasticMultipleNonLocalDamage()->plasticEnergy();
}

double nonLocalDamageHyperelasticDG3DIPVariableWithFailure::getConstRefToLocalVariable(const int idex) const{
  if (idex == 0){
    return getIPHyperViscoElastoPlasticMultipleNonLocalDamage()->getCurrentPlasticStrain();
  }
  else if (idex == 1){
    return getIPHyperViscoElastoPlasticMultipleNonLocalDamage()->getFailurePlasticity();
  }
  else{
    Msg::Error("the non-local variable %d is not defined",idex);
    return 0.;
  }
};
double& nonLocalDamageHyperelasticDG3DIPVariableWithFailure::getRefToLocalVariable(const int idex){
  if (idex == 0){
    return getIPHyperViscoElastoPlasticMultipleNonLocalDamage()->getRefToCurrentPlasticStrain();
  }
  else if (idex == 1){
    return getIPHyperViscoElastoPlasticMultipleNonLocalDamage()->getRefToFailurePlasticity();
  }
  else{
    Msg::Error("the non-local variable %d is not defined",idex);
    static double a(0.);
    return a;
  }
};

double nonLocalDamageHyperelasticDG3DIPVariableWithFailure::getConstRefToNonLocalVariableInMaterialLaw(const int idex) const{
  if (idex == 0){
    return getIPHyperViscoElastoPlasticMultipleNonLocalDamage()->getEffectivePlasticStrain();
  }
  else if (idex == 1){
    return getIPHyperViscoElastoPlasticMultipleNonLocalDamage()->getNonLocalFailurePlasticity();
  }
  else{
    Msg::Error("the non-local variable %d is not defined",idex);
    return 0.;
  }
};
double& nonLocalDamageHyperelasticDG3DIPVariableWithFailure::getRefToNonLocalVariableInMaterialLaw(const int idex){
  if (idex == 0){
    return getIPHyperViscoElastoPlasticMultipleNonLocalDamage()->getRefToEffectivePlasticStrain();
  }
  else if (idex == 1){
    return getIPHyperViscoElastoPlasticMultipleNonLocalDamage()->getRefToNonLocalFailurePlasticity();
  }
  else{
    Msg::Error("the non-local variable %d is not defined",idex);
    static double a(0.);
    return a;
  }
};

void nonLocalDamageHyperelasticDG3DIPVariableWithFailure::restart()
{
  dG3DIPVariable::restart();
  restartManager::restart(_nldHyperipv);
  return;
}


// ############################################################################## END CLASS - START TVEVP ######################################################################################

nonLocalDamageNonLinearTVEVPDG3DIPVariable::nonLocalDamageNonLinearTVEVPDG3DIPVariable(const mlawNonLocalDamagaNonLinearTVEVP &mlaw, const bool createBodyForceHO, const bool oninter)
          : dG3DIPVariable(createBodyForceHO, oninter,1,1), extraDofIPVariableBase()
{
  _nldTVEVP = new IPNonLinearTVEVPNonLocalDamage(mlaw.getConstRefToCompressionHardening(),
                                                            mlaw.getConstRefToTractionHardening(),
                                                            mlaw.getConstRefToKinematicHardening(),
                                                            mlaw.getViscoElasticNumberOfElement(),
                                                            mlaw.getConstRefToMullinsEffect(),
                                                            mlaw.getCLengthLaw(),mlaw.getDamageLaw());
}

nonLocalDamageNonLinearTVEVPDG3DIPVariable::nonLocalDamageNonLinearTVEVPDG3DIPVariable(const nonLocalDamageNonLinearTVEVPDG3DIPVariable &source):
          dG3DIPVariable(source),extraDofIPVariableBase()
{
  _nldTVEVP = new IPNonLinearTVEVPNonLocalDamage(*source._nldTVEVP);
}

nonLocalDamageNonLinearTVEVPDG3DIPVariable& nonLocalDamageNonLinearTVEVPDG3DIPVariable::operator=(const IPVariable &source){
  dG3DIPVariable::operator=(source);
  const nonLocalDamageNonLinearTVEVPDG3DIPVariable* src = dynamic_cast<const nonLocalDamageNonLinearTVEVPDG3DIPVariable*>(&source);
  if(src != NULL)
  {
    _nldTVEVP->operator= (*dynamic_cast<const IPVariable*>(src->_nldTVEVP));
  }
  return *this;
}

double nonLocalDamageNonLinearTVEVPDG3DIPVariable::get(const int i) const
{
  if(i == IPField::PLASTICSTRAIN)
  {
    return getIPNonLinearTVEVPNonLocalDamage()->getCurrentPlasticStrain();
  }
  else if (i == IPField::NONLOCAL_PLASTICSTRAIN){
    return getIPNonLinearTVEVPNonLocalDamage()->getEffectivePlasticStrain();
  }
  else if(i == IPField::DAMAGE)
  {
    return getIPNonLinearTVEVPNonLocalDamage()->getDamage();
  }
  else if (i == IPField::DAMAGE_0){
    return getIPNonLinearTVEVPNonLocalDamage()->getDamage();
  }
  else if (i == IPField::PLASTIC_POISSON_RATIO){
    return getIPNonLinearTVEVPNonLocalDamage()->_nup;
  }
  else if (i == IPField::PLASTICSTRAIN_COMPRESSION){
    return getIPNonLinearTVEVPNonLocalDamage()->getConstRefToCompressionPlasticStrain();
  }
  else if (i == IPField::PLASTICSTRAIN_TRACTION){
    return getIPNonLinearTVEVPNonLocalDamage()->getConstRefToTractionPlasticStrain();
  }
  else if (i == IPField::PLASTICSTRAIN_SHEAR){
    return getIPNonLinearTVEVPNonLocalDamage()->getConstRefToShearPlasticStrain();
  }
  else if (i == IPField::BACKSTRESS){
   if (getIPNonLinearTVEVPNonLocalDamage()->_ipKinematic!= NULL)
      return getIPNonLinearTVEVPNonLocalDamage()->_ipKinematic->getR();
    else
      return 0.;
  }
  else if (i == IPField::PLASTIC_RATE){
    return getIPNonLinearTVEVPNonLocalDamage()->getConstRefToPlasticDeformationRate();
  }
  else if (i == IPField::K_XX){
    const STensor3& K = getIPNonLinearTVEVPNonLocalDamage()->_kirchhoff;
    return K(0,0);
  }
  else if (i == IPField::K_YY){
    const STensor3& K = getIPNonLinearTVEVPNonLocalDamage()->_kirchhoff;
    return K(1,1);
  }
  else if (i == IPField::K_ZZ){
    const STensor3& K = getIPNonLinearTVEVPNonLocalDamage()->_kirchhoff;
    return K(2,2);
  }
  else if (i == IPField::K_XY){
    const STensor3& K = getIPNonLinearTVEVPNonLocalDamage()->_kirchhoff;
    return K(0,1);
  }
  else if (i == IPField::K_YZ){
    const STensor3& K = getIPNonLinearTVEVPNonLocalDamage()->_kirchhoff;
    return K(1,2);
  }
  else if (i == IPField::K_XZ){
    const STensor3& K = getIPNonLinearTVEVPNonLocalDamage()->_kirchhoff;
    return K(0,2);
  }
  else
  {
    return dG3DIPVariable::get(i);
  }
}
double nonLocalDamageNonLinearTVEVPDG3DIPVariable::defoEnergy() const
{
  return getIPNonLinearTVEVPNonLocalDamage()->defoEnergy();
}
double nonLocalDamageNonLinearTVEVPDG3DIPVariable::plasticEnergy() const
{
  return getIPNonLinearTVEVPNonLocalDamage()->plasticEnergy();
}

double nonLocalDamageNonLinearTVEVPDG3DIPVariable::getConstRefToLocalVariable(const int idex) const{
  if (idex == 0){
    return getIPNonLinearTVEVPNonLocalDamage()->getCurrentPlasticStrain();
  }
  else{
    Msg::Error("the non-local variable %d is not defined",idex);
    return 0.;
  }
};
double& nonLocalDamageNonLinearTVEVPDG3DIPVariable::getRefToLocalVariable(const int idex){
  if (idex == 0){
    return getIPNonLinearTVEVPNonLocalDamage()->getRefToCurrentPlasticStrain();
  }
  else{
    Msg::Error("the non-local variable %d is not defined",idex);
    static double a(0.);
    return a;
  }
};

double nonLocalDamageNonLinearTVEVPDG3DIPVariable::getConstRefToNonLocalVariableInMaterialLaw(const int idex) const{
  if (idex == 0){
    return getIPNonLinearTVEVPNonLocalDamage()->getEffectivePlasticStrain();
  }
  else{
    Msg::Error("the non-local variable %d is not defined",idex);
    return 0.;
  }
};
double& nonLocalDamageNonLinearTVEVPDG3DIPVariable::getRefToNonLocalVariableInMaterialLaw(const int idex){
  if (idex == 0){
    return getIPNonLinearTVEVPNonLocalDamage()->getRefToEffectivePlasticStrain();
  }
  else{
    Msg::Error("the non-local variable %d is not defined",idex);
    static double a(0.);
    return a;
  }
};

void nonLocalDamageNonLinearTVEVPDG3DIPVariable::restart()
{
  dG3DIPVariable::restart();
  if (_nldTVEVP != NULL)
    restartManager::restart(_nldTVEVP);
  return;
}


// ################################################################# END CLASS 1 ######################################################################################


localDamagenonLinearTVEVPDG3DIPVariableWithFailure::localDamagenonLinearTVEVPDG3DIPVariableWithFailure(const mlawLocalDamageNonLinearTVEVPWithFailure &mlaw, const bool createBodyForceHO, const bool oninter)
          :dG3DIPVariable(createBodyForceHO, oninter,1,1), extraDofIPVariableBase()
{
  _localIPv = new IPNonLinearTVEVPMultipleLocalDamage(mlaw.getConstRefToCompressionHardening(),
                                                            mlaw.getConstRefToTractionHardening(),
                                                            mlaw.getConstRefToKinematicHardening(),
                                                            mlaw.getViscoElasticNumberOfElement(),
                                                            mlaw.getConstRefToMullinsEffect(),
                                                            mlaw.getDamageLaw());
}

localDamagenonLinearTVEVPDG3DIPVariableWithFailure::localDamagenonLinearTVEVPDG3DIPVariableWithFailure(const localDamagenonLinearTVEVPDG3DIPVariableWithFailure &source):
          dG3DIPVariable(source),extraDofIPVariableBase()
{
  _localIPv = new IPNonLinearTVEVPMultipleLocalDamage(*source._localIPv);
}

localDamagenonLinearTVEVPDG3DIPVariableWithFailure& localDamagenonLinearTVEVPDG3DIPVariableWithFailure::operator=(const IPVariable &source){
  dG3DIPVariable::operator=(source);
  const localDamagenonLinearTVEVPDG3DIPVariableWithFailure* src = dynamic_cast<const localDamagenonLinearTVEVPDG3DIPVariableWithFailure*>(&source);
  if(src != NULL)
  {
    _localIPv->operator= (*dynamic_cast<const IPVariable*>(src->_localIPv));
  }
  return *this;
}

double localDamagenonLinearTVEVPDG3DIPVariableWithFailure::get(const int i) const
{
  if(i == IPField::PLASTICSTRAIN)
  {
    return getIPNonLinearTVEVPMultipleLocalDamage()->getConstRefToEqPlasticStrain();
  }
  else if (i == IPField::FAILURE_ONSET){
    return getIPNonLinearTVEVPMultipleLocalDamage()->getFailureOnset();
  }
  else if (i == IPField::FAILURE_PLASTICSTRAIN){
    return getIPNonLinearTVEVPMultipleLocalDamage()->getFailurePlasticity();
  }
  else if(i == IPField::DAMAGE)
  {
    double OneminusD = 1;
    int n =  this->getIPNonLinearTVEVPMultipleLocalDamage()->getNumNonLocalVariable();
    for (int iter =0; iter < n; iter ++){
      double Diter = this->getIPNonLinearTVEVPMultipleLocalDamage()->getDamage(iter);
      OneminusD *= (1.- Diter);
    }
    return (1.- OneminusD);
  }
  else if (i == IPField::DAMAGE_0){
    return getIPNonLinearTVEVPMultipleLocalDamage()->getDamage(0);
  }
  else if (i == IPField::DAMAGE_1){
    return getIPNonLinearTVEVPMultipleLocalDamage()->getDamage(1);
  }
  else if (i == IPField::PLASTIC_POISSON_RATIO){
    return getIPNonLinearTVEVPMultipleLocalDamage()->_nup;
  }
  else if (i == IPField::PLASTICSTRAIN_COMPRESSION){
    return getIPNonLinearTVEVPMultipleLocalDamage()->getConstRefToCompressionPlasticStrain();
  }
  else if (i == IPField::PLASTICSTRAIN_TRACTION){
    return getIPNonLinearTVEVPMultipleLocalDamage()->getConstRefToTractionPlasticStrain();
  }
  else if (i == IPField::PLASTICSTRAIN_SHEAR){
    return getIPNonLinearTVEVPMultipleLocalDamage()->getConstRefToShearPlasticStrain();
  }
  else if (i == IPField::BACKSTRESS){
   if (getIPNonLinearTVEVPMultipleLocalDamage()->_ipKinematic!= NULL)
      return getIPNonLinearTVEVPMultipleLocalDamage()->_ipKinematic->getR();
    else
      return 0.;
  }
  else
  {
    return dG3DIPVariable::get(i);
  }
}

void localDamagenonLinearTVEVPDG3DIPVariableWithFailure::restart()
{
  dG3DIPVariable::restart();
  if (_localIPv != NULL)
    restartManager::restart(_localIPv);
  return;
}

// ############################################################################## END CLASS 2 ######################################################################################

nonLocalDamageNonLinearTVEVPDG3DIPVariableWithFailure::nonLocalDamageNonLinearTVEVPDG3DIPVariableWithFailure(const mlawNonLocalDamageNonLinearTVEVPWithFailure &mlaw, const bool createBodyForceHO, const bool oninter)
          :dG3DIPVariable(createBodyForceHO, oninter,1,1), extraDofIPVariableBase()
{
  _nldTVEVPipv = new IPNonLinearTVEVPMultipleNonLocalDamage(mlaw.getConstRefToCompressionHardening(),
                                                            mlaw.getConstRefToTractionHardening(),
                                                            mlaw.getConstRefToKinematicHardening(),
                                                            mlaw.getViscoElasticNumberOfElement(),
                                                            mlaw.getConstRefToMullinsEffect(),
                                                            mlaw.getCLengthLaw(),mlaw.getDamageLaw());
}

  nonLocalDamageNonLinearTVEVPDG3DIPVariableWithFailure::nonLocalDamageNonLinearTVEVPDG3DIPVariableWithFailure(const nonLocalDamageNonLinearTVEVPDG3DIPVariableWithFailure &source):
          dG3DIPVariable(source),extraDofIPVariableBase()
{
  _nldTVEVPipv = new IPNonLinearTVEVPMultipleNonLocalDamage(*source._nldTVEVPipv);
}

nonLocalDamageNonLinearTVEVPDG3DIPVariableWithFailure& nonLocalDamageNonLinearTVEVPDG3DIPVariableWithFailure::operator=(const IPVariable &source){
  dG3DIPVariable::operator=(source);
  const nonLocalDamageNonLinearTVEVPDG3DIPVariableWithFailure* src = dynamic_cast<const nonLocalDamageNonLinearTVEVPDG3DIPVariableWithFailure*>(&source);
  if(src != NULL)
  {
    _nldTVEVPipv->operator= (*dynamic_cast<const IPVariable*>(src->_nldTVEVPipv));
  }
  return *this;
}

double nonLocalDamageNonLinearTVEVPDG3DIPVariableWithFailure::get(const int i) const
{
  if(i == IPField::PLASTICSTRAIN)
  {
    return getIPNonLinearTVEVPMultipleNonLocalDamage()->getCurrentPlasticStrain();
  }
  else if (i == IPField::NONLOCAL_PLASTICSTRAIN){
    return getIPNonLinearTVEVPMultipleNonLocalDamage()->getEffectivePlasticStrain();
  }
  else if (i == IPField::FAILURE_ONSET){
    return getIPNonLinearTVEVPMultipleNonLocalDamage()->getFailureOnset();
  }
  else if (i == IPField::FAILURE_PLASTICSTRAIN){
    return getIPNonLinearTVEVPMultipleNonLocalDamage()->getFailurePlasticity();
  }
  else if (i == IPField::NONLOCAL_FAILURE_PLASTICSTRAIN){
    return getIPNonLinearTVEVPMultipleNonLocalDamage()->getNonLocalFailurePlasticity();
  }
  else if(i == IPField::DAMAGE)
  {
    double OneminusD = 1;
    int n =  this->getIPNonLinearTVEVPMultipleNonLocalDamage()->getNumNonLocalVariable();
    for (int iter =0; iter < n; iter ++){
      double Diter = this->getIPNonLinearTVEVPMultipleNonLocalDamage()->getDamage(iter);
      OneminusD *= (1.- Diter);
    }
    return (1.- OneminusD);
  }
  else if (i == IPField::DAMAGE_0){
    return getIPNonLinearTVEVPMultipleNonLocalDamage()->getDamage(0);
  }
  else if (i == IPField::DAMAGE_1){
    return getIPNonLinearTVEVPMultipleNonLocalDamage()->getDamage(1);
  }
  else if (i == IPField::PLASTIC_POISSON_RATIO){
    return getIPNonLinearTVEVPMultipleNonLocalDamage()->_nup;
  }
  else if (i == IPField::PLASTICSTRAIN_COMPRESSION){
    return getIPNonLinearTVEVPMultipleNonLocalDamage()->getConstRefToCompressionPlasticStrain();
  }
  else if (i == IPField::PLASTICSTRAIN_TRACTION){
    return getIPNonLinearTVEVPMultipleNonLocalDamage()->getConstRefToTractionPlasticStrain();
  }
  else if (i == IPField::PLASTICSTRAIN_SHEAR){
    return getIPNonLinearTVEVPMultipleNonLocalDamage()->getConstRefToShearPlasticStrain();
  }
  else if (i == IPField::BACKSTRESS){
   if (getIPNonLinearTVEVPMultipleNonLocalDamage()->_ipKinematic!= NULL)
      return getIPNonLinearTVEVPMultipleNonLocalDamage()->_ipKinematic->getR();
    else
      return 0.;
  }
  else if (i == IPField::PLASTIC_RATE){
    return getIPNonLinearTVEVPMultipleNonLocalDamage()->getConstRefToPlasticDeformationRate();
  }
  else if (i == IPField::K_XX){
    const STensor3& K = getIPNonLinearTVEVPMultipleNonLocalDamage()->_kirchhoff;
    return K(0,0);
  }
  else if (i == IPField::K_YY){
    const STensor3& K = getIPNonLinearTVEVPMultipleNonLocalDamage()->_kirchhoff;
    return K(1,1);
  }
  else if (i == IPField::K_ZZ){
    const STensor3& K = getIPNonLinearTVEVPMultipleNonLocalDamage()->_kirchhoff;
    return K(2,2);
  }
  else if (i == IPField::K_XY){
    const STensor3& K = getIPNonLinearTVEVPMultipleNonLocalDamage()->_kirchhoff;
    return K(0,1);
  }
  else if (i == IPField::K_YZ){
    const STensor3& K = getIPNonLinearTVEVPMultipleNonLocalDamage()->_kirchhoff;
    return K(1,2);
  }
  else if (i == IPField::K_XZ){
    const STensor3& K = getIPNonLinearTVEVPMultipleNonLocalDamage()->_kirchhoff;
    return K(0,2);
  }
  else
  {
    return dG3DIPVariable::get(i);
  }
}
double nonLocalDamageNonLinearTVEVPDG3DIPVariableWithFailure::defoEnergy() const
{
  return getIPNonLinearTVEVPMultipleNonLocalDamage()->defoEnergy();
}
double nonLocalDamageNonLinearTVEVPDG3DIPVariableWithFailure::plasticEnergy() const
{
  return getIPNonLinearTVEVPMultipleNonLocalDamage()->plasticEnergy();
}

double nonLocalDamageNonLinearTVEVPDG3DIPVariableWithFailure::getConstRefToLocalVariable(const int idex) const{
  if (idex == 0){
    return getIPNonLinearTVEVPMultipleNonLocalDamage()->getCurrentPlasticStrain();
  }
  else if (idex == 1){
    return getIPNonLinearTVEVPMultipleNonLocalDamage()->getFailurePlasticity();
  }
  else{
    Msg::Error("the non-local variable %d is not defined",idex);
    return 0.;
  }
};
double& nonLocalDamageNonLinearTVEVPDG3DIPVariableWithFailure::getRefToLocalVariable(const int idex){
  if (idex == 0){
    return getIPNonLinearTVEVPMultipleNonLocalDamage()->getRefToCurrentPlasticStrain();
  }
  else if (idex == 1){
    return getIPNonLinearTVEVPMultipleNonLocalDamage()->getRefToFailurePlasticity();
  }
  else{
    Msg::Error("the non-local variable %d is not defined",idex);
    static double a(0.);
    return a;
  }
};

double nonLocalDamageNonLinearTVEVPDG3DIPVariableWithFailure::getConstRefToNonLocalVariableInMaterialLaw(const int idex) const{
  if (idex == 0){
    return getIPNonLinearTVEVPMultipleNonLocalDamage()->getEffectivePlasticStrain();
  }
  else if (idex == 1){
    return getIPNonLinearTVEVPMultipleNonLocalDamage()->getNonLocalFailurePlasticity();
  }
  else{
    Msg::Error("the non-local variable %d is not defined",idex);
    return 0.;
  }
};
double& nonLocalDamageNonLinearTVEVPDG3DIPVariableWithFailure::getRefToNonLocalVariableInMaterialLaw(const int idex){
  if (idex == 0){
    return getIPNonLinearTVEVPMultipleNonLocalDamage()->getRefToEffectivePlasticStrain();
  }
  else if (idex == 1){
    return getIPNonLinearTVEVPMultipleNonLocalDamage()->getRefToNonLocalFailurePlasticity();
  }
  else{
    Msg::Error("the non-local variable %d is not defined",idex);
    static double a(0.);
    return a;
  }
};

void nonLocalDamageNonLinearTVEVPDG3DIPVariableWithFailure::restart()
{
  dG3DIPVariable::restart();
  if (_nldTVEVPipv != NULL)
    restartManager::restart(_nldTVEVPipv);
  return;
}

// ############################################################################## END CLASS 3 ######################################################################################
