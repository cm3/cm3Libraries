//
// C++ Interface: terms
//
// Description: Class of terms for dg non linear shell
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "dG3DTerms.h"
#include "highOrderTensor.h"
#include "FractureCohesiveDG3DIPVariable.h"
#include "nonLocalDamageDG3DIPVariable.h"
#include "FractureCohesiveDG3DMaterialLaw.h"
#include "mixedFunctionSpace.h"
#include "dG3DCurlFunctionSpace.h"

void g3DLoadTerm::get(MElement *ele,int npts,IntPt *GP,fullVector<double> &m) const
{
  const int nbdof=this->space1.getNumKeys(ele);
  m.resize(nbdof);
  m.setAll(0.);

  SPoint3 p;
  double scaleEq = 1.;
  if (comp < 3){
    scaleEq = 1.; // disp
  }
  else if (3<=comp  and comp < 3+getNumNonLocalVariable()){
    scaleEq = getNonLocalEqRatio(); // nonlocal Dof
  }
  else if (3+getNumNonLocalVariable()<=comp and comp < 3+getNumNonLocalVariable()+getNumConstitutiveExtraDofDiffusionVariable()){
    scaleEq = getConstitutiveExtraDofDiffusionEqRatio(); // extraDof
  }
  else if (3+getNumNonLocalVariable()+getNumConstitutiveExtraDofDiffusionVariable() <=comp  && 
           comp < 3+getNumNonLocalVariable()+getNumConstitutiveExtraDofDiffusionVariable()+getNumConstitutiveCurlVariable())
  {
    scaleEq = getConstitutiveCurlEqRatio(); // curlData
  }
  else{
    Msg::Error("comp = %d has not been wrongly introduced in g3DLoadTerm::get",comp);
  }
    // Get shape functions values and gradients at all Gauss points
  nlsFunctionSpace<double>* sp1 = static_cast<nlsFunctionSpace<double>*>(&(this->space1));
  for (int i = 0; i < npts; i++)
  {
    const double weight = GP[i].weight;
    const double u = GP[i].pt[0];
    const double v = GP[i].pt[1];
    const double w = GP[i].pt[2];
    std::vector<double> Vals;
    // space->f must be used as dof depends on context of space
    sp1->f(ele,u,v,w,Vals);
    double detJ = ele->getJacobianDeterminant(u,v,w);
    ele->pnt(Vals,p);
    double load=Load->operator()(p.x(),p.y(),p.z());
    double loadvalue = load*weight*detJ;
    for (int j = 0; j < nbdof ; ++j)
    {
      m(j)+=Vals[j]*loadvalue*scaleEq;
    }
  }
}

void g3DFiniteStrainsPressureTerm::get(MElement *ele,int npts,IntPt *GP,fullVector<double> &m) const
{
  // resize the force vector
  const int nbdof=this->space1.getNumKeys(ele);
  m.resize(nbdof);
  m.setAll(0.);

  // get the displacements of the element
  std::vector<Dof> keys; // must be all dofs or al least ux, uy, uz dofs
  this->space1.getKeys(ele,keys);
  fullVector<double> disp(keys.size());
  _ufield->get(keys,disp);
  
  // Get shape functions values and gradients at all Gauss points
  nlsFunctionSpace<double>* sp1 = static_cast<nlsFunctionSpace<double>*>(&(this->space1));
  std::vector<GaussPointSpaceValues<double>*> vgps;
  sp1->get(ele,npts,GP,vgps);
  //
  SPoint3 p;
  for (int i = 0; i < npts; i++)
  {
    const double weight = GP[i].weight;
    const double u = GP[i].pt[0];
    const double v = GP[i].pt[1];
    const double w = GP[i].pt[2];
    // vals and grads must be evaluated by space, not element basis
    std::vector<TensorialTraits<double>::ValType> Vals;
    std::vector<TensorialTraits<double>::GradType> Grads;
    sp1->f(ele,u,v,w,Vals);
    sp1->gradf(ele,u,v,w,Grads);

    static STensor3 defo_grad;
    STensorOperation::unity(defo_grad);
    
    for (int cc=0; cc<3; cc++)
    {
      int nbFF = sp1->getNumShapeFunctions(ele,cc);
      int nbFFTotalLast = sp1->getShapeFunctionsIndex(ele,cc);
      for (int j = 0; j < nbFF; j++)
      {
        //x y z: take compoenent x arbitrarily
        // xx yy zz xy xz yz
        defo_grad(cc,0) += Grads[j+nbFFTotalLast][0]*disp(j+nbFFTotalLast);
        defo_grad(cc,1) += Grads[j+nbFFTotalLast][1]*disp(j+nbFFTotalLast);
        defo_grad(cc,2) += Grads[j+nbFFTotalLast][2]*disp(j+nbFFTotalLast);
      }
    }
    if(STensorOperation::determinantSTensor3(defo_grad) < 1.e-15) Msg::Info("Negative Jacobian g3DFiniteStrainsPressureTerm::get");
    

    // defo gradient and jac
    double jdefo = STensorOperation::determinantSTensor3(defo_grad);
    static STensor3 invF;
    STensorOperation::inverseSTensor3(defo_grad,invF);
    //
    std::vector<TensorialTraits<double>::GradType> &graduv = vgps[i]->_vgrads; // must be taken from element basis
    SVector3 ref_normal(0.,0.,0.);
    if (dom->getDim() == 3)
    {
      // initial normal de l'élément (compute and store it once at the beginning)
      SVector3 phi1(0.), phi2(0.);
      for(int j=0;j<ele->getNumVertices();++j)
      {
          phi1[0] += graduv[j](0) * ele->getVertex(j)->x(); phi2[0] += graduv[j](1) * ele->getVertex(j)->x();
          phi1[1] += graduv[j](0) * ele->getVertex(j)->y(); phi2[1] += graduv[j](1) * ele->getVertex(j)->y();
          phi1[2] += graduv[j](0) * ele->getVertex(j)->z(); phi2[2] += graduv[j](1) * ele->getVertex(j)->z();
      }
      ref_normal = crossprod(phi1,phi2);        
    }
    else if (dom->getDim() == 2)
    {
      // initial normal de l'élément (compute and store it once at the beginning)
      SVector3 phi1(0.);
      for(int j=0;j<ele->getNumVertices();++j)
      {
          phi1[0] += graduv[j](0) * ele->getVertex(j)->x(); 
          phi1[1] += graduv[j](0) * ele->getVertex(j)->y(); 
          phi1[2] += graduv[j](0) * ele->getVertex(j)->z(); 
      }
      ref_normal = crossprod(phi1,normalToDomain);
    }
    double detJ = ref_normal.normalize(); //  norm of ref_normal equal to detJ (Gauss integration) and  ref_normal become unit vector

    // current normal using Nanson formula
    static SVector3 cur_normal;
    STensorOperation::multSVector3STensor3(ref_normal,invF,cur_normal); // cur_normal  is not unit vector

    // value of the force at the given Gauss points. 
    ele->pnt(Vals,p);
    double load=Load->operator()(p.x(),p.y(),p.z());

    double loadvalue = -load*weight*detJ; /// minus as cur_normal == outward normal
    //
    
    for (int k=0; k< 3; k++)
    {
      int nbFF = sp1->getNumShapeFunctions(ele,k);
      int nbFFTotalLast = sp1->getShapeFunctionsIndex(ele,k);
      for (int j = 0; j < nbFF ; j++)
      {
        m(j+nbFFTotalLast)+=Vals[j+nbFFTotalLast]*loadvalue*jdefo*cur_normal[k];
      }
    }
  }
  //m.print("pressure term");
}

void g3DFiniteStrainsPressureBilinearTerm::get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const
{
  // resize the force vector
  const int nbdof=this->space1.getNumKeys(ele);
  m.resize(nbdof,nbdof);
  m.setAll(0.);

  // get the displacements of the element
  std::vector<Dof> keys;
  this->space1.getKeys(ele,keys);
  fullVector<double> disp(keys.size());
  _ufield->get(keys,disp);
  
  // Get shape functions values and gradients at all Gauss points
  nlsFunctionSpace<double>* sp1 = static_cast<nlsFunctionSpace<double>*>(&(this->space1));
  std::vector<GaussPointSpaceValues<double>*> vgps;
  sp1->get(ele,npts,GP,vgps);
  
  SPoint3 p;

  for (int i = 0; i < npts; i++)
  {
    const double weight = GP[i].weight;
    const double u = GP[i].pt[0];
    const double v = GP[i].pt[1];
    const double w = GP[i].pt[2];
    // vals and grads must be evaluated by space, not element basis
    std::vector<TensorialTraits<double>::ValType> Vals;
    std::vector<TensorialTraits<double>::GradType> Grads;
    sp1->f(ele,u,v,w,Vals);
    sp1->gradf(ele,u,v,w,Grads);
    
    static STensor3 defo_grad;
    STensorOperation::unity(defo_grad);
    
    for (int cc=0; cc<3; cc++)
    {
      int nbFF = sp1->getNumShapeFunctions(ele,cc);
      int nbFFTotalLast = sp1->getShapeFunctionsIndex(ele,cc);
      for (int j = 0; j < nbFF; j++)
      {
        //x y z: take compoenent x arbitrarily
        // xx yy zz xy xz yz
        defo_grad(cc,0) += Grads[j+nbFFTotalLast][0]*disp(j+nbFFTotalLast);
        defo_grad(cc,1) += Grads[j+nbFFTotalLast][1]*disp(j+nbFFTotalLast);
        defo_grad(cc,2) += Grads[j+nbFFTotalLast][2]*disp(j+nbFFTotalLast);
      }
    }
    if(STensorOperation::determinantSTensor3(defo_grad) < 1.e-15) Msg::Info("Negative Jacobian g3DFiniteStrainsPressureBilinearTerm::get");
    

    // defo gradient and jac
    double jdefo = STensorOperation::determinantSTensor3(defo_grad);
    static STensor3 invF;
    STensorOperation::inverseSTensor3(defo_grad,invF);

    SVector3 ref_normal(0.,0.,0.);
    std::vector<TensorialTraits<double>::GradType> &graduv = vgps[i]->_vgrads;
    if (dom->getDim() == 3)
    {
      // initial normal de l'élément (compute and store it once at the beginning)
      SVector3 phi1(0.), phi2(0.);
      for(int j=0;j<ele->getNumVertices();++j)
      {
          phi1[0] += graduv[j](0) * ele->getVertex(j)->x(); phi2[0] += graduv[j](1) * ele->getVertex(j)->x();
          phi1[1] += graduv[j](0) * ele->getVertex(j)->y(); phi2[1] += graduv[j](1) * ele->getVertex(j)->y();
          phi1[2] += graduv[j](0) * ele->getVertex(j)->z(); phi2[2] += graduv[j](1) * ele->getVertex(j)->z();
      }
      ref_normal = crossprod(phi1,phi2);        
    }
    else if (dom->getDim() == 2)
    {
      // initial normal de l'élément (compute and store it once at the beginning)
      SVector3 phi1(0.);
      for(int j=0;j<ele->getNumVertices();++j)
      {
          phi1[0] += graduv[j](0) * ele->getVertex(j)->x(); 
          phi1[1] += graduv[j](0) * ele->getVertex(j)->y(); 
          phi1[2] += graduv[j](0) * ele->getVertex(j)->z(); 
      }
      ref_normal = crossprod(phi1,normalToDomain);
    }
    const double detJ = ref_normal.normalize();  //  norm of ref_normal equal to detJ (Gauss integration) and  ref_normal become unit vector
    
    // define cur_normal = ref_normal*invF
    // current normal using Nanson formula
    // Jcur_normal = jdefo*ref_normal*invF
    static STensor33 DJcur_normalDF;
    STensorOperation::zero(DJcur_normalDF);
    for (int k=0; k<3; k++){
      for (int l=0; l<3; l++){
        for (int r=0; r<3; r++){
          for (int s=0; s<3; s++){
            DJcur_normalDF(l,r,s) +=  jdefo*invF(s,r)*ref_normal(k)*invF(k,l) -jdefo*ref_normal(k)*(invF(k,r)*invF(s,l));
          }
        }
      }
    }

    // value of the force at the given Gauss points.
    ele->pnt(Vals,p);
    double load=Load->operator()(p.x(),p.y(),p.z());

    double loadvalue = -load*weight*detJ; /// minus as cur_normal == outward normal
    
    for (int k=0; k<3; k++)
    {
      int nbFFRow = sp1->getNumShapeFunctions(ele,k);
      int nbFFTotalLastRow  = sp1->getShapeFunctionsIndex(ele,k);
      for (int j = 0; j < nbFFRow; j++)
      {
        for (int q=0; q<3; q++)
        {
          int nbFFCol = sp1->getNumShapeFunctions(ele,q);
          int nbFFTotalLastCol = sp1->getShapeFunctionsIndex(ele,q);
          for (int p=0; p< nbFFCol; p++)
          {
            for (int r=0; r<3; r++)
            {
              m(j+nbFFTotalLastRow,p+nbFFTotalLastCol) -=Vals[j+nbFFTotalLastRow]*loadvalue*DJcur_normalDF(k,q,r)*Grads[p+nbFFTotalLastCol](r); // negative as it is the derivative of Fext
            }
          }
        }
      }
    }
  }
}

void g3DFiniteStrainsScalarFluxLinearTerm::get(MElement *ele,int npts,IntPt *GP,fullVector<double> &m) const
{
  // resize the force vector
  const int nbdof=this->space1.getNumKeys(ele);
  m.resize(nbdof);
  m.setAll(0.);

  // get the displacements of the element
  std::vector<Dof> keys;
  this->space1.getKeys(ele,keys);
  fullVector<double> disp(keys.size());
  _ufield->get(keys,disp);

  // Get shape functions values and gradients at all Gauss points
  nlsFunctionSpace<double>* sp1 = static_cast<nlsFunctionSpace<double>*>(&(this->space1));
  std::vector<GaussPointSpaceValues<double>*> vgps;
  sp1->get(ele,npts,GP,vgps);
  
  SPoint3 p;

  for (int i = 0; i < npts; i++)
  {
    const double weight = GP[i].weight;
    const double u = GP[i].pt[0];
    const double v = GP[i].pt[1];
    const double w = GP[i].pt[2];
    // vals and grads must be evaluated by space, not element basis
    std::vector<TensorialTraits<double>::ValType> Vals;
    std::vector<TensorialTraits<double>::GradType> Grads;
    sp1->f(ele,u,v,w,Vals);
    sp1->gradf(ele,u,v,w,Grads);

    // deformation gradient
    static STensor3 defo_grad;
    STensorOperation::unity(defo_grad);
    
    for (int cc=0; cc<3; cc++)
    {
      int nbFF = sp1->getNumShapeFunctions(ele,cc);
      int nbFFTotalLast = sp1->getShapeFunctionsIndex(ele,cc);
      for (int j = 0; j < nbFF; j++)
      {
        //x y z: take compoenent x arbitrarily
        // xx yy zz xy xz yz
        defo_grad(cc,0) += Grads[j+nbFFTotalLast][0]*disp(j+nbFFTotalLast);
        defo_grad(cc,1) += Grads[j+nbFFTotalLast][1]*disp(j+nbFFTotalLast);
        defo_grad(cc,2) += Grads[j+nbFFTotalLast][2]*disp(j+nbFFTotalLast);
      }
    }
    if(STensorOperation::determinantSTensor3(defo_grad) < 1.e-15) Msg::Info("Negative Jacobian g3DFiniteStrainsScalarFluxLinearTerm::get");
    
    // defo gradient and jac
    double jdefo = STensorOperation::determinantSTensor3(defo_grad);
    static STensor3 invF;
    STensorOperation::inverseSTensor3(defo_grad,invF);
    
    SVector3 ref_normal(0.,0.,0.);
    std::vector<TensorialTraits<double>::GradType> &graduv = vgps[i]->_vgrads;
    if (_dom->getDim() == 3)
    {
      // initial normal de l'élément (compute and store it once at the beginning)
      SVector3 phi1(0.), phi2(0.);
      for(int j=0;j<ele->getNumVertices();++j)
      {
          phi1[0] += graduv[j](0) * ele->getVertex(j)->x(); phi2[0] += graduv[j](1) * ele->getVertex(j)->x();
          phi1[1] += graduv[j](0) * ele->getVertex(j)->y(); phi2[1] += graduv[j](1) * ele->getVertex(j)->y();
          phi1[2] += graduv[j](0) * ele->getVertex(j)->z(); phi2[2] += graduv[j](1) * ele->getVertex(j)->z();
      }
      ref_normal = crossprod(phi1,phi2);        
    }
    else if (_dom->getDim() == 2)
    {
      // initial normal de l'élément (compute and store it once at the beginning)
      SVector3 phi1(0.);
      for(int j=0;j<ele->getNumVertices();++j)
      {
          phi1[0] += graduv[j](0) * ele->getVertex(j)->x(); 
          phi1[1] += graduv[j](0) * ele->getVertex(j)->y(); 
          phi1[2] += graduv[j](0) * ele->getVertex(j)->z(); 
      }
      ref_normal = crossprod(phi1,normalToDomain);
    }
    const double detJ = ref_normal.normalize();  //  norm of ref_normal equal to detJ (Gauss integration) and  ref_normal become unit vector
    
    
    // current normal using Nanson formula
    static SVector3 cur_normal;
    STensorOperation::multSVector3STensor3(ref_normal,invF,cur_normal); // cur_normal  is not unit vector
    double normJcur_normal = jdefo*cur_normal.norm();
    
    // value of the force at the given Gauss points
    ele->pnt(Vals,p);
    // loop on the different components
    double load=_load->operator()(p.x(),p.y(),p.z());
    double loadvalue = - load*weight*detJ;
    double depenValue = 1.;
    // get field value
    if (_load->withUnknownDependency())
    {
      const std::vector<int>& depenComps = _load->getDependenceComponents();
      const scalarFunction* depenFunc = _load->getDependenceFunction();
      std::vector<double> fieldValue(depenComps.size());
      for (int ic = 0; ic < depenComps.size(); ic++)
      {
        fieldValue[ic] = 0.;
        int nbFF = sp1->getNumShapeFunctions(ele,depenComps[ic]);
        int nbFFTotalLast = sp1->getShapeFunctionsIndex(ele,depenComps[ic]);
        for (int jc = 0; jc < nbFF; jc++)
        {
          fieldValue[ic] += Vals[jc+nbFFTotalLast]*disp(jc+nbFFTotalLast); 
        }
      }
      depenValue = depenFunc->getVal(fieldValue); 
      //Msg::Info("depenValue = %e fieldValue = %e",depenValue,fieldValue[0]);
    }
    double scaleEq = 1.;
    if (_comp < 3){
      scaleEq = 1.; // disp
    }
    else if (3<=_comp and _comp < 3+getNumNonLocalVariable()){
      scaleEq = getNonLocalEqRatio(); // nonlocal Dof
    }
    else if (3+getNumNonLocalVariable()<=_comp and _comp < 3+getNumNonLocalVariable()+getNumConstitutiveExtraDofDiffusionVariable()){
      scaleEq = getConstitutiveExtraDofDiffusionEqRatio(); // extraDof
    }
    else if (3+getNumNonLocalVariable()+getNumConstitutiveExtraDofDiffusionVariable() <=_comp &&
             _comp < 3+getNumNonLocalVariable()+getNumConstitutiveExtraDofDiffusionVariable()+getNumConstitutiveCurlVariable())
    {
      scaleEq = getConstitutiveCurlEqRatio(); // curlData
    }
    else{
      Msg::Error("comp = %d has not been wrongly introduced in g3DFiniteStrainsScalarFluxLinearTerm::get",_comp);
    }
    int nbFF = sp1->getNumShapeFunctions(ele,_comp);
    int nbFFTotalLast = sp1->getShapeFunctionsIndex(ele,_comp);
    for (int j = 0; j < nbFF ; ++j)
    {
      m(j+nbFFTotalLast)+=Vals[j+nbFFTotalLast]*loadvalue*depenValue*normJcur_normal*scaleEq;
    }
  }
};

void g3DFiniteStrainsScalarFluxBiLinearTerm::get(MElement *ele, int npts, IntPt *GP, fullMatrix<double> &m) const
{
  // resize the force vector
  const int nbdof=this->space2.getNumKeys(ele);
  m.resize(nbdof,nbdof);
  m.setAll(0.);

  // get the displacements of the element
  std::vector<Dof> keys;
  this->space2.getKeys(ele,keys);
  fullVector<double> disp(keys.size());
  _ufield->get(keys,disp);


  // Get shape functions values and gradients at all Gauss points
  nlsFunctionSpace<double>* sp1 = static_cast<nlsFunctionSpace<double>*>(&(this->space1));
  std::vector<GaussPointSpaceValues<double>*> vgps;
  sp1->get(ele,npts,GP,vgps);
  
  SPoint3 p;

  for (int i = 0; i < npts; i++)
  {
    const double weight = GP[i].weight;
    const double u = GP[i].pt[0];
    const double v = GP[i].pt[1];
    const double w = GP[i].pt[2];
    // vals and grads must be evaluated by space, not element basis
    std::vector<TensorialTraits<double>::ValType> Vals;
    std::vector<TensorialTraits<double>::GradType> Grads;
    sp1->f(ele,u,v,w,Vals);
    sp1->gradf(ele,u,v,w,Grads);

    // deformation gradient
    static STensor3 defo_grad;
    STensorOperation::unity(defo_grad);
    
    for (int cc=0; cc<3; cc++)
    {
      int nbFF = sp1->getNumShapeFunctions(ele,cc);
      int nbFFTotalLast = sp1->getShapeFunctionsIndex(ele,cc);
      for (int j = 0; j < nbFF; j++)
      {
        //x y z: take compoenent x arbitrarily
        // xx yy zz xy xz yz
        defo_grad(cc,0) += Grads[j+nbFFTotalLast][0]*disp(j+nbFFTotalLast);
        defo_grad(cc,1) += Grads[j+nbFFTotalLast][1]*disp(j+nbFFTotalLast);
        defo_grad(cc,2) += Grads[j+nbFFTotalLast][2]*disp(j+nbFFTotalLast);
      }
    }
    if(STensorOperation::determinantSTensor3(defo_grad) < 1.e-15) Msg::Info("Negative Jacobian g3DFiniteStrainsScalarFluxBiLinearTerm::get");
    
    // defo gradient and jac
    double jdefo = STensorOperation::determinantSTensor3(defo_grad);
    static STensor3 invF;
    STensorOperation::inverseSTensor3(defo_grad,invF);
    
    SVector3 ref_normal(0.,0.,0.);
    std::vector<TensorialTraits<double>::GradType> &graduv = vgps[i]->_vgrads;
    if (_dom->getDim() == 3)
    {
      // initial normal de l'élément (compute and store it once at the beginning)
      SVector3 phi1(0.), phi2(0.);
      for(int j=0;j<ele->getNumVertices();++j)
      {
          phi1[0] += graduv[j](0) * ele->getVertex(j)->x(); phi2[0] += graduv[j](1) * ele->getVertex(j)->x();
          phi1[1] += graduv[j](0) * ele->getVertex(j)->y(); phi2[1] += graduv[j](1) * ele->getVertex(j)->y();
          phi1[2] += graduv[j](0) * ele->getVertex(j)->z(); phi2[2] += graduv[j](1) * ele->getVertex(j)->z();
      }
      ref_normal = crossprod(phi1,phi2);        
    }
    else if (_dom->getDim() == 2)
    {
      // initial normal de l'élément (compute and store it once at the beginning)
      SVector3 phi1(0.);
      for(int j=0;j<ele->getNumVertices();++j)
      {
          phi1[0] += graduv[j](0) * ele->getVertex(j)->x(); 
          phi1[1] += graduv[j](0) * ele->getVertex(j)->y(); 
          phi1[2] += graduv[j](0) * ele->getVertex(j)->z(); 
      }
      ref_normal = crossprod(phi1,normalToDomain);
    }
    const double detJ = ref_normal.normalize();  //  norm of ref_normal equal to detJ (Gauss integration) and  ref_normal become unit vector
    
    // current normal using Nanson formula
    static SVector3 cur_normal;
    STensorOperation::multSVector3STensor3(ref_normal,invF,cur_normal); // cur_normal  is not unit vector
    double normJcur_normal = jdefo*cur_normal.norm();
    
    // define cur_normal = ref_normal*invF
    // current normal using Nanson formula
    // Jcur_normal = jdefo*ref_normal*invF
    static STensor33 DJcur_normalDF;
    STensorOperation::zero(DJcur_normalDF);
    for (int k=0; k<3; k++){
      for (int l=0; l<3; l++){
        for (int r=0; r<3; r++){
          for (int s=0; s<3; s++){
            DJcur_normalDF(l,r,s) +=  jdefo*invF(s,r)*ref_normal(k)*invF(k,l) -jdefo*ref_normal(k)*(invF(k,r)*invF(s,l));
          }
        }
      }
    }
    static STensor3 DnormJcur_normalDF;
    STensorOperation::zero(DnormJcur_normalDF);
    for (int k=0; k<3; k++){
      for (int l=0; l<3; l++){
        for (int r=0; r<3; r++){
          DnormJcur_normalDF(k,l) += jdefo*cur_normal(r)*DJcur_normalDF(r,k,l)/normJcur_normal;
        }
      }
    }

    // value of the force at the given Gauss points. Change this by a evaluation only at the barycenter?
    ele->pnt(Vals,p);
    double loadvalue= - _load->operator()(p.x(),p.y(),p.z())*weight*detJ;
    double depenValue = 1.;
    std::vector<double> DdepenValue;
    std::vector<int> depenComps;
    const scalarFunction* depenFunc = NULL;
    // get field value
    if (_load->withUnknownDependency())
    {
      depenComps = _load->getDependenceComponents();
      depenFunc =  _load->getDependenceFunction();
      std::vector<double> fieldValue(depenComps.size());
      for (int ic = 0; ic < depenComps.size(); ic++){
        fieldValue[ic] = 0.;
        int nbFF = sp1->getNumShapeFunctions(ele,depenComps[ic]);
        int nbFFTotalLast = sp1->getShapeFunctionsIndex(ele,depenComps[ic]);
        for (int jc = 0; jc < nbFF; jc++)
        {
          fieldValue[ic] += Vals[jc+nbFFTotalLast]*disp(jc+nbFFTotalLast); 
        }
      }
      depenValue *= depenFunc->getVal(fieldValue); 
      depenFunc->getDiff(fieldValue,DdepenValue);
    }
    
    double scaleEq = 1.;
    if (_comp < 3){
      scaleEq = 1.; // disp
    }
    else if (3<=_comp and _comp < 3+getNumNonLocalVariable()){
      scaleEq = getNonLocalEqRatio(); // nonlocal Dof
    }
    else if (3+getNumNonLocalVariable()<=_comp and _comp < 3+getNumNonLocalVariable()+getNumConstitutiveExtraDofDiffusionVariable()){
      scaleEq = getConstitutiveExtraDofDiffusionEqRatio(); // extraDof
    }
    else if (3+getNumNonLocalVariable()+getNumConstitutiveExtraDofDiffusionVariable() <=_comp &&
             _comp < 3+getNumNonLocalVariable()+getNumConstitutiveExtraDofDiffusionVariable()+getNumConstitutiveCurlVariable())
    {
      scaleEq = getConstitutiveCurlEqRatio(); // curlData
    }
    else{
      Msg::Error("comp = %d has not been wrongly introduced in g3DFiniteStrainsScalarFluxLinearTerm::get",_comp);
    }
    
    // loop on the different components
    int nbFFRow = sp1->getNumShapeFunctions(ele,_comp);
    int nbFFTotalLastRow = sp1->getShapeFunctionsIndex(ele,_comp);
    
    for (int j = 0; j < nbFFRow ; ++j)
    {
      for (int q=0; q<3; q++)
      {
        int nbFFColF = sp1->getNumShapeFunctions(ele,q);
        int nbFFTotalLastColF = sp1->getShapeFunctionsIndex(ele,q);
        for (int p=0; p< nbFFColF; p++)
        {
          for (int r=0; r<3; r++)
          {
            m(j+nbFFTotalLastRow,p+nbFFTotalLastColF) -=Vals[j+nbFFTotalLastRow]*loadvalue*depenValue*DnormJcur_normalDF(q,r)*Grads[p+nbFFTotalLastColF](r)*scaleEq; // negative as it is the derivative of Fext
          }
        }
      }
      if (_load->withUnknownDependency())
      {
        for (int ic = 0; ic < depenComps.size(); ic++)
        {
          int nbFFColOther = sp1->getNumShapeFunctions(ele,depenComps[ic]);
          int nbFFTotalLastColOther = sp1->getShapeFunctionsIndex(ele,depenComps[ic]);
          for (int p=0; p< nbFFColOther; p++)
          {
            m(j+nbFFTotalLastRow,p+nbFFTotalLastColOther) -= Vals[j+nbFFTotalLastRow]* loadvalue*normJcur_normal*DdepenValue[ic]*Vals[p+nbFFTotalLastColOther]*scaleEq; // negative as it is the derivative of Fext
          }
        }
      }
    }
  }
}

void dG3DForceBulk::get(MElement *ele,int npts,IntPt *GP,fullVector<double> &vFor) const
{
  // Initialization of some data
  int nbdof = this->space1.getNumKeys(ele);
  vFor.resize(nbdof);
  vFor.setAll(0.);

  nlsFunctionSpace<double>* nlspace = dynamic_cast<nlsFunctionSpace<double>*>(&space1);

  // IP FIELD
  const AllIPState::ipstateElementContainer *vips = _ipf->getAips()->getIPstate(ele->getNum());
  /***
   * STRESS FIELD
   * */
  for (int i = 0; i < npts; i++)
  {
    const IPStateBase *ips        = (*vips)[i];
    const dG3DIPVariableBase *ipv     = static_cast<const dG3DIPVariableBase*>(ips->getState(IPStateBase::current));
    
    double weight = GP[i].weight;
    const double & detJ = ipv->getJacobianDeterminant(ele,GP[i]);
    double ratio = detJ*weight;

    const std::vector<TensorialTraits<double>::ValType> &Vals = ipv->f(&space1,ele,GP[i]);
    const std::vector<TensorialTraits<double>::GradType> &Grads = ipv->gradf(&space1,ele,GP[i]);
    
    const STensor3 *PK1 = &(ipv->getConstRefToFirstPiolaKirchhoffStress());
    static SVector3 BT;
    
    // x y z
    for(int kk=0;kk<3;kk++)
    {
      int nbFFRow = nlspace->getNumShapeFunctions(ele,kk);
      int nbFFTotalLastRow = nlspace->getShapeFunctionsIndex(ele,kk);
      for(int j=0;j<nbFFRow; j++)
      {
        BT(0) = Grads[j+nbFFTotalLastRow][0];
        BT(1) = Grads[j+nbFFTotalLastRow][1];
        BT(2) = Grads[j+nbFFTotalLastRow][2];
      
        for(int m=0; m<3; m++)
        {
          vFor(j+nbFFTotalLastRow) += ratio*(BT(m)*PK1->operator()(kk,m));
        }
        // if bodyforce ...
        if(ipv->hasBodyForceForHO())
        {
          vFor(j+nbFFTotalLastRow) -= ratio*(Vals[j+nbFFTotalLastRow]*ipv->getConstRefToBm()(kk));                 
        }

        if (getConstitutiveCurlAccountMecaSource())
        {
            for (int curlDof = 0; curlDof < ipv->getNumConstitutiveCurlVariable(); ++curlDof)
                vFor(j+nbFFTotalLastRow) += ratio*(Vals[j+nbFFTotalLastRow] * ipv->getConstRefToMechanicalFieldSource(curlDof)(kk));
        }
      }
    }
  }
  //vFor.print("bulkF");
  
  /***
   * NONLOCAL FIELD
   * */
  if (getNumNonLocalVariable() > 0)
  {
    for (int i = 0; i < npts; i++)
    {
      const IPStateBase *ips        = (*vips)[i];
      const dG3DIPVariableBase *ipv     = static_cast<const dG3DIPVariableBase*>(ips->getState(IPStateBase::current));
      const dG3DIPVariableBase *ipvprev     = static_cast<const dG3DIPVariableBase*>(ips->getState(IPStateBase::previous));
      
      double weight = GP[i].weight;
      const double & detJ = ipv->getJacobianDeterminant(ele,GP[i]);
      double ratio = detJ*weight;
      
      // shae function
      const std::vector<TensorialTraits<double>::GradType> &Grads = ipv->gradf(&space1,ele,GP[i]);
      const std::vector<TensorialTraits<double>::ValType> &Vals = ipv->f(&space1,ele,GP[i]);
      
      //non local
      if( ipv->getNumberNonLocalVariable() > getNumNonLocalVariable()) Msg::Error("Your material law uses more non local variables than your domain dG3DForceBulk::get");
      
      for (int nl = 0; nl< ipv->getNumberNonLocalVariable(); nl++)
      {
        const STensor3 *cg = &ipv->getConstRefToCharacteristicLengthMatrix(nl);
        double nonlocalVar = ipv->getConstRefToNonLocalVariable(nl);
        SVector3 gradNonlocalVar = ipv->getConstRefToGradNonLocalVariable()[nl];
        double localVar = ipv->getConstRefToLocalVariable(nl);
        if (_incrementNonlocalBased)
        {
          nonlocalVar -= ipvprev->getConstRefToNonLocalVariable(nl);
          gradNonlocalVar -= ipvprev->getConstRefToGradNonLocalVariable()[nl];
          localVar -= ipvprev->getConstRefToLocalVariable(nl);
        }
                
        int nbFFRow = nlspace->getNumShapeFunctions(ele,3+nl);
        int nbFFTotalLastRow = nlspace->getShapeFunctionsIndex(ele,3+nl);
        
        for(int j=0;j<nbFFRow; j++)
        {
          vFor(j+nbFFTotalLastRow) +=  getNonLocalEqRatio()*ratio*(Vals[j+nbFFTotalLastRow]*nonlocalVar);
          for(int m=0; m<3; m++)
          {
            for(int n=0; n<3; n++)
            {
              vFor(j+nbFFTotalLastRow) +=  getNonLocalEqRatio()*ratio*(Grads[j+nbFFTotalLastRow](m)*cg->operator()(m,n)*gradNonlocalVar(n));
            }
          }
          vFor(j+nbFFTotalLastRow) -=  getNonLocalEqRatio()*ratio*(Vals[j+nbFFTotalLastRow]*localVar);
        }
      }
    }
  }
   /***
    * EXTRADOF FIELD
    * */
  if (getNumConstitutiveExtraDofDiffusionVariable() > 0)
  {
    for (int i = 0; i < npts; i++)
    {
      const IPStateBase *ips        = (*vips)[i];
      const dG3DIPVariableBase *ipv     = static_cast<const dG3DIPVariableBase*>(ips->getState(IPStateBase::current));
      
      double weight = GP[i].weight;
      const double & detJ = ipv->getJacobianDeterminant(ele,GP[i]);
      double ratio = detJ*weight;
      
      // shae function
      const std::vector<TensorialTraits<double>::GradType> &Grads = ipv->gradf(&space1,ele,GP[i]);
      const std::vector<TensorialTraits<double>::ValType> &Vals = ipv->f(&space1,ele,GP[i]);
      
      //extra dof
      if(ipv->getNumConstitutiveExtraDofDiffusionVariable()>getNumConstitutiveExtraDofDiffusionVariable() )
      {
        Msg::Error("Your material law uses more constitutive extra dof variables than your domain dG3DForceBulk::get");
      }
      for (int extraDOFField = 0; extraDOFField< ipv->getNumConstitutiveExtraDofDiffusionVariable(); extraDOFField++)
      {
        const SVector3 &flux = ipv-> getConstRefToFlux()[extraDOFField];
        double w = 0.;
        if (getConstitutiveExtraDofDiffusionAccountFieldSource())
        {
           w += ipv->getConstRefToFieldSource()(extraDOFField);
        }
        if (getConstitutiveExtraDofDiffusionAccountMecaSource()){
           w += ipv->getConstRefToMechanicalSource()(extraDOFField);
        }
        if (getConstitutiveCurlAccountFieldSource())
        {
            w += ipv->getConstRefToEMFieldSource()(extraDOFField); // related to eddy current + hysteresis loss
        }
        int indexField=3+getNumNonLocalVariable()+extraDOFField;
        int nbFFRow = nlspace->getNumShapeFunctions(ele,indexField);
        int nbFFTotalLastRow = nlspace->getShapeFunctionsIndex(ele,indexField);
        for(int j=0;j<nbFFRow; j++)
        {
          for(int m=0; m<3; m++)
          {
            vFor(j+nbFFTotalLastRow) += getConstitutiveExtraDofDiffusionEqRatio()*ratio*(Grads[j+nbFFTotalLastRow][m]*flux(m));
          }
          //if(extraDOFField==0)
          { // corresponds to temperature, but when considering electro-mechanics, ???
            vFor(j+nbFFTotalLastRow) += getConstitutiveExtraDofDiffusionEqRatio()*ratio*(w*Vals[j+nbFFTotalLastRow]);// related to cp
          }
        }
      }
    }
  }
  
  // Curl data
  if (getNumConstitutiveCurlVariable() > 0)
  {
    mixedFunctionSpaceBase* mixedSpace = dynamic_cast<mixedFunctionSpaceBase*>(&space1);
    const int numStandardDof = mixedSpace->getNumKeysByType(mixedFunctionSpaceBase::DOF_STANDARD,ele);
    for (int i = 0; i < npts; i++)
    {
      const IPStateBase *ips        = (*vips)[i];
      const dG3DIPVariableBase *ipv     = static_cast<const dG3DIPVariableBase*>(ips->getState(IPStateBase::current));
      
      double weight = GP[i].weight;
      const double & detJ = ipv->getJacobianDeterminant(ele,GP[i]);
      double ratio = detJ*weight;
      
      // Basis functions for vector field at edges
      const std::vector<SVector3> &curlVals = ipv->fcurl(&space1,ele,GP[i],"HcurlLegendre");
      // Basis functions for Curl of vector field at edges
      const std::vector<SVector3> &CurlcurlVals = ipv->Curlfcurl(&space1,ele,GP[i],"CurlHcurlLegendre");
      
      if(ipv->getNumConstitutiveCurlVariable() > getNumConstitutiveCurlVariable())
      {
        Msg::Error("Your material law uses more constitutive curl variable than your domain dG3DForceBulk::get");
      }
      for (int curlDof=0;curlDof<ipv->getNumConstitutiveCurlVariable() ; curlDof++)
      {
        const int curveFieldIndex = 3 + getNumNonLocalVariable() + getNumConstitutiveExtraDofDiffusionVariable()+curlDof;
        const int nbFFRow =  nlspace->getNumShapeFunctions(ele,curveFieldIndex); // number of shape functinon for curveFieldIndex in curlVals and CurlcurlVals
        const int nbFFTotalLastRow = nlspace->getShapeFunctionsIndex(ele,curveFieldIndex); // the position of the first element of shape functinon for curveFieldIndex in curlVals and CurlcurlVals        

        // SourceVectorField = -fluxje -js0
        SVector3 sourceVectorField(0.0);
        if (getConstitutiveCurlAccountFieldSource())
        {
            sourceVectorField += ipv->getConstRefToSourceVectorField(curlDof);
        }

          const SVector3 & H = ipv->getConstRefToVectorField(curlDof);
          // To test interpolation of curlVals
          //const SVector3 & a = ipv->getConstRefToVectorPotential(curlDof);
        for(int j=0;j<nbFFRow; j++)
        {
          for(int m=0; m<3; m++)
          {
            vFor(j+nbFFTotalLastRow+numStandardDof) += getConstitutiveCurlEqRatio() * ratio * (H[m]*CurlcurlVals[j+nbFFTotalLastRow][m]);
            //vFor(j+nbFFTotalLastRow+numStandardDof) += getConstitutiveCurlEqRatio() * ratio * (a[m]*curlVals[j+nbFFTotalLastRow][m]);
          }
           for(int m=0; m<3; m++)
          {
            vFor(j+nbFFTotalLastRow+numStandardDof) += getConstitutiveCurlEqRatio() * ratio * (sourceVectorField[m]*curlVals[j+nbFFTotalLastRow][m]);
          }
        }
      }
    }
  }
  //vFor.print("force");
}

void dG3DStiffnessBulk::get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &mStiff) const
{
  dG3DStiffnessBulk::get(ele, npts, GP, mStiff, true, false);
}

void dG3DStiffnessBulk::get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &mStiff, bool considerNonElasticOnes, bool removeDamage ) const
{
  if (sym)
  {
    // Initialization of some data
    int nbdof = g3DBilinearTerm<double,double>::space1.getNumKeys(ele);
    const dG3DMaterialLaw *mlawbulk;
    if(_mlaw->getType() == materialLaw::fracture)
      mlawbulk = static_cast<const dG3DMaterialLaw* >((static_cast<const FractureByCohesive3DLaw* > (_mlaw) )->getBulkLaw());
    else
      mlawbulk= static_cast<const dG3DMaterialLaw*>(_mlaw);
    bool useBarF=mlawbulk->getUseBarF();
    
    mStiff.resize(nbdof, nbdof,true); // true --> setAll(0.)
    
    nlsFunctionSpace<double>* nlspace = dynamic_cast<nlsFunctionSpace<double>*>(&space1);
    CurlMixedFunctionSpace<double>* curlspace = dynamic_cast<CurlMixedFunctionSpace<double>* >(&space1);

    // get ipvariables
    const AllIPState::ipstateElementContainer *vips = _ipf->getAips()->getIPstate(ele->getNum());
    /***
     * MECHANICAL ********************************
     * */
    static std::vector<SVector3> dBarJdu;
    static std::vector< std::vector< STensor33 > > dBarFdu; //for each GP and for each node
    if(useBarF)
    {
      int nbFF = nlspace->getNumShapeFunctions(ele,0); // same for ux, uy, uz components
      dBarJdu.resize(nbFF);
      dBarFdu.resize(npts);
      for (int k=0;k<nbFF;k++)
      {
        STensorOperation::zero(dBarJdu[k]); //check here if ok
      }
      for (int ipg = 0; ipg < npts; ipg++)
      {
        dBarFdu[ipg].resize(nbFF);
        for (int k=0;k<nbFF;k++)
        {
          STensorOperation::zero(dBarFdu[ipg][k]); //check here if ok
        }
      }
      double totalWeight=0.;
      for(int ipg = 0; ipg < npts; ipg++)
        totalWeight+=GP[ipg].weight;
      for(int ipg = 0; ipg < npts; ipg++)
      {
        const IPStateBase *ips        = (*vips)[ipg];
        const dG3DIPVariableBase *ipv     = static_cast<const dG3DIPVariableBase*>(ips->getState(IPStateBase::current));
        const std::vector<TensorialTraits<double>::GradType>& Gradsipg = ipv->gradf(&space1,ele,GP[ipg]);
        const STensor3 &barF = ipv->getConstRefToDeformationGradient(); //this is actually barF
        STensor3 invBarF(barF.invert());
        double localJ=ipv->getLocalJ();
        double barJ=ipv->getBarJ();
        double weight=GP[ipg].weight*pow(localJ,(1.-1./((double)(_dim))))*pow(barJ,1./((double)(_dim)))/totalWeight;
        for(int k=0;k<nbFF;k++)
        {
          SVector3 dBarJduTmp;
          STensorOperation::zero(dBarJduTmp);
          SVector3 Bipg;
          Bipg(0) = Gradsipg[k][0];
          Bipg(1) = Gradsipg[k][1];
          Bipg(2) = Gradsipg[k][2];
          for(int ll=0; ll<_dim; ll++)
          {
            for(int n=0; n<_dim; n++)
            {
              dBarJduTmp(ll) += invBarF(n,ll)*Bipg(n)*weight;
            }
          }
          dBarJdu[k]+=dBarJduTmp; 
        }
      }
      
      static STensor3 I;
      STensorOperation::unity(I);
      for (int ipg = 0; ipg < npts; ipg++)
      {
        const IPStateBase *ips        = (*vips)[ipg];
        const dG3DIPVariableBase *ipv     = static_cast<const dG3DIPVariableBase*>(ips->getState(IPStateBase::current));
        const std::vector<TensorialTraits<double>::GradType>& Gradsipg = ipv->gradf(&space1,ele,GP[ipg]);
        const STensor3 &barF = ipv->getConstRefToDeformationGradient(); //this is actually barF
        STensor3 invBarF(barF.invert());
        double localJ=ipv->getLocalJ();
        double barJ=ipv->getBarJ();
        for(int k=0;k<nbFF;k++)
        {
          static STensor33 dBarFduTmp;
          STensorOperation::zero(dBarFduTmp);
          for(int kk=0; kk<_dim; kk++)
          {
            for(int m=0; m<_dim; m++)
            {
              for(int jj=0; jj<_dim; jj++)
              {
                dBarFduTmp(kk,m,jj) += pow(barJ,1./((double)(_dim)))*pow(localJ,-1./((double)(_dim)))*I(kk,jj)*Gradsipg[k](m);
                for(int p=0; p<_dim; p++)
                {
                  dBarFduTmp(kk,m,jj) -= 1./((double)(_dim))*pow(barJ,1./((double)(_dim)))*pow(localJ,-1./((double)(_dim)))*
                                                        barF(kk,m)*invBarF(p,jj)*Gradsipg[k](p);             
                }
                dBarFduTmp(kk,m,jj) += 1./((double)(_dim))/barJ*barF(kk,m)*dBarJdu[k](jj);
              }                                
            }
          }
          if(_dim==2 or _dim==1)
            dBarFduTmp(2,2,2)=Gradsipg[k](3);
          if(_dim==1)
            dBarFduTmp(1,1,1)=Gradsipg[k](2);
          dBarFdu[ipg][k]=dBarFduTmp;
        }
      }
    }

    for (int i = 0; i < npts; i++)
    {
      const IPStateBase *ips        = (*vips)[i];
      const dG3DIPVariableBase *ipv     = static_cast<const dG3DIPVariableBase*>(ips->getState(IPStateBase::current));
      
      double weight = GP[i].weight;
      const double& detJ = ipv->getJacobianDeterminant(ele,GP[i]);
      double ratio = detJ*weight;
      // shape function
      const std::vector<TensorialTraits<double>::GradType> &Grads = ipv->gradf(&space1,ele,GP[i]);
      const std::vector<TensorialTraits<double>::ValType> &Vals = ipv->f(&space1,ele,GP[i]);
      
      // MECA-MECA 
      const STensor43 *H =&ipv->getConstRefToTangentModuli();
      double damageCorrection=1.;
      if (not(considerNonElasticOnes))
      {
        H = &ipv->getConstRefToElasticTangentModuli();
        if(removeDamage)
        {
          double damageFromInitialState=ipv->get(IPField::DAMAGE);
          if(damageFromInitialState>0.999)
            damageFromInitialState=0.999;
          damageCorrection=1./(1.-damageFromInitialState);
        }
      }
      //
      static SVector3 B;
      static SVector3 BT;
      for(int kk=0;kk<3;kk++)
      {
        int nbFFRow = nlspace->getNumShapeFunctions(ele,kk);
        int nbFFTotalLastRow = nlspace->getShapeFunctionsIndex(ele,kk);
        for(int ll=0; ll<3; ll++)
        {
          int nbFFCol = nlspace->getNumShapeFunctions(ele,ll);
          int nbFFTotalLastCol = nlspace->getShapeFunctionsIndex(ele,ll);
        
          for(int j=0;j<nbFFRow; j++)
          {
            BT(0) = Grads[j+nbFFTotalLastRow][0];
            BT(1) = Grads[j+nbFFTotalLastRow][1];
            BT(2) = Grads[j+nbFFTotalLastRow][2];
            for(int k=0;k<nbFFCol;k++)
            {
              // x y z
              B(0) = Grads[k+nbFFTotalLastCol][0];
              B(1) = Grads[k+nbFFTotalLastCol][1];
              B(2) = Grads[k+nbFFTotalLastCol][2];

              if(useBarF)
              {
                for(int m=0; m<3; m++)
                {
                  for(int ii=0; ii<3; ii++)
                  {
                    for(int p=0; p<3; p++)
                    {
                      mStiff(j+nbFFTotalLastRow,k+nbFFTotalLastCol) += ratio*(BT(m)*damageCorrection*H->operator()(kk,m,ii,p)*dBarFdu[i][k](ii,p,ll));                     
                    }
                  }                                
                }
                // for high order
                if(this->getAccountBodyForceForHO() and ipv->hasBodyForceForHO())
                {
                  for(int ii=0; ii<3; ii++){
                    for(int p=0; p<3; p++){
                       mStiff(j+nbFFTotalLastRow,k+nbFFTotalLastCol) -= ratio*(Vals[j+nbFFTotalLastRow]*ipv->getConstRefTodBmdFm()(kk,ii,p)*dBarFdu[i][k](ii,p,ll));   
                    }
                  }                                
                }
              }  
              else
              {
                for(int m=0; m<3; m++)
                {
                  for(int n=0; n<3; n++)
                  {
                    mStiff(j+nbFFTotalLastRow,k+nbFFTotalLastCol) += ratio*(BT(m)*damageCorrection*H->operator()(kk,m,ll,n)*B(n));
                  }
                }
                // for high order
                if(this->getAccountBodyForceForHO() and ipv->hasBodyForceForHO())
                {
                  for(int p=0; p<3; p++){
                    mStiff(j+nbFFTotalLastRow,k+nbFFTotalLastCol) -= ratio*(Vals[j+nbFFTotalLastRow]*ipv->getConstRefTodBmdFm()(kk,ll,p)*B(p));   
                  }
                }                                
              }              
            }
          }
        }
       }
      
      // MECA - NONLOCAL
      if (getNumNonLocalVariable() > 0 && considerNonElasticOnes)
      {
        if( ipv->getNumberNonLocalVariable()>getNumNonLocalVariable())
        {  
          Msg::Error("Your material law uses more non local variables than your domain dG3DStiffnessBulk::get");
        }
        
        for (int nlk = 0; nlk < ipv->getNumberNonLocalVariable(); nlk++)
        {
          const STensor3  *dsdp    = &ipv->getConstRefToDStressDNonLocalVariable()[nlk];
          int nbFFCol = nlspace->getNumShapeFunctions(ele,3+nlk);
          int nbFFTotalLastCol = nlspace->getShapeFunctionsIndex(ele,3+nlk);
          for(int kk=0;kk<3;kk++)
          {
            int nbFFRow = nlspace->getNumShapeFunctions(ele,kk);
            int nbFFTotalLastRow = nlspace->getShapeFunctionsIndex(ele,kk);
            for(int j=0;j<nbFFRow; j++)
            {
              for(int k=0;k<nbFFCol;k++)
              {
                
                for(int m=0; m<3; m++)
                {
                  mStiff(j+nbFFTotalLastRow,k+nbFFTotalLastCol) += ratio*(Grads[j+nbFFTotalLastRow](m)*dsdp->operator()(kk,m)*Vals[k+nbFFTotalLastCol]);
                }
              }
            }
          }
        }
      }
      
      // MECA - EXTRA DOF
      if (getNumConstitutiveExtraDofDiffusionVariable() > 0)
      {
        if(ipv->getNumConstitutiveExtraDofDiffusionVariable()>getNumConstitutiveExtraDofDiffusionVariable()) 
          Msg::Error("Your material law uses more constitutive extra dof variables than your domain dG3DStiffnessBulk::get");
        
        if ((getNumConstitutiveExtraDofDiffusionVariable() == 2)&&(getConstitutiveExtraDofDiffusionUseEnergyConjugatedField()))
        {
          //if two fields (T, V) are considered using fT= 1/T, fV = -V/T as unknowns
          for (int extraDOFField = 0; extraDOFField< ipv->getNumConstitutiveExtraDofDiffusionVariable(); extraDOFField++)
          {
            const STensor3  &dPdField1 = ipv->getConstRefTodPdField()[extraDOFField];
            int extraDOFField_aux= (extraDOFField == 0) ? 1: 0;
            
            const STensor3  &dPdField2 = ipv->getConstRefTodPdField()[extraDOFField_aux];
            double dFielddEnergyConjugatedField1 = ipv->getConstRefTodFielddEnergyConjugatedField()[extraDOFField][extraDOFField];
            double dFielddEnergyConjugatedField2 = ipv->getConstRefTodFielddEnergyConjugatedField()[extraDOFField_aux][extraDOFField];
            //
            int indexField=3+getNumNonLocalVariable()+extraDOFField;
            int nbFFCol = nlspace->getNumShapeFunctions(ele,indexField);
            int nbFFTotalLastCol = nlspace->getShapeFunctionsIndex(ele,indexField);
            
            for(int kk=0;kk<3;kk++)
            {
              int nbFFRow = nlspace->getNumShapeFunctions(ele,kk);
              int nbFFTotalLastRow = nlspace->getShapeFunctionsIndex(ele,kk);    
              for(int j=0;j<nbFFRow; j++)
              {
                for(int k=0;k<nbFFCol;k++)
                {
                  for(int m=0; m<3; m++)
                  {
                    mStiff(j+nbFFTotalLastRow,k+nbFFTotalLastCol) += ratio*(Grads[j+nbFFTotalLastRow][m]*(dPdField1.operator()(kk,m)*dFielddEnergyConjugatedField1+
                                                 dPdField2.operator()(kk,m)*dFielddEnergyConjugatedField2)*Vals[k+nbFFTotalLastCol]);
                  }
                }
              }
            }                                  
          }
        }
        else
        {
          // in case of direct field
          for (int extraDOFField = 0; extraDOFField< ipv->getNumConstitutiveExtraDofDiffusionVariable(); extraDOFField++)
          {
            const STensor3  &dPdField = ipv->getConstRefTodPdField()[extraDOFField];
            int indexField=3+getNumNonLocalVariable()+extraDOFField;
            int nbFFCol = nlspace->getNumShapeFunctions(ele,indexField);
            int nbFFTotalLastCol = nlspace->getShapeFunctionsIndex(ele,indexField);
            
            for(int kk=0;kk<3;kk++)
            {
              int nbFFRow = nlspace->getNumShapeFunctions(ele,kk);
              int nbFFTotalLastRow = nlspace->getShapeFunctionsIndex(ele,kk);  
              
              for(int j=0;j<nbFFRow; j++)
              {
                for(int k=0;k<nbFFCol;k++)
                {
                  for(int m=0; m<3; m++)
                  {
                    mStiff(j+nbFFTotalLastRow,k+nbFFTotalLastCol) += ratio*(Grads[j+nbFFTotalLastRow][m]*dPdField.operator()(kk,m)*Vals[k+nbFFTotalLastCol]);
                  }
                }
              }
            }
          }
        }
      }
      
      // MECA-CURL DATA
      if (getNumConstitutiveCurlVariable() > 0)
      {
        if( ipv->getNumConstitutiveCurlVariable() > getNumConstitutiveCurlVariable() ) 
          Msg::Error("Your material law uses more constitutive curl data variables than your domain dG3DStiffnessBulk::get");
          
        // Basis functions for vector field at edges
        const std::vector<SVector3> &curlVals = ipv->fcurl(&space1,ele,GP[i],"HcurlLegendre");
        // Basis functions for Curl of vector field at edges
        const std::vector<SVector3> &CurlcurlVals = ipv->Curlfcurl(&space1,ele,GP[i],"CurlHcurlLegendre");
        
        for (unsigned int curlField = 0; curlField < ipv->getNumConstitutiveCurlVariable(); ++curlField)
        {
          const unsigned int indexCurl = 3+getNumNonLocalVariable()+getNumConstitutiveExtraDofDiffusionVariable()+curlField;
          const unsigned int numEdgeShapeFuncCol = nlspace->getNumShapeFunctions(ele,indexCurl);
          const unsigned int numEdgeShapeFuncTotalLastCol = nlspace->getShapeFunctionsIndex(ele,indexCurl);
          const unsigned int numStandardDof = curlspace->getNumKeysByType(CurlMixedFunctionSpace<double>::DOF_STANDARD,ele);

          // dPdA
          const STensor33 & dPdVectorPotential = ipv->getConstRefTodPdVectorPotential()[curlField];
          // dPdB
          const STensor33 & dPdVectorCurl = ipv->getConstRefTodPdVectorCurl()[curlField];
          
          for (unsigned int kk = 0; kk < 3; ++kk)
          {
            const unsigned int nbFFRow = nlspace->getNumShapeFunctions(ele,kk);
            const unsigned int nbFFTotalLastRow = nlspace->getShapeFunctionsIndex(ele,kk);
            for (unsigned int j = 0; j < nbFFRow; ++j)
            {
              for (unsigned int k = 0; k < numEdgeShapeFuncCol; ++k)
              {             
                  for (unsigned int m = 0; m < 3; ++m)
                  {
                      for (unsigned int n = 0; n < 3; ++n)
                      {
                          mStiff(j+nbFFTotalLastRow,k+numEdgeShapeFuncTotalLastCol+numStandardDof) += ratio *
                                  Grads[j+nbFFTotalLastRow][m]*
                                  dPdVectorPotential.operator()(kk,m,n)*
                                  curlVals[k+numEdgeShapeFuncTotalLastCol][n];
                          mStiff(j+nbFFTotalLastRow,k+numEdgeShapeFuncTotalLastCol+numStandardDof) += ratio *
                                  Grads[j+nbFFTotalLastRow][m]*
                                  dPdVectorCurl.operator()(kk,m,n)*
                                  CurlcurlVals[k+numEdgeShapeFuncTotalLastCol][n];
                      }
                  }
              }
            }
          }          
        }
      }
    }
    
    //
    /***
     * NONLOCAL 
     * */
    //    
    if (getNumNonLocalVariable() > 0)
    {
      for (int i = 0; i < npts; i++)
      {
        const IPStateBase *ips        = (*vips)[i];
        const dG3DIPVariableBase *ipv     = static_cast<const dG3DIPVariableBase*>(ips->getState(IPStateBase::current));
        if( ipv->getNumberNonLocalVariable()>getNumNonLocalVariable())
        {  
          Msg::Error("Your material law uses more non local variables than your domain dG3DStiffnessBulk::get");
        }
        
        double weight = GP[i].weight;
        const double& detJ = ipv->getJacobianDeterminant(ele,GP[i]);
        double ratio = detJ*weight;
        // shape function
        const std::vector<TensorialTraits<double>::GradType> &Grads = ipv->gradf(&space1,ele,GP[i]);
        const std::vector<TensorialTraits<double>::ValType> &Vals = ipv->f(&space1,ele,GP[i]);
        //
        for (int nlk = 0; nlk < ipv->getNumberNonLocalVariable(); nlk++)
        {
          const STensor3    *cg    = &ipv->getConstRefToCharacteristicLengthMatrix(nlk); 
          int nbFFRow = nlspace->getNumShapeFunctions(ele,3+nlk);
          int nbFFTotalLastRow = nlspace->getShapeFunctionsIndex(ele,3+nlk);
          
          // nonlocal-nonlocal, no couling hear
          for(int j=0;j<nbFFRow; j++)
          {
            for(int k=0;k<nbFFRow;k++)
            {
              // principal term
              double BtB = 0.;
              for(int m=0; m<3; m++)
              {
                for(int n=0; n<3; n++)
                {
                  BtB += Grads[j+nbFFTotalLastRow](m)*(cg->operator()(m,n))*Grads[k+nbFFTotalLastRow](n);
                }
              }
              mStiff(j+nbFFTotalLastRow,k+nbFFTotalLastRow)  += getNonLocalEqRatio()*ratio*(Vals[j+nbFFTotalLastRow]*Vals[k+nbFFTotalLastRow]+ BtB);
            }
          }
          // cross nonlocal terms due to local variable
          for (int mlk = 0; mlk < ipv->getNumberNonLocalVariable(); mlk++)
          {
            int nbFFCol = nlspace->getNumShapeFunctions(ele,3+mlk);
            int nbFFTotalLastCol = nlspace->getShapeFunctionsIndex(ele,3+mlk);
          
            double  SpBar = ipv->getConstRefToDLocalVariableDNonLocalVariable()(nlk,mlk);
            for(int j=0;j<nbFFRow; j++)
            {
              for(int k=0;k<nbFFCol;k++)
              {
                mStiff(j+nbFFTotalLastRow,k+nbFFTotalLastCol) -= getNonLocalEqRatio()*ratio*(Vals[j+nbFFTotalLastRow]*SpBar*Vals[k+nbFFTotalLastCol]);
              }
            }
          }
          
          // nonlocal - displacement field
          if(considerNonElasticOnes)
          {
            const STensor3  *dpds    = &ipv->getConstRefToDLocalVariableDStrain()[nlk];
            for(int kk=0;kk<3;kk++)
            {
              int nbFFCol = nlspace->getNumShapeFunctions(ele,kk);
              int nbFFTotalLastCol = nlspace->getShapeFunctionsIndex(ele,kk);   
              for(int j=0;j<nbFFRow; j++)
              {
                for(int k=0;k<nbFFCol;k++)
                {              
                  
                  if(useBarF)
                  {
                    for(int ii=0; ii<3; ii++)
                    {
                      for(int p=0; p<3; p++)
                      {
                        mStiff(j+nbFFTotalLastRow,k+nbFFTotalLastCol) -= getNonLocalEqRatio()*ratio*(Vals[j+nbFFTotalLastRow]*
                                               dpds->operator()(ii,p)*dBarFdu[i][k](ii,p,kk));
                      }                                
                    }
                  }
                  else
                  {
                    for(int m=0; m<3; m++)
                    {
                      mStiff(j+nbFFTotalLastRow,k+nbFFTotalLastCol) -= getNonLocalEqRatio()*ratio*(Vals[j+nbFFTotalLastRow]*
                                               dpds->operator()(kk,m)*Grads[k+nbFFTotalLastCol](m));
                    }
                  }
                }
              }
            }
          }
          
          // nonlocal extraDof
          if (getNumConstitutiveExtraDofDiffusionVariable() > 0)
          {
            if(ipv->getNumConstitutiveExtraDofDiffusionVariable()>getNumConstitutiveExtraDofDiffusionVariable()) 
            {
              Msg::Error("Your material law uses more constitutive extra dof variables than your domain dG3DStiffnessBulk::get");
            }
            
            if ((getNumConstitutiveExtraDofDiffusionVariable() == 2)&&(getConstitutiveExtraDofDiffusionUseEnergyConjugatedField()))
            {
              //if two fields (T, V) are considered using fT= 1/T, fV = -V/T as unknowns
              
              for (int extraDOFField = 0; extraDOFField < ipv->getNumConstitutiveExtraDofDiffusionVariable(); extraDOFField++)
              {
                //Msg::Error("Check non-local / extra dof coupling in dG3DStiffnessBulk::get");
                              
                int extraDOFField_aux= (extraDOFField == 0) ? 1: 0;
   
                const STensor3  &dPdField2 = ipv->getConstRefTodPdField()[extraDOFField_aux];
                double dFielddEnergyConjugatedField1 = ipv->getConstRefTodFielddEnergyConjugatedField()[extraDOFField][extraDOFField];
                double dFielddEnergyConjugatedField2 = ipv->getConstRefTodFielddEnergyConjugatedField()[extraDOFField_aux][extraDOFField];
                
                double  dLocalVariableDExtraDof1 = ipv->getConstRefTodLocalVariableDExtraDofDiffusionField()(nlk,extraDOFField);
                double  dLocalVariableDExtraDof2 = ipv->getConstRefTodLocalVariableDExtraDofDiffusionField()(nlk,extraDOFField_aux);
                
                int indexField=3+getNumNonLocalVariable()+extraDOFField;
                int nbFFCol = nlspace->getNumShapeFunctions(ele,indexField);
                int nbFFTotalLastCol = nlspace->getShapeFunctionsIndex(ele,indexField); 
              
                for(int j=0;j<nbFFRow; j++)
                {
                  for(int k=0;k<nbFFCol;k++)
                  {
                    
                    mStiff(j+nbFFTotalLastRow,k+nbFFTotalLastCol)
                          += getNonLocalEqRatio()*ratio*Vals[j+nbFFTotalLastRow]*Vals[k+nbFFTotalLastCol]*(dLocalVariableDExtraDof1*dFielddEnergyConjugatedField1+dLocalVariableDExtraDof2*dLocalVariableDExtraDof2);
                  }
                }
              }
            }
            else
            {
              for (int extraDOFField = 0; extraDOFField < ipv->getNumConstitutiveExtraDofDiffusionVariable(); extraDOFField++)
              {
                //Msg::Error("Check non-local / extra dof coupling in dG3DStiffnessBulk::get");
                double  dLocalVariableDExtraDof = ipv->getConstRefTodLocalVariableDExtraDofDiffusionField()(nlk,extraDOFField);
                int indexField=3+getNumNonLocalVariable()+extraDOFField;
                int nbFFCol = nlspace->getNumShapeFunctions(ele,indexField);
                int nbFFTotalLastCol = nlspace->getShapeFunctionsIndex(ele,indexField); 
                
                for(int j=0;j<nbFFRow; j++)
                {
                  for(int k=0;k<nbFFCol;k++)
                  {
                    // principal term
                    mStiff(j+nbFFTotalLastRow,k+nbFFTotalLastCol)
                          += getNonLocalEqRatio()*ratio*(Vals[j+nbFFTotalLastRow]*Vals[k+nbFFTotalLastCol]*dLocalVariableDExtraDof);
                  }
                }
              }
            }
          }
          
          // NON-LOCAL - CURL FIELD
          if (getNumConstitutiveCurlVariable() > 0)
          {
            if( ipv->getNumConstitutiveCurlVariable() > getNumConstitutiveCurlVariable() ) 
              Msg::Error("Your material law uses more constitutive curl data variables than your domain dG3DStiffnessBulk::get");
              
            // Basis functions for vector field at edges
            const std::vector<SVector3> &curlVals = ipv->fcurl(&space1,ele,GP[i],"HcurlLegendre");
            // Basis functions for Curl of vector field at edges
            const std::vector<SVector3> &CurlcurlVals = ipv->Curlfcurl(&space1,ele,GP[i],"CurlHcurlLegendre");
            
            for (unsigned int curlField = 0; curlField < ipv->getNumConstitutiveCurlVariable(); ++curlField)
            {
              const unsigned int IndexCurl = 3+getNumNonLocalVariable()+getNumConstitutiveExtraDofDiffusionVariable()+curlField;
              const unsigned int numEdgeShapeFuncCol = nlspace->getNumShapeFunctions(ele,IndexCurl);
              const unsigned int numEdgeShapeFuncTotalLastCol = nlspace->getShapeFunctionsIndex(ele,IndexCurl);
              const unsigned int numStandardDof = curlspace->getNumKeysByType(CurlMixedFunctionSpace<double>::DOF_STANDARD,ele);

              const SVector3 & dLocalVariabledVectorPotential = ipv->getConstRefTodLocalVariabledVectorPotential()[nlk][curlField];
              const SVector3 & dLocalVariabledVectorCurl = ipv->getConstRefTodLocalVariabledVectorCurl()[nlk][curlField];
              
              for (unsigned int j = 0; j < nbFFRow; ++j)
              {
                for (unsigned int k = 0; k < numEdgeShapeFuncCol; ++k)
                {
                    mStiff(j+nbFFTotalLastRow,k+numEdgeShapeFuncTotalLastCol+numStandardDof) += ratio * 0.0;
                }
              }
            }
          }
        }
      }
    }
    
    //
    /***
     * EXTRADOF 
     * */
    //    
    
    if (getNumConstitutiveExtraDofDiffusionVariable() > 0)
    {
      for (int i = 0; i < npts; i++)
      {
        const IPStateBase *ips        = (*vips)[i];
        const dG3DIPVariableBase *ipv     = static_cast<const dG3DIPVariableBase*>(ips->getState(IPStateBase::current));
        if(ipv->getNumConstitutiveExtraDofDiffusionVariable()>getNumConstitutiveExtraDofDiffusionVariable()) 
        {
          Msg::Error("Your material law uses more constitutive extra dof variables than your domain dG3DStiffnessBulk::get");
        }
        double weight = GP[i].weight;
        const double& detJ = ipv->getJacobianDeterminant(ele,GP[i]);
        double ratio = detJ*weight;
        // shape function
        const std::vector<TensorialTraits<double>::GradType> &Grads = ipv->gradf(&space1,ele,GP[i]);
        const std::vector<TensorialTraits<double>::ValType> &Vals = ipv->f(&space1,ele,GP[i]);
        
        // extraDof - extraDof
        if ((getNumConstitutiveExtraDofDiffusionVariable() == 2)&&(getConstitutiveExtraDofDiffusionUseEnergyConjugatedField()))
        {
          //if two fields (T, V) are considered using fT= 1/T, fV = -V/T as unknowns
          for (int extraDOFField = 0; extraDOFField< ipv->getNumConstitutiveExtraDofDiffusionVariable(); extraDOFField++)
          {
            int indexField=3+getNumNonLocalVariable()+extraDOFField;
            int nbFFRow = nlspace->getNumShapeFunctions(ele,indexField);
            int nbFFTotalLastRow = nlspace->getShapeFunctionsIndex(ele,indexField);
            
            for (int extraDOFField2 = 0; extraDOFField2< ipv->getNumConstitutiveExtraDofDiffusionVariable(); extraDOFField2++)
            {
              int indexField2=3+getNumNonLocalVariable()+extraDOFField2;
              int nbFFCol = nlspace->getNumShapeFunctions(ele,indexField2);
              int nbFFTotalLastCol = nlspace->getShapeFunctionsIndex(ele,indexField2);
              
              //int extraDOFField2=extraDOFField;
              const STensor3  &dFluxdGradField1   = ipv->getConstRefTodFluxdGradField()[extraDOFField][extraDOFField2]; //in reference conf
              const SVector3  &dFluxdField1  = ipv->getConstRefTodFluxdField()[extraDOFField][extraDOFField2]; //in reference conf

              int extraDOFField_aux= (extraDOFField2==0) ? 1: 0;

              const STensor3  &dFluxdGradField2 = ipv->getConstRefTodFluxdGradField()[extraDOFField][extraDOFField_aux]; //in reference conf
              const SVector3  &dFluxdField2     = ipv->getConstRefTodFluxdField()[extraDOFField][extraDOFField_aux]; //in reference conf

              const STensor3  &dGradFielddGradEnergyConjugatedField1 = ipv->getConstRefTodGradFielddGradEnergyConjugatedField()[extraDOFField2][extraDOFField2];
              const SVector3  &dGradFielddEnergyConjugatedField1 = ipv->getConstRefTodGradFielddEnergyConjugatedField()[extraDOFField2][extraDOFField2];
              double          dFielddEnergyConjugatedField1 = ipv->getConstRefTodFielddEnergyConjugatedField()[extraDOFField2][extraDOFField2];
          
              const STensor3  &dGradFielddGradEnergyConjugatedField2 = ipv->getConstRefTodGradFielddGradEnergyConjugatedField()[extraDOFField_aux][extraDOFField2];
              const SVector3  &dGradFielddEnergyConjugatedField2 = ipv->getConstRefTodGradFielddEnergyConjugatedField()[extraDOFField_aux][extraDOFField2];
              double          dFielddEnergyConjugatedField2 = ipv->getConstRefTodFielddEnergyConjugatedField()[extraDOFField_aux][extraDOFField2];

              double dwdt = 0.;
              if (getConstitutiveExtraDofDiffusionAccountFieldSource())
              {
                //if(extraDOFField==0 && extraDOFField2==0){
                  dwdt += ipv->getConstRefTodFieldSourcedField()(extraDOFField,extraDOFField2);
                //}
              }
              if (getConstitutiveExtraDofDiffusionAccountMecaSource())
              {
                //if(extraDOFField==0 && extraDOFField2==0){
                  dwdt += ipv->getConstRefTodMechanicalSourcedField()(extraDOFField,extraDOFField2);
                //}
              }
              for(int j=0;j<nbFFRow; j++)
              {
                for(int k=0;k<nbFFCol;k++)
                {
                  // Pure & Cross Terms
                  for(int m=0; m<3; m++)
                  {
                    mStiff(j+nbFFTotalLastRow,k+nbFFTotalLastCol)+=getConstitutiveExtraDofDiffusionEqRatio()*ratio*
                                  (Grads[j+nbFFTotalLastRow][m]*(dFluxdField1.operator()(m)*dFielddEnergyConjugatedField1+
                                   dFluxdField2.operator()(m)*dFielddEnergyConjugatedField2)*Vals[k+nbFFTotalLastCol]);

                    for(int n=0; n<3; n++)
                    {
                      mStiff(j+nbFFTotalLastRow,k+nbFFTotalLastCol)+=getConstitutiveExtraDofDiffusionEqRatio()*ratio*
                                 (Grads[j+nbFFTotalLastRow][m]*(dFluxdGradField1.operator()(m,n)*dGradFielddEnergyConjugatedField1.operator()(n)+
                                 dFluxdGradField2.operator()(m,n)*dGradFielddEnergyConjugatedField2.operator()(n))*Vals[k+nbFFTotalLastCol]);
                 
                      for(int p=0; p<3; p++)
                      {
                        mStiff(j+nbFFTotalLastRow,k+nbFFTotalLastCol)+=getConstitutiveExtraDofDiffusionEqRatio()*ratio*
                                 (Grads[j+nbFFTotalLastRow][m]*(dFluxdGradField1.operator()(m,p)*dGradFielddGradEnergyConjugatedField1.operator()(p,n)+
                                 dFluxdGradField2.operator()(m,p)*dGradFielddGradEnergyConjugatedField2.operator()(p,n))*Grads[k+nbFFTotalLastCol][n]);
                      }
                    }
                  }
                  //if(extraDOFField==0 && extraDOFField2==0) 
                  mStiff(j+nbFFTotalLastRow,k+nbFFTotalLastCol)+= getConstitutiveExtraDofDiffusionEqRatio()*ratio*dwdt*dFielddEnergyConjugatedField1*
                                Vals[j+nbFFTotalLastRow]*Vals[k+nbFFTotalLastCol];// related to cp
                }
              }
            }
          }
        }
        else
        {
          for (int extraDOFField = 0; extraDOFField< ipv->getNumConstitutiveExtraDofDiffusionVariable(); extraDOFField++)
          {
            int indexField=3+getNumNonLocalVariable()+extraDOFField;
            int nbFFRow = nlspace->getNumShapeFunctions(ele,indexField);
            int nbFFTotalLastRow = nlspace->getShapeFunctionsIndex(ele,indexField);
            
            for (int extraDOFField2 = 0; extraDOFField2< ipv->getNumConstitutiveExtraDofDiffusionVariable(); extraDOFField2++)
            {
              int indexField2=3+getNumNonLocalVariable()+extraDOFField2;
              int nbFFCol = nlspace->getNumShapeFunctions(ele,indexField2);
              int nbFFTotalLastCol = nlspace->getShapeFunctionsIndex(ele,indexField2);
              
              const STensor3  &dFluxdGradField = ipv->getConstRefTodFluxdGradField()[extraDOFField][extraDOFField2]; //in reference conf
              const SVector3  &dFluxdField     = ipv->getConstRefTodFluxdField()[extraDOFField][extraDOFField2]; //in reference conf

              double dwdField = 0.;
              //double dwdV = 0.0;
              if (getConstitutiveExtraDofDiffusionAccountFieldSource())
              {
                //if(extraDOFField==0 && extraDOFField2==0)
                {
                  dwdField += ipv->getConstRefTodFieldSourcedField()(extraDOFField,extraDOFField2);
                }
                //if (extraDOFField == 0 && extraDOFField2 == 1)
                //{
                //    dwdV += ipv->getConstRefTodFieldSourcedField()(extraDOFField,extraDOFField2);
                //}
              }
              if (getConstitutiveExtraDofDiffusionAccountMecaSource())
              {
                //if(extraDOFField==0 && extraDOFField2==0)
                //{
                  dwdField += ipv->getConstRefTodMechanicalSourcedField()(extraDOFField,extraDOFField2);
                //}
                //if (extraDOFField == 0 && extraDOFField2 == 1)
                //{
                //  dwdV += ipv->getConstRefTodMechanicalSourcedField()(extraDOFField,extraDOFField2);
                //}
              }
              SVector3 dw_AVdGradField(0.0);
              //SVector3 dw_AVdGradV(0.0);
              double dw_AVdField = 0.0;
              if (getConstitutiveCurlAccountFieldSource())
              {
                  //if (extraDOFField == 0 && extraDOFField2 == 0)
                  //{
                      dw_AVdField += ipv->getConstRefTodEMFieldSourcedField()(extraDOFField,extraDOFField2);
                      dw_AVdGradField += ipv->getConstRefTodEMFieldSourcedGradField()[extraDOFField][extraDOFField2];
                  //}
                  //if (extraDOFField == 0 && extraDOFField2 == 1)
                  //{
                  //    dw_AVdGradV += ipv->getConstRefTodEMFieldSourcedGradField()[extraDOFField][extraDOFField2];
                  //}
              }
              for(int j=0;j<nbFFRow; j++)
              {
                for(int k=0;k<nbFFCol;k++)
                {
                  // Pure & Cross Terms
                  for(int m=0; m<3; m++)
                  {
                    mStiff(j+nbFFTotalLastRow,k+nbFFTotalLastCol)+=getConstitutiveExtraDofDiffusionEqRatio()*ratio*(Grads[j+nbFFTotalLastRow][m]*dFluxdField.operator()(m)*Vals[k+nbFFTotalLastCol]);
                    for(int n=0; n<3; n++)
                    {
                      mStiff(j+nbFFTotalLastRow,k+nbFFTotalLastCol)+=getConstitutiveExtraDofDiffusionEqRatio()*ratio*(Grads[j+nbFFTotalLastRow][m]*dFluxdGradField.operator()(m,n)*Grads[k+nbFFTotalLastCol][n]);
                    }
                  }
               
                  mStiff(j+nbFFTotalLastRow,k+nbFFTotalLastCol)+=getConstitutiveExtraDofDiffusionEqRatio()*ratio*dwdField*Vals[j+nbFFTotalLastRow]*Vals[k+nbFFTotalLastCol];
                  mStiff(j+nbFFTotalLastRow,k+nbFFTotalLastCol) += getConstitutiveExtraDofDiffusionEqRatio()*ratio*dw_AVdField*Vals[j+nbFFTotalLastRow]*Vals[k+nbFFTotalLastCol];
                  for (unsigned int m = 0; m < 3; ++m)
                  {
                     mStiff(j+nbFFTotalLastRow,k+nbFFTotalLastCol) += getConstitutiveExtraDofDiffusionEqRatio()*ratio*Vals[j+nbFFTotalLastRow]*
                                                                           dw_AVdGradField.operator()(m)*Grads[k+nbFFTotalLastCol][m];
                  }
                  /*
                  if(extraDOFField==0 && extraDOFField2==0)
                  {
                      // related to cp
                      mStiff(j+nbFFTotalLastRow,k+nbFFTotalLastCol)+=getConstitutiveExtraDofDiffusionEqRatio()*ratio*dwdt*Vals[j+nbFFTotalLastRow]*Vals[k+nbFFTotalLastCol];
                      mStiff(j+nbFFTotalLastRow,k+nbFFTotalLastCol) += getConstitutiveExtraDofDiffusionEqRatio()*ratio*dw_AVdT*Vals[j+nbFFTotalLastRow]*Vals[k+nbFFTotalLastCol];

                      for (unsigned int m = 0; m < 3; ++m)
                      {
                          mStiff(j+nbFFTotalLastRow,k+nbFFTotalLastCol) += getConstitutiveExtraDofDiffusionEqRatio()*ratio*Vals[j+nbFFTotalLastRow]*
                                                                           dw_AVdGradT.operator()(m)*Grads[k+nbFFTotalLastCol][m];
                      }
                  }
                  if (extraDOFField == 0 && extraDOFField2 == 1)
                  {
                      mStiff(j+nbFFTotalLastRow,k+nbFFTotalLastCol)+=getConstitutiveExtraDofDiffusionEqRatio()*ratio*dwdV
                                                                        *Vals[j+nbFFTotalLastRow]*Vals[k+nbFFTotalLastCol];

                      for (unsigned int m = 0; m < 3; ++m)
                      {
                          mStiff(j+nbFFTotalLastRow,k+nbFFTotalLastCol) += getConstitutiveExtraDofDiffusionEqRatio()*ratio*Vals[j+nbFFTotalLastRow]*
                                                                           dw_AVdGradV.operator()(m)*Grads[k+nbFFTotalLastCol][m];
                      }
                  }*/
                }
              }
            }
          }
          
        }
        
        // extraDof-mecanics
        for (int extraDOFField = 0; extraDOFField< ipv->getNumConstitutiveExtraDofDiffusionVariable(); extraDOFField++)
        {
          int indexField=3+getNumNonLocalVariable()+extraDOFField;
          int nbFFRow = nlspace->getNumShapeFunctions(ele,indexField);
          int nbFFTotalLastRow = nlspace->getShapeFunctionsIndex(ele,indexField);
          //
          const STensor33 &dFluxdF = ipv->getConstRefTodFluxdF()[extraDOFField]; //in reference conf
          static STensor3 dwdf;
          STensorOperation::zero(dwdf);

          if (getConstitutiveExtraDofDiffusionAccountFieldSource())
          {
            //if(extraDOFField==0)
            //{
              dwdf += ipv->getConstRefTodFieldSourcedF()[extraDOFField];
            //}
          }
          if (getConstitutiveExtraDofDiffusionAccountMecaSource())
          {
            //if(extraDOFField==0)
            //{
              dwdf += ipv->getConstRefTodMechanicalSourcedF()[extraDOFField];
            //}
          }
          if (getConstitutiveCurlAccountFieldSource())
          {
              //if (extraDOFField==0)
                  dwdf += ipv->getConstRefTodEMFieldSourcedF()[extraDOFField]; // related to EM heat source from eddy + hysteresis loss
          }
          
          for(int kk=0;kk<3;kk++)
          {  
            int nbFFCol = nlspace->getNumShapeFunctions(ele,kk);
            int nbFFTotalLastCol = nlspace->getShapeFunctionsIndex(ele,kk);
              
            for(int j=0;j<nbFFRow; j++)
            {
              for(int k=0;k<nbFFCol;k++)
              {
                if(useBarF)
                {
                  for(int m=0; m<3; m++)
                  {
                    for(int ii=0; ii<3; ii++)
                    {
                      for(int p=0; p<3; p++)
                      {
                        mStiff(j+nbFFTotalLastRow,k+nbFFTotalLastCol) += getConstitutiveExtraDofDiffusionEqRatio()*ratio*
                             (Grads[j+nbFFTotalLastRow][m]*dFluxdF.operator()(m,ii,p)*dBarFdu[i][k](ii,p,kk));
                      }
                      if(extraDOFField==0) 
                        mStiff(j+nbFFTotalLastRow,k+nbFFTotalLastCol)+=getConstitutiveExtraDofDiffusionEqRatio()*ratio*
                                              (Vals[j+nbFFTotalLastRow]*dwdf.operator()(ii,m)*dBarFdu[i][k](ii,m,kk)); // related to cp
                    }
                  }
                }
                else
                {
                  for(int m=0; m<3; m++)
                  {
                    for(int n=0; n<3; n++)
                    {
                      mStiff(j+nbFFTotalLastRow,k+nbFFTotalLastCol) += getConstitutiveExtraDofDiffusionEqRatio()*ratio*
                                       (Grads[j+nbFFTotalLastRow][m]*dFluxdF.operator()(m,kk,n)*Grads[k+nbFFTotalLastCol][n]);
                    }
                    if(extraDOFField==0) 
                       mStiff(j+nbFFTotalLastRow,k+nbFFTotalLastCol)+=getConstitutiveExtraDofDiffusionEqRatio()*ratio*
                                              (Vals[j+nbFFTotalLastRow]*dwdf.operator()(kk,m)*Grads[k+nbFFTotalLastCol][m]); // related to cp
                  }
                }
              }
            }
          }
        }
        
        // extraDof-nonlocal
        if (getNumNonLocalVariable() > 0)
        {
          for (int extraDOFField = 0; extraDOFField< ipv->getNumConstitutiveExtraDofDiffusionVariable(); extraDOFField++)
          {
            int indexField=3+getNumNonLocalVariable()+extraDOFField;
            int nbFFRow = nlspace->getNumShapeFunctions(ele,indexField);
            int nbFFTotalLastRow = nlspace->getShapeFunctionsIndex(ele,indexField);
            
            for (int nlk = 0; nlk < ipv->getNumberNonLocalVariable(); nlk++)
            {
              int nbFFCol = nlspace->getNumShapeFunctions(ele,3+nlk);
              int nbFFTotalLastCol = nlspace->getShapeFunctionsIndex(ele,3+nlk);
            
              //Msg::Error("Check extra dof / non-local coupling in dG3DStiffnessBulk::get");
              const SVector3 &dFluxdNonLocalVariable = ipv->getConstRefTodFluxdNonLocalVariable()[extraDOFField][nlk];
              double dwdNonLocalVariable   = 0.;  
              if (getConstitutiveExtraDofDiffusionAccountFieldSource()){
                dwdNonLocalVariable += ipv->getConstRefTodFieldSourcedNonLocalVariable()(extraDOFField,nlk);
              }
              if (getConstitutiveExtraDofDiffusionAccountMecaSource()){
                dwdNonLocalVariable += ipv->getConstRefTodMechanicalSourcedNonLocalVariable()(extraDOFField,nlk);
              }
              if (getConstitutiveCurlAccountFieldSource())
                  dwdNonLocalVariable += ipv->getConstRefTodEMFieldSourcedNonLocalVariable()(extraDOFField,nlk); // related to EM heat source from eddy + hysteresis loss
              if(considerNonElasticOnes)
              {
                for(int j=0;j<nbFFRow; j++)
                {
                  for(int k=0;k<nbFFCol;k++)
                  {
                    for(int m=0; m<3; m++)
                    {
                      mStiff(j+nbFFTotalLastRow,k+nbFFTotalLastCol)+=getConstitutiveExtraDofDiffusionEqRatio()*ratio*
                                          (Grads[j+nbFFTotalLastRow][m]*dFluxdNonLocalVariable.operator()(m)*Vals[k+nbFFTotalLastCol]);
                    }
                    mStiff(j+nbFFTotalLastRow,k+nbFFTotalLastCol)+=getConstitutiveExtraDofDiffusionEqRatio()*ratio*
                                                  dwdNonLocalVariable*Vals[j+nbFFTotalLastRow]*Vals[k+nbFFTotalLastCol];
                  }
                }
              }
            }
          }
        }
        
        // EXTRA-DOF - CURL FIELD
        if (getNumConstitutiveCurlVariable() > 0)
        {
          if( ipv->getNumConstitutiveCurlVariable() > getNumConstitutiveCurlVariable() ) 
          Msg::Error("Your material law uses more constitutive curl data variables than your domain dG3DStiffnessBulk::get");

          // Basis functions for vector field at edges
          const std::vector<SVector3> &curlVals = ipv->fcurl(&space1,ele,GP[i],"HcurlLegendre");
          // Basis functions for Curl of vector field at edges
          const std::vector<SVector3> &CurlcurlVals = ipv->Curlfcurl(&space1,ele,GP[i],"CurlHcurlLegendre");
          
          for (unsigned int extraDOFField = 0; extraDOFField < ipv->getNumConstitutiveExtraDofDiffusionVariable(); ++extraDOFField)
          {
            const unsigned int IndexField = 3+getNumNonLocalVariable()+extraDOFField;
            const unsigned int nbFFRow = nlspace->getNumShapeFunctions(ele,IndexField);
            const unsigned int nbFFTotalLastRow = nlspace->getShapeFunctionsIndex(ele,IndexField);
            
            for (unsigned int curlField = 0; curlField < ipv->getNumConstitutiveCurlVariable(); ++curlField)
            {
              const unsigned int IndexCurl = 3+getNumNonLocalVariable()+getNumConstitutiveExtraDofDiffusionVariable()+curlField;
              const unsigned int numEdgeShapeFuncCol = nlspace->getNumShapeFunctions(ele,IndexCurl);
              const unsigned int numEdgeShapeFuncTotalLastCol = nlspace->getShapeFunctionsIndex(ele,IndexCurl);
              const unsigned int numStandardDof = curlspace->getNumKeysByType(CurlMixedFunctionSpace<double>::DOF_STANDARD,ele);

              const STensor3 & dFluxdVectorPotential = ipv->getConstRefTodFluxdVectorPotential()[extraDOFField][curlField];
              const STensor3 & dFluxdVectorCurl = ipv->getConstRefTodFluxdVectorCurl()[extraDOFField][curlField];

              SVector3 dwTdVectorPotential(0.0);
              SVector3 dw_AVdVectorPotential(0.0);
              SVector3 dw_AVdVectorCurl(0.0);

              //if (extraDOFField == 0)
              {
                  if (getConstitutiveExtraDofDiffusionAccountFieldSource())
                  {
                      dwTdVectorPotential += ipv->getConstRefTodFieldSourcedVectorPotential()[extraDOFField][curlField];
                  }

                  if (getConstitutiveExtraDofDiffusionAccountMecaSource())
                  {
                      dwTdVectorPotential += ipv->getConstRefTodMechanicalSourcedVectorPotential()[extraDOFField][curlField];
                  }

                  if (getConstitutiveCurlAccountFieldSource())
                  {
                      dw_AVdVectorPotential += ipv->getConstRefTodEMFieldSourcedVectorPotential()[extraDOFField][curlField];
                      dw_AVdVectorCurl += ipv->getConstRefTodEMFieldSourcedVectorCurl()[extraDOFField][curlField];
                  }
              }

              for (unsigned int j = 0; j < nbFFRow; ++j)
                  for (unsigned int k = 0; k < numEdgeShapeFuncCol; ++k)
                  {
                      for (unsigned int m = 0; m < 3; ++m)
                      {
                          for (unsigned int n = 0; n < 3; ++n)
                          {
                              mStiff(j+nbFFTotalLastRow,
                                     k+numEdgeShapeFuncTotalLastCol+numStandardDof) += getConstitutiveExtraDofDiffusionEqRatio() * ratio *
                                                                                       (Grads[j+nbFFTotalLastRow][m] * dFluxdVectorPotential.operator()(m,n) *
                                                                                        curlVals[k+numEdgeShapeFuncTotalLastCol][n]);
                              mStiff(j+nbFFTotalLastRow,
                                     k+numEdgeShapeFuncTotalLastCol+numStandardDof) += getConstitutiveExtraDofDiffusionEqRatio() * ratio *
                                                                                       (Grads[j+nbFFTotalLastRow][m] * dFluxdVectorCurl.operator()(m,n) *
                                                                                        CurlcurlVals[k+numEdgeShapeFuncTotalLastCol][n]);
                          }

                          //if (extraDOFField == 0)
                          {
                              for (unsigned int l = 0; l < 3; ++l)
                              {
                                  mStiff(j+nbFFTotalLastRow,
                                         k+numEdgeShapeFuncTotalLastCol+numStandardDof) += getConstitutiveExtraDofDiffusionEqRatio() * ratio *
                                                                                           (Vals[j+nbFFTotalLastRow] *
                                                                                            dwTdVectorPotential.operator()(l) *
                                                                                            curlVals[k+numEdgeShapeFuncTotalLastCol][l]);
                                  mStiff(j+nbFFTotalLastRow,
                                         k+numEdgeShapeFuncTotalLastCol+numStandardDof) += getConstitutiveExtraDofDiffusionEqRatio() * ratio *
                                                                                           (Vals[j+nbFFTotalLastRow] *
                                                                                            dw_AVdVectorPotential.operator()(l) *
                                                                                            curlVals[k+numEdgeShapeFuncTotalLastCol][l]);
                                  mStiff(j+nbFFTotalLastRow,
                                         k+numEdgeShapeFuncTotalLastCol+numStandardDof) += getConstitutiveExtraDofDiffusionEqRatio() * ratio *
                                                                                           (Vals[j+nbFFTotalLastRow] *
                                                                                            dw_AVdVectorCurl.operator()(l) *
                                                                                            CurlcurlVals[k+numEdgeShapeFuncTotalLastCol][l]);
                              }
                          }
                      }
                  }
            }
          }            
        }
      }
    }
    
    //
    /***
     * CURL FIELD
     * */
    if (getNumConstitutiveCurlVariable() >0)
    {
      for (int i = 0; i < npts; i++)
      {
        const IPStateBase *ips        = (*vips)[i];
        const dG3DIPVariableBase *ipv     = static_cast<const dG3DIPVariableBase*>(ips->getState(IPStateBase::current));
        
        if( ipv->getNumConstitutiveCurlVariable() > getNumConstitutiveCurlVariable() ) 
          Msg::Error("Your material law uses more constitutive curl data variables than your domain dG3DStiffnessBulk::get");
    
        const double weight = GP[i].weight;
        const double detJ = ipv->getJacobianDeterminant(ele,GP[i]);
        const double ratio = detJ*weight;
      
        // Basis functions for vector field at edges
        const std::vector<SVector3> &curlVals = ipv->fcurl(&space1,ele,GP[i],"HcurlLegendre");
        // Basis functions for Curl of vector field at edges
        const std::vector<SVector3> &CurlcurlVals = ipv->Curlfcurl(&space1,ele,GP[i],"CurlHcurlLegendre");

        // Nodal shape function
        const std::vector<TensorialTraits<double>::GradType> &Grads = ipv->gradf(&space1,ele,GP[i]);
        const std::vector<TensorialTraits<double>::ValType> &Vals = ipv->f(&space1,ele,GP[i]);

        for (unsigned int curlField = 0; curlField < ipv->getNumConstitutiveCurlVariable(); ++curlField)
        {
            const unsigned int IndexCurl = 3 + getNumNonLocalVariable() + getNumConstitutiveExtraDofDiffusionVariable() + curlField;
            const unsigned int numEdgeShapeFuncRow = nlspace->getNumShapeFunctions(ele, IndexCurl);
            const unsigned int numEdgeShapeFuncTotalLastRow = nlspace->getShapeFunctionsIndex(ele, IndexCurl);
            const unsigned int numStandardDof = curlspace->getNumKeysByType(CurlMixedFunctionSpace<double>::DOF_STANDARD, ele);

            // make curl-curl term
            for (unsigned int curlField2 = 0; curlField2 < ipv->getNumConstitutiveCurlVariable(); ++curlField2)
            {
                const unsigned int IndexCurl2 = 3 + getNumNonLocalVariable() + getNumConstitutiveExtraDofDiffusionVariable() + curlField2;
                const unsigned int numEdgeShapeFuncCol = nlspace->getNumShapeFunctions(ele, IndexCurl2);
                const unsigned int numEdgeShapeFuncTotalLastCol = nlspace->getShapeFunctionsIndex(ele, IndexCurl2);

                const STensor3 &dHdA = ipv->getConstRefTodVectorFielddVectorPotential()[curlField][curlField2];
                const STensor3 &dHdB = ipv->getConstRefTodVectorFielddVectorCurl()[curlField][curlField2];

                STensor3 dSourceVectorFielddVectorCurl(0.0);
                STensor3 dSourceVectorFielddVectorPotential(0.0);

                if (getConstitutiveCurlAccountFieldSource())
                {
                    dSourceVectorFielddVectorCurl += ipv->getConstRefTodSourceVectorFielddVectorCurl()[curlField][curlField2];
                    dSourceVectorFielddVectorPotential += ipv->getConstRefTodSourceVectorFielddVectorPotential()[curlField][curlField2];
                }

                for (unsigned int j = 0; j < numEdgeShapeFuncRow; ++j)
                {
                    for (unsigned int k = 0; k < numEdgeShapeFuncCol; ++k)
                    {
                        for (unsigned int m = 0; m < 3; ++m)
                        {
                            for (unsigned int n = 0; n < 3; ++n)
                            {
                                mStiff(j + numEdgeShapeFuncTotalLastRow + numStandardDof,
                                       k + numEdgeShapeFuncTotalLastCol + numStandardDof) +=
                                        getConstitutiveCurlEqRatio() * ratio *
                                        (CurlcurlVals[j + numEdgeShapeFuncTotalLastRow][m] *
                                         dHdA.operator()(m, n) *
                                         curlVals[k + numEdgeShapeFuncTotalLastCol][n]);
                                mStiff(j + numEdgeShapeFuncTotalLastRow + numStandardDof,
                                       k + numEdgeShapeFuncTotalLastCol + numStandardDof) +=
                                        getConstitutiveCurlEqRatio() * ratio *
                                        (CurlcurlVals[j + numEdgeShapeFuncTotalLastRow][m] *
                                         dHdB.operator()(m, n) *
                                         CurlcurlVals[k + numEdgeShapeFuncTotalLastCol][n]);
                                mStiff(j + numEdgeShapeFuncTotalLastRow + numStandardDof,
                                       k + numEdgeShapeFuncTotalLastCol + numStandardDof) +=
                                        getConstitutiveCurlEqRatio() * ratio *
                                        (curlVals[j + numEdgeShapeFuncTotalLastRow][m] *
                                        dSourceVectorFielddVectorPotential.operator()(m, n) *
                                         curlVals[k + numEdgeShapeFuncTotalLastCol][n]);
                                mStiff(j + numEdgeShapeFuncTotalLastRow + numStandardDof,
                                       k + numEdgeShapeFuncTotalLastCol + numStandardDof) +=
                                        getConstitutiveCurlEqRatio() * ratio *
                                        (curlVals[j + numEdgeShapeFuncTotalLastRow][m] *
                                        dSourceVectorFielddVectorCurl.operator()(m, n) *
                                         CurlcurlVals[k + numEdgeShapeFuncTotalLastCol][n]);
                            }
                        }
                    }
                }
            }
        }

          for (unsigned int curlField = 0; curlField < ipv->getNumConstitutiveCurlVariable(); ++curlField)
          {
              const unsigned int IndexCurl = 3 + getNumNonLocalVariable() + getNumConstitutiveExtraDofDiffusionVariable() + curlField;
              const unsigned int numEdgeShapeFuncRow = nlspace->getNumShapeFunctions(ele, IndexCurl);
              const unsigned int numEdgeShapeFuncTotalLastRow = nlspace->getShapeFunctionsIndex(ele, IndexCurl);
              const unsigned int numStandardDof = curlspace->getNumKeysByType(CurlMixedFunctionSpace<double>::DOF_STANDARD, ele);
              // make curl-disp term
              {
                  // dHdF
                  const STensor33 &dVectorFielddF = ipv->getConstRefTodVectorFielddF()[curlField];
                  STensor33 dSourceVectorFielddF;
                  STensor33 dMechFieldSourcedF;
                  STensor3 dMechFieldSourcedVectorPotential;
                  STensor3 dMechFieldSourcedVectorCurl;
                  STensorOperation::zero(dSourceVectorFielddF);
                  STensorOperation::zero(dMechFieldSourcedF);
                  STensorOperation::zero(dMechFieldSourcedVectorPotential);
                  STensorOperation::zero(dMechFieldSourcedVectorCurl);

                  if (getConstitutiveCurlAccountFieldSource())
                  {
                      dSourceVectorFielddF += ipv->getConstRefTodSourceVectorFielddF()[curlField];
                  }
                  if (getConstitutiveCurlAccountMecaSource())
                  {
                      dMechFieldSourcedF += ipv->getConstRefTodMechanicalFieldSourcedF()[curlField];
                      dMechFieldSourcedVectorPotential += ipv->getConstRefTodMechanicalFieldSourcedVectorPotential()[curlField][curlField];
                      dMechFieldSourcedVectorCurl += ipv->getConstRefTodMechanicalFieldSourcedVectorCurl()[curlField][curlField];
                  }

                  for (unsigned int kk = 0; kk < 3; ++kk)
                  {
                      const unsigned int nbFFCol = nlspace->getNumShapeFunctions(ele, kk);
                      const unsigned int nbFFTotalLastCol = nlspace->getShapeFunctionsIndex(ele, kk);

                      for (unsigned int j = 0; j < numEdgeShapeFuncRow; ++j)
                          for (unsigned int k = 0; k < nbFFCol; ++k)
                          {
                              for (unsigned int m = 0; m < 3; ++m)
                              {
                                  for (unsigned int n = 0; n < 3; ++n)
                                  {
                                      mStiff(j + numEdgeShapeFuncTotalLastRow + numStandardDof, k + nbFFTotalLastCol) +=
                                              getConstitutiveCurlEqRatio() * ratio *
                                              (CurlcurlVals[j+numEdgeShapeFuncTotalLastRow][m]*
                                              dVectorFielddF.operator()(m,kk,n)*
                                              Grads[k+nbFFTotalLastCol][n]);
                                      mStiff(j + numEdgeShapeFuncTotalLastRow + numStandardDof, k + nbFFTotalLastCol) +=
                                              getConstitutiveCurlEqRatio() * ratio *
                                              (curlVals[j+numEdgeShapeFuncTotalLastRow][m]*
                                               dSourceVectorFielddF.operator()(m,kk,n)*
                                               Grads[k+nbFFTotalLastCol][n]);
                                  }
                              }
                          }
                  }
              }
          }
     
          // make curl-nonlocal term
          for (unsigned int curlField = 0; curlField < ipv->getNumConstitutiveCurlVariable(); ++curlField)
          {
              const unsigned int IndexCurl = 3 + getNumNonLocalVariable() + getNumConstitutiveExtraDofDiffusionVariable() + curlField;
              const unsigned int numEdgeShapeFuncRow = nlspace->getNumShapeFunctions(ele, IndexCurl);
              const unsigned int numEdgeShapeFuncTotalLastRow = nlspace->getShapeFunctionsIndex(ele, IndexCurl);
              const unsigned int numStandardDof = curlspace->getNumKeysByType(CurlMixedFunctionSpace<double>::DOF_STANDARD, ele);

              if (getNumNonLocalVariable() > 0)
              {
                  if (ipv->getNumberNonLocalVariable() > getNumNonLocalVariable())
                      Msg::Error("Your material law uses more non local variables than your domain dG3DStiffnessBulk::get");

                  for (unsigned int nlk = 0; nlk < ipv->getNumberNonLocalVariable(); ++nlk)
                  {
                      const unsigned int IndexNlk = 3 + nlk;
                      const unsigned int nbFFCol = nlspace->getNumShapeFunctions(ele, IndexNlk);
                      const unsigned int nbFFTotalLastCol = nlspace->getShapeFunctionsIndex(ele, IndexNlk);

                      const unsigned int extraDOFField = 0; // Thermal field EM source
                      const SVector3 &dVectorFielddNonLocalVar = ipv->getConstRefTodVectorFielddNonLocalVariable()[curlField][nlk];

                      SVector3 dSourceVectorFielddNonLocalVar(0.0);

                      if (getConstitutiveCurlAccountFieldSource())
                      {
                          dSourceVectorFielddNonLocalVar += ipv->getConstRefTodSourceVectorFielddNonLocalVariable()[curlField][nlk];
                      }

                      for (unsigned int j = 0; j < numEdgeShapeFuncRow; ++j)
                          for (unsigned int k = 0; k < nbFFCol; ++k)
                          {
                              mStiff(j + numEdgeShapeFuncTotalLastRow + numStandardDof,
                                     k + nbFFTotalLastCol) += getConstitutiveCurlEqRatio() * ratio * 0.0;
                          }
                  }
              }
          }

          // make curl- extraDof term
          for (unsigned int curlField = 0; curlField < ipv->getNumConstitutiveCurlVariable(); ++curlField)
          {
              const unsigned int IndexCurl = 3 + getNumNonLocalVariable() + getNumConstitutiveExtraDofDiffusionVariable() + curlField;
              const unsigned int numEdgeShapeFuncRow = nlspace->getNumShapeFunctions(ele, IndexCurl);
              const unsigned int numEdgeShapeFuncTotalLastRow = nlspace->getShapeFunctionsIndex(ele, IndexCurl);
              const unsigned int numStandardDof = curlspace->getNumKeysByType(CurlMixedFunctionSpace<double>::DOF_STANDARD, ele);

              if (getNumConstitutiveExtraDofDiffusionVariable() > 0)
              {
                  if (ipv->getNumConstitutiveExtraDofDiffusionVariable() > getNumConstitutiveExtraDofDiffusionVariable())
                      Msg::Error("Your material law uses more constitutive extra dof variables than your domain dG3DStiffnessBulk::get");

                  for (unsigned int extraDOFField = 0; extraDOFField < ipv->getNumConstitutiveExtraDofDiffusionVariable(); ++extraDOFField)
                  {
                      const unsigned int IndexField = 3 + getNumNonLocalVariable() + extraDOFField;
                      const unsigned int nbFFCol = nlspace->getNumShapeFunctions(ele, IndexField);
                      const unsigned int nbFFTotalLastCol = nlspace->getShapeFunctionsIndex(ele, IndexField);

                      // dHdV, dHdT, dHdGradV, dHdGradT
                      const SVector3 &dVectorFielddExtraDofField = ipv->getConstRefTodVectorFielddExtraDofField()[curlField][extraDOFField];
                      const STensor3 &dVectorFielddGradExtraDofField = ipv->getConstRefTodVectorFielddGradExtraDofField()[curlField][extraDOFField];

                      SVector3 dSourceVectorFielddExtraDofField(0.0);
                      STensor3 dSourceVectorFielddGradExtraDofField(0.0);

                      if (getConstitutiveCurlAccountFieldSource())
                      {
                          dSourceVectorFielddExtraDofField += ipv->getConstRefTodSourceVectorFielddExtraDofField()[curlField][extraDOFField];
                          dSourceVectorFielddGradExtraDofField += ipv->getConstRefTodSourceVectorFielddGradExtraDofField()[curlField][extraDOFField];
                      }

                      for (unsigned int j = 0; j < numEdgeShapeFuncRow; ++j)
                          for (unsigned int k = 0; k < nbFFCol; ++k)
                          {
                              for (unsigned int m = 0; m < 3; ++m)
                              {
                                  mStiff(j+numEdgeShapeFuncTotalLastRow+numStandardDof,k+nbFFTotalLastCol) += getConstitutiveCurlEqRatio() * ratio *
                                                                                                              (dVectorFielddExtraDofField.operator()(m) *
                                                                                                              CurlcurlVals[j+numEdgeShapeFuncTotalLastRow][m] *
                                                                                                              Vals[k+nbFFTotalLastCol]);
                                  mStiff(j+numEdgeShapeFuncTotalLastRow+numStandardDof,k+nbFFTotalLastCol) += getConstitutiveCurlEqRatio() * ratio *
                                                                                                              (dSourceVectorFielddExtraDofField.operator()(m) *
                                                                                                               curlVals[j+numEdgeShapeFuncTotalLastRow][m] *
                                                                                                               Vals[k+nbFFTotalLastCol]);
                                  for (unsigned int n = 0; n < 3; ++n)
                                  {
                                      mStiff(j+numEdgeShapeFuncTotalLastRow+numStandardDof,k+nbFFTotalLastCol) += getConstitutiveCurlEqRatio() * ratio *
                                                                                                                  (CurlcurlVals[j+numEdgeShapeFuncTotalLastRow][m] *
                                                                                                                  dVectorFielddGradExtraDofField.operator()(m,n) *
                                                                                                                  Grads[k+nbFFTotalLastCol][n]);
                                      mStiff(j+numEdgeShapeFuncTotalLastRow+numStandardDof,k+nbFFTotalLastCol) += getConstitutiveCurlEqRatio() * ratio *
                                                                                                                  (curlVals[j+numEdgeShapeFuncTotalLastRow][m] *
                                                                                                                  dSourceVectorFielddGradExtraDofField.operator()(m,n) *
                                                                                                                   Grads[k+nbFFTotalLastCol][n]);
                                  }
                              }
                          }
                  }
              }
          }
      }
    }
  //mStiff.print("bulk");
  } // end if sym
  else
    printf("not implemented\n");
}

void dG3DElasticStiffnessBulk::get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &mStiff) const
{
    dG3DStiffnessBulk::get(ele, npts, GP, mStiff, false, false);
  //mStiff.print("Elastic bulk");
}

void dG3DUndamagedElasticStiffnessBulk::get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &mStiff) const
{
    dG3DStiffnessBulk::get(ele, npts, GP, mStiff, false, true);
  //mStiff.print("Elastic bulk");
}


void dG3DForceInter::get(MElement *ele, int npts, IntPt *GP, fullVector<double> &m)const
{
  MInterfaceElement *ie = dynamic_cast<MInterfaceElement*>(ele);
  const int nbdofm = _minusSpace->getNumKeys(ie->getElem(0));
  const int nbdofp = _plusSpace->getNumKeys(ie->getElem(1));
  m.resize(nbdofm+nbdofp);
  m.setAll(0.);
  if(_fullDg)
  {
    // characteristic size
    const double hs = ie->getCharacteristicSize();

    // get value at gauss's point
    const AllIPState::ipstateElementContainer *vips = _ipf->getAips()->getIPstate(ele->getNum());
    // Values on minus and plus elements
    IntPt *GPm; IntPt *GPp;
    _interQuad->getIntPoints(ie,GP,&GPm,&GPp);
    
    MElement* em = ie->getElem(0);
    MElement* ep = ie->getElem(1);
    
    /*
     * DISPLACEMENT FIELD
     * */
    // loop on Gauss Point
    for(int i=0;i<npts; i++)
    {
      // IP
      const IPStateBase *ipsm        = (*vips)[i];
      const dG3DIPVariableBase *ipvm     = static_cast<const dG3DIPVariableBase*>(ipsm->getState(IPStateBase::current));

      const IPStateBase *ipsp        = (*vips)[i+npts];
      const dG3DIPVariableBase *ipvp     = static_cast<const dG3DIPVariableBase*>(ipsp->getState(IPStateBase::current));
      
      const SVector3 &referenceOutwardNormalm = ipvm->getConstRefToReferenceOutwardNormal();
      const SVector3 &referenceOutwardNormalp = ipvp->getConstRefToReferenceOutwardNormal();
      const SVector3 &jump = ipvp->getConstRefToJump();

      // Weight of Gauss' point i and Jacobian's value at this point
      const double weight = GP[i].weight;
      const double& detJ = ipvm->getJacobianDeterminant(ie,GP[i]);
      const double wJ = detJ* weight;
      
      //
      const std::vector<TensorialTraits<double>::GradType> &Gradsm = ipvm->gradf(_minusSpace,ie->getElem(0),GPm[i]);
      const std::vector<TensorialTraits<double>::GradType> &Gradsp = ipvp->gradf(_plusSpace,ie->getElem(1),GPp[i]);
      const std::vector<TensorialTraits<double>::ValType> &Valsm = ipvm->f(_minusSpace,ie->getElem(0),GPm[i]);
      const std::vector<TensorialTraits<double>::ValType> &Valsp = ipvp->f(_plusSpace,ie->getElem(1),GPp[i]);
      
      // possibility of fracture
      bool broken = false;
      if(_mlawMinus->getType() == materialLaw::fracture )
      {
        // when if failure is checked at the convergence state, previous or current is not important
        // but when failure is checked on the fly,  current must be used
        // this  corresponds to a more general case 
        const FractureCohesive3DIPVariable *ipvwf = static_cast<const FractureCohesive3DIPVariable*>(ipvm); // broken via minus (OK broken on both sides)
        broken = ipvwf->isbroken();
      }
      
      int nbFFm = _minusSpace->getNumShapeFunctions(em,0);
      int nbFFp = _plusSpace->getNumShapeFunctions(ep,0); // same for 0, 1, 2

      if(!broken)
      {
        static SVector3 nu;
        nu = referenceOutwardNormalm;
        nu*=(1./referenceOutwardNormalm.norm());

        // consistency \mean{P}
        static SVector3 meanPN;
        for(int k =0; k<3; k++)
        {
          meanPN[k]  = 0.;
          for(int l =0; l<3; l++)
          {
            meanPN[k] += (ipvp->getConstRefToFirstPiolaKirchhoffStress()(k,l)+ipvm->getConstRefToFirstPiolaKirchhoffStress()(k,l))*(nu(l)/2.);
          }
        }
        // Stability  N \mean{H} (jump N)
        
        const double b1hs = getStabilityParameter()/hs;
        static STensor43 meanH;
        meanH = ipvp->getConstRefToDGElasticTangentModuli();
        meanH+= ipvm->getConstRefToDGElasticTangentModuli();
        meanH*=(0.5);

        static SVector3 NMeanHJumpNBetasc;
        for(int k = 0; k <3; k++)
        {
          NMeanHJumpNBetasc(k) = 0.;
          for(int l = 0; l <3; l++)
          {
            for(int m = 0; m <3; m++)
            {
              for(int n = 0; n <3; n++)
              {
                NMeanHJumpNBetasc(k) += meanH(k,l,m,n)*jump(m)*nu(n)*b1hs*nu(l);
              }
            }
          }
        }
        // Assembly consistency + stability (if not broken)
        for(int j=0;j<nbFFm;j++){
          for(int k=0;k<3;k++){
            m(j+k*nbFFm) -= (meanPN[k]+NMeanHJumpNBetasc[k])*(Valsm[j+k*nbFFm]*wJ);
          }
        }
        for(int j=0;j<nbFFp;j++){
          for(int k=0;k<3;k++){
            m(j+k*nbFFp+nbdofm) += (meanPN[k]+NMeanHJumpNBetasc[k])*(Valsp[j+k*nbFFp]*wJ);
          }
        }
        // compatibility membrane (4 terms to assembly)
        static STensor3 HmJumpN;
        static STensor3 HpJumpN;

        for(int k = 0; k <3; k++)
        {
          for(int l = 0; l <3; l++)
          {
            HmJumpN(k,l) = 0.;
            HpJumpN(k,l) = 0.;
            for(int m = 0; m <3; m++)
            {
              for(int n = 0; n <3; n++)
              {
                HmJumpN(k,l) += ipvm->getConstRefToDGElasticTangentModuli()(m,n,k,l)*jump(m)*nu(n);
                HpJumpN(k,l) += ipvp->getConstRefToDGElasticTangentModuli()(m,n,k,l)*jump(m)*nu(n);
              }
            }
          }
        }
        // Assembly (loop on shape function)
        for(int j=0;j<nbFFm;j++)
        {
          for(int k=0;k<3;k++)
          {
            for(int l=0;l<3;l++)
            {
              m(j+k*nbFFm) += (HmJumpN(k,l)*Gradsm[j+k*nbFFm][l])*(wJ/2.);
            }
          }
        }
        for(int j=0;j<nbFFm;j++)
        {
          for(int k=0;k<3;k++)
          {
            for(int l=0;l<3;l++)
            {
              m(j+k*nbFFp+nbdofm) += (HpJumpN(k,l)*Gradsp[j+k*nbFFp][l])*(wJ/2.);
            }
          }
        }
      }
      else
      { // broken case
        const FractureCohesive3DIPVariable *ipvmf = static_cast<const FractureCohesive3DIPVariable*>(ipvm);
        const FractureCohesive3DIPVariable *ipvpf = static_cast<const FractureCohesive3DIPVariable*>(ipvp);

        static SVector3 interfaceForce;
        interfaceForce = ipvmf->getConstRefToInterfaceForce();
        interfaceForce += ipvpf->getConstRefToInterfaceForce();
        interfaceForce*= 0.5;

        const std::vector<TensorialTraits<double>::ValType> &Valsm = ipvm->f(_minusSpace,ie->getElem(0),GPm[i]);
        const std::vector<TensorialTraits<double>::ValType> &Valsp = ipvp->f(_plusSpace,ie->getElem(1),GPp[i]);

        // Assembly consistency + stability (if not broken)
        for(int j=0;j<nbFFm;j++){
          for(int k=0;k<3;k++){
            m(j+k*nbFFm) -= interfaceForce[k]*(Valsm[j+k*nbFFm]*wJ);
          }
        }
        for(int j=0;j<nbFFp;j++){
          for(int k=0;k<3;k++){
            m(j+k*nbFFp+nbdofm) += interfaceForce[k]*(Valsp[j+k*nbFFp]*wJ);
          }
        }
      }
    } 
    /***
    * NONLOCAL FIELD
    * */
    if ((getNumNonLocalVariable()> 0) and getNonLocalContinuity())
    {
      // loop on Gauss Point
      for(int i=0;i<npts; i++)
      {
        // IP
        const IPStateBase *ipsm        = (*vips)[i];
        const dG3DIPVariableBase *ipvm     = static_cast<const dG3DIPVariableBase*>(ipsm->getState(IPStateBase::current));
        const dG3DIPVariableBase *ipvmprev = static_cast<const dG3DIPVariableBase*>(ipsm->getState(IPStateBase::previous));

        const IPStateBase *ipsp        = (*vips)[i+npts];
        const dG3DIPVariableBase *ipvp     = static_cast<const dG3DIPVariableBase*>(ipsp->getState(IPStateBase::current));
        const dG3DIPVariableBase *ipvpprev    = static_cast<const dG3DIPVariableBase*>(ipsp->getState(IPStateBase::previous));

        const SVector3 &referenceOutwardNormalm = ipvm->getConstRefToReferenceOutwardNormal();
        const SVector3 &referenceOutwardNormalp = ipvp->getConstRefToReferenceOutwardNormal();
        const SVector3 &jump = ipvp->getConstRefToJump();

        // Weight of Gauss' point i and Jacobian's value at this point
        const double weight = GP[i].weight;
        const double& detJ = ipvm->getJacobianDeterminant(ie,GP[i]);
        const double wJ = detJ* weight;


        const std::vector<TensorialTraits<double>::GradType> &Gradsm = ipvm->gradf(_minusSpace,ie->getElem(0),GPm[i]);
        const std::vector<TensorialTraits<double>::GradType> &Gradsp = ipvp->gradf(_plusSpace,ie->getElem(1),GPp[i]);
        const std::vector<TensorialTraits<double>::ValType> &Valsm = ipvm->f(_minusSpace,ie->getElem(0),GPm[i]);
        const std::vector<TensorialTraits<double>::ValType> &Valsp = ipvp->f(_plusSpace,ie->getElem(1),GPp[i]);
        // possibility of fracture
        bool broken = false;
        if(_mlawMinus->getType() == materialLaw::fracture )
        {
          // when if failure is checked at the convergence state, previous or current is not important
          // but when failure is checked on the fly,  current must be used
          // this  corresponds to a more general case - Van Dung
          const FractureCohesive3DIPVariable *ipvwf = static_cast<const FractureCohesive3DIPVariable*>(ipvm); // broken via minus (OK broken on both sides)
          broken = ipvwf->isbroken();
        }

        if(!broken)
        {
          static SVector3 nu;
          nu = referenceOutwardNormalm;
          nu*=(1./referenceOutwardNormalm.norm());

          //non local
          if( ipvm->getNumberNonLocalVariable()>getNumNonLocalVariable() ) Msg::Error("Your minus material law uses more non local variable than your domain dG3DForceInter::get");
          if( ipvp->getNumberNonLocalVariable()>getNumNonLocalVariable() ) Msg::Error("Your plus material law uses more non local variable than your domain dG3DForceInter::get");
          if( ipvm->getNumberNonLocalVariable()!= ipvm->getNumberNonLocalVariable()  ) Msg::Error("Your minus and plus material laws do not have the same number of non local variables dG3DForceInter::get");
          
 
          // characteristic size
          const double nbetahs = getNonLocalStabilityParameter()/hs;
 
          for (int nlk = 0; nlk< ipvp->getNumberNonLocalVariable(); nlk++)
          {
            int nbFFmNonlocal = _minusSpace->getNumShapeFunctions(em,3+nlk);
            int nbFFmTotalLastNonlocal = _minusSpace->getShapeFunctionsIndex(em,3+nlk);
            int nbFFpNonlocal = _plusSpace->getNumShapeFunctions(ep,3+nlk); 
            int nbFFpTotalLastNonlocal = _plusSpace->getShapeFunctionsIndex(ep,3+nlk);
            
            double epljump = ipvp->getConstRefToNonLocalJump()(nlk);
            if (_incrementNonlocalBased){
              epljump -= ipvpprev->getConstRefToNonLocalJump()(nlk);
            }
            const STensor3  *cgm = &(ipvm->getConstRefToCharacteristicLengthMatrix(nlk));           // by Wu Ling
            const STensor3  *cgp = &(ipvp->getConstRefToCharacteristicLengthMatrix(nlk));           // by Wu Ling
            
            static SVector3 gradm, gradp;
            gradm = ipvm->getConstRefToGradNonLocalVariable()[nlk];
            gradp = ipvp->getConstRefToGradNonLocalVariable()[nlk];
            if (_incrementNonlocalBased)
            {
              gradm -= ipvmprev->getConstRefToGradNonLocalVariable()[nlk];
              gradp -= ipvpprev->getConstRefToGradNonLocalVariable()[nlk];
            };

            SVector3 cgGradm;
            SVector3 cgGradp;

            for(int k =0; k<3; k++)
            {
              cgGradm[k] = 0.;
              cgGradp[k] = 0.;
              for(int l =0; l<3; l++)
              {
                cgGradm[k] += cgm->operator()(k,l)*gradm(l);
                cgGradp[k] += cgp->operator()(k,l)*gradp(l);
              }
            }

            // consistency \mean{cg*grad eps}
            double meanCgGradN =0.;
            for(int k =0; k<3; k++)
            {
              meanCgGradN += (cgGradm(k)+cgGradp(k))*(nu(k)/2.);
            }
            // Stability  N \mean{H} (jump N)
            double NMeanCgJumpNBetasc = 0.;
            static STensor3 meanCg;
            meanCg = (*cgm);
            meanCg+= (*cgp);
            meanCg*=(1./2.);
            for(int l = 0; l <3; l++)
            {
              for(int n = 0; n <3; n++)
              {
                NMeanCgJumpNBetasc += meanCg(l,n)*epljump*nu(n)*nbetahs*nu(l);
              }
            }
            // Assembly consistency + stability (if not broken)
            for(int j=0;j<nbFFmNonlocal;j++)
            {
              m(j+nbFFmTotalLastNonlocal) -= (meanCgGradN+NMeanCgJumpNBetasc)*(Valsm[j+nbFFmTotalLastNonlocal]*wJ*getNonLocalEqRatio());
            }
            for(int j=0;j<nbFFpNonlocal;j++)
            {
              m(j+nbFFpTotalLastNonlocal+nbdofm) += (meanCgGradN+NMeanCgJumpNBetasc)*(Valsp[j+nbFFpTotalLastNonlocal]*wJ*getNonLocalEqRatio());
            }
            // compatibility membrane (4 terms to assembly)
            SVector3 CgmJumpN;
            SVector3 CgpJumpN;

            for(int k = 0; k <3; k++)
            {
              CgmJumpN(k) = 0.;
              CgpJumpN(k) = 0.;
              for(int m = 0; m <3; m++)
              {
                CgmJumpN(k) += cgm->operator()(k,m)*epljump*nu(m);
                CgpJumpN(k) += cgp->operator()(k,m)*epljump*nu(m);
              }
            }
            // Assembly (loop on shape function)
            for(int j=0;j<nbFFmNonlocal;j++)
            {
              for(int l=0;l<3;l++)
              {
                m(j+nbFFmTotalLastNonlocal) += (CgmJumpN(l)*Gradsm[j+nbFFmTotalLastNonlocal][l])*(wJ*getNonLocalEqRatio()/2.);
              }
            }
            for(int j=0;j<nbFFpNonlocal;j++)
            {
              for(int l=0;l<3;l++)
              {
                m(j+nbFFpTotalLastNonlocal+nbdofm) += (CgpJumpN(l)*Gradsp[j+nbFFpTotalLastNonlocal][l])*(wJ*getNonLocalEqRatio()/2.);
              }
            }
          }
        }
      }
    }

    /***
     * EXTRA DOF FIELD
     * */
    if ((getNumConstitutiveExtraDofDiffusionVariable() > 0) and getConstitutiveExtraDofDiffusionContinuity())
    {
      // loop on Gauss Point
      for(int i=0;i<npts; i++)
      {
        // IP
        const IPStateBase *ipsm        = (*vips)[i];
        const dG3DIPVariableBase *ipvm     = static_cast<const dG3DIPVariableBase*>(ipsm->getState(IPStateBase::current));
        const dG3DIPVariableBase *ipvmprev = static_cast<const dG3DIPVariableBase*>(ipsm->getState(IPStateBase::previous));

        const IPStateBase *ipsp        = (*vips)[i+npts];
        const dG3DIPVariableBase *ipvp     = static_cast<const dG3DIPVariableBase*>(ipsp->getState(IPStateBase::current));
        const dG3DIPVariableBase *ipvpprev    = static_cast<const dG3DIPVariableBase*>(ipsp->getState(IPStateBase::previous));

        const SVector3 &referenceOutwardNormalm = ipvm->getConstRefToReferenceOutwardNormal();
        const SVector3 &referenceOutwardNormalp = ipvp->getConstRefToReferenceOutwardNormal();
        const SVector3 &jump = ipvp->getConstRefToJump();

        // Weight of Gauss' point i and Jacobian's value at this point
        const double weight = GP[i].weight;
        const double& detJ = ipvm->getJacobianDeterminant(ie,GP[i]);
        const double wJ = detJ* weight;


        const std::vector<TensorialTraits<double>::GradType> &Gradsm = ipvm->gradf(_minusSpace,ie->getElem(0),GPm[i]);
        const std::vector<TensorialTraits<double>::GradType> &Gradsp = ipvp->gradf(_plusSpace,ie->getElem(1),GPp[i]);
        const std::vector<TensorialTraits<double>::ValType> &Valsm = ipvm->f(_minusSpace,ie->getElem(0),GPm[i]);
        const std::vector<TensorialTraits<double>::ValType> &Valsp = ipvp->f(_plusSpace,ie->getElem(1),GPp[i]);
        // possibility of fracture
        bool broken = false;
        if(_mlawMinus->getType() == materialLaw::fracture )
        {
          // when if failure is checked at the convergence state, previous or current is not important
          // but when failure is checked on the fly,  current must be used
          // this  corresponds to a more general case - Van Dung
          const FractureCohesive3DIPVariable *ipvwf = static_cast<const FractureCohesive3DIPVariable*>(ipvm); // broken via minus (OK broken on both sides)
          broken = ipvwf->isbroken();
        }

        if(!broken)
        {
          /***
           * the weak form  of the extraDofs is generally given as
           * consistency terms  <q_j>*n*jump(vf_j)  
           * symmetrisation terms jump(f_i) n*<k0_ij:grad(vf_j)>
           * penalty terms jump(f_i) n*<betahs*k0_ij>*jump(vf_j)  
           * with i,j are indexes of extraDof fields
           * */
          static SVector3 nu;
          nu = referenceOutwardNormalm;
          nu*=(1./referenceOutwardNormalm.norm());
          
          if( ipvm->getNumConstitutiveExtraDofDiffusionVariable()>getNumConstitutiveExtraDofDiffusionVariable() ) 
              Msg::Error("Your material law uses more constitutive extra dof variables than your domain dG3DForceInter::get");
          if( ipvp->getNumConstitutiveExtraDofDiffusionVariable()>getNumConstitutiveExtraDofDiffusionVariable() ) 
              Msg::Error("Your material law uses more constitutive extra dof variables than your domain dG3DForceInter::get");
          if( ipvp->getNumConstitutiveExtraDofDiffusionVariable()!= ipvm->getNumConstitutiveExtraDofDiffusionVariable() ) 
              Msg::Error("Your plus and minus material laws do not use the same number of constitutive extra dof variables dG3DForceInter::get");
          for (int extraDOFField = 0; extraDOFField< ipvp->getNumConstitutiveExtraDofDiffusionVariable(); extraDOFField++)
          {
            int indexField=3+getNumNonLocalVariable()+extraDOFField;
            int nbFFmExtraDof = _minusSpace->getNumShapeFunctions(em,indexField);
            int nbFFmTotalLastExtraDof = _minusSpace->getShapeFunctionsIndex(em,indexField);
            int nbFFpExtraDof = _plusSpace->getNumShapeFunctions(ep,indexField); 
            int nbFFpTotalLastExtraDof = _plusSpace->getShapeFunctionsIndex(ep,indexField);
            
            for (int extraDOFField2 = 0; extraDOFField2< ipvp->getNumConstitutiveExtraDofDiffusionVariable(); extraDOFField2++)
            {
              const double nbetahs = getConstitutiveExtraDofDiffusionStabilityParameter()/hs;
              double fieldJump;
              double fieldp=0.;
              double fieldm=0.;

              if(ipvp->getNumConstitutiveExtraDofDiffusionVariable()==1)
              {
               fieldJump = ipvp->getConstRefToFieldJump()(extraDOFField);
               fieldp = ipvp->getConstRefToField(extraDOFField);
               fieldm = ipvm->getConstRefToField(extraDOFField);
              }
              else
              {
               fieldJump = ipvp->getConstRefToEnergyConjugatedFieldJump()(extraDOFField2);
               fieldp = ipvp->getConstRefToEnergyConjugatedField(extraDOFField);
               fieldm = ipvm->getConstRefToEnergyConjugatedField(extraDOFField);
              }

              static STensor3  k0m;
              k0m = *(ipvm->getConstRefToLinearK()[extraDOFField][extraDOFField2]);
              static STensor3  k0p;
              k0p = *(ipvp->getConstRefToLinearK()[extraDOFField][extraDOFField2]);
              
              
              if(ipvp->getNumConstitutiveExtraDofDiffusionVariable()==1) //thermomec
              {
                if(getConstitutiveExtraDofDiffusionUseEnergyConjugatedField())
                {
                  k0m*=(-1.)*fieldm*fieldm;
                  k0p*=(-1.)*fieldp*fieldp;
                }
              }
              
              const SVector3  &fluxm = (ipvm->getConstRefToFlux()[extraDOFField]);
              const SVector3  &fluxp = (ipvp->getConstRefToFlux()[extraDOFField]);
              // consistency \mean{cg*grad eps}
              double meantflux =0.;
              for(int k =0; k<3; k++)
              {
                meantflux += (fluxm(k)+fluxp(k))*(nu(k)/2.);
              }
              double NMeank0JumpNBetasc = 0.;
              static STensor3 meank;
              meank = (k0m);
              meank+= (k0p);
              meank*=(1./2.);

              for(int l = 0; l <3; l++)
              {
                for(int n = 0; n <3; n++)
                {
                  NMeank0JumpNBetasc += meank(l,n)*fieldJump*nu(n)*nbetahs*nu(l);
                }
              }
              
              if(extraDOFField==extraDOFField2)
              {
                for(int j=0;j<nbFFmExtraDof;j++)
                {
                  m(j+nbFFmTotalLastExtraDof) -= (meantflux+NMeank0JumpNBetasc)*(Valsm[j+nbFFmTotalLastExtraDof]*wJ*getConstitutiveExtraDofDiffusionEqRatio());
                }
                for(int j=0;j<nbFFpExtraDof;j++)
                {
                  m(j+nbFFpTotalLastExtraDof+nbdofm) += ( meantflux+NMeank0JumpNBetasc)*(Valsp[j+nbFFpTotalLastExtraDof]*wJ*getConstitutiveExtraDofDiffusionEqRatio());
                }
              }
              else
              {
                for(int j=0;j<nbFFmExtraDof;j++)
                {
                  m(j+nbFFmTotalLastExtraDof) -= (NMeank0JumpNBetasc)*(Valsm[j+nbFFmTotalLastExtraDof]*wJ*getConstitutiveExtraDofDiffusionEqRatio());
                }
                for(int j=0;j<nbFFpExtraDof;j++)
                {
                  m(j+nbFFpTotalLastExtraDof+nbdofm) += (NMeank0JumpNBetasc)*(Valsp[j+nbFFpTotalLastExtraDof]*wJ*getConstitutiveExtraDofDiffusionEqRatio());
                }
              }
      
              SVector3 k0pJumpN;
              STensorOperation::zero(k0pJumpN);
              SVector3 k0mJumpN;
              STensorOperation::zero(k0mJumpN);
              for(int k = 0; k <3; k++)
              {
                for(int m = 0; m <3; m++)
                {
                  k0mJumpN(k) += k0m(k,m)*fieldJump*nu(m);
                  k0pJumpN(k) += k0p(k,m)*fieldJump*nu(m);
                }
              }
              // Assembly (loop on shape function)
              for(int j=0;j<nbFFmExtraDof;j++)
              {
                for(int l=0;l<3;l++)
                {
                  m(j+nbFFmTotalLastExtraDof) += ( k0mJumpN(l)*Gradsm[j+nbFFmTotalLastExtraDof][l])*(wJ*getConstitutiveExtraDofDiffusionEqRatio()/2.);
                }
              }
              for(int j=0;j<nbFFpExtraDof;j++)
              {
                for(int l=0;l<3;l++)
                {
                  m(j+nbFFpTotalLastExtraDof+nbdofm) += ( k0pJumpN(l)*Gradsp[j+nbFFpTotalLastExtraDof][l])*(wJ*getConstitutiveExtraDofDiffusionEqRatio()/2.);
                }
              }
            }
          }
          
          /****
           * addtitional  term only for first (thermal) field
           *  gamma*[u]*<Stiff_alphadialitationm*varT>
           *  if getConstitutiveExtraDofDiffusionUseEnergyConjugatedField() is True
           *  f = 1/T --> gamma* [u]*<Stiff_alphadialitationm*(-T*T)*varf>
           * 
           * */
          for (int extraDOFField = 0; extraDOFField< ipvp->getNumConstitutiveExtraDofDiffusionVariable(); extraDOFField++)
          {
            int indexField=3+getNumNonLocalVariable()+extraDOFField;
            int nbFFmExtraDof = _minusSpace->getNumShapeFunctions(em,indexField);
            int nbFFmTotalLastExtraDof = _minusSpace->getShapeFunctionsIndex(em,indexField);
            int nbFFpExtraDof = _plusSpace->getNumShapeFunctions(ep,indexField); 
            int nbFFpTotalLastExtraDof = _plusSpace->getShapeFunctionsIndex(ep,indexField);
            
            if(extraDOFField==0)
            {
              double fieldJump;
              double fieldp=0.;
              double fieldm=0.;

              if(ipvp->getNumConstitutiveExtraDofDiffusionVariable()==1)
              {
               fieldJump = ipvp->getConstRefToFieldJump()(extraDOFField);
               fieldp = ipvp->getConstRefToField(extraDOFField);
               fieldm = ipvm->getConstRefToField(extraDOFField);
              }
              else
              {
               fieldJump = ipvp->getConstRefToEnergyConjugatedFieldJump()(extraDOFField);
               fieldp = ipvp->getConstRefToEnergyConjugatedField(extraDOFField);
               fieldm = ipvm->getConstRefToEnergyConjugatedField(extraDOFField);
              }

              static STensor3  Stiff_alphadialitationm;
              static STensor3  Stiff_alphadialitationp;
              Stiff_alphadialitationm = *(ipvm->getConstRefToLinearSymmetrizationCoupling()[extraDOFField]);
              Stiff_alphadialitationp = *(ipvp->getConstRefToLinearSymmetrizationCoupling()[extraDOFField]);
              
              if(ipvp->getNumConstitutiveExtraDofDiffusionVariable()==1) //thermomec
              {
                if(getConstitutiveExtraDofDiffusionUseEnergyConjugatedField())
                {
                  Stiff_alphadialitationm*= (-1.)*fieldm*fieldm;
                  Stiff_alphadialitationp*= (-1.)*fieldp*fieldp;
                }
              }
              else
              {
                Stiff_alphadialitationm*= (-1.)/(fieldm*fieldm);
                Stiff_alphadialitationp*= (-1.)/(fieldp*fieldp); 
              }  

              double Stiff_alphadialitationp_JumpN=0.;
              double Stiff_alphadialitationm_JumpN=0.;
              for(int k = 0; k <3; k++)
              {
                for(int m = 0; m <3; m++)
                {
                  Stiff_alphadialitationp_JumpN += Stiff_alphadialitationp(k,m)*jump(k)*nu(m);
                  Stiff_alphadialitationm_JumpN += Stiff_alphadialitationm(k,m)*jump(k)*nu(m);
                }
              }
              int indexField=3+getNumNonLocalVariable()+extraDOFField;
              
              //new
            
              double gamma=0.;
              for(int j=0;j<nbFFmExtraDof;j++){
                m(j+nbFFmTotalLastExtraDof) += gamma*( Stiff_alphadialitationm_JumpN*Valsm[j+nbFFmTotalLastExtraDof])*(wJ/2.)*(-1.);
              }
              for(int j=0;j<nbFFpExtraDof;j++){
                m(j+nbFFpTotalLastExtraDof+nbdofm) += gamma*( Stiff_alphadialitationp_JumpN*Valsp[j+nbFFpTotalLastExtraDof])*(wJ/2.)*(-1.);
              }
            }
          }
          //
        }
      }
    }
  }
//m.print("Force inter");
};

void dG3DStiffnessInter::get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &stiff) const
{
  MInterfaceElement *ie = dynamic_cast<MInterfaceElement*>(ele);
  const int nbdofm = _minusSpace->getNumKeys(ie->getElem(0));
  const int nbdofp = _plusSpace->getNumKeys(ie->getElem(1));
  stiff.resize(nbdofm+nbdofp,nbdofm+nbdofp);
  stiff.setAll(0.);
  if(_fullDg)
  {
    // characteristic size
    const double hs = ie->getCharacteristicSize();
   
    // get value at gauss's point
    const AllIPState::ipstateElementContainer *vips = _ipf->getAips()->getIPstate(ele->getNum());
    // Values on minus and plus elements
    IntPt *GPm; IntPt *GPp;
    _interQuad->getIntPoints(ie,GP,&GPm,&GPp);
    
    MElement* em = ie->getElem(0);
    MElement* ep = ie->getElem(1);
    
    /****
     * DISPLACEMENT FIELD
     * */
    static std::vector<SVector3> dBarJdum,dBarJdup;
    static std::vector< std::vector< STensor33 > > dBarFdum,dBarFdup; //for each GP and for each node

    const dG3DMaterialLaw *mlawbulkm;
    const dG3DMaterialLaw *mlawbulkp;
    if(_mlawMinus->getType() == materialLaw::fracture)
      mlawbulkm = static_cast<const dG3DMaterialLaw* >((static_cast<const FractureByCohesive3DLaw* > (_mlawMinus) )->getBulkLaw());
    else
      mlawbulkm= static_cast<const dG3DMaterialLaw*>(_mlawMinus);
    if(_mlawPlus->getType() == materialLaw::fracture)
      mlawbulkp = static_cast<const dG3DMaterialLaw* >((static_cast<const FractureByCohesive3DLaw* > (_mlawPlus) )->getBulkLaw());
    else
      mlawbulkp= static_cast<const dG3DMaterialLaw*>(_mlawPlus);
    if(mlawbulkm->getUseBarF()!=mlawbulkp->getUseBarF())
      Msg::Error("dG3DStiffnessInter::get both law should use barF or not");
    bool useBarF=mlawbulkm->getUseBarF();
    if(useBarF)
    {
      int nbFFm = _minusSpace->getNumShapeFunctions(em,0); // same for component 0, 1, 2
      int nbFFp = _plusSpace->getNumShapeFunctions(ep,0);
      dBarJdum.resize(nbFFm);
      dBarFdum.resize(npts);
      dBarJdup.resize(nbFFp);
      dBarFdup.resize(npts);
      for (int k=0;k<nbFFm;k++)
      {
        STensorOperation::zero(dBarJdum[k]); //check here if ok
      }
      for (int k=0;k<nbFFp;k++)
      {
        STensorOperation::zero(dBarJdup[k]); //check here if ok
      }
      for (int ipg = 0; ipg < npts; ipg++)
      {
        dBarFdum[ipg].resize(nbFFm);
        dBarFdup[ipg].resize(nbFFp);
        for (int k=0;k<nbFFm;k++)
        {
          STensorOperation::zero(dBarFdum[ipg][k]); //check here if ok
        }
        for (int k=0;k<nbFFp;k++)
        {
          STensorOperation::zero(dBarFdup[ipg][k]); //check here if ok
        }
      }
      double totalWeight=0.;
      for(int ipg = 0; ipg < npts; ipg++)
        totalWeight+=GP[ipg].weight;
      for(int ipg = 0; ipg < npts; ipg++)
      {
        const IPStateBase *ipsm        = (*vips)[ipg];
        const dG3DIPVariableBase *ipvm     = static_cast<const dG3DIPVariableBase*>(ipsm->getState(IPStateBase::current));
        const IPStateBase *ipsp        = (*vips)[ipg+npts];
        const dG3DIPVariableBase *ipvp     = static_cast<const dG3DIPVariableBase*>(ipsp->getState(IPStateBase::current));
        const FractureCohesive3DIPVariable *ipvwfm;
        const FractureCohesive3DIPVariable *ipvwfp;
        const STensor3 *barFm;
        const STensor3 *barFp;
        if(_mlawMinus->getType() == materialLaw::fracture )
        {
          ipvwfm = static_cast<const FractureCohesive3DIPVariable*>(ipvm);
          barFm=&ipvwfm->getBulkDeformationGradient(); //this is actually barF
          ipvwfp = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
          barFp=&ipvwfp->getBulkDeformationGradient(); //this is actually barF
        }
        else
        {
          barFm=&ipvm->getConstRefToDeformationGradient(); //this is actually barF
          barFp=&ipvp->getConstRefToDeformationGradient(); //this is actually barF
        }
        const std::vector<TensorialTraits<double>::GradType> &Gradsm = ipvm->gradf(_minusSpace,ie->getElem(0),GPm[ipg]);
        const std::vector<TensorialTraits<double>::GradType> &Gradsp = ipvp->gradf(_plusSpace,ie->getElem(1),GPp[ipg]);

        STensor3 invBarFm(barFm->invert());
        double localJm=ipvm->getLocalJ();
        double barJm=ipvm->getBarJ();
        double weightm=GP[ipg].weight*pow(localJm,(1.-1./((double)(_dim))))*pow(barJm,1./((double)(_dim)))/totalWeight;

        STensor3 invBarFp(barFp->invert());
        double localJp=ipvp->getLocalJ();
        double barJp=ipvp->getBarJ();
        double weightp=GP[ipg].weight*pow(localJp,(1.-1./((double)(_dim))))*pow(barJp,1./((double)(_dim)))/totalWeight;

        for(int k=0;k<nbFFm;k++)
        {
          SVector3 dBarJduTmp;
          STensorOperation::zero(dBarJduTmp);
          for(int ll=0; ll<_dim; ll++)
          {
            for(int n=0; n<_dim; n++)
            {
              dBarJduTmp(ll) += invBarFm(n,ll)*Gradsm[k][n]*weightm;
            }
          }
          dBarJdum[k]+=dBarJduTmp; 
        }
        for(int k=0;k<nbFFp;k++)
        {
          SVector3 dBarJduTmp;
          STensorOperation::zero(dBarJduTmp);
          for(int ll=0; ll<_dim; ll++)
          {
            for(int n=0; n<_dim; n++)
            {
              dBarJduTmp(ll) += invBarFp(n,ll)*Gradsp[k][n]*weightp;
            }
          }
          dBarJdup[k]+=dBarJduTmp; 
        }
      }
      
      static STensor3 I;
      STensorOperation::unity(I);
      for (int ipg = 0; ipg < npts; ipg++)
      {
        const IPStateBase *ipsm        = (*vips)[ipg];
        const dG3DIPVariableBase *ipvm     = static_cast<const dG3DIPVariableBase*>(ipsm->getState(IPStateBase::current));
        const IPStateBase *ipsp        = (*vips)[ipg+npts];
        const dG3DIPVariableBase *ipvp     = static_cast<const dG3DIPVariableBase*>(ipsp->getState(IPStateBase::current));
        const FractureCohesive3DIPVariable *ipvwfm;
        const FractureCohesive3DIPVariable *ipvwfp;
        const STensor3 *barFm;
        const STensor3 *barFp;
        if(_mlawMinus->getType() == materialLaw::fracture )
        {
          ipvwfm = static_cast<const FractureCohesive3DIPVariable*>(ipvm);
          barFm=&ipvwfm->getBulkDeformationGradient(); //this is actually barF
          ipvwfp = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
          barFp=&ipvwfp->getBulkDeformationGradient(); //this is actually barF
        }
        else
        {
          barFm=&ipvm->getConstRefToDeformationGradient(); //this is actually barF
          barFp=&ipvp->getConstRefToDeformationGradient(); //this is actually barF
        }

        const std::vector<TensorialTraits<double>::GradType> &Gradsm = ipvm->gradf(_minusSpace,ie->getElem(0),GPm[ipg]);
        const std::vector<TensorialTraits<double>::GradType> &Gradsp = ipvp->gradf(_plusSpace,ie->getElem(1),GPp[ipg]);

        STensor3 invBarFm(barFm->invert());
        double localJm=ipvm->getLocalJ();
        double barJm=ipvm->getBarJ();
        double weightm=GP[ipg].weight*pow(localJm,2./3.)*pow(barJm,1./3.)/totalWeight;

        STensor3 invBarFp(barFp->invert());
        double localJp=ipvp->getLocalJ();
        double barJp=ipvp->getBarJ();
        double weightp=GP[ipg].weight*pow(localJp,2./3.)*pow(barJp,1./3.)/totalWeight;

        for(int k=0;k<nbFFm;k++)
        {
          static STensor33 dBarFduTmp;
          STensorOperation::zero(dBarFduTmp);
          for(int kk=0; kk<_dim; kk++)
          {
            for(int m=0; m<_dim; m++)
            {
              for(int jj=0; jj<_dim; jj++)
              {
                dBarFduTmp(kk,m,jj) += pow(barJm,1./((double)(_dim)))*pow(localJm,-1./((double)(_dim)))*I(kk,jj)*Gradsm[k](m);
                for(int p=0; p<_dim; p++)
                {
                  dBarFduTmp(kk,m,jj) -= 1./((double)(_dim))*pow(barJm,1./((double)(_dim)))*pow(localJm,-1./((double)(_dim)))*
                                                        barFm->operator()(kk,m)*invBarFm(p,jj)*Gradsm[k](p);             
                }
                dBarFduTmp(kk,m,jj) += 1./((double)(_dim))/barJm*barFm->operator()(kk,m)*dBarJdum[k](jj);
              }                                
            }
          }
          if(_dim==2 or _dim ==1)
            dBarFduTmp(2,2,2)=Gradsm[k](3);
          if(_dim ==1)
            dBarFduTmp(1,1,1)=Gradsm[k](2);
          dBarFdum[ipg][k]=dBarFduTmp;
        }
        for(int k=0;k<nbFFp;k++)
        {
          static STensor33 dBarFduTmp;
          STensorOperation::zero(dBarFduTmp);
          for(int kk=0; kk<_dim; kk++)
          {
            for(int m=0; m<_dim; m++)
            {
              for(int jj=0; jj<_dim; jj++)
              {
                dBarFduTmp(kk,m,jj) += pow(barJp,1./((double)(_dim)))*pow(localJp,-1./((double)(_dim)))*I(kk,jj)*Gradsp[k](m);
                for(int p=0; p<_dim; p++)
                {
                  dBarFduTmp(kk,m,jj) -= 1./((double)(_dim))*pow(barJp,1./((double)(_dim)))*pow(localJp,-1./((double)(_dim)))*
                                                        barFp->operator()(kk,m)*invBarFp(p,jj)*Gradsp[k](p);             
                }
                dBarFduTmp(kk,m,jj) += 1./((double)(_dim))/barJp*barFp->operator()(kk,m)*dBarJdup[k](jj);
              }                                
            }
          }
          if(_dim==2 or _dim ==1)
            dBarFduTmp(2,2,2)=Gradsp[k](3);
          if(_dim ==1)
            dBarFduTmp(1,1,1)=Gradsp[k](2);
          dBarFdup[ipg][k]=dBarFduTmp;
        }
      }
    }
    
    for(int i=0;i<npts; i++)
    {
      // IP
      const IPStateBase *ipsm        = (*vips)[i];
      const dG3DIPVariableBase *ipvm     = static_cast<const dG3DIPVariableBase*>(ipsm->getState(IPStateBase::current));

      const IPStateBase *ipsp        = (*vips)[i+npts];
      const dG3DIPVariableBase *ipvp     = static_cast<const dG3DIPVariableBase*>(ipsp->getState(IPStateBase::current));

      const SVector3 &referenceOutwardNormalm = ipvm->getConstRefToReferenceOutwardNormal();
      const SVector3 &referenceOutwardNormalp = ipvp->getConstRefToReferenceOutwardNormal();
      const SVector3 &jump = ipvp->getConstRefToJump();

      // Weight of Gauss' point i and Jacobian's value at this point
      const double weight = GP[i].weight;
      const double& detJ = ipvm->getJacobianDeterminant(ie,GP[i]);
      const double wJ = detJ* weight;

      const std::vector<TensorialTraits<double>::GradType> &Gradsm = ipvm->gradf(_minusSpace,ie->getElem(0),GPm[i]);
      const std::vector<TensorialTraits<double>::GradType> &Gradsp = ipvp->gradf(_plusSpace,ie->getElem(1),GPp[i]);
      
      const std::vector<TensorialTraits<double>::ValType> &Valsm = ipvm->f(_minusSpace,ie->getElem(0),GPm[i]);
      const std::vector<TensorialTraits<double>::ValType> &Valsp = ipvp->f(_plusSpace,ie->getElem(1),GPp[i]);


      // possibility of fracture
      bool broken = false;
//    bool prevbroken = false;
      if(_mlawMinus->getType() == materialLaw::fracture )
      {
        const FractureCohesive3DIPVariable *ipvwf = static_cast<const FractureCohesive3DIPVariable*>(ipvm); // broken via minus (OK broken on both sides)
        broken = ipvwf->isbroken();
      }

      if(broken)
      {
        
        const FractureCohesive3DIPVariable *ipvmf = static_cast<const FractureCohesive3DIPVariable*>(ipvm);
        const FractureCohesive3DIPVariable *ipvpf = static_cast<const FractureCohesive3DIPVariable*>(ipvp);

        static STensor3 DInterfaceForceDjump;
        DInterfaceForceDjump = ipvmf->getConstRefToDInterfaceForceDjump();
        DInterfaceForceDjump += ipvpf->getConstRefToDInterfaceForceDjump();
        DInterfaceForceDjump *= 0.5;
        
        int nbFFm = _minusSpace->getNumShapeFunctions(em,0); // same for component 0, 1, 2
        int nbFFp = _plusSpace->getNumShapeFunctions(ep,0); // same for component 0, 1, 2

        // Assembly (if broken)
        for(int j=0;j<nbFFm;j++)
        {
          for(int k=0;k<3;k++)
          {
            for(int l=0;l<nbFFm;l++)
            {
              for(int m=0;m<3;m++)
              {
                stiff(j+k*nbFFm,l+m*nbFFm) += DInterfaceForceDjump(k,m)*(Valsm[j+0*nbFFm]*Valsm[l+0*nbFFm]*wJ);
              }
            }
            for(int l=0;l<nbFFp;l++)
            {
              for(int m=0;m<3;m++)
              {
                stiff(j+k*nbFFm,l+m*nbFFp+nbdofm) -= DInterfaceForceDjump(k,m)*(Valsm[j+0*nbFFm]*Valsp[l+0*nbFFp]*wJ);
              }
            }
          }
        }
        for(int j=0;j<nbFFp;j++)
        {
          for(int k=0;k<3;k++)
          {
            for(int l=0;l<nbFFm;l++)
            {
              for(int m=0;m<3;m++)
              {
                stiff(j+k*nbFFp+nbdofm,l+m*nbFFm) -= DInterfaceForceDjump(k,m)*(Valsp[j+0*nbFFp]*Valsm[l+0*nbFFm]*wJ);
              }
            }
            for(int l=0;l<nbFFp;l++)
            {
                for(int m=0;m<3;m++)
              {
                stiff(j+k*nbFFp+nbdofm,l+m*nbFFp+nbdofm) += DInterfaceForceDjump(k,m)*(Valsp[j+0*nbFFp]*Valsp[l+0*nbFFp]*wJ);
              }
            }
          }
        }
        
        if (ipvmf->withDeformationGradient() and ipvpf->withDeformationGradient())
        {
          const STensor33 &DInterfaceForceDFm = ipvmf->getConstRefToDInterfaceForceDDeformationGradient();
          const STensor33 &DInterfaceForceDFp = ipvpf->getConstRefToDInterfaceForceDDeformationGradient();

          for(int j=0;j<nbFFm;j++){
            for(int k=0;k<3;k++){
              for(int l=0;l<nbFFm;l++){
                for(int m=0;m<3;m++)
                {
                  if(useBarF)
                  {
                    for(int ii=0; ii<3; ii++)
                    {
                      for(int p=0; p<3; p++)
                      {
                        stiff(j+k*nbFFm,l+m*nbFFm) -= DInterfaceForceDFm(k,ii,p)*dBarFdum[i][l](ii,p,m)*Valsm[j+0*nbFFm]*wJ*0.5;
                      }
                    }
                  }
                  else
                  {
                    for (int n=0; n<3; n++){
                      stiff(j+k*nbFFm,l+m*nbFFm) -= DInterfaceForceDFm(k,m,n)*Gradsm[l+0*nbFFm](n)*Valsm[j+0*nbFFm]*wJ*0.5;
                    }
                  }
                }
              }
              for(int l=0;l<nbFFp;l++){
                for(int m=0;m<3;m++){
                  if(useBarF)
                  {
                    for(int ii=0; ii<3; ii++)
                    {
                      for(int p=0; p<3; p++)
                      { 
                        stiff(j+k*nbFFm,l+m*nbFFp+nbdofm) -= DInterfaceForceDFp(k,ii,p)*dBarFdup[i][l](ii,p,m)*Valsm[j+0*nbFFm]*wJ*0.5;
                      }
                    }
                  }
                  else
                  {
                    for (int n=0; n<3; n++){
                      stiff(j+k*nbFFm,l+m*nbFFp+nbdofm) -= DInterfaceForceDFp(k,m,n)*Gradsp[l+0*nbFFp](n)*Valsm[j+0*nbFFm]*wJ*0.5;
                    }
                  }
                }
              }
            }
          }

          for(int j=0;j<nbFFp;j++){
            for(int k=0;k<3;k++){
              for(int l=0;l<nbFFm;l++){
                for(int m=0;m<3;m++){
                  if(useBarF)
                  {
                    for(int ii=0; ii<3; ii++)
                    {
                      for(int p=0; p<3; p++)
                      {
                        stiff(j+k*nbFFp+nbdofm,l+m*nbFFm) += DInterfaceForceDFm(k,ii,p)*dBarFdum[i][l](ii,p,m)*Valsp[j+0*nbFFp]*wJ*0.5;
                      }
                    }
                  }
                  else
                  {
                    for (int n=0; n<3; n++){
                      stiff(j+k*nbFFp+nbdofm,l+m*nbFFm) += DInterfaceForceDFm(k,m,n)*Gradsm[l+0*nbFFm](n)*Valsp[j+0*nbFFp]*wJ*0.5;
                    }
                  }
                }
              }
              for(int l=0;l<nbFFp;l++){
                for(int m=0;m<3;m++){
                  if(useBarF)
                  {
                    for(int ii=0; ii<3; ii++)
                    {
                      for(int p=0; p<3; p++)
                      { 
                        stiff(j+k*nbFFp+nbdofm,l+m*nbFFp+nbdofm) += DInterfaceForceDFp(k,ii,p)*
                                                                   dBarFdup[i][l](ii,p,m)*Valsp[j+0*nbFFp]*wJ*0.5;
                      }
                    }
                  }
                  else
                  {
                    for (int n=0; n<3; n++){
                      stiff(j+k*nbFFp+nbdofm,l+m*nbFFp+nbdofm) += DInterfaceForceDFp(k,m,n)*Gradsp[l+0*nbFFp](n)*Valsp[j+0*nbFFp]*wJ*0.5;
                    }
                  }
                }
              }
            }
          }
        }
        
        if (ipvmf->withJumpGradient() and ipvpf->withJumpGradient())
        {

          //std::vector<TensorialTraits<double>::GradType> gradValInt;
          //_minusSpace->gradf(ele,GP[i].pt[0],GP[i].pt[1],GP[i].pt[2],gradValInt);
          std::vector<TensorialTraits<double>::GradType>& gradValInt = ipvm->gradf(_minusSpace,ie,GP[i]);

          int nbvertexInter = ele->getNumVertices();
          std::vector<int> vn;
          vn.resize(nbvertexInter);
          ie->getLocalVertexNum(0,vn);
          std::vector<int> vp;
          vp.resize(nbvertexInter);
          ie->getLocalVertexNum(1,vp);

          const STensor33 &DInterfaceForceDjumpGradm = ipvmf->getConstRefToDInterfaceForceDjumpGradient();
          const STensor33 &DInterfaceForceDjumpGradp = ipvpf->getConstRefToDInterfaceForceDjumpGradient();

          for(int j=0;j<nbFFm;j++){
            for(int k=0;k<3;k++){
              for(int l=0;l<nbvertexInter;l++){
                for(int m=0;m<3;m++){
                  for (int n=0; n<3; n++){
                    stiff(j+k*nbFFm,vn[l]+m*nbFFm) += 0.5*(DInterfaceForceDjumpGradm(k,m,n)+DInterfaceForceDjumpGradp(k,m,n))*gradValInt[l+m*nbvertexInter](n)*Valsm[j+0*nbFFm]*wJ;
                  }
                }
              }
              for(int l=0;l<nbvertexInter;l++){
                for(int m=0;m<3;m++){
                  for (int n=0; n<3; n++){
                    stiff(j+k*nbFFm,vp[l]+m*nbFFp+nbdofm) -= 0.5*(DInterfaceForceDjumpGradm(k,m,n)+DInterfaceForceDjumpGradp(k,m,n))*gradValInt[l+m*nbvertexInter](n)*Valsm[j+0*nbFFm]*wJ;
                  }
                }
              }
            }
          }

          for(int j=0;j<nbFFp;j++){
            for(int k=0;k<3;k++){
              for(int l=0;l<nbvertexInter;l++){
                for(int m=0;m<3;m++){
                  for (int n=0; n<3; n++){
                    stiff(j+k*nbFFp+nbdofm,vn[l]+m*nbFFm) -= 0.5*(DInterfaceForceDjumpGradm(k,m,n)+DInterfaceForceDjumpGradp(k,m,n))*gradValInt[l+m*nbvertexInter](n)*Valsp[j+0*nbFFp]*wJ;
                  }
                }
              }
              for(int l=0;l<nbvertexInter;l++){
                for(int m=0;m<3;m++){
                  for (int n=0; n<3; n++){
                    stiff(j+k*nbFFp+nbdofm,vp[l]+m*nbFFp+nbdofm) += 0.5*(DInterfaceForceDjumpGradm(k,m,n)+DInterfaceForceDjumpGradp(k,m,n))*gradValInt[l+m*nbvertexInter](n)*Valsp[j+0*nbFFp]*wJ;
                  }
                }
              }
            }
          }
        }
        // Here we have extra dof part
        if( ipvm->getNumConstitutiveExtraDofDiffusionVariable()>getNumConstitutiveExtraDofDiffusionVariable() ) 
            Msg::Error("Your material law uses more constitutive extra dof variable than your domain dG3DStiffnessInter::get");
        if( ipvp->getNumConstitutiveExtraDofDiffusionVariable()>getNumConstitutiveExtraDofDiffusionVariable() ) 
            Msg::Error("Your material law uses more constitutive extra dof variable than your domain dG3DStiffnessInter::get");
        if( ipvp->getNumConstitutiveExtraDofDiffusionVariable()!= ipvm->getNumConstitutiveExtraDofDiffusionVariable() ) 
            Msg::Error("Your plus and minus material laws do not use the same number of constitutive extra dof variables dG3DStiffnessInter::get");
        for (int extraDOFField = 0; extraDOFField< ipvp->getNumConstitutiveExtraDofDiffusionVariable(); extraDOFField++){
            Msg::Error("Need to implement the stiffness matrix related to the CZM/CBM and constitutiveExtraDOFVariable dG3DStiffnessInter::get");
        }
      }
      else
      {
        static SVector3 nu;
        nu = referenceOutwardNormalm;
        nu.normalize();
        
        /***DISPLACEMENt - DISPLACEMENT*/
        
        // consistency D\mean{P}DF
        const STensor43 &Hp = ipvp->getConstRefToTangentModuli();
        const STensor43 &Hm = ipvm->getConstRefToTangentModuli();
        const STensor43 &eHp = ipvp->getConstRefToDGElasticTangentModuli();
        const STensor43 &eHm = ipvm->getConstRefToDGElasticTangentModuli();
        
        int nbFFm = _minusSpace->getNumShapeFunctions(em,0); // same for component 0, 1, 2
        int nbFFp = _plusSpace->getNumShapeFunctions(ep,0); // same for component 0, 1, 2
        
        // Assembly consistency
        for(int j=0;j<nbFFm;j++)
        {
          for(int k=0;k<3;k++)
          {
            for(int l=0;l<nbFFm;l++)
            {
              for(int m=0;m<3;m++)
              {
                for(int q = 0; q< 3; q++)
                {
                  if(useBarF)
                  {
                    for(int ii=0; ii<3; ii++)
                    {
                      for(int p=0; p<3; p++)
                      {
                        stiff(j+k*nbFFm,l+m*nbFFm) -= Hm(k,q,ii,p)*(dBarFdum[i][l](ii,p,m)*Valsm[j+0*nbFFm]*wJ/2.*nu(q));
                      }
                    }
                  }
                  else
                  {
                    for(int p = 0; p< 3; p++)
                    {
                      stiff(j+k*nbFFm,l+m*nbFFm) -= Hm(k,q,m,p)*(Gradsm[l+0*nbFFm][p]*Valsm[j+0*nbFFm]*wJ/2.*nu(q));
                    }
                  }
                }
              }
            }
            for(int l=0;l<nbFFp;l++)
            {
              for(int m=0;m<3;m++)
              {
                for(int q = 0; q< 3; q++)
                {
                  if(useBarF)
                  {
                    for(int ii=0; ii<3; ii++)
                    {
                      for(int p=0; p<3; p++)
                      {
                        stiff(j+k*nbFFm,l+m*nbFFp+nbdofm) -= Hp(k,q,ii,p)*(dBarFdup[i][l](ii,p,m)*Valsm[j+0*nbFFm]*wJ/2.*nu(q));
                      }
                    }
                  }
                  else
                  {
                    for(int p = 0; p< 3; p++)
                    {
                      stiff(j+k*nbFFm,l+m*nbFFp+nbdofm) -= Hp(k,q,m,p)*(Gradsp[l+0*nbFFp][p]*Valsm[j+0*nbFFm]*wJ/2.*nu(q));
                    }
                  }
                }
              }
            }
          }
        }
        for(int j=0;j<nbFFp;j++)
        {
          for(int k=0;k<3;k++)
          {
            for(int l=0;l<nbFFm;l++)
            {
              for(int m=0;m<3;m++)
              {
                for(int q = 0; q< 3; q++)
                {
                  if(useBarF)
                  {
                    for(int ii=0; ii<3; ii++)
                    {
                      for(int p=0; p<3; p++)
                      {
                        stiff(j+k*nbFFp+nbdofm,l+m*nbFFm) += Hm(k,q,ii,p)*(dBarFdum[i][l](ii,p,m)*Valsp[j+0*nbFFp]*wJ/2.*nu(q));
                      }
                    }
                  }
                  else
                  {
                    for(int p = 0; p< 3; p++)
                    {
                      stiff(j+k*nbFFp+nbdofm,l+m*nbFFm) += Hm(k,q,m,p)*(Gradsm[l+0*nbFFm][p]*Valsp[j+0*nbFFp]*wJ/2.*nu(q));
                    }
                  }
                }
              }
            }
            for(int l=0;l<nbFFp;l++)
            {
              for(int m=0;m<3;m++)
              {
                for(int q = 0; q< 3; q++)
                {
                  if(useBarF)
                  {
                    for(int ii=0; ii<3; ii++)
                    {
                      for(int p=0; p<3; p++)
                      {
                        stiff(j+k*nbFFp+nbdofm,l+m*nbFFp+nbdofm) += Hp(k,q,ii,p)*(dBarFdup[i][l](ii,p,m)*Valsp[j+0*nbFFp]*wJ/2.*nu(q));
                      }
                    }
                  }
                  else
                  {
                    for(int p = 0; p< 3; p++)
                    {
                      stiff(j+k*nbFFp+nbdofm,l+m*nbFFp+nbdofm) += Hp(k,q,m,p)*(Gradsp[l+0*nbFFp][p]*Valsp[j+0*nbFFp]*wJ/2.*nu(q));
                    }
                  }
                }
              }
            }
          }
        }

        // Stability  N \mean{H} (N)
        const double b1hs = getStabilityParameter()/hs;
        static STensor3 NMeanHNBetasc;
        for(int k = 0; k <3; k++)
        {
          for(int m = 0; m <3; m++)
          {
            NMeanHNBetasc(k,m) = 0.;
            for(int l = 0; l <3; l++)
            {
              for(int n = 0; n <3; n++)
              {
                NMeanHNBetasc(k,m) += (eHp(k,l,m,n)+eHm(k,l,m,n))*nu(n)*b1hs*nu(l)/2.;
              }
            }
          }
        }
        // Assembly stability (if not broken)
        for(int j=0;j<nbFFm;j++)
        {
          for(int k=0;k<3;k++)
          {
            for(int l=0;l<nbFFm;l++)
            {
              for(int m=0;m<3;m++)
              {
                stiff(j+k*nbFFm,l+m*nbFFm) += NMeanHNBetasc(k,m)*(Valsm[j+0*nbFFm]*Valsm[l+0*nbFFm]*wJ);
              }
            }
            for(int l=0;l<nbFFp;l++)
            {
              for(int m=0;m<3;m++)
              {
                stiff(j+k*nbFFm,l+m*nbFFp+nbdofm) -= NMeanHNBetasc(k,m)*(Valsm[j+0*nbFFm]*Valsp[l+0*nbFFp]*wJ);
              }
            }
          }
        }
        for(int j=0;j<nbFFp;j++)
        {
          for(int k=0;k<3;k++)
          {
            for(int l=0;l<nbFFm;l++)
            {
              for(int m=0;m<3;m++)
              {
                stiff(j+k*nbFFp+nbdofm,l+m*nbFFm) -= NMeanHNBetasc(k,m)*(Valsp[j+0*nbFFp]*Valsm[l+0*nbFFm]*wJ);
              }
            }
            for(int l=0;l<nbFFp;l++)
            {
              for(int m=0;m<3;m++)
              {
                stiff(j+k*nbFFp+nbdofm,l+m*nbFFp+nbdofm) += NMeanHNBetasc(k,m)*(Valsp[j+0*nbFFp]*Valsp[l+0*nbFFp]*wJ);
              }
            }
          }
        }
        // compatibility membrane (4 terms to assembly)
        static STensor3 HmJumpN;
        static STensor3 HpJumpN;

        for(int k = 0; k <3; k++)
        {
          for(int l = 0; l <3; l++)
          {
            HmJumpN(k,l) = 0.;
            HpJumpN(k,l) = 0.;
            for(int m = 0; m <3; m++)
            {
              for(int n = 0; n <3; n++)
              {
                HmJumpN(k,l) += eHm(m,n,k,l)*jump(m)*nu(n);
                HpJumpN(k,l) += eHp(m,n,k,l)*jump(m)*nu(n);
              }
            }
          }
        }
        // Assembly (loop on shape function)
        for(int j=0;j<nbFFm;j++)
        {
          for(int k=0;k<3;k++)
          {
            for(int l=0;l<nbFFm;l++)
            {
              for(int m=0;m<3;m++)
              {
                for(int p =0; p < 3; p++)
                {
                  for(int q =0; q < 3; q++)
                  {
                    stiff(j+k*nbFFm,l+m*nbFFm) -= (eHm(m,p,k,q)*Gradsm[j+0*nbFFm][q])*(nu(p)*wJ/2.*Valsm[l+0*nbFFm]);
                  }
                }
              }
            }
            for(int l=0;l<nbFFp;l++)
            {
              for(int m=0;m<3;m++)
              {
                for(int p =0; p < 3; p++)
                {
                  for(int q =0; q < 3; q++)
                  {
                    stiff(j+k*nbFFm,l+m*nbFFp+nbdofm) += (eHm(m,p,k,q)*Gradsm[j+0*nbFFm][q])*(nu(p)*wJ/2.*Valsp[l+0*nbFFp]);
                  }
                }
              }
            }
          }
        }
        for(int j=0;j<nbFFp;j++)
        {
          for(int k=0;k<3;k++)
          {
            for(int l=0;l<nbFFm;l++)
            {
              for(int m=0;m<3;m++)
              {
                for(int p =0; p < 3; p++)
                {
                  for(int q =0; q < 3; q++)
                  {
                    stiff(j+k*nbFFp+nbdofm,l+m*nbFFm) -= (eHp(m,p,k,q)*Gradsp[j+0*nbFFp][q])*(nu(p)*wJ/2.*Valsm[l+0*nbFFm]);
                  }
                }
              }
            }
            for(int l=0;l<nbFFp;l++)
            {
              for(int m=0;m<3;m++)
              {
                for(int p =0; p < 3; p++)
                {
                  for(int q =0; q < 3; q++)
                  {
                    stiff(j+k*nbFFp+nbdofm,l+m*nbFFp+nbdofm) += (eHp(m,p,k,q)*Gradsp[j+0*nbFFp][q])*(nu(p)*wJ/2.*Valsp[l+0*nbFFp]);
                  }
                }
              }
            }
          }
        }
        
        /***DISPLACEMENt - NONLOCAL*/
        if( getNumNonLocalVariable()>0)
        {
          if( ipvm->getNumberNonLocalVariable()>getNumNonLocalVariable() ) Msg::Error("Your minus material law uses more non local variables than your domain dG3DStiffnessInter::get");
          if( ipvp->getNumberNonLocalVariable()>getNumNonLocalVariable() ) Msg::Error("Your plus material law uses more non local variables than your domain dG3DStiffnessInter::get");
        
          for (int nlk = 0; nlk < ipvp->getNumberNonLocalVariable(); nlk++)
          {
            
            const STensor3  *dsdpp = &(ipvp->getConstRefToDStressDNonLocalVariable()[nlk]);
            const STensor3  *dsdpm = &(ipvm->getConstRefToDStressDNonLocalVariable()[nlk]);
            
            int nbFFmNonlocal = _minusSpace->getNumShapeFunctions(em,3+nlk); 
            int nbFFmTotalLastNonlocal = _minusSpace->getShapeFunctionsIndex(em,3+nlk); 
            int nbFFpNonlocal = _plusSpace->getNumShapeFunctions(ep,3+nlk); 
            int nbFFpTotalLastNonlocal = _plusSpace->getShapeFunctionsIndex(ep,3+nlk);
            
            // Assembly consistency (from derivative of sigma with tilde{p}
            for(int j=0;j<nbFFm;j++)
            {
              for(int k=0;k<3;k++)
              {
                for(int l=0;l<nbFFmNonlocal;l++)
                {
                  for(int q = 0; q< 3; q++)
                  {
                    stiff(j+k*nbFFm,l+nbFFmTotalLastNonlocal) -= dsdpm->operator()(k,q)*(Valsm[l+nbFFmTotalLastNonlocal]*Valsm[j+k*nbFFm]*wJ/2.*nu(q));
                  }
                }
                for(int l=0;l<nbFFpNonlocal;l++)
                {
                  for(int q = 0; q< 3; q++)
                  {
                    stiff(j+k*nbFFm,l+nbFFpTotalLastNonlocal+nbdofm) -= dsdpp->operator()(k,q)*(Valsp[l+nbFFpTotalLastNonlocal]*Valsm[j+k*nbFFm]*wJ/2.*nu(q));
                  }
                }
              }
            }
            for(int j=0;j<nbFFp;j++)
            {
              for(int k=0;k<3;k++)
              {
                for(int l=0;l<nbFFmNonlocal;l++)
                {
                  for(int q = 0; q< 3; q++)
                  {
                    stiff(j+k*nbFFp+nbdofm,l+nbFFmTotalLastNonlocal) += dsdpm->operator()(k,q)*(Valsm[l+nbFFmTotalLastNonlocal]*
                                                                           Valsp[j+k*nbFFp]*wJ/2.*nu(q));
                  }
                }
                for(int l=0;l<nbFFpNonlocal;l++)
                {
                  for(int q = 0; q< 3; q++)
                  {
                    stiff(j+k*nbFFp+nbdofm,l+nbFFpTotalLastNonlocal+nbdofm) += dsdpp->operator()(k,q)*(Valsp[l+nbFFpTotalLastNonlocal]* 
                                                                            Valsp[j+k*nbFFp]*wJ/2.*nu(q));
                  }
                }
              }
            }
          }
        }
        
        /***DISPLACEMENt - EXTRADOF*/
        if (getNumConstitutiveExtraDofDiffusionVariable() > 0) 
        {
          if( ipvm->getNumConstitutiveExtraDofDiffusionVariable()>getNumConstitutiveExtraDofDiffusionVariable() ) 
            Msg::Error("Your material law uses more constitutive extra dof variable than your domain dG3DStiffnessInter::get");
          if( ipvp->getNumConstitutiveExtraDofDiffusionVariable()>getNumConstitutiveExtraDofDiffusionVariable() ) 
            Msg::Error("Your material law uses more constitutive extra dof variable than your domain dG3DStiffnessInter::get");
          if( ipvp->getNumConstitutiveExtraDofDiffusionVariable()!= ipvm->getNumConstitutiveExtraDofDiffusionVariable() ) 
            Msg::Error("Your plus and minus material laws do not use the same number of constitutive extra dof variables dG3DStiffnessInter::get");
            
          
          if((ipvp->getNumConstitutiveExtraDofDiffusionVariable()==2) && getConstitutiveExtraDofDiffusionUseEnergyConjugatedField()) 
          {
            // using fT=1/T, fV=-V/T as unknowns
            for (int extraDOFField = 0; extraDOFField< ipvp->getNumConstitutiveExtraDofDiffusionVariable(); extraDOFField++)
            {
              // coupleing
              const STensor3  *dPdFieldm = &(ipvm->getConstRefTodPdField()[extraDOFField]);
              const STensor3  *dPdFieldp = &(ipvp->getConstRefTodPdField()[extraDOFField]);
              
              int extraDOFField_aux = (extraDOFField == 0)? 1: 0;
              double dFielddEnergyConjugatedFieldp = ipvp->getConstRefTodFielddEnergyConjugatedField()[extraDOFField][extraDOFField];
              double dFielddEnergyConjugatedFieldm = ipvm->getConstRefTodFielddEnergyConjugatedField()[extraDOFField][extraDOFField]; 
              double dFielddEnergyConjugatedField2p = ipvp->getConstRefTodFielddEnergyConjugatedField()[extraDOFField_aux][extraDOFField];
              double dFielddEnergyConjugatedField2m = ipvm->getConstRefTodFielddEnergyConjugatedField()[extraDOFField_aux][extraDOFField];  
              const STensor3& dPdField2p = ipvp->getConstRefTodPdField()[extraDOFField_aux];
              const STensor3& dPdField2m = ipvm->getConstRefTodPdField()[extraDOFField_aux]; 

              int indexField=3+getNumNonLocalVariable()+extraDOFField;
              int nbFFmExtraDof = _minusSpace->getNumShapeFunctions(em,indexField); 
              int nbFFmTotalLastExtraDof = _minusSpace->getShapeFunctionsIndex(em,indexField); 
              int nbFFpExtraDof = _plusSpace->getNumShapeFunctions(ep,indexField); 
              int nbFFpTotalLastExtraDof = _plusSpace->getShapeFunctionsIndex(ep,indexField);

              // Assembly consistency (from derivative of sigma with extra field)
              for(int j=0;j<nbFFm;j++)
              {
                for(int k=0;k<3;k++)
                {
                  for(int l=0;l<nbFFmExtraDof;l++)
                  {
                    for(int q = 0; q< 3; q++)
                    {
                      stiff(j+k*nbFFm,l+nbFFmTotalLastExtraDof) -= (dPdFieldm->operator()(k,q)*dFielddEnergyConjugatedFieldm+dPdField2m(k,q)*
                                                                   dFielddEnergyConjugatedField2m)*(Valsm[l+nbFFmTotalLastExtraDof]*Valsm[j+k*nbFFm]*wJ/2.*nu(q));
                    }
                  }
                  for(int l=0;l<nbFFpExtraDof;l++)
                  {
                    for(int q = 0; q< 3; q++)
                    {
                      stiff(j+k*nbFFm,l+nbFFpTotalLastExtraDof+nbdofm) -= (dPdFieldp->operator()(k,q)*dFielddEnergyConjugatedFieldp+
                                                    dPdField2p(k,q)*dFielddEnergyConjugatedField2p)*(Valsp[l+nbFFpTotalLastExtraDof]*Valsm[j+k*nbFFm]*wJ/2.*nu(q));
                    }
                  }
                }
              }
              for(int j=0;j<nbFFp;j++)
              {
                for(int k=0;k<3;k++)
                {
                  for(int l=0;l<nbFFmExtraDof;l++)
                  {
                    for(int q = 0; q< 3; q++)
                    {
                      stiff(j+k*nbFFp+nbdofm,l+nbFFmTotalLastExtraDof) += (dPdFieldm->operator()(k,q)*dFielddEnergyConjugatedFieldm+dPdField2m(k,q)*
                                                                  dFielddEnergyConjugatedField2m)*(Valsm[l+nbFFmTotalLastExtraDof]*Valsp[j+k*nbFFp]*wJ/2.*nu(q));
                    }
                  }
                  for(int l=0;l<nbFFpExtraDof;l++)
                  {
                    for(int q = 0; q< 3; q++)
                    {
                        stiff(j+k*nbFFp+nbdofm,l+nbFFpTotalLastExtraDof+nbdofm) += (dPdFieldp->operator()(k,q)*dFielddEnergyConjugatedFieldp+dPdField2p(k,q)*
                                                                     dFielddEnergyConjugatedField2p)*(Valsp[l+nbFFpTotalLastExtraDof]*Valsp[j+k*nbFFp]*wJ/2.*nu(q));
                    }
                  }
                }
              }
            }
          }
          else
          {
            for (int extraDOFField = 0; extraDOFField< ipvp->getNumConstitutiveExtraDofDiffusionVariable(); extraDOFField++)
            {
              // coupleing
              const STensor3  *dPdFieldm = &(ipvm->getConstRefTodPdField()[extraDOFField]);
              const STensor3  *dPdFieldp = &(ipvp->getConstRefTodPdField()[extraDOFField]);
              
              int indexField=3+getNumNonLocalVariable()+extraDOFField;
              int nbFFmExtraDof = _minusSpace->getNumShapeFunctions(em,indexField); 
              int nbFFmTotalLastExtraDof = _minusSpace->getShapeFunctionsIndex(em,indexField); 
              int nbFFpExtraDof = _plusSpace->getNumShapeFunctions(ep,indexField); 
              int nbFFpTotalLastExtraDof = _plusSpace->getShapeFunctionsIndex(ep,indexField);

              // Assembly consistency (from derivative of sigma with extra field)
              for(int j=0;j<nbFFm;j++)
              {
                for(int k=0;k<3;k++)
                {
                  for(int l=0;l<nbFFmExtraDof;l++)
                  {
                    for(int q = 0; q< 3; q++)
                    {
                      stiff(j+k*nbFFm,l+nbFFmTotalLastExtraDof) -= dPdFieldm->operator()(k,q)*(Valsm[l+nbFFmTotalLastExtraDof]*Valsm[j+k*nbFFm]*wJ/2.*nu(q));
                    }
                  }
                  for(int l=0;l<nbFFpExtraDof;l++)
                  {
                    for(int q = 0; q< 3; q++)
                    {
                      stiff(j+k*nbFFm,l+nbFFpTotalLastExtraDof+nbdofm) -= dPdFieldp->operator()(k,q)*(Valsp[l+nbFFpTotalLastExtraDof]*Valsm[j+k*nbFFm]*wJ/2.*nu(q));
                    }
                  }
                }
              }
              for(int j=0;j<nbFFp;j++)
              {
                for(int k=0;k<3;k++)
                {
                  for(int l=0;l<nbFFmExtraDof;l++)
                  {
                    for(int q = 0; q< 3; q++)
                    {
                      stiff(j+k*nbFFp+nbdofm,l+nbFFmTotalLastExtraDof) += dPdFieldm->operator()(k,q)*(Valsm[l+nbFFmTotalLastExtraDof]*Valsp[j+k*nbFFp]*wJ/2.*nu(q));
                    }
                  }
                  for(int l=0;l<nbFFpExtraDof;l++)
                  {
                    for(int q = 0; q< 3; q++)
                    {
                      stiff(j+k*nbFFp+nbdofm,l+nbFFpTotalLastExtraDof+nbdofm) += dPdFieldp->operator()(k,q)*(Valsp[l+nbFFpTotalLastExtraDof]*Valsp[j+k*nbFFp]*wJ/2.*nu(q));
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    
    /***
     * NONLOCAL FIELD
     * Note: there is no couling in the nonlocal interface terms with displacement field and extra-Dof
     *       since the nonlocal fluxes depend only on the nonlocal variable !!!! 
     *       The coupling exists when considering the nonlocal length as functions of displacement unknowns or extraDof   
     * */
    if(getNumNonLocalVariable() >0 and getNonLocalContinuity())
    {
      for(int i=0;i<npts; i++)
      {
        // IP
        const IPStateBase *ipsm        = (*vips)[i];
        const dG3DIPVariableBase *ipvm     = static_cast<const dG3DIPVariableBase*>(ipsm->getState(IPStateBase::current));
        const dG3DIPVariableBase *ipvmprev = static_cast<const dG3DIPVariableBase*>(ipsm->getState(IPStateBase::previous));
      
        const IPStateBase *ipsp        = (*vips)[i+npts];
        const dG3DIPVariableBase *ipvp     = static_cast<const dG3DIPVariableBase*>(ipsp->getState(IPStateBase::current));
        const dG3DIPVariableBase *ipvpprev = static_cast<const dG3DIPVariableBase*>(ipsp->getState(IPStateBase::previous));
      
        const SVector3 &referenceOutwardNormalm = ipvm->getConstRefToReferenceOutwardNormal();
        const SVector3 &referenceOutwardNormalp = ipvp->getConstRefToReferenceOutwardNormal();
        const SVector3 &jump = ipvp->getConstRefToJump();
        
        // Weight of Gauss' point i and Jacobian's value at this point
        const double weight = GP[i].weight;
        const double& detJ = ipvm->getJacobianDeterminant(ie,GP[i]);
        const double wJ = detJ* weight;

        const std::vector<TensorialTraits<double>::GradType> &Gradsm = ipvm->gradf(_minusSpace,ie->getElem(0),GPm[i]);
        const std::vector<TensorialTraits<double>::GradType> &Gradsp = ipvp->gradf(_plusSpace,ie->getElem(1),GPp[i]);
        const std::vector<TensorialTraits<double>::ValType> &Valsm = ipvm->f(_minusSpace,ie->getElem(0),GPm[i]);
        const std::vector<TensorialTraits<double>::ValType> &Valsp = ipvp->f(_plusSpace,ie->getElem(1),GPp[i]);


        // possibility of fracture
        bool broken = false;
        if(_mlawMinus->getType() == materialLaw::fracture )
        {
          const FractureCohesive3DIPVariable *ipvwf = static_cast<const FractureCohesive3DIPVariable*>(ipvm); // broken via minus (OK broken on both sides)
          broken = ipvwf->isbroken();
        }        
        
        if(!broken)
        {
          static SVector3 nu;
          nu = referenceOutwardNormalm;
          nu.normalize();
      
          //non local
          if( ipvm->getNumberNonLocalVariable()>getNumNonLocalVariable() ) Msg::Error("Your minus material law uses more non local variables than your domain dG3DStiffnessInter::get");
          if( ipvp->getNumberNonLocalVariable()>getNumNonLocalVariable() ) Msg::Error("Your plus material law uses more non local variables than your domain dG3DStiffnessInter::get");
          
          // NONLOCAL-NONLOCAL          
          const double nbetahs = getNonLocalStabilityParameter()/hs;
          //
          for (int nlk = 0; nlk < ipvp->getNumberNonLocalVariable(); nlk++)
          {
            double epljump = ipvp->getConstRefToNonLocalJump()(nlk);
            static SVector3  gradm, gradp; 
            gradm = ipvm->getConstRefToGradNonLocalVariable()[nlk];
            gradp = ipvp->getConstRefToGradNonLocalVariable()[nlk];
            if (_incrementNonlocalBased)
            {
              epljump -= ipvpprev->getConstRefToNonLocalJump()(nlk);
              gradm -= ipvmprev->getConstRefToGradNonLocalVariable()[nlk];
              gradp -= ipvpprev->getConstRefToGradNonLocalVariable()[nlk];
            }
              
            const STensor3  *cgm = &(ipvm->getConstRefToCharacteristicLengthMatrix(nlk));           
            const STensor3  *cgp = &(ipvp->getConstRefToCharacteristicLengthMatrix(nlk));    

            //
            int nbFFmNonlocal = _minusSpace->getNumShapeFunctions(em,3+nlk); 
            int nbFFmTotalLastNonlocal = _minusSpace->getShapeFunctionsIndex(em,3+nlk); 
            int nbFFpNonlocal = _plusSpace->getNumShapeFunctions(ep,3+nlk); 
            int nbFFpTotalLastNonlocal = _plusSpace->getShapeFunctionsIndex(ep,3+nlk);    
                          
      
            // consistency D\mean{cG grad epl} Depl
            // Assembly consistency
            for(int j=0;j<nbFFmNonlocal;j++)
            {
              for(int l=0;l<nbFFmNonlocal;l++)
              {
                for(int p = 0; p< 3; p++)
                {
                  for(int q = 0; q< 3; q++)
                  {
                    stiff(j+nbFFmTotalLastNonlocal,l+nbFFmTotalLastNonlocal) -= cgm->operator()(p,q)*
                                      (Gradsm[l+nbFFmTotalLastNonlocal][p]*Valsm[j+nbFFmTotalLastNonlocal]*wJ*getNonLocalEqRatio()/2.*nu(q));
                  }
                }
              }
              for(int l=0;l<nbFFpNonlocal;l++)
              {
                for(int p = 0; p< 3; p++)
                {
                  for(int q = 0; q< 3; q++)
                  {
                    stiff(j+nbFFmTotalLastNonlocal,l+nbFFpTotalLastNonlocal+nbdofm) -= cgp->operator()(p,q)*
                                       (Gradsp[l+nbFFpTotalLastNonlocal][p]*Valsm[j+nbFFmTotalLastNonlocal]*wJ*getNonLocalEqRatio()/2.*nu(q));
                  }
                }
              }
            }
            for(int j=0;j<nbFFpNonlocal;j++)
            {
              for(int l=0;l<nbFFmNonlocal;l++)
              {
                for(int p = 0; p< 3; p++)
                {
                  for(int q = 0; q< 3; q++)
                  {
                    stiff(j+nbFFpTotalLastNonlocal+nbdofm,l+nbFFmTotalLastNonlocal) += cgm->operator()(p,q)*
                                       (Gradsm[l+nbFFmTotalLastNonlocal][p]*Valsp[j+nbFFpTotalLastNonlocal]*wJ*getNonLocalEqRatio()/2.*nu(q));
                  }
                }
              }
              for(int l=0;l<nbFFpNonlocal;l++)
              {
                for(int p = 0; p< 3; p++)
                {
                  for(int q = 0; q< 3; q++)
                  {
                    stiff(j+nbFFpTotalLastNonlocal+nbdofm,l+nbFFpTotalLastNonlocal+nbdofm) += cgp->operator()(p,q)*
                                       (Gradsp[l+nbFFpTotalLastNonlocal][p]*Valsp[j+nbFFpTotalLastNonlocal]*wJ*getNonLocalEqRatio()/2.*nu(q));
                  }
                }
              }
            }

            // Stability  N \mean{cg} (N)
            double NMeanCgNBetasc=0.;
            for(int l = 0; l <3; l++)
            {
              for(int n = 0; n <3; n++)
              {
                NMeanCgNBetasc += (cgp->operator()(l,n)+cgm->operator()(l,n))*nu(n)*nbetahs*nu(l)/2.;
              }
            }
            // Assembly stability (if not broken)
            for(int j=0;j<nbFFmNonlocal;j++)
            {
              for(int l=0;l<nbFFmNonlocal;l++)
              {
                stiff(j+nbFFmTotalLastNonlocal,l+nbFFmTotalLastNonlocal) += NMeanCgNBetasc*
                                     (Valsm[j+nbFFmTotalLastNonlocal]*Valsm[l+nbFFmTotalLastNonlocal]*wJ*getNonLocalEqRatio());
              }
              for(int l=0;l<nbFFpNonlocal;l++)
              {
                stiff(j+nbFFmTotalLastNonlocal,l+nbFFpTotalLastNonlocal+nbdofm) -= NMeanCgNBetasc*
                                     (Valsm[j+nbFFmTotalLastNonlocal]*Valsp[l+nbFFpTotalLastNonlocal]*wJ*getNonLocalEqRatio());
              }
            }
            for(int j=0;j<nbFFpNonlocal;j++)
            {
              for(int l=0;l<nbFFmNonlocal;l++)
              {
                stiff(j+nbFFpTotalLastNonlocal+nbdofm,l+nbFFmTotalLastNonlocal) -= NMeanCgNBetasc*
                                     (Valsp[j+nbFFpTotalLastNonlocal]*Valsm[l+nbFFmTotalLastNonlocal]*wJ*getNonLocalEqRatio());
              }
              for(int l=0;l<nbFFpNonlocal;l++)
              {
                stiff(j+nbFFpTotalLastNonlocal+nbdofm,l+nbFFpTotalLastNonlocal+nbdofm) += NMeanCgNBetasc*
                                    (Valsp[j+nbFFpTotalLastNonlocal]*Valsp[l+nbFFpTotalLastNonlocal]*wJ*getNonLocalEqRatio());
              }
            }
            // Assembly (loop on shape function)
            for(int j=0;j<nbFFmNonlocal;j++)
            {
              for(int l=0;l<nbFFmNonlocal;l++)
              {
                for(int p =0; p < 3; p++)
                {
                  for(int q =0; q < 3; q++)
                  {
                    stiff(j+nbFFmTotalLastNonlocal,l+nbFFmTotalLastNonlocal) -= (cgm->operator()(p,q)*
                                Gradsm[j+nbFFmTotalLastNonlocal][q])*(nu(p)*wJ*getNonLocalEqRatio()/2.*Valsm[l+nbFFmTotalLastNonlocal]);
                  }
                }
              }
              for(int l=0;l<nbFFpNonlocal;l++)
              {
                for(int p =0; p < 3; p++)
                {
                  for(int q =0; q < 3; q++)
                  {
                    stiff(j+nbFFmTotalLastNonlocal,l+nbFFpTotalLastNonlocal+nbdofm) += (cgm->operator()(p,q)*
                                Gradsm[j+nbFFmTotalLastNonlocal][q])*(nu(p)*wJ*getNonLocalEqRatio()/2.*Valsp[l+nbFFpTotalLastNonlocal]);
                  }
                }
              }
            }
            for(int j=0;j<nbFFpNonlocal;j++)
            {
              for(int l=0;l<nbFFmNonlocal;l++)
              {
                for(int p =0; p < 3; p++)
                {
                  for(int q =0; q < 3; q++)
                  {
                    stiff(j+nbFFpTotalLastNonlocal+nbdofm,l+nbFFmTotalLastNonlocal) -= (cgp->operator()(p,q)*
                                Gradsp[j+nbFFpTotalLastNonlocal][q])*(nu(p)*wJ*getNonLocalEqRatio()/2.*Valsm[l+nbFFmTotalLastNonlocal]);
                  }
                }
              }
              for(int l=0;l<nbFFpNonlocal;l++)
              {
                for(int p =0; p < 3; p++)
                {
                  for(int q =0; q < 3; q++)
                  {
                    stiff(j+nbFFpTotalLastNonlocal+nbdofm,l+nbFFpTotalLastNonlocal+nbdofm) += (cgp->operator()(p,q)*
                                Gradsp[j+nbFFpTotalLastNonlocal][q])*(nu(p)*wJ*getNonLocalEqRatio()/2.*Valsp[l+nbFFpTotalLastNonlocal]);
                  }
                }
              }
            }
          }
        }
      }
    }
     
    /***
     * EXTRADOF FIELD
     * */
    
    if ((getNumConstitutiveExtraDofDiffusionVariable() > 0) && getConstitutiveExtraDofDiffusionContinuity())
    {    
      for(int i=0;i<npts; i++)
      {
        // IP
        const IPStateBase *ipsm        = (*vips)[i];
        const dG3DIPVariableBase *ipvm     = static_cast<const dG3DIPVariableBase*>(ipsm->getState(IPStateBase::current));

        const IPStateBase *ipsp        = (*vips)[i+npts];
        const dG3DIPVariableBase *ipvp     = static_cast<const dG3DIPVariableBase*>(ipsp->getState(IPStateBase::current));
        
        const SVector3 &referenceOutwardNormalm = ipvm->getConstRefToReferenceOutwardNormal();
        const SVector3 &referenceOutwardNormalp = ipvp->getConstRefToReferenceOutwardNormal();
        const SVector3 &jump = ipvp->getConstRefToJump();

        // Weight of Gauss' point i and Jacobian's value at this point
        const double weight = GP[i].weight;
        const double& detJ = ipvm->getJacobianDeterminant(ie,GP[i]);
        const double wJ = detJ* weight;

        const std::vector<TensorialTraits<double>::GradType> &Gradsm = ipvm->gradf(_minusSpace,ie->getElem(0),GPm[i]);
        const std::vector<TensorialTraits<double>::GradType> &Gradsp = ipvp->gradf(_plusSpace,ie->getElem(1),GPp[i]);
        const std::vector<TensorialTraits<double>::ValType> &Valsm = ipvm->f(_minusSpace,ie->getElem(0),GPm[i]);
        const std::vector<TensorialTraits<double>::ValType> &Valsp = ipvp->f(_plusSpace,ie->getElem(1),GPp[i]);

        // possibility of fracture
        bool broken = false;
        if(_mlawMinus->getType() == materialLaw::fracture )
        {
          const FractureCohesive3DIPVariable *ipvwf = static_cast<const FractureCohesive3DIPVariable*>(ipvm); // broken via minus (OK broken on both sides)
          broken = ipvwf->isbroken();
        }
        
        if(!broken)
        {
          static SVector3 nu;
          nu = referenceOutwardNormalm;
          nu.normalize();

          // constitutive extra dof
          if( ipvm->getNumConstitutiveExtraDofDiffusionVariable()>getNumConstitutiveExtraDofDiffusionVariable() ) 
              Msg::Error("Your material law uses more constitutive extra dof variable than your domain dG3DStiffnessInter::get");
          if( ipvp->getNumConstitutiveExtraDofDiffusionVariable()>getNumConstitutiveExtraDofDiffusionVariable() ) 
              Msg::Error("Your material law uses more constitutive extra dof variable than your domain dG3DStiffnessInter::get");
          if( ipvp->getNumConstitutiveExtraDofDiffusionVariable()!= ipvm->getNumConstitutiveExtraDofDiffusionVariable() ) 
              Msg::Error("Your plus and minus material laws do not use the same number of constitutive extra dof variables dG3DStiffnessInter::get");
          
          // EXTRADOF- ExtraDof
          const double nbetahs = getConstitutiveExtraDofDiffusionStabilityParameter()/hs;
          if (getNumConstitutiveExtraDofDiffusionVariable() == 1)// thermomec
          {
            double fieldJump = ipvp->getConstRefToFieldJump()(0);
            double fieldp = ipvp->getConstRefToField(0);
            double fieldm = ipvm->getConstRefToField(0);
            double dfieldJumpdFieldp = 1.;
            double dfieldJumpdFieldm = -1.;
            double oneOverFieldJump = ipvp->getConstRefToOneOverFieldJump()(0);
            static STensor3 dk0mdFieldm, dk0pdFieldp;
            STensorOperation::zero(dk0mdFieldm);
            STensorOperation::zero(dk0pdFieldp);
            if(getConstitutiveExtraDofDiffusionUseEnergyConjugatedField())
            {
              fieldJump=ipvp->getConstRefToOneOverFieldJump()(0);
            }
            
           
            
            const STensor3  *dqdGradFieldm = &(ipvm->getConstRefTodFluxdGradField()[0][0]);
            const STensor3  *dqdGradFieldp = &(ipvp->getConstRefTodFluxdGradField()[0][0]);
            const SVector3  *dqdFieldm = &(ipvm->getConstRefTodFluxdField()[0][0]);
            const SVector3  *dqdFieldp = &(ipvp->getConstRefTodFluxdField()[0][0]);
            
            static STensor3  k0m, k0p;
            k0m = *(ipvm->getConstRefToLinearK()[0][0]);
            k0p = *(ipvp->getConstRefToLinearK()[0][0]);
            if(getConstitutiveExtraDofDiffusionUseEnergyConjugatedField())
            {
              k0m*=(-1.)*fieldm*fieldm;
              k0p*=(-1.)*fieldp*fieldp;
              dk0mdFieldm=k0m;
              dk0mdFieldm*=(1.)*2./fieldm;//the - is removed
              dk0pdFieldp=k0p;
              dk0pdFieldp*=(1.)*2./fieldp;//the - is removed
            }
            
            int indexField= 3+getNumNonLocalVariable();            
            int nbFFmExtraDof = _minusSpace->getNumShapeFunctions(em,indexField);
            int nbFFmTotalLastExtraDof = _minusSpace->getShapeFunctionsIndex(em,indexField);
            int nbFFpExtraDof = _plusSpace->getNumShapeFunctions(ep,indexField); 
            int nbFFpTotalLastExtraDof = _plusSpace->getShapeFunctionsIndex(ep,indexField);
            
            for(int j=0;j<nbFFmExtraDof;j++)
            {
              for(int l=0;l<nbFFmExtraDof;l++)
              {
                for(int p = 0; p< 3; p++)
                {
                  stiff(j+nbFFmTotalLastExtraDof,l+nbFFmTotalLastExtraDof) -= dqdFieldm->operator()(p)*(Valsm[l+nbFFmTotalLastExtraDof]*
                                                        Valsm[j+nbFFmTotalLastExtraDof]*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.*nu(p));
                  for(int q = 0; q< 3; q++)
                  {
                    stiff(j+nbFFmTotalLastExtraDof,l+nbFFmTotalLastExtraDof) -= dqdGradFieldm->operator()(q,p)*
                                     (Gradsm[l+nbFFmTotalLastExtraDof][p]*Valsm[j+nbFFmTotalLastExtraDof]*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.*nu(q));
                  }
                }
              }
              for(int l=0;l<nbFFpExtraDof;l++)
              {
                for(int p = 0; p< 3; p++)
                {
                  stiff(j+nbFFmTotalLastExtraDof,l+nbFFpTotalLastExtraDof+nbdofm) -= dqdFieldp->operator()(p)*
                               (Valsp[l+nbFFpTotalLastExtraDof]*Valsm[j+nbFFmTotalLastExtraDof]*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.*nu(p));

                  for(int q = 0; q< 3; q++)
                  {
                    stiff(j+nbFFmTotalLastExtraDof,l+nbFFpTotalLastExtraDof+nbdofm) -= dqdGradFieldp->operator()(q,p)*
                                                 (Gradsp[l+nbFFpTotalLastExtraDof][p]*Valsm[j+nbFFmTotalLastExtraDof]*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.*nu(q));

                  }
                }
              }
            }
            for(int j=0;j<nbFFpExtraDof;j++)
            {
              for(int l=0;l<nbFFmExtraDof;l++)
              {
                for(int p = 0; p< 3; p++)
                {
                  stiff(j+nbFFpTotalLastExtraDof+nbdofm,l+nbFFmTotalLastExtraDof) += dqdFieldm->operator()(p)*(Valsm[l+nbFFmTotalLastExtraDof]*
                                Valsp[j+nbFFpTotalLastExtraDof]*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.*nu(p));
                  for(int q = 0; q< 3; q++)
                  {

                      stiff(j+nbFFpTotalLastExtraDof+nbdofm,l+nbFFmTotalLastExtraDof) += dqdGradFieldm->operator()(q,p)*
                                    (Gradsm[l+nbFFmTotalLastExtraDof][p]*Valsp[j+nbFFpTotalLastExtraDof]*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.*nu(q));
                  }
                }
              }
              for(int l=0;l<nbFFpExtraDof;l++)
              {
                for(int p = 0; p< 3; p++)
                {
                  stiff(j+nbFFpTotalLastExtraDof+nbdofm,l+nbFFpTotalLastExtraDof+nbdofm) += dqdFieldp->operator()(p)*
                              (Valsp[l+nbFFpTotalLastExtraDof]*Valsp[j+nbFFpTotalLastExtraDof]*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.*nu(p));
                  for(int q = 0; q< 3; q++)
                  {

                    stiff(j+nbFFpTotalLastExtraDof+nbdofm,l+nbFFpTotalLastExtraDof+nbdofm) += dqdGradFieldp->operator()(q,p)*(Gradsp[l+nbFFpTotalLastExtraDof][p]*
                                Valsp[j+nbFFpTotalLastExtraDof]*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.*nu(q));
                  }
                }
              }
            }
            
            //
            // Stability  N \mean{k0} (N)
            double NMeankNBetasc=0.;
            double dNMeankNBetascdFieldp=0.;
            double dNMeankNBetascdFieldm=0.;

            for(int l = 0; l <3; l++)
            {
              for(int n = 0; n <3; n++)
              {
                NMeankNBetasc += (k0p(l,n)+k0m(l,n))*nu(n)*nbetahs*nu(l)/2.;
                dNMeankNBetascdFieldp+=dk0pdFieldp(l,n)*nu(n)*nbetahs*nu(l)/2.;
                dNMeankNBetascdFieldm+=dk0mdFieldm(l,n)*nu(n)*nbetahs*nu(l)/2.;
              }
            }

            // Assembly stability (if not broken)
            for(int j=0;j<nbFFmExtraDof;j++)
            {
              for(int l=0;l<nbFFmExtraDof;l++)
              {
                stiff(j+nbFFmTotalLastExtraDof,l+nbFFmTotalLastExtraDof) -= dfieldJumpdFieldm*NMeankNBetasc*
                                      (Valsm[j+nbFFmTotalLastExtraDof]*Valsm[l+nbFFmTotalLastExtraDof]*wJ*getConstitutiveExtraDofDiffusionEqRatio());
                if(getConstitutiveExtraDofDiffusionUseEnergyConjugatedField())
                  stiff(j+nbFFmTotalLastExtraDof,l+nbFFmTotalLastExtraDof) -= oneOverFieldJump*dNMeankNBetascdFieldm*
                                      (Valsm[j+nbFFmTotalLastExtraDof]*Valsm[l+nbFFmTotalLastExtraDof]*wJ*getConstitutiveExtraDofDiffusionEqRatio());
              }
              for(int l=0;l<nbFFpExtraDof;l++)
              {
                stiff(j+nbFFmTotalLastExtraDof,l+nbFFpTotalLastExtraDof+nbdofm) -= dfieldJumpdFieldp*NMeankNBetasc*
                                      (Valsm[j+nbFFmTotalLastExtraDof]*Valsp[l+nbFFpTotalLastExtraDof]*wJ*getConstitutiveExtraDofDiffusionEqRatio());
                if(getConstitutiveExtraDofDiffusionUseEnergyConjugatedField())
                  stiff(j+nbFFmTotalLastExtraDof,l+nbFFpTotalLastExtraDof+nbdofm) -= oneOverFieldJump*dNMeankNBetascdFieldp*
                                                  (Valsm[j+nbFFmTotalLastExtraDof]*Valsp[l+nbFFpTotalLastExtraDof]*wJ*getConstitutiveExtraDofDiffusionEqRatio());
              }
            }
            for(int j=0;j<nbFFpExtraDof;j++)
            {
              for(int l=0;l<nbFFmExtraDof;l++)
              {
                stiff(j+nbFFpTotalLastExtraDof+nbdofm,l+nbFFmTotalLastExtraDof) += dfieldJumpdFieldm*NMeankNBetasc*
                                                  (Valsp[j+nbFFpTotalLastExtraDof]*Valsm[l+nbFFmTotalLastExtraDof]*wJ*getConstitutiveExtraDofDiffusionEqRatio());
                if(getConstitutiveExtraDofDiffusionUseEnergyConjugatedField())
                  stiff(j+nbFFpTotalLastExtraDof+nbdofm,l+nbFFmTotalLastExtraDof) += oneOverFieldJump*dNMeankNBetascdFieldm*
                                                  (Valsp[j+nbFFpTotalLastExtraDof]*Valsm[l+nbFFmTotalLastExtraDof]*wJ*getConstitutiveExtraDofDiffusionEqRatio());
              }
              for(int l=0;l<nbFFpExtraDof;l++)
              {
                stiff(j+nbFFpTotalLastExtraDof+nbdofm,l+nbFFpTotalLastExtraDof+nbdofm) += dfieldJumpdFieldp*NMeankNBetasc*
                                                  (Valsp[j+nbFFpTotalLastExtraDof]*Valsp[l+nbFFpTotalLastExtraDof]*wJ*getConstitutiveExtraDofDiffusionEqRatio());
                if(getConstitutiveExtraDofDiffusionUseEnergyConjugatedField())
                  stiff(j+nbFFpTotalLastExtraDof+nbdofm,l+nbFFpTotalLastExtraDof+nbdofm) += oneOverFieldJump*dNMeankNBetascdFieldp*
                                                  (Valsp[j+nbFFpTotalLastExtraDof]*Valsp[l+nbFFpTotalLastExtraDof]*wJ*getConstitutiveExtraDofDiffusionEqRatio());
              }
            }
            // compatibility
            // Assembly (loop on shape function)
            for(int j=0;j<nbFFmExtraDof;j++)
            {
              for(int l=0;l<nbFFmExtraDof;l++)
              {
                for(int p =0; p < 3; p++)
                {
                  for(int q =0; q < 3; q++)
                  {
                    stiff(j+nbFFmTotalLastExtraDof,l+nbFFmTotalLastExtraDof) += dfieldJumpdFieldm*(k0m(p,q)*
                                                  Gradsm[j+nbFFmTotalLastExtraDof][q])*(nu(p)*wJ*getConstitutiveExtraDofDiffusionEqRatio()/2.*Valsm[l+nbFFmTotalLastExtraDof]);
                    if(getConstitutiveExtraDofDiffusionUseEnergyConjugatedField())
                      stiff(j+nbFFmTotalLastExtraDof,l+nbFFmTotalLastExtraDof) += oneOverFieldJump*(dk0mdFieldm(p,q)*
                                                  Gradsm[j+nbFFmTotalLastExtraDof][q])*(nu(p)*wJ*getConstitutiveExtraDofDiffusionEqRatio()/2.*Valsm[l+nbFFmTotalLastExtraDof]);
                  }
                }
              }
              for(int l=0;l<nbFFpExtraDof;l++)
              {
                for(int p =0; p < 3; p++)
                {
                  for(int q =0; q < 3; q++)
                  {
                    stiff(j+nbFFmTotalLastExtraDof,l+nbFFpTotalLastExtraDof+nbdofm) += dfieldJumpdFieldp*(k0m(p,q)*
                                                Gradsm[j+nbFFmTotalLastExtraDof][q])*(nu(p)*wJ*getConstitutiveExtraDofDiffusionEqRatio()/2.*Valsp[l+nbFFpTotalLastExtraDof]);
                  }
                }
              }
            }
            for(int j=0;j<nbFFpExtraDof;j++)
            {
              for(int l=0;l<nbFFmExtraDof;l++)
              {
                for(int p =0; p < 3; p++)
                {
                  for(int q =0; q < 3; q++)
                  {
                    stiff(j+nbFFpTotalLastExtraDof+nbdofm,l+nbFFmTotalLastExtraDof) += dfieldJumpdFieldm*(k0p(p,q)*Gradsp[j+nbFFpTotalLastExtraDof][q])*
                                               (nu(p)*wJ*getConstitutiveExtraDofDiffusionEqRatio()/2.*Valsm[l+nbFFmTotalLastExtraDof]);
                  }
                }
              }
              for(int l=0;l<nbFFpExtraDof;l++)
              {
                for(int p =0; p < 3; p++)
                {
                  for(int q =0; q < 3; q++)
                  {
                    stiff(j+nbFFpTotalLastExtraDof+nbdofm,l+nbFFpTotalLastExtraDof+nbdofm) += dfieldJumpdFieldp*(k0p(p,q)*
                                      Gradsp[j+nbFFpTotalLastExtraDof][q])*(nu(p)*wJ*getConstitutiveExtraDofDiffusionEqRatio()/2.*Valsp[l+nbFFpTotalLastExtraDof]);
                    if(getConstitutiveExtraDofDiffusionUseEnergyConjugatedField())
                      stiff(j+nbFFpTotalLastExtraDof+nbdofm,l+nbFFpTotalLastExtraDof+nbdofm) += oneOverFieldJump*
                                 (dk0pdFieldp(p,q)*Gradsp[j+nbFFpTotalLastExtraDof][q])*(nu(p)*wJ*getConstitutiveExtraDofDiffusionEqRatio()/2.*Valsp[l+nbFFpTotalLastExtraDof]);
                  }
                }
              }
            }
            
          }
          else if (getNumConstitutiveExtraDofDiffusionVariable() == 2 && getConstitutiveExtraDofDiffusionUseEnergyConjugatedField()) // electro-thermomec
          {            
            for (int extraDOFField = 0; extraDOFField< ipvp->getNumConstitutiveExtraDofDiffusionVariable(); extraDOFField++)
            {
              const double fieldp = ipvp->getConstRefToEnergyConjugatedField(extraDOFField);
              const double fieldm = ipvm->getConstRefToEnergyConjugatedField(extraDOFField);
                        
              int indexField=3+getNumNonLocalVariable()+extraDOFField;
              int nbFFmTotalLastExtraDofRow = _minusSpace->getShapeFunctionsIndex(em,indexField);
              int nbFFpTotalLastExtraDofRow = _plusSpace->getShapeFunctionsIndex(ep,indexField);
              int nbFFmExtraDofRow = _minusSpace->getNumShapeFunctions(em,indexField);
              int nbFFpExtraDofRow = _plusSpace->getNumShapeFunctions(ep,indexField);

              
              for (int extraDOFField2 = 0; extraDOFField2< ipvp->getNumConstitutiveExtraDofDiffusionVariable(); extraDOFField2++)
              {
                const STensor3  *dqdGradFieldm = &(ipvm->getConstRefTodFluxdGradField()[extraDOFField][extraDOFField2]);
                const STensor3  *dqdGradFieldp = &(ipvp->getConstRefTodFluxdGradField()[extraDOFField][extraDOFField2]);
                const SVector3  *dqdFieldm = &(ipvm->getConstRefTodFluxdField()[extraDOFField][extraDOFField2]);
                const SVector3  *dqdFieldp = &(ipvp->getConstRefTodFluxdField()[extraDOFField][extraDOFField2]);
                
                static STensor3  k0m, k0p;
                k0m = *(ipvm->getConstRefToLinearK()[extraDOFField][extraDOFField2]);
                k0p = *(ipvp->getConstRefToLinearK()[extraDOFField][extraDOFField2]);
                
                int extraDOFField_aux= (extraDOFField2==0)?1:0;
                
                double fieldJump = ipvp->getConstRefToEnergyConjugatedFieldJump()(extraDOFField2);
                double fieldJump2 = ipvp->getConstRefToEnergyConjugatedFieldJump()(extraDOFField_aux);

                const STensor3& dk0mdFieldm  = ipvm->getConstRefTodLinearKdField()[extraDOFField][extraDOFField2][extraDOFField2];
                const STensor3& dk0pdFieldp  = ipvp->getConstRefTodLinearKdField()[extraDOFField][extraDOFField2][extraDOFField2];
                const STensor3& dk0mdField2m = ipvm->getConstRefTodLinearKdField()[extraDOFField][extraDOFField_aux][extraDOFField2];
                const STensor3& dk0pdField2p = ipvp->getConstRefTodLinearKdField()[extraDOFField][extraDOFField_aux][extraDOFField2];

                const STensor3& dk0mdField_auxm = ipvm->getConstRefTodLinearKdField()[extraDOFField][extraDOFField2][extraDOFField_aux];
                const STensor3& dk0pdField_auxp = ipvp->getConstRefTodLinearKdField()[extraDOFField][extraDOFField2][extraDOFField_aux];
                const STensor3& dk0mdField_aux2m= ipvm->getConstRefTodLinearKdField()[extraDOFField][extraDOFField_aux][extraDOFField_aux];
                const STensor3& dk0pdField_aux2p = ipvp->getConstRefTodLinearKdField()[extraDOFField][extraDOFField_aux][extraDOFField_aux];
                 
                const double& dFielddEnergyConjugatedFieldp = ipvp->getConstRefTodFielddEnergyConjugatedField()[extraDOFField2][extraDOFField2];
                const double& dFielddEnergyConjugatedFieldm = ipvm->getConstRefTodFielddEnergyConjugatedField()[extraDOFField2][extraDOFField2]; 
                const double& dFielddEnergyConjugatedField2p = ipvp->getConstRefTodFielddEnergyConjugatedField()[extraDOFField_aux][extraDOFField2];
                const double& dFielddEnergyConjugatedField2m = ipvm->getConstRefTodFielddEnergyConjugatedField()[extraDOFField_aux][extraDOFField2]; 

                const SVector3& dGradFielddEnergyConjugatedFieldp = ipvp->getConstRefTodGradFielddEnergyConjugatedField()[extraDOFField2][extraDOFField2]; 
                const SVector3& dGradFielddEnergyConjugatedFieldm = ipvm->getConstRefTodGradFielddEnergyConjugatedField()[extraDOFField2][extraDOFField2];
                const STensor3& dGradFielddGradEnergyConjugatedFieldp = ipvp->getConstRefTodGradFielddGradEnergyConjugatedField()[extraDOFField2][extraDOFField2];
                const STensor3& dGradFielddGradEnergyConjugatedFieldm = ipvm->getConstRefTodGradFielddGradEnergyConjugatedField()[extraDOFField2][extraDOFField2]; 

                const SVector3& dGradFielddEnergyConjugatedField2p = ipvp->getConstRefTodGradFielddEnergyConjugatedField()[extraDOFField_aux][extraDOFField2]; 
                const SVector3& dGradFielddEnergyConjugatedField2m = ipvm->getConstRefTodGradFielddEnergyConjugatedField()[extraDOFField_aux][extraDOFField2];
                const STensor3& dGradFielddGradEnergyConjugatedField2p = ipvp->getConstRefTodGradFielddGradEnergyConjugatedField()[extraDOFField_aux][extraDOFField2];
                const STensor3& dGradFielddGradEnergyConjugatedField2m = ipvm->getConstRefTodGradFielddGradEnergyConjugatedField()[extraDOFField_aux][extraDOFField2]; 

                const SVector3& dqdField2m = ipvm->getConstRefTodFluxdField()[extraDOFField][extraDOFField_aux];
                const SVector3& dqdField2p = ipvp->getConstRefTodFluxdField()[extraDOFField][extraDOFField_aux];
                const STensor3& dqdGradField2m = ipvm->getConstRefTodFluxdGradField()[extraDOFField][extraDOFField_aux];
                const STensor3& dqdGradField2p = ipvp->getConstRefTodFluxdGradField()[extraDOFField][extraDOFField_aux];
                
                //
                int indexField2=3+getNumNonLocalVariable()+extraDOFField2;
                int nbFFmTotalLastExtraDofCol = _minusSpace->getShapeFunctionsIndex(em,indexField2);
                int nbFFpTotalLastExtraDofCol = _plusSpace->getShapeFunctionsIndex(ep,indexField2);
                int nbFFmExtraDofCol = _minusSpace->getNumShapeFunctions(em,indexField2);
                int nbFFpExtraDofCol = _plusSpace->getNumShapeFunctions(ep,indexField2);
                for(int j=0;j<nbFFmExtraDofRow;j++)
                {
                  for(int l=0;l<nbFFmExtraDofCol;l++)
                  {
                    for(int p = 0; p< 3; p++)
                    {
                      stiff(j+nbFFmTotalLastExtraDofRow,l+nbFFmTotalLastExtraDofCol) -= (dqdFieldm->operator()(p)*dFielddEnergyConjugatedFieldm+dqdField2m(p)*
                                                           dFielddEnergyConjugatedField2m)*(Valsm[l+nbFFmTotalLastExtraDofCol]*Valsm[j+nbFFmTotalLastExtraDofRow]*
                                                           getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.*nu(p));
                   
                      for(int q = 0; q< 3; q++)
                      {

                          stiff(j+nbFFmTotalLastExtraDofRow,l+nbFFmTotalLastExtraDofCol) -=(dqdGradFieldm->operator()(p,q)*dGradFielddEnergyConjugatedFieldm(q)+dqdGradField2m(p,q)*
                                                        dGradFielddEnergyConjugatedField2m(q))*(Valsm[j+nbFFmTotalLastExtraDofRow]*
                                                        Valsm[l+nbFFmTotalLastExtraDofCol]*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.*nu(p));
                   

                          for(int o = 0; o< 3; o++)
                          {

                            stiff(j+nbFFmTotalLastExtraDofRow,l+nbFFmTotalLastExtraDofCol) -=(dqdGradFieldm->operator()(p,o)*dGradFielddGradEnergyConjugatedFieldm(o,q)+dqdGradField2m(p,o)*
                                                       dGradFielddGradEnergyConjugatedField2m(o,q))*Valsm[j+nbFFmTotalLastExtraDofRow]*
                                                       (Gradsm[l+nbFFmTotalLastExtraDofCol][q]*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.*nu(p));
                          }
                         
                      }
                    }
                  }
                  for(int l=0;l<nbFFpExtraDofCol;l++)
                  {
                    for(int p = 0; p< 3; p++)
                    {
                      
                      stiff(j+nbFFmTotalLastExtraDofRow,l+nbFFpTotalLastExtraDofCol+nbdofm) -= (dqdFieldp->operator()(p)*dFielddEnergyConjugatedFieldp+dqdField2p(p)*
                                    dFielddEnergyConjugatedField2p)*(Valsp[l+nbFFpTotalLastExtraDofCol]*
                                    Valsm[j+nbFFmTotalLastExtraDofRow]*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.*nu(p));

                      for(int q = 0; q< 3; q++)
                      {
                        stiff(j+nbFFmTotalLastExtraDofRow,l+nbFFpTotalLastExtraDofCol+nbdofm) -= (dqdGradFieldp->operator()(p,q)*
                                                     dGradFielddEnergyConjugatedFieldp(q)+dqdGradField2p(p,q)*dGradFielddEnergyConjugatedField2p(q))*
                                                     Valsm[j+nbFFmTotalLastExtraDofRow]*(Valsp[l+nbFFpTotalLastExtraDofCol]*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.*nu(p));

    
                        for(int o = 0; o< 3; o++)
                        {

                          stiff(j+nbFFmTotalLastExtraDofRow,l+nbFFpTotalLastExtraDofCol+nbdofm) -=(dqdGradFieldp->operator()(p,o)*dGradFielddGradEnergyConjugatedFieldp(o,q)+
                                                      dqdGradField2p(p,o)*dGradFielddGradEnergyConjugatedField2p(o,q))*Valsm[j+nbFFmTotalLastExtraDofRow]*
                                                      (Gradsp[l+nbFFpTotalLastExtraDofCol][q]*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.*nu(p));
                        
                        }
                      }
                    }
                  }
                }
                for(int j=0;j<nbFFpExtraDofRow;j++)
                {
                  for(int l=0;l<nbFFmExtraDofCol;l++)
                  {
                    for(int p = 0; p< 3; p++)
                    {
                       stiff(j+nbFFpTotalLastExtraDofRow+nbdofm,l+nbFFmTotalLastExtraDofCol) += (dqdFieldm->operator()(p)*dFielddEnergyConjugatedFieldm+dqdField2m(p)*
                                   dFielddEnergyConjugatedField2m)*
                                   (Valsm[l+nbFFmTotalLastExtraDofCol]*Valsp[j+nbFFpTotalLastExtraDofRow]*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.*nu(p));
                      for(int q = 0; q< 3; q++)
                      {

                        stiff(j+nbFFpTotalLastExtraDofRow+nbdofm,l+nbFFmTotalLastExtraDofCol) +=(dqdGradFieldm->operator()(p,q)*dGradFielddEnergyConjugatedFieldm(q)+
                                        dqdGradField2m(p,q)*dGradFielddEnergyConjugatedField2m(q))*Valsp[j+nbFFpTotalLastExtraDofRow]*
                                        (Valsm[l+nbFFmTotalLastExtraDofCol]*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.*nu(p));
                        for(int o = 0; o< 3; o++)
                        {

                          stiff(j+nbFFpTotalLastExtraDofRow+nbdofm,l+nbFFmTotalLastExtraDofCol) +=(dqdGradFieldm->operator()(p,o)*dGradFielddGradEnergyConjugatedFieldm(o,q)+
                                               dqdGradField2m(p,o)*dGradFielddGradEnergyConjugatedField2m(o,q))*Valsp[j+nbFFpTotalLastExtraDofRow]*
                                               (Gradsm[l+nbFFmTotalLastExtraDofCol][q]*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.*nu(p));
                        }
                        
                      }
                    }
                  }
                  for(int l=0;l<nbFFpExtraDofCol;l++)
                  {
                    for(int p = 0; p< 3; p++)
                    {

                      stiff(j+nbFFpTotalLastExtraDofRow+nbdofm,l+nbFFpTotalLastExtraDofCol+nbdofm) += (dqdFieldp->operator()(p)*dFielddEnergyConjugatedFieldp+
                                                              dqdField2p(p)*dFielddEnergyConjugatedField2p)*
                                  (Valsp[l+nbFFpTotalLastExtraDofCol]*Valsp[j+nbFFpTotalLastExtraDofRow]*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.*nu(p));
                      for(int q = 0; q< 3; q++)
                      {
                        stiff(j+nbFFpTotalLastExtraDofRow+nbdofm,l+nbFFpTotalLastExtraDofCol+nbdofm) += (dqdGradFieldp->operator()(p,q)*dGradFielddEnergyConjugatedFieldp(q)+
                                    dqdGradField2p(p,q)*dGradFielddEnergyConjugatedField2p(q))*(Valsp[j+nbFFpTotalLastExtraDofRow]*
                                    Valsp[l+nbFFpTotalLastExtraDofCol]*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.*nu(p));

                        for(int o = 0; o< 3; o++)
                        {

                          stiff(j+nbFFpTotalLastExtraDofRow+nbdofm,l+nbFFpTotalLastExtraDofCol+nbdofm) += (dqdGradFieldp->operator()(p,o)*dGradFielddGradEnergyConjugatedFieldp(o,q)+
                                   dqdGradField2p(p,o)*dGradFielddGradEnergyConjugatedField2p(o,q))*Valsp[j+nbFFpTotalLastExtraDofRow]*
                                   (Gradsp[l+nbFFpTotalLastExtraDofCol][q]*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.*nu(p));
                        }
                        
                      }
                    }
                  }
                }
                
                //
                // Stability  N \mean{k0} (N)
                double NMeankNBetasc=0.;
                double NMeank2NBetasc=0.;
                double dNMeankNBetascdFieldp=0.;
                double dNMeankNBetascdFieldm=0.;
                double dNMeankNBetascdField2p=0.;
                double dNMeankNBetascdField2m=0.;
                double dNMeankNBetascdField_auxp=0.;
                double dNMeankNBetascdField_auxm=0.;
                double dNMeankNBetascdField_aux2p=0.;
                double dNMeankNBetascdField_aux2m=0.;          

                for(int l = 0; l <3; l++)
                {
                  for(int n = 0; n <3; n++)
                  {
                    NMeankNBetasc  += (k0p(l,n)+k0m(l,n))*nu(n)*nbetahs*nu(l)/2.;

                    dNMeankNBetascdFieldp  += dk0pdFieldp(l,n)*nu(n)*nbetahs*nu(l)/2.;
                    dNMeankNBetascdFieldm  += dk0mdFieldm(l,n)*nu(n)*nbetahs*nu(l)/2.;
                    dNMeankNBetascdField2p += dk0pdField2p(l,n)*nu(n)*nbetahs*nu(l)/2.;
                    dNMeankNBetascdField2m += dk0mdField2m(l,n)*nu(n)*nbetahs*nu(l)/2.; 
                    
                    dNMeankNBetascdField_auxp += dk0pdField_auxp(l,n)*nu(n)*nbetahs*nu(l)/2.;         
                    dNMeankNBetascdField_auxm += dk0mdField_auxm(l,n)*nu(n)*nbetahs*nu(l)/2.; 
                    dNMeankNBetascdField_aux2p += dk0pdField_aux2p(l,n)*nu(n)*nbetahs*nu(l)/2.;         
                    dNMeankNBetascdField_aux2m += dk0mdField_aux2m(l,n)*nu(n)*nbetahs*nu(l)/2.;                 
                  }
                }

                dNMeankNBetascdFieldp *= dFielddEnergyConjugatedFieldp;
                dNMeankNBetascdFieldp += (dNMeankNBetascdField_auxp*dFielddEnergyConjugatedField2p);
                dNMeankNBetascdFieldm *= dFielddEnergyConjugatedFieldm;
                dNMeankNBetascdFieldm += (dNMeankNBetascdField_auxp*dFielddEnergyConjugatedField2m);
                dNMeankNBetascdField2p *= dFielddEnergyConjugatedFieldp;
                dNMeankNBetascdField2p += (dNMeankNBetascdField_aux2p*dFielddEnergyConjugatedField2p);
                dNMeankNBetascdField2m *= dFielddEnergyConjugatedFieldm;
                dNMeankNBetascdField2m += (dNMeankNBetascdField_aux2p*dFielddEnergyConjugatedField2m);

                // Assembly stability (if not broken)
                for(int j=0;j<nbFFmExtraDofRow;j++)
                {
                  for(int l=0;l<nbFFmExtraDofCol;l++)
                  {
                    stiff(j+nbFFmTotalLastExtraDofRow,l+nbFFmTotalLastExtraDofCol) += NMeankNBetasc*(Valsm[j+nbFFmTotalLastExtraDofRow]*Valsm[l+nbFFmTotalLastExtraDofCol]*
                                                                        wJ*getConstitutiveExtraDofDiffusionEqRatio());
                    stiff(j+nbFFmTotalLastExtraDofRow,l+nbFFmTotalLastExtraDofCol) -= (fieldJump*dNMeankNBetascdFieldm+fieldJump2*dNMeankNBetascdField2m)*
                                          (Valsm[j+nbFFmTotalLastExtraDofRow]*Valsm[l+nbFFmTotalLastExtraDofCol]*wJ*getConstitutiveExtraDofDiffusionEqRatio());
                  }
                  for(int l=0;l<nbFFpExtraDofCol;l++)
                  {
                    stiff(j+nbFFmTotalLastExtraDofRow,l+nbFFpTotalLastExtraDofCol+nbdofm) -= NMeankNBetasc*(Valsm[j+nbFFmTotalLastExtraDofRow]*
                                          Valsp[l+nbFFpTotalLastExtraDofCol]*wJ*getConstitutiveExtraDofDiffusionEqRatio());
                    stiff(j+nbFFmTotalLastExtraDofRow,l+nbFFpTotalLastExtraDofCol+nbdofm) -= (fieldJump*dNMeankNBetascdFieldp+fieldJump2*dNMeankNBetascdField2p)*
                                          (Valsm[j+nbFFmTotalLastExtraDofRow]*Valsp[l+nbFFpTotalLastExtraDofCol]*wJ*getConstitutiveExtraDofDiffusionEqRatio());
                  }
                }
                for(int j=0;j<nbFFpExtraDofRow;j++)
                {
                  for(int l=0;l<nbFFmExtraDofCol;l++)
                  {
                    stiff(j+nbFFpTotalLastExtraDofRow+nbdofm,l+nbFFmTotalLastExtraDofCol) -= NMeankNBetasc*
                                                         (Valsp[j+nbFFpTotalLastExtraDofRow]*Valsm[l+nbFFmTotalLastExtraDofCol]*wJ*getConstitutiveExtraDofDiffusionEqRatio());
                    stiff(j+nbFFpTotalLastExtraDofRow+nbdofm,l+nbFFmTotalLastExtraDofCol) += (fieldJump*dNMeankNBetascdFieldm+fieldJump2*dNMeankNBetascdField2m)*
                                                          (Valsp[j+nbFFpTotalLastExtraDofRow]*Valsm[l+nbFFmTotalLastExtraDofCol]*wJ*getConstitutiveExtraDofDiffusionEqRatio());
                  }
                  for(int l=0;l<nbFFpExtraDofCol;l++)
                  {
                    stiff(j+nbFFpTotalLastExtraDofRow+nbdofm,l+nbFFpTotalLastExtraDofCol+nbdofm) += NMeankNBetasc*
                                             (Valsp[j+nbFFpTotalLastExtraDofRow]*Valsp[l+nbFFpTotalLastExtraDofCol]*wJ*getConstitutiveExtraDofDiffusionEqRatio());

                    stiff(j+nbFFpTotalLastExtraDofRow+nbdofm,l+nbFFpTotalLastExtraDofCol+nbdofm) += (fieldJump*dNMeankNBetascdFieldp+fieldJump2*dNMeankNBetascdField2p)*
                                             (Valsp[j+nbFFpTotalLastExtraDofRow]*Valsp[l+nbFFpTotalLastExtraDofCol]*wJ*getConstitutiveExtraDofDiffusionEqRatio());
                  }
                }
     

                // compatibility
                // Assembly (loop on shape function)

                for(int j=0;j<nbFFmExtraDofRow;j++)
                {
                  for(int l=0;l<nbFFmExtraDofCol;l++)
                  {
                    for(int p =0; p < 3; p++)
                    {
                      for(int q =0; q < 3; q++)
                      {
                        stiff(j+nbFFmTotalLastExtraDofRow,l+nbFFmTotalLastExtraDofCol) -= (k0m(p,q)*Gradsm[j+nbFFmTotalLastExtraDofRow][q])*nu(p)*
                                                       Valsm[l+nbFFmTotalLastExtraDofCol]*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.;
                        stiff(j+nbFFmTotalLastExtraDofRow,l+nbFFmTotalLastExtraDofCol) += (fieldJump*(dk0mdFieldm(p,q)*dFielddEnergyConjugatedFieldm+dk0mdField_auxm(p,q)*
                                          dFielddEnergyConjugatedField2m)+fieldJump2*(dk0mdField2m(p,q)*dFielddEnergyConjugatedFieldm+dk0mdField_aux2m(p,q)*
                                          dFielddEnergyConjugatedField2m))*Gradsm[j+nbFFmTotalLastExtraDofRow][q]*nu(p)*Valsm[l+nbFFmTotalLastExtraDofCol]*
                                          getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.;
                      }
                    }
                  }
                  for(int l=0;l<nbFFpExtraDofCol;l++)
                  {
                    for(int p =0; p < 3; p++)
                    {
                      for(int q =0; q < 3; q++)
                      {
                        stiff(j+nbFFmTotalLastExtraDofRow,l+nbFFpTotalLastExtraDofCol+nbdofm) += (k0m(p,q)*Gradsm[j+nbFFmTotalLastExtraDofRow][q])*nu(p)*
                                              Valsp[l+nbFFpTotalLastExtraDofCol]*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.;
                      }
                    }
                  }
                }

                for(int j=0;j<nbFFpExtraDofRow;j++)
                {
                  for(int l=0;l<nbFFmExtraDofCol;l++)
                  {
                    for(int p =0; p < 3; p++)
                    {
                      for(int q =0; q < 3; q++)
                      {
                        stiff(j+nbFFpTotalLastExtraDofRow+nbdofm,l+nbFFmTotalLastExtraDofCol) -= (k0p(p,q)*Gradsp[j+nbFFpTotalLastExtraDofRow][q])*
                                               (nu(p)*Valsm[l+nbFFmTotalLastExtraDofCol])*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.;
                      }
                    }
                  }
                  for(int l=0;l<nbFFpExtraDofCol;l++)
                  {
                    for(int p =0; p < 3; p++)
                    {
                      for(int q =0; q < 3; q++)
                      {
                        stiff(j+nbFFpTotalLastExtraDofRow+nbdofm,l+nbFFpTotalLastExtraDofCol+nbdofm) += (k0p(p,q)*Gradsp[j+nbFFpTotalLastExtraDofRow][q])*nu(p)*
                                                      Valsp[l+nbFFpTotalLastExtraDofCol]*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.;
                        stiff(j+nbFFpTotalLastExtraDofRow+nbdofm,l+nbFFpTotalLastExtraDofCol+nbdofm) += (fieldJump*(dk0pdFieldp(p,q)*
                                                      dFielddEnergyConjugatedFieldp+dk0pdField_auxp(p,q)*dFielddEnergyConjugatedField2p)+
                                                      fieldJump2*(dk0pdField2p(p,q)*dFielddEnergyConjugatedFieldp+
                                                      dk0pdField_aux2p(p,q)*dFielddEnergyConjugatedField2p))*Gradsp[j+nbFFpTotalLastExtraDofRow][q]*
                                                      (nu(p)*getConstitutiveExtraDofDiffusionEqRatio()*(wJ/2.)*Valsp[l+nbFFpTotalLastExtraDofCol]);
                      }
                    }
                  }
                }
               
                
              }
            }
          }
          else if (getNumConstitutiveExtraDofDiffusionVariable() == 2 && !getConstitutiveExtraDofDiffusionUseEnergyConjugatedField()) // electro-thermomec
          {
            for (int extraDOFField = 0; extraDOFField< ipvp->getNumConstitutiveExtraDofDiffusionVariable(); extraDOFField++)
            {
              const double fieldp = ipvp->getConstRefToEnergyConjugatedField(extraDOFField);
              const double fieldm = ipvm->getConstRefToEnergyConjugatedField(extraDOFField);
              
              int indexField=3+getNumNonLocalVariable()+extraDOFField;
              int nbFFmTotalLastExtraDofRow = _minusSpace->getShapeFunctionsIndex(em,indexField);
              int nbFFpTotalLastExtraDofRow = _plusSpace->getShapeFunctionsIndex(ep,indexField);
              int nbFFmExtraDofRow = _minusSpace->getNumShapeFunctions(em,indexField);
              int nbFFpExtraDofRow = _plusSpace->getNumShapeFunctions(ep,indexField);
              for (int extraDOFField2 = 0; extraDOFField2< ipvp->getNumConstitutiveExtraDofDiffusionVariable(); extraDOFField2++)
              {
                const STensor3  *dqdGradFieldm = &(ipvm->getConstRefTodFluxdGradField()[extraDOFField][extraDOFField2]);
                const STensor3  *dqdGradFieldp = &(ipvp->getConstRefTodFluxdGradField()[extraDOFField][extraDOFField2]);
                const SVector3  *dqdFieldm = &(ipvm->getConstRefTodFluxdField()[extraDOFField][extraDOFField2]);
                const SVector3  *dqdFieldp = &(ipvp->getConstRefTodFluxdField()[extraDOFField][extraDOFField2]);
                
                static STensor3  k0m, k0p;
                k0m = *(ipvm->getConstRefToLinearK()[extraDOFField][extraDOFField2]);
                k0p = *(ipvp->getConstRefToLinearK()[extraDOFField][extraDOFField2]);
                
                int extraDOFField_aux= (extraDOFField2==0)? 1:0;
                
                const double fieldJump = ipvp->getConstRefToEnergyConjugatedFieldJump()(extraDOFField2);
                const double fieldJump2 = ipvp->getConstRefToEnergyConjugatedFieldJump()(extraDOFField_aux);

                const STensor3& dk0mdFieldm  = ipvm->getConstRefTodLinearKdField()[extraDOFField][extraDOFField2][extraDOFField2];
                const STensor3& dk0pdFieldp  = ipvp->getConstRefTodLinearKdField()[extraDOFField][extraDOFField2][extraDOFField2];
                const STensor3& dk0mdField2m = ipvm->getConstRefTodLinearKdField()[extraDOFField][extraDOFField_aux][extraDOFField2];
                const STensor3& dk0pdField2p = ipvp->getConstRefTodLinearKdField()[extraDOFField][extraDOFField_aux][extraDOFField2];
                
                double dfieldJumpdFieldp, dfieldJumpdFieldm, dfieldJumpdField2p, dfieldJumpdField2m;
                static STensor3 k02m, k02p;
                if(extraDOFField==extraDOFField2)
                {
                  dfieldJumpdFieldp=ipvp->getConstRefTodEnergyConjugatedFieldJumpdFieldp()[extraDOFField][extraDOFField2];
                  dfieldJumpdFieldm=ipvm->getConstRefTodEnergyConjugatedFieldJumpdFieldm()[extraDOFField][extraDOFField2];
                  dfieldJumpdField2p=ipvp->getConstRefTodEnergyConjugatedFieldJumpdFieldp()[extraDOFField_aux][extraDOFField];
                  dfieldJumpdField2m=ipvm->getConstRefTodEnergyConjugatedFieldJumpdFieldm()[extraDOFField_aux][extraDOFField];

                  k02m = *(ipvm->getConstRefToLinearK()[extraDOFField][extraDOFField_aux]);
                  k02p = *(ipvp->getConstRefToLinearK()[extraDOFField][extraDOFField_aux]);
                }
                else 
                {
                  dfieldJumpdFieldp=ipvp->getConstRefTodEnergyConjugatedFieldJumpdFieldp()[extraDOFField][extraDOFField2];
                  dfieldJumpdFieldm=ipvm->getConstRefTodEnergyConjugatedFieldJumpdFieldm()[extraDOFField][extraDOFField2];
                  dfieldJumpdField2p=ipvp->getConstRefTodEnergyConjugatedFieldJumpdFieldp()[extraDOFField2][extraDOFField2];
                  dfieldJumpdField2m=ipvm->getConstRefTodEnergyConjugatedFieldJumpdFieldm()[extraDOFField2][extraDOFField2];

                  k0m  = *(ipvm->getConstRefToLinearK()[extraDOFField][extraDOFField]);
                  k0p  = *(ipvp->getConstRefToLinearK()[extraDOFField][extraDOFField]);
                  k02m = *(ipvm->getConstRefToLinearK()[extraDOFField][extraDOFField2]);
                  k02p = *(ipvp->getConstRefToLinearK()[extraDOFField][extraDOFField2]);
                }
                //
                int indexField2=3+getNumNonLocalVariable()+extraDOFField2;
                int nbFFmTotalLastExtraDofCol = _minusSpace->getShapeFunctionsIndex(em,indexField2);
                int nbFFpTotalLastExtraDofCol = _plusSpace->getShapeFunctionsIndex(ep,indexField2);
                int nbFFmExtraDofCol = _minusSpace->getNumShapeFunctions(em,indexField2);
                int nbFFpExtraDofCol = _plusSpace->getNumShapeFunctions(ep,indexField2);
                for(int j=0;j<nbFFmExtraDofRow;j++)
                {
                  for(int l=0;l<nbFFmExtraDofCol;l++)
                  {
                    for(int p = 0; p< 3; p++)
                    {

                      stiff(j+nbFFmTotalLastExtraDofRow,l+nbFFmTotalLastExtraDofCol) -= dqdFieldm->operator()(p)*(Valsm[l+nbFFmTotalLastExtraDofCol]*
                                                            Valsm[j+nbFFmTotalLastExtraDofRow]*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.*nu(p));
                      for(int q = 0; q< 3; q++)
                      {

                        stiff(j+nbFFmTotalLastExtraDofRow,l+nbFFmTotalLastExtraDofCol) -= dqdGradFieldm->operator()(q,p)*
                                         (Gradsm[l+nbFFmTotalLastExtraDofCol][p]*Valsm[j+nbFFmTotalLastExtraDofRow]*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.*nu(q));
                      }
                    }
                  }
                  for(int l=0;l<nbFFpExtraDofCol;l++)
                  {
                    for(int p = 0; p< 3; p++)
                    {
                      stiff(j+nbFFmTotalLastExtraDofRow,l+nbFFpTotalLastExtraDofCol+nbdofm) -= dqdFieldp->operator()(p)*
                                   (Valsp[l+nbFFpTotalLastExtraDofCol]*Valsm[j+nbFFmTotalLastExtraDofRow]*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.*nu(p));

                      for(int q = 0; q< 3; q++)
                      {
                        stiff(j+nbFFmTotalLastExtraDofRow,l+nbFFpTotalLastExtraDofCol+nbdofm) -= dqdGradFieldp->operator()(q,p)*
                                                     (Gradsp[l+nbFFpTotalLastExtraDofCol][p]*Valsm[j+nbFFmTotalLastExtraDofRow]*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.*nu(q));
                      }
                    }
                  }
                }
                for(int j=0;j<nbFFpExtraDofRow;j++)
                {
                  for(int l=0;l<nbFFmExtraDofCol;l++)
                  {
                    for(int p = 0; p< 3; p++)
                    {
                      stiff(j+nbFFpTotalLastExtraDofRow+nbdofm,l+nbFFmTotalLastExtraDofCol) += dqdFieldm->operator()(p)*(Valsm[l+nbFFmTotalLastExtraDofCol]*
                                    Valsp[j+nbFFpTotalLastExtraDofRow]*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.*nu(p));
                      for(int q = 0; q< 3; q++)
                      {
                        stiff(j+nbFFpTotalLastExtraDofRow+nbdofm,l+nbFFmTotalLastExtraDofCol) += dqdGradFieldm->operator()(q,p)*
                                        (Gradsm[l+nbFFmTotalLastExtraDofCol][p]*Valsp[j+nbFFpTotalLastExtraDofRow]*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.*nu(q));
                      }
                    }
                  }
                  for(int l=0;l<nbFFpExtraDofCol;l++)
                  {
                    for(int p = 0; p< 3; p++)
                    {
                      stiff(j+nbFFpTotalLastExtraDofRow+nbdofm,l+nbFFpTotalLastExtraDofCol+nbdofm) += dqdFieldp->operator()(p)*
                                  (Valsp[l+nbFFpTotalLastExtraDofCol]*Valsp[j+nbFFpTotalLastExtraDofRow]*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.*nu(p));
                      for(int q = 0; q< 3; q++)
                      {
                        stiff(j+nbFFpTotalLastExtraDofRow+nbdofm,l+nbFFpTotalLastExtraDofCol+nbdofm) += dqdGradFieldp->operator()(q,p)*(Gradsp[l+nbFFpTotalLastExtraDofCol][p]*
                                    Valsp[j+nbFFpTotalLastExtraDofRow]*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.*nu(q));
                      }
                    }
                  }
                }
                //
                // Stability  N \mean{k0} (N)
                double NMeankNBetasc=0.;
                double NMeank2NBetasc=0.;
                double dNMeankNBetascdFieldp=0.;
                double dNMeankNBetascdFieldm=0.;
                double dNMeankNBetascdField2p=0.;
                double dNMeankNBetascdField2m=0.;
                double dNMeankNBetascdField_auxp=0.;
                double dNMeankNBetascdField_auxm=0.;
                double dNMeankNBetascdField_aux2p=0.;
                double dNMeankNBetascdField_aux2m=0.;          

                for(int l = 0; l <3; l++)
                {
                  for(int n = 0; n <3; n++)
                  {
                    NMeankNBetasc  += (k0p(l,n)+k0m(l,n))*nu(n)*nbetahs*nu(l)/2.;
                    NMeank2NBetasc += (k02p(l,n)+k02m(l,n))*nu(n)*nbetahs*nu(l)/2.;

                    dNMeankNBetascdFieldp  += dk0pdFieldp(l,n)*nu(n)*nbetahs*nu(l)/2.;
                    dNMeankNBetascdFieldm  += dk0mdFieldm(l,n)*nu(n)*nbetahs*nu(l)/2.;
                    dNMeankNBetascdField2p += dk0pdField2p(l,n)*nu(n)*nbetahs*nu(l)/2.;
                    dNMeankNBetascdField2m += dk0mdField2m(l,n)*nu(n)*nbetahs*nu(l)/2.; 
                  }
                }
                // Assembly stability (if not broken)
                for(int j=0;j<nbFFmExtraDofRow;j++)
                {
                  for(int l=0;l<nbFFmExtraDofCol;l++)
                  {
                    stiff(j+nbFFmTotalLastExtraDofRow,l+nbFFmTotalLastExtraDofCol) -= (dfieldJumpdFieldm*NMeankNBetasc+dfieldJumpdField2m*NMeank2NBetasc)*
                                          (Valsm[j+nbFFmTotalLastExtraDofRow]*Valsm[l+nbFFmTotalLastExtraDofCol]*wJ*getConstitutiveExtraDofDiffusionEqRatio());
                    stiff(j+nbFFmTotalLastExtraDofRow,l+nbFFmTotalLastExtraDofCol) -= (fieldJump*dNMeankNBetascdFieldm+fieldJump2*dNMeankNBetascdField2m)*
                                          (Valsm[j+nbFFmTotalLastExtraDofRow]*Valsm[l+nbFFmTotalLastExtraDofCol]*wJ*getConstitutiveExtraDofDiffusionEqRatio());
                  }
                  for(int l=0;l<nbFFpExtraDofCol;l++)
                  {
                    stiff(j+nbFFmTotalLastExtraDofRow,l+nbFFpTotalLastExtraDofCol+nbdofm) -= (dfieldJumpdFieldp*NMeankNBetasc+dfieldJumpdField2p*NMeank2NBetasc)*
                                          (Valsm[j+nbFFmTotalLastExtraDofRow]*Valsp[l+nbFFpTotalLastExtraDofCol]*wJ*getConstitutiveExtraDofDiffusionEqRatio());
                    stiff(j+nbFFmTotalLastExtraDofRow,l+nbFFpTotalLastExtraDofCol+nbdofm) -= (fieldJump*dNMeankNBetascdFieldp+fieldJump2*dNMeankNBetascdField2p)*
                                          (Valsm[j+nbFFmTotalLastExtraDofRow]*Valsp[l+nbFFpTotalLastExtraDofCol]*wJ*getConstitutiveExtraDofDiffusionEqRatio());
                  }
                }
                for(int j=0;j<nbFFpExtraDofRow;j++)
                {
                  for(int l=0;l<nbFFmExtraDofCol;l++)
                  {
                    stiff(j+nbFFpTotalLastExtraDofRow+nbdofm,l+nbFFmTotalLastExtraDofCol) += (dfieldJumpdFieldm*NMeankNBetasc+dfieldJumpdField2m*NMeank2NBetasc)*
                                                          (Valsp[j+nbFFpTotalLastExtraDofRow]*Valsm[l+nbFFmTotalLastExtraDofCol]*wJ*getConstitutiveExtraDofDiffusionEqRatio());
                    stiff(j+nbFFpTotalLastExtraDofRow+nbdofm,l+nbFFmTotalLastExtraDofCol) += (fieldJump*dNMeankNBetascdFieldm+fieldJump2*dNMeankNBetascdField2m)*
                                                          (Valsp[j+nbFFpTotalLastExtraDofRow]*Valsm[l+nbFFmTotalLastExtraDofCol]*wJ*getConstitutiveExtraDofDiffusionEqRatio());
                  }
                  for(int l=0;l<nbFFpExtraDofCol;l++)
                  {
                    stiff(j+nbFFpTotalLastExtraDofRow+nbdofm,l+nbFFpTotalLastExtraDofCol+nbdofm) += (dfieldJumpdFieldp*NMeankNBetasc+dfieldJumpdField2p*NMeank2NBetasc)*
                                             (Valsp[j+nbFFpTotalLastExtraDofRow]*Valsp[l+nbFFpTotalLastExtraDofCol]*wJ*getConstitutiveExtraDofDiffusionEqRatio());
                    stiff(j+nbFFpTotalLastExtraDofRow+nbdofm,l+nbFFpTotalLastExtraDofCol+nbdofm) += (fieldJump*dNMeankNBetascdFieldp+fieldJump2*dNMeankNBetascdField2p)*
                                             (Valsp[j+nbFFpTotalLastExtraDofRow]*Valsp[l+nbFFpTotalLastExtraDofCol]*wJ*getConstitutiveExtraDofDiffusionEqRatio());
                  }
                }
     

                
                // compatibility
                // Assembly (loop on shape function)

                for(int j=0;j<nbFFmExtraDofRow;j++)
                {
                  for(int l=0;l<nbFFmExtraDofCol;l++)
                  {
                    for(int p =0; p < 3; p++)
                    {
                      for(int q =0; q < 3; q++)
                      {
                        stiff(j+nbFFmTotalLastExtraDofRow,l+nbFFmTotalLastExtraDofCol) += ((k0m(p,q)*Gradsm[j+nbFFmTotalLastExtraDofRow][q])*nu(p)*Valsm[l+nbFFmTotalLastExtraDofCol]*
                                          dfieldJumpdFieldm*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.)+((k02m(p,q)*Gradsm[j+nbFFmTotalLastExtraDofRow][q])*
                                          nu(p)*Valsm[l+nbFFmTotalLastExtraDofCol]*dfieldJumpdField2m*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.);

                        stiff(j+nbFFmTotalLastExtraDofRow,l+nbFFmTotalLastExtraDofCol) += (fieldJump*(dk0mdFieldm(p,q)*Gradsm[j+nbFFmTotalLastExtraDofRow][q])*nu(p)*
                                          Valsm[l+nbFFmTotalLastExtraDofCol]*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.)+
                                          (fieldJump2*(dk0mdField2m(p,q)*Gradsm[j+nbFFmTotalLastExtraDofRow][q])*nu(p)*
                                          Valsm[l+nbFFmTotalLastExtraDofCol]*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.);
                      }
                    }
                  }
                  for(int l=0;l<nbFFpExtraDofCol;l++)
                  {
                    for(int p =0; p < 3; p++)
                    {
                      for(int q =0; q < 3; q++)
                      {
                        stiff(j+nbFFmTotalLastExtraDofRow,l+nbFFpTotalLastExtraDofCol+nbdofm) += ((k0m(p,q)*Gradsm[j+nbFFmTotalLastExtraDofRow][q])*nu(p)*
                                              Valsp[l+nbFFpTotalLastExtraDofCol]*dfieldJumpdFieldp*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.)+
                                              ((k02m(p,q)*Gradsm[j+nbFFmTotalLastExtraDofRow][q])*nu(p)*Valsp[l+nbFFpTotalLastExtraDofCol]*
                                              dfieldJumpdField2p*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.);
                      }
                    }
                  }
                }

                for(int j=0;j<nbFFpExtraDofRow;j++)
                {
                  for(int l=0;l<nbFFmExtraDofCol;l++)
                  {
                    for(int p =0; p < 3; p++)
                    {
                      for(int q =0; q < 3; q++)
                      {
                        stiff(j+nbFFpTotalLastExtraDofRow+nbdofm,l+nbFFmTotalLastExtraDofCol) += ((k0p(p,q)*Gradsp[j+nbFFpTotalLastExtraDofRow][q])*nu(p)*
                                               Valsm[l+nbFFmTotalLastExtraDofCol]*dfieldJumpdFieldm*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.)+
                                               ((k02p(p,q)*Gradsp[j+nbFFpTotalLastExtraDofRow][q])*nu(p)*Valsm[l+nbFFmTotalLastExtraDofCol]*
                                               dfieldJumpdField2m*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.);
                      }
                    }
                  }
                  for(int l=0;l<nbFFpExtraDofCol;l++)
                  {
                    for(int p =0; p < 3; p++)
                    {
                      for(int q =0; q < 3; q++)
                      {
                        stiff(j+nbFFpTotalLastExtraDofRow+nbdofm,l+nbFFpTotalLastExtraDofCol+nbdofm) += ((k0p(p,q)*Gradsp[j+nbFFpTotalLastExtraDofRow][q])*nu(p)*
                                                       Valsp[l+nbFFpTotalLastExtraDofCol]*dfieldJumpdFieldp*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.)+
                                                      ((k02p(p,q)*Gradsp[j+nbFFpTotalLastExtraDofRow][q])*nu(p)*Valsp[l+nbFFpTotalLastExtraDofCol]*
                                                      dfieldJumpdField2p*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.);
                        stiff(j+nbFFpTotalLastExtraDofRow+nbdofm,l+nbFFpTotalLastExtraDofCol+nbdofm) += (fieldJump*(dk0pdFieldp(p,q)*Gradsp[j+nbFFpTotalLastExtraDofRow][q])*nu(p)*
                                                      Valsp[l+nbFFpTotalLastExtraDofCol]*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.)+
                                                      (fieldJump2*(dk0pdField2p(p,q)*Gradsp[j+nbFFpTotalLastExtraDofRow][q])*nu(p)*
                                                      Valsp[l+nbFFpTotalLastExtraDofCol]*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.);
                      }
                    }
                  }
                }
               
                
                
              }
            }
          }
          else
          {
            Msg::Error("extraDof with more than 2 extra dofs not implemented");
          }
        
          // EXTRADOF - displacement
          
          if (getNumConstitutiveExtraDofDiffusionVariable() == 1)
          {
            // not
          }
          else if (getNumConstitutiveExtraDofDiffusionVariable() == 2)
          {            
            for (int extraDOFField = 0; extraDOFField< ipvp->getNumConstitutiveExtraDofDiffusionVariable(); extraDOFField++)
            {
                        
              int indexField=3+getNumNonLocalVariable()+extraDOFField;
              int nbFFmTotalLastExtraDofRow = _minusSpace->getShapeFunctionsIndex(em,indexField);
              int nbFFpTotalLastExtraDofRow = _plusSpace->getShapeFunctionsIndex(ep,indexField);
              int nbFFmExtraDofRow = _minusSpace->getNumShapeFunctions(em,indexField);
              int nbFFpExtraDofRow = _plusSpace->getNumShapeFunctions(ep,indexField);
              int nbFFmDispCol = _minusSpace->getNumShapeFunctions(em,0);// same for 0, 1, 2
              int nbFFpDispCol = _plusSpace->getNumShapeFunctions(ep,0);
              
              for (int extraDOFField2 = 0; extraDOFField2< ipvp->getNumConstitutiveExtraDofDiffusionVariable(); extraDOFField2++)
              {
                double fieldJump = ipvp->getConstRefToEnergyConjugatedFieldJump()(extraDOFField2);
                const STensor43& dk0mdFm  = ipvm->getConstRefTodLinearKdF()[extraDOFField][extraDOFField2];
                const STensor43& dk0pdFp  = ipvp->getConstRefTodLinearKdF()[extraDOFField][extraDOFField2];
                
                static STensor3 dNMeankNBetascdFm, dNMeankNBetascdFp;
                STensorOperation::zero(dNMeankNBetascdFm); 
                STensorOperation::zero(dNMeankNBetascdFp);

                for(int l = 0; l <3; l++)
                {
                  for(int n = 0; n <3; n++)
                  {                    
                    for(int j = 0; j<3; j++)
                      {
                       for(int p = 0; p<3; p++)
                        {
                          dNMeankNBetascdFm(j,p)+=dk0mdFm(l,n,j,p)*nu(j)*nbetahs*nu(p)/2.;
                          dNMeankNBetascdFp(j,p)+=dk0pdFp(l,n,j,p)*nu(j)*nbetahs*nu(p)/2.;
                       }                
                     }
                  }
                }
                
                int indexField2=3+getNumNonLocalVariable()+extraDOFField2;
                for(int j=0;j<nbFFmExtraDofRow;j++)
                {
                  for(int k=0;k<3;k++)
                  {
                    for(int l=0;l<nbFFmDispCol;l++)
                    {
                      if(useBarF)
                      {
                        for(int ii=0; ii<3; ii++)
                        {
                          for(int p=0; p<3; p++)
                          {
                            stiff(j+nbFFmTotalLastExtraDofRow,l+k*nbFFmDispCol) -= fieldJump*dNMeankNBetascdFm(ii,p)*dBarFdum[i][l](ii,p,k)*Valsm[j+nbFFmTotalLastExtraDofRow]*
                                                               wJ*getConstitutiveExtraDofDiffusionEqRatio();
                          }
                        }
                      }
                      else
                      {
                        for(int p=0;p<3;p++)
                        {
                          stiff(j+nbFFmTotalLastExtraDofRow,l+k*nbFFmDispCol) -= fieldJump*dNMeankNBetascdFm(k,p)*Gradsm[l+k*nbFFmDispCol][p]*Valsm[j+nbFFmTotalLastExtraDofRow]*
                                                               wJ*getConstitutiveExtraDofDiffusionEqRatio();
                        }
                      }
                    }
                    for(int l=0;l<nbFFpDispCol;l++)
                    {
                      if(useBarF)
                      {
                        for(int ii=0; ii<3; ii++)
                        {
                          for(int p=0; p<3; p++)
                          {
                           stiff(j+nbFFmTotalLastExtraDofRow,l+k*nbFFpDispCol+nbdofm) -= fieldJump*dNMeankNBetascdFp(ii,p)*dBarFdup[i][l](ii,p,k)*Valsm[j+nbFFmTotalLastExtraDofRow]*
                                                                  wJ*getConstitutiveExtraDofDiffusionEqRatio();
                          }
                        }
                      }
                      else
                      {
                        for(int p=0;p<3;p++)
                        {
                          stiff(j+nbFFmTotalLastExtraDofRow,l+k*nbFFpDispCol+nbdofm) -= fieldJump*dNMeankNBetascdFp(k,p)*Gradsp[l+k*nbFFpDispCol][p]*Valsm[j+nbFFmTotalLastExtraDofRow]*
                                                                 wJ*getConstitutiveExtraDofDiffusionEqRatio();
                        }
                      }
                    }
                  } 
                }

                for(int j=0;j<nbFFpExtraDofRow;j++)
                {
                  for(int k=0;k<3;k++)
                  {
                    for(int l=0;l<nbFFmDispCol;l++)
                    {
                      if(useBarF)
                      {
                        for(int ii=0; ii<3; ii++)
                        {
                          for(int p=0; p<3; p++)
                          {
                            stiff(j+nbFFpTotalLastExtraDofRow+nbdofm,l+k*nbFFmDispCol) += fieldJump*dNMeankNBetascdFm(ii,p)*dBarFdum[i][l](ii,p,k)*Valsp[j+nbFFpTotalLastExtraDofRow]*
                                                                        wJ*getConstitutiveExtraDofDiffusionEqRatio();
                          }
                        }
                      }
                      else
                      {
                        for(int p=0;p<3;p++)
                        {
                          stiff(j+nbFFpTotalLastExtraDofRow+nbdofm,l+k*nbFFmDispCol) += fieldJump*dNMeankNBetascdFm(k,p)*Gradsm[l+k*nbFFmDispCol][p]*Valsp[j+nbFFpTotalLastExtraDofRow]*
                                                                        wJ*getConstitutiveExtraDofDiffusionEqRatio();
                        }
                      }
                    }

                    for(int l=0;l<nbFFpDispCol;l++)
                    {
                      if(useBarF)
                      {
                        for(int ii=0; ii<3; ii++)
                        {
                          for(int p=0; p<3; p++)
                          {
                            stiff(j+nbFFpTotalLastExtraDofRow+nbdofm,l+k*nbFFpDispCol+nbdofm) +=fieldJump*dNMeankNBetascdFp(ii,p)*dBarFdup[i][l](ii,p,k)*Valsp[j+nbFFpTotalLastExtraDofRow]*
                                                                         wJ*getConstitutiveExtraDofDiffusionEqRatio();
                          }
                        }
                      }
                      else
                      {
                        for(int p=0;p<3;p++)
                        {
                          stiff(j+nbFFpTotalLastExtraDofRow+nbdofm,l+k*nbFFpDispCol+nbdofm) +=fieldJump*dNMeankNBetascdFp(k,p)*Gradsp[l+k*nbFFpDispCol][p]*Valsp[j+nbFFpTotalLastExtraDofRow]*
                                                                         wJ*getConstitutiveExtraDofDiffusionEqRatio();
                        }
                      }
                    }
                  }
                }
                //
                
                for(int j=0;j<nbFFmExtraDofRow;j++)
                {
                  for(int l=0;l<nbFFmDispCol;l++)
                  {
                    for(int m =0; m < 3; m++)
                    {
                      for(int p =0; p < 3; p++)
                      {
                        for(int q =0; q < 3; q++)
                        {
                          if(useBarF)
                          {
                            for(int ii=0; ii<3; ii++)
                            {
                              for(int n=0; n<3; n++)
                              {
                                stiff(j+nbFFmTotalLastExtraDofRow,l+m*nbFFmDispCol) += fieldJump*(dk0mdFm(p,q,ii,n)*
                                          Gradsm[j+nbFFmTotalLastExtraDofRow][q]*dBarFdum[i][l](ii,n,m))*(nu(p)*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.);
                              }
                            }
                          }
                          else
                          {
                            for(int n =0; n< 3; n++)
                            {
                              stiff(j+nbFFmTotalLastExtraDofRow,l+m*nbFFmDispCol) += fieldJump*(dk0mdFm(p,q,m,n)*
                                          Gradsm[j+nbFFmTotalLastExtraDofRow][q]*Gradsm[l+m*nbFFmDispCol][n])*(nu(p)*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.);
                            }
                          }
                        }
                      }
                    }
                  }
                }

                for(int j=0;j<nbFFpExtraDofRow;j++)
                {
                  for(int l=0;l<nbFFpDispCol;l++)
                  {
                    for(int m =0; m < 3; m++)
                    {
                      for(int q =0; q< 3; q++)
                      {
                        for(int p =0; p < 3; p++)
                        {
                          if(useBarF)
                          {
                            for(int ii=0; ii<3; ii++)
                            {
                              for(int n=0; n<3; n++)
                              {
                                stiff(j+nbFFpTotalLastExtraDofRow+nbdofm,l+m*nbFFpDispCol+nbdofm) += fieldJump*(dk0pdFp(p,q,ii,n)*
                                          Gradsp[j+nbFFpTotalLastExtraDofRow][q]*dBarFdup[i][l](ii,n,m))*
                                          (nu(p)*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.);
                              }
                            }
                          }
                          else
                          {
                            for(int n =0; n < 3; n++)
                            {
                              stiff(j+nbFFpTotalLastExtraDofRow+nbdofm,l+m*nbFFpDispCol+nbdofm) += fieldJump*(dk0pdFp(p,q,m,n)*
                                          Gradsp[j+nbFFpTotalLastExtraDofRow][q]*Gradsp[l+m*nbFFpDispCol][n])*
                                          (nu(p)*getConstitutiveExtraDofDiffusionEqRatio()*wJ/2.);
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
          else
          {
            Msg::Error("extraDof with more than 2 extra dofs not implemented");
          }
          
          
          for (int extraDOFField = 0; extraDOFField< ipvp->getNumConstitutiveExtraDofDiffusionVariable(); extraDOFField++)
          {
            const STensor33 *dqdFm = &(ipvm->getConstRefTodFluxdF()[extraDOFField]);
            const STensor33 *dqdFp = &(ipvp->getConstRefTodFluxdF()[extraDOFField]);

            int indexField=3+getNumNonLocalVariable()+extraDOFField;
            int nbFFmTotalLastExtraDofRow = _minusSpace->getShapeFunctionsIndex(em,indexField);
            int nbFFpTotalLastExtraDofRow = _plusSpace->getShapeFunctionsIndex(ep,indexField);
            int nbFFmExtraDofRow = _minusSpace->getNumShapeFunctions(em,indexField);
            int nbFFpExtraDofRow = _plusSpace->getNumShapeFunctions(ep,indexField);
            int nbFFmDispCol = _minusSpace->getNumShapeFunctions(em,0);// same for 0, 1, 2
            int nbFFpDispCol = _plusSpace->getNumShapeFunctions(ep,0);
            //F-related terms
            for(int j=0;j<nbFFmExtraDofRow;j++)
            {
              for(int k=0;k<3;k++)
              {
                for(int l=0;l<nbFFmDispCol;l++)
                {
                  for(int N = 0; N< 3; N++)
                  {
                    if(useBarF)
                    {
                      for(int ii=0; ii<3; ii++)
                      {
                        for(int q=0; q<3; q++)
                        {
                          stiff(j+nbFFmTotalLastExtraDofRow,l+k*nbFFmDispCol) -= dqdFm->operator()(N,ii,q)*(dBarFdum[i][l](ii,q,k)*Valsm[j+nbFFmTotalLastExtraDofRow]*
                                                               wJ*getConstitutiveExtraDofDiffusionEqRatio()/2.*nu(N));
                        }
                      }
                    }
                    else
                    {
                      for(int q = 0; q< 3; q++)
                      {
                        stiff(j+nbFFmTotalLastExtraDofRow,l+k*nbFFmDispCol) -= dqdFm->operator()(N,k,q)*(Gradsm[l+k*nbFFmDispCol][q]*Valsm[j+nbFFmTotalLastExtraDofRow]*
                                                               wJ*getConstitutiveExtraDofDiffusionEqRatio()/2.*nu(N));
                      }
                    }
                  }
                }
                for(int l=0;l<nbFFpDispCol;l++)
                {
                  for(int N = 0; N< 3; N++)
                  {
                    if(useBarF)
                    {
                      for(int ii=0; ii<3; ii++)
                      {
                        for(int q=0; q<3; q++)
                        {
                          stiff(j+nbFFmTotalLastExtraDofRow,l+k*nbFFpDispCol+nbdofm) -= dqdFp->operator()(N,ii,q)*(dBarFdup[i][l](ii,q,k)*Valsm[j+nbFFmTotalLastExtraDofRow]*
                                                               wJ*getConstitutiveExtraDofDiffusionEqRatio()/2.*nu(N));
                        }
                      }
                    }
                    else
                    {
                      for(int q = 0; q< 3; q++)
                      {
                        stiff(j+nbFFmTotalLastExtraDofRow,l+k*nbFFpDispCol+nbdofm) -= dqdFp->operator()(N,k,q)*(Gradsp[l+k*nbFFpDispCol][q]*Valsm[j+nbFFmTotalLastExtraDofRow]*
                                                               wJ*getConstitutiveExtraDofDiffusionEqRatio()/2.*nu(N));
                      }
                    }
                  }
                }
              }
            }
            for(int j=0;j<nbFFpExtraDofRow;j++)
            {
              for(int k=0;k<3;k++)
              {
                for(int l=0;l<nbFFmDispCol;l++)
                {
                  for(int N = 0; N< 3; N++)
                  {
                    if(useBarF)
                    {
                      for(int ii=0; ii<3; ii++)
                      {
                        for(int q=0; q<3; q++)
                        {
                          stiff(j+nbFFpTotalLastExtraDofRow+nbdofm,l+k*nbFFmDispCol) += dqdFm->operator()(N,ii,q)*(dBarFdum[i][l](ii,q,k)*Valsp[j+nbFFpTotalLastExtraDofRow]*
                                                                 wJ*getConstitutiveExtraDofDiffusionEqRatio()/2.*nu(N));
                        }
                      }
                    }
                    else
                    {
                      for(int q = 0; q< 3; q++)
                      {
                        stiff(j+nbFFpTotalLastExtraDofRow+nbdofm,l+k*nbFFmDispCol) += dqdFm->operator()(N,k,q)*(Gradsm[l+k*nbFFmDispCol][q]*Valsp[j+nbFFpTotalLastExtraDofRow]*
                                                               wJ*getConstitutiveExtraDofDiffusionEqRatio()/2.*nu(N));
                      }
                    }
                  }
                }
                for(int l=0;l<nbFFpDispCol;l++)
                {
                  for(int N = 0; N< 3; N++)
                  {
                    if(useBarF)
                    {
                      for(int ii=0; ii<3; ii++)
                      {
                        for(int q=0; q<3; q++)
                        {
                          stiff(j+nbFFpTotalLastExtraDofRow+nbdofm,l+k*nbFFpDispCol+nbdofm) += dqdFp->operator()(N,ii,q)*(dBarFdup[i][l](ii,q,k)*Valsp[j+nbFFpTotalLastExtraDofRow]*
                                                              wJ*getConstitutiveExtraDofDiffusionEqRatio()/2.*nu(N));
                        }
                      }
                    }
                    else
                    {
                      for(int q = 0; q< 3; q++)
                      {
                        stiff(j+nbFFpTotalLastExtraDofRow+nbdofm,l+k*nbFFpDispCol+nbdofm) += dqdFp->operator()(N,k,q)*(Gradsp[l+k*nbFFpDispCol][q]*Valsp[j+nbFFpTotalLastExtraDofRow]*
                                                              wJ*getConstitutiveExtraDofDiffusionEqRatio()/2.*nu(N));
                      }
                    }
                  }
                }
              }
            }
          }
          
          // Extra Dof - nonlocal
          if (getNumNonLocalVariable() > 0)
          {
            // only case when flux depend on nonlocal variables
             
          }
          
          // additional term 
          for (int extraDOFField = 0; extraDOFField< ipvp->getNumConstitutiveExtraDofDiffusionVariable(); extraDOFField++)
          {            
            if(extraDOFField==0)
            {
              double fieldp=0.;
              double fieldm=0.; 

              static STensor3 Stiff_alphadialitationm, Stiff_alphadialitationp,  dStiff_alphadialitationmdFieldm, dStiff_alphadialitationpdFieldp;

              if(extraDOFField==0)
              {
                Stiff_alphadialitationm = *(ipvm->getConstRefToLinearSymmetrizationCoupling()[0]);
                Stiff_alphadialitationp = *(ipvp->getConstRefToLinearSymmetrizationCoupling()[0]);
              }
              else
              {
                STensorOperation::zero(Stiff_alphadialitationm);
                STensorOperation::zero(Stiff_alphadialitationp);
              }

              if(ipvp->getNumConstitutiveExtraDofDiffusionVariable()==1)
              {
                fieldp = ipvp->getConstRefToField(extraDOFField);
                fieldm = ipvm->getConstRefToField(extraDOFField);
                STensorOperation::zero(dStiff_alphadialitationmdFieldm);
                STensorOperation::zero(dStiff_alphadialitationpdFieldp);
                if(getConstitutiveExtraDofDiffusionUseEnergyConjugatedField())
                {
                   Stiff_alphadialitationm*=(-1.)*fieldm*fieldm;
                   Stiff_alphadialitationp*=(-1.)*fieldp*fieldp;
                   dStiff_alphadialitationmdFieldm=Stiff_alphadialitationm;//the - is removed
                   dStiff_alphadialitationmdFieldm*=(1.)*2./fieldm;
                   dStiff_alphadialitationpdFieldp*=Stiff_alphadialitationp;//the - is removed
                   dStiff_alphadialitationpdFieldp*=(1.)*2./fieldp;
                }
              }
              else
              {
                fieldp = ipvp->getConstRefToEnergyConjugatedField(extraDOFField);
                fieldm = ipvm->getConstRefToEnergyConjugatedField(extraDOFField);
                if(extraDOFField==0)
                {
                  Stiff_alphadialitationm*= (-1.)/(fieldm*fieldm);
                  Stiff_alphadialitationp*= (-1.)/(fieldp*fieldp); 
                }
              }

              int indexField=3+getNumNonLocalVariable()+extraDOFField;
              int nbFFmTotalLastExtraDofRow = _minusSpace->getShapeFunctionsIndex(em,indexField);
              int nbFFpTotalLastExtraDofRow = _plusSpace->getShapeFunctionsIndex(ep,indexField);
              int nbFFmExtraDofRow = _minusSpace->getNumShapeFunctions(em,indexField);
              int nbFFpExtraDofRow = _plusSpace->getNumShapeFunctions(ep,indexField);
              int nbFFmDispCol = _minusSpace->getNumShapeFunctions(em,0);// same for 0, 1, 2
              int nbFFpDispCol = _plusSpace->getNumShapeFunctions(ep,0);
            
              double gamma=0.;
              if(ipvp->getNumConstitutiveExtraDofDiffusionVariable()==1)
              {
                for(int j=0;j<nbFFmExtraDofRow;j++)
                {
                  for(int k=0;k<3;k++)
                  {
                    for(int l=0;l<nbFFmDispCol;l++)
                    {
                      for(int p =0; p < 3; p++)
                      {
                        stiff(j+nbFFmTotalLastExtraDofRow,l+k*nbFFmDispCol) -= gamma*(Stiff_alphadialitationm(k,p)*Valsm[j+nbFFmTotalLastExtraDofRow])*(nu(p)*wJ/2.*Valsm[l+k*nbFFmDispCol])*(-1.);
                      }
                    }
                    for(int l=0;l<nbFFpDispCol;l++)
                    {
                      for(int p =0; p < 3; p++)
                      {
                        stiff(j+nbFFmTotalLastExtraDofRow,l+k*nbFFpDispCol+nbdofm) += gamma*(Stiff_alphadialitationm(k,p)*Valsm[j+nbFFmTotalLastExtraDofRow])*(nu(p)*wJ/2.*Valsp[l+k*nbFFpDispCol])*(-1.);
                      }
                    }
                  }
                }
                for(int j=0;j<nbFFpExtraDofRow;j++)
                {
                  for(int k=0;k<3;k++)
                  {
                    for(int l=0;l<nbFFmDispCol;l++)
                    {
                      for(int p =0; p < 3; p++)
                      {
                        stiff(j+nbFFpTotalLastExtraDofRow+nbdofm,l+k*nbFFmDispCol) -= gamma*(Stiff_alphadialitationp(k,p)*Valsp[j+nbFFpTotalLastExtraDofRow])*(nu(p)*wJ/2.*Valsm[l+k*nbFFmDispCol])*(-1.);
                      }
                    }
                    for(int l=0;l<nbFFpDispCol;l++)
                    {
                      for(int p =0; p < 3; p++)
                      {
                        stiff(j+nbFFpTotalLastExtraDofRow+nbdofm,l+k*nbFFpDispCol+nbdofm) += gamma*(Stiff_alphadialitationp(k,p)*Valsp[j+nbFFpTotalLastExtraDofRow])*(nu(p)*wJ/2.*Valsp[l+k*nbFFpDispCol])*(-1.);
                      }
                    }
                  }
                }
               
                if(getConstitutiveExtraDofDiffusionUseEnergyConjugatedField())
                {
                  for(int j=0;j<nbFFmExtraDofRow;j++)
                  {
                    for(int l=0;l<nbFFmExtraDofRow;l++)
                    {
                      for(int k =0; k < 3; k++)
                      {
                        for(int p =0; p < 3; p++)
                        {
                          stiff(j+nbFFmTotalLastExtraDofRow,l+nbFFmTotalLastExtraDofRow) += gamma*jump(k)*
                              (dStiff_alphadialitationmdFieldm(k,p)*Valsm[j+nbFFmTotalLastExtraDofRow])*nu(p)*Valsm[l+nbFFmTotalLastExtraDofRow]*wJ/2.*(-1.);
                        }
                      }
                    }
                  }
                  for(int j=0;j<nbFFpExtraDofRow;j++)
                  {
                    for(int l=0;l<nbFFpExtraDofRow;l++)
                    {
                      for(int k =0; k < 3; k++)
                      {
                        for(int p =0; p< 3; p++)
                        {
                          stiff(j+nbFFpTotalLastExtraDofRow+nbdofm,l+nbFFpTotalLastExtraDofRow+nbdofm) += gamma*jump(k)*
                              (dStiff_alphadialitationpdFieldp(k,p)*Valsp[j+nbFFpTotalLastExtraDofRow])*nu(p)*Valsp[l+nbFFpTotalLastExtraDofRow]*wJ/2.*(-1.);
                         }
                       }
                     }
                   }
                }
              }
              if(ipvp->getNumConstitutiveExtraDofDiffusionVariable()==2)
              {
                double gamma=0.;
                for(int j=0;j<nbFFmExtraDofRow;j++)
                {
                  for(int l=0;l<nbFFmExtraDofRow;l++)
                  {
                    for(int k =0; k < 3; k++)
                    {
                      for(int p =0; p < 3; p++)
                      {
                        stiff(j+nbFFmTotalLastExtraDofRow,l+nbFFmTotalLastExtraDofRow) += gamma*jump(k)*(Stiff_alphadialitationm(k,p)*
                                                    Valsm[j+nbFFmTotalLastExtraDofRow])*(2.*fieldm)*nu(p)*Valsm[l+nbFFmTotalLastExtraDofRow]*wJ/2.*(-1.);
                      }
                    }
                  }
                }

                for(int j=0;j<nbFFpExtraDofRow;j++)
                {
                  for(int l=0;l<nbFFpExtraDofRow;l++)
                  {
                    for(int k =0; k < 3; k++)
                    {
                      for(int p =0; p< 3; p++)
                      {

                        stiff(j+nbFFpTotalLastExtraDofRow+nbdofm,l+nbFFpTotalLastExtraDofRow+nbdofm) += gamma*jump(k)*(Stiff_alphadialitationp(k,p)*
                                                    (2.*fieldp)*Valsp[j+nbFFpTotalLastExtraDofRow])*nu(p)*Valsp[l+nbFFpTotalLastExtraDofRow]*wJ/2.*(-1.);
                      }
                    }
                  }
                }
                for(int j=0;j<nbFFmExtraDofRow;j++)
                {
                  for(int k=0;k<3;k++)
                  {
                    for(int l=0;l<nbFFmDispCol;l++)
                    {
                      for(int p =0; p < 3; p++)
                      {
                          stiff(j+nbFFmTotalLastExtraDofRow,l+k*nbFFmDispCol) -= gamma*(Stiff_alphadialitationm(k,p)*Valsm[j+nbFFmTotalLastExtraDofRow])*(nu(p)*wJ/2.*Valsm[l+k*nbFFmDispCol])*(-1.);
                      }
                    }
                    for(int l=0;l<nbFFpDispCol;l++)
                    {
                      for(int p =0; p < 3; p++)
                      {
                          stiff(j+nbFFmTotalLastExtraDofRow,l+k*nbFFpDispCol+nbdofm) +=  gamma*(Stiff_alphadialitationm(k,p)*Valsm[j+nbFFmTotalLastExtraDofRow])*(nu(p)*wJ/2.*Valsp[l+k*nbFFpDispCol])*(-1.);
                      }
                    }
                  }
                }

                for(int j=0;j<nbFFpExtraDofRow;j++)
                {
                  for(int k=0;k<3;k++)
                  {
                    for(int l=0;l<nbFFmDispCol;l++)
                    {
                      for(int p =0; p < 3; p++)
                      {
                        stiff(j+nbFFpTotalLastExtraDofRow+nbdofm,l+k*nbFFmDispCol) -=  gamma*(Stiff_alphadialitationp(k,p)*Valsp[j+nbFFpTotalLastExtraDofRow])*(nu(p)*wJ/2.*Valsm[l+k*nbFFmDispCol])*(-1.);
                      }
                    }
                    for(int l=0;l<nbFFpDispCol;l++)
                    {
                      for(int p =0; p < 3; p++)
                      {
                          stiff(j+nbFFpTotalLastExtraDofRow+nbdofm,l+k*nbFFpDispCol+nbdofm) +=  gamma*(Stiff_alphadialitationp(k,p)*Valsp[j+nbFFpTotalLastExtraDofRow])*(nu(p)*wJ/2.*Valsp[l+k*nbFFpDispCol])*(-1.);
                      }
                    }
                  }
                }
              }
            }
          }
          // end additional
        }
      }
    }
  }
   //stiff.print("dginter");
};
