//
// C++ Interface: cohesive ipvariable
//
// Description: Class with definition of cohesive ipvarible for dG3D
//
//
// Author:  <L. Noels>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "dG3DCohesiveIPVariable.h"
#include "highOrderTensor.h"
#include "dG3DIPVariable.h"

Cohesive3DIPVariable::Cohesive3DIPVariable(const CohesiveLawBase& law):Cohesive3DIPVariableBase(),
_interfaceForce(0.),_DInterfaceForceDjump(0.),_ujump0 (0.),_cohesiveJump0(0.),_cohesiveJump(0.),_cohesiveNormal(0.),
_qCohesive(NULL)
{
  law.createIPVariable(_qCohesive);
};
Cohesive3DIPVariable::Cohesive3DIPVariable(const Cohesive3DIPVariable& src):Cohesive3DIPVariableBase(src),
_interfaceForce(src._interfaceForce), _DInterfaceForceDjump(src._DInterfaceForceDjump),
_ujump0 (src._ujump0),_cohesiveJump0(src._cohesiveJump0),_cohesiveJump(src._cohesiveJump),
_cohesiveNormal(src._cohesiveNormal){
  _qCohesive = NULL;
  if (src._qCohesive != NULL){
    _qCohesive = dynamic_cast<IPCohesive*>(src._qCohesive->clone());
  }
};
Cohesive3DIPVariable& Cohesive3DIPVariable::operator =(const IPVariable& src){
  Cohesive3DIPVariableBase::operator=(src);
	const Cohesive3DIPVariable* psrc = dynamic_cast<const Cohesive3DIPVariable*>(&src);
	if (psrc!=NULL){
	_interfaceForce = psrc->_interfaceForce;
		_DInterfaceForceDjump = psrc->_DInterfaceForceDjump;
		_ujump0 =  psrc->_ujump0;
    _cohesiveJump0 = psrc->_cohesiveJump0;
    _cohesiveJump = psrc->_cohesiveJump;
    _cohesiveNormal = psrc->_cohesiveNormal;
    if (psrc->_qCohesive != NULL){
      if (_qCohesive!=NULL){
        _qCohesive->operator=(*dynamic_cast<const IPVariable*>(psrc->_qCohesive));
      }
      else{
        _qCohesive = dynamic_cast<IPCohesive*>(psrc->_qCohesive->clone());
      }
    }
	}
  return *this;
};
Cohesive3DIPVariable::~Cohesive3DIPVariable(){
  if (_qCohesive != NULL) {delete _qCohesive; _qCohesive = NULL;};
};

void Cohesive3DIPVariable::restart(){
  IPVariableMechanics::restart();
	restartManager::restart(_interfaceForce);
	restartManager::restart(_DInterfaceForceDjump);
	restartManager::restart(_ujump0);
  restartManager::restart(_cohesiveJump0);
  restartManager::restart(_cohesiveJump);
  restartManager::restart(_cohesiveNormal);
  if (_qCohesive!= NULL)
    _qCohesive->restart();
}


void Cohesive3DIPVariable::initializeFracture(const SVector3 &ujump_, const double Kp_, // compressive penalty
                          const double seff, const double snor, const double tau, const double Gc,
                          const double beta, const bool ift, const STensor3 &cauchy,
                          const double K, const SVector3& normDir_, const SVector3& f0,
                          const IPVariable* bulkIPv)
{
  // set DGjumpt at insertion time
  getRefToDGJump()= ujump_;
  // set cohesive data
  if(fabs(seff)<1.e-18) Msg::Error("Cohesive3DIPVariable::initializeFracture with a seff = 0");
  double deltac  = 2.*Gc/seff;
  double Kn = K;
  double delta0 = seff/Kn;
  
  if (delta0 > 0.005*deltac){
    delta0 = 0.005*deltac;
    Kn = seff/delta0;
    Msg::Warning("small penalty delta0 = %e, deltac = %e, increasing K = from %e to  %e",delta0, deltac,K,Kn);
  }
  // initial data for cohesive data
  if (_qCohesive == NULL){
    Msg::Error("cohesive data is not created Cohesive3DIPVariable::initializeFracture");
  };
  double Kp = std::max(Kp_,Kn);
  _qCohesive->initializeCohesiveData(beta,seff,deltac,Kn, Kp,ift);

	// at insertion time
	// f0 = S0*jump0
  // S0 is initial stiffness tensor
  // S0 = Kn*(betaSq*I + (1-betaSq)*normDirxnormDir)
  //

	double betaSq = beta*beta;
  static SVector3 normDir;
  normDir=normDir_;
  normDir.normalize();

  static STensor3 S0;
  S0=(betaSq);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      S0(i,j) +=   (1.-betaSq)*normDir(i)*normDir(j);
    }
  }
  S0 *= Kn;
  static STensor3 invS0;
  invS0  = S0.invert();

  SVector3& jump0 = getRefToInitialCohesiveJump();
  // compute initial jump
  STensorOperation::zero(jump0);
  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++){
      jump0(i) += invS0(i,j)*f0(j);
    }
  getRefToCohesiveJump() = jump0;
  getRefToCohesiveNormal() =  normDir;
};

ElasticCohesive3DIPVariable::ElasticCohesive3DIPVariable(const CohesiveLawBase& law) : Cohesive3DIPVariable(law)
{
}
ElasticCohesive3DIPVariable::~ElasticCohesive3DIPVariable(){

};

ElasticCohesive3DIPVariable::ElasticCohesive3DIPVariable(const ElasticCohesive3DIPVariable &source): Cohesive3DIPVariable(source)
{

};

ElasticCohesive3DIPVariable& ElasticCohesive3DIPVariable::operator = (const IPVariable &source)
{
  Cohesive3DIPVariable::operator=(source);
  const ElasticCohesive3DIPVariable *src = static_cast<const ElasticCohesive3DIPVariable*>(&source);
  if(src != NULL)
  {

  }
  return *this;
}

void ElasticCohesive3DIPVariable::initializeFracture(const SVector3 &ujump_, const double Kp_, // compressive penalty
                          const double seff, const double snor, const double tau, const double Gc,
                          const double beta, const bool ift, const STensor3 &cauchy,
                          const double K, const SVector3& normDir_, const SVector3& f0,
                          const IPVariable* bulkIPv)
{
  // set DGjumpt at insertion time
  getRefToDGJump()= ujump_;
  // set cohesive data
  _qCohesive->initializeCohesiveData(beta,seff,1e10,K, Kp_,ift);
	double betaSq = beta*beta;
  static SVector3 normDir;
  normDir=normDir_;
  normDir.normalize();
  SVector3& jump0 = getRefToInitialCohesiveJump();
  // compute initial jump
  STensorOperation::zero(jump0);
  getRefToCohesiveJump() = jump0;
  getRefToCohesiveNormal() =  normDir;
};


LinearCohesive3DIPVariable::LinearCohesive3DIPVariable(const CohesiveLawBase& law) : Cohesive3DIPVariable(law)
{
}
LinearCohesive3DIPVariable::~LinearCohesive3DIPVariable(){

};

LinearCohesive3DIPVariable::LinearCohesive3DIPVariable(const LinearCohesive3DIPVariable &source): Cohesive3DIPVariable(source)
{

};

LinearCohesive3DIPVariable& LinearCohesive3DIPVariable::operator = (const IPVariable &source)
{
  Cohesive3DIPVariable::operator=(source);
  const LinearCohesive3DIPVariable *src = static_cast<const LinearCohesive3DIPVariable*>(&source);
  if(src != NULL)
  {

  }
  return *this;
}


BaseTransverseIsotropicLinearCohesive3DIPVariable::BaseTransverseIsotropicLinearCohesive3DIPVariable(const CohesiveLawBase& law) : Cohesive3DIPVariable(law), _cohesiveTangent(0.), _cohesiveBiTangent(0.),
    _cohesiveRefNormal(0.), _cohesiveRefTangent(0.), _cohesiveRefBiTangent(0.)
{

}
BaseTransverseIsotropicLinearCohesive3DIPVariable::~BaseTransverseIsotropicLinearCohesive3DIPVariable(){

};

BaseTransverseIsotropicLinearCohesive3DIPVariable::BaseTransverseIsotropicLinearCohesive3DIPVariable(const BaseTransverseIsotropicLinearCohesive3DIPVariable &source): Cohesive3DIPVariable(source),
                                           _cohesiveTangent(source._cohesiveTangent), _cohesiveBiTangent(source._cohesiveBiTangent),
    _cohesiveRefNormal(source._cohesiveRefNormal), _cohesiveRefTangent(source._cohesiveRefTangent), _cohesiveRefBiTangent(source._cohesiveRefBiTangent)
{

};

BaseTransverseIsotropicLinearCohesive3DIPVariable& BaseTransverseIsotropicLinearCohesive3DIPVariable::operator = (const IPVariable &source)
{
  Cohesive3DIPVariable::operator=(source);
  const BaseTransverseIsotropicLinearCohesive3DIPVariable *src = static_cast<const BaseTransverseIsotropicLinearCohesive3DIPVariable*>(&source);
  if(src != NULL)
  {
        _cohesiveTangent = src->_cohesiveTangent;
        _cohesiveBiTangent = src->_cohesiveBiTangent;
        _cohesiveRefNormal = src->_cohesiveRefNormal;
        _cohesiveRefTangent = src->_cohesiveRefTangent;
        _cohesiveRefBiTangent = src->_cohesiveRefBiTangent;

  }
  return *this;
}

void BaseTransverseIsotropicLinearCohesive3DIPVariable::restart()
{
  Cohesive3DIPVariable::restart();
  restartManager::restart(_cohesiveTangent);
  restartManager::restart(_cohesiveBiTangent);
  restartManager::restart(_cohesiveRefNormal);
  restartManager::restart(_cohesiveRefTangent);
  restartManager::restart(_cohesiveRefBiTangent);
  return;
}

TransverseIsotropicLinearCohesive3DIPVariable::TransverseIsotropicLinearCohesive3DIPVariable(const CohesiveLawBase& law) : BaseTransverseIsotropicLinearCohesive3DIPVariable(law)
{
}
TransverseIsotropicLinearCohesive3DIPVariable::~TransverseIsotropicLinearCohesive3DIPVariable(){

};

TransverseIsotropicLinearCohesive3DIPVariable::TransverseIsotropicLinearCohesive3DIPVariable(const TransverseIsotropicLinearCohesive3DIPVariable &source): 
                                                                            BaseTransverseIsotropicLinearCohesive3DIPVariable(source)
{

};

TransverseIsotropicLinearCohesive3DIPVariable& TransverseIsotropicLinearCohesive3DIPVariable::operator = (const IPVariable &source)
{
  BaseTransverseIsotropicLinearCohesive3DIPVariable::operator=(source);
  const TransverseIsotropicLinearCohesive3DIPVariable *src = static_cast<const TransverseIsotropicLinearCohesive3DIPVariable*>(&source);
  if(src != NULL)
  {

  }
  return *this;
}

void TransverseIsotropicLinearCohesive3DIPVariable::restart()
{
  BaseTransverseIsotropicLinearCohesive3DIPVariable::restart();
  return;
}

TransverseIsoCurvatureLinearCohesive3DIPVariable::TransverseIsoCurvatureLinearCohesive3DIPVariable(const CohesiveLawBase& law, const SVector3 &GaussP, const SVector3 &Centroid, const SVector3 &PointStart1, const SVector3 &PointStart2, const double init_Angle) : BaseTransverseIsotropicLinearCohesive3DIPVariable(law)
{
  if (_qCohesive != NULL) delete _qCohesive;
  _qCohesive = new IPTransverseIsoCurvatureCohesive(GaussP, Centroid, PointStart1, PointStart2, init_Angle);
}
TransverseIsoCurvatureLinearCohesive3DIPVariable::~TransverseIsoCurvatureLinearCohesive3DIPVariable(){

};

TransverseIsoCurvatureLinearCohesive3DIPVariable::TransverseIsoCurvatureLinearCohesive3DIPVariable(const TransverseIsoCurvatureLinearCohesive3DIPVariable &source): 
                                                                            BaseTransverseIsotropicLinearCohesive3DIPVariable(source)
{

};

TransverseIsoCurvatureLinearCohesive3DIPVariable& TransverseIsoCurvatureLinearCohesive3DIPVariable::operator = (const IPVariable &source)
{
  BaseTransverseIsotropicLinearCohesive3DIPVariable::operator=(source);
  const TransverseIsoCurvatureLinearCohesive3DIPVariable *src = static_cast<const TransverseIsoCurvatureLinearCohesive3DIPVariable*>(&source);
  if(src != NULL)
  {

  }
  return *this;
}

void TransverseIsoCurvatureLinearCohesive3DIPVariable::restart()
{
  BaseTransverseIsotropicLinearCohesive3DIPVariable::restart();
  return;
}


NonLocalDamageLinearCohesive3DIPVariable::NonLocalDamageLinearCohesive3DIPVariable(const CohesiveLawBase& law) : LinearCohesive3DIPVariable(law)
{
}

NonLocalDamageLinearCohesive3DIPVariable::NonLocalDamageLinearCohesive3DIPVariable(const NonLocalDamageLinearCohesive3DIPVariable &source): LinearCohesive3DIPVariable(source)
{


}
NonLocalDamageLinearCohesive3DIPVariable& NonLocalDamageLinearCohesive3DIPVariable::operator = (const IPVariable &source)
{
  LinearCohesive3DIPVariable::operator=(source);
  const NonLocalDamageLinearCohesive3DIPVariable *src = static_cast<const NonLocalDamageLinearCohesive3DIPVariable*>(&source);
  if(src != NULL)
  {
  }
  return *this;
}


void NonLocalDamageLinearCohesive3DIPVariable::initializeFracture(const SVector3 &ujump_, const double Kp,
                          const double smax, const double snor, const double tau, const double Gc,
                          const double beta_, const bool ift, const STensor3 &cauchy, const double K,
                          const SVector3& norm, const SVector3& f0,
                          const IPVariable* bulkIPv)
{
  LinearCohesive3DIPVariable::initializeFracture(ujump_, Kp, smax, snor, tau, Gc, beta_, ift, cauchy,K,norm,f0,bulkIPv);
}

void NonLocalDamageLinearCohesive3DIPVariable::restart()
{
  LinearCohesive3DIPVariable::restart();
  return;
}

NonLocalDamageTransverseIsotropicLinearCohesive3DIPVariable::NonLocalDamageTransverseIsotropicLinearCohesive3DIPVariable(const CohesiveLawBase& law) : BaseTransverseIsotropicLinearCohesive3DIPVariable(law)
{
  
}

NonLocalDamageTransverseIsotropicLinearCohesive3DIPVariable::NonLocalDamageTransverseIsotropicLinearCohesive3DIPVariable(const NonLocalDamageTransverseIsotropicLinearCohesive3DIPVariable &source): BaseTransverseIsotropicLinearCohesive3DIPVariable(source)
{
};
NonLocalDamageTransverseIsotropicLinearCohesive3DIPVariable& NonLocalDamageTransverseIsotropicLinearCohesive3DIPVariable::operator = (const IPVariable &source)
{
  BaseTransverseIsotropicLinearCohesive3DIPVariable::operator=(source);
  const NonLocalDamageTransverseIsotropicLinearCohesive3DIPVariable *src = static_cast<const NonLocalDamageTransverseIsotropicLinearCohesive3DIPVariable*>(&source);
  if(src != NULL)
  {
  }
  return *this;
}


void NonLocalDamageTransverseIsotropicLinearCohesive3DIPVariable::initializeFracture(const SVector3 &ujump_, const double Kp,
                          const double smax, const double snor, const double tau, const double Gc,
                          const double beta_, const bool ift, const STensor3 &cauchy, const double K,
                          const SVector3& norm, const SVector3& f0,
                          const IPVariable* bulkIPv)
{
  BaseTransverseIsotropicLinearCohesive3DIPVariable::initializeFracture(ujump_, Kp, smax, snor, tau, Gc, beta_, ift, cauchy,K,norm,f0,bulkIPv);
}

void NonLocalDamageTransverseIsotropicLinearCohesive3DIPVariable::restart()
{
  BaseTransverseIsotropicLinearCohesive3DIPVariable::restart();
  return;
}



DelaminationLinearCohesive3DIPVariable::DelaminationLinearCohesive3DIPVariable(const CohesiveLawBase& law) : LinearCohesive3DIPVariable(law)
{

}

DelaminationLinearCohesive3DIPVariable::DelaminationLinearCohesive3DIPVariable(const DelaminationLinearCohesive3DIPVariable &source): LinearCohesive3DIPVariable(source)
{

}
DelaminationLinearCohesive3DIPVariable& DelaminationLinearCohesive3DIPVariable::operator = (const IPVariable &source)
{
  LinearCohesive3DIPVariable::operator=(source);
  const DelaminationLinearCohesive3DIPVariable *src = static_cast<const DelaminationLinearCohesive3DIPVariable*>(&source);
  if(src != NULL)
  {
  }
  return *this;
}

void DelaminationLinearCohesive3DIPVariable::initializeFracture(const SVector3 &ujump_, const double Kp,
                          const double smax, const double snor, const double tau, const double Gc,
                          const double beta_, const bool ift, const STensor3 &cauchy, const double K,
                          const SVector3& norm, const SVector3& f0,
                          const IPVariable* bulkIPv)
{
  //beta should ne equal to one to make sur the equivalent jump is well computed
  LinearCohesive3DIPVariable::initializeFracture(ujump_, Kp, smax, snor, tau, Gc, 1., ift, cauchy, K, norm, f0,bulkIPv);
}

void DelaminationLinearCohesive3DIPVariable::restart()
{
  LinearCohesive3DIPVariable::restart();
  return;
}


void BulkFollowedCohesive3DIPVariable::restart(){
  Cohesive3DIPVariableBase::restart();
  restartManager::restart(_isTension);
  restartManager::restart(_interfaceForce);
  restartManager::restart(_DInterfaceForceDjump);
  restartManager::restart(_DInterfaceForceDF);
	restartManager::restart(_cohesiveJump);
	restartManager::restart(_ujump0);
  restartManager::restart(_Fb0);
	restartManager::restart(_dFinterfacedF);
	restartManager::restart(_dFinterfacedJump);
	restartManager::restart(_irreversibleEnergy);
	restartManager::restart(_DirreversibleEnergyDjump);
	restartManager::restart(_DirreversibleEnergyDF);
};

void BulkFollowedCohesive3DIPVariable::initializeFracture(const SVector3 &ujump_, const double Kp,
                        const double seff, const double snor, const double tau, const double Gc,
                        const double beta_, const bool ift, const STensor3 & cauchy,
                        const double K, // penalty
                        const SVector3& normal,  // normal
                        const SVector3& f0,  // interface force at insertion time
                        const IPVariable* bulkIPv // bulk data at insertion tim
                        ) {
  const dG3DIPVariableBase* ipdg = dynamic_cast<const dG3DIPVariableBase*>(bulkIPv);
  if (ipdg == NULL)
    Msg::Error("dG3DIPVariableBase must be usued");
  // get Fb0 at the insertion moment
  _Fb0 = this->getBulkDeformationGradient();
  _ujump0  = ipdg->getConstRefToJump();
  STensorOperation::zero(_cohesiveJump);
  _isTension = ift;
	_irreversibleEnergy = 0.;
};

#if defined(HAVE_MPI)
// using in multiscale analysis with MPI
int BulkFollowedCohesive3DIPVariable::getMacroNumberElementDataSendToMicroProblem() const {
  // send Fb0, Fb, ujump0
  return 9+9+3;
};
int BulkFollowedCohesive3DIPVariable::getMicroNumberElementDataSendToMacroProblem() const {
	// // send interfaceForce, dInterfaceForceDJump, dInterfaceForceDF
  //  _isDeleted, _isTension,
	// irrEner, DirrEnergDJUmp, dirrEnergdF 
  return 3+9+27+1+1+1+3+9;
};
void BulkFollowedCohesive3DIPVariable::getMacroDataSendToMicroProblem(double* val) const {
  const STensor3& Fb0 = this->getConstRefToBulkDeformationGradientAtFailureOnset();
  const STensor3& Fb = this->getBulkDeformationGradient();
  const SVector3& ujump0 = this->getConstRefToDGJump();
	
  int row = 0;
  for (int idex=row; idex<row+9; idex++){
    int i,j;
    Tensor23::getIntsFromIndex(idex-row,i,j);
    val[idex] = Fb0(i,j);
  }
  row+= 9;

  for (int idex=row; idex<row+9; idex++){
    int i,j;
    Tensor23::getIntsFromIndex(idex-row,i,j);
    val[idex] = Fb(i,j);
  }
	row+=9;

  for (int idex=row; idex<row+3; idex++){
    int i;
    Tensor13::getIntsFromIndex(idex-row,i);
    val[idex] = ujump0(i);
  }
	row+= 3;
};
void BulkFollowedCohesive3DIPVariable::getMicroDataToMacroProblem(double* val) const {
	const SVector3& T = this->getConstRefToInterfaceForce();
  const STensor3& dTdJump = this->getConstRefToDInterfaceForceDjump();
  const STensor33& dTdF = this->getConstRefToDInterfaceForceDDeformationGradient();
	
  const double& irrEnerg = this->irreversibleEnergy();
	const SVector3& DirrEnergDjump = this->getConstRefToDIrreversibleEnergyDJump();
	const STensor3& DirrEnergDF = this->getConstRefToDIrreversibleEnergyDDeformationGradient();

	int row = 0;
	for (int idex=row; idex < row+3; idex++){
    int i;
    Tensor13::getIntsFromIndex(idex-row,i);
    val[idex] = T(i);
  }
  row += 3;

  for (int idex=row; idex < row+9; idex++){
    int i,j;
    Tensor23::getIntsFromIndex(idex-row,i,j);
    val[idex] = dTdJump(i,j);
  }
  row+=9;

  for (int idex=row; idex < row+27; idex++){
    int i,j,k;
    Tensor33::getIntsFromIndex(idex-row,i,j,k);
    val[idex] = dTdF(i,j,k);
  }
  row+=27;
	
  // convert bool to double and send +1--> true -1 --> false
  double isD = -1.;
  if (_isDeleted) isD = 1.;
  val[row] = isD;
	row += 1;

  double isT = -1;
  if (_isTension) isT = 1.;
  val[row] = isT;
	row += 1;

	val[row] = irrEnerg;
	row += 1;

	for (int index = row; index < row+3; index++){
		int type = index-row;
		int i;
		Tensor13::getIntsFromIndex(type,i);
		val[index] = DirrEnergDjump(i);
	}
	row+= 3;

	for (int index = row; index < row+9; index ++){
		int type = index - row;
		int i,j;
		Tensor23::getIntsFromIndex(type,i,j);
		val[index] = DirrEnergDF(i,j);
	}
	row+= 9;

};
void BulkFollowedCohesive3DIPVariable::setReceivedMacroDataToMicroProblem(const double* val){
  STensor3& Fb0 = this->getRefToBulkDeformationGradientAtFailureOnset();
  SVector3& ujump0 = this->getRefToDGJump();
	
	int row = 0;
  for (int idex=row; idex<row+9; idex++){
    int i,j;
    Tensor23::getIntsFromIndex(idex-row,i,j);
    Fb0(i,j) = val[idex];
  }
	row+= 9;
	
	STensor3 Fb(0.);
	for (int idex=row; idex<row+9; idex++){
    int i,j;
    Tensor23::getIntsFromIndex(idex-row,i,j);
    Fb(i,j) = val[idex];
  }
	row+=9;
	this->setBulkDeformationGradient(Fb);

  for (int idex=row; idex<row+3; idex++){
    int i;
    Tensor13::getIntsFromIndex(idex-row,i);
    ujump0(i) = val[idex];
  }
	row+= 3;
	
};
void BulkFollowedCohesive3DIPVariable::setReceivedMicroDataToMacroProblem(const double* val){
	SVector3& T = this->getRefToInterfaceForce();
  STensor3& dTdJump = this->getRefToDInterfaceForceDjump();
  STensor33& dTdF = this->getRefToDInterfaceForceDDeformationGradient();

	double& irrEnerg = this->getRefToIrreversibleEnergy();
	SVector3& DirrEnergDjump = this->getRefToDIrreversibleEnergyDJump();
	STensor3& DirrEnergDF = this->getRefToDIrreversibleEnergyDDeformationGradient();

  int row = 0;
	for (int idex=row; idex < row+3; idex++){
    int i;
    Tensor13::getIntsFromIndex(idex-row,i);
    T(i) = val[idex];
  }
  row += 3;

  for (int idex=row; idex < row+9; idex++){
    int i,j;
    Tensor23::getIntsFromIndex(idex-row,i,j);
    dTdJump(i,j) = val[idex];
  }
  row+=9;

  for (int idex=row; idex < row+27; idex++){
    int i,j,k;
    Tensor33::getIntsFromIndex(idex-row,i,j,k);
    dTdF(i,j,k) = val[idex];
  }
  row+=27;
	

  // convert bool to double and send +1--> true -1 --> false
  if (val[row] > 0)  _isDeleted = true;
	else _isDeleted = false;
	row += 1;

  if (val[row] > 0) _isTension = true;
	else _isTension = false;
	row += 1;

	irrEnerg = val[row];
	row += 1;
	
	for (int index = row; index < row+3; index++){
		int type = index-row;
		int i;
		Tensor13::getIntsFromIndex(type,i);
		DirrEnergDjump(i) = val[index];
	}
	row+= 3;

	for (int index = row; index < row+9; index ++){
		int type = index - row;
		int i,j;
		Tensor23::getIntsFromIndex(type,i,j);
		DirrEnergDF(i,j) = val[index];
	}
	row+= 9;
	
};
#endif // MPI

void TwoFieldBulkFollowedCohesive3DIPVariable::restart(){
  BulkFollowedCohesive3DIPVariable::restart();
  restartManager::restart(_DcohesiveJumpDF);
	restartManager::restart(_DcohesiveJumpDJump);
	restartManager::restart(_DcohesiveJumpDInJump);
	restartManager::restart(_incompatibleJump0);
	restartManager::restart(_DirreversibleEnergyDInJump);
	restartManager::restart(_dFinterfacedInJump);
};

void TwoFieldBulkFollowedCohesive3DIPVariable::initializeFracture(const SVector3 &ujump_, const double Kp,
                        const double seff, const double snor, const double tau, const double Gc,
                        const double beta_, const bool ift, const STensor3 & cauchy,
                        const double K, // penalty
                        const SVector3& normal,  // normal
                        const SVector3& f0,  // interface force at insertion time
                        const IPVariable* bulkIPv // bulk data at insertion tim
                        ) {
													
	BulkFollowedCohesive3DIPVariable::initializeFracture(ujump_, Kp,seff,snor,tau,Gc,beta_,ift,cauchy,K,normal,f0,bulkIPv);
	
  const dG3DIPVariableBase* ipdg = dynamic_cast<const dG3DIPVariableBase*>(bulkIPv);
  if (ipdg == NULL)
    Msg::Error("dG3DIPVariableBase must be usued");
  // get Fb0 at the insertion moment
  _incompatibleJump0 = ipdg->getConstRefToIncompatibleJump();
};

#if defined(HAVE_MPI)
// using in multiscale analysis with MPI
int TwoFieldBulkFollowedCohesive3DIPVariable::getMacroNumberElementDataSendToMicroProblem() const {
  // send as MultiscaleCohesive3DIPVariable + _incompatibleStrain0
  return BulkFollowedCohesive3DIPVariable::getMacroNumberElementDataSendToMicroProblem()+3;
};
int TwoFieldBulkFollowedCohesive3DIPVariable::getMicroNumberElementDataSendToMacroProblem() const {
	// send as MultiscaleCohesive3DIPVariable  
	// dInterfaceForceDInJump
  // set cjump, dcjumpdJump, dcjumpdF, dcjumpdInJump,
	// dirrEnergdInJump
  return BulkFollowedCohesive3DIPVariable::getMicroNumberElementDataSendToMacroProblem()+9+3+9+27+9+3;
};
void TwoFieldBulkFollowedCohesive3DIPVariable::getMacroDataSendToMicroProblem(double* val) const {
	BulkFollowedCohesive3DIPVariable::getMacroDataSendToMicroProblem(val);
	
  const SVector3& inJump0 = this->getConstRefToIncompatibleJumpAtFailureOnset();

	int row = BulkFollowedCohesive3DIPVariable::getMacroNumberElementDataSendToMicroProblem();
  for (int idex=row; idex<row+3; idex++){
    int i;
    Tensor13::getIntsFromIndex(idex-row,i);
    val[idex] = inJump0(i);
  }
	row+=3;
};
void TwoFieldBulkFollowedCohesive3DIPVariable::getMicroDataToMacroProblem(double* val) const {
	BulkFollowedCohesive3DIPVariable::getMicroDataToMacroProblem(val);
	
  const STensor3& dTdInJump= this->getConstRefToDInterfaceForceDIncompatibleJump();

  const SVector3& cjump = this->getConstRefToCohesiveJump();
  const STensor3& dcjumpdJump = this->getConstRefToDCohesiveJumpDJump();
  const STensor33& dcjumpdF = this->getConstRefToDCohesiveJumpDDeformationGradient();
  const STensor3& dcjumpdInJump = this->getConstRefToDCohesiveJumpDIncompatibleJump();

	const SVector3& DirrEnergDInJump = this->getConstRefToDIrreversibleEnergyDIncompatibleJump();
	
	int row = BulkFollowedCohesive3DIPVariable::getMicroNumberElementDataSendToMacroProblem();

  for (int idex=row; idex < row+9; idex++){
    int i,j;
    Tensor23::getIntsFromIndex(idex-row,i,j);
    val[idex] = dTdInJump(i,j);
  }
  row+=9;

  for (int idex = row; idex<row+3; idex++){
    int i;
    Tensor13::getIntsFromIndex(idex-row,i);
    val[idex] = cjump(i);
  }
	row+= 3;

  for (int idex = row; idex <row+9; idex ++){
    int type = idex-row;
    int i,j;
    Tensor23::getIntsFromIndex(type,i,j);
    val[idex] = dcjumpdJump(i,j);
  };
	row+= 9;

  for (int idex = row; idex <row+27; idex ++){
    int type = idex-row;
    int i,j,k;
    Tensor33::getIntsFromIndex(type,i,j,k);
    val[idex] = dcjumpdF(i,j,k);
  };
	row+= 27;

  for (int idex = row; idex <row+9; idex ++){
    int type = idex-row;
    int i,j;
    Tensor23::getIntsFromIndex(type,i,j);
    val[idex] = dcjumpdInJump(i,j);
  };
	row += 9;
  
	for (int index = row; index < row+3; index++){
		int type = index-row;
		int i;
		Tensor13::getIntsFromIndex(type,i);
		val[index] = DirrEnergDInJump(i);
	}
	row += 3;

};
void TwoFieldBulkFollowedCohesive3DIPVariable::setReceivedMacroDataToMicroProblem(const double* val){
	BulkFollowedCohesive3DIPVariable::setReceivedMacroDataToMicroProblem(val);
	
	SVector3& inJump0 = this->getRefToIncompatibleJumpAtFailureOnset();

	int row = BulkFollowedCohesive3DIPVariable::getMacroNumberElementDataSendToMicroProblem();
  
  for (int idex=row; idex<row+3; idex++){
    int i;
    Tensor13::getIntsFromIndex(idex-row,i);
    inJump0(i) = val[idex];
  }
	row+=3;
};
void TwoFieldBulkFollowedCohesive3DIPVariable::setReceivedMicroDataToMacroProblem(const double* val){
	BulkFollowedCohesive3DIPVariable::setReceivedMicroDataToMacroProblem(val);
	
  STensor3& dTdInStrain = this->getRefToDInterfaceForceDIncompatibleJump();

  SVector3& cjump = this->getRefToCohesiveJump();
  STensor3& dcjumpdJump = this->getRefToDCohesiveJumpDJump();
  STensor33& dcjumpdF = this->getRefToDCohesiveJumpDDeformationGradient();
  STensor3& dcjumpdInJump = this->getRefToDCohesiveJumpDIncompatibleJump();

	SVector3& DirrEnergDInJump = this->getRefToDIrreversibleEnergyDIncompatibleJump();

  int row = BulkFollowedCohesive3DIPVariable::getMicroNumberElementDataSendToMacroProblem();
	

  for (int idex=row; idex < row+9; idex++){
    int i,j;
    Tensor23::getIntsFromIndex(idex-row,i,j);
    dTdInStrain(i,j) = val[idex];
  }
  row+=9;

  for (int idex = row; idex<row+3; idex++){
    int i;
    Tensor13::getIntsFromIndex(idex-row,i);
    cjump(i) = val[idex];
  }
	row+= 3;

  for (int idex = row; idex <row+9; idex ++){
    int type = idex-row;
    int i,j;
    Tensor23::getIntsFromIndex(type,i,j);
    dcjumpdJump(i,j) = val[idex];
  };
	row+= 9;

  for (int idex = row; idex <row+27; idex ++){
    int type = idex-row;
    int i,j,k;
    Tensor33::getIntsFromIndex(type,i,j,k);
    dcjumpdF(i,j,k) = val[idex];
  };
	row+= 27;

  for (int idex = row; idex <row+9; idex ++){
    int type = idex-row;
    int i,j;
    Tensor23::getIntsFromIndex(type,i,j);
    dcjumpdInJump(i,j) = val[idex];
  };
	row += 9;

	for (int index = row; index < row+3; index++){
		int type = index-row;
		int i;
		Tensor13::getIntsFromIndex(type,i);
		DirrEnergDInJump(i) = val[index];
	}
	row += 3;

};
#endif // MPI

