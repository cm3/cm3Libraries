//
// Author: Van Dung NGUYEN, (C) 2016
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef DG3DMULTISCALEMATERIALLAW_H_
#define DG3DMULTISCALEMATERIALLAW_H_

#include "dG3DMultiscaleIPVariable.h"
#include "dG3DMaterialLaw.h"
#include "numericalMaterial.h"
#include "elasticPotential.h"
#include "ipField.h"

class dG3DMultiscaleMaterialLawBase : public dG3DMaterialLaw, public numericalMaterial{
	#ifndef SWIG
	protected:
		virtual void fillElasticStiffness(double & rho, STensor43& L);
    
	public:
		dG3DMultiscaleMaterialLawBase(const int lnum, const int tag);
		dG3DMultiscaleMaterialLawBase(const dG3DMultiscaleMaterialLawBase& src);
    virtual ~dG3DMultiscaleMaterialLawBase(){};
    
    virtual bool withEnergyDissipation() const;
    virtual void setMacroSolver(const nonLinearMechSolver* sv);
    virtual void createIPState(const bool isSolve, IPStateBase* &ips, bool hasBodyForce, const bool* state =NULL,
                                 const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt=0) const=0;
    virtual materialLaw* clone() const = 0;
    #endif //SWIG      
};

class dG3DElasticMultiscaleMaterialLaw : public dG3DMultiscaleMaterialLawBase{
protected:
    #ifndef SWIG
    elasticPotential* _elasticPotential; //
    #endif //SWIG
  public:
		dG3DElasticMultiscaleMaterialLaw(const int lnum, const int tag);
		#ifndef SWIG
		dG3DElasticMultiscaleMaterialLaw(const dG3DElasticMultiscaleMaterialLaw& src);
    virtual ~dG3DElasticMultiscaleMaterialLaw();

		virtual materialLaw::matname getType() const {return materialLaw::linearElastic;}
		virtual void createIPState(IPStateBase* &ips, bool hasBodyForce,  const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
		virtual void createIPState(const bool isSolve, IPStateBase* &ips, bool hasBodyForce, const bool* state =NULL,
                                 const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt=0) const
		{
			Msg::Error("numericalMaterial::createIPState is not used in this material law");
		};
	// To allow initialization of bulk ip in case of fracture
		virtual void createIPVariable(IPVariable* &ipv, bool hasBodyForce,  const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
		virtual void initLaws(const std::map<int,materialLaw*> &maplaw);
		virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{};
		virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent =false);
		virtual double scaleFactor() const;
		virtual double soundSpeed() const;
		virtual bool isNumeric() const{return false;} // not true numeric material
		virtual materialLaw* clone() const {return new dG3DElasticMultiscaleMaterialLaw(*this);};
    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
      Msg::Error("getConstNonLinearSolverMaterialLaw in dG3DElasticMultiscaleMaterialLaw does not exist");
      return NULL;
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
      Msg::Error("getNonLinearSolverMaterialLaw in dG3DElasticMultiscaleMaterialLaw does not exist");
      return NULL;
    }
		#endif //SWIG
};


class dG3DMultiscaleMaterialLaw : public dG3DMultiscaleMaterialLawBase{
  protected:
    bool _blockDamageAfterFailureOnset; // true if failure is blocked after failure onset
    double _lostSolutionUniquenssTolerance;

  public:
    dG3DMultiscaleMaterialLaw(const int lnum, const int tag);
    void setBlockDamageAfterFailureOnset(const bool flg){ _blockDamageAfterFailureOnset = flg;};
    void setBlockDamageAfterFailureOnsetTolerance(const double tol){ _lostSolutionUniquenssTolerance = tol;};
    #ifndef SWIG
    dG3DMultiscaleMaterialLaw(const dG3DMultiscaleMaterialLaw& src);
    virtual ~dG3DMultiscaleMaterialLaw(){};
    
    bool getBlockDamageAfterFailureOnset() const {return _blockDamageAfterFailureOnset;};
    double getBlockDamageAfterFailureOnsetTolerance() const{return _lostSolutionUniquenssTolerance;}

    virtual matname getType() const{return materialLaw::numeric;};
    virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPState(const bool isSolve, IPStateBase* &ips,bool hasBodyForce, const bool* state =NULL,
                                 const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt=0) const;
    // To allow initialization of bulk ip in case of fracture
    virtual void createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
    virtual void calledBy2lawFracture(){}
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw);
    virtual double density() const {return _rho;};
    // As to be defined for explicit scheme. If law for implicit only
    // please define this function with return 0
    virtual double soundSpeed() const {return 0;};
    // If law enable fracture the bool checkfrac allow to detect fracture. This bool is unused otherwise
    // The bool is necessery for computation of matrix by perturbation in this case the check of fracture
    // has not to be performed.
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
    virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent =false);
    virtual void setElasticStiffness(IPVariable* ipv) const;
    virtual bool isNumeric() const{return true;}
		virtual void initialIPVariable(IPVariable* ipv, bool stiff);
		virtual bool brokenCheck(const IPVariable* ipv) const;
		virtual materialLaw* clone() const {return new dG3DMultiscaleMaterialLaw(*this);};
    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
      Msg::Error("getConstNonLinearSolverMaterialLaw in dG3DMultiscaleMaterialLaw does not exist");
      return NULL;
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
      Msg::Error("getNonLinearSolverMaterialLaw in dG3DMultiscaleMaterialLaw does not exist");
      return NULL;
    }
    #endif // SWIG
};

class MultiscaleThermoMechanicsDG3DMaterialLaw : public dG3DMaterialLaw, public numericalMaterial{
	#ifndef SWIG
	protected:
		STensor3            linearK;
		STensor3	      Stiff_alphaDilatation;
		virtual void fillElasticStiffness(double & rho, STensor43& L){
			Msg::Error("MultiscaleThermoMechanicsDG3DMaterialLaw::fillElasticStiffness should not be used");
      Msg::Exit(0);
		};
		virtual void fillElasticStiffness(double& rho, STensor43& L, STensor3& Klinear, STensor3& alphaDilatation);

	#endif //SWIG


	public:
		MultiscaleThermoMechanicsDG3DMaterialLaw(const int num, const int tag);

		#ifndef SWIG
		MultiscaleThermoMechanicsDG3DMaterialLaw(const MultiscaleThermoMechanicsDG3DMaterialLaw&  src);
		virtual ~MultiscaleThermoMechanicsDG3DMaterialLaw(){}
		virtual matname getType() const{return materialLaw::numeric;};

    virtual bool withEnergyDissipation() const;
    virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;

    virtual void createIPState(const bool isSolve, IPStateBase* &ips,bool hasBodyForce, const bool* state =NULL,
                                 const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt=0) const;
    // To allow initialization of bulk ip in case of fracture
    virtual void createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
    virtual void calledBy2lawFracture(){}
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw);
    virtual double density() const {return _rho;};
    // As to be defined for explicit scheme. If law for implicit only
    // please define this function with return 0
    virtual double soundSpeed() const {return 0;};
    // If law enable fracture the bool checkfrac allow to detect fracture. This bool is unused otherwise
    // The bool is necessery for computation of matrix by perturbation in this case the check of fracture
    // has not to be performed.
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
    virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent =false);
    virtual void setElasticStiffness(IPVariable* ipv) const;
    virtual bool isNumeric() const{return true;}
		virtual void initialIPVariable(IPVariable* ipv, bool stiff);
		virtual materialLaw* clone() const {return new MultiscaleThermoMechanicsDG3DMaterialLaw(*this);};
    virtual void setMacroSolver(const nonLinearMechSolver* sv){
        dG3DMaterialLaw::setMacroSolver(sv);
        numericalMaterial::_macroSolver = sv;
      };
      
    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const
    {
      Msg::Error("getConstNonLinearSolverMaterialLaw in MultiscaleThermoMechanicsDG3DMaterialLaw does not exist");
      return NULL;
    }
    virtual materialLaw *getNonLinearSolverMaterialLaw()
    {
      Msg::Error("getNonLinearSolverMaterialLaw in MultiscaleThermoMechanicsDG3DMaterialLaw does not exist");
      return NULL;
    }
		#endif //SWIG
};

#endif // DG3DMULTISCALEMATERIALLAW_H_
