//
// C++ Interface: 1D domain
//
// Description: Class of terms for dg
//
//
// Author:  <V.D Nguyen>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "dG3D1DDomain.h"
#include "dG3D1DTerms.h"
#include "dG3DHomogenizedTangentTerms.h"
#include "nonLinearMechSolver.h"
#include "pathFollowingTerms.h"
#include "FractureCohesiveDG3DMaterialLaw.h"

dG3D1DDomain::dG3D1DDomain(const dG3D1DDomain& src) : dG3DDomain(src),_state(src._state){
  if (src._crossSectionDistribution){
    _crossSectionDistribution = src._crossSectionDistribution->clone();
  }
  else{
    _crossSectionDistribution = new constantScalarFunction(1.);
  }
};
dG3D1DDomain::~dG3D1DDomain(){
  if (_crossSectionDistribution) {
    delete _crossSectionDistribution;
    _crossSectionDistribution = NULL;
  }
}

void dG3D1DDomain::setCrossSection(const double val)
{
  if (_crossSectionDistribution) delete _crossSectionDistribution;
  _crossSectionDistribution = new constantScalarFunction(val);
}

void dG3D1DDomain::setCrossSectionDistribution(const scalarFunction* fct){
  if (_crossSectionDistribution) delete _crossSectionDistribution;
  _crossSectionDistribution = fct->clone();
};

void dG3D1DDomain::setState(const int s){
  _state = (State)s;
  if (_state == UniaxialStrain){
    Msg::Info("uniaxial strain will be considered");
  }
  else if (_state == UniaxialStress){
    Msg::Info("uniaxial stress will be considered");
  }
  else if (_state == ConstantTriaxiality){
    Msg::Info("constant stress triaxiality will be considered");
  }
  else{
    Msg::Error("state %d has not been implemented",_state);
  }
};
void dG3D1DDomain::setStressTriaxiality(const double T){
  Msg::Info("constant stress triaxiality %e is applied",T);
  _T = T;
};


dG3D1DDomain::dG3D1DDomain(const int tag, const int phys, const int sp_, const int lnum, const int fdg, int numNL, int numExtraDOF) : dG3DDomain(tag,phys,sp_,lnum,fdg,1,numNL,numExtraDOF),
_state(UniaxialStrain), _T(0.){
  _crossSectionDistribution = new constantScalarFunction(1.);
  #if _DEBUG
  Msg::Warning("1D must be defined in x-coordinate only");
  #endif //

  if( getNumConstitutiveExtraDofDiffusionVariable() >0){
        setConstitutiveExtraDofDiffusionEqRatio(_ThermoMechanicsEqRatio);
    }
};


void dG3D1DDomain::createTerms(unknownField *uf,IPField*ip)
{
  FunctionSpace<double>* dgspace = static_cast<FunctionSpace<double>*>(_space);
  if (getFunctionSpaceType() == functionSpaceType::Lagrange)
  {
    massterm = new mass3D(*dgspace,_mlaw,getNumNonLocalVariable());
  }
  else if (getFunctionSpaceType() == functionSpaceType::Hierarchical)
  {
    massterm = new mass3DHierarchicalFE(*dgspace,_mlaw,getNumNonLocalVariable());
  }
  else
  {
    Msg::Error("mass matrix has not implemented for this kind of functions space");
    massterm = new BiNonLinearTermVoid();
  }
  ltermBulk = new dG3D1DForceBulk(this,ip,_crossSectionDistribution);
  if(_bmbp)
    btermBulk = new BilinearTermPerturbation<double>(ltermBulk,*dgspace,*dgspace,uf,ip,this,_eps);
  else
   btermBulk = new dG3D1DStiffnessBulk(this,ip,_crossSectionDistribution);
  if( getNumNonLocalVariable() >0)
  {
    (static_cast<dG3D1DForceBulk*>(ltermBulk))->setNonLocalEqRatio(getNonLocalEqRatio());
    (static_cast<dG3D1DForceBulk*>(ltermBulk))->setNumNonLocalVariable(getNumNonLocalVariable());
    if(!_bmbp)
    {
      (static_cast<dG3D1DStiffnessBulk*>(btermBulk))->setNonLocalEqRatio(getNonLocalEqRatio());
      (static_cast<dG3D1DStiffnessBulk*>(btermBulk))->setNumNonLocalVariable(getNumNonLocalVariable());
    }
  }
 //temporary
 if(getNumNonLocalVariable()==0)
 {


  ltermBound = new nonLinearTermVoid();
  btermBound = new BiNonLinearTermVoid();

  this->btermVirtBound = new BiNonLinearTermVoid();
  this->ltermVirtBound = new nonLinearTermVoid();


	btermTangent = new dG3DHomogenizedTangent(this,ip);

	// path following term
  bool withPF = false;
  if (this->getMacroSolver() != NULL and _accountPathFollowing)
  {
    withPF = this->getMacroSolver()->withPathFollowing();
  }

  if (withPF){
    if (getMacroSolver()->getPathFollowingLocation() == pathFollowingManager::INTERFACE){
      scalarTermPF = new nonLinearScalarTermVoid();
      linearTermPF = new nonLinearTermVoid();
    }
    else{
      scalarTermPF = new dG3DDissipationPathFollowingBulkScalarTerm(this,ip);
      linearTermPF = new dG3DDissipationPathFollowingBulkLinearTerm(this,ip);
    }

    if (getMacroSolver()->getPathFollowingLocation() == pathFollowingManager::BULK){
      scalarTermPFBound = new nonLinearScalarTermVoid();
      linearTermPFBound = new nonLinearTermVoid();
    }
    else{
      scalarTermPFBound = new dG3DDissipationPathFollowingBoundScalarTerm(this,ip);
      linearTermPFBound = new dG3DDissipationPathFollowingBoundLinearTerm(this,ip);
    }
  }
  else{
    scalarTermPF = new nonLinearScalarTermVoid();;
    linearTermPF = new nonLinearTermVoid();;
    scalarTermPFBound = new nonLinearScalarTermVoid();;
    linearTermPFBound = new nonLinearTermVoid();
  }
 } else {

  ltermBound = new nonLinearTermVoid();
  btermBound = new BiNonLinearTermVoid();

  this->btermVirtBound = new BiNonLinearTermVoid();
  this->ltermVirtBound = new nonLinearTermVoid();


	btermTangent = new dG3DHomogenizedTangent(this,ip);

	// path following term
  bool withPF = false;
  if (this->getMacroSolver() != NULL and _accountPathFollowing)
  {
    withPF = getMacroSolver()->withPathFollowing();
  }

  if (withPF){
    if (getMacroSolver()->getPathFollowingLocation() == pathFollowingManager::INTERFACE){
      scalarTermPF = new nonLinearScalarTermVoid();
      linearTermPF = new nonLinearTermVoid();
    }
    else{
      scalarTermPF = new dG3DDissipationPathFollowingBulkScalarTerm(this,ip);
      linearTermPF = new dG3DDissipationPathFollowingBulkLinearTerm(this,ip);
    }

    if (getMacroSolver()->getPathFollowingLocation() == pathFollowingManager::BULK){
      scalarTermPFBound = new nonLinearScalarTermVoid();
      linearTermPFBound = new nonLinearTermVoid();
    }
    else{
      scalarTermPFBound = new dG3DDissipationPathFollowingBoundScalarTerm(this,ip);
      linearTermPFBound = new dG3DDissipationPathFollowingBoundLinearTerm(this,ip);
    }
  }
  else{
    scalarTermPF = new nonLinearScalarTermVoid();;
    linearTermPF = new nonLinearTermVoid();;
    scalarTermPFBound = new nonLinearScalarTermVoid();;
    linearTermPFBound = new nonLinearTermVoid();
  }
 }
}


bool dG3D1DDomain::computeIpv(AllIPState *aips,MElement *e, IPStateBase::whichState ws,
                                        materialLaw *mlaw__,fullVector<double> &disp, bool stiff)
{
	if (!getElementErosionFilter()(e)) return true;

  dG3DMaterialLaw *mlaw;
  if(mlaw__->getType() == materialLaw::fracture)
    mlaw = static_cast< dG3DMaterialLaw* >((static_cast< FractureByCohesive3DLaw* > (mlaw__) )->getBulkLaw());
  else{
    mlaw= static_cast<dG3DMaterialLaw*>(mlaw__);
  }

  AllIPState::ipstateElementContainer *vips = aips->getIPstate(e->getNum());
  IntPt *GP;
  int npts_bulk=integBulk->getIntPoints(e,&GP);

  // full computation
  for(int j=0;j<npts_bulk;j++)
  {
    // get dgipv from ipstate
    IPStateBase* ips = (*vips)[j];
    dG3DIPVariableBase *ipv = static_cast<dG3DIPVariableBase*>(ips->getState(ws));
    const dG3DIPVariableBase *ipvprev = static_cast<const dG3DIPVariableBase*>(ips->getState(IPStateBase::previous));

    if (_state == UniaxialStress){
      mlaw->stress3DTo1D(ipv,ipvprev,stiff);
    }
    else if (_state == ConstantTriaxiality){
      mlaw->stress3DTo1D_constantTriaxiality(_T,ipv,ipvprev,stiff);
    }
    else if (_state == UniaxialStrain){
      mlaw->stress(ipv,ipvprev, stiff);
    }
    else {
      Msg::Error("state must be set via dG3D1DDomain::setState");
    }
    if (!(ipv->getConstitutiveSuccessFlag()))
    {
      return false;
    }
  }
  return true;
};
