//
// C++ Interface: ipvariable
//
// Description: Class with definition of ipvarible for dG3D
//
//
// Author:  <L. Noels>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "nonLocalDamageDG3DIPVariable.h"
#include "ipstate.h"
#include "ipField.h"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <string.h>
#include "restartManager.h"
#include "highOrderTensor.h"
#include "failureDetection.h"


virtualNonLocalDamageDG3DIPVariable::virtualNonLocalDamageDG3DIPVariable(const int num, const dG3DIPVariableBase* base, const std::vector<STensor3>& length, int numExtra,bool createBodyForceHO):
      _nonlocalData(num,numExtra), _constitutiveExtraDofDiffusionData(NULL), dG3DIPVariableBase(),characteristicLength(length), _bodyForceForHO(NULL){
  _ipvBase = dynamic_cast<dG3DIPVariableBase*>(base->clone());
  if (_ipvBase == NULL){
    Msg::Error("dG3DIPVariableBase must be used");
  }
  if (num > 0){
    localVar.resize(num,0.);
    nonLocalVar.resize(num,0.);
    DirrEnergDNonlocalVar.resize(num,0.);
  }
  if(numExtra>0.)
  {
    _constitutiveExtraDofDiffusionData = new constitutiveExtraDofDiffusionData(numExtra,num);
    constitutiveExtraDofVar.resize(numExtra,0);
    DirrEnergDConstitutiveExtraDofDiffusion.resize(numExtra,0.);
  }
  if(createBodyForceHO)
    _bodyForceForHO = new bodyForceForHO();
};

virtualNonLocalDamageDG3DIPVariable::virtualNonLocalDamageDG3DIPVariable(const virtualNonLocalDamageDG3DIPVariable& src):
  dG3DIPVariableBase(src), _nonlocalData(src._nonlocalData), localVar(src.localVar),nonLocalVar(src.nonLocalVar),
  characteristicLength(src.characteristicLength),DirrEnergDNonlocalVar(src.DirrEnergDNonlocalVar),
  constitutiveExtraDofVar(src.constitutiveExtraDofVar),
  DirrEnergDConstitutiveExtraDofDiffusion(src.DirrEnergDConstitutiveExtraDofDiffusion)
{
  _ipvBase = NULL;
  if (src._ipvBase != NULL){
    _ipvBase = dynamic_cast<dG3DIPVariableBase*>(src._ipvBase->clone());
  }
  _constitutiveExtraDofDiffusionData = NULL;
  if(src._constitutiveExtraDofDiffusionData!=NULL)
  {
    _constitutiveExtraDofDiffusionData = new constitutiveExtraDofDiffusionData(*(src._constitutiveExtraDofDiffusionData));
  }
  if (src._bodyForceForHO != NULL)
    {
        _bodyForceForHO  = new bodyForceForHO(*(src._bodyForceForHO));
    }
};
virtualNonLocalDamageDG3DIPVariable& virtualNonLocalDamageDG3DIPVariable::operator =(const IPVariable& src){
  dG3DIPVariableBase::operator =(src);
  const virtualNonLocalDamageDG3DIPVariable* psrc = dynamic_cast<const virtualNonLocalDamageDG3DIPVariable*>(&src);
  if (psrc != NULL){
    _nonlocalData = psrc->_nonlocalData;
    localVar = psrc->localVar;
    nonLocalVar = psrc->nonLocalVar;
    DirrEnergDNonlocalVar = psrc->DirrEnergDNonlocalVar;
    if(psrc->_constitutiveExtraDofDiffusionData!=NULL)
    {
      if(_constitutiveExtraDofDiffusionData==NULL)
        _constitutiveExtraDofDiffusionData = new constitutiveExtraDofDiffusionData(*(psrc->_constitutiveExtraDofDiffusionData));
      else
        _constitutiveExtraDofDiffusionData->operator=(*(psrc->_constitutiveExtraDofDiffusionData));
    }
    constitutiveExtraDofVar = psrc->constitutiveExtraDofVar;
    DirrEnergDConstitutiveExtraDofDiffusion = psrc->DirrEnergDConstitutiveExtraDofDiffusion;
    if (psrc->_ipvBase != NULL){
      if (_ipvBase != NULL){
        _ipvBase->operator=(*dynamic_cast<const IPVariable*>(psrc->_ipvBase));
      }
      else{
        _ipvBase = dynamic_cast<dG3DIPVariableBase*>(psrc->_ipvBase->clone());
      }
    }
    if (psrc->_bodyForceForHO != NULL )
    {
        if (_bodyForceForHO == NULL)
        {
          _bodyForceForHO = new bodyForceForHO(*(psrc->_bodyForceForHO));
        }
        else
        {
          _bodyForceForHO->operator=(*(psrc->_bodyForceForHO));
        }
    }
  }
  return *this;
};

virtualNonLocalDamageDG3DIPVariable::~virtualNonLocalDamageDG3DIPVariable(){
  if(_constitutiveExtraDofDiffusionData!=NULL) delete _constitutiveExtraDofDiffusionData;
  if (_ipvBase != NULL) delete _ipvBase; _ipvBase = NULL;
};

double virtualNonLocalDamageDG3DIPVariable::get(const int comp) const{
  if (comp == IPField::LOCAL_0){
    if (getNumberNonLocalVariable()>0){
      return getConstRefToLocalVariable(0);
    }
    else return 0.;
  }
  else if (comp == IPField::LOCAL_1){
    if (getNumberNonLocalVariable()>1){
      return getConstRefToLocalVariable(1);
    }
    else return 0.;
  }
  else if (comp == IPField::LOCAL_2){
    if (getNumberNonLocalVariable() > 2){
      return getConstRefToLocalVariable(2);
    }
    else return 0.;
  }
  else if (comp == IPField::NONLOCAL_0){
    if (getNumberNonLocalVariable() > 0){
      return getConstRefToNonLocalVariableInMaterialLaw(0);
    }
    else return 0.;
  }
  else if (comp == IPField::NONLOCAL_1){
    if (getNumberNonLocalVariable()> 1){
      return getConstRefToNonLocalVariableInMaterialLaw(1);
    }
    else return 0.;
  }
  else if (comp == IPField::NONLOCAL_2){
    if (getNumberNonLocalVariable() > 2)
      return getConstRefToNonLocalVariableInMaterialLaw(2);
    else return 0.;
  }
  else if (comp == IPField::CONSTITUTIVEEXTRADOFDIFFUSION_0){
    if (getNumConstitutiveExtraDofDiffusionVariable() > 0)
      return getConstRefToField(0);
  }
  else if (comp == IPField::CONSTITUTIVEEXTRADOFDIFFUSION_1){
    if (getNumConstitutiveExtraDofDiffusionVariable() > 1)
      return getConstRefToField(1);
  }
  else if (comp == IPField::CONSTITUTIVEEXTRADOFDIFFUSION_2){
    if (getNumConstitutiveExtraDofDiffusionVariable() > 2)
      return getConstRefToField(2);
  }
  else{
    return _ipvBase->get(comp);
  }
  return 0.;
}

void virtualNonLocalDamageDG3DIPVariable::restart(){
  _ipvBase->restart();
  _nonlocalData.restart();
  if(_constitutiveExtraDofDiffusionData!=NULL)
    _constitutiveExtraDofDiffusionData->restart();
  if(_bodyForceForHO!=NULL)
    _bodyForceForHO->restart();
};

//
nonLocalDamageDG3DIPVariable::nonLocalDamageDG3DIPVariable(int _nsdv, int _nlVar, const bool createBodyForceHO, const bool oninter) : dG3DIPVariable(createBodyForceHO,oninter,_nlVar)
{
  _nldipv = new IPNonLocalDamage(_nsdv);
}

nonLocalDamageDG3DIPVariable::nonLocalDamageDG3DIPVariable(const nonLocalDamageDG3DIPVariable &source) :
                                                                   dG3DIPVariable(source)
{
  _nldipv = NULL;
  if (source.getIPNonLocalDamage() != NULL){
    _nldipv = new IPNonLocalDamage((int)source.getIPNonLocalDamage()->getNsdv());
    _nldipv->operator= (*dynamic_cast<const IPVariable*>(source.getIPNonLocalDamage()));
  }

}

nonLocalDamageDG3DIPVariable& nonLocalDamageDG3DIPVariable::operator=(const IPVariable &source)
{
  dG3DIPVariable::operator=(source);
  const nonLocalDamageDG3DIPVariable* src = dynamic_cast<const nonLocalDamageDG3DIPVariable*>(&source);
  if(src != NULL)
  {
    if (src->getIPNonLocalDamage() != NULL){
      if (_nldipv != NULL){
        _nldipv->operator=(*dynamic_cast<const IPVariable*>(src->getIPNonLocalDamage()));
      }
      else{
        _nldipv = dynamic_cast<IPNonLocalDamage*>(src->getIPNonLocalDamage()->clone());
      }
    }
    else{
      if (_nldipv != NULL){
        delete _nldipv;
        _nldipv = NULL;
      }
    }
  }
  return *this;
}
double nonLocalDamageDG3DIPVariable::get(const int i) const
{
  double val = getIPNonLocalDamage()->get(i);
  if (val == 0.)
    val = dG3DIPVariable::get(i);
  return val;
}
double nonLocalDamageDG3DIPVariable::defoEnergy() const
{
  return getIPNonLocalDamage()->defoEnergy();
}
double nonLocalDamageDG3DIPVariable::plasticEnergy() const
{
  return getIPNonLocalDamage()->plasticEnergy();
}

double nonLocalDamageDG3DIPVariable::getConstRefToLocalVariable(const int idex) const {
  if (idex == 0)
    return getIPNonLocalDamage()->_nldCurrentPlasticStrain;
  else if (idex == 1)
    return getIPNonLocalDamage()->_nlFiber_loc; //change here and below
  else{
    Msg::Error("the non-local variable %d is not defined",idex);    
  }
  return getIPNonLocalDamage()->_nldCurrentPlasticStrain;
};
double& nonLocalDamageDG3DIPVariable::getRefToLocalVariable(const int idex) {
  if (idex == 0)
    return getIPNonLocalDamage()->_nldCurrentPlasticStrain;
  else if (idex == 1) 
    return getIPNonLocalDamage()->_nlFiber_loc;
  else{
    Msg::Error("the non-local variable %d is not defined",idex);
  }
  return getIPNonLocalDamage()->_nldCurrentPlasticStrain;
};

double nonLocalDamageDG3DIPVariable::getConstRefToNonLocalVariableInMaterialLaw(const int idex) const{
  if (idex == 0)
    return getIPNonLocalDamage()->_nldEffectivePlasticStrain;
  else if (idex == 1)
    return getIPNonLocalDamage()->_nlFiber_d_bar;
  else{
    Msg::Error("the non-local variable %d is not defined",idex);
  }
    return getIPNonLocalDamage()->_nldEffectivePlasticStrain;
};
double& nonLocalDamageDG3DIPVariable::getRefToNonLocalVariableInMaterialLaw(const int idex) {
  if (idex == 0)
    return getIPNonLocalDamage()->_nldEffectivePlasticStrain;
  else if (idex == 1)
    return getIPNonLocalDamage()->_nlFiber_d_bar;
  else{
    Msg::Error("the non-local variable %d is not defined",idex);
  }
    return getIPNonLocalDamage()->_nldEffectivePlasticStrain;
};

void nonLocalDamageDG3DIPVariable::restart()
{
  dG3DIPVariable::restart();
  if (_nldipv != NULL)
    restartManager::restart(_nldipv);
  return;
}


/* Beginning IPvariable for nonlocalDamageTorchANNDG3DIPVariable*/
nonLocalDamageTorchANNDG3DIPVariableDG3DIPVariable::nonLocalDamageTorchANNDG3DIPVariableDG3DIPVariable(const std::vector< CLengthLaw *> cll, const int n_stress, const int n_elTang, const int n_NonDamTang, const double initial_h, const int numNonlocal, const bool createBodyForceHO, const bool oninter):
      dG3DIPVariable(createBodyForceHO, oninter,numNonlocal), _n_stress(n_stress), _n_elTang(n_elTang), _n_NonDamTang(n_NonDamTang), _numNonlocal(numNonlocal), _initial_h(initial_h), _nlD(1,6), _D(1,6), _nlD36comp(6,6), _D36comp(6,6),
      restart_internalVars_stress(1,n_stress), restart_internalVars_elTang(1,n_elTang), restart_internalVars_NonDamTang(1,n_NonDamTang), _kinematicVariables(1,6),   
      _defoEnergy(0.), _plasticEnergy(0.), _damageEnergy(0.), _irreversibleEnergy(0.), _DirrEnergyDF(0.), _DirrEnergyDVar_nl0(0.), _DirrEnergyDVar_nl1(0.)

{
    localVar.resize(numNonlocal,0.);
    _ipMeca = NULL; 
#if defined(HAVE_TORCH)
   _internalVars_stress = _initial_h*torch::ones({1, 1, _n_stress});
   _internalVars_elTang = _initial_h*torch::ones({1, 1, _n_elTang});
   _internalVars_NonDamTang = _initial_h*torch::ones({1, 1, _n_NonDamTang});
#endif

  ipvCL.resize(numNonlocal, NULL);
  for(int i=0; i<numNonlocal; i++)
  {
    if(cll[i] ==NULL) Msg::Error("nonLocalDamageTorchANNDG3DIPVariableDG3DIPVariable::nonLocalDamageTorchANNDG3DIPVariableDG3DIPVariable has no cll");
    cll[i]->createIPVariable(ipvCL[i]);
  }

}
nonLocalDamageTorchANNDG3DIPVariableDG3DIPVariable::nonLocalDamageTorchANNDG3DIPVariableDG3DIPVariable(const nonLocalDamageTorchANNDG3DIPVariableDG3DIPVariable &source):dG3DIPVariable(source),_n_stress(source._n_stress), _n_elTang(source._n_elTang), _n_NonDamTang(source._n_NonDamTang), _numNonlocal(source._numNonlocal), _initial_h(source._initial_h), _nlD(source._nlD), 
    _D(source._D), _nlD36comp(source._nlD36comp), 
    _D36comp(source._D36comp), _DTensor43(source._DTensor43), _nlDTensor43(source._nlDTensor43), localVar(source.localVar), _internalVars_stress(source._internalVars_stress), _internalVars_elTang(source._internalVars_elTang), _internalVars_NonDamTang(source._internalVars_NonDamTang), 
    _kinematicVariables(source._kinematicVariables),_irreversibleEnergy(source._irreversibleEnergy), _DirrEnergyDF(source._DirrEnergyDF),_DirrEnergyDVar_nl0(source._DirrEnergyDVar_nl0), _DirrEnergyDVar_nl1(source._DirrEnergyDVar_nl1), _plasticEnergy(source._plasticEnergy), 
     _defoEnergy(source._defoEnergy), _damageEnergy(source._damageEnergy), _ipMeca(NULL)

{
  for( std::vector< IPCLength * > ::const_iterator it=source.ipvCL.begin(); it!=source.ipvCL.end(); it++)
  {
     if(*it != NULL)
     {
       ipvCL.push_back(dynamic_cast<IPCLength*>((*it)->clone()));
     }
  }
  if(source._ipMeca!=NULL)
  {
    if(_ipMeca!=NULL)
    {
      delete _ipMeca;
      _ipMeca=NULL;
    }
    _ipMeca = dynamic_cast<IPVariableMechanics *>(source.getConstRefToIpMeca().clone());
  }      
};


    
nonLocalDamageTorchANNDG3DIPVariableDG3DIPVariable& nonLocalDamageTorchANNDG3DIPVariableDG3DIPVariable::operator =(const IPVariable& src)
{
  dG3DIPVariable::operator=(src);
#if defined(HAVE_TORCH)
  const nonLocalDamageTorchANNDG3DIPVariableDG3DIPVariable* psrc = dynamic_cast<const nonLocalDamageTorchANNDG3DIPVariableDG3DIPVariable*>(&src);
  if (psrc!=NULL)
  {
    _internalVars_stress = psrc->_internalVars_stress;
    _internalVars_elTang = psrc->_internalVars_elTang;
    _internalVars_NonDamTang = psrc->_internalVars_NonDamTang;
    _kinematicVariables = psrc->_kinematicVariables;
    
    _n_stress = psrc->_n_stress;
    _n_elTang = psrc->_n_elTang;
    _n_NonDamTang = psrc->_n_NonDamTang;
    _numNonlocal = psrc->_numNonlocal;
    _initial_h = psrc->_initial_h;
    _nlD = psrc->_nlD;   //not used finally
    _D = psrc->_D;       //not used finally
    _nlD36comp = psrc->_nlD36comp;   //not used finally
    _D36comp = psrc->_D36comp;       //not used finally
    _nlDTensor43 = psrc->_nlDTensor43;
    _DTensor43 = psrc->_DTensor43;
    localVar =  psrc->localVar;
    _Eplast = psrc->_Eplast;
    _Eplastrec = psrc->_Eplastrec;  //plastic strain reconstructed
    _Cel = psrc->_Cel;   //elastic stiffness without damage
    _DS_nlDE = psrc->_DS_nlDE; //DStressreconstructedDStrain
    _DEplastDE = psrc->_DEplastDE;   //DEplastDE
    _defoEnergy = psrc -> _defoEnergy; //added for energy-based path Following
    _plasticEnergy = psrc -> _plasticEnergy; //added for energy-based path Following
    _damageEnergy = psrc -> _damageEnergy; //added for energy-based path Following    
    _irreversibleEnergy = psrc -> _irreversibleEnergy; //added for energy-based path Following
    _DirrEnergyDF = psrc -> _DirrEnergyDF; //added for energy-based path Following
    _DirrEnergyDVar_nl0 = psrc -> _DirrEnergyDVar_nl0; //added for energy-based path Following
    _DirrEnergyDVar_nl1 = psrc -> _DirrEnergyDVar_nl1; //added for energy-based path Following

    if(psrc->_ipMeca!=NULL)
    {
      if(_ipMeca!=NULL)
      {
        _ipMeca->operator=(dynamic_cast<const IPVariable &> (*(psrc->_ipMeca)));
      }
      else
      {
        _ipMeca = dynamic_cast<IPVariableMechanics *>((psrc->getConstRefToIpMeca()).clone());
      }
    }
    
    int i=0;
    for(std::vector< IPCLength * > ::const_iterator it=psrc->ipvCL.begin(); it!=psrc->ipvCL.end(); it++,i++)
    {
      if(*it != NULL)
      {
	if (ipvCL[i] != NULL){
	   ipvCL[i]->operator=(*dynamic_cast<const IPCLength*>(*it));
	}
	else
	   ipvCL[i]= dynamic_cast<IPCLength*>((*it)->clone());
      }
      else{
        if(ipvCL[i] != NULL) delete ipvCL[i]; ipvCL[i] = NULL;
      }
    }

  }
#else
  Msg::Error("NOT COMPILED WITH TORCH");
#endif
  return *this;
}


nonLocalDamageTorchANNDG3DIPVariableDG3DIPVariable::~nonLocalDamageTorchANNDG3DIPVariableDG3DIPVariable()
{
  for( std::vector< IPCLength * > ::iterator it=ipvCL.begin(); it!=ipvCL.end(); it++)
  {
    if(*it != NULL)
      delete *it;
  }
  if(_ipMeca!=NULL)
  {
    delete _ipMeca;
    _ipMeca=NULL;
  }
};


void nonLocalDamageTorchANNDG3DIPVariableDG3DIPVariable::restart()
{
#if defined(HAVE_TORCH)
  auto Vars_a = _internalVars_stress.accessor<float,3>();
  auto Vars_a_elTang = _internalVars_elTang.accessor<float,3>();
  auto Vars_a_NonDamTang = _internalVars_NonDamTang.accessor<float,3>();
  for(int i=0;i<_n_stress;i++) restart_internalVars_stress(0,i) = Vars_a[0][0][i];
  for(int i=0;i<_n_elTang;i++) restart_internalVars_elTang(0,i) = Vars_a_elTang[0][0][i];
  for(int i=0;i<_n_NonDamTang;i++) restart_internalVars_NonDamTang(0,i) = Vars_a_NonDamTang[0][0][i];
  dG3DIPVariable::restart();
  restartManager::restart(restart_internalVars_stress.getDataPtr(),restart_internalVars_stress.size1()*restart_internalVars_stress.size2());
  restartManager::restart(restart_internalVars_elTang.getDataPtr(),restart_internalVars_elTang.size1()*restart_internalVars_elTang.size2());
  restartManager::restart(restart_internalVars_NonDamTang.getDataPtr(),restart_internalVars_NonDamTang.size1()*restart_internalVars_NonDamTang.size2());
  restartManager::restart(_kinematicVariables.getDataPtr(),_kinematicVariables.size1()*_kinematicVariables.size2());
  for(int i=0;i<_n_stress;i++) _internalVars_stress[0][0][i] = restart_internalVars_stress(0,i);
  for(int i=0;i<_n_elTang;i++) _internalVars_elTang[0][0][i] = restart_internalVars_elTang(0,i);
  for(int i=0;i<_n_NonDamTang;i++) _internalVars_NonDamTang[0][0][i] = restart_internalVars_NonDamTang(0,i);
  restartManager::restart(_nlD.getDataPtr(),_nlD.size1()*_nlD.size2());
  restartManager::restart(_D.getDataPtr(),_nlD.size1()*_nlD.size2());
  restartManager::restart(_D36comp.getDataPtr(),_nlD36comp.size1()*_nlD36comp.size2());
  restartManager::restart(_D36comp.getDataPtr(),_nlD36comp.size1()*_nlD36comp.size2());
  restartManager::restart(localVar);
  //plastic strain
  restartManager::restart(_Eplast);
  //plastic strain reconstructed
  restartManager::restart(_Eplastrec);
  //elastic stiffness without damage
  restartManager::restart(_Cel);
  restartManager::restart(_DS_nlDE);  //DStressreconstructedDStrain
  restartManager::restart(_DEplastDE); //DEplastDE
  restartManager::restart(ipvCL);
  restartManager::restart(_irreversibleEnergy);  //added for energy-based path Following
  restartManager::restart(_DirrEnergyDF);  //added for energy-based path Following
  restartManager::restart(_DirrEnergyDVar_nl0);  //added for energy-based path Following
  restartManager::restart(_DirrEnergyDVar_nl1);  //added for energy-based path Following
  restartManager::restart(_defoEnergy);  //added for energy-based path Following
  restartManager::restart(_damageEnergy);  //added for energy-based path Following
  restartManager::restart(_plasticEnergy);  //added for energy-based path Following
  _ipMeca->restart();
  
#else
  Msg::Error("NOT COMPILED WITH TORCH");
#endif
}

double nonLocalDamageTorchANNDG3DIPVariableDG3DIPVariable::get(const int comp) const
{
#if defined(HAVE_TORCH)
   auto Vars_a = _internalVars_stress.accessor<float,3>();
    if (comp == IPField::USER0)
  {
    if (_n_stress > 1) return Vars_a[0][0][0];
    else return 0.;
  }
  else if (comp == IPField::USER1)
  {
    if (_n_stress > 2) return Vars_a[0][0][1];
    else return 0.;
  }
  else if (comp == IPField::USER2)
  {
    if (_n_stress > 3) return Vars_a[0][0][2];
    else return 0.;
  }
  else if (comp == IPField::USER3)
  {
    if (_n_stress > 4) return Vars_a[0][0][3];
    else return 0.;
  }
  else if (comp == IPField::USER4)
  {
    if (_n_stress > 5) return Vars_a[0][0][4];
    else return 0.;
  }
  else if (comp == IPField::USER5)
  {
    if (_n_stress > 6) return Vars_a[0][0][5];
    else return 0.;
  }
  else if (comp == IPField::USER6)
  {
    if (_n_stress > 7) return Vars_a[0][0][6];
    else return 0.;
  }
  else if (comp == IPField::USER7)
  {
    if (_n_stress > 8) return Vars_a[0][0][7];
    else return 0.;
  }
  else if (comp == IPField::USER8)
  {
    if (_n_stress > 9) return Vars_a[0][0][8];
    else return 0.;
  }
  else if (comp == IPField::USER9)
  {
    if (_n_stress > 10) return Vars_a[0][0][9];
    else return 0.;
  }
  else if (comp == IPField::USER10)
  {
    if (_n_stress > 11) return Vars_a[0][0][10];
    else return 0.;
  }
  else if (comp == IPField::LOCAL_0)
  {
    if (_numNonlocal > 1) return localVar[0];
    else return 0.;
  }
  else if (comp == IPField::LOCAL_1)
  {
    if (_numNonlocal > 2) return localVar[1];
    else return 0.;
  }
  else if (comp == IPField::LOCAL_2)
  {
    if (_numNonlocal > 3) return localVar[2];
    else return 0.;
  }
  else if (comp == IPField::LOCAL_3)
  {
    if (_numNonlocal > 4) return localVar[3];
    else return 0.;
  }
  else if (comp == IPField::LOCAL_4)
  {
    if (_numNonlocal > 5) return localVar[4];
    else return 0.;
  }
  else if (comp == IPField::LOCAL_5)
  {
    if (_numNonlocal > 6) return localVar[5];
    else return 0.;
  }
  else if (comp == IPField::LOCAL_6)
  {
    if (_numNonlocal > 7) return localVar[6];
    else return 0.;
  }
  else if (comp == IPField::LOCAL_7)
  {
    if (_numNonlocal > 8) return localVar[7];
    else return 0.;
  }
  else if (comp == IPField::LOCAL_8)
  {
    if (_numNonlocal > 9) return localVar[8];
    else return 0.;
  }
  else if (comp == IPField::LOCAL_9)
  {
    if (_numNonlocal > 10) return localVar[9];
    else return 0.;
  }
  else if (comp == IPField::NONLOCAL_0)
  {
    if (_numNonlocal > 1) return _nonlocalData->nonLocalVariable(0);
    else return 0.;
  }
  else if (comp == IPField::NONLOCAL_1)
  {
    if (_numNonlocal > 2) return _nonlocalData->nonLocalVariable(1);
    else return 0.;
  }
  else if (comp == IPField::NONLOCAL_2)
  {
    if (_numNonlocal > 3) return _nonlocalData->nonLocalVariable(2);
    else return 0.;
  }
  else if (comp == IPField::NONLOCAL_3)
  {
    if (_numNonlocal > 4) return _nonlocalData->nonLocalVariable(3);
    else return 0.;
  }
  else if (comp == IPField::NONLOCAL_4)
  {
    if (_numNonlocal > 5) return _nonlocalData->nonLocalVariable(4);
    else return 0.;
  }
  else if (comp == IPField::NONLOCAL_5)
  {
    if (_numNonlocal > 6) return _nonlocalData->nonLocalVariable(5);
    else return 0.;
  }
  else if (comp == IPField::NONLOCAL_6)
  {
    if (_numNonlocal > 7) return _nonlocalData->nonLocalVariable(6);
    else return 0.;
  }
  else if (comp == IPField::NONLOCAL_7)
  {
    if (_numNonlocal > 8) return _nonlocalData->nonLocalVariable(7);
    else return 0.;
  }
  else if (comp == IPField::NONLOCAL_8)
  {
    if (_numNonlocal > 9) return _nonlocalData->nonLocalVariable(8);
    else return 0.;
  }
  else if (comp == IPField::NONLOCAL_9)
  {
    if (_numNonlocal > 10) return _nonlocalData->nonLocalVariable(9);
    else return 0.;
  }
  else if (comp ==IPField::DAMAGE_TENSOR43_0011)
  {
    return _DTensor43(0,0,1,1);
  }
  //local damage (only 6 out of 81 components because we start in 1D)
  else if (comp ==IPField::DAMAGE_TENSOR43_0011)
  {
    return _DTensor43(0,0,1,1);
  }  
  else if (comp ==IPField::DAMAGE_TENSOR43_0022)
  {
    return _DTensor43(0,0,2,2);
  }  
  else if (comp ==IPField::DAMAGE_TENSOR43_1111)
  {
    return _DTensor43(1,1,1,1);
  }  
  else if (comp ==IPField::DAMAGE_TENSOR43_1122)
  {
    return _DTensor43(1,1,2,2);
  }  
  else if (comp ==IPField::DAMAGE_TENSOR43_2222)
  {
    return _DTensor43(2,2,2,2);
  }  
  else if (comp ==IPField::DAMAGE_TENSOR43_0101)
  {
    return _DTensor43(0,1,0,1);
  }  
  else if (comp ==IPField::EPLAST_XX)
  {
    return _Eplast(0,0);
  }  
  else if (comp ==IPField::EPLAST_YY)
  {
    return _Eplast(1,1);
  }  
  else if (comp ==IPField::EPLAST_ZZ)
  {
    return _Eplast(2,2);
  }  
  else if (comp ==IPField::EPLAST_XY)
  {
    return _Eplast(0,1);
  }  
  else if (comp ==IPField::EPLAST_XZ)
  {
    return _Eplast(0,2);
  }  
  else if (comp ==IPField::EPLAST_YZ)
  {
    return _Eplast(1,2);
  }  
  else if (comp ==IPField::C_EL_0000)
  {
    return _Cel(0,0,0,0);
  }  
  else if (comp ==IPField::C_EL_0011)
  {
    return _Cel(0,0,1,1);
  }  
  else if (comp ==IPField::C_EL_0022)
  {
    return _Cel(0,0,2,2);
  }  
  else if (comp ==IPField::C_EL_1111)
  {
    return _Cel(1,1,1,1);
  }  
  else if (comp ==IPField::C_EL_1122)
  {
    return _Cel(1,1,2,2);
  }  
  else if (comp ==IPField::C_EL_2222)
  {
    return _Cel(2,2,2,2);
  }  
  else if (comp ==IPField::C_EL_0101)
  {
    return _Cel(0,1,0,1);
  }  
  else if (comp == IPField::DSIGMA_XX_DNONLOCALVAR)
  {
    return _nonlocalData->dStressDNonLocalVariable[0](0,0);
  }
  else if (comp == IPField::DLOCALVAR_DEPSILON_XX)
  {
    return _nonlocalData->dLocalVariableDStrain[0](0,0);
  }
  else if (comp == IPField::DSIGMAREC_XX_DEPSILON_XX)
  {
    return _DS_nlDE(0,0,0,0);
  }
  else if (comp == IPField::DSIGMAREC_YY_DEPSILON_XX)
  {
    return _DS_nlDE(1,1,0,0);
  }
  else if (comp == IPField::DSIGMAREC_ZZ_DEPSILON_XX)
  {
    return _DS_nlDE(2,2,0,0);
  }
  else if (comp == IPField::DSIGMAREC_XY_DEPSILON_XX)
  {
    return _DS_nlDE(0,1,0,0);
  }
  else if (comp == IPField::DEPLAST_XX_DEPSILON_XX)
  {
    return _DEplastDE(0,0,0,0);
  }
  else if (comp == IPField::DEPLAST_YY_DEPSILON_XX)
  {
    return _DEplastDE(1,1,0,0);
  }
  else if (comp == IPField::DEPLAST_ZZ_DEPSILON_XX)
  {
    return _DEplastDE(2,2,0,0);
  }
  else if (comp == IPField::DEPLAST_XY_DEPSILON_XX)
  {
    return _DEplastDE(0,1,0,0);
  }
  else if (comp == IPField::PLASTIC_ENERGY)
  {
    return _plasticEnergy;
  }
  else
  {
    return dG3DIPVariable::get(comp);
  }
#else
  Msg::Error("NOT COMPILED WITH TORCH");
  return 0;
#endif
}

/* End IPvariable for nonlocalDamageTorchANNDG3DIPVariable*/



nonLocalDamageIsotropicElasticityDG3DIPVariable::nonLocalDamageIsotropicElasticityDG3DIPVariable(const mlawNonlocalDamageHyperelastic &isoElastLaw,
    const bool createBodyForceHO, const bool oninter) : dG3DIPVariable(createBodyForceHO, oninter,1)
{
    _nldIsotropicElasticityipv = new IPNonLocalDamageIsotropicElasticity(isoElastLaw.getCLengthLaw(),isoElastLaw.getDamageLaw());
}

nonLocalDamageIsotropicElasticityDG3DIPVariable::nonLocalDamageIsotropicElasticityDG3DIPVariable(const mlawNonLocalDamageIsotropicElasticity &isoElastLaw,
    const bool createBodyForceHO, const bool oninter) : dG3DIPVariable(createBodyForceHO, oninter,1)
{
    _nldIsotropicElasticityipv = new IPNonLocalDamageIsotropicElasticity(isoElastLaw.getCLengthLaw(),isoElastLaw.getDamageLaw());
}

nonLocalDamageIsotropicElasticityDG3DIPVariable::nonLocalDamageIsotropicElasticityDG3DIPVariable(const nonLocalDamageIsotropicElasticityDG3DIPVariable &source)
    : dG3DIPVariable(source)
{
  _nldIsotropicElasticityipv = NULL;
  if (source.getIPNonLocalDamageIsotropicElasticity() != NULL){
     _nldIsotropicElasticityipv = dynamic_cast<IPNonLocalDamageIsotropicElasticity*>(source.getIPNonLocalDamageIsotropicElasticity()->clone());
  }

}

nonLocalDamageIsotropicElasticityDG3DIPVariable& nonLocalDamageIsotropicElasticityDG3DIPVariable::operator=(const IPVariable &source)
{
  dG3DIPVariable::operator=(source);
  const nonLocalDamageIsotropicElasticityDG3DIPVariable* src = dynamic_cast<const nonLocalDamageIsotropicElasticityDG3DIPVariable*>(&source);
  if(src != NULL)
  {
    if (src->getIPNonLocalDamageIsotropicElasticity() != NULL){
      if (_nldIsotropicElasticityipv!=NULL){
        _nldIsotropicElasticityipv->operator=(*dynamic_cast<const IPVariable*>(src->getIPNonLocalDamageIsotropicElasticity()));
      }
      else
        _nldIsotropicElasticityipv = dynamic_cast<IPNonLocalDamageIsotropicElasticity*>(src->getIPNonLocalDamageIsotropicElasticity()->clone());
    }
    else{
      if (_nldIsotropicElasticityipv!=NULL){
        delete _nldIsotropicElasticityipv;
        _nldIsotropicElasticityipv=NULL;
      }
    }
  }
  return *this;
}

double nonLocalDamageIsotropicElasticityDG3DIPVariable::get(const int i) const
{
    if(i == IPField::DAMAGE)
    {
        return getIPNonLocalDamageIsotropicElasticity()->getDamage();
    }
    else if(i == IPField::LOCAL_EQUIVALENT_STRAINS)
    {
        return getIPNonLocalDamageIsotropicElasticity()->getLocalEffectiveStrains();
    }
    else if(i == IPField::NONLOCAL_EQUIVALENT_STRAINS)
    {
        return getIPNonLocalDamageIsotropicElasticity()->getNonLocalEffectiveStrains();
    }
		else if (i == IPField::ACTIVE_DISSIPATION){
			if (getIPNonLocalDamageIsotropicElasticity()->dissipationIsActive()) return 1.;
			else return 0.;
		}
    else
    {
        return dG3DIPVariable::get(i);
    }
}

double nonLocalDamageIsotropicElasticityDG3DIPVariable::defoEnergy() const
{
    return getIPNonLocalDamageIsotropicElasticity()->defoEnergy();
}
double nonLocalDamageIsotropicElasticityDG3DIPVariable::plasticEnergy() const
{
    return getIPNonLocalDamageIsotropicElasticity()->plasticEnergy();
}


double nonLocalDamageIsotropicElasticityDG3DIPVariable::getConstRefToLocalVariable(const int idex) const
{
    if (idex == 0)
        return getIPNonLocalDamageIsotropicElasticity()->getLocalEffectiveStrains();
    else
        {
            Msg::Error("the non-local variable %d is not defined",idex);
            return 0.;
        }
};
double& nonLocalDamageIsotropicElasticityDG3DIPVariable::getRefToLocalVariable(const int idex)
{
    if (idex == 0)
        return getIPNonLocalDamageIsotropicElasticity()->getRefToLocalEffectiveStrains();
    else
        {
            Msg::Error("the non-local variable %d is not defined",idex);
            static double a=0; return a;
        }
};

double nonLocalDamageIsotropicElasticityDG3DIPVariable::getConstRefToNonLocalVariableInMaterialLaw(const int idex) const
{
    if (idex == 0)
        return getIPNonLocalDamageIsotropicElasticity()->getNonLocalEffectiveStrains();
    else
        {
            Msg::Error("the non-local variable %d is not defined",idex);
            static double a=0; return a;
        }
};

double& nonLocalDamageIsotropicElasticityDG3DIPVariable::getRefToNonLocalVariableInMaterialLaw(const int idex)
{
    if (idex == 0)
        return getIPNonLocalDamageIsotropicElasticity()->getRefToNonLocalEffectiveStrains();
    else
        {
            Msg::Error("the non-local variable %d is not defined",idex);
            static double a=0; return a;
        }
};

void nonLocalDamageIsotropicElasticityDG3DIPVariable::restart()
{
    dG3DIPVariable::restart();
    if (_nldIsotropicElasticityipv != NULL)
      restartManager::restart(_nldIsotropicElasticityipv);
    return;
}






nonLocalDamageJ2HyperDG3DIPVariable::nonLocalDamageJ2HyperDG3DIPVariable(const mlawNonLocalDamageJ2Hyper &_j2law, const bool createBodyForceHO, const bool oninter) :
                                   dG3DIPVariable(createBodyForceHO, oninter,1)
{
  _nldJ2Hyperipv = new IPNonLocalDamageJ2Hyper(_j2law.getJ2IsotropicHardening(),_j2law.getCLengthLaw(),_j2law.getDamageLaw());
}

nonLocalDamageJ2HyperDG3DIPVariable::nonLocalDamageJ2HyperDG3DIPVariable(const mlawNonLocalDamageJ2SmallStrain &_j2law, const bool createBodyForceHO, const bool oninter) :
                                   dG3DIPVariable(createBodyForceHO, oninter,1)
{
  _nldJ2Hyperipv = new IPNonLocalDamageJ2Hyper(_j2law.getJ2IsotropicHardening(),_j2law.getCLengthLaw(),_j2law.getDamageLaw());
}

nonLocalDamageJ2HyperDG3DIPVariable::nonLocalDamageJ2HyperDG3DIPVariable(const nonLocalDamageJ2HyperDG3DIPVariable &source) : dG3DIPVariable(source)
{
  _nldJ2Hyperipv = NULL;
  if (source.getIPNonLocalDamageJ2Hyper() != NULL){
    _nldJ2Hyperipv = dynamic_cast<IPNonLocalDamageJ2Hyper*>(source.getIPNonLocalDamageJ2Hyper()->clone());
  }
}

nonLocalDamageJ2HyperDG3DIPVariable& nonLocalDamageJ2HyperDG3DIPVariable::operator=(const IPVariable &source)
{
  dG3DIPVariable::operator=(source);
  const nonLocalDamageJ2HyperDG3DIPVariable* src = dynamic_cast<const nonLocalDamageJ2HyperDG3DIPVariable*>(&source);
  if(src != NULL)
  {
    if (src->getIPNonLocalDamageJ2Hyper() != NULL){
      if (_nldJ2Hyperipv != NULL)
        _nldJ2Hyperipv->operator=(*dynamic_cast<const IPVariable*>(src->getIPNonLocalDamageJ2Hyper()));
      else
        _nldJ2Hyperipv = dynamic_cast<IPNonLocalDamageJ2Hyper*>(src->getIPNonLocalDamageJ2Hyper()->clone());
    }
    else{
      if (_nldJ2Hyperipv != NULL) {
        delete _nldJ2Hyperipv;
        _nldJ2Hyperipv= NULL;
      }
    }

  }
  return *this;
}
double nonLocalDamageJ2HyperDG3DIPVariable::get(const int i) const
{
  if(i == IPField::PLASTICSTRAIN)
  {
    return getIPNonLocalDamageJ2Hyper()->getConstRefToEquivalentPlasticStrain();
  }
  else if(i == IPField::DAMAGE)
  {
    return getIPNonLocalDamageJ2Hyper()->getDamage();
  }
	else if (i == IPField::ACTIVE_DISSIPATION){
		if (getIPNonLocalDamageJ2Hyper()->dissipationIsActive()) return 1.;
		else return 0.;
	}
  else
  {
    return dG3DIPVariable::get(i);
  }
}
double nonLocalDamageJ2HyperDG3DIPVariable::defoEnergy() const
{
  return getIPNonLocalDamageJ2Hyper()->defoEnergy();
}
double nonLocalDamageJ2HyperDG3DIPVariable::plasticEnergy() const
{
  return getIPNonLocalDamageJ2Hyper()->plasticEnergy();
}


double nonLocalDamageJ2HyperDG3DIPVariable::getConstRefToLocalVariable(const int idex) const {
  if (idex == 0)
    return getIPNonLocalDamageJ2Hyper()->getConstRefToEquivalentPlasticStrain();
  else{
    Msg::Error("the non-local variable %d is not defined",idex);
    static double a=0; return a;
  }
};
double& nonLocalDamageJ2HyperDG3DIPVariable::getRefToLocalVariable(const int idex){
  if (idex == 0)
    return getIPNonLocalDamageJ2Hyper()->getRefToEquivalentPlasticStrain();
  else{
    Msg::Error("the non-local variable %d is not defined",idex);
    static double a=0; return a;
  }
};

double nonLocalDamageJ2HyperDG3DIPVariable::getConstRefToNonLocalVariableInMaterialLaw(const int idex) const{
  if (idex == 0)
    return getIPNonLocalDamageJ2Hyper()->getEffectivePlasticStrain();
  else{
    Msg::Error("the non-local variable %d is not defined",idex);
    static double a=0; return a;
  }
};
double& nonLocalDamageJ2HyperDG3DIPVariable::getRefToNonLocalVariableInMaterialLaw(const int idex){
  if (idex == 0)
    return getIPNonLocalDamageJ2Hyper()->getRefToEffectivePlasticStrain();
  else{
    Msg::Error("the non-local variable %d is not defined",idex);
    static double a=0; return a;
  }
};

void nonLocalDamageJ2HyperDG3DIPVariable::restart()
{
  dG3DIPVariable::restart();
  if (_nldJ2Hyperipv != NULL)
    restartManager::restart(_nldJ2Hyperipv);
  return;
}
//
nonLocalPorosityDG3DIPVariable::nonLocalPorosityDG3DIPVariable(const bool createBodyForceHO, const bool oninter, const int numLocalVar, const int numExtraDofDiffusion):
  dG3DIPVariable(createBodyForceHO, oninter,numLocalVar, numExtraDofDiffusion),_nldPorousipv(NULL){};

nonLocalPorosityDG3DIPVariable::nonLocalPorosityDG3DIPVariable(const mlawNonLocalPorosity &law,const double fvInitial, const bool createBodyForceHO, const bool oninter, const int numLocalVar, const int numExtraDofDiffusion):
  dG3DIPVariable(createBodyForceHO, oninter,numLocalVar, numExtraDofDiffusion)
{
  _nonlocalMethod = law.getNonLocalMethod();
  _nldPorousipv = NULL;
  law.createIPVariable(fvInitial,_nldPorousipv);
}

nonLocalPorosityDG3DIPVariable::nonLocalPorosityDG3DIPVariable(const mlawNonLocalPorosity &law, const bool createBodyForceHO, const bool oninter, const int numLocalVar,
                                    const int numExtraDofDiffusion) :
                                   dG3DIPVariable(createBodyForceHO, oninter,numLocalVar, numExtraDofDiffusion)
{
  _nonlocalMethod = law.getNonLocalMethod();
  _nldPorousipv = NULL;
  law.createIPVariable(law.generateARandomInitialPorosityValue(),_nldPorousipv);
}

nonLocalPorosityDG3DIPVariable::nonLocalPorosityDG3DIPVariable(const nonLocalPorosityDG3DIPVariable &source) : dG3DIPVariable(source)
{
  _nonlocalMethod = source._nonlocalMethod;
  _nldPorousipv  = NULL;
  if (source.getIPNonLocalPorosity() != NULL){
    _nldPorousipv = dynamic_cast<IPNonLocalPorosity*>(source.getIPNonLocalPorosity()->clone());
  }

}

nonLocalPorosityDG3DIPVariable& nonLocalPorosityDG3DIPVariable::operator=(const IPVariable &source)
{
  dG3DIPVariable::operator=(source);
  const nonLocalPorosityDG3DIPVariable* src = dynamic_cast<const nonLocalPorosityDG3DIPVariable*>(&source);
  if(src != NULL)
  {
    _nonlocalMethod = src->_nonlocalMethod;
    if (src->getIPNonLocalPorosity() != NULL){
      if (_nldPorousipv != NULL)
        _nldPorousipv->operator=(*dynamic_cast<const IPVariable*>(src->getIPNonLocalPorosity()));
      else
        _nldPorousipv = dynamic_cast<IPNonLocalPorosity*>(src->getIPNonLocalPorosity()->clone());
    }
    else{
      if (_nldPorousipv!=NULL){
        delete _nldPorousipv;
        _nldPorousipv = NULL;
      }
    }
  }
  return *this;
}
double nonLocalPorosityDG3DIPVariable::get(const int i) const
{
  if(i == IPField::PLASTICSTRAIN)
  {
    return getIPNonLocalPorosity()->getLocalMatrixPlasticStrain();
  }
  else if (i == IPField::ISO_YIELD or
           i == IPField::ISO_YIELD_TENSILE or
           i == IPField::ISO_YIELD_COMPRESSION)
  {
    return getIPNonLocalPorosity()->getConstRefToIPJ2IsotropicHardening().getR();
  }
  else if (i== IPField::PLASTIC_INSTABILITY_VAL)
  {
    return getIPNonLocalPorosity()->getConstRefToIPJ2IsotropicHardening().getWp();
  }
  else if (i == IPField::VOLUMETRIC_PLASTIC_STRAIN){
    return getIPNonLocalPorosity()->getLocalVolumetricPlasticStrain();
  }
  else if (i == IPField::DEVIATORIC_PLASTIC_STRAIN){
    return getIPNonLocalPorosity()->getLocalDeviatoricPlasticStrain();
  }
  else if (i == IPField::NONLOCAL_POROSITY){
    return getIPNonLocalPorosity()->getNonLocalPorosity();
  }
  else if (i == IPField::NUCLEATED_POROSITY_TOT){
    return getIPNonLocalPorosity()->getNucleatedPorosity();
  }
  else if (i == IPField::NUCLEATED_POROSITY_0){
    return getIPNonLocalPorosity()->getNucleatedPorosity(0);
  }
  else if (i == IPField::NUCLEATED_POROSITY_1){
    return getIPNonLocalPorosity()->getNucleatedPorosity(1);
  }
  else if (i == IPField::NUCLEATED_POROSITY_2){
    return getIPNonLocalPorosity()->getNucleatedPorosity(2);
  }
  else if (i == IPField::NUCLEATION_ONSET_0){
    if(getIPNonLocalPorosity()->getNucleationOnset(0)) return 1.;
    else return 0.;
  }
  else if (i == IPField::NUCLEATION_ONSET_1){
    if(getIPNonLocalPorosity()->getNucleationOnset(1)) return 1.;
    else return 0.;
  }
  else if (i == IPField::NUCLEATION_ONSET_2){
    if(getIPNonLocalPorosity()->getNucleationOnset(2)) return 1.;
    else return 0.;
  }
  else if(i == IPField::DAMAGE)
  {
    return getIPNonLocalPorosity()->getDamage();
  }
  else if(i == IPField::LOCAL_POROSITY)
  {
    return getIPNonLocalPorosity()->getLocalPorosity();
  }
  else if (i == IPField::CORRECTED_POROSITY)
  {
    return getIPNonLocalPorosity()->getCorrectedPorosity();
  }
  else if (i == IPField::YIELD_POROSITY){
    return getIPNonLocalPorosity()->getYieldPorosity();
  }
  else if (i == IPField::LIGAMENT_RATIO)
  {
    return getIPNonLocalPorosity()->getConstRefToIPVoidState().getVoidLigamentRatio();
  }
  else if (i == IPField::ASPECT_RATIO){
    return getIPNonLocalPorosity()->getConstRefToIPVoidState().getVoidAspectRatio();
  }
  else if (i == IPField::SHAPE_FACTOR){
    return getIPNonLocalPorosity()->getConstRefToIPVoidState().getVoidShapeFactor();
  }
  else if (i == IPField::COALESCENCE_ACTIVE)
  {
    if (getIPNonLocalPorosity()->getConstRefToIPCoalescence().getCoalescenceActiveFlag()){
      return 1.;
    }
    else return 0.;
  }
  else if ( i == IPField::COALESCENCE){
    if (getIPNonLocalPorosity()->getConstRefToIPCoalescence().getCoalescenceOnsetFlag()){
      return 1.;
    }
    else return 0.;
  }
  else if (i == IPField::COALESCENCE_NECKING)
  {
    if (getIPNonLocalPorosity()->getConstRefToIPCoalescence().getCoalescenceOnsetFlag() and
        getIPNonLocalPorosity()->getConstRefToIPCoalescence().getCoalescenceMode() == 1){
      return 1.;
    }
    else return 0.;
  }
  else if (i == IPField::COALESCENCE_SHEAR)
  {
    if (getIPNonLocalPorosity()->getConstRefToIPCoalescence().getCoalescenceOnsetFlag() and
        getIPNonLocalPorosity()->getConstRefToIPCoalescence().getCoalescenceMode() == 2){
      return 1.;
    }
    else return 0.;
  }
  else if (i == IPField::POROSITY_COALESCENCE_ONSET){
    if (getIPNonLocalPorosity()->getConstRefToIPCoalescence().getCoalescenceOnsetFlag())
      return getIPNonLocalPorosity()->getConstRefToIPCoalescence().getIPvAtCoalescenceOnset()->getYieldPorosity();
    else
      return 0.;
  }
  else if (i == IPField::LIGAMENT_RATIO_COALESCENCE_ONSET){
    if (getIPNonLocalPorosity()->getConstRefToIPCoalescence().getCoalescenceOnsetFlag())
      return getIPNonLocalPorosity()->getConstRefToIPCoalescence().getIPvAtCoalescenceOnset()->getConstRefToIPVoidState().getVoidLigamentRatio();
    else return 0.;
  }
  else if (i == IPField::ASPECT_RATIO_COALESCENCE_ONSET){
    if (getIPNonLocalPorosity()->getConstRefToIPCoalescence().getCoalescenceOnsetFlag())
      return getIPNonLocalPorosity()->getConstRefToIPCoalescence().getIPvAtCoalescenceOnset()->getConstRefToIPVoidState().getVoidAspectRatio();
    else return 0.;
  }
  else if (i == IPField::SHAPE_FACTOR_COALESCENCE_ONSET){
    if (getIPNonLocalPorosity()->getConstRefToIPCoalescence().getCoalescenceOnsetFlag())
      return getIPNonLocalPorosity()->getConstRefToIPCoalescence().getIPvAtCoalescenceOnset()->getConstRefToIPVoidState().getVoidShapeFactor();
    else return 0.;
  }
	else if (i == IPField::FAILED){
		if (getIPNonLocalPorosity()->isFailed()) return 1.;
		else return 0.;
	}
  else if (i == IPField::K_XX){
    return getIPNonLocalPorosity()->getConstRefToCorotationalKirchhoffStress()(0,0);
  }
  else if (i == IPField::K_YY){
    return getIPNonLocalPorosity()->getConstRefToCorotationalKirchhoffStress()(1,1);
  }
  else if (i == IPField::K_ZZ){
    return getIPNonLocalPorosity()->getConstRefToCorotationalKirchhoffStress()(2,2);
  }
  else if (i == IPField::K_XY){
    return getIPNonLocalPorosity()->getConstRefToCorotationalKirchhoffStress()(0,1);
  }
  else if (i == IPField::K_YZ){
    return getIPNonLocalPorosity()->getConstRefToCorotationalKirchhoffStress()(1,2);
  }
  else if (i == IPField::K_XZ){
    return getIPNonLocalPorosity()->getConstRefToCorotationalKirchhoffStress()(0,2);
  }
  else if(i == IPField::TEMPERATURE && getNumConstitutiveExtraDofDiffusionVariable()==1)
  {
    return getConstRefToField(0);
  }
  else if(i == IPField::THERMALFLUX_X && getNumConstitutiveExtraDofDiffusionVariable()==1)
  {
    return getConstRefToFlux()[0](0);
  }
  else if(i == IPField::THERMALFLUX_Y && getNumConstitutiveExtraDofDiffusionVariable()==1)
  {
    return getConstRefToFlux()[0](1);
  }
  else if(i == IPField::THERMALFLUX_Z && getNumConstitutiveExtraDofDiffusionVariable()==1)
  {
    return getConstRefToFlux()[0](2);
  }
  else if (i == IPField::FIELD_SOURCE && getNumConstitutiveExtraDofDiffusionVariable()==1){
    return getConstRefToFieldSource()(0);
  }
  else if (i == IPField::MECHANICAL_SOURCE && getNumConstitutiveExtraDofDiffusionVariable()==1){
    return getConstRefToMechanicalSource()(0);
  }
  else if (i == IPField::ACTIVE_DISSIPATION){
    if (getIPNonLocalPorosity()->dissipationIsActive()){
      return 1.;
    }
    else{
      return 0.;
    }
  }
  else if (i == IPField::LOST_ELLIPTICITY){
    double cr; 
    static SVector3 dir;
    static STensor43 DPDF;
    DPDF = this->getConstRefToTangentModuli();
    for (int h=0; h>this->getNumberNonLocalVariable(); h++){
      const STensor3& dPdlocVar = this->getConstRefToDStressDNonLocalVariable()[h];
      const STensor3& dlocVardF = this->getConstRefToDLocalVariableDStrain()[h];
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              DPDF(i,j,k,l) += dPdlocVar(i,j)*dlocVardF(k,l);
            }
          }
        }
      }
    }
    FailureDetection::getLostSolutionUniquenessCriterion(2, DPDF, dir, cr);
    return cr;
  }
  else
  {
    return dG3DIPVariable::get(i);
  }
  return 0.;
}
double nonLocalPorosityDG3DIPVariable::defoEnergy() const
{
  return getIPNonLocalPorosity()->defoEnergy();
}
double nonLocalPorosityDG3DIPVariable::plasticEnergy() const
{
  return getIPNonLocalPorosity()->plasticEnergy();
}

double nonLocalPorosityDG3DIPVariable::getConstRefToLocalVariable(const int idex) const{
  if (this->getNumberNonLocalVariable() > 0){
    if (_nonlocalMethod == mlawNonLocalPorosity::NONLOCAL_POROSITY){
      if (idex == 0){
        return getIPNonLocalPorosity()->getLocalPorosity();
      }
      else{
        Msg::Error("the non-local variable %d is not defined",idex);
      }
    }
    else if (_nonlocalMethod == mlawNonLocalPorosity::NONLOCAL_LOG_POROSITY){
      if (idex == 0){
        return getIPNonLocalPorosity()->getLocalLogarithmPorosity();
      }
      else{
        Msg::Error("the non-local variable %d is not defined",idex);
      }
    }
    else if (_nonlocalMethod == mlawNonLocalPorosity::NONLOCAL_FULL_THREEVAR){
      if (idex == 0){
        return getIPNonLocalPorosity()->getLocalVolumetricPlasticStrain();
      }
      else if (idex == 1){
        return getIPNonLocalPorosity()->getLocalMatrixPlasticStrain();
      }
      else if (idex == 2){
        return getIPNonLocalPorosity()->getLocalDeviatoricPlasticStrain();
      }
      else{
        Msg::Error("the non-local variable %d is not defined",idex);
      }
    }
    else{
      Msg::Error("nonlocal method has not correctly defined");
    }

  }
  return 0.;
};
double& nonLocalPorosityDG3DIPVariable::getRefToLocalVariable(const int idex){
  if (this->getNumberNonLocalVariable() > 0){
    if (_nonlocalMethod == mlawNonLocalPorosity::NONLOCAL_POROSITY){
      if (idex == 0){
        return getIPNonLocalPorosity()->getRefToLocalPorosity();
      }
      else{
        Msg::Error("the non-local variable %d is not defined",idex);
      }
    }
    else if (_nonlocalMethod == mlawNonLocalPorosity::NONLOCAL_LOG_POROSITY){
      if (idex == 0){
        return getIPNonLocalPorosity()->getRefToLocalLogarithmPorosity();
      }
      else{
        Msg::Error("the non-local variable %d is not defined",idex);
      }
    }
    else if (_nonlocalMethod == mlawNonLocalPorosity::NONLOCAL_FULL_THREEVAR){
      if (idex == 0){
        return getIPNonLocalPorosity()->getRefToLocalVolumetricPlasticStrain();
      }
      else if (idex == 1){
        return getIPNonLocalPorosity()->getRefToLocalMatrixPlasticStrain();
      }
      else if (idex == 2){
        return getIPNonLocalPorosity()->getRefToLocalDeviatoricPlasticStrain();
      }
      else{
        Msg::Error("the non-local variable %d is not defined",idex);
        static double a=0; return a;
      }
    }
    else{
      Msg::Error("nonlocal method has not correctly defined");
      static double a=0; return a;
    }
  }
  else{
    Msg::Error("reference to nonlocal var does not exist");
    static double a=0; return a;
  }
  static double a=0; return a;
};

double nonLocalPorosityDG3DIPVariable::getConstRefToNonLocalVariableInMaterialLaw(const int idex) const{
  if (this->getNumberNonLocalVariable() > 0){
    if (_nonlocalMethod == mlawNonLocalPorosity::NONLOCAL_POROSITY){
      if (idex == 0){
        return getIPNonLocalPorosity()->getNonLocalPorosity();
      }
      else{
        Msg::Error("the non-local variable %d is not defined",idex);
        static double a=0; return a;
      }
    }
    else if (_nonlocalMethod == mlawNonLocalPorosity::NONLOCAL_LOG_POROSITY){
      if (idex == 0){
        return getIPNonLocalPorosity()->getNonLocalLogarithmPorosity();
      }
      else{
        Msg::Error("the non-local variable %d is not defined",idex);
        static double a=0; return a;
      }
    }
    else if (_nonlocalMethod == mlawNonLocalPorosity::NONLOCAL_FULL_THREEVAR){
      if (idex == 0){
        return getIPNonLocalPorosity()->getNonLocalVolumetricPlasticStrain();
      }
      else if (idex == 1){
        return getIPNonLocalPorosity()->getNonLocalMatrixPlasticStrain();
      }
      else if (idex == 2){
        return getIPNonLocalPorosity()->getNonLocalDeviatoricPlasticStrain();
      }
      else{
        Msg::Error("the non-local variable %d is not defined",idex);
        static double a=0; return a;
      }
    }
    else{
      Msg::Error("nonlocal method has not correctly defined");
      static double a=0; return a;
    }
  }
  else{
    return 0.;
  }
  return 0.;
};
double& nonLocalPorosityDG3DIPVariable::getRefToNonLocalVariableInMaterialLaw(const int idex){
  if (this->getNumberNonLocalVariable() > 0){
    if (_nonlocalMethod == mlawNonLocalPorosity::NONLOCAL_POROSITY){
      if (idex == 0){
        return getIPNonLocalPorosity()->getRefToNonLocalPorosity();
      }
      else{
        Msg::Error("the non-local variable %d is not defined",idex);
        static double a=0; return a;
      }
    }
    else if (_nonlocalMethod == mlawNonLocalPorosity::NONLOCAL_LOG_POROSITY){
      if (idex == 0){
        return getIPNonLocalPorosity()->getRefToNonLocalLogarithmPorosity();
      }
      else{
        Msg::Error("the non-local variable %d is not defined",idex);
        static double a=0; return a;
      }
    }
    else if (_nonlocalMethod == mlawNonLocalPorosity::NONLOCAL_FULL_THREEVAR){
      if (idex == 0){
        return getIPNonLocalPorosity()->getRefToNonLocalVolumetricPlasticStrain();
      }
      else if (idex == 1){
        return getIPNonLocalPorosity()->getRefToNonLocalMatrixPlasticStrain();
      }
      else if (idex == 2){
        return getIPNonLocalPorosity()->getRefToNonLocalDeviatoricPlasticStrain();
      }
      else{
        Msg::Error("the non-local variable %d is not defined",idex);
        static double a=0; return a;
      }
    }
    else{
      Msg::Error("nonlocal method has not correctly defined");
      static double a=0; return a;
    }
  }
  else{
    Msg::Error("reference to nonlocal var does not exist");
    static double a=0; return a;
  }
  static double a=0; return a;
};


void nonLocalPorosityDG3DIPVariable::restart()
{
  dG3DIPVariable::restart();
  if (_nldPorousipv != NULL)
    restartManager::restart(_nldPorousipv);
  return;
};

nonLocalPorosityWithCleavageDG3DIPVariable::nonLocalPorosityWithCleavageDG3DIPVariable(const mlawNonLocalPorousWithCleavageFailure &law,const double fvInitial, const bool createBodyForceHO, const bool oninter, const int numLocalVar, const int numExtraDofDiffusion):
  nonLocalPorosityDG3DIPVariable(createBodyForceHO, oninter,numLocalVar, numExtraDofDiffusion)
{
  _nonlocalMethod = law.getNonLocalPorosityLaw()->getNonLocalMethod();
  if(getNumConstitutiveExtraDofDiffusionVariable()==0){
    _nldPorousipv = NULL;
    law.createIPVariable(fvInitial,_nldPorousipv);
    _cleavageNonLocalIPv  = static_cast<IPNonLocalPorosityWithCleavage*>(_nldPorousipv);
  }
  else if(getNumConstitutiveExtraDofDiffusionVariable()==1){
    Msg::Error("thermomechanical has not been implemented");
  }
}

nonLocalPorosityWithCleavageDG3DIPVariable::nonLocalPorosityWithCleavageDG3DIPVariable(const nonLocalPorosityWithCleavageDG3DIPVariable &source):
    nonLocalPorosityDG3DIPVariable(source){};
nonLocalPorosityWithCleavageDG3DIPVariable& nonLocalPorosityWithCleavageDG3DIPVariable::operator=(const IPVariable &source){
  nonLocalPorosityDG3DIPVariable::operator=(source);
  const nonLocalPorosityWithCleavageDG3DIPVariable* psrc = dynamic_cast<const nonLocalPorosityWithCleavageDG3DIPVariable*>(&source);
  if (psrc != NULL){

  }
  return *this;
};
nonLocalPorosityWithCleavageDG3DIPVariable::~nonLocalPorosityWithCleavageDG3DIPVariable(){}

double nonLocalPorosityWithCleavageDG3DIPVariable::get(const int i) const{
  if (i == IPField::DAMAGE){
    return _cleavageNonLocalIPv->getConstRefToIPDamageCleavage().getDamage();
  }
  else{
    return nonLocalPorosityDG3DIPVariable::get(i);
  }
};

double nonLocalPorosityWithCleavageDG3DIPVariable::getConstRefToLocalVariable(const int idex) const{
  int numNonlocalVar = this->getNumberNonLocalVariable();
  if (idex == numNonlocalVar-1){
    return _cleavageNonLocalIPv->getConstRefToCleavageMatrixPlasticStrain();
  }
  else{
    return nonLocalPorosityDG3DIPVariable::getConstRefToLocalVariable(idex);
  }
};
double& nonLocalPorosityWithCleavageDG3DIPVariable::getRefToLocalVariable(const int idex){
  int numNonlocalVar = this->getNumberNonLocalVariable();
  if (idex == numNonlocalVar-1){
    return _cleavageNonLocalIPv->getRefToCleavageMatrixPlasticStrain();
  }
  else{
    return nonLocalPorosityDG3DIPVariable::getRefToLocalVariable(idex);
  }
};

double nonLocalPorosityWithCleavageDG3DIPVariable::getConstRefToNonLocalVariableInMaterialLaw(const int idex) const{
  int numNonlocalVar = this->getNumberNonLocalVariable();
  if (idex == numNonlocalVar-1){
    return _cleavageNonLocalIPv->getConstRefToNonLocalCleavageMatrixPlasticStrain();
  }
  else{
    return nonLocalPorosityDG3DIPVariable::getConstRefToNonLocalVariableInMaterialLaw(idex);
  }
};
double& nonLocalPorosityWithCleavageDG3DIPVariable::getRefToNonLocalVariableInMaterialLaw(const int idex){
  int numNonlocalVar = this->getNumberNonLocalVariable();
  if (idex == numNonlocalVar-1){
    return _cleavageNonLocalIPv->getRefToNonLocalCleavageMatrixPlasticStrain();
  }
  else{
    return nonLocalPorosityDG3DIPVariable::getRefToNonLocalVariableInMaterialLaw(idex);
  };
};

const STensor3 &nonLocalPorosityWithCleavageDG3DIPVariable::getConstRefToCharacteristicLengthMatrix(const int idex) const{
  int numNonlocalVar = this->getNumberNonLocalVariable();
  if (idex == numNonlocalVar-1){
    return _cleavageNonLocalIPv->getConstRefToCharacteristicLengthCleavage();
  }
  else{
    return nonLocalPorosityDG3DIPVariable::getConstRefToCharacteristicLengthMatrix(idex);
  }
};

void nonLocalPorosityWithCleavageDG3DIPVariable::restart(){
  nonLocalPorosityDG3DIPVariable::restart();
  _cleavageNonLocalIPv = dynamic_cast<IPNonLocalPorosityWithCleavage*>(_nldPorousipv);
};


nonLocalDamageJ2FullyCoupledDG3DIPVariable::nonLocalDamageJ2FullyCoupledDG3DIPVariable(const mlawNonLocalDamageJ2FullyCoupledThermoMechanics &_j2law, 
                         const bool createBodyForceHO, const bool oninter) : dG3DIPVariable(createBodyForceHO, oninter,1,1), extraDofIPVariableBase()
{
  _nldJ2Hyperipv = new IPNonLocalDamageJ2FullyCoupledThermoMechanics(_j2law.getJ2IsotropicHardening(),_j2law.getCLengthLaw(),_j2law.getDamageLaw());
}

nonLocalDamageJ2FullyCoupledDG3DIPVariable::nonLocalDamageJ2FullyCoupledDG3DIPVariable(const nonLocalDamageJ2FullyCoupledDG3DIPVariable &source) : dG3DIPVariable(source), extraDofIPVariableBase()
{
  _nldJ2Hyperipv = NULL;
  if (source.getIPNonLocalDamageJ2FullyCoupledThermoMechanics() != NULL){
    _nldJ2Hyperipv = dynamic_cast<IPNonLocalDamageJ2FullyCoupledThermoMechanics*>(source.getIPNonLocalDamageJ2FullyCoupledThermoMechanics()->clone());
  }
}

nonLocalDamageJ2FullyCoupledDG3DIPVariable& nonLocalDamageJ2FullyCoupledDG3DIPVariable::operator=(const IPVariable &source)
{
  dG3DIPVariable::operator=(source);
  const nonLocalDamageJ2FullyCoupledDG3DIPVariable* src = dynamic_cast<const nonLocalDamageJ2FullyCoupledDG3DIPVariable*>(&source);
  if(src != NULL)
  {
    if (src->getIPNonLocalDamageJ2FullyCoupledThermoMechanics() != NULL){
      if (_nldJ2Hyperipv != NULL)
        _nldJ2Hyperipv->operator=(*dynamic_cast<const IPVariable*>(src->getIPNonLocalDamageJ2FullyCoupledThermoMechanics()));
      else
        _nldJ2Hyperipv = dynamic_cast<IPNonLocalDamageJ2FullyCoupledThermoMechanics*>(src->getIPNonLocalDamageJ2FullyCoupledThermoMechanics()->clone());
    }
    else{
      if (_nldJ2Hyperipv != NULL) {
        delete _nldJ2Hyperipv;
        _nldJ2Hyperipv= NULL;
      }
    }

  }
  return *this;
}
double nonLocalDamageJ2FullyCoupledDG3DIPVariable::get(const int i) const
{
  if(i == IPField::PLASTICSTRAIN)
  {
    return getIPNonLocalDamageJ2FullyCoupledThermoMechanics()->getConstRefToEquivalentPlasticStrain();
  }
  else if(i == IPField::DAMAGE)
  {
    return getIPNonLocalDamageJ2FullyCoupledThermoMechanics()->getDamage();
  }
  else if (i == IPField::ACTIVE_DISSIPATION){
    if (getIPNonLocalDamageJ2FullyCoupledThermoMechanics()->dissipationIsActive()) return 1.;
      else return 0.;
  }
  else if (i == IPField::LOCAL_0){
    if (getNumberNonLocalVariable()>0){
      return getConstRefToLocalVariable(0);
    }
    else return 0.;
  }
  else if (i == IPField::NONLOCAL_0){
    if (getNumberNonLocalVariable() > 0){
      return getConstRefToNonLocalVariableInMaterialLaw(0);
    }
    else return 0.;
  }
  else if(i == IPField::TEMPERATURE)
  {
    return getConstRefToField(0);
  }
  else if(i == IPField::THERMALFLUX_X)
  {
    return getConstRefToFlux()[0](0);
  }
  else if(i == IPField::THERMALFLUX_Y)
  {
    return getConstRefToFlux()[0](1);
  }
  else if(i == IPField::THERMALFLUX_Z)
  {
    return getConstRefToFlux()[0](2);
  }
  else if (i == IPField::FIELD_SOURCE){
    return getConstRefToFieldSource()(0);
  }
  else if (i == IPField::MECHANICAL_SOURCE){
    return getConstRefToMechanicalSource()(0);
  }
  else
  {
    return dG3DIPVariable::get(i);
  }
}
double nonLocalDamageJ2FullyCoupledDG3DIPVariable::defoEnergy() const
{
  return getIPNonLocalDamageJ2FullyCoupledThermoMechanics()->defoEnergy();
}
double nonLocalDamageJ2FullyCoupledDG3DIPVariable::plasticEnergy() const
{
  return getIPNonLocalDamageJ2FullyCoupledThermoMechanics()->plasticEnergy();
}


double nonLocalDamageJ2FullyCoupledDG3DIPVariable::getConstRefToLocalVariable(const int idex) const {
  if (idex == 0)
    return getIPNonLocalDamageJ2FullyCoupledThermoMechanics()->getConstRefToEquivalentPlasticStrain();
  else{
    Msg::Error("the non-local variable %d is not defined",idex);
    static double a=0; return a;
  }
};
double& nonLocalDamageJ2FullyCoupledDG3DIPVariable::getRefToLocalVariable(const int idex){
  if (idex == 0)
    return getIPNonLocalDamageJ2FullyCoupledThermoMechanics()->getRefToEquivalentPlasticStrain();
  else{
    Msg::Error("the non-local variable %d is not defined",idex);
    static double a=0; return a;
  }
};

double nonLocalDamageJ2FullyCoupledDG3DIPVariable::getConstRefToNonLocalVariableInMaterialLaw(const int idex) const{
  if (idex == 0)
    return getIPNonLocalDamageJ2FullyCoupledThermoMechanics()->getEffectivePlasticStrain();
  else{
    Msg::Error("the non-local variable %d is not defined",idex);
    static double a=0; return a;
  }
};
double& nonLocalDamageJ2FullyCoupledDG3DIPVariable::getRefToNonLocalVariableInMaterialLaw(const int idex){
  if (idex == 0)
    return getIPNonLocalDamageJ2FullyCoupledThermoMechanics()->getRefToEffectivePlasticStrain();
  else{
    Msg::Error("the non-local variable %d is not defined",idex);
    static double a=0; return a;
  }
};

void nonLocalDamageJ2FullyCoupledDG3DIPVariable::restart()
{
  dG3DIPVariable::restart();
  if (_nldJ2Hyperipv != NULL)
    restartManager::restart(_nldJ2Hyperipv);
  return;
}


genericCrackPhaseFieldDG3DIPVariable::genericCrackPhaseFieldDG3DIPVariable(IPGenericCrackPhaseField *pfipv,
   int _nlVar, const bool createBodyForceHO, const bool oninter) : dG3DIPVariable(createBodyForceHO,oninter,_nlVar)
{
  _pfipv = pfipv;
}

genericCrackPhaseFieldDG3DIPVariable::genericCrackPhaseFieldDG3DIPVariable(const genericCrackPhaseFieldDG3DIPVariable &source) :
                                                                   dG3DIPVariable(source)
{
  _pfipv = NULL;
  // if (source.getRefToIPGenericCrackPhaseField() != NULL){
  if (source._pfipv != NULL){
    // _pfipv = new IPGenericCrackPhaseField(*(source.getRefToIPGenericCrackPhaseField()));
    _pfipv = new IPGenericCrackPhaseField(*(source._pfipv));
    // _pfipv->operator= (*dynamic_cast<const IPVariable*>(source.getRefToIPGenericCrackPhaseField()));
    // _pfipv->operator= (*dynamic_cast<const IPVariable*>(source._pfipv));
  }

}

genericCrackPhaseFieldDG3DIPVariable& genericCrackPhaseFieldDG3DIPVariable::operator=(const IPVariable &source)
{
  dG3DIPVariable::operator=(source);
  const genericCrackPhaseFieldDG3DIPVariable* src = dynamic_cast<const genericCrackPhaseFieldDG3DIPVariable*>(&source);
  if(src != NULL)
  {
    // // if (src->getRefToIPGenericCrackPhaseField() != NULL){
    // if (src->_pfipv != NULL){
    //   if (_pfipv != NULL){
    //     // _pfipv->operator=(*dynamic_cast<const IPVariable*>(src->getRefToIPGenericCrackPhaseField()));
    //     _pfipv->operator=(*dynamic_cast<const IPVariable*>(src->_pfipv));
    //   }
    //   else{
    //     // _pfipv = dynamic_cast<genericCrackPhaseFieldDG3DIPVariable*>(src->getRefToIPGenericCrackPhaseField()->clone());
    //     _pfipv->operator=(*dynamic_cast<genericCrackPhaseFieldDG3DIPVariable*>(src->_pfipv->clone()));
    //   }
    // }
    // else{
    //   if (_pfipv != NULL){
    //     delete _pfipv;
    //     _pfipv = NULL;
    //   }
    // }
    _pfipv->operator=((dynamic_cast<const IPVariable&>(*src->_pfipv)));
  }
  return *this;
}
double genericCrackPhaseFieldDG3DIPVariable::get(const int i) const
{
  double val = getConstRefToIPGenericCrackPhaseField()->get(i);
  if (val == 0.)
    val = dG3DIPVariable::get(i);
  return val;
}

double genericCrackPhaseFieldDG3DIPVariable::defoEnergy() const
{
  return getConstRefToIPGenericCrackPhaseField()->defoEnergy();
  //return getConstRefToIpMeca()->defoEnergy();
}
double genericCrackPhaseFieldDG3DIPVariable::plasticEnergy() const
{
  //return getConstRefToIpMeca()->plasticEnergy();
  return getConstRefToIPGenericCrackPhaseField()->plasticEnergy();
}

void genericCrackPhaseFieldDG3DIPVariable::restart()
{
  dG3DIPVariable::restart();
  // if (_pfipv != NULL)
  //   restartManager::restart(_pfipv);
  // return;
  _pfipv->restart();
  return;
}

const IPVariableMechanics& genericCrackPhaseFieldDG3DIPVariable::getConstRefToIpMeca() const
{
    return _pfipv->getConstRefToIpMeca();
}
IPVariableMechanics& genericCrackPhaseFieldDG3DIPVariable::getRefToIpMeca()
{
    return _pfipv->getRefToIpMeca();
}
void genericCrackPhaseFieldDG3DIPVariable::setIpMeca(IPVariable& ipMeca)
{
    _pfipv->setIpMeca(ipMeca);
}

double genericCrackPhaseFieldDG3DIPVariable::getConstRefToLocalVariable(const int idex) const {
  if (idex == 0)
    return getConstRefToIPGenericCrackPhaseField()->getConstRefToNonLocalDamageVariable();
  else{
    Msg::Error("the non-local variable %d is not defined",idex);    
  }
    return getConstRefToIPGenericCrackPhaseField()->getConstRefToNonLocalDamageVariable();
};
double& genericCrackPhaseFieldDG3DIPVariable::getRefToLocalVariable(const int idex) {
  if (idex == 0)
    return getRefToIPGenericCrackPhaseField()->getRefToLocalDamageVariable();
  else{
    Msg::Error("the non-local variable %d is not defined",idex);
  }
    return getRefToIPGenericCrackPhaseField()->getRefToLocalDamageVariable();
};

double genericCrackPhaseFieldDG3DIPVariable::getConstRefToNonLocalVariableInMaterialLaw(const int idex) const{
  if (idex == 0)
    return getConstRefToIPGenericCrackPhaseField()->getConstRefToNonLocalDamageVariable();
  else{
    Msg::Error("the non-local variable %d is not defined",idex);
  }
    return getConstRefToIPGenericCrackPhaseField()->getConstRefToNonLocalDamageVariable();
};
double& genericCrackPhaseFieldDG3DIPVariable::getRefToNonLocalVariableInMaterialLaw(const int idex) {
  if (idex == 0)
    return getRefToIPGenericCrackPhaseField()->getRefToNonLocalDamageVariable();
  else{
    Msg::Error("the non-local variable %d is not defined",idex);
  }
    return getRefToIPGenericCrackPhaseField()->getRefToNonLocalDamageVariable();
};





