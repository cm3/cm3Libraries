#ifndef DG3DCOHESIVEBANDMATERIALLAW_H_
#define DG3DCOHESIVEBANDMATERIALLAW_H_

#include "dG3DCohesiveMaterialLaw.h"
#include "CoalescenceLaw.h"
#include "mlawNonLocalPorous.h"
#include "nonLocalDamageDG3DMaterialLaw.h"

class CohesiveBand3DLaw :public Cohesive3DLaw{
#ifndef SWIG
  protected:
    bool _damageBasedCriterion;     // true if cohesive insert based on Damage, false if stress
    bool _useInitialFmean;          // true if normal part of F is blocked at insertion time
    double _Dc;                     // critial damage allows crack transtion
    const double _sigmac;           // critical stress for crack transition
    double _h;                      // cohesive band thickness
    const double _Kp;               // penalty parameter
    const double _mu;               // friction coefficient
    const double _fscmin,_fscmax;   // boundary value for uncertainty on sigma_c (default min = max = 1)
    const double _beta;             // ratio between mode I and II
    double _gradFactor;             // weight factor associated to spatial gradient of jump in F computation
    double _deltaOffset;            // jump offset for unloading

#endif // SWIG
  public:
    CohesiveBand3DLaw(const int num, const double sigmac, const double Dc, const double h, const double mu,
                        const double fractureStrengthFactorMin, const double fractureStrengthFactorMax, const double beta,
                        const double Kp, const bool damageBasedCriterion, double gradFactor, const bool useInitialFmean, const double deltaOffset);

  #ifndef SWIG
    CohesiveBand3DLaw(const CohesiveBand3DLaw& src);
    virtual ~CohesiveBand3DLaw();

    virtual void initLaws(const std::map<int,materialLaw*> &maplaw);
    virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;

    // General access functions
    virtual double getKp() const {return _Kp;};
    virtual double getKn() const {return _Kp;};
    virtual double getGc() const{Msg::Error("This function is not used"); return 0;}
    virtual double getSigmac() const{return _sigmac;}
    virtual double getBeta() const{return _beta;}
    virtual double getMu() const{return _mu;}
    virtual double getFractureStrengthFactorMin() const{return _fscmin;}
    virtual double getFractureStrengthFactorMax() const{return _fscmax;}

    // Proper functions
    virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent =false);
    virtual void checkCohesiveInsertion(IPStateBase* ipsm, IPStateBase* ipsp, const bool forcedInsert = false) const;
    virtual void transferInterfaceDataToBulk(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff) const;

    // Properties functions
    virtual double getCharacteristicLength() const {return _h;};
		virtual materialLaw* clone() const {return new CohesiveBand3DLaw(*this);};
		virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const;
  #endif // SWIG
};


class PorosityCohesiveBand3DLaw : public Cohesive3DLaw{
    #ifndef SWIG
  protected:
    // Cohesive band parameters
    double _h;                        // cohesive band thickness
    double _Kp;                       // penalty parameter to avoid penetration
    double _gradFactor;
    // Options and numerical parameters
    bool _accountNormalBulk;          //false if the normal part of bulk defo is not accounted for in interfacical defo
                                        // true otherwise (==all the components of the bulk F is used...)
    bool _enabledNegativeJumpN_limit; // true if the normal jump is limited in the negative values
    double _maxPenetrationjumpN;      // maximal (negative!) value of admissible penetration jump
    bool _enableDeletionOfBadConditionedIPVs; // true if bad-conditionned ipvs are deleted for convergence purpose
    
    double _crackDelayingFactor;
    
    const NonLocalPorousDuctileDamageDG3DMaterialLaw* _bulkLaw;
    #endif

  public:
    PorosityCohesiveBand3DLaw(const int num, const double h, const double Kp, const bool accountNormalBulk=true);
    void accountJumpGradientAtInterfaces(const double factor);
    void setMaximalAdmissiblePenetrationJump(bool activationFlag, double maxNegJump=-1.);
    void setDeletionOfBadConditionnedIPV(bool activationFlag);
    void setCrackDelayingFactor(double fact);
    void setBulkLaw(const NonLocalPorousDuctileDamageDG3DMaterialLaw& bulkLaw);
    #ifndef SWIG
    PorosityCohesiveBand3DLaw(const PorosityCohesiveBand3DLaw& src);
    virtual ~PorosityCohesiveBand3DLaw(){};
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw);
    virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPVariable* &ipv,bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;

    // General access functions
    virtual double getKp() const {return _Kp;};
    virtual double getKn() const {return _Kp;};
    // Proper functions
    virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff=true, const bool checkfrac=true, const bool dTangent =false);
    virtual void checkCohesiveInsertion(IPStateBase* ipsm, IPStateBase* ipsp, const bool forcedInsert = false) const;
    virtual void transferInterfaceDataToBulk(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff) const;

    // Properties functions
    virtual double getCharacteristicLength() const {return _h;};
		virtual materialLaw* clone() const {return new PorosityCohesiveBand3DLaw(*this);};
		virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvp) const;
    #endif //SWIG
};


class PorosityWithLossOfEllipcitiyCohesiveBand3DLaw : public PorosityCohesiveBand3DLaw{
    #ifndef SWIG
protected:
    bool    _crackWithCoales;  // ==True if coalescence is required for crack insertion (by default, ==false)
    double  _tolOnCriterion;   // Value for -det(N.DP/DF.N) at which crack insertion is allowed
    #endif

public:
    PorosityWithLossOfEllipcitiyCohesiveBand3DLaw(const int num, const double h, const double Kp, const bool accountNormalBulk);
    virtual void setToleranceOnCriterion(const double newTolconst, const bool crackWithCoales=false);
    #ifndef SWIG
    PorosityWithLossOfEllipcitiyCohesiveBand3DLaw(const PorosityWithLossOfEllipcitiyCohesiveBand3DLaw& src);
    virtual ~PorosityWithLossOfEllipcitiyCohesiveBand3DLaw(){
    };

    // Proper functions
    virtual void checkCohesiveInsertion(IPStateBase* ipsm, IPStateBase* ipsp, const bool forcedInsert = false) const;

    // Properties functions
		virtual materialLaw* clone() const {return new PorosityWithLossOfEllipcitiyCohesiveBand3DLaw(*this);};
    #endif //SWIG
};
#endif // DG3DCOHESIVEBANDMATERIALLAW_H_
