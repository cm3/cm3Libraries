//
// C++ Interface: Function Space
//
// Description: FunctionSpace used (curl function space in 3D)
//
//
// Author:  <V-D Nguyen>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef DG3DCURLFUNCTIONSPACE_H_
#define DG3DCURLFUNCTIONSPACE_H_
#include "GModel.h"
#include "MElement.h"
#include "functionSpace.h"
#include "MInterfaceElement.h"
#include "dG3DDof3IntType.h"
#include "CurlFunctionSpace.h"

class TwoNum;

class g3DHierarchicalCurlFunctionSpace : public ScalarHierarchicalCurlFunctionSpace, public FunctionSpaceBase
{
  protected:
    int _iField;
    int _ncomp; // num of componsnt
    std::vector<int> _comp; // all components
    
    static bool initMap;
    static std::map<TwoNum, int> allEdges;
    static void numerateEdge();
    static int getEdgeNumber(int verNum1, int verNum2);
    
  public :
    g3DHierarchicalCurlFunctionSpace(int id, int ncomp, int order) :FunctionSpaceBase(), ScalarHierarchicalCurlFunctionSpace(order), _iField(id), _ncomp(ncomp)
    {
      _comp.resize(ncomp);
      for (int i=0; i<ncomp; i++)
      {
        _comp[i] = i;
      }
    }
    g3DHierarchicalCurlFunctionSpace(int id, int ncomp, int c, int order) : FunctionSpaceBase(), ScalarHierarchicalCurlFunctionSpace(order),_iField(id), _ncomp(1)
    {
      _comp.resize(1);
      _comp[0]= c;
    };
    virtual ~g3DHierarchicalCurlFunctionSpace(){};
    
    virtual void updateCompIndex(const int numComp)
    {
      Msg::Info("update curl comp indexes, all curl comps start from %d",numComp);
      for (int i=0; i<_ncomp; i++)
      {
        _comp[i] += numComp;
      }
    }
    
    virtual int getId(void) const {return _iField;};
    
    virtual int getNumComp() const {return _ncomp;};
    virtual void getComp(std::vector<int> &comp) const 
    {
      comp.resize(_comp.size());
      std::copy(_comp.begin(),_comp.end(),comp.begin());
    }
    
    virtual int getNumShapeFunctions(MElement* ele, int c) const 
    {
      // equal numer of edge
      return ele->getNumEdges();
    }
    virtual int getShapeFunctionsIndex(MElement* ele, int c) const
    {
      int num = 0;
      bool found = false;
      for (int i=0; i< _comp.size(); i++)
      {
        if (c == _comp[i])
        {
          found = true;
          break;
        }
        num += getNumShapeFunctions(ele,_comp[i]);
      }
      
      if (!found)
      {
        Msg::Error("field index %d does not exist",c);
      };
      
      return num;
    };
    
    virtual int getTotalNumShapeFunctions(MElement* ele) const
    {
      int num = 0;
      for (int i=0; i< _comp.size(); i++)
      {
        num += getNumShapeFunctions(ele,_comp[i]);
      }
      return num;
    };
    
    virtual int getNumKeys(MElement *ele) const 
    {
      return _ncomp*ele->getNumEdges();
    }
    virtual void getKeys(MElement *ele, std::vector<Dof> &keys) const
    {
      numerateEdge();
      // get number of edges in element
      int nk=ele->getNumEdges();
      // negative type in mpi if the Dof are not located on this partition
      #if defined(HAVE_MPI) // small duplication to avoid multiple if in a loop
      if( (ele->getPartition() != 0) and (ele->getPartition() != Msg::GetCommRank() +1))
      {
        for (int j=0;j<_ncomp;++j)
        {
          for (int i=0;i<nk;++i)
          {
            std::vector<MVertex*> vedges;
            ele->getEdgeVertices(i,vedges);
            int num = g3DHierarchicalCurlFunctionSpace::getEdgeNumber(vedges[0]->getNum(),vedges[1]->getNum());
            #if defined(HAVE_CG_MPI_INTERFACE)
            keys.push_back(Dof(num,-dG3DDof3IntType::createTypeWithThreeInts(_comp[j],_iField,ele->getPartition())));
            #else
            keys.push_back(Dof(num,dG3DDof3IntType::createTypeWithThreeInts(_comp[j],_iField,0)));
            #endif
          }
        }
      }
      else
      #endif // HAVE_MPI
      {
        for (int j=0;j<_ncomp;++j)
        {
          for (int i=0;i<nk;++i){
            std::vector<MVertex*> vedges;
            ele->getEdgeVertices(i,vedges);
            int num = g3DHierarchicalCurlFunctionSpace::getEdgeNumber(vedges[0]->getNum(),vedges[1]->getNum());
            #if defined(HAVE_CG_MPI_INTERFACE)
            keys.push_back(Dof(num,dG3DDof3IntType::createTypeWithThreeInts(_comp[j],_iField,ele->getPartition())));
            #else
            keys.push_back(Dof(num,dG3DDof3IntType::createTypeWithThreeInts(_comp[j],_iField,0)));
            #endif
          }
        }
      }
    }
    virtual FunctionSpaceBase *clone(const int id) const
    {
      if (_ncomp == 1) return new g3DHierarchicalCurlFunctionSpace(id,1,_comp[0],getOrderCurlBasis());
      else
        return new g3DHierarchicalCurlFunctionSpace(getId(),_ncomp,getOrderCurlBasis());
    }; // copy space with new Id
};


class g3DDirichletBoundaryConditionHierarchicalCurlFunctionSpace : public g3DHierarchicalCurlFunctionSpace
{
  public:
    g3DDirichletBoundaryConditionHierarchicalCurlFunctionSpace(int id, int ncomp, int order) : g3DHierarchicalCurlFunctionSpace(id, ncomp, order){}
    g3DDirichletBoundaryConditionHierarchicalCurlFunctionSpace(int id, int ncomp, int comp1, int order) : g3DHierarchicalCurlFunctionSpace(id,ncomp,comp1,order){}
  
    virtual ~g3DDirichletBoundaryConditionHierarchicalCurlFunctionSpace(){};
	
    virtual FunctionSpaceBase* clone(const int id) const
    {
      if (_ncomp == 1) return new g3DDirichletBoundaryConditionHierarchicalCurlFunctionSpace(id,1,_comp[0],getOrderCurlBasis());
      else return new g3DDirichletBoundaryConditionHierarchicalCurlFunctionSpace(id,_ncomp,getOrderCurlBasis());
    }

    virtual void getKeys(MElement *ele, std::vector<Dof> &keys) const
    {
      numerateEdge();
      int nk=ele->getNumEdges();
      // negative type in mpi if the Dof are not located on this partition
      // For BC on edge in the case of CG in // ele->getPartition() is equal to 0.
      // Therefore generate more keys to be sure to fix the dofs (There will be extra fixed dof but normally it doesn't matter)
      #if defined(HAVE_MPI) // small duplication to avoid multiple if in a loop
      if( (ele->getPartition() != 0) and (ele->getPartition() != Msg::GetCommRank() +1))
      {
        if( (ele->getPartition() != 0)) // Sure than it will match
        {
          for (int j=0;j<_ncomp;++j)
          {
            for (int i=0;i<nk;++i)
            {
              std::vector<MVertex*> vedges;
              ele->getEdgeVertices(i,vedges);
              int num = g3DHierarchicalCurlFunctionSpace::getEdgeNumber(vedges[0]->getNum(),vedges[1]->getNum());
              #if defined(HAVE_CG_MPI_INTERFACE)
              keys.push_back(Dof(num,-dG3DDof3IntType::createTypeWithThreeInts(_comp[j],_iField,ele->getPartition())));
              #else
              keys.push_back(Dof(num,dG3DDof3IntType::createTypeWithThreeInts(_comp[j],_iField,0)));
              #endif
            }
          }
        }
        else // match is not ensured --> generate keys for each partitions
        {
          #if defined(HAVE_CG_MPI_INTERFACE)
          for(int p=0;p<=Msg::GetCommSize();p++)
          {
            for (int j=0;j<_ncomp;++j)
            {
              for (int i=0;i<nk;++i)
              {
                std::vector<MVertex*> vedges;
                ele->getEdgeVertices(i,vedges);
                int num = g3DHierarchicalCurlFunctionSpace::getEdgeNumber(vedges[0]->getNum(),vedges[1]->getNum());
                keys.push_back(Dof(num,-dG3DDof3IntType::createTypeWithThreeInts(_comp[j],_iField,p)));
              }
            }
          }
          #else
          for (int j=0;j<_ncomp;++j)
          {
            for (int i=0;i<nk;++i)
            {
              std::vector<MVertex*> vedges;
              ele->getEdgeVertices(i,vedges);
              int num = g3DHierarchicalCurlFunctionSpace::getEdgeNumber(vedges[0]->getNum(),vedges[1]->getNum());
              keys.push_back(Dof(num,dG3DDof3IntType::createTypeWithThreeInts(_comp[j],_iField,0)));
            }
          }
          #endif
        }      
      }
      else
      #endif // HAVE_MPI
      {
        for (int j=0;j<_ncomp;++j)
        {
          for (int i=0;i<nk;++i){
            std::vector<MVertex*> vedges;
            ele->getEdgeVertices(i,vedges);
            int num = g3DHierarchicalCurlFunctionSpace::getEdgeNumber(vedges[0]->getNum(),vedges[1]->getNum());
            #if defined(HAVE_CG_MPI_INTERFACE)
            keys.push_back(Dof(num,dG3DDof3IntType::createTypeWithThreeInts(_comp[j],_iField,ele->getPartition())));
            #else
            keys.push_back(Dof(num,dG3DDof3IntType::createTypeWithThreeInts(_comp[j],_iField,0)));
            #endif
          }
        }
      }
    }
};


class g3DNeumannBoundaryConditionHierarchicalCurlFunctionSpace : public g3DHierarchicalCurlFunctionSpace
{
  protected:
  bool _parallelMesh;
  void setParallelMesh()
  {
  #if defined(HAVE_MPI)
    if(GModel::current()->getNumPartitions()>0)
     {
       _parallelMesh = true;
     }
    else
  #endif // HAVE_MPI
   {
     _parallelMesh = false;
   }
  }
  public:
    g3DNeumannBoundaryConditionHierarchicalCurlFunctionSpace(int id, int ncomp, int order) : g3DHierarchicalCurlFunctionSpace(id, ncomp, order){this->setParallelMesh();}
    g3DNeumannBoundaryConditionHierarchicalCurlFunctionSpace(int id, int ncomp, int comp1, int order) : g3DHierarchicalCurlFunctionSpace(id,ncomp,comp1,order){this->setParallelMesh();}
  
    virtual ~g3DNeumannBoundaryConditionHierarchicalCurlFunctionSpace(){};
    
	
	virtual FunctionSpaceBase* clone(const int id) const{
		if (_ncomp == 1) return new g3DNeumannBoundaryConditionHierarchicalCurlFunctionSpace(id,1,_comp[0],getOrderCurlBasis());
    else return new g3DNeumannBoundaryConditionHierarchicalCurlFunctionSpace(id,_ncomp,getOrderCurlBasis());
	}

  virtual void getKeys(MElement *ele, std::vector<Dof> &keys) const
  {
    numerateEdge();
    // For Neumann BC we sure that the BC is applied on element that are on this partition.
    // Indeed Neumann BC are never applied on ghost element
    int p = (_parallelMesh) ? Msg::GetCommRank()+1 : 0;
    // get number of edges in element
    int nk=ele->getNumEdges();
    
    for (int j=0;j<_ncomp;++j)
    {
      for (int i=0;i<nk;++i){
        std::vector<MVertex*> vedges;
        ele->getEdgeVertices(i,vedges);
        int num = g3DHierarchicalCurlFunctionSpace::getEdgeNumber(vedges[0]->getNum(),vedges[1]->getNum());
        #if defined(HAVE_CG_MPI_INTERFACE)
        keys.push_back(Dof(num,dG3DDof3IntType::createTypeWithThreeInts(_comp[j],_iField,p)));
        #else
        keys.push_back(Dof(num,dG3DDof3IntType::createTypeWithThreeInts(_comp[j],_iField,0)));
        #endif
      }
    }
  }
};

#endif // DG3DCURLFUNCTIONSPACE_H_
