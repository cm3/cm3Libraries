//
// C++ Interface: axisymmetric
//
// Description: Interface class for dg 3D for axisymmetric problems
//
// Author:  <V.-D. Nguyen>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "axisymmetricDG3DDomain.h"
#include "STensorOperations.h"
#include "axisymmetricDG3DTerms.h"
#include "nonLinearMechSolver.h"
#include "dG3DHomogenizedTangentTerms.h"
#include "pathFollowingTerms.h"
#include "nonLocalDamageDG3DIPVariable.h"
#include "axisymmetricDG3DPathFollowingTerms.h"
#include "FractureCohesiveDG3DMaterialLaw.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

axisymmetricDG3DDomain::axisymmetricDG3DDomain(const int tag, const int phys, const int sp_, const int lnum, const int fdg, const int dim, int numNL, int nonConstitutiveExtraDOFVar):
       dG3DDomain(tag,phys,sp_,lnum,fdg,dim,numNL,nonConstitutiveExtraDOFVar){};

axisymmetricDG3DDomain::axisymmetricDG3DDomain(const axisymmetricDG3DDomain& src): dG3DDomain(src){};

void axisymmetricDG3DDomain::allocateInterfaceMPI(const int rankOtherPart, const elementGroup &groupOtherPart,dgPartDomain** dgdom)
{
  // domain creation has it function create a domain with a part of the same domain contained on an other part,
  // the type of domain is known
  if(*dgdom==NULL){ // domain doesn't exist --> creation
    (*dgdom) = new axisymmetricInterDomainBetween3D(this->getTag(),this,this,0);
    // set the stability parameters
    axisymmetricInterDomainBetween3D* newInterDom3D = static_cast< axisymmetricInterDomainBetween3D* >(*dgdom);
    newInterDom3D->stabilityParameters(this->stabilityParameter(1));
    // set other options from domain
    newInterDom3D->setBulkDamageBlockedMethod(_bulkDamageBlockMethod);
    newInterDom3D->strainSubstep(_subSteppingMethod,_subStepNumber);
    newInterDom3D->forceCohesiveInsertionAllIPs(_forceInterfaceElementBroken,_failureIPRatio);
    newInterDom3D->setNonLocalStabilityParameters(this->getNonLocalStabilityParameter(),this->getNonLocalContinuity());
    newInterDom3D->setNonLocalEqRatio(this->getNonLocalEqRatio());

    newInterDom3D->setConstitutiveExtraDofDiffusionStabilityParameters(this->getConstitutiveExtraDofDiffusionStabilityParameter(), this->getConstitutiveExtraDofDiffusionContinuity());
    newInterDom3D->setConstitutiveExtraDofDiffusionEqRatio(this->getConstitutiveExtraDofDiffusionEqRatio());
    newInterDom3D->setConstitutiveExtraDofDiffusionUseEnergyConjugatedField(this->getConstitutiveExtraDofDiffusionUseEnergyConjugatedField());
    newInterDom3D->setConstitutiveExtraDofDiffusionAccountSource(this->getConstitutiveExtraDofDiffusionAccountFieldSource(),this->getConstitutiveExtraDofDiffusionAccountMecaSource());

    newInterDom3D->setIncrementNonlocalBased(this->_incrementNonlocalBased);
  }
}

void axisymmetricDG3DDomain::computeStrain(MElement *e, const int npts_bulk, IntPt *GP,
                               AllIPState::ipstateElementContainer *vips,
                               IPStateBase::whichState ws,
                               fullVector<double> &disp, bool useBarF) const
{
	if (!getElementErosionFilter()(e)) return;

  /***
   * DISPLACEMENT FIELD
   * */
  int nbFF = e->getNumShapeFunctions();
  //Msg::Info("nb bulk gp %d",npts_bulk);
  for(int j=0;j<npts_bulk;j++)
  {
    IPStateBase* ips = (*vips)[j];
    dG3DIPVariableBase *ipv = static_cast<dG3DIPVariableBase*>(ips->getState(ws));

    const std::vector<TensorialTraits<double>::ValType> &Vals = ipv->f(_space,e,GP[j]);
    // get grad of shape function from Gauss point
    const std::vector<TensorialTraits<double>::GradType> &Grads = ipv->gradf(_space,e,GP[j]);
    //x y z
    STensor3& F = ipv->getRefToDeformationGradient();
    STensorOperation::unity(F);

    SPoint3 p;
    e->pnt(GP[j].pt[0],GP[j].pt[1],GP[j].pt[2],p);
    
    for (int i = 0; i < nbFF; i++){
      // rr
      F(0,0) += Grads[i+0*nbFF][0]*disp(i);
      // rz
      F(0,2) += Grads[i+0*nbFF][2]*disp(i);
      // theta-theta
      F(1,1) += Vals[i+0*nbFF]*disp(i)/p[0];
      //zr
      F(2,0) += Grads[i+0*nbFF][0]*disp(i+2*nbFF);
      //zz
      F(2,2) += Grads[i+0*nbFF][2]*disp(i+2*nbFF);

    }
    #ifdef _DEBUG
    if(STensorOperation::determinantSTensor3(F) < 1.e-15) Msg::Error("Negative Jacobian");
    #endif //_DEBUG
  }
  
  if(useBarF)
  {
    Msg::Error("bar F method not implemented for axisymmetric cases");
  }
  
  /**
   * NONLOCAL FIELD
   **/
  for(int j=0;j<npts_bulk;j++)
  {
    IPStateBase* ips = (*vips)[j];
    dG3DIPVariableBase *ipv = static_cast<dG3DIPVariableBase*>(ips->getState(ws));

    const std::vector<TensorialTraits<double>::ValType> &Vals = ipv->f(_space,e,GP[j]);
    // get grad of shape function from Gauss point
    const std::vector<TensorialTraits<double>::GradType> &Grads = ipv->gradf(_space,e,GP[j]);

    // non local var
    if( ipv->getNumberNonLocalVariable()>getNumNonLocalVariable() ) Msg::Error("Your material law use more non local variable than your domain");
    for (int nl = 0; nl < ipv->getNumberNonLocalVariable(); nl++)
    {
      double& nonLocalVar = ipv->getRefToNonLocalVariable(nl);
      SVector3& gradNonlocalVar = ipv->getRefToGradNonLocalVariable()[nl];
      nonLocalVar = 0.;
      STensorOperation::zero(gradNonlocalVar);
      for (int i = 0; i < nbFF; i++){
        nonLocalVar += Vals[i+3*nbFF+nl*nbFF]*disp(i+3*nbFF+nl*nbFF);
        gradNonlocalVar(0) += Grads[i+3*nbFF+nl*nbFF][0]*disp(i+3*nbFF+nl*nbFF);
        gradNonlocalVar(2) += Grads[i+3*nbFF+nl*nbFF][2]*disp(i+3*nbFF+nl*nbFF);
      }
    }
  }
  
  // compute nonlocal var for law
  if (getNumNonLocalVariable() > 0)
  {
    // make equal
    for(int j=0;j<npts_bulk;j++){
      IPStateBase* ips = (*vips)[j];
      dG3DIPVariableBase *ipv = static_cast<dG3DIPVariableBase*>(ips->getState(ws));
      for (int nl = 0; nl < ipv->getNumberNonLocalVariable(); nl++)
      {
        ipv->getRefToNonLocalVariableInMaterialLaw(nl) = ipv->getConstRefToNonLocalVariable(nl);
      }
    }
  }
};


void axisymmetricDG3DDomain::computeStrain(AllIPState *aips,MInterfaceElement *ie, IntPt *GP,const IPStateBase::whichState ws,
                                        partDomain* efMinus, partDomain *efPlus, fullVector<double> &dispm,
                                        fullVector<double> &dispp, const bool virt, bool useBarF)
{
  // Compute strain at the interface element
    // get element
  MElement *ele = dynamic_cast<MElement*>(ie);

  // Erosion: if the element is eroded, do nothing and return
  if (!getElementErosionFilter()(ele)) return;

  if(!virt)
  {
    FunctionSpaceBase* _spaceminus = efMinus->getFunctionSpace();
    FunctionSpaceBase* _spaceplus = efPlus->getFunctionSpace();

    int npts=integBound->getIntPoints(ele,&GP);

    MElement *em = ie->getElem(0);
    MElement *ep = ie->getElem(1);

    int nbFFm = em->getNumVertices();
    int nbFFp = ep->getNumVertices();

    // Get Gauss points values on minus and plus elements
    IntPt *GPm; IntPt *GPp;
    _interQuad->getIntPoints(ie,GP,&GPm,&GPp);


    // Compute the jump and the displacmement (at the mean surface) at the interface at each nodes
    // from the nodal values
    fullVector<double> dispinter, jumpinter;
    int nbdofinter = _space->getNumKeys(ele);
    int nbvertexInter = ele->getNumVertices();

      // Minus part
    std::vector<int> vm;
    vm.resize(nbvertexInter);
    ie->getLocalVertexNum(0,vm);
    dispinter.resize(nbdofinter);
    jumpinter.resize(nbdofinter);
    for(int i=0;i<nbvertexInter;i++){
      dispinter(i) = 0.5*dispm(vm[i]);
      dispinter(i+nbvertexInter) = 0;
      dispinter(i+2*nbvertexInter) = 0.5*dispm(vm[i]+2*nbFFm);
      jumpinter(i) = -dispm(vm[i]);
      jumpinter(i+nbvertexInter) = 0;
      jumpinter(i+2*nbvertexInter) = -dispm(vm[i]+2*nbFFm);
    }
      // Plus part
    std::vector<int> vp;
    vp.resize(nbvertexInter);
    ie->getLocalVertexNum(1,vp);
    for(int i=0;i<nbvertexInter;i++){
      dispinter(i) += 0.5*dispp(vp[i]);
      dispinter(i+2*nbvertexInter) += 0.5*dispp(vp[i]+2*nbFFp);
      jumpinter(i) += dispp(vp[i]);
      jumpinter(i+2*nbvertexInter) += dispp(vp[i]+2*nbFFp);
    }

    // Compute deformation gradient, interface normals and non-local jump,...
    // get grad shape functions values at all Gauss points at the interface in order to compute normal to interfaces
    std::vector<GaussPointSpaceValues<double>*> vgps;
    static_cast<nlsFunctionSpace<double>*>(_spaceminus)->get(ele,npts,GP,vgps);

    // Loop on gauss points
    AllIPState::ipstateElementContainer *vips = aips->getIPstate(ele->getNum());
    
    /***
     * CURRENT AND REEFRENCE NORMAL
     * */
    for(int j=0;j<npts;j++)
    {
      // get ipVariable
      IPStateBase* ipsm = (*vips)[j];
      IPStateBase* ipsp = (*vips)[j+npts];

      dG3DIPVariableBase* ipvm = static_cast<dG3DIPVariableBase*>(ipsm->getState(ws));
      dG3DIPVariableBase* ipvp = static_cast<dG3DIPVariableBase*>(ipsp->getState(ws));
      
      // Compute interface normal basis
      const std::vector<TensorialTraits<double>::GradType> &Grads = vgps[j]->_vgrads;

      // tangent vector at the interface
      static SVector3 referencePhi0[2];
      static SVector3 currentPhi0[2];
      // set vectors to zero first
      STensorOperation::zero(referencePhi0[0]);
      STensorOperation::zero(referencePhi0[1]);
      STensorOperation::zero(currentPhi0[0]);
      STensorOperation::zero(currentPhi0[1]);

      // get tangent vctors in r-direction (== x)  in the plane r-z (== x-z)
      for(int k=0;k<nbvertexInter;k++){
        double x = ele->getVertex(k)->x();
        double y = ele->getVertex(k)->y();
        double z = ele->getVertex(k)->z();
        referencePhi0[0](0) += Grads[k](0)*x;
        referencePhi0[0](2) += Grads[k+nbvertexInter+nbvertexInter](0)*z;
        currentPhi0[0](0) += Grads[k](0)*(x+dispinter(k));
        currentPhi0[0](2) += Grads[k+nbvertexInter+nbvertexInter](0)*(z+dispinter(k+2*nbvertexInter));
      }

      // get tangent vectors in theta-direction (==y), which are in the direction theta (==y)
      referencePhi0[1](1) = 1.;
      currentPhi0[1](1) = 1.;

      // get normal vector from tangents at the interface surface
      (ipvm->getRefToCurrentOutwardNormal())   = crossprod(currentPhi0[0],currentPhi0[1]);
      (ipvm->getRefToReferenceOutwardNormal()) = crossprod(referencePhi0[0],referencePhi0[1]);

      (ipvp->getRefToCurrentOutwardNormal())   = crossprod(currentPhi0[0],currentPhi0[1]);
      (ipvp->getRefToReferenceOutwardNormal()) = crossprod(referencePhi0[0],referencePhi0[1]);
      
      FractureCohesive3DIPVariable *ipvfm = dynamic_cast<FractureCohesive3DIPVariable*>(ipvm);
      FractureCohesive3DIPVariable *ipvfp = dynamic_cast<FractureCohesive3DIPVariable*>(ipvp);
      if((ipvfm!=NULL) and (ipvfp!= NULL) )
      {
        Cohesive3DIPVariableBase* cohipvfm = ipvfm->getIPvFrac();
        Cohesive3DIPVariableBase* cohipvfp = ipvfp->getIPvFrac();
        if (cohipvfm->useLocalBasis())
        {
          cohipvfm->getRefToCohesiveNormal() =ipvm->getConstRefToCurrentOutwardNormal();
          cohipvfm->getRefToCohesiveTangent() = currentPhi0[0];
          cohipvfm->getRefToCohesiveBiTangent() = currentPhi0[1];

          cohipvfm->getRefToCohesiveNormal().normalize();
          cohipvfm->getRefToCohesiveTangent().normalize();

          cohipvfm->getRefToCohesiveReferenceNormal() =ipvm->getConstRefToReferenceOutwardNormal();
          cohipvfm->getRefToCohesiveReferenceTangent() = referencePhi0[0];
          cohipvfm->getRefToCohesiveReferenceBiTangent() = referencePhi0[1];

          cohipvfm->getRefToCohesiveReferenceNormal().normalize();
          cohipvfm->getRefToCohesiveReferenceTangent().normalize();
        }

        if (cohipvfp->useLocalBasis())
        {
          cohipvfp->getRefToCohesiveNormal() =ipvp->getConstRefToCurrentOutwardNormal();
          cohipvfp->getRefToCohesiveTangent() = currentPhi0[0];
          cohipvfp->getRefToCohesiveBiTangent() = currentPhi0[1];

          cohipvfp->getRefToCohesiveNormal().normalize();
          cohipvfp->getRefToCohesiveTangent().normalize();

          cohipvfp->getRefToCohesiveReferenceNormal() =ipvp->getConstRefToReferenceOutwardNormal();
          cohipvfp->getRefToCohesiveReferenceTangent() = referencePhi0[0];
          cohipvfp->getRefToCohesiveReferenceBiTangent() = referencePhi0[1];

          cohipvfp->getRefToCohesiveReferenceNormal().normalize();
          cohipvfp->getRefToCohesiveReferenceTangent().normalize();
        }
      }
    }
    
    /***
     * DISPLACEMENT FIELD
     * */
    for(int j=0;j<npts;j++)
    {
      // get ipVariable
      IPStateBase* ipsm = (*vips)[j];
      IPStateBase* ipsp = (*vips)[j+npts];

      dG3DIPVariableBase* ipvm = static_cast<dG3DIPVariableBase*>(ipsm->getState(ws));
      dG3DIPVariableBase* ipvp = static_cast<dG3DIPVariableBase*>(ipsp->getState(ws));

      // get the position of the gauss points at the interface
      /* is it the correct value (mean surface position) or is it from one side ??????? */
      SPoint3 p;
      ele->pnt(GP[j].pt[0],GP[j].pt[1],GP[j].pt[2],p);

      // get shape function values and gradients in bulk elements
      const std::vector<TensorialTraits<double>::GradType> &Gradm = ipvm->gradf(_spaceminus,em,GPm[j]);
      const std::vector<TensorialTraits<double>::GradType> &Gradp = ipvp->gradf(_spaceplus,ep,GPp[j]);
      const std::vector<TensorialTraits<double>::ValType> &Valm = ipvm->f(_spaceminus,em,GPm[j]);
      const std::vector<TensorialTraits<double>::ValType> &Valp = ipvp->f(_spaceplus,ep,GPp[j]);

      // compute minus deformation gradient
      STensor3& Fm = ipvm->getRefToDeformationGradient();
      STensorOperation::unity(Fm);
      for (int i = 0; i < nbFFm; i++)
      {
        // in coordiniates r-theta-z
          // r-r
        Fm(0,0) += Gradm[i+0*nbFFm][0]*dispm(i);
          // r-z
        Fm(0,2) += Gradm[i+0*nbFFm][2]*dispm(i);
          // theta-theta
        Fm(1,1) += Valm[i+0*nbFFm]*dispm(i)/p[0];
          // z-r
        Fm(2,0) += Gradm[i+0*nbFFm][0]*dispm(i+2*nbFFm);
          // z-z
        Fm(2,2) += Gradm[i+0*nbFFm][2]*dispm(i+2*nbFFm);
      }
      #ifdef _DEBUG
      if(STensorOperation::determinantSTensor3(Fm) < 1.e-15) Msg::Error("Negative Jacobian Fm");
      #endif //_DEBUG
      // compute plus deformation gradient
      STensor3& Fp = ipvp->getRefToDeformationGradient();
      STensorOperation::unity(Fp);
      for (int i = 0; i < nbFFp; i++)
      {
        // in coordiniates r-theta-z
        // r-r
        Fp(0,0) += Gradp[i+0*nbFFp][0]*dispp(i);
        // r-z
        Fp(0,2) += Gradp[i+0*nbFFp][2]*dispp(i);
        // theta-theta
        Fp(1,1) += Valp[i+0*nbFFp]*dispp(i)/p[0];
        // z-r
        Fp(2,0) += Gradp[i+0*nbFFp][0]*dispp(i+2*nbFFp);
        // z-z
        Fp(2,2) += Gradp[i+0*nbFFp][2]*dispp(i+2*nbFFp);
      }
      #ifdef _DEBUG
      if(STensorOperation::determinantSTensor3(Fp) < 1.e-15) Msg::Error("Negative Jacobian Fp");
      #endif //_DEBUG
      //
      SVector3& ujump = ipvm->getRefToJump();
      computeJump(Valm,nbFFm,Valp,nbFFp,dispm,dispp,ujump);
      ujump(1) = 0.;
      ipvp->getRefToJump() = ujump;
    }
    
    if(useBarF)
    {
      Msg::Error("bar F method not implemented for axisymmetric cases");
    }
      
    if (_averageStrainBased and (efMinus->getMaterialLaw()->getNum() == efPlus->getMaterialLaw()->getNum()))
    {
      for(int j=0;j<npts;j++)
      {
        // get ipVariable
        IPStateBase* ipsm = (*vips)[j];
        IPStateBase* ipsp = (*vips)[j+npts];

        dG3DIPVariableBase* ipvm = static_cast<dG3DIPVariableBase*>(ipsm->getState(ws));
        dG3DIPVariableBase* ipvp = static_cast<dG3DIPVariableBase*>(ipsp->getState(ws));
        
        STensor3& Fm = ipvm->getRefToDeformationGradient();
        STensor3& Fp = ipvp->getRefToDeformationGradient();

        // compute Fm as average 0.5*(Fm+Fp)
        // assign Fp = Fm
        Fm += Fp;
        Fm *= (0.5);
        Fp = Fm;
      }
    }
    
    /***
     * NONLOCAL FIELD
     * 
     * */
    for(int j=0;j<npts;j++)
    {
      // get ipVariable
      IPStateBase* ipsm = (*vips)[j];
      IPStateBase* ipsp = (*vips)[j+npts];

      dG3DIPVariableBase* ipvm = static_cast<dG3DIPVariableBase*>(ipsm->getState(ws));
      dG3DIPVariableBase* ipvp = static_cast<dG3DIPVariableBase*>(ipsp->getState(ws));
      
      // get the position of the gauss points at the interface
      /* is it the correct value (mean surface position) or is it from one side ??????? */
      SPoint3 p;
      ele->pnt(GP[j].pt[0],GP[j].pt[1],GP[j].pt[2],p);
      
      // get shape function values and gradients in bulk elements
      const std::vector<TensorialTraits<double>::GradType> &Gradm = ipvm->gradf(_spaceminus,em,GPm[j]);
      const std::vector<TensorialTraits<double>::GradType> &Gradp = ipvp->gradf(_spaceplus,ep,GPp[j]);
      const std::vector<TensorialTraits<double>::ValType> &Valm = ipvm->f(_spaceminus,em,GPm[j]);
      const std::vector<TensorialTraits<double>::ValType> &Valp = ipvp->f(_spaceplus,ep,GPp[j]);
      
      // Compute non-local values and their gradient on both sides and the jump at the interfaces.
      // non-local values and gradient for minus part
      if( ipvm->getNumberNonLocalVariable()>getNumNonLocalVariable() )
        Msg::Error("Your material law uses more non local variables than your domain dG3DDomain::computeStrain negative interface");
      for (int nlm = 0; nlm < ipvm->getNumberNonLocalVariable(); nlm++){
        // get
        double& nonlocalVarm = ipvm->getRefToNonLocalVariable(nlm);
        nonlocalVarm = 0.;
        SVector3& gradNonlocalVarm = ipvm->getRefToGradNonLocalVariable()[nlm];
        STensorOperation::zero(gradNonlocalVarm);

        for (int i = 0; i < nbFFm; i++)
        {
          nonlocalVarm += Valm[i+3*nbFFm+nlm*nbFFm]*dispm(i+3*nbFFm+nlm*nbFFm);
          gradNonlocalVarm(0) += Gradm[i+3*nbFFm+nlm*nbFFm][0]*dispm(i+3*nbFFm+nlm*nbFFm);
          gradNonlocalVarm(1) += Valm[i+3*nbFFm+nlm*nbFFm]*dispm(i+3*nbFFm+nlm*nbFFm)/p[0];
          gradNonlocalVarm(2) += Gradm[i+3*nbFFm+nlm*nbFFm][2]*dispm(i+3*nbFFm+nlm*nbFFm);
        }
      }

      // non-local values and gradient for minus part
      if( ipvp->getNumberNonLocalVariable()>getNumNonLocalVariable() ) Msg::Error("Your material law uses more non local variables than your domain dG3DDomain::computeStrain positive interface");
      for (int nlp = 0; nlp < ipvp->getNumberNonLocalVariable(); nlp++){
        //get
        double& nonlocalVarp = ipvp->getRefToNonLocalVariable(nlp);
        nonlocalVarp = 0.;
        SVector3& gradNonlocalVarp = ipvp->getRefToGradNonLocalVariable()[nlp];
        STensorOperation::zero(gradNonlocalVarp);

        for (int i = 0; i < nbFFp; i++)
        {
          nonlocalVarp += Valp[i+3*nbFFp+nlp*nbFFp]*dispp(i+3*nbFFp+nlp*nbFFp);
          gradNonlocalVarp(0) += Gradp[i+3*nbFFp+nlp*nbFFp][0]*dispp(i+3*nbFFp+nlp*nbFFp);
          gradNonlocalVarp(1) += Valp[i+3*nbFFp+nlp*nbFFp]*dispp(i+3*nbFFp+nlp*nbFFp)/p[0];
          gradNonlocalVarp(2) += Gradp[i+3*nbFFp+nlp*nbFFp][2]*dispp(i+3*nbFFp+nlp*nbFFp);
        }
      }

      // compute the jump
      for (int nl = 0; nl < ipvp->getNumberNonLocalVariable(); nl++){
        double& epsjump= ipvm->getRefToNonLocalJump()(nl);
        computeNonLocalJump(Valm,nbFFm,Valp,nbFFp,dispm,dispp,epsjump,nl);
        ipvp->getRefToNonLocalJump()(nl) = epsjump;
      }
    }
    if (getNumNonLocalVariable() > 0)
    {
 
      for(int j=0;j<npts;j++)
      {
        IPStateBase* ipsm = (*vips)[j];
        IPStateBase* ipsp = (*vips)[j+npts];

        dG3DIPVariableBase* ipvm = static_cast<dG3DIPVariableBase*>(ipsm->getState(ws));
        dG3DIPVariableBase* ipvp = static_cast<dG3DIPVariableBase*>(ipsp->getState(ws));
        for (int nlm = 0; nlm < ipvm->getNumberNonLocalVariable(); nlm++)
        {
          ipvm->getRefToNonLocalVariableInMaterialLaw(nlm) = ipvm->getConstRefToNonLocalVariable(nlm);
        }
        for (int nlp = 0; nlp < ipvp->getNumberNonLocalVariable(); nlp++)
        {
          ipvp->getRefToNonLocalVariableInMaterialLaw(nlp) = ipvp->getConstRefToNonLocalVariable(nlp);
        }
      
      }
    }

    // if _averageStrainBased, avergaed both sides
    if (_averageStrainBased and (efMinus->getMaterialLaw()->getNum() == efPlus->getMaterialLaw()->getNum())){
      for(int j=0;j<npts;j++)
      {
        // get ipVariable
        IPStateBase* ipsm = (*vips)[j];
        IPStateBase* ipsp = (*vips)[j+npts];

        dG3DIPVariableBase* ipvm = static_cast<dG3DIPVariableBase*>(ipsm->getState(ws));
        dG3DIPVariableBase* ipvp = static_cast<dG3DIPVariableBase*>(ipsp->getState(ws));
        
        // loop on n-l variables
        for (int nlv = 0; nlv < ipvm->getNumberNonLocalVariable(); nlv++)
        {
          // average values
          double& valminus = ipvm->getRefToNonLocalVariableInMaterialLaw(nlv);
          valminus += ipvp->getConstRefToNonLocalVariableInMaterialLaw(nlv);
          valminus *= 0.5;
          ipvp->getRefToNonLocalVariableInMaterialLaw(nlv) = valminus;
        }
      }
    }
      
    /***
     * COHESIVE AND FRACTURE
     * */
    for(int j=0;j<npts;j++)
    {
      // get ipVariable
      IPStateBase* ipsm = (*vips)[j];
      IPStateBase* ipsp = (*vips)[j+npts];

      dG3DIPVariableBase* ipvm = static_cast<dG3DIPVariableBase*>(ipsm->getState(ws));
      dG3DIPVariableBase* ipvp = static_cast<dG3DIPVariableBase*>(ipsp->getState(ws));

      FractureCohesive3DIPVariable *ipvfm = dynamic_cast<FractureCohesive3DIPVariable*>(ipvm);
      FractureCohesive3DIPVariable *ipvfp = dynamic_cast<FractureCohesive3DIPVariable*>(ipvp);
      
      STensor3& Fm = ipvm->getRefToDeformationGradient();
      STensor3& Fp = ipvp->getRefToDeformationGradient();
      
      SPoint3 p;
      ele->pnt(GP[j].pt[0],GP[j].pt[1],GP[j].pt[2],p);

      if((ipvfm!=NULL) and (ipvfp!= NULL) ){

        Cohesive3DIPVariableBase* cohipvfm = ipvfm->getIPvFrac();
        Cohesive3DIPVariableBase* cohipvfp = ipvfp->getIPvFrac();

        // store bulk deformation gradient at interface IP
        if (cohipvfm->withDeformationGradient() and cohipvfp->withDeformationGradient()){
          cohipvfm->setBulkDeformationGradient(Fm);
          cohipvfp->setBulkDeformationGradient(Fp);
        }

        // Compute spatial gradient of jump on interface at interface IP
        if (cohipvfm->withJumpGradient() and cohipvfp->withJumpGradient()){
          // get shape function values and gradients at the IP
          const std::vector<TensorialTraits<double>::ValType>& Val = ipvm->f(_spaceminus,ie,GP[j]);
          const std::vector<TensorialTraits<double>::GradType>& gradVal = ipvm->gradf(_spaceminus,ie,GP[j]);
          STensor3& Gradjumpm =  cohipvfm->getRefToJumpGradient();
          STensorOperation::zero(Gradjumpm);

          for(int l=0; l<nbvertexInter; l++)
          {
            // derivatives in r-theta-z coordinates
            Gradjumpm(0,0) += jumpinter(l)*gradVal[l](0); // r-r
            Gradjumpm(0,2) += jumpinter(l)*gradVal[l](2); // r-z

            Gradjumpm(1,1) += jumpinter(l)*Val[l]/p[0]; // theta-theta

            Gradjumpm(2,0) += jumpinter(l+2*nbvertexInter)*gradVal[l+2*nbvertexInter](0); // z-r
            Gradjumpm(2,2) += jumpinter(l+2*nbvertexInter)*gradVal[l+2*nbvertexInter](2); // z-z
          }
          cohipvfp->getRefToJumpGradient() = Gradjumpm;
        };
      }
    }
  }
};


void axisymmetricDG3DDomain::createTerms(unknownField *uf,IPField*ip)
{
  // This function is an adapted copy of dG3DDomain::createTerms
  // get space
  FunctionSpace<double>* dgspace = static_cast<FunctionSpace<double>*>(_space);

  // Get each terms
  // mass term
  massterm = new axisymmetricMass3D(*dgspace,_mlaw,getNumNonLocalVariable());

  // bulk term
  ltermBulk = new axisymmetricDG3DForceBulk(*dgspace,_mlaw,_fullDg,ip,_incrementNonlocalBased);
  g3DLinearTerm<double>* gltermBulk = static_cast<g3DLinearTerm<double>*>(ltermBulk);
  gltermBulk->setDim(_dim);
  if( getNumNonLocalVariable() >0)
  {
    gltermBulk->setNonLocalEqRatio(getNonLocalEqRatio());
    gltermBulk->setNumNonLocalVariable(getNumNonLocalVariable()); 
  }
  if( getNumConstitutiveExtraDofDiffusionVariable() >0)
  {
    gltermBulk->setNumConstitutiveExtraDofDiffusionVariable(getNumConstitutiveExtraDofDiffusionVariable());
    gltermBulk->setConstitutiveExtraDofDiffusionEqRatio(getConstitutiveExtraDofDiffusionEqRatio());
    gltermBulk->setConstitutiveExtraDofDiffusionUseEnergyConjugatedField(getConstitutiveExtraDofDiffusionUseEnergyConjugatedField());
    gltermBulk->setConstitutiveExtraDofDiffusionAccountFieldSource(getConstitutiveExtraDofDiffusionAccountFieldSource());
    gltermBulk->setConstitutiveExtraDofDiffusionAccountMecaSource(getConstitutiveExtraDofDiffusionAccountMecaSource());
  };
  
  // bulk stiffness
  if(_bmbp)
    btermBulk = new BilinearTermPerturbation<double>(ltermBulk,*dgspace,*dgspace,uf,ip,this,_eps);
  else{
    btermBulk = new axisymmetricDG3DStiffnessBulk(*dgspace,_mlaw,_fullDg,ip);
    g3DBilinearTerm<double,double>* gbtermBulk = static_cast<g3DBilinearTerm<double,double>*>(btermBulk);
    gbtermBulk->setDim(_dim);
    if( getNumNonLocalVariable() >0)
    {
      gbtermBulk->setNonLocalEqRatio(getNonLocalEqRatio());
      gbtermBulk->setNumNonLocalVariable(getNumNonLocalVariable()); 
    }
    if( getNumConstitutiveExtraDofDiffusionVariable() >0)
    {
      gbtermBulk->setNumConstitutiveExtraDofDiffusionVariable(getNumConstitutiveExtraDofDiffusionVariable());
      gbtermBulk->setConstitutiveExtraDofDiffusionEqRatio(getConstitutiveExtraDofDiffusionEqRatio());
      gbtermBulk->setConstitutiveExtraDofDiffusionUseEnergyConjugatedField(getConstitutiveExtraDofDiffusionUseEnergyConjugatedField());
      gbtermBulk->setConstitutiveExtraDofDiffusionAccountFieldSource(getConstitutiveExtraDofDiffusionAccountFieldSource());
      gbtermBulk->setConstitutiveExtraDofDiffusionAccountMecaSource(getConstitutiveExtraDofDiffusionAccountMecaSource());
    };
  }

  // DG terms
  if(_fullDg)
  {
    ltermBound = new axisymmetricDG3DForceInter(*dgspace,dgspace,_mlaw,_mlaw,_interQuad,_beta1,_fullDg,ip,_incrementNonlocalBased);
    dG3DLinearTerm<double>* gltermBound = static_cast<dG3DLinearTerm<double>*>(ltermBound);
    gltermBound->setDim(_dim);
    if( getNumNonLocalVariable() >0)
    {
      gltermBound->setNonLocalEqRatio(getNonLocalEqRatio());
      gltermBound->setNumNonLocalVariable(getNumNonLocalVariable());
      gltermBound->setNonLocalStabilityParameter(getNonLocalStabilityParameter());
      gltermBound->setNonLocalContinuity(getNonLocalContinuity());
    }
    if (getNumConstitutiveExtraDofDiffusionVariable() > 0){
      gltermBound->setNumConstitutiveExtraDofDiffusionVariable(getNumConstitutiveExtraDofDiffusionVariable());
      gltermBound->setConstitutiveExtraDofDiffusionEqRatio(getConstitutiveExtraDofDiffusionEqRatio());
      gltermBound->setConstitutiveExtraDofDiffusionUseEnergyConjugatedField(getConstitutiveExtraDofDiffusionUseEnergyConjugatedField());
      gltermBound->setConstitutiveExtraDofDiffusionAccountFieldSource(getConstitutiveExtraDofDiffusionAccountFieldSource());
      gltermBound->setConstitutiveExtraDofDiffusionAccountMecaSource(getConstitutiveExtraDofDiffusionAccountMecaSource());
      gltermBound->setConstitutiveExtraDofDiffusionStabilityParameter(getConstitutiveExtraDofDiffusionStabilityParameter());
      gltermBound->setConstitutiveExtraDofDiffusionContinuity(getConstitutiveExtraDofDiffusionContinuity());
    }

    // Stiffness computation
    if(_interByPert){
      btermBound = new BilinearTermPerturbation<double>(ltermBound,*dgspace,*dgspace,uf,ip,this,_eps);
    }
    else
    {
      btermBound = new axisymmetricDG3DStiffnessInter(*dgspace,*dgspace,_mlaw,_mlaw,_interQuad,_beta1,ip,uf,_fullDg,_incrementNonlocalBased);
      dG3DBilinearTerm<double,double>* gbtermBound = static_cast<dG3DBilinearTerm<double,double>*>(btermBound);
      gbtermBound->setDim(_dim);
      if( getNumNonLocalVariable() >0)
      {
        gbtermBound->setNonLocalEqRatio(getNonLocalEqRatio());
        gbtermBound->setNumNonLocalVariable(getNumNonLocalVariable());
        gbtermBound->setNonLocalStabilityParameter(getNonLocalStabilityParameter());
        gbtermBound->setNonLocalContinuity(getNonLocalContinuity());
      }
      if (getNumConstitutiveExtraDofDiffusionVariable() > 0){
        gbtermBound->setNumConstitutiveExtraDofDiffusionVariable(getNumConstitutiveExtraDofDiffusionVariable());
        gbtermBound->setConstitutiveExtraDofDiffusionEqRatio(getConstitutiveExtraDofDiffusionEqRatio());
        gbtermBound->setConstitutiveExtraDofDiffusionUseEnergyConjugatedField(getConstitutiveExtraDofDiffusionUseEnergyConjugatedField());
        gbtermBound->setConstitutiveExtraDofDiffusionAccountFieldSource(getConstitutiveExtraDofDiffusionAccountFieldSource());
        gbtermBound->setConstitutiveExtraDofDiffusionAccountMecaSource(getConstitutiveExtraDofDiffusionAccountMecaSource());
        gbtermBound->setConstitutiveExtraDofDiffusionStabilityParameter(getConstitutiveExtraDofDiffusionStabilityParameter());
        gbtermBound->setConstitutiveExtraDofDiffusionContinuity(getConstitutiveExtraDofDiffusionContinuity());
      }
    }
  }
  else
  {
     ltermBound = new nonLinearTermVoid();
     btermBound = new BiNonLinearTermVoid();
  }
  
  
  // virtual terms
  this->btermVirtBound = new BiNonLinearTermVoid();
  this->ltermVirtBound = new nonLinearTermVoid();


  // path following
  bool withPF = false;
  if (getMacroSolver() != NULL and _accountPathFollowing)
  {
    withPF = getMacroSolver()->withPathFollowing();
  }
  
  
  if (withPF){

    if (getMacroSolver()->getPathFollowingLocation() == pathFollowingManager::INTERFACE){
      Msg::Error("path following is not implemented for this case");
    }
    scalarTermPFBound = new nonLinearScalarTermVoid();
    linearTermPFBound = new nonLinearTermVoid();

    scalarTermPF = new axisymmetricDG3DDissipationPathFollowingBulkScalarTerm(this,ip);
    linearTermPF = new axisymmetricDG3DDissipationPathFollowingBulkLinearTerm(this,ip);
  }
  else{
    scalarTermPF = new nonLinearScalarTermVoid();;
    linearTermPF = new nonLinearTermVoid();;
    scalarTermPFBound = new nonLinearScalarTermVoid();;
    linearTermPFBound = new nonLinearTermVoid();
  }


  // tangent term is not implemented
  btermElasticBulk = new BiNonLinearTermVoid();
  btermTangent = new BiNonLinearTermVoid();
  btermElasticTangent = new BiNonLinearTermVoid();
  btermUndamagedElasticTangent = new BiNonLinearTermVoid();
}





LinearTermBase<double>*
axisymmetricDG3DDomain::createNeumannTerm(FunctionSpace<double> *spneu,  const elementGroup* g,const simpleFunctionTime<double>* f,
                                   const unknownField *uf,const IPField * ip, 
                                   const nonLinearBoundaryCondition::location onWhat,
                                   const nonLinearNeumannBC::NeumannBCType neumann_type,
                                   const int comp) const
{
  LinearTermBase<double>*term;
  if(neumann_type == nonLinearNeumannBC::PRESSURE)
  {
    term = new axisymmetricG3DFiniteStrainsPressureTerm(*spneu,uf,f);
  }
  else if (neumann_type == nonLinearNeumannBC::FORCE){
    term= new axisymmetricG3DLoadTerm(*spneu,f,comp);
  }
  else{
    Msg::Error("axisymmetricDG3DDomain::createNeumannTerm %s is not defined dG3DDomain::createNeumannTerm",neumann_type);
  }
  g3DLinearTerm<double>* gterm = static_cast<g3DLinearTerm<double>*>(term);
  gterm->setDim(_dim);
  if(getNumNonLocalVariable()>0)
  {
    gterm->setNonLocalEqRatio(getNonLocalEqRatio());
    gterm->setNumNonLocalVariable(getNumNonLocalVariable());
  }
  if( getNumConstitutiveExtraDofDiffusionVariable() >0)
  {
    gterm->setNumConstitutiveExtraDofDiffusionVariable(getNumConstitutiveExtraDofDiffusionVariable());
    gterm->setConstitutiveExtraDofDiffusionEqRatio(getConstitutiveExtraDofDiffusionEqRatio());
    gterm->setConstitutiveExtraDofDiffusionUseEnergyConjugatedField(getConstitutiveExtraDofDiffusionUseEnergyConjugatedField());
    gterm->setConstitutiveExtraDofDiffusionAccountFieldSource(getConstitutiveExtraDofDiffusionAccountFieldSource());
    gterm->setConstitutiveExtraDofDiffusionAccountMecaSource(getConstitutiveExtraDofDiffusionAccountMecaSource());
  }
  return term;
}

BilinearTermBase* axisymmetricDG3DDomain::createNeumannMatrixTerm(FunctionSpace<double> *spneu, const elementGroup* g,
                                                                     const simpleFunctionTime<double>* f, const unknownField *uf,
                                                                     const IPField * ip, 
                                                                     const nonLinearBoundaryCondition::location onWhat,
                                                                     const nonLinearNeumannBC::NeumannBCType neumann_type,
                                                                     const int comp) const
{
  BilinearTermBase* btr = NULL;
  if (neumann_type == nonLinearNeumannBC::PRESSURE){
    btr = new axisymmetricG3DFiniteStrainsPressureBilinearTerm(*spneu,uf,f);
  }
  else if (neumann_type == nonLinearNeumannBC::FORCE){
    btr =  new BiNonLinearTermVoid();
  }
  else{
    Msg::Error("axisymmetricDG3DDomain::createNeumannMatrixTerm %s is not defined dG3DDomain::createNeumannTerm",neumann_type);
  }
  
  g3DBilinearTerm<double,double>* gbiterm = dynamic_cast<g3DBilinearTerm<double,double>*>(btr);
  if (gbiterm != NULL){
    gbiterm->setDim(_dim);
    if(getNumNonLocalVariable()>0)
    {
      gbiterm->setNonLocalEqRatio(getNonLocalEqRatio());
      gbiterm->setNumNonLocalVariable(getNumNonLocalVariable());
    }
    if( getNumConstitutiveExtraDofDiffusionVariable() >0)
    {
      gbiterm->setNumConstitutiveExtraDofDiffusionVariable(getNumConstitutiveExtraDofDiffusionVariable());
      gbiterm->setConstitutiveExtraDofDiffusionEqRatio(getConstitutiveExtraDofDiffusionEqRatio());
      gbiterm->setConstitutiveExtraDofDiffusionUseEnergyConjugatedField(getConstitutiveExtraDofDiffusionUseEnergyConjugatedField());
      gbiterm->setConstitutiveExtraDofDiffusionAccountFieldSource(getConstitutiveExtraDofDiffusionAccountFieldSource());
      gbiterm->setConstitutiveExtraDofDiffusionAccountMecaSource(getConstitutiveExtraDofDiffusionAccountMecaSource());
    }    
  }
  return btr;
}

double axisymmetricDG3DDomain::computeVolumeDomain(const IPField* ipf) const{
  double volume = 0;
	if (g->size()>0){
		IntPt* GP;
    SPoint3 p;
		const ipFiniteStrain *vipv[256];
		for (elementGroup::elementContainer::const_iterator it= g->begin(); it!=g->end(); it++){
			MElement* ele= it->second;
			int npts= integBulk->getIntPoints(ele,&GP);
			ipf->getIPv(ele,vipv);
			for (int i=0; i<npts; i++){
				double weight= GP[i].weight;
        ele->pnt(GP[i].pt[0],GP[i].pt[1],GP[i].pt[2],p);
				double& detJ = vipv[i]->getJacobianDeterminant(ele,GP[i]);
				double ratio = detJ*weight*2.*M_PI*p[0];
				volume+=ratio;
			};
		};
	}
  return volume;
};

double axisymmetricDG3DDomain::computeDeformationEnergy(const IPField* ipf) const{
	double energy = 0.;
	if (g->size()>0){
		IntPt* GP;
    SPoint3 p;
		const ipFiniteStrain *vipv[256];
		for (elementGroup::elementContainer::const_iterator it= g->begin(); it!=g->end(); it++){
			MElement* ele= it->second;
			ipf->getIPv(ele,vipv);
			int npts= integBulk->getIntPoints(ele,&GP);
			for (int i=0; i<npts; i++){
        ele->pnt(GP[i].pt[0],GP[i].pt[1],GP[i].pt[2],p);
				double weight= GP[i].weight;
				double &detJ = vipv[i]->getJacobianDeterminant(ele,GP[i]);
				energy += (vipv[i]->defoEnergy()*detJ*weight*2.*M_PI*p[0]);
			};
		};
	}
	return energy;
};
double axisymmetricDG3DDomain::computePlasticEnergy(const IPField* ipf) const{
	double energy = 0.;
	if (g->size()>0){
		IntPt* GP;
    SPoint3 p;
		const ipFiniteStrain *vipv[256];
		for (elementGroup::elementContainer::const_iterator it= g->begin(); it!=g->end(); it++){
			MElement* ele= it->second;
			ipf->getIPv(ele,vipv);
			int npts= integBulk->getIntPoints(ele,&GP);
			for (int i=0; i<npts; i++){
        ele->pnt(GP[i].pt[0],GP[i].pt[1],GP[i].pt[2],p);
				double weight= GP[i].weight;
				double &detJ = vipv[i]->getJacobianDeterminant(ele,GP[i]);
				energy += (vipv[i]->plasticEnergy()*detJ*weight*2.*M_PI*p[0]);
			};
		};
	}
	return energy;
};

double axisymmetricDG3DDomain::computeIrreversibleEnergy(const IPField* ipf) const{
	double energy = 0.;
	if (g->size()>0){
		IntPt* GP;
    SPoint3 p;
		const ipFiniteStrain *vipv[256];
		for (elementGroup::elementContainer::const_iterator it= g->begin(); it!=g->end(); it++){
			MElement* ele= it->second;
			ipf->getIPv(ele,vipv);
			int npts= integBulk->getIntPoints(ele,&GP);
			for (int i=0; i<npts; i++){
        ele->pnt(GP[i].pt[0],GP[i].pt[1],GP[i].pt[2],p);
				double weight= GP[i].weight;
				double &detJ = vipv[i]->getJacobianDeterminant(ele,GP[i]);
				energy += (vipv[i]->irreversibleEnergy()*detJ*weight*2.*M_PI*p[0]);
			};
		};
	}
	return energy;
};

double axisymmetricDG3DDomain::computeMeanValueDomain(const int num, const IPField* ipf, double& solid, const GPFilter* filter) const{
  double meanVal = 0;
  solid = 0;
  if (g->size() > 0){
    IntPt* GP;
    SPoint3 p;
    const ipFiniteStrain *vipv[256];
    const ipFiniteStrain *vipvprev[256];
    for (elementGroup::elementContainer::const_iterator it = this->g->begin(); it != this->g->end(); ++it){
      MElement *ele = it->second;
      ipf->getIPv(ele,vipv,vipvprev);
      int npts_bulk=this->getBulkGaussIntegrationRule()->getIntPoints(ele,&GP);
      for (int i=0; i<npts_bulk; i++){
        ele->pnt(GP[i].pt[0],GP[i].pt[1],GP[i].pt[2],p);
				double weight= GP[i].weight;
				double &detJ = vipv[i]->getJacobianDeterminant(ele,GP[i]);
        double ratio = weight*detJ*2.*M_PI*p[0];
        double fact = filter->getFactor(vipv[i],vipvprev[i]);
        meanVal += fact*vipv[i]->get(num)*ratio;
        solid += fact*ratio;
      };
    }
    meanVal /= solid;
  }
  return meanVal;
};

double axisymmetricDG3DDomain::averagingOnActiveDissipationZone(const int num, const IPField* ipf, double& solid, const IPStateBase::whichState ws) const{
  double meanVal = 0;
  solid = 0.;
  if (g->size() > 0){
    IntPt* GP;
    SPoint3 p;
    const ipFiniteStrain *vipv[256];
    for (elementGroup::elementContainer::const_iterator it = this->g->begin(); it != this->g->end(); ++it){
      MElement *ele = it->second;
      ipf->getIPv(ele,vipv,ws);
      int npts_bulk=this->getBulkGaussIntegrationRule()->getIntPoints(ele,&GP);
      for (int i=0; i<npts_bulk; i++){
        // compute if damage is active and non unloading
        if (vipv[i]->dissipationIsActive()){
          ele->pnt(GP[i].pt[0],GP[i].pt[1],GP[i].pt[2],p);
          double weight= GP[i].weight;
          double &detJ = vipv[i]->getJacobianDeterminant(ele,GP[i]);
          double ratio = weight*detJ*2.*M_PI*p[0];
          double valnum = vipv[i]->get(num);
          if (num == IPField::F_XX or num == IPField::F_YY or num == IPField::F_ZZ){
            valnum -= 1.;
          }
          meanVal += valnum*ratio;
          solid += ratio;
        }
      };
    }
  }
  if (solid > 0.)
    return meanVal/solid;
  else
    return 0.;

};



axisymmetricInterDomainBetween3D::axisymmetricInterDomainBetween3D(const int tag, partDomain *dom1,partDomain *dom2,const int lnum, const int bnum) :
                                  interDomainBase(dom1,dom2,bnum),
                                  axisymmetricDG3DDomain(tag,manageInterface::getKey(dom1->getPhysical(),
                                                                    dom2->getPhysical()),10000,lnum,true,dom1->getDim(),
                                                                    dom1->getNumNonLocalVariable())
{
  if(dom1->getNumNonLocalVariable()<dom2->getNumNonLocalVariable() or dom1->getNumConstitutiveExtraDofDiffusionVariable()<dom2->getNumConstitutiveExtraDofDiffusionVariable())
    Msg::Error("interDomainBetween3D::interDomainBetween3D: domain 2 has less variable than domain 1");
  // create empty elementGroup
  g = new elementGroup();
  // set functionalSpace

  if(_space != NULL) delete _space;
  _space = new dG3DFunctionSpaceBetween2Domains(_domMinus->getFunctionSpace(),_domPlus->getFunctionSpace());

  _nonLocalBeta = (static_cast<axisymmetricDG3DDomain *> (dom1))-> getNonLocalStabilityParameter();
  _nonLocalContinuity = (static_cast<axisymmetricDG3DDomain *> (dom1))->getNonLocalContinuity();
  _nonLocalEqRatio =  (static_cast<axisymmetricDG3DDomain *> (dom1))->getNonLocalEqRatio();

  _constitutiveExtraDofDiffusionBeta = (static_cast<axisymmetricDG3DDomain *> (dom1))->getConstitutiveExtraDofDiffusionStabilityParameter();
  _constitutiveExtraDofDiffusionContinuity = (static_cast<axisymmetricDG3DDomain *> (dom1))->getConstitutiveExtraDofDiffusionContinuity();
  _constitutiveExtraDofDiffusionEqRatio = (static_cast<axisymmetricDG3DDomain *> (dom1))->getConstitutiveExtraDofDiffusionEqRatio();
  _constitutiveExtraDofDiffusionUseEnergyConjugatedField = (static_cast<axisymmetricDG3DDomain *> (dom1))->getConstitutiveExtraDofDiffusionUseEnergyConjugatedField();
  _constitutiveExtraDofDiffusionAccountFieldSource = (static_cast<axisymmetricDG3DDomain *> (dom1))->getConstitutiveExtraDofDiffusionAccountFieldSource();
  _constitutiveExtraDofDiffusionAccountMecaSource = (static_cast<axisymmetricDG3DDomain *> (dom1))->getConstitutiveExtraDofDiffusionAccountMecaSource();

}


axisymmetricInterDomainBetween3D::axisymmetricInterDomainBetween3D(const axisymmetricInterDomainBetween3D &source) : interDomainBase(source), axisymmetricDG3DDomain(source)
{}

void axisymmetricInterDomainBetween3D::createTerms(unknownField *uf,IPField*ip)
{
   // This function is an adapted copy of dG3DDomain::createTerms
  // get space
  FunctionSpace<double>* spMinus = static_cast<FunctionSpace<double>*>(getMinusDomain()->getFunctionSpace());
  FunctionSpace<double>* spPlus = static_cast<FunctionSpace<double>*>(getPlusDomain()->getFunctionSpace());

  // bulk term
  // void term (as they have to exist)
  this->ltermBulk = new nonLinearTermVoid();
  this->btermBulk = new BiNonLinearTermVoid();

  this->btermVirtBound = new BiNonLinearTermVoid();
  this->ltermVirtBound = new nonLinearTermVoid();
  this->massterm = new BiNonLinearTermVoid();

  // DG terms
  ltermBound = new axisymmetricDG3DForceInter(*spMinus,spPlus,_mlawMinus,_mlawPlus,_interQuad,_beta1,_fullDg,ip,_incrementNonlocalBased);
  dG3DLinearTerm<double>* gltermBound = static_cast<dG3DLinearTerm<double>*>(ltermBound);
  gltermBound->setDim(_dim);
  if( getNumNonLocalVariable() >0)
  {
    gltermBound->setNonLocalEqRatio(getNonLocalEqRatio());
    gltermBound->setNumNonLocalVariable(getNumNonLocalVariable());
    gltermBound->setNonLocalStabilityParameter(getNonLocalStabilityParameter());
    gltermBound->setNonLocalContinuity(getNonLocalContinuity());
  }
  if (getNumConstitutiveExtraDofDiffusionVariable() > 0){
    gltermBound->setNumConstitutiveExtraDofDiffusionVariable(getNumConstitutiveExtraDofDiffusionVariable());
    gltermBound->setConstitutiveExtraDofDiffusionEqRatio(getConstitutiveExtraDofDiffusionEqRatio());
    gltermBound->setConstitutiveExtraDofDiffusionUseEnergyConjugatedField(getConstitutiveExtraDofDiffusionUseEnergyConjugatedField());
    gltermBound->setConstitutiveExtraDofDiffusionAccountFieldSource(getConstitutiveExtraDofDiffusionAccountFieldSource());
    gltermBound->setConstitutiveExtraDofDiffusionAccountMecaSource(getConstitutiveExtraDofDiffusionAccountMecaSource());
    gltermBound->setConstitutiveExtraDofDiffusionStabilityParameter(getConstitutiveExtraDofDiffusionStabilityParameter());
    gltermBound->setConstitutiveExtraDofDiffusionContinuity(getConstitutiveExtraDofDiffusionContinuity());
  }

  // Stiffness computation
  if(_interByPert){
    btermBound = new BilinearTermPerturbation<double>(ltermBound,*spMinus,*spPlus,uf,ip,this,_eps);
  }
  else{
    btermBound = new axisymmetricDG3DStiffnessInter(*spMinus,*spPlus,_mlawMinus,_mlawPlus,_interQuad,_beta1,ip,uf,_fullDg,_incrementNonlocalBased);
    dG3DBilinearTerm<double,double>* gbtermBound = static_cast<dG3DBilinearTerm<double,double>*>(btermBound);
    gbtermBound->setDim(_dim);
    if( getNumNonLocalVariable() >0)
    {
      gbtermBound->setNonLocalEqRatio(getNonLocalEqRatio());
      gbtermBound->setNumNonLocalVariable(getNumNonLocalVariable());
      gbtermBound->setNonLocalStabilityParameter(getNonLocalStabilityParameter());
      gbtermBound->setNonLocalContinuity(getNonLocalContinuity());
    }
    if (getNumConstitutiveExtraDofDiffusionVariable() > 0){
      gbtermBound->setNumConstitutiveExtraDofDiffusionVariable(getNumConstitutiveExtraDofDiffusionVariable());
      gbtermBound->setConstitutiveExtraDofDiffusionEqRatio(getConstitutiveExtraDofDiffusionEqRatio());
      gbtermBound->setConstitutiveExtraDofDiffusionUseEnergyConjugatedField(getConstitutiveExtraDofDiffusionUseEnergyConjugatedField());
      gbtermBound->setConstitutiveExtraDofDiffusionAccountFieldSource(getConstitutiveExtraDofDiffusionAccountFieldSource());
      gbtermBound->setConstitutiveExtraDofDiffusionAccountMecaSource(getConstitutiveExtraDofDiffusionAccountMecaSource());
      gbtermBound->setConstitutiveExtraDofDiffusionStabilityParameter(getConstitutiveExtraDofDiffusionStabilityParameter());
      gbtermBound->setConstitutiveExtraDofDiffusionContinuity(getConstitutiveExtraDofDiffusionContinuity());
    }
  }

  // virtual terms
  this->btermVirtBound = new BiNonLinearTermVoid();
  this->ltermVirtBound = new nonLinearTermVoid();


  // path following
  bool withPF = false;
  if (getMacroSolver() != NULL and _accountPathFollowing)
  {
    withPF = getMacroSolver()->withPathFollowing();
  }
  
  if (withPF){
    //Msg::Error("axisymmetricDG3DDomain::createTerms PathFollowing has not been verified in axisymmetricDG3DDomain");
    this->scalarTermPF = new nonLinearScalarTermVoid();
    this->linearTermPF = new nonLinearTermVoid();

    this->scalarTermPFBound = new nonLinearScalarTermVoid();
    this->linearTermPFBound = new nonLinearTermVoid();
  }
  else{
    scalarTermPF = new nonLinearScalarTermVoid();;
    linearTermPF = new nonLinearTermVoid();;
    scalarTermPFBound = new nonLinearScalarTermVoid();;
    linearTermPFBound = new nonLinearTermVoid();
  }

  // tangent term is not implemented
  btermElasticBulk = new BiNonLinearTermVoid();
  btermTangent = new BiNonLinearTermVoid();
  btermElasticTangent = new BiNonLinearTermVoid();
  btermUndamagedElasticTangent = new BiNonLinearTermVoid();

}

void axisymmetricInterDomainBetween3D::setMaterialLaw(const std::map<int,materialLaw*> &maplaw)
{
  // oterwise the law is already set
  if(!setmaterial){
    _domMinus->setMaterialLaw(maplaw);
    _domPlus->setMaterialLaw(maplaw);
    _mlaw = NULL;
    if(_lnum == 0){
      // no fracture law prescribed --> materialLawMinus and Plus are given via domain
      if (_bulkNum != 0){
        for(std::map<int,materialLaw*>::const_iterator it=maplaw.begin(); it!=maplaw.end(); ++it){
          if(it->first == _bulkNum){
            _mlawMinus = it->second;
            _mlawPlus = it->second;
            break;
          }
        }
        if (_mlawMinus == NULL){
          Msg::Error("The material law %d is not found",_bulkNum);
        }
      }
      else{
        if ((_domMinus->getMaterialLaw()->getType() == materialLaw::fracture) and _lawFromBulkPart){
          materialLaw2LawsInitializer *m2law = dynamic_cast<materialLaw2LawsInitializer*>(_domMinus->getMaterialLaw());
          if (m2law){
            int lawMinusNum = m2law->bulkLawNumber();
            for(std::map<int,materialLaw*>::const_iterator it=maplaw.begin(); it!=maplaw.end(); ++it){
              if (it->first == lawMinusNum){
                _mlawMinus = it->second;
                break;
              }
            }
            if (_mlawMinus == NULL){
              Msg::Error("The material law %d is not found",lawMinusNum);
            }
          }
          else{
            Msg::Error("fractureBy2Laws must be used interDomainBetween3D::setMaterialLaw");
          }
        }
        else{
          _mlawMinus = _domMinus->getMaterialLaw();
        }

        if ((_domPlus->getMaterialLaw()->getType() == materialLaw::fracture) and _lawFromBulkPart){
          materialLaw2LawsInitializer *m2law = dynamic_cast<materialLaw2LawsInitializer*>(_domPlus->getMaterialLaw());
          if (m2law){
            int lawPlusNum = m2law->bulkLawNumber();
            for(std::map<int,materialLaw*>::const_iterator it=maplaw.begin(); it!=maplaw.end(); ++it){
              if (it->first == lawPlusNum){
                _mlawPlus = it->second;
                break;
              }
            }
            if (_mlawPlus == NULL){
              Msg::Error("The material law %d is not found",lawPlusNum);
            }

          }
          else{
            Msg::Error("fractureBy2Laws must be used interDomainBetween3D::setMaterialLaw");
          }
        }
        else{
          _mlawPlus = _domPlus->getMaterialLaw();
        }
      }
    }
    else{
      // create law (use lnum with negative number same number for the two but it doesn't matter)
      // these law don't appear in map with all laws
      // check if minus and plus laws are already fracture by 2 laws;
      if (_bulkNum != 0){
        // if _bulkNum is set, this used for bulk law
        materialLaw* mlawInt = NULL;
        for(std::map<int,materialLaw*>::const_iterator it=maplaw.begin(); it!=maplaw.end(); ++it){
          if(it->first == _bulkNum){
            mlawInt = it->second;
            break;
          }
        }
        if (mlawInt == NULL){
          Msg::Error("The material law %d is not found",_bulkNum);
        }

        if (mlawInt->isNumeric()){
          _mlawMinus = new MultiscaleFractureByCohesive3DLaw(-_bulkNum,_bulkNum,_lnum);
        }
        else{
          _mlawMinus = new FractureByCohesive3DLaw(-_bulkNum,_bulkNum,_lnum);
        }

        _mlawPlus = _mlawMinus;
      }
      else{
        // if _bulkNum is not set, the material laws are constructed from positive and negative parts
        int numlawBulkMinus(0), numlawBulkPlus(0);
        if(_domMinus->getMaterialLaw()->getType() != materialLaw::fracture){
          numlawBulkMinus = _domMinus->getLawNum();
        }
        else{
          materialLaw2LawsInitializer *m2law = dynamic_cast<materialLaw2LawsInitializer*>(_domMinus->getMaterialLaw());
          numlawBulkMinus = m2law->bulkLawNumber();
        }
        if(_domPlus->getMaterialLaw()->getType() != materialLaw::fracture){
          numlawBulkPlus = _domPlus->getLawNum();
        }
        else{
          materialLaw2LawsInitializer *m2law = dynamic_cast<materialLaw2LawsInitializer*>(_domPlus->getMaterialLaw());
          numlawBulkPlus = m2law->bulkLawNumber();
        }

        if (_domMinus->getMaterialLaw()->isNumeric()){
          _mlawMinus = new MultiscaleFractureByCohesive3DLaw(-numlawBulkMinus,numlawBulkMinus,_lnum);
        }
        else
          _mlawMinus = new FractureByCohesive3DLaw(-numlawBulkMinus,numlawBulkMinus,_lnum);

        if (_domPlus->getMaterialLaw()->isNumeric()){
          _mlawPlus = new MultiscaleFractureByCohesive3DLaw(-numlawBulkPlus,numlawBulkPlus,_lnum);
        }
        else
          _mlawPlus = new FractureByCohesive3DLaw(-numlawBulkPlus,numlawBulkPlus,_lnum);
      }
    }
    _mlawMinus->initLaws(maplaw);
    _mlawPlus->initLaws(maplaw);
    setmaterial=true;
  }
}
