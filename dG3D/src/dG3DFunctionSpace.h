//
// C++ Interface: Function Space
//
// Description: FunctionSpace used (scalar lagragian function space in 3D)
//
//
// Author:  <L. Noels>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef DG3DFUNCTIONSPACE_H_
#define DG3DFUNCTIONSPACE_H_
#include "GModel.h"
#include "MElement.h"
#include "functionSpace.h"
#include "MInterfaceElement.h"
#include "dG3DDof3IntType.h"
#include "elementGroup.h"
#include "GmshConfig.h"
#include "interFunctionSpace.h"
#include "MInterfaceTriangleN.h"
#include "MInterfaceQuadrangleN.h"
#include "ThreeDLagrangeFunctionSpace.h"
#include "InterfaceKeys.h"

class g3DLagrangeFunctionSpace : public ThreeDLagrangeFunctionSpace{
 public :
  g3DLagrangeFunctionSpace(int id, int ncomp) : ThreeDLagrangeFunctionSpace(id, ncomp,false,false){};
  g3DLagrangeFunctionSpace(int id, int ncomp, int comp1) : ThreeDLagrangeFunctionSpace(id,ncomp,comp1,false,false){}
  g3DLagrangeFunctionSpace(int id, int ncomp, int comp1,int comp2) : ThreeDLagrangeFunctionSpace(id,ncomp, comp1,comp2,false,false){}
  g3DLagrangeFunctionSpace(int id, int ncomp, int comp1,int comp2,int comp3) : ThreeDLagrangeFunctionSpace(id,ncomp,comp1,comp2,comp3,false,false){}
  g3DLagrangeFunctionSpace(int id, int ncomp, int comp1,int comp2,int comp3,int comp4) : ThreeDLagrangeFunctionSpace(id,ncomp,comp1,comp2,comp3,comp4,false,false){}
  g3DLagrangeFunctionSpace(int id, int ncomp, int comp1,int comp2,int comp3,int comp4, int comp5) : 
                                           ThreeDLagrangeFunctionSpace(id,ncomp,comp1,comp2,comp3,comp4,comp5,false,false){}
  g3DLagrangeFunctionSpace(int id, int ncomp, int comp1,int comp2,int comp3,int comp4, int comp5, int comp6) : 
                                           ThreeDLagrangeFunctionSpace(id,ncomp,comp1,comp2,comp3,comp4,comp5,comp6,false,false){}
  virtual ~g3DLagrangeFunctionSpace(){};
  
  virtual std::string getFunctionSpaceName() const 
  {
    return "g3DLagrangeFunctionSpace";
  }

 protected :
  virtual void getKeysOnElement(MElement *ele, std::vector<Dof> &keys) const
  {
    // negative type in mpi if the Dof are not located on this partition
    #if defined(HAVE_MPI) // small duplication to avoid multiple if in a loop
    if( (ele->getPartition() != 0) and (ele->getPartition() != Msg::GetCommRank() +1))
    {
      for (int j=0;j<_ncomp;++j)
      {
        int nk=ele->getNumVertices(); // return the number of vertices
        if (withPrimaryShapeFunction(comp[j]))
        {
          nk = ele->getNumPrimaryVertices();
        }
        for (int i=0;i<nk;++i){
          #if defined(HAVE_CG_MPI_INTERFACE)
          keys.push_back(Dof(ele->getVertex(i)->getNum(),-dG3DDof3IntType::createTypeWithThreeInts(comp[j],ifield[j],ele->getPartition())));
          #else
          keys.push_back(Dof(ele->getVertex(i)->getNum(),dG3DDof3IntType::createTypeWithThreeInts(comp[j],ifield[j])));
          #endif
        }          
      }
    }
    else
    #endif // HAVE_MPI
    {
      for (int j=0;j<_ncomp;++j)
      {
        int nk=ele->getNumVertices(); // return the number of vertices
        if (withPrimaryShapeFunction(comp[j]))
        {
          nk = ele->getNumPrimaryVertices();
        }
        for (int i=0;i<nk;++i){
          #if defined(HAVE_CG_MPI_INTERFACE)
          keys.push_back(Dof(ele->getVertex(i)->getNum(),dG3DDof3IntType::createTypeWithThreeInts(comp[j],ifield[j],ele->getPartition())));
          #else
          keys.push_back(Dof(ele->getVertex(i)->getNum(),dG3DDof3IntType::createTypeWithThreeInts(comp[j],ifield[j])));
          #endif
        }
      }
    }
  }
    
 public:
  // warning MPI used ??
  //virtual void getKeys(MVertex *ver, std::vector<Dof> &keys){
  //  for(int j=0;j<_ncomp;j++)
  //    keys.push_back(Dof(ver->getNum(),dG3DDof3IntType::createTypeWithThreeInts(comp[j],_ifield)));
  //}
	virtual FunctionSpaceBase* clone(const int id) const{
    g3DLagrangeFunctionSpace* sp = NULL;
		if (_ncomp ==1) sp = new g3DLagrangeFunctionSpace(id,_ncomp,comp[0]);
		else if (_ncomp == 2) sp = new g3DLagrangeFunctionSpace(id,_ncomp,comp[0],comp[1]);
		else if (_ncomp == 3) sp = new g3DLagrangeFunctionSpace(id,_ncomp,comp[0],comp[1],comp[2]);
		else if (_ncomp == 4) sp = new g3DLagrangeFunctionSpace(id,_ncomp,comp[0],comp[1],comp[2],comp[3]);
		else if (_ncomp == 5) sp = new g3DLagrangeFunctionSpace(id,_ncomp,comp[0],comp[1],comp[2],comp[3],comp[4]);
		else if (_ncomp == 6) sp = new g3DLagrangeFunctionSpace(id,_ncomp,comp[0],comp[1],comp[2],comp[3],comp[4],comp[5]);
		else sp = new g3DLagrangeFunctionSpace(id,_ncomp);
    return dynamic_cast<FunctionSpaceBase*>(sp);
	}
};

class dG3DLagrangeFunctionSpace : public ThreeDLagrangeFunctionSpace{
 protected:
  virtual void getKeysOnElement(MElement *ele, std::vector<Dof> &keys) const{
    #if defined(HAVE_MPI) // small duplication to avoid multiple if in a loop
    if( (ele->getPartition() != 0)and (ele->getPartition() != Msg::GetCommRank() +1))
    {
      for (int j=0;j<_ncomp;++j)
      {
        int nk=ele->getNumVertices(); // return the number of vertices
        if (withPrimaryShapeFunction(comp[j]))
        {
          nk = ele->getNumPrimaryVertices();
        }
        for (int i=0;i<nk;i++)
          keys.push_back(Dof(ele->getNum(),-dG3DDof3IntType::createTypeWithThreeInts(comp[j],ifield[j],i)));
          
      }
    }
    else
    #endif // HAVE_MPI
    {
      for (int j=0;j<_ncomp;++j)
      {
        int nk=ele->getNumVertices(); // return the number of vertices
        if (withPrimaryShapeFunction(comp[j]))
        {
          nk = ele->getNumPrimaryVertices();
        }
        for (int i=0;i<nk;i++)
          keys.push_back(Dof(ele->getNum(),dG3DDof3IntType::createTypeWithThreeInts(comp[j],ifield[j],i)));
          
      }
    }
  }
 public :
  dG3DLagrangeFunctionSpace(int id, int ncomp) : ThreeDLagrangeFunctionSpace(id,ncomp,false,false){};
  dG3DLagrangeFunctionSpace(int id, int ncomp, int comp1) : ThreeDLagrangeFunctionSpace(id,ncomp,comp1,false,false){}
  dG3DLagrangeFunctionSpace(int id, int ncomp,int comp1,int comp2) : ThreeDLagrangeFunctionSpace(id,ncomp,comp1,comp2,false,false){}
  dG3DLagrangeFunctionSpace(int id, int ncomp,int comp1,int comp2, int comp3) : ThreeDLagrangeFunctionSpace(id,ncomp,comp1,comp2,comp3,false,false){}
  dG3DLagrangeFunctionSpace(int id, int ncomp,int comp1,int comp2, int comp3, int comp4) : ThreeDLagrangeFunctionSpace(id,ncomp,comp1,comp2,comp3,comp4,false,false){}
  dG3DLagrangeFunctionSpace(int id, int ncomp,int comp1,int comp2, int comp3, int comp4, int comp5) : 
                              ThreeDLagrangeFunctionSpace(id,ncomp,comp1,comp2,comp3,comp4,comp5,false,false){}
  dG3DLagrangeFunctionSpace(int id, int ncomp,int comp1,int comp2, int comp3, int comp4, int comp5, int comp6) : 
                              ThreeDLagrangeFunctionSpace(id,ncomp,comp1,comp2,comp3,comp4,comp5,comp6,false,false){}
  virtual ~dG3DLagrangeFunctionSpace(){};
  
  virtual std::string getFunctionSpaceName() const 
  {
    return "dG3DLagrangeFunctionSpace";
  }
	
	virtual FunctionSpaceBase* clone(const int id) const {
		if (_ncomp == 1){
			return new dG3DLagrangeFunctionSpace(id,1,comp[0]);
		}
		else if (_ncomp == 2){
			return new dG3DLagrangeFunctionSpace(id,2,comp[0],comp[1]);
		}
		else if (_ncomp == 3){
			return new dG3DLagrangeFunctionSpace(id,3,comp[0],comp[1],comp[2]);
		}
		else if (_ncomp == 4){
			return new dG3DLagrangeFunctionSpace(id,4,comp[0],comp[1],comp[2],comp[3]);
		}
		else if (_ncomp == 5){
			return new dG3DLagrangeFunctionSpace(id,4,comp[0],comp[1],comp[2],comp[3],comp[4]);
		}
		else if (_ncomp == 6){
			return new dG3DLagrangeFunctionSpace(id,4,comp[0],comp[1],comp[2],comp[3],comp[4],comp[5]);
		}
		else {
			return new dG3DLagrangeFunctionSpace(id,_ncomp);
		}
	}
};

// for high order

class g3DhoDGFunctionSpace : public ThreeDLagrangeFunctionSpace{
/*
  protected:
    virtual void getKeysOnElement(MElement *ele, std::vector<Dof> &keys){
      int nver = ele->getNumVertices();
      for (int i=0; i<_ncomp; i++){
        for (int j=0; j<nver; j++){
          keys.push_back(Dof(ele->getVertex(j)->getNum(),Dof::createTypeWithTwoInts(this->comp[i],ifield[j])));
        };
      };
    };
*/
 public:
  g3DhoDGFunctionSpace(int id, int ncomp) : ThreeDLagrangeFunctionSpace(id,ncomp,true,true){};
  g3DhoDGFunctionSpace(int id, int ncomp, int comp1) : ThreeDLagrangeFunctionSpace(id,ncomp,comp1,true,true){};
  g3DhoDGFunctionSpace(int id, int ncomp, int comp1, int comp2) : ThreeDLagrangeFunctionSpace(id,ncomp,comp1,comp2,true,true){};
  g3DhoDGFunctionSpace(int id, int ncomp, int comp1, int comp2, int comp3) : ThreeDLagrangeFunctionSpace(id,ncomp,comp1,comp2,comp3,true,true){};
  g3DhoDGFunctionSpace(int id, int ncomp, int comp1, int comp2, int comp3, int comp4) : 
                     ThreeDLagrangeFunctionSpace(id,ncomp,comp1,comp2,comp3,comp4,true,true){};
  g3DhoDGFunctionSpace(int id, int ncomp, int comp1, int comp2, int comp3, int comp4, int comp5) : 
                     ThreeDLagrangeFunctionSpace(id,ncomp,comp1,comp2,comp3,comp4,comp5,true,true){};
  virtual ~g3DhoDGFunctionSpace(){};
  virtual std::string getFunctionSpaceName() const 
  {
    return "g3DhoDGFunctionSpace";
  }
 protected :
  virtual void getKeysOnElement(MElement *ele, std::vector<Dof> &keys) const
  {
    // negative type in mpi if the Dof are not located on this partition
    #if defined(HAVE_MPI) // small duplication to avoid multiple if in a loop
    if( (ele->getPartition() != 0) and (ele->getPartition() != Msg::GetCommRank() +1))
    {
      for (int j=0;j<_ncomp;++j)
      {
        int nk=ele->getNumVertices(); // return the number of vertices
        if (withPrimaryShapeFunction(comp[j]))
        {
          nk = ele->getNumPrimaryVertices();
        }
        for (int i=0;i<nk;++i)
          #if defined(HAVE_CG_MPI_INTERFACE)
          keys.push_back(Dof(ele->getVertex(i)->getNum(),-dG3DDof3IntType::createTypeWithThreeInts(comp[j],ifield[j],ele->getPartition())));
          #else
          keys.push_back(Dof(ele->getVertex(i)->getNum(),dG3DDof3IntType::createTypeWithThreeInts(comp[j],ifield[j])));
          #endif
      } 
    }
    else
    #endif // HAVE_MPI
    {
      for (int j=0;j<comp.size();++j)
      {
        int nk=ele->getNumVertices(); // return the number of vertices
        if (withPrimaryShapeFunction(comp[j]))
        {
          nk = ele->getNumPrimaryVertices();
        }
        for (int i=0;i<nk;++i)
          #if defined(HAVE_CG_MPI_INTERFACE)
          keys.push_back(Dof(ele->getVertex(i)->getNum(),dG3DDof3IntType::createTypeWithThreeInts(comp[j],ifield[j],ele->getPartition())));
          #else
          keys.push_back(Dof(ele->getVertex(i)->getNum(),dG3DDof3IntType::createTypeWithThreeInts(comp[j],ifield[j])));
          #endif
      }
    }
  }
 public:
  // warning MPI used ?? Seems unused
  //virtual void getKeys(MVertex *ver, std::vector<Dof> &keys){
  //  for(int j=0;j<_ncomp;j++)
  //    keys.push_back(Dof(ver->getNum(),dG3DDof3IntType::createTypeWithThreeInts(comp[j],ifield[j],ele->getPartition())));
  //}
	virtual FunctionSpaceBase* clone(const int id) const{
		if (_ncomp ==1) return new g3DhoDGFunctionSpace(id,_ncomp,comp[0]);
		else if (_ncomp == 2) return new g3DhoDGFunctionSpace(id,_ncomp,comp[0],comp[1]);
		else if (_ncomp == 3) return new g3DhoDGFunctionSpace(id,_ncomp,comp[0],comp[1],comp[2]);
		else if (_ncomp == 4) return new g3DhoDGFunctionSpace(id,_ncomp,comp[0],comp[1],comp[2],comp[3]);
		else if (_ncomp == 5) return new g3DhoDGFunctionSpace(id,_ncomp,comp[0],comp[1],comp[2],comp[3],comp[4]);
		else return new g3DhoDGFunctionSpace(id,_ncomp);
	}
};

class dG3DhoDGFunctionSpace : public ThreeDLagrangeFunctionSpace{
 public:
  dG3DhoDGFunctionSpace(int id, int ncomp) : ThreeDLagrangeFunctionSpace(id,ncomp,true,true){};
  dG3DhoDGFunctionSpace(int id, int ncomp, int comp1) : ThreeDLagrangeFunctionSpace(id,ncomp,comp1,true,true){};
  dG3DhoDGFunctionSpace(int id, int ncomp, int comp1, int comp2) : ThreeDLagrangeFunctionSpace(id,ncomp,comp1,comp2,true,true){};
  dG3DhoDGFunctionSpace(int id, int ncomp, int comp1, int comp2, int comp3) : ThreeDLagrangeFunctionSpace(id,ncomp,comp1,comp2,comp3,true,true){};
  dG3DhoDGFunctionSpace(int id, int ncomp, int comp1, int comp2, int comp3, int comp4) : 
                                            ThreeDLagrangeFunctionSpace(id,ncomp,comp1,comp2,comp3,comp4,true,true){};
  dG3DhoDGFunctionSpace(int id, int ncomp, int comp1, int comp2, int comp3, int comp4, int comp5) : 
                                            ThreeDLagrangeFunctionSpace(id,ncomp,comp1,comp2,comp3,comp4,comp5,true,true){};
  virtual ~dG3DhoDGFunctionSpace(){};
  virtual std::string getFunctionSpaceName() const 
  {
    return "dG3DhoDGFunctionSpace";
  }

 protected: // avoid duplication of the following functions with dG3DLagrangeFunctionSpace HOW ??
  virtual void getKeysOnElement(MElement *ele, std::vector<Dof> &keys) const{
    #if defined(HAVE_MPI) // small duplication to avoid multiple if in a loop
    if( (ele->getPartition() != 0)and (ele->getPartition() != Msg::GetCommRank() +1))
    {
      for (int j=0;j<_ncomp;++j)
      {
        int nk=ele->getNumVertices(); // return the number of vertices
        if (withPrimaryShapeFunction(comp[j]))
        {
          nk = ele->getNumPrimaryVertices();
        }
        for (int i=0;i<nk;i++)
          keys.push_back(Dof(ele->getNum(),-dG3DDof3IntType::createTypeWithThreeInts(comp[j],ifield[j],i)));
      }
    }
    else
    #endif // HAVE_MPI
    {
      for (int j=0;j<_ncomp;++j)
      {
        int nk=ele->getNumVertices(); // return the number of vertices
        if (withPrimaryShapeFunction(comp[j]))
        {
          nk = ele->getNumPrimaryVertices();
        }
        for (int i=0;i<nk;i++)
          keys.push_back(Dof(ele->getNum(),dG3DDof3IntType::createTypeWithThreeInts(comp[j],ifield[j],i)));
      }
    }
  }
	
	virtual FunctionSpaceBase* clone(const int id) const {
		if (_ncomp == 1){
			return new dG3DhoDGFunctionSpace(id,1,comp[0]);
		}
		else if (_ncomp == 2){
			return new dG3DhoDGFunctionSpace(id,2,comp[0],comp[1]);
		}
		else if (_ncomp == 3){
			return new dG3DhoDGFunctionSpace(id,3,comp[0],comp[1],comp[2]);
		}
		else if (_ncomp == 4){
			return new dG3DhoDGFunctionSpace(id,3,comp[0],comp[1],comp[2],comp[3]);
		}
		else if (_ncomp == 5){
			return new dG3DhoDGFunctionSpace(id,3,comp[0],comp[1],comp[2],comp[3],comp[4]);
		}
		else{
			return new dG3DhoDGFunctionSpace(id,_ncomp);
		}
	};
};


class g3DDirichletBoundaryConditionLagrangeFunctionSpace : public g3DLagrangeFunctionSpace{
 public:
  g3DDirichletBoundaryConditionLagrangeFunctionSpace(int id, int ncomp) : g3DLagrangeFunctionSpace(id, ncomp){}
  g3DDirichletBoundaryConditionLagrangeFunctionSpace(int id, int ncomp, int comp1) : g3DLagrangeFunctionSpace(id,ncomp,comp1){}
  g3DDirichletBoundaryConditionLagrangeFunctionSpace(int id, int ncomp, int comp1, int comp2) : g3DLagrangeFunctionSpace(id, ncomp,comp1,comp2){}
  g3DDirichletBoundaryConditionLagrangeFunctionSpace(int id, int ncomp, int comp1, int comp2, int comp3) : g3DLagrangeFunctionSpace(id, ncomp, comp1,comp2, comp3){}
  g3DDirichletBoundaryConditionLagrangeFunctionSpace(int id, int ncomp, int comp1, int comp2, int comp3, int comp4) : 
               g3DLagrangeFunctionSpace(id, ncomp, comp1, comp2, comp3, comp4){}
  g3DDirichletBoundaryConditionLagrangeFunctionSpace(int id, int ncomp, int comp1, int comp2, int comp3, int comp4, int comp5) : 
               g3DLagrangeFunctionSpace(id, ncomp, comp1, comp2, comp3, comp4, comp5){}
  g3DDirichletBoundaryConditionLagrangeFunctionSpace(int id, int ncomp, int comp1, int comp2, int comp3, int comp4, int comp5, int comp6) : 
               g3DLagrangeFunctionSpace(id, ncomp, comp1, comp2, comp3, comp4, comp5, comp6){}
  virtual ~g3DDirichletBoundaryConditionLagrangeFunctionSpace(){};
  
  virtual std::string getFunctionSpaceName() const 
  {
    return "g3DDirichletBoundaryConditionLagrangeFunctionSpace";
  }
	
	virtual FunctionSpaceBase* clone(const int id) const{
		if (_ncomp == 1) return new g3DDirichletBoundaryConditionLagrangeFunctionSpace(id,1,comp[0]);
		else if (_ncomp == 2) return new g3DDirichletBoundaryConditionLagrangeFunctionSpace(id,2,comp[0],comp[1]);
		else if (_ncomp == 3) return new g3DDirichletBoundaryConditionLagrangeFunctionSpace(id,3,comp[0],comp[1],comp[2]);
		else if (_ncomp == 4) return new g3DDirichletBoundaryConditionLagrangeFunctionSpace(id,4,comp[0],comp[1],comp[2],comp[3]);
		else if (_ncomp == 5) return new g3DDirichletBoundaryConditionLagrangeFunctionSpace(id,4,comp[0],comp[1],comp[2],comp[3],comp[4]);
		else if (_ncomp == 6) return new g3DDirichletBoundaryConditionLagrangeFunctionSpace(id,4,comp[0],comp[1],comp[2],comp[3],comp[4],comp[5]);
		else return new g3DDirichletBoundaryConditionLagrangeFunctionSpace(id,_ncomp);
 	}

  virtual void getKeysOnElement(MElement *ele, std::vector<Dof> &keys) const
  {
    // negative type in mpi if the Dof are not located on this partition
    // For BC on edge in the case of CG in // ele->getPartition() is equal to 0.
    // Therefore generate more keys to be sure to fix the dofs (There will be extra fixed dof but normally it doesn't matter)
    #if defined(HAVE_MPI) // small duplication to avoid multiple if in a loop
    if( (ele->getPartition() != 0) and (ele->getPartition() != Msg::GetCommRank() +1))
    {
      if(ele->getPartition()!=0){ // Sure than it will match
        for (int j=0;j<_ncomp;++j)
        {
          int nk=ele->getNumVertices(); // return the number of vertices
          if (withPrimaryShapeFunction(comp[j]))
          {
            nk = ele->getNumPrimaryVertices();
          }
          for (int i=0;i<nk;++i)
            #if defined(HAVE_CG_MPI_INTERFACE)
            keys.push_back(Dof(ele->getVertex(i)->getNum(),-dG3DDof3IntType::createTypeWithThreeInts(comp[j],ifield[j],ele->getPartition())));
            #else
            keys.push_back(Dof(ele->getVertex(i)->getNum(),dG3DDof3IntType::createTypeWithThreeInts(comp[j],ifield[j])));
            #endif
        }
      }
      else{ // match is not ensured --> generate keys for each partitions
        #if defined(HAVE_CG_MPI_INTERFACE)
        for(int p=0;p<=Msg::GetCommSize();p++){
          for (int j=0;j<_ncomp;++j)
          {
            int nk=ele->getNumVertices(); // return the number of vertices
            if (withPrimaryShapeFunction(comp[j]))
            {
              nk = ele->getNumPrimaryVertices();
            }
            for (int i=0;i<nk;++i)
              keys.push_back(Dof(ele->getVertex(i)->getNum(),-dG3DDof3IntType::createTypeWithThreeInts(comp[j],ifield[j],p)));
          }
        }
        #else
        for (int j=0;j<_ncomp;++j)
        {
          int nk=ele->getNumVertices(); // return the number of vertices
          if (withPrimaryShapeFunction(comp[j]))
          {
            nk = ele->getNumPrimaryVertices();
          }
          for (int i=0;i<nk;++i)
            keys.push_back(Dof(ele->getVertex(i)->getNum(),dG3DDof3IntType::createTypeWithThreeInts(comp[j],ifield[j])));
        }
        #endif
      }
    }
    else
    #endif // HAVE_MPI
    {

      for (int j=0;j<_ncomp;++j)
      {
        int nk=ele->getNumVertices(); // return the number of vertices
        if (withPrimaryShapeFunction(comp[j]))
        {
          nk = ele->getNumPrimaryVertices();
        }
        
        for (int i=0;i<nk;++i)
          #if defined(HAVE_CG_MPI_INTERFACE)
          keys.push_back(Dof(ele->getVertex(i)->getNum(),dG3DDof3IntType::createTypeWithThreeInts(comp[j],ifield[j],ele->getPartition())));
          #else
          keys.push_back(Dof(ele->getVertex(i)->getNum(),dG3DDof3IntType::createTypeWithThreeInts(comp[j],ifield[j])));
          #endif
      }
    }
  }
};

// It is tricky to account for CG parallel case especially for the msch project
// in which non partiotioned mesh are considered with Msg::GetCommSize()>1
// This works as long as only 1 GModel exists (Normally OK). Otherwise the Good Gmodel as
// to be selected before prescribing the BCs
class g3DNeumannBoundaryConditionLagrangeFunctionSpace : public g3DLagrangeFunctionSpace{
 protected:
   bool _parallelMesh;
   void setParallelMesh()
   {
    #if defined(HAVE_MPI)
     if(GModel::current()->getNumPartitions()>0)
     {
       _parallelMesh = true;
     }
     else
    #endif // HAVE_MPI
     {
       _parallelMesh = false;
     }
   }
 public:
  g3DNeumannBoundaryConditionLagrangeFunctionSpace(int id, int ncomp) : g3DLagrangeFunctionSpace(id, ncomp){this->setParallelMesh();}
  g3DNeumannBoundaryConditionLagrangeFunctionSpace(int id, int ncomp, int comp1) : g3DLagrangeFunctionSpace(id,ncomp,comp1){this->setParallelMesh();}
  g3DNeumannBoundaryConditionLagrangeFunctionSpace(int id, int ncomp, int comp1, int comp2) : g3DLagrangeFunctionSpace(id, ncomp,comp1,comp2){this->setParallelMesh();}
  g3DNeumannBoundaryConditionLagrangeFunctionSpace(int id, int ncomp, int comp1, int comp2, int comp3) : g3DLagrangeFunctionSpace(id, ncomp, comp1,comp2, comp3){this->setParallelMesh();}
  g3DNeumannBoundaryConditionLagrangeFunctionSpace(int id, int ncomp, int comp1, int comp2, int comp3, int comp4) : 
          g3DLagrangeFunctionSpace(id, ncomp, comp1,comp2, comp3, comp4){this->setParallelMesh();}
  g3DNeumannBoundaryConditionLagrangeFunctionSpace(int id, int ncomp, int comp1, int comp2, int comp3, int comp4, int comp5) : 
          g3DLagrangeFunctionSpace(id, ncomp, comp1,comp2, comp3, comp4, comp5){this->setParallelMesh();}
  g3DNeumannBoundaryConditionLagrangeFunctionSpace(int id, int ncomp, int comp1, int comp2, int comp3, int comp4, int comp5, int comp6) : 
          g3DLagrangeFunctionSpace(id, ncomp, comp1,comp2, comp3, comp4, comp5, comp6){this->setParallelMesh();}
  virtual ~g3DNeumannBoundaryConditionLagrangeFunctionSpace(){};
	virtual std::string getFunctionSpaceName() const 
  {
    return "g3DNeumannBoundaryConditionLagrangeFunctionSpace";
  }
	virtual FunctionSpaceBase* clone(const int id) const{
		if (_ncomp == 1) return new g3DNeumannBoundaryConditionLagrangeFunctionSpace(id,1,comp[0]);
		else if (_ncomp == 2) return new g3DNeumannBoundaryConditionLagrangeFunctionSpace(id,2,comp[0],comp[1]);
		else if (_ncomp == 3) return new g3DNeumannBoundaryConditionLagrangeFunctionSpace(id,3,comp[0],comp[1],comp[2]);
		else if (_ncomp == 4) return new g3DNeumannBoundaryConditionLagrangeFunctionSpace(id,4,comp[0],comp[1],comp[2],comp[3]);
		else if (_ncomp == 5) return new g3DNeumannBoundaryConditionLagrangeFunctionSpace(id,4,comp[0],comp[1],comp[2],comp[3],comp[4]);
		else if (_ncomp == 6) return new g3DNeumannBoundaryConditionLagrangeFunctionSpace(id,4,comp[0],comp[1],comp[2],comp[3],comp[4],comp[5]);
		else return new g3DNeumannBoundaryConditionLagrangeFunctionSpace(id,_ncomp);
	}

  virtual void getKeysOnElement(MElement *ele, std::vector<Dof> &keys) const
  {
    // For Neumann BC we sure that the BC is applied on element that are on this partition.
    // Indeed Neumann BC are never applied on ghost element
    int p = (_parallelMesh) ? Msg::GetCommRank()+1 : 0;
    for (int j=0;j<_ncomp;++j)
    {
      int nk=ele->getNumVertices(); // return the number of vertices
      if (withPrimaryShapeFunction(comp[j]))
      {
        nk = ele->getNumPrimaryVertices();
      }
      for (int i=0;i<nk;++i)
        #if defined(HAVE_CG_MPI_INTERFACE)
        keys.push_back(Dof(ele->getVertex(i)->getNum(),dG3DDof3IntType::createTypeWithThreeInts(comp[j],ifield[j],p)));
        #else
        keys.push_back(Dof(ele->getVertex(i)->getNum(),dG3DDof3IntType::createTypeWithThreeInts(comp[j],ifield[j])));
        #endif
        
    }
  }
};


class g3DDirichletBoundaryConditionLagrangeFunctionSpaceGhost : public g3DDirichletBoundaryConditionLagrangeFunctionSpace
{
 public:
  g3DDirichletBoundaryConditionLagrangeFunctionSpaceGhost(int id, int ncomp) : g3DDirichletBoundaryConditionLagrangeFunctionSpace(id, ncomp){}
  g3DDirichletBoundaryConditionLagrangeFunctionSpaceGhost(int id, int ncomp, int comp1) : g3DDirichletBoundaryConditionLagrangeFunctionSpace(id,ncomp,comp1){}
  g3DDirichletBoundaryConditionLagrangeFunctionSpaceGhost(int id, int ncomp, int comp1, int comp2) : g3DDirichletBoundaryConditionLagrangeFunctionSpace(id, ncomp,comp1,comp2){}
  g3DDirichletBoundaryConditionLagrangeFunctionSpaceGhost(int id, int ncomp, int comp1, int comp2, int comp3) : g3DDirichletBoundaryConditionLagrangeFunctionSpace(id, ncomp, comp1,comp2, comp3){}
  g3DDirichletBoundaryConditionLagrangeFunctionSpaceGhost(int id, int ncomp, int comp1, int comp2, int comp3, int comp4) : 
                               g3DDirichletBoundaryConditionLagrangeFunctionSpace(id, ncomp, comp1,comp2, comp3, comp4){}
  g3DDirichletBoundaryConditionLagrangeFunctionSpaceGhost(int id, int ncomp, int comp1, int comp2, int comp3, int comp4, int comp5) : 
                               g3DDirichletBoundaryConditionLagrangeFunctionSpace(id, ncomp, comp1,comp2, comp3, comp4, comp5){}
  g3DDirichletBoundaryConditionLagrangeFunctionSpaceGhost(int id, int ncomp, int comp1, int comp2, int comp3, int comp4, int comp5, int comp6) : 
                               g3DDirichletBoundaryConditionLagrangeFunctionSpace(id, ncomp, comp1,comp2, comp3, comp4, comp5, comp6){}
  virtual ~g3DDirichletBoundaryConditionLagrangeFunctionSpaceGhost(){};
	
	virtual FunctionSpaceBase* clone(const int id) const{
		if (_ncomp == 1) return new g3DDirichletBoundaryConditionLagrangeFunctionSpaceGhost(id,1,comp[0]);
		else if (_ncomp == 2) return new g3DDirichletBoundaryConditionLagrangeFunctionSpaceGhost(id,2,comp[0],comp[1]);
		else if (_ncomp == 3) return new g3DDirichletBoundaryConditionLagrangeFunctionSpaceGhost(id,3,comp[0],comp[1],comp[2]);
		else if (_ncomp == 4) return new g3DDirichletBoundaryConditionLagrangeFunctionSpaceGhost(id,4,comp[0],comp[1],comp[2],comp[3]);
		else if (_ncomp == 5) return new g3DDirichletBoundaryConditionLagrangeFunctionSpaceGhost(id,4,comp[0],comp[1],comp[2],comp[3],comp[4]);
		else if (_ncomp == 6) return new g3DDirichletBoundaryConditionLagrangeFunctionSpaceGhost(id,4,comp[0],comp[1],comp[2],comp[3],comp[4],comp[5]);
		else return new g3DDirichletBoundaryConditionLagrangeFunctionSpaceGhost(id,_ncomp);
	} 
  
  virtual std::string getFunctionSpaceName() const 
  {
    return "g3DDirichletBoundaryConditionLagrangeFunctionSpaceGhost";
  }

  // be sure that the type is negative !!
  virtual void getKeys(MElement *ele, std::vector<Dof> &keys) const{
    if(ele->getPartition()!=0){ // Sure than it will match
      for (int j=0;j<_ncomp;++j)
      {
        int nk=ele->getNumVertices(); // return the number of vertices
        if (withPrimaryShapeFunction(comp[j]))
        {
          nk = ele->getNumPrimaryVertices();
        }
        for (int i=0;i<nk;++i)
          #if defined(HAVE_CG_MPI_INTERFACE)
          keys.push_back(Dof(ele->getVertex(i)->getNum(),-dG3DDof3IntType::createTypeWithThreeInts(comp[j],ifield[j],ele->getPartition())));
          #else
          keys.push_back(Dof(ele->getVertex(i)->getNum(),dG3DDof3IntType::createTypeWithThreeInts(comp[j],ifield[j])));
          #endif
      }
    }
    else{ // match is not ensured --> generate keys for each partitions
      #if defined(HAVE_CG_MPI_INTERFACE)
      for(int p=0;p<=Msg::GetCommSize();p++)
      {
        for (int j=0;j<_ncomp;++j)
        {
          int nk=ele->getNumVertices(); // return the number of vertices
          if (withPrimaryShapeFunction(this->comp[j]))
          {
            nk = ele->getNumPrimaryVertices();
          }
          for (int i=0;i<nk;++i)
            keys.push_back(Dof(ele->getVertex(i)->getNum(),-dG3DDof3IntType::createTypeWithThreeInts(comp[j],ifield[j],p)));
        }
      }
      #else
      for (int j=0;j<_ncomp;++j)
      {
        int nk=ele->getNumVertices(); // return the number of vertices
        if (withPrimaryShapeFunction(this->comp[j]))
        {
          nk = ele->getNumPrimaryVertices();
        }
        for (int i=0;i<nk;++i)
          keys.push_back(Dof(ele->getVertex(i)->getNum(),dG3DDof3IntType::createTypeWithThreeInts(comp[j],ifield[j])));
      }
      
      #endif
    }
  }
};

class dG3DBoundaryConditionLagrangeFunctionSpace : public dG3DLagrangeFunctionSpace{
 protected:  
  std::map<MElement*,std::map<MElement*,std::vector<int> > > mapBoundaryInterfaces;
  
private:
  bool checkElement(MElement* e, MElement* eleBulk) const{
    int nv=e->getNumVertices();
    std::vector<MVertex*> tabV;
    e->getVertices(tabV);
    int nn = eleBulk->getNumVertices();
    
    if (e->getNum() == eleBulk->getNum()){
      return true;
    }
    else if (e->getDim() == 0){
      for (int i=0; i< nn; i++){
        if (tabV[0]->getNum() == eleBulk->getVertex(i)->getNum()){
          return true;
        }
      }
    }
    else if (e->getDim() == 1){
      int nedge = eleBulk->getNumEdges();
      for(int kk=0;kk<nedge;kk++){
        std::vector<MVertex*> verEdge;
        eleBulk->getEdgeVertices(kk,verEdge);
        if((tabV[0]->getNum() == verEdge[0]->getNum() and tabV[1]->getNum() == verEdge[1]->getNum()) or 
           (tabV[0]->getNum() == verEdge[1]->getNum() and tabV[1]->getNum() == verEdge[0]->getNum())){
          return true;
        }
      }
    }
    else if (e->getDim() == 2){
      int nface = eleBulk->getNumFaces();
      for(int kk=0;kk<nface;kk++){
        std::vector<MVertex*> verFace;
        eleBulk->getFaceVertices(kk,verFace);
        int nFaceVer = verFace.size();
        if (nv == nFaceVer){
          interfaceKeys keyBound, keyVolume;
          if (e->getType() == TYPE_TRI){
            keyBound.setValues(e->getVertex(0)->getNum(),e->getVertex(1)->getNum(),e->getVertex(2)->getNum());
            keyVolume.setValues(verFace[0]->getNum(),verFace[1]->getNum(),verFace[2]->getNum());
          }
          else if (e->getType() == TYPE_QUA){
            keyBound.setValues(e->getVertex(0)->getNum(),e->getVertex(1)->getNum(),e->getVertex(2)->getNum(),e->getVertex(3)->getNum());
            keyVolume.setValues(verFace[0]->getNum(),verFace[1]->getNum(),verFace[2]->getNum(),verFace[3]->getNum());
          }
          else{
            Msg::Error("dG3DBoundaryConditionLagrangeFunctionSpace has been defined for tri and quad elements only, complete list for other element");
          }
          
          if (keyBound == keyVolume){
            return true;
          }
        }
      }
    }
    
    return false;
  }
  
  void getLocalVertexNumber(MElement* e, MElement* eleBulk, std::vector<int>& posfind) const{
    int nv=e->getNumVertices();
    int nn = eleBulk->getNumVertices();
    posfind.resize(nv);
    for(int j=0;j<nv;j++){
      for (int i=0;i<nn;++i){
        if(e->getVertex(j)->getNum() == eleBulk->getVertex(i)->getNum()){
          posfind[j] = i;
          break;
        }
      }
    }
  };
  
  void __init__(const elementGroup *g, elementGroup *gBulk, elementGroup *ghostMPI){
    for(elementGroup::elementContainer::const_iterator it=g->begin(); it!=g->end(); ++it)
    {
      MElement *e = it->second;
      std::map<MElement*,std::vector<int> > supportEle;
      std::vector<int> posfind;
      for (elementGroup::elementContainer::const_iterator itBulk = gBulk->begin(); itBulk != gBulk->end(); itBulk++){
        MElement* eleBulk = itBulk->second;
        bool found = checkElement(e,eleBulk);
        if (found){
          getLocalVertexNumber(e,eleBulk,posfind);
          supportEle[eleBulk] = posfind;
        }
      }
      
      for (elementGroup::elementContainer::const_iterator itBulk = ghostMPI->begin(); itBulk != ghostMPI->end(); itBulk++){
        MElement* eleGhostMPI = itBulk->second;
        bool found = checkElement(e,eleGhostMPI);
        if (found){
          getLocalVertexNumber(e,eleGhostMPI,posfind);
          supportEle[eleGhostMPI] = posfind;
        }
      }
      
      if (supportEle.size() == 0){
        Msg::Error("volumic element is not found for boundary element %d",e->getNum());
      }
      else{
        if (mapBoundaryInterfaces.find(e) == mapBoundaryInterfaces.end()){
          mapBoundaryInterfaces[e] = supportEle;
        }
        else{
          std::map<MElement*,std::vector<int> >& allocatedPart = mapBoundaryInterfaces[e];
          allocatedPart.insert(supportEle.begin(),supportEle.end());
        }
      }
    }  
  };
  
  
  // constructor function
  void __init__(const elementGroup *g, elementGroup *gi, elementGroup *vinter,elementGroup *ghostMPI)
  {
    for(elementGroup::elementContainer::const_iterator it=g->begin(); it!=g->end(); ++it)
    {
      MElement *e = it->second;
      bool found=false;
      
      // Find the interface element linked with e
      std::map<MElement*,std::vector<int> > supportEle;
      std::vector<int> posfind; // position of vertices of e in interface element
      for(elementGroup::elementContainer::const_iterator it2=vinter->begin();it2!=vinter->end();++it2)
      {
        MElement* vie = (it2->second);
        found = checkElement(e,vie);
        if (found){
          MInterfaceElement* ielem=dynamic_cast<MInterfaceElement*>(vie);
          // find bulk local vertex numbers
          MElement* eleBulk = ielem->getElem(0);
          getLocalVertexNumber(e,eleBulk,posfind);
          supportEle[eleBulk] = posfind;
          break;
        }
      }
      
      // If not find on external edge. Look in internal edge
      if(!found)
      {
        for(elementGroup::elementContainer::const_iterator it2=gi->begin();it2!=gi->end();++it2)
        {
          MElement* interE = it2->second;
          found = checkElement(e,interE);
          if (found){
            MInterfaceElement* ielem = dynamic_cast<MInterfaceElement*>(interE);
            MElement* eleBulkMinus = ielem->getElem(0);
            getLocalVertexNumber(e,eleBulkMinus,posfind);
            supportEle[eleBulkMinus] = posfind;
            
            MElement* eleBulkPlus = ielem->getElem(1);
            getLocalVertexNumber(e,eleBulkPlus,posfind);
            supportEle[eleBulkPlus] = posfind;
            
            break;
          }
        }
      }
        
      // not found yet look on ghost element
      if(!found)
      {
        if(ghostMPI != NULL) // NULL for Neumman BC
        {
          for(elementGroup::elementContainer::const_iterator itE=ghostMPI->begin(); itE!=ghostMPI->end();++itE)
          {
            MElement *gele = itE->second;
            found = checkElement(e,gele);
            if (found) {
              getLocalVertexNumber(e,gele,posfind);
              supportEle[gele] = posfind;
              break;
            }
          }
        
          if(!found)
          {
            Msg::Error("Impossible to fix dof for element %d on rank %d",e->getNum(),Msg::GetCommRank());
            Msg::Error("I am on Proc  %d", Msg::GetCommRank());
            Msg::Error("Vertex %d", e->getVertex(0)->getNum());
            Msg::Error("Vertex %d", e->getVertex(1)->getNum());
            Msg::Error("Vertex %d", e->getVertex(2)->getNum());
          }
        }
      }
      
      if (supportEle.size() == 0){
        Msg::Error("volumic element is not found for boundary element %d",e->getNum());
      }
      else{
        if (mapBoundaryInterfaces.find(e) == mapBoundaryInterfaces.end()){
          mapBoundaryInterfaces[e] = supportEle;
        }
        else{
          std::map<MElement*,std::vector<int> >& allocatedPart = mapBoundaryInterfaces[e];
          allocatedPart.insert(supportEle.begin(),supportEle.end());
        }
      }
    }
  }
 public:

  virtual ~dG3DBoundaryConditionLagrangeFunctionSpace(){}
  // Copy the constructor function
  dG3DBoundaryConditionLagrangeFunctionSpace(int id, int ncomp, const elementGroup *g, elementGroup *gi,
                                          elementGroup *vinter, elementGroup *ghostMPI) : dG3DLagrangeFunctionSpace(id, ncomp)
  {
    this->__init__(g,gi,vinter,ghostMPI);
  }
  dG3DBoundaryConditionLagrangeFunctionSpace(int id, int ncomp, int comp1, const elementGroup *g, elementGroup *gi,
                                          elementGroup *vinter, elementGroup *ghostMPI) : dG3DLagrangeFunctionSpace(id, ncomp,comp1)
  {
    this->__init__(g,gi,vinter,ghostMPI);
  }
  dG3DBoundaryConditionLagrangeFunctionSpace(int id, int ncomp, int comp1, int comp2, const elementGroup *g, elementGroup *gi,
                                          elementGroup *vinter, elementGroup *ghostMPI) : dG3DLagrangeFunctionSpace(id, ncomp,comp1,comp2)
  {
    this->__init__(g,gi,vinter,ghostMPI);
  }
  dG3DBoundaryConditionLagrangeFunctionSpace(int id, int ncomp, int comp1, int comp2, int comp3, const elementGroup *g, elementGroup *gi,
                                          elementGroup *vinter, elementGroup *ghostMPI) : dG3DLagrangeFunctionSpace(id, ncomp,comp1,comp2,comp3)
  {
    this->__init__(g,gi,vinter,ghostMPI);
  }
  dG3DBoundaryConditionLagrangeFunctionSpace(int id, int ncomp, int comp1, int comp2, int comp3, int comp4, const elementGroup *g, elementGroup *gi,
                                          elementGroup *vinter, elementGroup *ghostMPI) : dG3DLagrangeFunctionSpace(id, ncomp,comp1,comp2,comp3,comp4)
  {
    this->__init__(g,gi,vinter,ghostMPI);
  }
  dG3DBoundaryConditionLagrangeFunctionSpace(int id, int ncomp, int comp1, int comp2, int comp3, int comp4, int comp5, const elementGroup *g, elementGroup *gi,
                                          elementGroup *vinter, elementGroup *ghostMPI) : dG3DLagrangeFunctionSpace(id, ncomp,comp1,comp2,comp3,comp4,comp5)
  {
    this->__init__(g,gi,vinter,ghostMPI);
  }
  dG3DBoundaryConditionLagrangeFunctionSpace(int id, int ncomp, int comp1, int comp2, int comp3, int comp4, int comp5, int comp6, const elementGroup *g, elementGroup *gi,
                                          elementGroup *vinter, elementGroup *ghostMPI) : dG3DLagrangeFunctionSpace(id, ncomp,comp1,comp2,comp3,comp4,comp5,comp6)
  {
    this->__init__(g,gi,vinter,ghostMPI);
  }
  
  //
  dG3DBoundaryConditionLagrangeFunctionSpace(int id, int ncomp, const elementGroup *g, elementGroup *gBulk, elementGroup *ghostMPI) : 
    dG3DLagrangeFunctionSpace(id, ncomp)
  {
    this->__init__(g,gBulk,ghostMPI);
  }
  dG3DBoundaryConditionLagrangeFunctionSpace(int id, int ncomp, int comp1, const elementGroup *g, elementGroup *gBulk, elementGroup *ghostMPI) : 
    dG3DLagrangeFunctionSpace(id, ncomp,comp1)
  {
    this->__init__(g,gBulk,ghostMPI);
  }
  dG3DBoundaryConditionLagrangeFunctionSpace(int id, int ncomp, int comp1, int comp2, const elementGroup *g, elementGroup *gBulk, elementGroup *ghostMPI) : 
      dG3DLagrangeFunctionSpace(id, ncomp,comp1,comp2)
  {
    this->__init__(g,gBulk,ghostMPI);
  }
  dG3DBoundaryConditionLagrangeFunctionSpace(int id, int ncomp, int comp1, int comp2, int comp3, const elementGroup *g, elementGroup *gBulk, elementGroup *ghostMPI) : 
    dG3DLagrangeFunctionSpace(id, ncomp,comp1,comp2,comp3)
  {
    this->__init__(g,gBulk,ghostMPI);
  }
  dG3DBoundaryConditionLagrangeFunctionSpace(int id, int ncomp, int comp1, int comp2, int comp3, int comp4, const elementGroup *g, elementGroup *gBulk, elementGroup *ghostMPI) : 
        dG3DLagrangeFunctionSpace(id, ncomp,comp1,comp2,comp3,comp4)
  {
    this->__init__(g,gBulk,ghostMPI);
  }
  dG3DBoundaryConditionLagrangeFunctionSpace(int id, int ncomp, int comp1, int comp2, int comp3, int comp4, int comp5, const elementGroup *g, elementGroup *gBulk, elementGroup *ghostMPI) : 
        dG3DLagrangeFunctionSpace(id, ncomp,comp1,comp2,comp3,comp4,comp5)
  {
    this->__init__(g,gBulk,ghostMPI);
  }
  dG3DBoundaryConditionLagrangeFunctionSpace(int id, int ncomp, int comp1, int comp2, int comp3, int comp4, int comp5, int comp6, const elementGroup *g, elementGroup *gBulk, elementGroup *ghostMPI) : 
        dG3DLagrangeFunctionSpace(id, ncomp,comp1,comp2,comp3,comp4,comp5,comp6)
  {
    this->__init__(g,gBulk,ghostMPI);
  }
	
	virtual FunctionSpaceBase* clone(const int id) const{
		Msg::Error("dG3DBoundaryConditionLagrangeFunctionSpace::clone needs to be defined");
		return NULL;
	}
  
  virtual std::string getFunctionSpaceName() const 
  {
    return "dG3DBoundaryConditionLagrangeFunctionSpace";
  }

  virtual void getKeys(MInterfaceElement *ielem, std::vector<Dof> &keys) const{
    Msg::Error("Impossible to get keys on interface element for a Dirichlet Boundary Conditions");
  }
  virtual void getKeys(MElement *e, std::vector<Dof> &keys) const
  {
  
    std::map<MElement*,std::map<MElement*,std::vector<int> > >::const_iterator it = mapBoundaryInterfaces.find(e);
    if (it == mapBoundaryInterfaces.end()){
      Msg::Error("mapBoundaryInterfaces is wrong initialized in dG3DBoundaryConditionLagrangeFunctionSpace::getKeys");
    }
    else
    {
      const std::map<MElement*,std::vector<int> >& support = it->second;
      for (std::map<MElement*,std::vector<int> >::const_iterator itm = support.begin(); itm != support.end(); itm++)
      {
        MElement* ebulk = itm->first;
        const std::vector<int>& posfind = itm->second;
        int nbcomp=comp.size();
        
        #if defined(HAVE_MPI) // small duplication to avoid multiple if in a loop
        if( (ebulk->getPartition() != 0)and (ebulk->getPartition() != Msg::GetCommRank() +1))
        {
          
          for (int j=0;j<nbcomp;++j)
          {
            for (int i=0;i<posfind.size();i++)
            {
              if (withPrimaryShapeFunction(comp[j]))
              {
                // check if posfind[i] is a primary vertix of ebulk
                if (posfind[i] < ebulk->getNumPrimaryVertices())
                {
                  keys.push_back(Dof(ebulk->getNum(),-dG3DDof3IntType::createTypeWithThreeInts(comp[j],ifield[j],posfind[i])));
                }
              }
              else
              {
                keys.push_back(Dof(ebulk->getNum(),-dG3DDof3IntType::createTypeWithThreeInts(comp[j],ifield[j],posfind[i])));
              }
            }
          }
        }
        else
        #endif // HAVE_MPI
        {
          for (int j=0;j<nbcomp;++j)
          {
            for (int i=0;i<posfind.size();i++)
            {
              if (withPrimaryShapeFunction(comp[j]))
              {
                // check if posfind[i] is a primary vertix of ebulk
                if (posfind[i] < ebulk->getNumPrimaryVertices())
                {
                  keys.push_back(Dof(ebulk->getNum(),dG3DDof3IntType::createTypeWithThreeInts(comp[j],ifield[j],posfind[i])));
                }
              }
              else
              {
                keys.push_back(Dof(ebulk->getNum(),dG3DDof3IntType::createTypeWithThreeInts(comp[j],ifield[j],posfind[i])));
              }
            }
          }
        }
      }
    }
  }
  
  virtual void getKeysOnVertex(MElement *ele, MVertex *v, const std::vector<int> &cc, std::vector<Dof> &keys) const
  {
    std::map<MElement*,std::map<MElement*,std::vector<int> > >::const_iterator it = mapBoundaryInterfaces.find(ele);
    if (it == mapBoundaryInterfaces.end()){
      Msg::Error("mapBoundaryInterfaces is wrong initialized in dG3DBoundaryConditionLagrangeFunctionSpace::getKeys");
    }
    else
    {
      // find position of vextex in list vertex of element
      int numVer =ele->getNumVertices();
      int idx = -1;
      // find vertex index in element
      bool found = false;
      for (int iv = 0; iv<numVer; iv++){
        if (ele->getVertex(iv)  == v){
          found = true;
          idx = iv;
          break;
        };
      };
      if (found == false){
        Msg::Error("Vertex %d does not belong to element %d",v->getNum(),ele->getNum());
        return;
      }
      //
      const std::map<MElement*,std::vector<int> >& support = it->second;
      for (std::map<MElement*,std::vector<int> >::const_iterator itm = support.begin(); itm != support.end(); itm++)
      {
        MElement* ebulk = itm->first;
        const std::vector<int>& posfind = itm->second;
        int nbcomp=cc.size();
        
        #if defined(HAVE_MPI) // small duplication to avoid multiple if in a loop
        if( (ebulk->getPartition() != 0)and (ebulk->getPartition() != Msg::GetCommRank() +1))
        {
          for (int j=0;j<nbcomp;++j)
          {
            if (withPrimaryShapeFunction(cc[j]))
            {
              // check if posfind[i] is a primary vertix of ebulk
              if (posfind[idx] < ebulk->getNumPrimaryVertices())
              {
                keys.push_back(Dof(ebulk->getNum(),-dG3DDof3IntType::createTypeWithThreeInts(cc[j],ifield[j],posfind[idx])));
              }
            }
            else
            {
              keys.push_back(Dof(ebulk->getNum(),-dG3DDof3IntType::createTypeWithThreeInts(cc[j],ifield[j],posfind[idx])));
            }
          }
        }
        else
        #endif // HAVE_MPI
        {
          for (int j=0;j<nbcomp;++j)
          {
            if (withPrimaryShapeFunction(cc[j]))
            {
              // check if posfind[i] is a primary vertix of ebulk
              if (posfind[idx] < ebulk->getNumPrimaryVertices())
              {
                keys.push_back(Dof(ebulk->getNum(),dG3DDof3IntType::createTypeWithThreeInts(cc[j],ifield[j],posfind[idx])));
              }
            }
            else
            {
              keys.push_back(Dof(ebulk->getNum(),dG3DDof3IntType::createTypeWithThreeInts(cc[j],ifield[j],posfind[idx])));
            }
          }
        }
      }
    }
  };
};

// Space of the interface with acces to the two function space
class dG3DFunctionSpaceBetween2Domains : public FunctionSpaceBase, public interFunctionSpace{
 protected:
  FunctionSpaceBase *spaceMinus;
  FunctionSpaceBase *spacePlus;
  virtual void getKeysOnElement(MElement *ele, std::vector<Dof> &keys) const{
    Msg::Error("Impossible to get key for an element on a interface Domain");
  }
 public:
  dG3DFunctionSpaceBetween2Domains(FunctionSpaceBase*sp1, FunctionSpaceBase *sp2) : spaceMinus(sp1), spacePlus(sp2){}
  virtual ~dG3DFunctionSpaceBetween2Domains(){}
  
  virtual std::string getFunctionSpaceName() const 
  {
    return "dG3DFunctionSpaceBetween2Domains";
  }
	
	virtual FunctionSpaceBase* clone(const int id) const{
		return new dG3DFunctionSpaceBetween2Domains(spaceMinus,spacePlus);
	};
  
  virtual int getId(void) const {Msg::Error("dG3DFunctionSpaceBetween2Domains::getId cannot be called !!!"); return 0;};
  virtual int getNumKeys(MElement *ele) const
  {
    MInterfaceElement *iele = dynamic_cast<MInterfaceElement*>(ele);
    if (iele)
    {
      return spaceMinus->getNumKeys(iele->getElem(0)) +  spacePlus->getNumKeys(iele->getElem(1));
    }
    else
    {
      Msg::Error("dG3DFunctionSpaceBetween2Domains::getNumKeys cannot be called with bulk element !!!");
			return 0;
    }
  }
  virtual void getKeys(MElement *ele, std::vector<Dof> &keys) const
  {
    MInterfaceElement *iele = dynamic_cast<MInterfaceElement*>(ele);
    if (iele)
    {
      spaceMinus->getKeys(iele->getElem(0),keys);
      spacePlus->getKeys(iele->getElem(1),keys);
    }
    else
    {
      Msg::Error("dG3DFunctionSpaceBetween2Domains::getKeys cannot be called with bulk element !!!");
    }
  };
  virtual void getKeysOnVertex(MElement *ele, MVertex *v, const std::vector<int> &comp, std::vector<Dof> &keys) const
  {
    Msg::Error("dG3DFunctionSpaceBetween2Domains::getKeysOnVertex cannot be called with bulk element !!!");
  };

  // special function of interFunctionSpace
  virtual FunctionSpaceBase* getMinusSpace() {return spaceMinus;}
  virtual FunctionSpaceBase* getPlusSpace()  {return spacePlus;}
  
  virtual const FunctionSpaceBase* getMinusSpace()const {return spaceMinus;}
  virtual const FunctionSpaceBase* getPlusSpace() const {return spacePlus;}
  
  
  virtual void getNumKeys(MInterfaceElement *ele, int &numMinus, int &numPlus) const
  {
    numMinus = spaceMinus->getNumKeys(ele->getElem(0));
    numPlus = spacePlus->getNumKeys(ele->getElem(1));
  }
  virtual void getKeys(MInterfaceElement *ele, std::vector<Dof> &Rminus,std::vector<Dof> &Rplus) const
  {
    spaceMinus->getKeys(ele->getElem(0),Rminus);
    spacePlus->getKeys(ele->getElem(1),Rplus);
  }
};

#endif // DG3DFUNCTIONSPACE_H_
