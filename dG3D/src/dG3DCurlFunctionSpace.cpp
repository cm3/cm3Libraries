//
// C++ Interface: Function Space
//
// Description: FunctionSpace used (curl function space in 3D)
//
//
// Author:  <V-D Nguyen>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "dG3DCurlFunctionSpace.h"
#include "ipField.h"

bool g3DHierarchicalCurlFunctionSpace::initMap = false;
std::map<TwoNum, int> g3DHierarchicalCurlFunctionSpace::allEdges;
void g3DHierarchicalCurlFunctionSpace::numerateEdge()
{
  if (!initMap)
  {
    initMap = true;
    GModel* pModel = GModel::current();
    int dim = pModel->getNumRegions() ? 3 : 2;
    std::map<int, std::vector<GEntity*> > groups[4];
    pModel->getPhysicalGroups(groups);
    std::map<int, std::vector<GEntity*> > &entmap = groups[dim];

    elementGroup g;
    for (std::map<int, std::vector<GEntity*> >::iterator it = entmap.begin(); it!= entmap.end(); it++){
      std::vector<GEntity*> &ent = it->second;
      for (unsigned int i = 0; i < ent.size(); i++){
        for (unsigned int j = 0; j < ent[i]->getNumMeshElements(); j++){
          MElement *e = ent[i]->getMeshElement(j);
          g.insert(e);
        }
      }
    }
    for (elementGroup::elementContainer::const_iterator it = g.begin(); it != g.end(); it++)
    {
      MElement* ele = it->second;
      for (int i=0; i< ele->getNumEdges(); i++)
      {
        MEdge ide= ele->getEdge(i);
        MVertex* v0 = ide.getVertex(0);
        MVertex* v1 = ide.getVertex(1);
        TwoNum tn(v0->getNum(), v1->getNum());
        if (allEdges.find(tn) == allEdges.end())
        {
          int totalE = allEdges.size();
          allEdges[tn] = totalE;
        };
      }
    }
  }
};

int g3DHierarchicalCurlFunctionSpace::getEdgeNumber(int verNum1, int verNum2) 
{
  int maxVerNum = GModel::current()->getMaxVertexNumber();
  TwoNum tn(verNum1,verNum2);
  if (allEdges.find(tn) == allEdges.end())
  {
    int mapSize = allEdges.size();
    allEdges[tn] = maxVerNum+mapSize+1;
  }
  return allEdges[tn];
};