//
// C++ Interface: terms
//
// Description: Class with definition of terms for finite strain problem
//
//
// Author: V.D. NGUYEN 2011
//
// Copyright: See COPYING file that comes with this distribution
//
#ifndef HODGTERMS_H_
#define HODGTERMS_H_

#include "dG3DTerms.h"
#include "nlTerms.h"
#include "hoDGIPVariable.h"
#include "hoDGMaterialLaw.h"

class hoDGStiffnessBulk : public dG3DStiffnessBulk{
  public:
    hoDGStiffnessBulk(FunctionSpace<double>& space1, FunctionSpace<double>& space2,
                      materialLaw* law, const bool fdg, IPField* ipf ):
                      dG3DStiffnessBulk(space1,space2,law,fdg,ipf){};
    hoDGStiffnessBulk(FunctionSpace<double>& space, const materialLaw* law , const bool fdg, const IPField* ipf):
                      dG3DStiffnessBulk(space,law,fdg,ipf){};
    virtual ~hoDGStiffnessBulk(){};

    virtual void get(MElement *ele, int npts, IntPt *GP, fullMatrix<double> &m) const;
    virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const {
      Msg::Error("Define me ?? get by integration point hoDGStiffnessBulk");
    };
    virtual void set(const fullVector<double> *datafield) {} ;
    virtual const bool isData() const{ return false;};
    virtual BilinearTermBase* clone () const{
      hoDGStiffnessBulk *DG= new hoDGStiffnessBulk(space1,_mlaw,_fullDg,_ipf);
      DG->setNonLocalEqRatio(this->getNonLocalEqRatio());
      DG->setNumNonLocalVariable(this->getNumNonLocalVariable());
      return DG;
    };
};



class hoDGForceBulk : public dG3DForceBulk{
  public:
    hoDGForceBulk(FunctionSpace<double>& space, const materialLaw* law ,const bool fdg, const IPField* ipf , const bool increBased)
                :dG3DForceBulk(space,law,fdg,ipf,increBased){};
    virtual ~hoDGForceBulk(){};
    virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullVector<double> > &vv) const{
      Msg::Error("Define me ?? get by integration point hoDGForceBulk");
    };
    virtual void get(MElement *ele,int npts,IntPt *GP,fullVector<double> &m) const;
    virtual const bool isData() const {
      return false;
    };
    virtual void set(const fullVector<double> *datafield){};
    virtual LinearTermBase<double>* clone () const{
      hoDGForceBulk* DG= new hoDGForceBulk(space1,_mlaw,_fullDg,_ipf,_incrementNonlocalBased); // we need a copy constructor or to pass all the parameters in the constructor
      DG->setNonLocalEqRatio(this->getNonLocalEqRatio());
      DG->setNumNonLocalVariable(this->getNumNonLocalVariable());
      return DG;
    };
};


class hoDGStiffnessInter : public dG3DStiffnessInter{
  private:
    mutable const hoDGIPVariable *vipv[256];
    mutable const hoDGIPVariable* vipvprev[256];

  public:
    hoDGStiffnessInter(FunctionSpace<double>& space1_,FunctionSpace<double>& space2_,
                        const materialLaw* mlawMinus, const materialLaw* mlawPlus,interfaceQuadratureBase *iquad,
                        double beta1_, const IPField *ip, unknownField *uf,
                        bool fulldg, const bool increBased,double eps=1.e-6)
                       : dG3DStiffnessInter(space1_,space2_,mlawMinus,mlawPlus,iquad,beta1_,ip,uf,fulldg,increBased,eps){};

    virtual ~hoDGStiffnessInter(){};

    virtual void get(MElement *ele,int npts,IntPt *GP, fullMatrix<double> &m)const;
    virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const{
      Msg::Error("Define me ?? get by integration point hoDGStiffnessInter");
    };
    virtual void set(const fullVector<double> *datafield) {} ;
    virtual const bool isData() const{ return false;};
    virtual BilinearTermBase* clone () const{
      hoDGStiffnessInter* DG= new hoDGStiffnessInter(space1,space2,_mlawMinus,_mlawPlus,_interQuad,_stabilityParameter,
                                              _ipf,_ufield,_fullDg,_incrementNonlocalBased,_perturbation);
      DG->setNonLocalEqRatio(this->getNonLocalEqRatio());
      DG->setNumNonLocalVariable(this->getNumNonLocalVariable());
      DG->setNonLocalContinuity(this->getNonLocalContinuity());
      DG->setNonLocalStabilityParameter(this->getNonLocalStabilityParameter());
      return DG;
    }

};

class hoDGForceInter : public dG3DForceInter{
  protected:
    mutable const hoDGIPVariable *vipv[256];
    mutable const hoDGIPVariable* vipvprev[256];

  public:
    hoDGForceInter(FunctionSpace<double>& space1_, FunctionSpace<double> *space2_,
                   const materialLaw *mlawMinus, const materialLaw *mlawPlus,interfaceQuadratureBase *iquad,
                   double beta1_, bool fdg, const IPField *ip, const bool increBased):
                   dG3DForceInter(space1_,space2_,mlawMinus,mlawPlus,iquad,beta1_,fdg,ip,_incrementNonlocalBased){};

    virtual ~hoDGForceInter(){};

    virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullVector<double> > &vv) const{
      Msg::Error("Define me ?? get by integration point hoDGForceInter");
    };
    virtual void get(MElement *ele,int npts,IntPt *GP,fullVector<double> &m) const;
    virtual const bool isData() const {
      return false;
    };
    virtual void set(const fullVector<double> *datafield){};
    virtual LinearTermBase<double>* clone () const{
      hoDGForceInter* DG= new hoDGForceInter(*_minusSpace,_plusSpace,_mlawMinus,_mlawPlus,_interQuad,_stabilityParameter,_fullDg,_ipf,_incrementNonlocalBased); // we need a copy constructor or to pass all the parameters in the constructor
      DG->setNonLocalEqRatio(this->getNonLocalEqRatio());
      DG->setNumNonLocalVariable(this->getNumNonLocalVariable());
      DG->setNonLocalContinuity(this->getNonLocalContinuity());
      DG->setNonLocalStabilityParameter(this->getNonLocalStabilityParameter());
      return DG;
    };

};




#endif // HODGTERMS_H_
