//
// C++ Interface: reduction
//
// Author:  <V.D Nguyen>, (C) 2018
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef DG3DREDUCTION_H_
#define DG3DREDUCTION_H_

#include "dG3DMaterialLaw.h"
#include "dG3DIPVariable.h"

inline void reduction3DTo1DStress(dG3DMaterialLaw *mlaw, IPVariable* ipv, const IPVariable* ipvprev, bool stiff, bool checkfrac){
	dG3DIPVariableBase* dgipv = dynamic_cast<dG3DIPVariableBase*>(ipv);
  const dG3DIPVariableBase* dgipvPrev = dynamic_cast<const dG3DIPVariableBase*>(ipvprev);
  if (dgipv == NULL) Msg::Error("dG3DIPVariableBase must be used in dG3D1DDomain::reduction3DTo1DStress");

  const STensor3& Fprev = dgipvPrev->getConstRefToDeformationGradient();
  const STensor3& Pprev = dgipvPrev->getConstRefToFirstPiolaKirchhoffStress();

  STensor3& F = dgipv->getRefToDeformationGradient();
  STensor3& P = dgipv->getRefToFirstPiolaKirchhoffStress();
  STensor43& L = dgipv->getRefToTangentModuli();


  double tol = 1e-6;
  double refTol = 1e-12;
  //F.print("F after computing");
  double Fyy = Fprev(1,1);
  F(1,1) = F(2,2) = Fyy;
  mlaw->stress(ipv,ipvprev,true,checkfrac);
  if (STensorOperation::isnan(P)){
    Msg::Error("P is NAN");
    return;
  }

  double Pyy = P(1,1);
  if (fabs(Pyy) > refTol) {
    // solution
    int iter = 0;
    int maxIter = 100;
    double Pxx = fabs(P(0,0));
    while (fabs(Pyy) > tol*Pxx){
      Fyy -= Pyy/(L(1,1,1,1)+L(1,1,2,2));
      F(1,1) = F(2,2) = Fyy;
      mlaw->stress(ipv,ipvprev,true,checkfrac);
      if (STensorOperation::isnan(P)){
        Msg::Error("P is NAN");
        return;
      }
      Pyy = P(1,1);
      iter++;

     // Msg::Info("iter = %d f= %e",iter,fabs(Pyy/Pxx));
      if (iter > maxIter){
        Msg::Error("iteration is not converged");
        break;
      }
    }
  }

  double dFyydFxx = -L(1,1,0,0)/(L(1,1,1,1)+L(1,1,2,2));
  L(0,0,0,0) += (L(0,0,1,1)+L(0,0,2,2))*dFyydFxx;


  int numNonlocalVar = dgipv->getNumberNonLocalVariable();

  if (numNonlocalVar > 0){
    std::vector<STensor3>& dPDnonlocalVar = dgipv->getRefToDStressDNonLocalVariable();
    for (int i=0; i<numNonlocalVar; i++){
      STensor3& dPi = dPDnonlocalVar[i];
      double dFyyDNonlocalVar =  -dPi(1,1)/(L(1,1,1,1)+L(1,1,2,2));
      dPi(0,0) += (L(0,0,1,1)+L(0,0,2,2))*dFyyDNonlocalVar;
    }
  }

};

inline void reduction3DTo1DStress_constantTriaxiality(double T, dG3DMaterialLaw *mlaw, IPVariable* ipv, const IPVariable* ipvprev, bool stiff, bool checkfrac){
	dG3DIPVariableBase* dgipv = dynamic_cast<dG3DIPVariableBase*>(ipv);
  const dG3DIPVariableBase* dgipvPrev = dynamic_cast<const dG3DIPVariableBase*>(ipvprev);
  if (dgipv == NULL) Msg::Error("dG3DIPVariableBase must be used in dG3D1DDomain::reduction3DTo1DStress");

  const STensor3& Fprev = dgipvPrev->getConstRefToDeformationGradient();
  const STensor3& Pprev = dgipvPrev->getConstRefToFirstPiolaKirchhoffStress();

  STensor3& F = dgipv->getRefToDeformationGradient();
  STensor3& P = dgipv->getRefToFirstPiolaKirchhoffStress();
  STensor43& L = dgipv->getRefToTangentModuli();


  double tol = 1e-6;
  double refTol = 1e-12;
  //F.print("F after computing");
  double Fyy = Fprev(1,1);
  F(1,1) = F(2,2) = Fyy;
  mlaw->stress(ipv,ipvprev,true,checkfrac);
  if (STensorOperation::isnan(P)){
    Msg::Error("P is NAN");
    return;
  };

  double Pxx0 = P(0,0);
  double k = (3.*T-1.)/(3.*T+2.); // constant factor PyyFyy -kPxx*Fxx = 0;
  double res = P(1,1)*Fyy - k*P(0,0)*F(0,0);
  double DresDFyy = P(1,1)+ (L(1,1,1,1)+L(1,1,2,2))*Fyy - k*(L(0,0,1,1) + L(0,0,2,2))*F(0,0);
  if (fabs(res) > refTol) {
    // solution
    int iter = 0;
    int maxIter = 100;
    double Pxx = fabs(P(0,0));
    while (fabs(res) > tol*Pxx0){
      DresDFyy = P(1,1)+ (L(1,1,1,1)+L(1,1,2,2))*Fyy - k*(L(0,0,1,1) + L(0,0,2,2))*F(0,0);
      Fyy -= res/DresDFyy;
      F(1,1) = F(2,2) = Fyy;
      mlaw->stress(ipv,ipvprev,true,checkfrac);
      if (STensorOperation::isnan(P)){
        Msg::Error("P is NAN");
        return;
      }
      res = P(1,1)*Fyy - k*P(0,0)*F(0,0);
      iter++;

     // Msg::Info("iter = %d res= %e",iter,fabs(res/Pxx0));
      if (iter > maxIter){
        Msg::Error("iteration is not converged");
        P(0,0) = sqrt(-1.);
        return;
      }
    }
  }
  double DresDFxx = L(1,1,0,0)*Fyy - k*P(0,0) - k *L(0,0,0,0)*F(0,0);
  double dFyydFxx = -DresDFxx/DresDFyy;
  L(0,0,0,0) += (L(0,0,1,1)+L(0,0,2,2))*dFyydFxx;


  int numNonlocalVar = dgipv->getNumberNonLocalVariable();

  if (numNonlocalVar > 0){
    std::vector<STensor3>& dPDnonlocalVar = dgipv->getRefToDStressDNonLocalVariable();
    for (int i=0; i<numNonlocalVar; i++){
      STensor3& dPi = dPDnonlocalVar[i];
      double DresDnonlocalVar = dPi(1,1)*Fyy - k*dPi(0,0)*F(0,0);
      double dFyyDNonlocalVar =  -DresDnonlocalVar/DresDFyy;
      dPi(0,0) += (L(0,0,1,1)+L(0,0,2,2))*dFyyDNonlocalVar;
    }
  }

};

inline void reduction3DTo2DStress(dG3DMaterialLaw *mlaw, IPVariable* ipv, const IPVariable* ipvprev, bool stiff, bool checkfrac, bool dTangent){
	// reduction from XYZ--> XY
	// Fzz is found by solving the equation Pzz = 0

	dG3DIPVariableBase* dgipv = dynamic_cast<dG3DIPVariableBase*>(ipv);
  const dG3DIPVariableBase* dgipvPrev = dynamic_cast<const dG3DIPVariableBase*>(ipvprev);
  if (dgipv == NULL) Msg::Error("dG3DIPVariableBase must be used in dG3D1DDomain::reduction3DTo1DStress");

  const STensor3& Fprev = dgipvPrev->getConstRefToDeformationGradient();
  const STensor3& Pprev = dgipvPrev->getConstRefToFirstPiolaKirchhoffStress();

  STensor3& F = dgipv->getRefToDeformationGradient();
  STensor3& P = dgipv->getRefToFirstPiolaKirchhoffStress();
  STensor43& L = dgipv->getRefToTangentModuli();

	double tol = 1e-6;
  double refTol = 1e-12;
  //F.print("F after computing");
	// starting from previous
  double Fzz= Fprev(2,2);
	F(2,2) = Fzz;

	// stress evaluation
  mlaw->stress(ipv,ipvprev,true,checkfrac);
  if (STensorOperation::isnan(P)){
    Msg::Error("P is NAN");
    return;
  }
  double Pzz = P(2,2);
  if (fabs(Pzz) > refTol) {
    // solution
    int iter = 0;
    int maxIter = 100;

    double Pref = 0.5*sqrt(P(0,0)*P(0,0) + P(1,1)*P(1,1) + P(0,1)*P(0,1) + P(1,0)*P(1,0));

    while (fabs(Pzz) > tol*Pref){
      Fzz -= Pzz/L(2,2,2,2);
      F(2,2)= Fzz;
      mlaw->stress(ipv,ipvprev,true,checkfrac);
      if (STensorOperation::isnan(P)){
        Msg::Error("P is NAN");
        return;
      }
      Pzz = P(2,2);
      iter++;

     	//Msg::Info("iter = %d f= %e",iter,fabs(Pzz/Pref));
      if (iter > maxIter){
        P(0,0) = sqrt(-1.);
        Msg::Error("maximal iteration %d is reached", maxIter);
        return;
      }
    }
  }

	// update tangent due to plan stress contrain
	double dFzzdFxx = -L(2,2,0,0)/L(2,2,2,2);
	double dFzzdFxy = -L(2,2,0,1)/L(2,2,2,2);
	double dFzzdFyx = -L(2,2,1,0)/L(2,2,2,2);
	double dFzzdFyy = -L(2,2,1,1)/L(2,2,2,2);

	L(0,0,0,0) += L(0,0,2,2)*dFzzdFxx;
	L(0,0,0,1) += L(0,0,2,2)*dFzzdFxy;
	L(0,0,1,0) += L(0,0,2,2)*dFzzdFyx;
	L(0,0,1,1) += L(0,0,2,2)*dFzzdFyy;

	L(0,1,0,0) += L(0,1,2,2)*dFzzdFxx;
	L(0,1,0,1) += L(0,1,2,2)*dFzzdFxy;
	L(0,1,1,0) += L(0,1,2,2)*dFzzdFyx;
	L(0,1,1,1) += L(0,1,2,2)*dFzzdFyy;

	L(1,0,0,0) += L(1,0,2,2)*dFzzdFxx;
	L(1,0,0,1) += L(1,0,2,2)*dFzzdFxy;
	L(1,0,1,0) += L(1,0,2,2)*dFzzdFyx;
	L(1,0,1,1) += L(1,0,2,2)*dFzzdFyy;

	L(1,1,0,0) += L(1,1,2,2)*dFzzdFxx;
	L(1,1,0,1) += L(1,1,2,2)*dFzzdFxy;
	L(1,1,1,0) += L(1,1,2,2)*dFzzdFyx;
	L(1,1,1,1) += L(1,1,2,2)*dFzzdFyy;

  int numNonlocalVar = dgipv->getNumberNonLocalVariable();
  if (numNonlocalVar > 0){
    std::vector<STensor3>& dPDnonlocalVar = dgipv->getRefToDStressDNonLocalVariable();
    for (int i=0; i<numNonlocalVar; i++){
      STensor3& dPi = dPDnonlocalVar[i];
      double dFzzDNonlocalVar =  -dPi(2,2)/L(2,2,2,2);
      dPi(0,0) += L(0,0,2,2)*dFzzDNonlocalVar;
			dPi(0,1) += L(0,1,2,2)*dFzzDNonlocalVar;
			dPi(1,0) += L(1,0,2,2)*dFzzDNonlocalVar;
			dPi(1,1) += L(1,1,2,2)*dFzzDNonlocalVar;
    }
  }
};



inline void reduction3DTo2DStress_XZ(dG3DMaterialLaw *mlaw, IPVariable* ipv, const IPVariable* ipvprev, bool stiff, bool checkfrac, bool dTangent){
	// reduction from XYZ--> XZ
	// Fyy is found by solving the equation Pyy = 0

	dG3DIPVariableBase* dgipv = dynamic_cast<dG3DIPVariableBase*>(ipv);
  const dG3DIPVariableBase* dgipvPrev = dynamic_cast<const dG3DIPVariableBase*>(ipvprev);
  if (dgipv == NULL) Msg::Error("dG3DIPVariableBase must be used in dG3D1DDomain::reduction3DTo1DStress");

  const STensor3& Fprev = dgipvPrev->getConstRefToDeformationGradient();
  const STensor3& Pprev = dgipvPrev->getConstRefToFirstPiolaKirchhoffStress();

  STensor3& F = dgipv->getRefToDeformationGradient();
  STensor3& P = dgipv->getRefToFirstPiolaKirchhoffStress();
  STensor43& L = dgipv->getRefToTangentModuli();

  double tol = 1e-6;
  double refTol = 1e-12;
  //F.print("F after computing");
 // starting from previous
  double Fyy= Fprev(1,1);
  F(1,1) = Fyy;

  // stress evaluation
  mlaw->stress(ipv,ipvprev,true,checkfrac);
  if (STensorOperation::isnan(P)){
    Msg::Error("P is NAN");
    return;
  }
  double Pyy = P(1,1);
  if (fabs(Pyy) > refTol) {
    // solution
    int iter = 0;
    int maxIter = 100;

    double Pref = 0.5*sqrt(P(0,0)*P(0,0) + P(2,2)*P(2,2) + P(0,2)*P(0,2) + P(2,0)*P(2,0));

    while (fabs(Pyy) > tol*Pref){
      Fyy -= Pyy/L(1,1,1,1);
      F(1,1)= Fyy;
      mlaw->stress(ipv,ipvprev,true,checkfrac);
      if (STensorOperation::isnan(P)){
        Msg::Error("P is NAN");
        return;
      }
      Pyy = P(1,1);
      iter++;

     	//Msg::Info("iter = %d f= %e",iter,fabs(Pyy/Pref));
      if (iter > maxIter){
        P(0,0) = sqrt(-1.);
        Msg::Error("maximal iteration %d is reached", maxIter);
        return;
      }
    }
  }

	// update tangent due to plan stress contrain
	double dFyydFxx = -L(1,1,0,0)/L(1,1,1,1);
	double dFyydFxz = -L(1,1,0,2)/L(1,1,1,1);
	double dFyydFzx = -L(1,1,2,0)/L(1,1,1,1);
	double dFyydFzz = -L(1,1,2,2)/L(1,1,1,1);
	
	L(0,0,0,0) += L(0,0,1,1)*dFyydFxx;
	L(0,0,0,2) += L(0,0,1,1)*dFyydFxz;
	L(0,0,2,0) += L(0,0,1,1)*dFyydFzx;
	L(0,0,2,2) += L(0,0,1,1)*dFyydFzz;
	
	L(0,2,0,0) += L(0,2,1,1)*dFyydFxx;
	L(0,2,0,2) += L(0,2,1,1)*dFyydFxz;
	L(0,2,2,0) += L(0,2,1,1)*dFyydFzx;
	L(0,2,2,2) += L(0,2,1,1)*dFyydFzz;
	
	L(2,0,0,0) += L(2,0,1,1)*dFyydFxx;
	L(2,0,0,2) += L(2,0,1,1)*dFyydFxz;
	L(2,0,2,0) += L(2,0,1,1)*dFyydFzx;
	L(2,0,2,2) += L(2,0,1,1)*dFyydFzz;	
	
	L(2,2,0,0) += L(2,2,1,1)*dFyydFxx;
	L(2,2,0,2) += L(2,2,1,1)*dFyydFxz;
	L(2,2,2,0) += L(2,2,1,1)*dFyydFzx;
	L(2,2,2,2) += L(2,2,1,1)*dFyydFzz;


  int numNonlocalVar = dgipv->getNumberNonLocalVariable();
  if (numNonlocalVar > 0){
    std::vector<STensor3>& dPDnonlocalVar = dgipv->getRefToDStressDNonLocalVariable();
    for (int i=0; i<numNonlocalVar; i++){
      STensor3& dPi = dPDnonlocalVar[i];
      double dFyyDNonlocalVar =  -dPi(1,1)/L(1,1,1,1);
      dPi(0,0) += L(0,0,1,1)*dFyyDNonlocalVar;
      dPi(0,2) += L(0,2,1,1)*dFyyDNonlocalVar;
      dPi(2,0) += L(2,0,1,1)*dFyyDNonlocalVar;
      dPi(2,2) += L(2,2,1,1)*dFyyDNonlocalVar;
    }
  }
};
#endif // DG3DREDUCTION_H_




