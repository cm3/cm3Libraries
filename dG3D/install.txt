INSTALL dG3D for Ubuntu 11.04 and subsequent
A JERUSALEM 11/7/2012

**************************************************************
*******************     Installing Gmsh    *******************
**************************************************************

Please check cm3-apps/install.txt

Note that for Ubuntu, you can use the Petsc provided on the Ubuntu (11.04+) repository (no need to follow the part on the manual download, except for adding in the .bashrc:

export PETSC_DIR=/usr/lib/petsc
export PETSC_ARCH=linux-gnu-c-opt (or -debug) 

; but make sure you download both the dev (or dbg) libraries), however the Swig2.0 does not work for Ubuntu 11.04, but does for 12.04+. In the former case, it needs to be installed from the website as described.

If you have an error during the compilation about mpi.h which is not found for petsc (can occur with petsc 3.2) you must define in your bashrc the variable NLSMPIINC which has to give the path of your mpi include (eg /usr/lib64/mpi/gcc/openmpi/include) type then bash in your shell of compilation and compile again.

Important note: it seems that Petsc on this distribution only works with openmpi (no mpich). If you try with mpich, it will warn you with a message anyway. However, I found out that among the necessary packages that Petsc installs, one is libscalapack-mpi-dev and libhdf5-mpi-dev which are for mpich (!). Appartently it's no trouble, but well...

Finally, I did not install gotoblas.


**************************************************************
*********************     Running Gmsh    ********************
**************************************************************

On Ubuntu, openmpi has a small bug and running a problem using:
> python myproblem.py

will give you the (well known) following error:

python: symbol lookup error: /usr/lib/openmpi/lib/openmpi/mca_paffinity_linux.so: undefined symbol: mca_base_param_reg_int


to this end, please use the python-gmsh that I put and replace python by python-gmsh for any run.

Example with 1 proc:
> ./python-gmsh myproblem.py

Example with 666 procs:
> mpiexec -n 666 ./python-gmsh myproblem.py


PS: Note that the library that is used in the script is the one by default on ubuntu, please modify it if needed
PPS: You can also add in 
export LD_PRELOAD=$LD_PRELOAD:/usr/lib/openmpi/lib/libmpi.so
your .bashrc
