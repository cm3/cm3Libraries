@@@@@ Van Dung NGUYEN @@@@

Build code from the command line
----------------------------------------------
In clusters, try to load CMake, OpenMPI, GCC, Python, SWIG

----------------------------------------------
* create install directory

    export CM3_DIR=$HOME/workspace
    mkdir $CM3_DIR

* get PETSc and compile (mandatory if PETSc is not available)

#cloning source petsc
cd $CM3_DIR
git clone -b main https://gitlab.com/petsc/petsc.git petsc

#choose PETSc version
cd $CM3_DIR/petsc
export PETSC_VERSION=3.19.4
git checkout tags/v$PETSC_VERSION

#configuration and compiling
export PETSC_DIR=$PWD
export PETSC_ARCH=arch-linux2-c-opt
./configure --with-debugging=0 --with-mpi=1 \
--download-fblaslapack=1 \
--download-scalapack=1 \
--download-mumps=1 \
--with-shared-libraries=1 && \
make PETSC_DIR=$PETSC_DIR PETSC_ARCH=$PETSC_ARCH all


* code installation

#get source code and gmsh
cd $CM3_DIR
git clone https://gitlab.onelab.info/gmsh/gmsh.git gmsh
# clone protected repository with your username
git clone https://username@gitlab.onelab.info/cm3/cm3Libraries.git cm3Libraries

#make a build directory
cd $CM3_DIR/cm3Libraries/dG3D && mkdir release && cd release

# configuring
cmake -DCMAKE_BUILD_TYPE=Release \
-DENABLE_CG_MPI_INTERFACE=OFF \
-DENABLE_MPI=ON \
-DENABLE_FLTK=OFF \
-DENABLE_PETSC=ON \
-DENABLE_PRIVATE_API=ON \
-DENABLE_WRAP_PYTHON=ON \
-DPETSC_DIR=$PETSC_DIR \
-DPETSC_ARCH=$PETSC_ARCH ..

otherwise use "ccmake .." and configure manually

# if there are conflicts with python library, include directory
# python include dir and library must be found
export PYTHON_INCLUDE_DIR=path-to-python-include
export PYTHON_LIBRARY=path-to-python-library

# compile with python
cmake -DCMAKE_BUILD_TYPE=Release \
-DENABLE_CG_MPI_INTERFACE=OFF \
-DENABLE_MPI=ON \
-DENABLE_FLTK=OFF \
-DENABLE_PETSC=ON \
-DENABLE_PRIVATE_API=ON \
-DENABLE_WRAP_PYTHON=ON \
-DPYTHON_INCLUDE_DIR=$PYTHON_INCLUDE_DIR \
-DPYTHON_LIBRARY=$PYTHON_LIBRARY \
-DPETSC_DIR=$PETSC_DIR \
-DPETSC_ARCH=$PETSC_ARCH ..


# building code
make -j4

# add path for gmsh and python path, add to .bashrc
export CM3_DIR=$HOME/workspace
export PATH=$CM3_DIR/cm3Libraries/dG3D/release/NonLinearSolver/gmsh:$PATH
export PYTHONPATH=$CM3_DIR/cm3Libraries/dG3D/release:$CM3_DIR/cm3Libraries/dG3D/release/NonLinearSolver/gmsh/utils/wrappers:$PYTHONPATH

# To use PETSc's built-in direct LU solver,
# create the empty file .petscrc in $HOME and add the following options
-ksp_type preonly
-pc_type lu

