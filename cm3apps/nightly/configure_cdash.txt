0) ubuntu 14 Install the package required by cdash (see cdash documentation) this includes mysql, php5, apache2,...
for unbuntu sudo apt-get install apache2 mysql-server php5 php5-mysql php5-xsl php5-curl php5-gd
 also install the mail server
sudo apt-get install mailutils

ubuntu 16, no php5, but php7:
sudo apt-get install mysql-server apache2 php  php-mysql php-xml php-curl php-gd libapache2-mod-php php-mcrypt
sudo apt-get install mailutils

1) Extract the zip in the /srv/www/htdocs or (ubuntu) /var/www/html
unzip blabla

2) copy the config.local.php in CDash/cdash
see information on http://www.vtk.org/Wiki/CDash:Installation


3) Allow apache to have an index.php as home page
 edit /etc/apache2/httpd.conf or /etc/apache2/mods-available/dir.conf and search for DirectoryIndex on the list of possible index file (as index.html) add index.php
DirectoryIndex index.php index.html index.html.var 

4) ensure that the CDash/rss CDash/backup CDash/uplaod directories are writable by the user account that Apache runs under (typically wwwrun or www-data). 

sudo mkdir /var/www/htdocs/CDash/rss
sudo groupadd cdashaccess
sudo usermod -a -G cdashaccess www-data
sudo chgrp -R cdashaccess  /var/www/html/CDash/rss
sudo chmod -R g+wx /var/www/html/CDash/rss
sudo chgrp -R cdashaccess  /var/www/html/CDash/backup/
sudo chmod -R g+wx /var/www/html/CDash/backup/
sudo chgrp -R cdashaccess  /var/www/html/CDash/upload/
sudo chmod -R g+wx /var/www/html/CDash/upload/


5) Copy cdash.conf in /etc/apache2/mods-enabled/ (modity cdash-conf if not ubuntu see point 1)

6) start required service
ubuntu 14: a2enmod php5 ubuntu 16: it should be automatic (check in /etc/apache2/mods-enabled if php7.0 appears if not a2enmod php7.0)
sudo service mysql start
sudo service apache2 start

7) Set auto start of the service at boot (in ubuntu use sysv-rc-conf) 
sudo chkconfig mysql on
sudo chkconfig apache2 on

8) check if php is working
copy test.php in /var/www/html and load cm3011.ltas.ulg.ac.be/test.php in a browser on the station (see firwall problm, point 10)

9) start a web browser on the station and connect to cm3011.ltas.ulg.ac.be/CDash follow installation
   create the project cm3_nightly
   create the project gmsh_nightly
   create the project mfh_nightly
   create the project cp_nightly

10) on the running machine
   add ani IN exception (http) in firewall (using gufw )

11) (no need with the new git) copy private key without passwd in $HOME/cron//id_rsa.cvsbackup (only user readable)
   copy public key without passwd in your cm3015:$HOME/.ssh/authorized_keys(2) 
   copy cvs.cron.identity in $HOME

12)   make sure the password of git is stored: 
   git config --global credential.helper store  (#use store for long time period , cache for 5 minute)
   ##git config --global credential.helper cache
   ##git config --system credential.helper cache ## not this one

            "I tried to copy public key in git project to have access without password but it does not work"

13) at first you need to get an initial version of gmsh cm3Libraries (the initial clone does not work)

  /usr/bin/git clone http://gitlab.onelab.info/gmsh/gmsh.git $HOME/nightly_gmsh/gmsh
  /usr/bin/git clone http://$USER@gitlab.onelab.info/cm3/cm3Libraries.git $HOME/nightly_gmsh/cm3Libraries
  /usr/bin/git clone http://$USER@gitlab.onelab.info/cm3/cm3MFH.git $HOME/nightly_gmsh/cm3MFH
  /usr/bin/git clone http://$USER@gitlab.onelab.info/cm3/cm3Private.git $HOME/nightly_gmsh/cm3Private

14) add the crontab for automatic execution 
  crontab -e -u noels
  and copy paste the nighly.cron file

